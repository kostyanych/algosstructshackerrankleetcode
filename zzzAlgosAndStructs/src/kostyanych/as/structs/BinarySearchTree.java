package kostyanych.as.structs;

public class BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T> {
	
	public static <T extends Comparable<T>> BinarySearchTree<T> startTree(T data){
		BinaryTreeNode<T> node=new BinaryTreeNode<T>();
		node.data=data;
		BinarySearchTree<T> tree=new BinarySearchTree<T>();
		tree.root=node;
		return tree;
	}
	
	public BinaryTreeNode<T> insert(T value) {
		
		BinaryTreeNode<T> buf=root;
        
		BinaryTreeNode<T> ins=new BinaryTreeNode<T>();
        ins.data=value;
        
        if (root==null)
            return ins;
        
        while (true) {
            if (buf.data.equals(value))
                break;
            if (buf.data.compareTo(value)>0){
                if (buf.left==null) {
                    buf.left=ins;
                    break;
                }
                buf=buf.left;
            }
            else {
                if (buf.right==null) {
                    buf.right=ins;
                    break;
                }
                buf=buf.right;
            }
        }
        return ins;
    }
	
	/**
	 * Lowest Common Ancestor
	 * @param v1
	 * @param v2
	 * @return
	 */
	public BinaryTreeNode<T> lca(T v1, T v2) {
        
		//making it so that v1<v2 (v2 is on the left and v2 is on the right)
        if (v1.compareTo(v2)>0) {
            T buf=v2;
            v2=v1;
            v1=buf;
        }
        
        BinaryTreeNode<T> node=root;
        
        while (! (node.data.compareTo(v1)>0 && node.data.compareTo(v2)<0)) {
        	//if v1 and v2 are on different sides of node, then node is their LCA
            if (node.data.equals(v1) || node.data.equals(v2)) {
            //if v1 or v2 IS the node, then node is their LCA
                break;
            }
            if (node.data.compareTo(v1)>0)
            	node=node.left;
            else
            	node=node.right;
        }
        return node;
    }
	
	public BinaryTreeNode<T> findNode(T dt) {
		if (root==null)
			return null;
		return findNode(root,dt);
	}
	
	public BinaryTreeNode<T> findNode(BinaryTreeNode<T> rt, T dt) {
		if (rt==null)
			return null;
		if (rt.data.equals(dt))
			return root;
		if (dt.compareTo(rt.data)>0)
			return findNode(rt.right, dt);
		return findNode(rt.left, dt);
	}
	
	public BinaryTreeNode<T> findMinValueNode(BinaryTreeNode<T> rt) {
		BinaryTreeNode<T> res=rt;
        while (res.left != null) {
        	res = res.left;
        }
        return res;
    }

	public BinaryTreeNode<T> findMaxValueNode(BinaryTreeNode<T> rt) {
		BinaryTreeNode<T> res=rt;
        while (res.right != null) {
        	res = res.right;
        }
        return res;
    }
	
	public void deleteNodeWithValue(T dt) {
		root = deleteNode(root, dt);
	}
	 
	/* A recursive function to delete a node fro BST */
	public BinaryTreeNode<T> deleteNode(BinaryTreeNode<T> rt, T dt) {
		/* Base Case: If the tree is empty */
	    if (rt == null)
	    	return rt;
	 
	    /* Otherwise, recur down the tree */
	    if (dt.compareTo(rt.data)<0)
	    	rt.left = deleteNode(rt.left, dt);
	    else if (dt.compareTo(rt.data)>0)
	    	rt.right = deleteNode(rt.right, dt);
	    else {
	    	// if key is same as root's key, then This is the node
	        // to be deleted
            // node with only one child or no child
            if (rt.left == null)
                return rt.right;
            else if (rt.right == null)
                return rt.left;
	 
            // node with two children: Get the inorder successor (smallest
            // in the right subtree)
            rt.data=findMinValueNode(rt.right).data;
            
            // Delete the inorder successor
            rt.right = deleteNode(rt.right, rt.data);
        }
	 
	    return rt;
	}

//	public boolean isRealBST()  {
//		if (root==null)
//			return true;
//		T min=findMinValueNode(root).data;
//		T max=findMaxValueNode(root).data;
//		return (isBSTUtilLeft(root.left, min, root.data) &&
//                isBSTUtilRight(root.right, root.data, max));
//	}
//	 
//    /* Returns true if the given tree is a BST and its
//      values are >= min and <= max. */
//	public boolean isBSTUtilLeft(BinaryTreeNode<T> node, T min, T max) {
//		/* an empty tree is BST */
//	    if (node == null)
//	    	return true;
//	 
//	    /* false if this node violates the min/max constraints */
//	    if (node.data.compareTo(min) <= 0 || node.data.compareTo(max) > 0)
//	    	return false;
//	 
//	    /* otherwise check the subtrees recursively
//	    tightening the min/max constraints */
//	    // Allow only distinct values
//	    return (isBSTUtilLeft(node.left, min, node.data) &&
//	                isBSTUtilRight(node.right, node.data, max));
//    }
//	
//	public boolean isBSTUtilRight(BinaryTreeNode<T> node, T min, T max) {
//		/* an empty tree is BST */
//	    if (node == null)
//	    	return true;
//	 
//	    /* false if this node violates the min/max constraints */
//	    if (node.data.compareTo(min) < 0 || node.data.compareTo(max) >= 0)
//	    	return false;
//	 
//	    /* otherwise check the subtrees recursively
//	    tightening the min/max constraints */
//	    // Allow only distinct values
//	    return (isBSTUtilLeft(node.left, min, node.data) &&
//	                isBSTUtilRight(node.right, node.data, max));
//    }	
	
}
