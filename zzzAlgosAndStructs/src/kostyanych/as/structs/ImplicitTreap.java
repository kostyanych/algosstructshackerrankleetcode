package kostyanych.as.structs;

import java.util.Date;
import java.util.Queue;
import java.util.Random;

/*
 * Priorities are random
 * Value is real value
 * Sorting is made by "array index" - key. Which is implicit. It is calculated as the number of nodes which are BST less than current. 
 */
public class ImplicitTreap<T extends Comparable<T>> {
	
	static final Random rnd=new Random(new Date().getTime());

	TreapNode<T> root;
	
	public TreapNode<T> insert(T value, int key) {
		TreapNode<T> newNode = new TreapNode<T>(value, rnd.nextLong());
		TreapNode<T>[] pair = split(root, key);
		root=merge(merge(pair[0], newNode), pair[1]);
		return newNode;
	}
	
	public TreapNode<T> insertNextArrayLike(T value) {
		TreapNode<T> newNode = new TreapNode<T>(value, rnd.nextLong());
		root=merge(root, newNode);
		return newNode;
	}
	
	
	public Treap<T> split(int key) {
        Treap<T> res=new Treap<T>();
		if (root == null)
        	return res;
        
        TreapNode<T>[] pair=split(root,key);
        if (root.equals(pair[0]))
        	res.root=pair[1];
        else
        	res.root=pair[0];
        
        return res;
    }
	
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	public TreapNode<T>[] split(TreapNode<T> node, int key) {
        if (node == null) {
        	TreapNode[] empty = {null, null};
            return empty;
        }

        if (count(node.left) > key) {
        	TreapNode<T>[] result = split(node.left, key);
            node.left = result[1];
            update(node);
            TreapNode[] pair = {result[0], node};
            return pair;
        }
        else {
        	TreapNode<T>[] result = split(node.right, key - count(node.left) - 1);
            node.right = result[0];
            update(node);
            TreapNode[] pair = {node, result[1]};
            return pair;
        }
    }
	
	public TreapNode<T> merge(TreapNode<T> treapA, TreapNode<T> treapB) {
		if (treapA == null || treapB == null)
	        return treapA != null ? treapA : treapB;
		else if (treapA.priority > treapB.priority)  {
//		else if (treapA.val.compareTo(treapB.val)>0)  {
			treapA.right = merge(treapA.right, treapB);
	        update(treapA);
	        return treapA;
		}
		else {
			treapB.left = merge(treapA, treapB.left);
	        update(treapB);
	        return treapB;
		}
	}
	
	int count(TreapNode<T> node) {
        return node == null ? 0 : node.size;
    }

    void update(TreapNode<T> node) {
    	node.size = count(node.left) + count(node.right) + 1;
    }	

	public void levelOrderTraversalLineByLevel() {
		if (root==null)
			System.out.println("EMPTY");
		levelOrderTraversalLineByLevel(root);
	}
    
    void levelOrderTraversalLineByLevel(TreapNode<T> root) {
        Queue<TreapNode<T>> q=new java.util.LinkedList<TreapNode<T>>();
        q.add(root);
        
        int nodeCount=0;
        
        while (true) {
        	nodeCount=q.size();
        	if (nodeCount<1)
        		break;
        	while (nodeCount>0) {
        		root=q.remove();
        		nodeCount--;
        		System.out.print(root+" ");
                if (root.left!=null)
                    q.add(root.left);
                if (root.right!=null)
                    q.add(root.right);
        	}
        	System.out.println();
        }
    }	
}
