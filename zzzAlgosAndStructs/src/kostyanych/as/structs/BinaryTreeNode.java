package kostyanych.as.structs;

public class BinaryTreeNode<T> {
	
	public T data;
    public BinaryTreeNode<T> left;
    public BinaryTreeNode<T> right;
    public int height=1; //used for AVL trees
    public boolean isBlack=false; //used for red-black trees
    public BinaryTreeNode<T> parent; //used for red-black trees
    
    @Override
    public String toString() {
    	StringBuffer sb=new StringBuffer("[");
    	sb.append(data.toString()).append("\r\n");
    	sb.append(isBlack?"black":"red").append("]");
    	return sb.toString();
    }
    

}
