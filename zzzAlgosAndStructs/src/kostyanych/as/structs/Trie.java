package kostyanych.as.structs;

public class Trie {
	
	// Alphabet size (# of symbols) 
    static final int ALPHABET_SIZE = 26;

	static class TrieNode { 
        TrieNode[] children = new TrieNode[ALPHABET_SIZE]; 
        int existingChildren = 0;
       
        // isEndOfWord is true if the node represents 
        // end of a word 
        boolean isEndOfWord; 
          
        TrieNode(){ 
            isEndOfWord = false; 
            for (int i = 0; i < ALPHABET_SIZE; i++) 
                children[i] = null; 
        } 
    };

    TrieNode root;

    public Trie() {
    	root=new TrieNode();
    }
    
    static int charIndex(char c) {
    	return c - 'a';
    }

    static char charFromIndex(int index) {
    	return (char)(index + 'a');
    }
    
    // If not present, inserts key into trie 
    // If the key is prefix of trie node,  
    // just marks leaf node 
    public void insert(String key) { 
        int level; 
        int length = key.length(); 
        int index; 
       
        TrieNode pCrawl = root; 
       
        for (level = 0; level < length; level++) { 
            index = charIndex(key.charAt(level)); 
            if (pCrawl.children[index] == null) {
                pCrawl.children[index] = new TrieNode();
                pCrawl.existingChildren++;
            }
       
            pCrawl = pCrawl.children[index]; 
        } 
       
        // mark last node as leaf 
        pCrawl.isEndOfWord = true; 
    } 
       
    // Returns true if key presents in trie, else false 
    public boolean search(String key) { 
        int level; 
        int length = key.length(); 
        int index; 
        TrieNode pCrawl = root; 
       
        for (level = 0; level < length; level++) { 
            index = charIndex(key.charAt(level)); 
       
            if (pCrawl.children[index] == null) 
                return false; 
       
            pCrawl = pCrawl.children[index]; 
        } 
       
        return (pCrawl != null && pCrawl.isEndOfWord); 
    }
    
    public void delete(String key) { 
        int len = key.length(); 
      
        if( len > 0 ) 
            deleteHelper(root, key, 0, len); 
    }    

    boolean deleteHelper(TrieNode node, String key, int level, int len) { 
        if( node==null )
        	return false;

        // Base case 
        if( level == len ) {
        	if( node.isEndOfWord ) { 
        		// Unmark leaf node 
                node.isEndOfWord = false; 
      
                // If empty, node to be deleted 
                if( node.existingChildren==0 ) 
                	return true; 
                return false; 
        	} 
        } 
        else {// Recursive case 
        	int index = charIndex(key.charAt(level)); 
      
        	if( deleteHelper(node.children[index], key, level+1, len) ) { 
        		//last node marked, delete it
        		node.children[index]=null;
        		node.existingChildren--;
      
        		// recursively climb up, and delete eligible nodes 
                return ( !node.isEndOfWord && node.existingChildren==0);
        	} 
        } 
        return false; 
    }
    
    public void display() {
    	display(root, "", 0);
    }
    
 // function to display the content of Trie 
    private void display(TrieNode node, String prefix, int level) { 
        // If node is leaf node, it indiicates end 
        // of string, so a null charcter is added 
        // and string is displayed 
        if (node.isEndOfWord) 
            System.out.println(prefix); 
      
        for (int i = 0; i < ALPHABET_SIZE; i++) { 
            // if NON NULL child is found 
            // add parent key to str and 
            // call the display function recursively 
            // for child node 
            if (node.children[i]!=null) { 
                display(node.children[i], prefix+charFromIndex(i), level + 1); 
            } 
        } 
    }     
      
}
