package kostyanych.as.structs;

public class TreapNode<T extends Comparable<T>> {
	
	long priority;
	T val;
	int size=1;
	
	TreapNode<T> left;
	TreapNode<T> right;
	
	public TreapNode(){
	}

	public TreapNode(T value, long priority){
		this.val=value;
		this.priority=priority;
	}
	
	@Override
	public String toString() {
		return "[v="+val+", Pr="+priority+", sz="+size+"]";
	}
	
}
