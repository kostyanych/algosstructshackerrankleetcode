package kostyanych.as.structs;

import java.util.ArrayList;
import java.util.List;

public class BinaryHeap <T extends Comparable<T>> {
	
	List<T> heap=new ArrayList<T>();
	
	public void insert(T val){
        heap.add(val);
        recalcHeapUp(heap.size()-1);
    }
	
	public void remove(T v){
        int ind=-1;
        int size=heap.size();
        for (int i=0;i<size;i++){
            if (v.equals(heap.get(i))) {
                ind=i;
                break;
            }
        }
        if (ind==-1)
            return;
        if (ind==size-1) {
            heap.remove(size-1);
            return;
        }
        T last=heap.get(size-1);
        heap.set(ind,last);
        heap.remove(size-1);
        int parentIndex = getParentIndex(ind);
        T parentv=heap.get(parentIndex);
        if (parentv.compareTo(last)>0)
            recalcHeapUp(ind);
        else
            recalcHeapDown(ind);
    }	
	
	void recalcHeapUp(int ind){
        if (ind==0)
            return;

        int parentInd;
        T thisv;
        T parentv;
        
        thisv=heap.get(ind);
        parentInd = getParentIndex(ind);
        parentv=heap.get(parentInd);
        if (parentv.compareTo(thisv)>0) {
            heap.set(parentInd,thisv);
            heap.set(ind,parentv);
            recalcHeapUp(parentInd);
        }
    }
	
	private void recalcHeapDown(int ind){
        int size=heap.size();
        T thisval=heap.get(ind);

        int lchindex=getLeftChildIndex(ind);
        int rchindex=getRightChildIndex(ind);
        if (lchindex>size-1)
            return;
        int newind=lchindex;
        if (rchindex<size && heap.get(lchindex).compareTo(heap.get(rchindex))>0)
            newind=rchindex;
        
        T chval=heap.get(newind);
        if (chval.compareTo(thisval)<0) {
            heap.set(ind,chval);
            heap.set(newind,thisval);
            recalcHeapDown(newind);
        }
    }    
	
	
	int getLeftChildIndex(int ind) {
        return 2 * ind + 1;
    }
 
    int getRightChildIndex(int ind) {
        return 2 * ind + 2;
    }
 
    int getParentIndex(int ind) {
        return (ind - 1) / 2;
    }	

}
