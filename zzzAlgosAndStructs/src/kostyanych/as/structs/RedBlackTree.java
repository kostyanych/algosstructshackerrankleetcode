package kostyanych.as.structs;

public class RedBlackTree<T extends Comparable<T>> extends BinarySearchTree<T> {
	
	public static <T extends Comparable<T>> RedBlackTree<T> startTree(T data){
		RedBlackTree<T> tree=new RedBlackTree<T>();
		tree.insert(data);
		return tree;
	}
	
	public BinaryTreeNode<T> insert(T value) {
		
		BinaryTreeNode<T> ins=new BinaryTreeNode<T>();
        ins.data=value;
		
	    // Do a normal BST insert
       	root = bstInsertUtil(root, ins);
	 
	    // fix Red Black Tree violations
	    fixViolation(ins);
	    
	    return ins;
	}
	
	public BinaryTreeNode<T> bstInsertUtil(BinaryTreeNode<T> rt, BinaryTreeNode<T> ins) {
	    /* If the tree is empty, return a new node */
	    if (rt == null)
	       return ins;
	 
	    /* Otherwise, recur down the tree */
	    if (ins.data.compareTo(rt.data) < 0) {
	        rt.left = bstInsertUtil(rt.left, ins);
	        rt.left.parent = rt;
	    }
	    else if (ins.data.compareTo(rt.data) > 0) {
	        rt.right = bstInsertUtil(rt.right, ins);
	        rt.right.parent = rt;
	    }
	 
	    /* return the (unchanged) node pointer */
	    return rt;
	}	
	
	public void fixViolation(BinaryTreeNode<T> pt) {
		BinaryTreeNode<T> parent_pt = null;
		BinaryTreeNode<T> grand_parent_pt = null;
	 
	    while (pt != root && !pt.isBlack && !pt.parent.isBlack) {
	 
	        parent_pt = pt.parent;
	        grand_parent_pt = pt.parent.parent;
	 
	        /*  Case : A
	            Parent of pt is left child of Grand-parent of pt */
	        if (parent_pt == grand_parent_pt.left) {
	 
	        	BinaryTreeNode<T> uncle_pt = grand_parent_pt.right;
	 
	            /* Case : 1
	               The uncle of pt is also red
	               Only Recoloring required */
	            if (uncle_pt != null && !uncle_pt.isBlack) {
	                grand_parent_pt.isBlack=false;
	                parent_pt.isBlack = true;
	                uncle_pt.isBlack = true;
	                pt = grand_parent_pt;
	            }
	            else {
	                /* Case : 2
	                   pt is right child of its parent
	                   Left-rotation required */
	                if (pt == parent_pt.right) {
	                    rotateLeft(/*root, */parent_pt);
	                    pt = parent_pt;
	                    parent_pt = pt.parent;
	                }
	 
	                /* Case : 3
	                   pt is left child of its parent
	                   Right-rotation required */
	                rotateRight(/*root, */grand_parent_pt);
	                swapColors(parent_pt, grand_parent_pt);
	                pt = parent_pt;
	            }
	        }
	 
	        /* Case : B
	           Parent of pt is right child of Grand-parent of pt */
	        else {
	        	BinaryTreeNode<T> uncle_pt = grand_parent_pt.left;
	 
	            /*  Case : 1
	                The uncle of pt is also red
	                Only Recoloring required */
	            if ( uncle_pt != null && !uncle_pt.isBlack) {
	                grand_parent_pt.isBlack=false;
	                parent_pt.isBlack=true;
	                uncle_pt.isBlack=true;
	                pt = grand_parent_pt;
	            }
	            else {
	                /* Case : 2
	                   pt is left child of its parent
	                   Right-rotation required */
	                if (pt == parent_pt.left) {
	                    rotateRight(/*root, */parent_pt);
	                    pt = parent_pt;
	                    parent_pt = pt.parent;
	                }
	 
	                /* Case : 3
	                   pt is right child of its parent
	                   Left-rotation required */
	                rotateLeft(/*root, */grand_parent_pt);
	                swapColors(parent_pt, grand_parent_pt);
	                pt = parent_pt;
	            }
	        }
	    }
	 
	    root.isBlack=true;
	}	
	
	void rotateLeft(BinaryTreeNode<T> pt) {
		BinaryTreeNode<T> pt_right = pt.right;
	 
	    pt.right = pt_right.left;
	 
	    if (pt.right != null)
	        pt.right.parent = pt;
	 
	    pt_right.parent = pt.parent;
	 
	    if (pt.parent == null)
	        root = pt_right;
	 
	    else if (pt == pt.parent.left)
	        pt.parent.left = pt_right;
	 
	    else
	        pt.parent.right = pt_right;
	 
	    pt_right.left = pt;
	    pt.parent = pt_right;
	}
	
	void rotateRight(BinaryTreeNode<T> pt) {
		BinaryTreeNode<T> pt_left = pt.left;
	 
	    pt.left = pt_left.right;
	 
	    if (pt.left != null)
	        pt.left.parent = pt;
	 
	    pt_left.parent = pt.parent;
	 
	    if (pt.parent == null)
	        root = pt_left;
	 
	    else if (pt == pt.parent.left)
	        pt.parent.left = pt_left;
	 
	    else
	        pt.parent.right = pt_left;
	 
	    pt_left.right = pt;
	    pt.parent = pt_left;
	}
	
	void swapColors(BinaryTreeNode<T> n1, BinaryTreeNode<T> n2) {
		boolean buf=n1.isBlack;
		n1.isBlack=n2.isBlack;
		n2.isBlack=buf;
	}
	 

}
