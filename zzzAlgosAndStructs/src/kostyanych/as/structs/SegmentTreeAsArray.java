package kostyanych.as.structs;

/**
 * @author Kostya
 * The root of T will represent the whole array A[0:N-1].
 * Each leaf in the Segment Tree T will represent a single element A[i] such that 0<=i<N.
 * The internal nodes in the Segment Tree T represents the union of elementary intervals A[i:j] where 0<=i<j<N.
 * 
 * 
 * For each node at index i, the left child is at index 2*i+1, 
 * right child at 2*i+2 and the parent is at floor((i-1)/2)
 */
public class SegmentTreeAsArray {

    int[] tree; // The array that stores segment tree nodes
    int[] arr;
    int arrlen;
    
    /* Constructor to construct segment tree from given array. This 
       constructor  allocates memory for segment tree and calls 
       constructSTUtil() to  fill the allocated memory */
	public SegmentTreeAsArray(int[] _arr) {
		arr=_arr;
		arrlen=arr.length;
        // Allocate memory for segment tree 
        //Height of segment tree 
        int x = (int) (Math.ceil(Math.log(arrlen) / Math.log(2))); 
  
        //Maximum size of segment tree 
        int maxSize = 2 * (int) Math.pow(2, x) - 1; 
  
        tree = new int[maxSize]; // Memory allocation 
  
        constructSTUtil(0, arrlen - 1, 0); 
    } 

    // A recursive function that constructs Segment Tree for array[ss..se]. 
    // si is index of current node in segment tree st 
    private int constructSTUtil(int left, int right, int stIndex) { 
        // If there is one element in array, store it in current node of 
        // segment tree and return 
        if (left == right) { 
            tree[stIndex] = arr[left]; 
            return arr[left]; 
        } 
  
        // If there are more than one elements, then recur for left and 
        // right subtrees and store the sum of values in this node 
        int mid = getMid(left, right); 
        tree[stIndex] = constructSTUtil(left, mid, stIndex * 2 + 1) + 
                 constructSTUtil(mid + 1, right, stIndex * 2 + 2); 
        return tree[stIndex]; 
    } 
	
    // A utility function to get the middle index from corner indexes. 
	private int getMid(int left, int right) { 
        return left + (right - left) / 2;	//same as (left+right)/2 
    } 
	
    // Return sum of elements in range from index qs (quey start) to 
	// qe (query end).  It mainly uses getSumUtil() 
    int getSum(int left, int right) { 
        // Check for erroneous input values 
        if (left < 0 || right > arrlen - 1 || left > right) { 
            System.out.println("Invalid Input"); 
            return -1; 
        } 
        return getSumUtil(0, arrlen - 1, left, right, 0); 
    } 
	
    /*  A recursive function to get the sum of values in given range 
        of the array.  The following are parameters for this function. 
  
      tree    --> Pointer to segment tree 
      stIndex    --> Index of current node in the segment tree. Initially 
                0 is passed as root is always at index 0 
      segleft & segright  --> Starting and ending indexes of the segment represented 
                    by current node, i.e., tree[stIndex] 
      left & right  --> Starting and ending indexes of query range */
    int getSumUtil(int segleft, int segright, int left, int right, int stIndex) { 
        // If segment of this node is a part of given range, then return 
        // the sum of the segment 
        if (left <= segleft && right >= segright) 
            return tree[stIndex]; 
  
        // If segment of this node is outside the given range 
        if (segright < left || segleft > right) 
            return 0; 
  
        // If a part of this segment overlaps with the given range 
        int mid = getMid(segleft, segright); 
        return getSumUtil(segleft, mid, left, right, 2 * stIndex + 1) + 
                getSumUtil(mid + 1, segright, left, right, 2 * stIndex + 2); 
    } 
  
    /* A recursive function to update the nodes which have the given  
       index in their range. The following are parameters 
        tree, stIndexi, segleft and segright are same as getSumUtil() 
        i    --> index of the element to be updated. This index is in 
                 input array. 
       diff --> Value to be added to all nodes which have i in range */
    void updateValueUtil(int segleft, int segright, int i, int diff, int stIndex) { 
        // Base Case: If the input index lies outside the range of  
        // this segment 
        if (i < segleft || i > segright) 
            return; 
  
        // If the input index is in range of this node, then update the 
        // value of the node and its children 
        tree[stIndex] = tree[stIndex] + diff; 
        if (segright != segleft) { 
            int mid = getMid(segleft, segright); 
            updateValueUtil(segleft, mid, i, diff, 2 * stIndex + 1); 
            updateValueUtil(mid + 1, segright, i, diff, 2 * stIndex + 2); 
        } 
    } 
  
    // The function to update a value in input array and segment tree. 
	// It uses updateValueUtil() to update the value in segment tree 
    private void updateValue(int i, int newVal) {
    	int len=arr.length;
        // Check for erroneous input index 
        if (i < 0 || i > len - 1) { 
            System.out.println("Invalid Input"); 
            return; 
        } 
  
        // Get the difference between new value and old value 
        int diff = newVal - arr[i]; 
  
        // Update the value in array 
        arr[i] = newVal; 
  
        // Update the values of nodes in segment tree 
        updateValueUtil(0, arrlen - 1, i, diff, 0); 
    } 
  
  
  
    // Driver program to test above functions 
    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
        int[] arr = {1, 3, 5, 7, 9, 11}; 
        int n = arr.length; 
        // Build segment tree from given array
        SegmentTreeAsArray  tree = new SegmentTreeAsArray(arr); 
  
        tree.getSum(0, 1);
  
        // Print sum of values in array from index 1 to 3 
        System.out.println("Sum of values in given range = " + tree.getSum(1, 3)); 
  
        // Update: set arr[1] = 10 and update corresponding segment 
        // tree nodes 
        tree.updateValue(1, 10); 
  
        // Find sum after the value is updated 
        System.out.println("Updated sum of values in given range = " + tree.getSum(1, 3)); 
    } 	
}
