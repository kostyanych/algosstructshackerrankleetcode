package kostyanych.as.structs;

import java.util.Comparator;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree<T> {
	
	public BinaryTreeNode<T> root=null;
	
	public static <T> BinaryTreeNode<T> generareNode(T data){
		BinaryTreeNode<T> node=new BinaryTreeNode<T>();
		node.data=data;
		return node;
	}

	public static <T> BinaryTree<T> startTree(T data){
		BinaryTreeNode<T> node=new BinaryTreeNode<T>();
		node.data=data;
		BinaryTree<T> tree=new BinaryTree<T>();
		tree.root=node;
		return tree;
	}
	
	/**
	 * Check if the current node is empty / null.
	 * Display the data part of the root (or current node).
     * Traverse the left subtree by recursively calling the pre-order function.
     * Traverse the right subtree by recursively calling the pre-order function.
	 */
	public void preOrderTraversal() {
		if (root!=null) {
			preOrderTraversal(root);
			System.out.println();
		}
	}

	/**
	 * Check if the current node is empty / null.
	 * Traverse the left subtree by recursively calling the post-order function.
	 * Traverse the right subtree by recursively calling the post-order function.
	 * Display the data part of the root (or current node).
	 */
	public void postOrderTraversal() {
		if (root!=null) {
			postOrderTraversal(root);
			System.out.println();
		}
	}

	/**
	 * Check if the current node is empty / null.
	 * Traverse the left subtree by recursively calling the in-order function.
	 * Display the data part of the root (or current node).
	 * Traverse the right subtree by recursively calling the in-order function.
	 */
	public void inOrderTraversal() {
		if (root!=null) {
			inOrderTraversal(root);
			System.out.println();
		}
	}

	/**
	 *  In level order traversal, we visit the nodes level by level from left to right. 
	 */
	public void levelOrderTraversal() {
		if (root!=null) {
			levelOrderTraversal(root);
			System.out.println();
		}
	}

	/**
	 *  In level order traversal, we visit the nodes level by level from left to right.
	 *  Levels are printed from farther to the root 
	 */
	public void reverseLevelOrderTraversal() {
		if (root!=null) {
			reverseLevelOrderTraversal(root);
			System.out.println();
		}
	}
	
	/**
	 *  Same as simple level order traversal, but each level is printed on its own line. 
	 */
	public void levelOrderTraversalLineByLevel() {
		if (root!=null) {
			levelOrderTraversalLineByLevel(root);
		}
	}

	/**
	 *  Prints leaves from left to right 
	 */
	public void printLeaves() {
		if (root!=null) {
			printLeaves(root);
			System.out.println();
		}
	}
	
	// A function to do boundary traversal of a given binary tree
	public void printBoundary() {
		if (root != null) {
			System.out.print(root.data + " ");

			// Print the left boundary in top-down manner.
			printBoundaryLeftDown(root.left);

			// Print all leaf nodes
//			printLeaves(root.left);
//			printLeaves(root.right);
			printLeaves(root);

			// Print the right boundary in bottom-up manner
			printBoundaryRightUp(root.right);
		}
		System.out.println();
	}	
	
	public void preOrderTraversal(BinaryTreeNode<T> node) {
	    System.out.print(""+node.data+" ");
	    if (node.left!=null)
	    	preOrderTraversal(node.left);
	    if (node.right!=null)
	    	preOrderTraversal(node.right);
	}

	public void postOrderTraversal(BinaryTreeNode<T> node) {
	    if (node.left!=null)
	    	postOrderTraversal(node.left);
	    if (node.right!=null)
	    	postOrderTraversal(node.right);
	    System.out.print(""+node.data+" ");
	}

	public void inOrderTraversal(BinaryTreeNode<T> node) {
	    if (node.left!=null)
	    	inOrderTraversal(node.left);
	    System.out.print(""+node.data+" ");
	    if (node.right!=null)
	    	inOrderTraversal(node.right);
	}
	
	void levelOrderTraversal(BinaryTreeNode<T> root) {
        Queue<BinaryTreeNode<T>> q=new java.util.LinkedList<BinaryTreeNode<T>>();
        q.add(root);
        while (!q.isEmpty()){
            root=q.remove();
            System.out.print(root.data+" ");
            if (root.left!=null)
                q.add(root.left);
            if (root.right!=null)
                q.add(root.right);
        }
    }	

	void reverseLevelOrderTraversal(BinaryTreeNode<T> root) {
		Stack<BinaryTreeNode<T>> st=new Stack<BinaryTreeNode<T>>();
        Queue<BinaryTreeNode<T>> q=new java.util.LinkedList<BinaryTreeNode<T>>();
        q.add(root);
        while (!q.isEmpty()){
            root=q.remove();
            st.push(root);
            if (root.left!=null)
                q.add(root.left);
            if (root.right!=null)
                q.add(root.right);
        }
        
        while (!st.isEmpty()) {
        	System.out.print(st.pop().data+" ");
        }
    }	
	
	void levelOrderTraversalLineByLevel(BinaryTreeNode<T> root) {
        Queue<BinaryTreeNode<T>> q=new java.util.LinkedList<BinaryTreeNode<T>>();
        q.add(root);
        
        int nodeCount=0;
        
        while (true) {
        	nodeCount=q.size();
        	if (nodeCount<1)
        		break;
        	while (nodeCount>0) {
        		root=q.remove();
        		nodeCount--;
        		System.out.print(root.data+" ");
                if (root.left!=null)
                    q.add(root.left);
                if (root.right!=null)
                    q.add(root.right);
        	}
        	System.out.println();
        }
    }	
	
	public int height(){
		return height(root);
	}
	public int height(BinaryTreeNode<T> root) {
    	if (root.left==null && root.right==null)
            return 0;

        int h=1;
        int hl=0;
        int hr=0;
        if (root.left!=null)
            hl=height(root.left);
        if (root.right!=null)
            hr=height(root.right);
        h+=Math.max(hl,hr);
        return h;
    }

	void printLeaves(BinaryTreeNode<T> node) {
		if (node != null) {
			printLeaves(node.left);
            // Print it if it is a leaf node
            if (node.left == null && node.right == null)
            	System.out.print(node.data + " ");
            printLeaves(node.right);
		}
	}
	 
	void printBoundaryLeftDown(BinaryTreeNode<T> node) {
		if (node != null) {
			if (node.left != null) {
			// to ensure top down order, print the node
	        // before calling itself for left subtree
				System.out.print(node.data + " ");
	            printBoundaryLeftDown(node.left);
			} 
			else if (node.right != null) {
				System.out.print(node.data + " ");
	            printBoundaryLeftDown(node.right);
			}
			// do nothing if it is a leaf node, this way we avoid
	        // duplicates in output
		}
	}
	  
	// A function to print all right boundry nodes, except a leaf node
	// Print the nodes in BOTTOM UP manner
	void printBoundaryRightUp(BinaryTreeNode<T> node) {
		if (node != null) {
			if (node.right != null) {
				// to ensure bottom up order, first call for right
				//  subtree, then print this node
				printBoundaryRightUp(node.right);
				System.out.print(node.data + " ");
			} 
			else if (node.left != null) {
				printBoundaryRightUp(node.left);
				System.out.print(node.data + " ");
			}
			// do nothing if it is a leaf node, this way we avoid
			// duplicates in output
		}
	}	 
	
	/**
	 * This is not a proper top view. It hides extremities that originate from inner nodes.
	 * But hackerrank defined this so for their test
	 */
	public void hackerRankTopView() {
 		if (root==null)
 			return;
 		BinaryTree.printLeftNodes(root.left);
 		System.out.print(root.data+" ");
 		BinaryTree.printRightNodes(root.right);
 		System.out.println();
	}
 
	public static <T> void printLeftNodes(BinaryTreeNode<T> node) {
		if (node==null)
			return;
		if (node.left!=null)
    		printLeftNodes(node.left);
		System.out.print(node.data+" ");
	}

	public static <T> void printRightNodes(BinaryTreeNode<T> node) {
		if (node==null)
			return;
	    System.out.print(node.data+" ");
	    if (node.right!=null)
	    	printRightNodes(node.right);
	}
	
	static <T> int findIndInArray(T[] arr, T elem){
		return BinaryTree.findIndInArray(arr, elem, 0, arr.length-1);
	}
	
	static <T> int findIndInArray(T[] arr, T elem, int start, int end){
		int i;
        for (i = start; i <= end; i++) {
            if (arr[i].equals(elem))
                return i;
        }
        return i;		
	}
	
	public int countNodes(){
		return BinaryTree.countNodes(root,0);
	}
	
	public static <T> int countNodes(BinaryTreeNode<T> root) {
		return BinaryTree.countNodes(root,0);
	}
	
	public static <T> int countNodes(BinaryTreeNode<T> root, int curCount) {
		curCount++;
		if (root.left!=null)
			curCount=BinaryTree.countNodes(root.left,curCount);
		if (root.right!=null)
			curCount=BinaryTree.countNodes(root.right,curCount);
		return curCount;
	}
    
/*	
	public static <T extends Comparable<T>> boolean isBST(Tree<T> tree) {
        if (tree.root==null)
        	return true;
        return Tree.checkNode(tree.root,tree.root.data,tree.root.data, true);
	}

	private static <T extends Comparable<T>> boolean checkNode(TreeNode<T> node, T min, T max, boolean dontMindEquality) {
		if (node==null)
			return true;
		if (node.data.compareTo(max)>0 || node.data.compareTo(min)<0)
			return false;
		if (!dontMindEquality) {
			if (node.data.equals(max) || node.data.equals(min))
				return false;
		}
		
		return Tree.checkNode(node.right,min,node.data,false) 
	            && Tree.checkNode(node.left,node.data,max,false);
	}
*/
	
	public BinaryTreeNode<T> findMinValueNode(Comparator<T> comparator) {
		if (root==null)
			return null;
		
		BinaryTreeNode<T> node=root;
		BinaryTreeNode<T> res=node;
		
		Queue<BinaryTreeNode<T>> q=new java.util.LinkedList<BinaryTreeNode<T>>();
	    q.add(node);
	    while (!q.isEmpty()){
	    	node=q.remove();
	    	
	    	if (comparator.compare(node.data, res.data)<0)
	    		res=node;
	    	
	        if (node.left!=null)
	        	q.add(node.left);
	        if (node.right!=null)
	        	q.add(node.right);
	    }		
        return res;
    }

	public BinaryTreeNode<T> findMaxValueNode(Comparator<T> comparator) {
		if (root==null)
			return null;
		
		BinaryTreeNode<T> node=root;
		BinaryTreeNode<T> res=node;
		
		Queue<BinaryTreeNode<T>> q=new java.util.LinkedList<BinaryTreeNode<T>>();
	    q.add(node);
	    while (!q.isEmpty()){
	    	node=q.remove();
	    	
	    	if (comparator.compare(node.data, res.data)>0)
	    		res=node;
	    	
	        if (node.left!=null)
	        	q.add(node.left);
	        if (node.right!=null)
	        	q.add(node.right);
	    }		
        return res;
    }
	
	public boolean isBST(Comparator<T> comparator)  {
		if (root==null)
			return true;
		BinaryTreeNode<T> min=findMinValueNode(comparator);
		BinaryTreeNode<T> max=findMaxValueNode(comparator);
		return (isBSTUtilLeft(root.left, min, root, comparator) &&
                isBSTUtilRight(root.right, root, max, comparator));
	}
	 
    /* Returns true if the given tree is a BST and its
      values are >= min and <= max. */
	public boolean isBSTUtilLeft(BinaryTreeNode<T> node, BinaryTreeNode<T> min, BinaryTreeNode<T> max, Comparator<T> comparator) {
		/* an empty tree is BST */
	    if (node == null)
	    	return true;
	 
	    /* false if this node violates the min/max constraints */
	    if (comparator.compare(node.data,max.data) > 0)
	    	return false;
	    if (node!=min && comparator.compare(node.data,min.data) <= 0)
	    	return false;
	 
	    /* otherwise check the subtrees recursively
	    tightening the min/max constraints */
	    // Allow only distinct values
	    return (isBSTUtilLeft(node.left, min, node, comparator) &&
	                isBSTUtilRight(node.right, node, max, comparator));
    }
	
	public boolean isBSTUtilRight(BinaryTreeNode<T> node, BinaryTreeNode<T> min, BinaryTreeNode<T> max, Comparator<T> comparator) {
		/* an empty tree is BST */
	    if (node == null)
	    	return true;
	 
	    /* false if this node violates the min/max constraints */
	    if (comparator.compare(node.data,min.data) < 0)
	    	return false;
	    if (node!=max && comparator.compare(node.data,max.data) >= 0)
	    	return false;
	 
	    /* otherwise check the subtrees recursively
	    tightening the min/max constraints */
	    // Allow only distinct values
	    return (isBSTUtilLeft(node.left, min, node, comparator) &&
	                isBSTUtilRight(node.right, node, max, comparator));
    }	
}
