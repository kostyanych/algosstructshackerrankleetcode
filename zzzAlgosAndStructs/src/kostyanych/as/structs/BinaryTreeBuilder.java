package kostyanych.as.structs;

public class BinaryTreeBuilder {

	// This function mainly initializes index of root
	// and calls buildUtil()
	public static <T> BinaryTree<T> buildTreeFromInPostOrder(T inOrder[], T postOrder[]) {
		int n = inOrder.length;
		BinaryTreeNode<T> root = BinaryTreeBuilder.buildTreeIPost(inOrder, postOrder, 0, n - 1, n - 1);
		BinaryTree<T> tree = new BinaryTree<T>();
		tree.root = root;
		return tree;
	}

	/*
	 * Recursive function to construct binary of size n from Inorder traversal
	 * in[] and Preorder traversal post[]. Initial values of inStrt and inEnd
	 * should be 0 and n -1. The function doesn't do any error checking for
	 * cases where inorder and postorder do not form a tree
	 */
	public static <T> BinaryTreeNode<T> buildTreeIPost(T inOrder[], T postOrder[], int inStrt, int inEnd,
			int postIndex) {
		// Base case
		if (inStrt > inEnd)
			return null;

		/*
		 * Pick current node from Preorder traversal using postIndex and
		 * decrement postIndex
		 */
		BinaryTreeNode<T> node = BinaryTree.generareNode(postOrder[postIndex]);
		postIndex--;

		/* If this node has no children then return */
		if (inStrt == inEnd)
			return node;

		/*
		 * Else find the index of this node in Inorder traversal
		 */
		int inIndex = BinaryTree.findIndInArray(inOrder, node.data, inStrt, inEnd);

		/*
		 * Using index in Inorder traversal, construct left and right subtress
		 */
		node.right = buildTreeIPost(inOrder, postOrder, inIndex + 1, inEnd, postIndex);
		node.left = buildTreeIPost(inOrder, postOrder, inStrt, inIndex - 1,
				postIndex - BinaryTree.countNodes(node) + 1);
		return node;
	}

	/*
	 * Inorder traversal in[] and Preorder traversal pre[]. Initial values of
	 * inStrt and inEnd should be 0 and len -1. The function doesn't do any
	 * error checking for cases where inorder and preorder do not form a tree
	 */
	public static <T> BinaryTreeNode<T> buildTreeIPre(T[] in, T[] pre, int inStrt, int inEnd, int preIndex) {
		if (inStrt > inEnd)
			return null;

		/*
		 * Pick current node from Preorder traversal using preIndex and
		 * increment preIndex
		 */
		BinaryTreeNode<T> node = BinaryTree.generareNode(pre[preIndex]);
		preIndex++;

		/* If this node has no children then return */
		if (inStrt == inEnd)
			return node;

		/* Else find the index of this node in Inorder traversal */
		int inIndex = BinaryTree.findIndInArray(in, node.data, inStrt, inEnd);

		/*
		 * Using index in Inorder traversal, construct left and right subtress
		 */
		node.left = buildTreeIPre(in, pre, inStrt, inIndex - 1, preIndex);
		node.right = buildTreeIPre(in, pre, inIndex + 1, inEnd, preIndex + BinaryTree.countNodes(node) - 1);

		return node;
	}

	public static <T> BinaryTree<T> buildTreeFromInPreOrder(T[] inOrder, T[] preOrder) {
		BinaryTreeNode<T> root = BinaryTreeBuilder.buildTreeIPre(inOrder, preOrder, 0, inOrder.length - 1, 0);
		BinaryTree<T> tree = new BinaryTree<T>();
		tree.root = root;
		return tree;
	}

	public static <T> BinaryTree<T> buildTreeFromInLevelOrder(T[] inOrder, T[] levelOrder) {
		BinaryTreeNode<T> root = BinaryTreeBuilder.buildTreeIL(null, inOrder, levelOrder, 0, inOrder.length - 1);
		BinaryTree<T> tree = new BinaryTree<T>();
		tree.root = root;
		return tree;
	}

	static <T> BinaryTreeNode<T> buildTreeIL(BinaryTreeNode<T> startNode, T[] inOrder, T[] levelOrder, int inStart,
			int inEnd) {
		// if start index is more than end index
		if (inStart > inEnd)
			return null;

		if (inStart == inEnd)
			return BinaryTree.generareNode(inOrder[inStart]);

		boolean found = false;
		int index = 0;

		// int index represents the index in inOrder array of element that
		// appear first in levelOrder array.
		for (int i = 0; i < levelOrder.length - 1; i++) {
			T data = levelOrder[i];
			for (int j = inStart; j < inEnd; j++) {
				if (data.equals(inOrder[j])) {
					startNode = BinaryTree.generareNode(data);
					index = j;
					found = true;
					break;
				}
			}
			if (found)
				break;
		}

		// elements present before index are part of left child of startNode.
		// elements present after index are part of right child of startNode.
		startNode.left = BinaryTreeBuilder.buildTreeIL(startNode, inOrder, levelOrder, inStart, index - 1);
		startNode.right = BinaryTreeBuilder.buildTreeIL(startNode, inOrder, levelOrder, index + 1, inEnd);
		return startNode;
	}
}
