package kostyanych.as.structs;

public class AVLTree<T extends Comparable<T>> extends BinarySearchTree<T> {
	
	public static <T extends Comparable<T>> AVLTree<T> startTree(T data){
		BinaryTreeNode<T> node=new BinaryTreeNode<T>();
		node.data=data;
		AVLTree<T> tree=new AVLTree<T>();
		tree.root=node;
		return tree;
	}
	
	public BinaryTreeNode<T> insert(T data){
		return insertUtil(root,data);
	}
	
	public BinaryTreeNode<T> insertUtil(BinaryTreeNode<T> rt, T data) {
	
        /* 1.  Perform the normal BST insertion */
        if (rt == null) {
        	BinaryTreeNode<T> node=new BinaryTreeNode<T>();
    		node.data=data;
    		return node;
        }
 
        if (data.compareTo(rt.data) < 0)
            rt.left = insertUtil(rt.left, data);
        else if (data.compareTo(rt.data) > 0)
            rt.right = insertUtil(rt.right, data);
        else // Duplicate keys not allowed
            return rt;
 
        /* 2. Update height of this ancestor node */
        rt.height = 1 + Math.max(height(rt.left), height(rt.right));
 
        /* 3. Get the balance factor of this ancestor
              node to check whether this node became
              unbalanced */
        int balance = getBalance(rt);
 
        // If this node becomes unbalanced, then there
        // are 4 cases Left Left Case
        if (balance > 1 && data.compareTo(rt.left.data) < 0)
            return rightRotate(rt);
 
        // Right Right Case
        if (balance < -1 && data.compareTo(rt.right.data) > 0)
            return leftRotate(rt);
 
        // Left Right Case
        if (balance > 1 && data.compareTo(rt.left.data) > 0) {
            rt.left = leftRotate(rt.left);
            return rightRotate(rt);
        }
 
        // Right Left Case
        if (balance < -1 && data.compareTo(rt.right.data) < 0) {
            rt.right = rightRotate(rt.right);
            return leftRotate(rt);
        }
 
        /* return the (unchanged) node pointer */
        return rt;
    }
	
	private BinaryTreeNode<T> rightRotate(BinaryTreeNode<T> y) {
		boolean updateRoot=false;
		if (y==this.root)
			updateRoot=true;
		BinaryTreeNode<T> x = y.left;
		BinaryTreeNode<T> T2 = x.right;
 
        // Perform rotation
        x.right = y;
        y.left = T2;
 
        // Update heights
        y.height = Math.max(height(y.left), height(y.right)) + 1;
        x.height = Math.max(height(x.left), height(x.right)) + 1;

        if (updateRoot)
        	this.root=x;
        // Return new root
        return x;
    }
	
	// A utility function to left rotate subtree rooted with x
    // See the diagram given above.
	private BinaryTreeNode<T> leftRotate(BinaryTreeNode<T> x) {
		boolean updateRoot=false;
		if (x==this.root)
			updateRoot=true;
		
		BinaryTreeNode<T> y = x.right;
		BinaryTreeNode<T> T2 = y.left;
 
        // Perform rotation
        y.left = x;
        x.right = T2;
 
        //  Update heights
        x.height = Math.max(height(x.left), height(x.right)) + 1;
        y.height = Math.max(height(y.left), height(y.right)) + 1;
 
        if (updateRoot)
        	this.root=y;
        // Return new root
        return y;
    }
	
	 // Get Balance factor of node N
    int getBalance(BinaryTreeNode<T> N) {
        if (N == null)
            return 0;
 
        return height(N.left) - height(N.right);
    }	

    public int height(BinaryTreeNode<T> N) {
        if (N == null)
            return 0;
 
        return N.height;
    }

	public void deleteNode(T dt) {
		root = deleteNode(root, dt);
	}
    
    public BinaryTreeNode<T> deleteNode(BinaryTreeNode<T> rt, T dt) {
    	// STEP 1: PERFORM STANDARD BST DELETE
        if (rt == null)
            return rt;
 
        // If the key to be deleted is smaller than
        // the root's key, then it lies in left subtree
        if (dt.compareTo(rt.data)<0)
            rt.left = deleteNode(rt.left, dt);
 
        // If the key to be deleted is greater than the
        // root's key, then it lies in right subtree
        else if (dt.compareTo(rt.data)>0)
            rt.right = deleteNode(rt.right, dt);
 
        // if key is same as root's key, then this is the node
        // to be deleted
        else {
            // node with only one child or no child
            if ((rt.left == null) || (rt.right == null)) {
            	BinaryTreeNode<T> temp = null;
                if (temp == rt.left)
                    temp = rt.right;
                else
                    temp = rt.left;
 
                // No child case
                if (temp == null) {
                    temp = rt;
                    rt = null;
                }
                else   // One child case
                    rt = temp; // Copy the contents of
                                 // the non-empty child
            }
            else {
                // node with two children: Get the inorder
                // successor (smallest in the right subtree)
            	BinaryTreeNode<T> temp = findMinValueNode(rt.right);
 
                // Copy the inorder successor's data to this node
                rt.data = temp.data;
 
                // Delete the inorder successor
                rt.right = deleteNode(rt.right, temp.data);
            }
        }
 
        // If the tree had only one node then return
        if (rt == null)
            return rt;
 
        // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
        rt.height = Math.max(height(rt.left), height(rt.right)) + 1;
 
        // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
        //  this node became unbalanced)
        int balance = getBalance(rt);
 
        // If this node becomes unbalanced, then there are 4 cases
        // Left Left Case
        if (balance > 1 && getBalance(rt.left) >= 0)
            return rightRotate(rt);
 
        // Left Right Case
        if (balance > 1 && getBalance(rt.left) < 0) {
            rt.left = leftRotate(rt.left);
            return rightRotate(rt);
        }
 
        // Right Right Case
        if (balance < -1 && getBalance(rt.right) <= 0)
            return leftRotate(rt);
 
        // Right Left Case
        if (balance < -1 && getBalance(rt.right) > 0) {
            rt.right = rightRotate(rt.right);
            return leftRotate(rt);
        }
 
        return rt;
    }    
    
}
