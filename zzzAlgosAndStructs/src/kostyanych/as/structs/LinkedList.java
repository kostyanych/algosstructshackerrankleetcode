package kostyanych.as.structs;

public class LinkedList<T> {
	LinkedList<T> head=null;
	T data = null;
	public LinkedList<T> next=null;
	
	public static <TT> LinkedList<TT> generateNode(TT data) {
		LinkedList<TT> newnode=new LinkedList<TT>();
		newnode.data=data;
		return newnode;
	}

	public static <TT> LinkedList<TT> generateNewList(TT data) {
		LinkedList<TT> newnode=new LinkedList<TT>();
		newnode.data=data;
		newnode.head=newnode;
		return newnode;
	}
	
	public LinkedList<T> insertAtHead(T data){
		LinkedList<T> newhead=LinkedList.generateNode(data);
		newhead.head=newhead;
		newhead.next=head;
		LinkedList<T> buf=head;
		while (buf!=null) {
			buf.head=newhead;
			buf=buf.next;
		}
		return newhead;
	}
	
	public LinkedList<T> insertAfterThis(T data){
		LinkedList<T> newnode=LinkedList.generateNode(data);
		newnode.next=this.next;
		newnode.head=this.head;
		this.next=newnode;
		return newnode;
	}

	public LinkedList<T> insertAtTail(T data){
		LinkedList<T> buf=head;
		while (buf.next!=null) {
			buf=buf.next;
		}
		LinkedList<T> newnode=LinkedList.generateNode(data);
		buf.next=newnode;
		newnode.head=buf.head;
		return newnode;
	}
	
	public LinkedList<T> insertNth(T data, int position) {
		if (position<0)
			return insertAtHead(data);
		LinkedList<T> buf=head;
		for (int i=1;i<position;i++) {
			buf=buf.next;
			if (buf.next==null)
				break;
		}
		LinkedList<T> newnode=LinkedList.generateNode(data);
		newnode.head=buf.head;
		newnode.next=buf.next;
		buf.next=newnode;
		return newnode;
	}

	public LinkedList<T> getNth(int position) {
		if (position<0)
			return null;
		LinkedList<T> buf=head;
		for (int i=0;i<position;i++) {
			buf=buf.next;
			if (buf.next==null)
				break;
		}
		return buf;
	}

	public LinkedList<T> deleteHead() {
		LinkedList<T> buf=head;
		LinkedList<T> res=head;
		LinkedList<T> newhead=buf.next;
		while (buf.next!=null) {
			buf.next.head=newhead;
			buf=buf.next;
		}
		return LinkedList.stripDeletedNode(res);
	}

	public LinkedList<T> deleteTail() {
		LinkedList<T> buf=head;
		LinkedList<T> res=head.next;
		if (res==null)
			return deleteHead();
		while(res.next!=null) {
			buf=buf.next;
			res=res.next;
		}
		buf.next=null;
		return LinkedList.stripDeletedNode(res);
	}
	
	public LinkedList<T> deleteNth(int position) {
		if (position<0)
			return null;
		if (position==0)
			return deleteHead();
		LinkedList<T> buf=head;
		for (int i=1;i<position;i++) {
			buf=buf.next;
			if (buf.next==null)
				return null;
		}
		LinkedList<T> res=buf.next;
		buf.next=res.next;
		return LinkedList.stripDeletedNode(res);
	}
	
	private static <TT> LinkedList<TT> stripDeletedNode(LinkedList<TT> node) {
		node.head=node;
		node.next=null;
		return node;
	}
	
	public void print() {
		System.out.println(data);
	}
	
	public void printWholeList(String separator) {
		LinkedList<T> buf=head;
		boolean needSeparator=false;
		while (buf!=null) {
			if (needSeparator)
				System.out.print(separator);
			System.out.print(buf.data);
			buf=buf.next;
			needSeparator=true;
		}
		System.out.println();
	}
	
	public void printWholeList() {
		printWholeList(", ");
	}
	
	public void printWholeListReverse() {
		printWholeListReverse(", ");
	}

	public void printWholeListReverse(String separator) {
		printWholeListReverse(head, separator);
		System.out.println();
	}
	
	private void printWholeListReverse(LinkedList<T> node, String separator) {
		  
	    if (node==null)
	        return;
	    
	    if (node.next==null) {
	        System.out.print(node.data);
		    if (!node.isHead())
		    	System.out.print(separator);
	    }
	    else {
	    	printWholeListReverse(node.next, separator);
	        System.out.print(node.data);
		    if (!node.isHead())
		    	System.out.print(separator);
	    } 
	}
	
	public boolean isHead() {
		if (this==head)
			return true;
		return false;
	}
	
	public LinkedList<T> reverseAndReturnHead() {
		LinkedList<T> buf=head;
		if (buf.next==null)
			return buf;
		buf=reverseFromHead(buf);
		LinkedList<T> h=buf;
		while (buf!=null){
			buf.head=h;
			buf=buf.next;
		}
		return h;
	}

	private LinkedList<T> reverseFromHead(LinkedList<T> head) {
		if (head==null || head.next==null)
	        return head;
	    
		LinkedList<T> nxt=reverseFromHead(head.next);
	    head.next.next=head;
	    head.next=null;
	    return nxt;
	}
	
	public boolean hasCycle(LinkedList<T> node) {
		if (node==null)
			return false;
		node=node.head;
		LinkedList<T> fast = node;
	    
	    while(fast != null && fast.next != null) {
	        fast = fast.next.next;
	        node = node.next;
	        
	        if(node.equals(fast))
	            return true;
	    }
	    return false;
	}
	
	public static <T> boolean compareLists(LinkedList<T> headA, LinkedList<T> headB) {
	    
	    if (headA==null && headB==null)
	        return true;
	    if (headA==null || headB==null)
	        return false;
	    headA=headA.head;
	    headB=headB.head;
	    while(headA!=null && headB!=null){
	        if (!headA.data.equals(headB.data))
	            return false;
	        headA=headA.next;
	        headB=headB.next;
	    }
	    
	    if (headA==null && headB==null)
	        return true;
	    return false;
	}
	
	/**
	 * Finds the node where two list merge (up to this point they are distinct lists, 
	 * 			and from that point on they have same nodes in the same sequence)
	 * We need to put the pointer in the longer list so that the remaining length would be equal to 
	 * 			the length of the shorter list.
	 * If there is a merge, then this pointer is sure to be at most that same point, or even before that point.
	 * So the number of steps before the merge point in both lists would be equal.
	 * And we only need to check the equality of nodes in both lists on every step.
	 * @param nodeA
	 * @param nodeB
	 * @return LinkedList<TT> merge node or null if no merge found
	 */
	public static <TT> LinkedList<TT> findMergeNode(LinkedList<TT> headA, LinkedList<TT> headB) {
		int la=LinkedList.getLength(headA);
	    int lb=LinkedList.getLength(headB);
	    if (la>lb) {
	        for(int i=0;i<(la-lb);i++){
	            headA=headA.next;
	        }
	    }
	    else {
	        for(int i=0;i<(lb-la);i++){
	            headB=headB.next;
	        }
	    }
	    
	    while(headA!=null && headB!=null) {
	    	if (headA==headB)
	    		return headA;
	        headA=headA.next;
	        headB=headB.next;
	    }
	    
	    return null;
	}
	
	public static <TT> int getLength(LinkedList<TT> node) {
		LinkedList<TT> headA=node.head;
		int cnt=1;
	    while(headA.next!=null) {
	        cnt++;
	        headA=headA.next;
	    }
	    return cnt;
	}
	
}
