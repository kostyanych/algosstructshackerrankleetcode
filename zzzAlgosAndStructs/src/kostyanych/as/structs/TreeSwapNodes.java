package kostyanych.as.structs;

import java.util.LinkedList;
import java.util.Queue;

public class TreeSwapNodes {

	public static <T> void swapNodes(BinaryTree<T> tree, int level){
        Queue<BinaryTreeNode<T>> q=new LinkedList<BinaryTreeNode<T>>();
        putNeededNodesInQ(q,tree.root,level,1);
        while(!q.isEmpty()) {
        	BinaryTreeNode<T> cur=q.remove();
        	BinaryTreeNode<T> buf=cur.left;
            cur.left=cur.right;
            cur.right=buf;
        }
    }
	
	static <T> void putNeededNodesInQ(Queue<BinaryTreeNode<T>> q, BinaryTreeNode<T> node, int neededlevel, int curlevel){
		if (curlevel%neededlevel==0)
            q.add(node);
        if (node.left!=null)
            putNeededNodesInQ(q, node.left, neededlevel, curlevel+1);
        if (node.right!=null)
			putNeededNodesInQ(q, node.right, neededlevel, curlevel+1);
	}	
}
