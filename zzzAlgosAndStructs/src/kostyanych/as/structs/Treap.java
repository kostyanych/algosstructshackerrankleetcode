package kostyanych.as.structs;

import java.util.Queue;

public class Treap<T extends Comparable<T>> {
	
	TreapNode<T> root;

	public Treap() {
	}
	
	public Treap(TreapNode<T> root) {
		this.root=root;
	}
	
	public void insert(T value, long priority) {
		if (root==null)
			root=new TreapNode<T>(value, priority);
		else
			root=insert(root, value, priority);
	}
	
	private TreapNode<T> insert(TreapNode<T> rt, T value, long priority) {
		if (rt==null)
			return new TreapNode<T>(value, priority);
	            
		int compareResult = value.compareTo(rt.val);
	        
		if( compareResult < 0 ) {//new value is less than root
			rt.left = insert(rt.left, value, priority);
			if(rt.left.priority < rt.priority )
				rt = rotateWithLeftChild( rt );
		}
	    else if( compareResult > 0  ) {//new value is greater than root
	    	rt.right = insert(rt.right, value, priority);
	    	if(rt.right.priority < rt.priority )
	    		rt = rotateWithRightChild(rt);
	    }
	    // Otherwise, it's a duplicate; do nothing
        return rt;
	}
	
	private TreapNode<T> rotateWithLeftChild(TreapNode<T> k2) {
		TreapNode<T> k1 = k2.left;
		k2.left = k1.right;
	    k1.right = k2;
	    return k1;
	}

	private TreapNode<T> rotateWithRightChild(TreapNode<T> k1) {
		TreapNode<T> k2 = k1.right;
		k1.right = k2.left;
	    k2.left = k1;
	    return k2;
	}

	public Treap<T> split(T value){
		TreapNodeHolder<T> leftH=new TreapNodeHolder<T>();
		TreapNodeHolder<T> rightH=new TreapNodeHolder<T>();
		split (root, leftH, rightH,value);
		if (root.equals(leftH.hold))
			return new Treap<T>(rightH.hold);
		return new Treap<T>(leftH.hold);
	}
	
	public void split(TreapNode<T> rt, TreapNodeHolder<T> l, TreapNodeHolder<T> r, T value){
		if (rt==null) {
			l.hold=null;
			r.hold=null;
			return;
		}
		if (rt.val.compareTo(value)<=0) {
			l.hold=rt.right;
			split(rt.right, l, r, value);
			//split(rt.right,new TreapNodeHolder<T>(rt.right), r, value);
			l.hold=rt;
		}
		else {
			r.hold=rt.left;
			split(rt.left,l,r, value);
			//split(rt.left,l,new TreapNodeHolder<T>(rt.left), value);
			r.hold=rt;
		}
		updateNodeSize(rt);
	}
	
	public Treap<T> mySplit(T value){
		if (root==null)
			return new Treap<T>();
		if (root.val.equals(value)) {
			Treap<T> res=new Treap<T>();
			res.root=root.right;
			root.right=null;
		}
		TreapNodeHolder<T> p1=new TreapNodeHolder<T>();
		p1.hold=null;
		TreapNodeHolder<T> p2=new TreapNodeHolder<T>();
		p2.hold=root;
		mySplit (root, p1, p2,value);
		if (p1==null)
			return new Treap<T>();
		return new Treap<T>(p1.hold);
	}
	
	
	public void mySplit(TreapNode<T> rt, TreapNodeHolder<T> p1, TreapNodeHolder<T> p2, T value){
		
		if (p2.hold.left!=null && p2.hold.left.val.equals(value)) {
			p1.hold=p2.hold.left;
			p2.hold.left=p2.hold.left.right;
			p1.hold.right=null;
			rt.right=p1.hold;
			p1.hold=p2.hold;
			p2.hold=null;
			return;
		}

		if (p2.hold.right!=null && p2.hold.right.val.equals(value)) {
			p1.hold=p2.hold.right.right;	//hold.right is the max value we can include into unchopped tree!	
			p2.hold.right.right=null;
//			
//			p2.hold.right=p2.hold.right.left;
//			p1.hold.left=null;
//			rt.left=p1.hold;
//			p1.hold=p2.hold;
			p2.hold=null;
			return;
		}
		
		if (value.compareTo(p2.hold.val)>0)
			p2.hold=p2.hold.right;
		else
			p2.hold=p2.hold.left;
		
		while (p2.hold!=null) {
			mySplit(p2.hold, p1, p2, value);
		}
	}
	
	
	protected int getNodeSize(TreapNode<T> n){
	    return n!=null?n.size:0;
	}
	
	protected void updateNodeSize(TreapNode<T> n){
	    if (n==null)
	    	return;
	    n.size=getNodeSize(n.left)+1+getNodeSize(n.right);
	}
	
	public void levelOrderTraversalLineByLevel() {
		if (root==null)
			System.out.println("EMPTY");
		levelOrderTraversalLineByLevel(root);
	}
	
	void levelOrderTraversalLineByLevel(TreapNode<T> root) {
        Queue<TreapNode<T>> q=new java.util.LinkedList<TreapNode<T>>();
        q.add(root);
        
        int nodeCount=0;
        
        while (true) {
        	nodeCount=q.size();
        	if (nodeCount<1)
        		break;
        	while (nodeCount>0) {
        		root=q.remove();
        		nodeCount--;
        		System.out.print(root+" ");
                if (root.left!=null)
                    q.add(root.left);
                if (root.right!=null)
                    q.add(root.right);
        	}
        	System.out.println();
        }
    }		
	
//	void split(pnode t,pnode &l,pnode &r,int key){
//	    if(!t) l=r=NULL;
//	    else if(t->val<=key) split(t->r,t->r,r,key),l=t; //elem=key comes in l
//	    else split(t->l,l,t->l,key),r=t;    
//	    upd_sz(t);
//	}
	
	
//	public TreapNode<T> split(T value) {
//		TreapNode<T> res=null;
//		if (root.val.equals(value)){
//			res=root.right;
//			root.right=null;
//			return res;
//		}
//				
//	}
//	
//	public Pair<TreapNode<T>> split(TreapNode<T> t, T value, long priority) {
//        if (t == null)
//            return new Pair<TreapNode<T>>(null, null);
//        if (t.val.compareTo(value)<0) {
//            Pair<TreapNode<T>> pair = split(t.right, value, priority);
//            t.right = null;
//            return new Pair<TreapNode<T>>(merge(t, pair.o1), pair.o2);
//        }
//        else {
//            Pair<TreapNode<T>> pair = split(t.left, value, priority);
//            t.left = null;
//            return new Pair<TreapNode<T>>(pair.o1, merge(pair.o2, t));
//        }
//    }
//	
//	private TreapNode<T> merge(TreapNode<T> l, TreapNode<T> r) {
//        if (r == null)
//            return l;
//        else if (l == null)
//            return r;
//        else if (l.priority < r.priority) {
//            r.left = merge(l, r.left);
//            return r;
//        }
//        else {
//            l.right = merge(l.right, r);
//            return l;
//        }
//    }

//	
//	public Pair<TreapNode<T>> split(TreapNode<T> node, int index) {
//        
//		if (node == null)
//            return new Pair<TreapNode<T>>(null, null);
//
//        final long leftSize = (node.left!=null? 
//        						node.left.size
//        						:
//        						0);
//        if (index <= leftSize) {
//            final Pair<TreapNode<T>> sub = split(node.left, index);
//            node.left = sub.o2;
//            if (node.left != null)
//                node.left.parent = node;
//            sub.right = node;
//            node.update();
//            return sub;
//        }
//        // else
//        final Pair<T> sub = split((Node<T>)node.right, (index - leftSize - 1));
//        node.right = sub.left;
//        if (node.right != null)
//            node.right.parent = node;
//        sub.left = node;
//        node.update();
//        return sub;
//    }	
}

class TreapNodeHolder<T extends Comparable<T>> {
	TreapNode<T> hold;
	
	public TreapNodeHolder(){
	}
	
	public TreapNodeHolder(TreapNode<T> node){
		hold=node;
	}
	
	@Override
	public String toString() {
		return hold==null? "null" : hold.toString();
	}
	
}