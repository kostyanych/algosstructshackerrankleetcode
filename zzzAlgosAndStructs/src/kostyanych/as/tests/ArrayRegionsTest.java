package kostyanych.as.tests;

import kostyanych.as.algos.ArrayMaxRegions;

public class ArrayRegionsTest {
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		int[] vals={1,-2,3,4,5,6,-7,8};
		System.out.println("just max region sum= "+ArrayMaxRegions.arrayMaxSum(vals));
		long[] valsl={1,-2,3,4,5,6,-7,8};
		long[] res=ArrayMaxRegions.kadane(valsl);
		System.out.println("Kadane. sum= "+res[0]+"   {"+res[1]+","+res[2]+"}");
		int[][] arr = {{1, 2, -1, -4, -20},
	            {-8, -3, 4, 2, 1},
	            {3, 8, 10, 1, 3},
	            {-4, -1, 1, 7, -6}
	           };
		res=ArrayMaxRegions.findMaxSubMatrix(arr);
		System.out.println("2D. sum= "+res[0]+"   {"+res[1]+","+res[2]+"}  --> {"+res[3]+","+res[4]+"}");
	}	
}
