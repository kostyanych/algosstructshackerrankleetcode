package kostyanych.as.tests;

import kostyanych.as.structs.ImplicitTreap;
import kostyanych.as.structs.Treap;

public class TreapTest {

	public static void testTreapInsert(boolean isNeeded) {
		if (!isNeeded)
			return;
		Treap<Integer> tr=new Treap<Integer>();
		tr.insert(10,2);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(25,3);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(20,7);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(30,6);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(36,8);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(21,5);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(7,4);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(19,1);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		
		Treap<Integer> tr2=tr.mySplit(30);
		System.out.println("-tr-");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		System.out.println("-tr2-");
		tr2.levelOrderTraversalLineByLevel();
		System.out.println("----");
		
	}

	public static void testImplicitTreapInsert(boolean isNeeded) {
		if (!isNeeded)
			return;
		ImplicitTreap<Integer> tr=new ImplicitTreap<Integer>();
		tr.insert(19,0);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(10,1);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(25,2);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(7,3);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(21,4);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(30,5);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(20,6);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insert(36,7);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		
		Treap<Integer> tr2=tr.split(3);
		System.out.println("-tr-");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		System.out.println("-tr2-");
		tr2.levelOrderTraversalLineByLevel();
		System.out.println("----");
		
	}
	
	public static void testImplicitTreapInsertArrayLike(boolean isNeeded) {
		if (!isNeeded)
			return;
		ImplicitTreap<Integer> tr=new ImplicitTreap<Integer>();
		tr.insertNextArrayLike(19);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insertNextArrayLike(10);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insertNextArrayLike(25);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insertNextArrayLike(7);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insertNextArrayLike(21);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insertNextArrayLike(30);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insertNextArrayLike(20);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		tr.insertNextArrayLike(36);
		System.out.println("----");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		
		Treap<Integer> tr2=tr.split(3);
		System.out.println("-tr-");
		tr.levelOrderTraversalLineByLevel();
		System.out.println("----");
		System.out.println("-tr2-");
		tr2.levelOrderTraversalLineByLevel();
		System.out.println("----");
		
	}
	
	
}
