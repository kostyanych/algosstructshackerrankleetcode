package kostyanych.as.tests;

import java.util.Arrays;

import kostyanych.as.algos.StockSpan;

public class StockSpanTest {

	public static void test(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] stocks={130,60,110,70,80,90,120,10,150};
		int[] spans=StockSpan.getSpans(stocks);
		System.out.println(Arrays.toString(spans));
	}
	
}
