package kostyanych.as.tests;

import java.util.Comparator;

import kostyanych.as.structs.AVLTree;
import kostyanych.as.structs.BinarySearchTree;
import kostyanych.as.structs.BinaryTree;
import kostyanych.as.structs.BinaryTreeBuilder;
import kostyanych.as.structs.BinaryTreeNode;
import kostyanych.as.structs.RedBlackTree;
import kostyanych.as.structs.TreeSwapNodes;

public class TreeTest {

	public static void testTraversals(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		BinaryTree<Integer> tree=generateTree();
		System.out.println("preOrderTraversal");
		tree.preOrderTraversal();
		System.out.println("postOrderTraversal");
		tree.postOrderTraversal();
		System.out.println("inOrderTraversal");
		tree.inOrderTraversal();
		System.out.println("levelOrderTraversal");
		tree.levelOrderTraversal();
		System.out.println("hackerRankTopView");
		tree.hackerRankTopView();
		System.out.println("height="+tree.height());
//		System.out.println("Is binary tree: "+Tree.isBST(tree));
	}

	public static void testBST(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		BinarySearchTree<Integer> bst=BinarySearchTree.startTree(5);
		bst.insert(3);
		bst.insert(4);
		bst.insert(7);
		System.out.println("BST current node count: "+bst.countNodes());
		bst.insert(2);
		bst.insert(1);
		bst.insert(6);
		bst.insert(8);
		System.out.println("BST current node count: "+bst.countNodes());
		System.out.println("BST preOrderTraversal");
		bst.preOrderTraversal();
		System.out.println("BST postOrderTraversal");
		bst.postOrderTraversal();
		System.out.println("BST inOrderTraversal");
		bst.inOrderTraversal();
		System.out.println("BST leaves:");
		bst.printLeaves();
		System.out.println("BST boundary");
		bst.printBoundary();
		System.out.println("BST levelOrderTraversal");
		bst.levelOrderTraversal();
		System.out.println("BST reverseLevelOrderTraversal");
		bst.reverseLevelOrderTraversal();
		System.out.println("----------- BST levelOrderTraversalLineByLevel ------------");
		bst.levelOrderTraversalLineByLevel();
		System.out.println("----------- BST levelOrderTraversalLineByLevel END ------------");
		System.out.println("BST hackerRankTopView");
		bst.hackerRankTopView();
		System.out.println("BST height="+bst.height());
		System.out.println("LCA of 1 and 4 is "+bst.lca(1, 4).data);
		System.out.println("LCA of 1 and 2 is "+bst.lca(1, 2).data);
		System.out.println("LCA of 2 and 6 is "+bst.lca(2, 6).data);
		System.out.println("LCA of 4 and 1 is "+bst.lca(4, 1).data);
		System.out.println("LCA of 2 and 1 is "+bst.lca(2, 1).data);
		System.out.println("LCA of 8 and 6 is "+bst.lca(8, 6).data);
//		System.out.println("Is binary tree: "+Tree.isBST(bst));
	}
	
	public static BinaryTree<Integer> generateTree() {
		BinaryTree<Integer> tree=BinaryTree.startTree(1);
		tree.root.left=BinaryTree.generareNode(-1);
		BinaryTreeNode<Integer> next1=BinaryTree.generareNode(2);
		tree.root.right=next1;
		BinaryTreeNode<Integer> next2=BinaryTree.generareNode(5);
		next1.right=next2;
		next2.right=BinaryTree.generareNode(6);
		next1=BinaryTree.generareNode(3);
		next2.left=next1;
		next2=BinaryTree.generareNode(4);
		next1.right=next2;
		return tree;
	}

	public static void testSwapNodes(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		BinaryTree<Integer> tree=generateTree();
		tree.inOrderTraversal();
		TreeSwapNodes.swapNodes(tree, 1);
		tree.inOrderTraversal();
	}
	
	public static void testGeneration(boolean isNeeded) {
		if (!isNeeded)
			return;
		
/*
preOrderTraversal
1 -1 2 5 3 4 6 
postOrderTraversal
-1 4 3 6 5 2 1 
inOrderTraversal
-1 1 2 3 4 5 6
levelOrderTraversal
5 3 7 2 4 6 8 1  		
 */
		Integer[] inorder={-1,1,2,3,4,5,6};
		Integer[] preorder={1,-1,2,5,3,4,6};
		Integer[] levelorder={1,-1,2,5,3,6,4};
		Integer[] postorder={-1,4,3,6,5,2,1};
		
		System.out.println("++++++++++++ preorder / inorder =======================");
		BinaryTree<Integer> tree=BinaryTreeBuilder.buildTreeFromInPreOrder(inorder, preorder);
		System.out.println("for constructed tree InOrder (expected/actual):");
		System.out.println("-1 1 2 3 4 5 6");
		tree.inOrderTraversal();
		System.out.println("for constructed tree PreOrder (expected/actual):");
		System.out.println("1 -1 2 5 3 4 6");
		tree.preOrderTraversal();
		System.out.println("for constructed tree PostOrder (expected/actual):");
		System.out.println("-1 4 3 6 5 2 1");
		tree.postOrderTraversal();
		System.out.println("for constructed tree LevelOrder (expected/actual):");
		System.out.println("1 -1 2 5 3 6 4");
		tree.levelOrderTraversal();
		System.out.println("++++++++++++ level / inorder =======================");
		tree=BinaryTreeBuilder.buildTreeFromInLevelOrder(inorder, levelorder);
		System.out.println("for constructed tree InOrder (expected/actual):");
		System.out.println("-1 1 2 3 4 5 6");
		tree.inOrderTraversal();
		System.out.println("for constructed tree PreOrder (expected/actual):");
		System.out.println("1 -1 2 5 3 4 6");
		tree.preOrderTraversal();
		System.out.println("for constructed tree PostOrder (expected/actual):");
		System.out.println("-1 4 3 6 5 2 1");
		tree.postOrderTraversal();
		System.out.println("for constructed tree LevelOrder (expected/actual):");
		System.out.println("1 -1 2 5 3 6 4");
		tree.levelOrderTraversal();
		System.out.println("++++++++++++ postorder / inorder =======================");
		tree=BinaryTreeBuilder.buildTreeFromInPostOrder(inorder, postorder);
		System.out.println("for constructed tree InOrder (expected/actual):");
		System.out.println("-1 1 2 3 4 5 6");
		tree.inOrderTraversal();
		System.out.println("for constructed tree PreOrder (expected/actual):");
		System.out.println("1 -1 2 5 3 4 6");
		tree.preOrderTraversal();
		System.out.println("for constructed tree PostOrder (expected/actual):");
		System.out.println("-1 4 3 6 5 2 1");
		tree.postOrderTraversal();
		System.out.println("for constructed tree LevelOrder (expected/actual):");
		System.out.println("1 -1 2 5 3 6 4");
		tree.levelOrderTraversal();
	}
	
	public static void testBST2(boolean isNeeded) {
		if (!isNeeded)
			return;
		BinarySearchTree<Integer> bst=BinarySearchTree.startTree(15);
		bst.insert(20);
		bst.insert(28);
		bst.insert(10);
		bst.insert(13);
		bst.insert(7);
		bst.insert(30);
		bst.insert(36);
		bst.insert(29);
		bst.insert(19);
		bst.insert(21);
		bst.insert(23);
		bst.insert(22);
		bst.insert(25);
		System.out.println("BST current node count: "+bst.countNodes());
		System.out.println("BST preOrderTraversal");
		bst.preOrderTraversal();
		System.out.println("BST postOrderTraversal");
		bst.postOrderTraversal();
		System.out.println("BST inOrderTraversal");
		bst.inOrderTraversal();
		System.out.println("BST leaves:");
		bst.printLeaves();
		System.out.println("BST boundary");
		bst.printBoundary();
		System.out.println("BST levelOrderTraversal");
		bst.levelOrderTraversal();
		System.out.println("BST reverseLevelOrderTraversal");
		bst.reverseLevelOrderTraversal();
		System.out.println("----------- BST levelOrderTraversalLineByLevel ------------");
		bst.levelOrderTraversalLineByLevel();
		System.out.println("----------- BST levelOrderTraversalLineByLevel END ------------");
		System.out.println("BST hackerRankTopView");
		bst.hackerRankTopView();
		System.out.println("BST height="+bst.height());
		bst.deleteNodeWithValue(20);
		bst.deleteNodeWithValue(36);
		System.out.println("----------- BST levelOrderTraversalLineByLevel ------------");
		bst.levelOrderTraversalLineByLevel();
		bst.hackerRankTopView();
		System.out.println("BST height="+bst.height());
	}
	
	public static void testIsBST(boolean isNeeded) {
		if (!isNeeded)
			return;
		BinarySearchTree<Integer> bst=BinarySearchTree.startTree(15);
		bst.insert(20);
		bst.insert(28);
		bst.insert(10);
		bst.insert(13);
		bst.insert(7);
		bst.insert(30);
		bst.insert(36);
		bst.insert(29);
		bst.insert(19);
		bst.insert(21);
		bst.insert(23);
		bst.insert(22);
		bst.insert(25);
		
		BinaryTree<Integer> tree=BinaryTree.startTree(20);
		tree.root.left=BinaryTree.generareNode(15);
		tree.root.right=BinaryTree.generareNode(30);
		tree.root.left.right=BinaryTree.generareNode(16);
		BinaryTreeNode<Integer> next1=BinaryTree.generareNode(10);
		tree.root.left.left=next1;
		next1.left=BinaryTree.generareNode(8);
		next1.right=BinaryTree.generareNode(17);
		
		Comparator<Integer> intComp=(x,y)->x.compareTo(y);
		System.out.println("this should be a BST: "+bst.isBST(intComp) );
		System.out.println("this should NOT be a BST: "+tree.isBST(intComp) );
	}
	
	//15, 20, 24, 10, 13, 7, 30, 36, and 25.
	public static void testAVL(boolean isNeeded) {
		if (!isNeeded)
			return;
		AVLTree<Integer> avl=AVLTree.startTree(15);
		avl.insert(20);
		avl.insert(24);
		avl.insert(10);
		avl.insert(13);
		avl.insert(7);
		avl.insert(30);
		avl.insert(36);
		avl.insert(25);
//		avl.insert(20);
//		avl.insert(28);
//		avl.insert(10);
//		avl.insert(13);
//		avl.insert(7);
//		avl.insert(30);
//		avl.insert(36);
//		avl.insert(29);
//		avl.insert(19);
//		bst.insert(21);
//		bst.insert(23);
//		bst.insert(22);
//		bst.insert(25);
		
		Comparator<Integer> intComp=(x,y)->x.compareTo(y);
		System.out.println("this should be a BST: "+avl.isBST(intComp) );
		System.out.println("----------- AVL levelOrderTraversalLineByLevel ------------");
		avl.levelOrderTraversalLineByLevel();
		
		avl.deleteNodeWithValue(24);
		System.out.println("----------- AVL levelOrderTraversalLineByLevel ------------");
		avl.levelOrderTraversalLineByLevel();

		avl.deleteNodeWithValue(7);
		System.out.println("----------- AVL levelOrderTraversalLineByLevel ------------");
		avl.levelOrderTraversalLineByLevel();
		
	}
	
	//15, 20, 24, 10, 13, 7, 30, 36, and 25.
		public static void testRedBlack(boolean isNeeded) {
			if (!isNeeded)
				return;
			RedBlackTree<Integer> rbt=RedBlackTree.startTree(15);
			System.out.println("----");
			rbt.levelOrderTraversalLineByLevel();
			System.out.println("----");
			rbt.insert(20);
			System.out.println("----");
			rbt.levelOrderTraversalLineByLevel();
			System.out.println("----");
			rbt.insert(24);
			System.out.println("----");
			rbt.levelOrderTraversalLineByLevel();
			System.out.println("----");
			rbt.insert(10);
			System.out.println("----");
			rbt.levelOrderTraversalLineByLevel();
			System.out.println("----");
			rbt.insert(13);
			rbt.insert(7);
			rbt.insert(30);
			rbt.insert(36);
			rbt.insert(25);
//			avl.insert(20);
//			avl.insert(28);
//			avl.insert(10);
//			avl.insert(13);
//			avl.insert(7);
//			avl.insert(30);
//			avl.insert(36);
//			avl.insert(29);
//			avl.insert(19);
//			bst.insert(21);
//			bst.insert(23);
//			bst.insert(22);
//			bst.insert(25);
			
			Comparator<Integer> intComp=(x,y)->x.compareTo(y);
			System.out.println("this should be a BST: "+rbt.isBST(intComp) );
			System.out.println("----------- RedBlack levelOrderTraversalLineByLevel ------------");
			rbt.levelOrderTraversalLineByLevel();
			
//			rbt.deleteNodeWithValue(24);
//			System.out.println("----------- AVL levelOrderTraversalLineByLevel ------------");
//			rbt.levelOrderTraversalLineByLevel();
//
//			rbt.deleteNodeWithValue(7);
//			System.out.println("----------- AVL levelOrderTraversalLineByLevel ------------");
//			rbt.levelOrderTraversalLineByLevel();
			
		}
}
