package kostyanych.as.tests;

import java.util.Arrays;

import kostyanych.as.algos.strings.SuffixesPrefixes;

public class StringTests {

	public static void testLPSArray(String s, boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] res= SuffixesPrefixes.computeLPSArray(s);
		System.out.println(Arrays.toString(res));
	}
}
