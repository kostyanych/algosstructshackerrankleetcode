package kostyanych.as.tests;

import kostyanych.as.structs.LinkedList;

public class LinkedListTest {
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		LinkedList<Integer> list=LinkedList.generateNewList(3);
		LinkedList<Integer> fiver=list.insertAfterThis(5);
		list.insertAfterThis(4);
		list.insertAtHead(1);
		list.insertAtTail(7);
		fiver.insertAfterThis(6);
		list.insertNth(2, 1);
		list.printWholeList();
		list.deleteHead().print();
		fiver.printWholeList();
		fiver.insertAtHead(-1);
		fiver.printWholeList();
		list.deleteNth(0).print();
		fiver.printWholeList();
		list.deleteNth(2).print();
		fiver.printWholeList();
		fiver.deleteTail().print();
		fiver.printWholeList();
		fiver.insertAtHead(1);
		fiver.insertNth(4, 3);
		fiver.insertAtTail(7);
		fiver.printWholeList();
		fiver.printWholeListReverse();
		list=fiver.reverseAndReturnHead();
		list.printWholeList();
		list.printWholeListReverse();
		
		//test merging
		LinkedList<Integer> lista=LinkedList.generateNewList(1);
		LinkedList<Integer> listb=LinkedList.generateNewList(-1);
		lista.insertAtTail(2);
		listb.insertAtTail(-2);
		lista.insertAtTail(3);
		LinkedList<Integer> btail=listb.insertAtTail(-3);
		lista.insertAtTail(4);
		lista.insertAtTail(5);
		lista.printWholeList();
		listb.printWholeList();
		System.out.println("the shouldn't be a merge");
		System.out.println(LinkedList.findMergeNode(lista, listb));
		LinkedList<Integer> mergepoint=lista.insertAtTail(6);
		lista.insertAtTail(7);
		lista.insertAtTail(8);
		System.out.println("making artifitial merge between lists");
		btail.next=mergepoint;
		lista.printWholeList();
		listb.printWholeList();
		LinkedList.findMergeNode(lista, listb).print();
		LinkedList.findMergeNode(listb, lista).print();
	}

}
