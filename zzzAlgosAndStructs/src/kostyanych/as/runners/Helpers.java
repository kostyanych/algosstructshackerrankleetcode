package kostyanych.as.runners;

import java.util.concurrent.ThreadLocalRandom;

public class Helpers {

	private final static ThreadLocalRandom RND = ThreadLocalRandom.current();

	public static int[] createRandomArray(int size, int max) {
		int[] res=new int[size];
		for (int i=0;i<size;i++) {
			res[i]=RND.nextInt(max+1);
		}
		return res;
	}

}
