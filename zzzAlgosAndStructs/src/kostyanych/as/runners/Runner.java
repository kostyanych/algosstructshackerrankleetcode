package kostyanych.as.runners;

import kostyanych.as.algos.BoyerMooreVotingAlgorithm;
import kostyanych.as.algos.ExpRussianPeasant;
import kostyanych.as.algos.NthFiboDP;
import kostyanych.as.algos.sorting.ArraySortings;
import kostyanych.as.algos.sorting.CountingSort;
import kostyanych.as.algos.sorting.MergeSort;
import kostyanych.as.algos.strings.AhoCorasickOnArrays;
import kostyanych.as.algos.strings.AhoCorasickStraight;
import kostyanych.as.algos.strings.BoyerMoore;
import kostyanych.as.algos.strings.FiniteAutomata;
import kostyanych.as.algos.strings.KnuthMorrisPratt;
import kostyanych.as.algos.strings.LongestCommonSubstring;
import kostyanych.as.algos.strings.LongestPalindromeSubstringDP;
import kostyanych.as.algos.strings.RabinKarp;
import kostyanych.as.algos.strings.SuffixArray;
import kostyanych.as.structs.SegmentTreeAsArray;
import kostyanych.as.tests.ArrayRegionsTest;
import kostyanych.as.tests.LinkedListTest;
import kostyanych.as.tests.StockSpanTest;
import kostyanych.as.tests.StringTests;
import kostyanych.as.tests.TreapTest;
import kostyanych.as.tests.TreeTest;
import kostyanych.as.tests.TrieTest;

public class Runner {

	public static void main(String[] args) {

		LinkedListTest.testMe(false);
		TreeTest.testTraversals(false);
		TreeTest.testBST(false);
		TreeTest.testSwapNodes(false);
		TreeTest.testGeneration(false);
		TreeTest.testBST2(false);
		TreeTest.testIsBST(false);
		TreeTest.testAVL(false);
		TreeTest.testRedBlack(false);
		//AVL.testInsert();
		TreapTest.testTreapInsert(false);
		TreapTest.testImplicitTreapInsert(false);
		TreapTest.testImplicitTreapInsertArrayLike(false);
		ArrayRegionsTest.testMe(false);
		StockSpanTest.test(false);
		//SuperiorCharacters.testMe();
		MergeSort.test(false);
		//HackerlandRadioTransmitters.doMe(true);
		TrieTest.doMe(false);
		StringTests.testLPSArray("ABABB", false);
		KnuthMorrisPratt.doMe(false);
		RabinKarp.doMe(false);
		FiniteAutomata.doMe(false);
		BoyerMoore.doMe(false);
		LongestCommonSubstring.doMe(false);
		SuffixArray.doMe(false);
		LongestPalindromeSubstringDP.doMe(false);
		AhoCorasickOnArrays.doMe(false);
		AhoCorasickStraight.doMe(false);
		SegmentTreeAsArray.doMe(false);
		ExpRussianPeasant.doMe(false);
		NthFiboDP.doMe(false);
		MergeSort.test(false);
		ArraySortings.testMe(false);
		CountingSort.testMe(false);
		BoyerMooreVotingAlgorithm.testMe(true);
	}


}
