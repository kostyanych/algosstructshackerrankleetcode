package kostyanych.as.algos;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Kostya
 *
 * | 1  1 |   |  f(n)  |   | f(n+1) | 
 * |      | x |        | = |        |
 * | 1  0 |   | f(n-1) |   |  f(n)  |
 * 
 * Look at the matrix A = [ [ 1 1 ] [ 1 0 ] ]. 
 * Multiplying A with [ F(n) F(n-1) ] gives us [ F(n+1) F(n) ] , so we say that
 * A* [ F(n) F(n-1) ] = [ F(n+1) F(n) ]
 * start with [ F(1) F(0) ], multiplying it with A gives us [ F(2) F(1) ]; 
 * again multiplying [ F(2) F(1) ] with A gives us [ F(3) F(2) ] and so on...
 * 
 * [ F(2) F(1) ] = A* [ F(1) F(0) ]
 * [ F(3) F(2) ] = A* [ F(2) F(1) ] = A^2 * [ F(1) F(0) ]
 * [ F(4) F(3) ] = A* [ F(3) F(2) ] = A^3 * [ F(1) F(0) ]
 * ..
 * [ F(n+1) F(n) ] = A* [ F(n) F(n-1) ] = A^n * [ F(1) F(0) ]
 *
 * So all that is left is finding the nth power of the matrix A. 
 * This can be computed in O(log n) time, by recursive doubling. 
 * The idea is, to find A^n , we can do R = A^(n/2) * A^(n/2) 
 * and if n is odd, we need do multiply with an A at the end. 
 */
class Mtx {
	public long a11=1;
	public long a12=1;
	public long a21=1;
	public long a22=0;
}

public class NthFiboDP {
	
	final static long MOD=1000000007;
	
//	static Map<Integer, Mtx> memo=new HashMap<Integer, Mtx>();
	
	static Mtx multiplySquare(Mtx a, Mtx b){
		Mtx res=new Mtx();
		
		res.a11 = a.a11*b.a11 + a.a12*b.a21;
		res.a21 = a.a21*b.a11 + a.a22*b.a21;
		res.a12 = a.a11*b.a21 + a.a12*b.a22;
		res.a22 = a.a21*b.a21 + a.a22*b.a22;
		return res;
	}

	static Mtx multiplySquareMod(Mtx a, Mtx b){
		Mtx res=new Mtx();
		
//		res.a11 = ((a.a11%MOD*b.a11%MOD)%MOD + (a.a12%MOD*b.a21%MOD)%MOD)%MOD;
//		res.a21 = ((a.a21%MOD*b.a11%MOD)%MOD + (a.a22%MOD*b.a21%MOD)%MOD)%MOD;
//		res.a12 = ((a.a11%MOD*b.a21%MOD)%MOD + (a.a12%MOD*b.a22%MOD)%MOD)%MOD;
//		res.a22 = ((a.a21%MOD*b.a21%MOD)%MOD + (a.a22%MOD*b.a22%MOD)%MOD)%MOD;
		res.a11 = ((a.a11*b.a11)%MOD + (a.a12*b.a21)%MOD)%MOD;
		res.a21 = ((a.a21*b.a11)%MOD + (a.a22*b.a21)%MOD)%MOD;
		res.a12 = ((a.a11*b.a21)%MOD + (a.a12*b.a22)%MOD)%MOD;
		res.a22 = ((a.a21*b.a21)%MOD + (a.a22*b.a22)%MOD)%MOD;
		return res;
	}

	static Mtx mtxNthPowerMod(Mtx m, long n) {
		if ( n == 1 )
			return m;
//		Mtx res=memo.get(n);
//		if (res!=null)
//			return res;
		Mtx res = mtxNthPowerMod(m, n/2);
		res = multiplySquareMod(res, res);
		if ( n%2 == 1 )
			res = multiplySquareMod(res, m);
//		memo.put(null, res);
		return res;
	}
	
	static Mtx mtxNthPower(Mtx m, long n) {
		if ( n == 1 )
			return m;
//		Mtx res=memo.get(n);
//		if (res!=null)
//			return res;
		Mtx res = mtxNthPower(m, n/2);
		res = multiplySquare(res, res);
		if ( n%2 == 1 )
			res = multiplySquare(res, m);
//		memo.put(null, res);
		return res;
	}
	
	static Mtx mtxPower(Mtx m, long power) {
//		memo.clear();
		return mtxNthPower(m, power);
	}
	
	public static long findNthFibo(long n, long f0, long f1) {
		if (n==0)
			return f0;
		if (n==1)
			return f1;
		Mtx m=mtxPower(new Mtx(), n-1);
		long res=f1*m.a11+f0*m.a12;
		return res;
	}

	public static long findNthFiboMod(long n, long f0, long f1) {
		if (n==0)
			return f0;
		if (n==1)
			return f1;
		Mtx m=mtxNthPowerMod(new Mtx(), n-1);
		//long res=((f1%MOD*m.a11%MOD)%MOD+(f0%MOD*m.a12%MOD)%MOD)%MOD;
		long res=((f1*m.a11)%MOD+(f0*m.a12)%MOD)%MOD;
		return res;
	}
	
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		System.out.println(findNthFibo(6,0,1));
//		System.out.println(findNthFiboMod(6,0,1));
		System.out.println(findNthFiboMod(229176339,509618737,460201239));
		 
	}

}
