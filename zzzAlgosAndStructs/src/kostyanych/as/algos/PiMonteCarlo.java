package kostyanych.as.algos;

import java.util.Date;
import java.util.Random;

public class PiMonteCarlo {
	
	
	static final int CIRCLE_R=100;
	static final int MIN_POINTS_NUM=100000;
	static final double R_SQUARED=CIRCLE_R*CIRCLE_R;
	
	static Random rnd=new Random();

	public static double calcPi(int pointsNum) {
		if (pointsNum<MIN_POINTS_NUM) {
			System.out.println("Nah, number of points cannot be less than "+MIN_POINTS_NUM+". Using that value instead");
			pointsNum=MIN_POINTS_NUM;
		}
		
		rnd.setSeed((new Date()).getTime());
		
		int allPoints=0;
		int circlePoints=0;
		int x=0;
		int y=0;
		for (int i=0;i<pointsNum;i++) {
			x=rnd.nextInt(2*CIRCLE_R);
			y=rnd.nextInt(2*CIRCLE_R);
			allPoints++;
			if (isInsideCircle(x,y))
				circlePoints++;
		}
		return 4*((double)circlePoints/(double)allPoints);
	}
	
	static boolean isInsideCircle(int x, int y) {
		int xx=Math.abs(x-CIRCLE_R);
		int yy=Math.abs(y-CIRCLE_R);
//		double l=Math.sqrt(xx*xx+yy*yy);
//		if (l<=CIRCLE_R)
//			return true;
		if (R_SQUARED>=(xx*xx+yy*yy))
			return true;
		return false;
	}
	
}
