package kostyanych.as.algos;

import java.util.Stack;

public class Euclidian {

	static int eucl(int p, int q) throws Exception {
		if (p==0 && q==0)
			throw new Exception("GCD is undefined for both zeroes");
		if (p==0)
			return q;
		if (q==0)
			return p;
		
        int temp;
        while (q != 0) {
            temp = q;
            q = p % q;
            p = temp;
        }
        return p;
    }
}
