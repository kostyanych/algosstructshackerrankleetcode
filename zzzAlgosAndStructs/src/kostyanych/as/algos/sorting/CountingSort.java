package kostyanych.as.algos.sorting;

import java.util.Arrays;

import kostyanych.as.runners.Helpers;

/**
 * For simplicity, consider the data in the range 0 to 9.
 *
 *  Input data: 1, 4, 1, 2, 7, 5, 2
 *    1) Take a count array to store the count of each unique object.
 *      Index:     0  1  2  3  4  5  6  7  8  9
 *      Count:     0  2  2  0  1  1  0  1  0  0
 *
 *   2) Modify the count array such that each element at each index stores the sum of previous counts.
 *     Index:     0  1  2  3  4  5  6  7  8  9
 *     Count:     0  2  4  4  5  6  6  7  7  7
 *
 *     The modified count array indicates the position of each object in the output sequence.
 *
 *   3) Output each object from the input sequence followed by decreasing its count by 1.
 *     Process the input data: 1, 4, 1, 2, 7, 5, 2. Position of 1 is 2.
 *     Put data 1 at index 2 in output. Decrease count by 1 to place next data 1 at an index 1 smaller than this index.
 */
public class CountingSort {

	public static void sort(int[] arr, int maxNumber) {
		int[] count=new int[maxNumber+1];
		for (int i : arr) {
			count[i]++;
		}
		for (int i=1;i<=maxNumber;i++) {
			count[i]+=count[i-1];
		}
		int[] buf=new int[arr.length];
		//for (int i=0;i<arr.length;i++) {
		//if we want stable sorting we have to go backwards
		//because later entries in arr contributed to bigger counts
		//if we start from 0 now, we'll put later entries to lower-count index
		for (int i=arr.length-1;i>=0;i--) {
			buf[count[arr[i]]-1]=arr[i];
			count[arr[i]]--;
		}

		for (int i=0;i<arr.length;i++) {
			arr[i]=buf[i];
		}

    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

//        char arr[] = {'g', 'e', 'e', 'k', 's', 'f', 'o',
//                    'r', 'g', 'e', 'e', 'k', 's'
//                    };
//
//        sortg(arr);
//
//        System.out.print("Sorted character array is ");
//        System.out.println(Arrays.toString(arr));


		int arrSize = 10;
		int maxElement = 100;
		System.out.println("Array of "+arrSize+" [0,100]");
		int[] arrSrc = Helpers.createRandomArray(arrSize, maxElement);

		int[] arr;
		arr = Arrays.copyOf(arrSrc, arrSize);
		System.out.println(Arrays.toString(arr));
		long time1=System.currentTimeMillis();
		sort(arr, 100);
		long time2=System.currentTimeMillis();
		System.out.println("Counting sort: "+(time2-time1));
		System.out.println(Arrays.toString(arr));
	}

}
