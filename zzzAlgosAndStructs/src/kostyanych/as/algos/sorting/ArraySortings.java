package kostyanych.as.algos.sorting;

import java.util.Arrays;

import kostyanych.as.runners.Helpers;

public class ArraySortings {

	/**
	 * Selection sort: get min element on each pass and put it to the current front (0,1,2,...)
	 * @param arr
	 */
	public static void sortSelection(int[] arr) {
		if (arr==null)
			return;
		int len=arr.length;
		if (len<2)
			return;
		for (int i=0;i<len-1;i++) {
			int minInd=i;
			int min=arr[i];
			for (int j=i+1;j<len;j++) {
				if (arr[j]<min) {
					min=arr[j];
					minInd=j;
				}
			}
			if (i!=minInd) {
				int tmp=arr[i];
				arr[i]=min;
				arr[minInd]=tmp;
			}
		}
	}

	public static void sortBubble(int[] arr) {
		if (arr==null)
			return;
		int len=arr.length;
		if (len<2)
			return;
		boolean ok=false;
		int cnt=0;
		while (!ok) {
			ok=true;
			for (int i=0;i<len-cnt-1;i++) {
				if (arr[i]>arr[i+1]) {
					int tmp=arr[i];
					arr[i]=arr[i+1];
					arr[i+1]=tmp;
					ok=false;
				}
			}
			cnt++;
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		boolean needToPrintArrays=false;

		int arrSize = 10000;
		int maxElement = 1000000;
		System.out.println("Array of "+arrSize+" [0,"+maxElement+"]");
		int[] arrSrc = Helpers.createRandomArray(arrSize, maxElement);

		int[] arr;
		arr = Arrays.copyOf(arrSrc, arrSize);
		if (needToPrintArrays)
			System.out.println(Arrays.toString(arr));
		long time1=System.currentTimeMillis();
		sortSelection(arr);
		long time2=System.currentTimeMillis();
		System.out.println("Selection sort: "+(time2-time1));
		arr = Arrays.copyOf(arrSrc, arrSize);
		time1=System.currentTimeMillis();
		sortBubble(arr);
		time2=System.currentTimeMillis();
		System.out.println("Bubble sort: "+(time2-time1));
		if (needToPrintArrays)
			System.out.println(Arrays.toString(arr));

		arr = Arrays.copyOf(arrSrc, arrSize);
		time1=System.currentTimeMillis();
		CountingSort.sort(arr, maxElement);
		time2=System.currentTimeMillis();
		System.out.println("Counting sort: "+(time2-time1));
		if (needToPrintArrays)
			System.out.println(Arrays.toString(arr));
	}



}
