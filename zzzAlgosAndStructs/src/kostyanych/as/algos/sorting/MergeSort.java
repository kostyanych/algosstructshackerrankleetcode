package kostyanych.as.algos.sorting;

import java.util.Arrays;

public class MergeSort {

	static boolean printArrOnEveryStep;
	
	public static void sort(int[] arr, boolean _printArrOnEveryStep) {
		printArrOnEveryStep=_printArrOnEveryStep;
		int len=arr.length;
		if (printArrOnEveryStep)
			System.out.println("* "+Arrays.toString(arr));
		sort(arr, 0, len-1);
    }
	
	static void sort(int[] arr, int left,  int right) {
		if (right<=left)
			return;
			
		int midPoint = (left+right)/2; 
  
		// Sort first and second halves 
        sort(arr, left, midPoint); 
        sort(arr , midPoint+1, right); 
  
        // Merge the sorted halves 
        merge(arr, left, midPoint, right);
        if (printArrOnEveryStep)
        	System.out.println("+ "+Arrays.toString(arr)+" | "+left+", "+midPoint+", "+right);
	}
	
	static void merge(int arr[], int left, int midPoint, int right) { 
        // Find sizes of two subarrays to be merged 
        int len1 = midPoint - left + 1; 
        int len2 = right - midPoint; 

        /* Create temp arrays */
        int L[] = new int [len1]; 
        int R[] = new int [len2]; 
  
        /*Copy data to temp arrays*/
        for (int i=0; i<len1; ++i) { 
            L[i] = arr[left + i]; 
        }
        for (int j=0; j<len2; ++j) { 
            R[j] = arr[midPoint + 1+ j];
        }
  
  
        /* Merge the temp arrays */
  
        // Initial indexes of first and second subarrays 
        int i = 0, j = 0; 
  
        // Initial index of merged subarry array 
        int curr = left; 
        while (i < len1 && j < len2) { 
            if (L[i] <= R[j]) { 
                arr[curr] = L[i]; 
                i++; 
            } 
            else { 
                arr[curr] = R[j]; 
                j++; 
            } 
            curr++; 
        } 
  
        /* Copy remaining elements of L[] if any */
        while (i < len1) { 
            arr[curr] = L[i]; 
            i++;
            curr++; 
        } 
  
        /* Copy remaining elements of R[] if any */
        while (j < len2) { 
            arr[curr] = R[j]; 
            j++; 
            curr++; 
        } 
    }
	
	public static void test(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {12, 1, 15, 4, 11, 12, 13, 5, 6, 7};
		sort(arr,true);
    }
}
