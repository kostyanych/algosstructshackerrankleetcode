package kostyanych.as.algos.strings;

public class LongestPalindromeSubstringDP {

	 // This function prints the longest palindrome substring of str[]. 
    // It also returns the length of the longest palindrome 
    static int longestPalSubstr(String str) { 
        int len = str.length();   // get length of input string 
  
        // table[i][j] will be false if substring str[i..j] is not palindrome. 
        // Else table[i][j] will be true 
        boolean table[][] = new boolean[len][len]; 
  
        // All substrings of length 1 are palindromes 
        int maxLength = 1; 
        for (int i = 0; i < len; i++) { 
            table[i][i] = true; 
        }
  
        // check for sub-string of length 2. 
        int start = 0; 
        for (int i = 0; i < len - 1; i++) { 
            if (str.charAt(i) == str.charAt(i + 1)) { 
                table[i][i + 1] = true; 
                start = i; 
                maxLength = 2; 
            } 
        } 

        int j;
        // Check for lengths greater than 2. k is length of substring 
        for (int sublen = 3; sublen <= len; ++sublen) { 
              
            // Fix the starting index 
            for (int i = 0; i < len - sublen + 1; i++) { 
                // Get the ending index of substring from 
                // starting index i and length sublen 
            	j = i + sublen - 1; 
  
                // checking for sub-string from ith index to jth index  
                // iff str.charAt(i+1) to str.charAt(j-1) is a palindrome 
                if (table[i + 1][j - 1] && str.charAt(i) ==  str.charAt(j)) { 
                    table[i][j] = true; 
  
                    if (sublen > maxLength) { 
                        start = i; 
                        maxLength = sublen; 
                    } 
                } 
            } 
        } 
        System.out.print("Longest palindrome substring is: "); 
        System.out.println(str.substring(start, start + maxLength));
          
        return maxLength; // return length of LPS 
    }

    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	 String str = "forgeeksskeegfor"; 
         System.out.println("Length is: " +  longestPalSubstr(str));     	
    }
}
