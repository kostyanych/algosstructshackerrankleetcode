package kostyanych.as.algos.strings;

import java.util.HashSet;
import java.util.Set;

public class FiniteAutomata {
	
	final static int NUM_OF_CHARS = 128;
	
	/* This function builds the TF table which 
    represents Finite Automata for a given pattern */
    static int[][] computeTF(String pat, int numOfChars) {
    	int patlen = pat.length();
    	int[] lpsArray=SuffixesPrefixes.computeLPSArray(pat);
    	Set<Character> patChars=new HashSet<Character>();
    	for (int i=0;i<patlen;i++) {
    		patChars.add(pat.charAt(i));
    	}
    	int[][] res=new int[patlen+1][numOfChars];
 
        for (int state = 0; state <= patlen; state++) {
            for (int i = 0; i < numOfChars; i++) {
            	if (!patChars.contains((char)i))
            			continue;
                res[state][i] = getNextState(pat, patlen, state, (char)i, lpsArray);
            }
        }
        return res;
    }
    
    static int getNextState(String pat, int patlen, int state, char chr, int[] lpsArray) {
    	// If the character chr is same as next character in pattern,then simply  
    	// increment state 
    	if(state < patlen && chr == pat.charAt(state)) 
    		return state + 1; 

    	if (state==0)
    		return 0;
    	
    	if (pat.charAt(state-1-lpsArray[state-1])==chr)
    		return lpsArray[state-1]+1;
    	if (pat.charAt(0)==chr)
    		return 1;
    	return 0;
    }
    
    /* Prints all occurrences of pat in txt */
    static void search(String txt, String pat) { 
        int patlen = pat.length();
        int txtlen = txt.length();
  
        int[][] TF = computeTF(pat,NUM_OF_CHARS); 
  
        // Process txt over FA. 
        int state = 0; 
        for (int i = 0; i < txtlen; i++) { 
            state = TF[state][txt.charAt(i)]; 
            if (state == patlen) 
                System.out.println("Pattern found at index " + (i-patlen+1)); 
        } 
    }
    
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		System.out.println("------------------- FiniteAutomata --------------");		
		
		String txt = "ZhopaSruchkoy zhopa Zhopa asdaZhoZhopaasd"; 
        String pat = "Zhopa";
        search(txt, pat);
	}
}
