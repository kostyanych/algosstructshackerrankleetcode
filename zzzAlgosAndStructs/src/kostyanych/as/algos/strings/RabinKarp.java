package kostyanych.as.algos.strings;

public class RabinKarp {
	
	// d is the number of characters in the input alphabet (not so many really, but who cares?) 
    public final static int D = 256;
    // A prime number
    public final static int Q = 101;

    private static int calculateH(int len, int d, int q) {
    	int res = 1;
        // The value of h would be "pow(d, M-1)%q" 
        for (int i = 0; i < len-1; i++) { 
            res = (res*d)%q; 
        }
        return res;
    }
    
    private static int calculateHash(String str, int len, int d, int q, int h) {
		int res = 0;
      
		// Calculate the hash value of pattern and first 
        // window of text 
        for (int i = 0; i < len; i++) { 
            res = (d*res + str.charAt(i))%q; 
        }
        return res;
    }
    
    public static void search(String txt, String pat) { 
        int txtlen=txt.length();
    	int patlen = pat.length(); 

        int h = calculateH(patlen, D, Q); 
      
        int patHash = calculateHash(pat, patlen, D, Q, h);
        int txtHash = calculateHash(txt, patlen, D, Q, h);
        
        int j;
      
        // Slide the pattern over text one by one 
        for (int i = 0; i <= txtlen - patlen; i++) { 
      
            // Check the hash values of current window of text 
            // and pattern. If the hash values match then only 
            // check for characters on by one 
            if ( patHash == txtHash ) {//possible match! 
                /* Check for characters one by one */
                for (j = 0; j < patlen; j++) { 
                    if (txt.charAt(i+j) != pat.charAt(j)) 
                        break; 
                } 
      
                // if patHash == txtHash and pat[0...M-1] = txt[i, i+1, ...i+M-1] 
                if (j == patlen) 
                    System.out.println("Pattern found at index " + i); 
            } 
      
            // Calculate hash value for next window of text: Remove 
            // leading digit, add trailing digit 
            if ( i < txtlen-patlen ) { 
                txtHash = (D*(txtHash - txt.charAt(i)*h) + txt.charAt(i+patlen))%Q; 
      
                // We might get negative value of txtHash, converting it 
                // to positive 
                if (txtHash < 0) 
                	txtHash = (txtHash + Q); 
            } 
        } 
    }
    
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		System.out.println("------------------- RabinKarp --------------");

		String txt = "ZhopaSruchkoy zhopa Zhopa asdaZhoZhopaasd"; 
        String pat = "Zhopa";
        search(txt, pat);
	}
    
}
