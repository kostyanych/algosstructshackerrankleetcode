package kostyanych.as.algos.strings;

public class KnuthMorrisPratt {

	public static void KMPSearch(String txt, String pat) { 
        int patlen = pat.length(); 
        int txtlen = txt.length(); 
  
        // create lps[] that will hold the longest 
        // prefix suffix values for pattern 
        int lps[] = SuffixesPrefixes.computeLPSArray(pat); 
        
        int j = 0; // index for pat[] 
        int i = 0; // index for txt[]
        
        while (i < txtlen) { 
            if (pat.charAt(j) == txt.charAt(i)) { 
                j++; 
                i++; 
            } 
            if (j == patlen) { 
                System.out.println("Found pattern "
                                   + "at index " + (i - j)); 
                j = lps[j - 1]; 
            } 
  
            // mismatch after j matches 
            else if (i < txtlen && pat.charAt(j) != txt.charAt(i)) { 
                // Do not match lps[0..lps[j-1]] characters, 
                // they will match anyway 
                if (j != 0) 
                    j = lps[j - 1]; 
                else
                    i = i + 1; 
            } 
        } 
    }
	
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		System.out.println("------------------- KnuthMorrisPratt --------------");

		String txt = "ZhopaSruchkoy zhopa Zhopa asdaZhoZhopaasd"; 
        String pat = "Zhopa";
        KMPSearch(txt, pat);
	}
}
