package kostyanych.as.algos.strings;

import java.util.ArrayList;
import java.util.List;

public class BoyerMoore {
	
	final static int NUM_OF_CHARS = 128;

	// A utility function to get maximum of two integers 
	static private int max (int a, int b) {
		return (a > b)? a: b;
	} 
	  
	// The preprocessing function for Boyer Moore's 
	// bad character heuristic 
	static private int[] badCharHeuristic(String str, int numOfChars) { 
	    
		int[] res=new int[numOfChars];
		int len=str.length();
	  
	    // Initialize all occurrences as -1 
	    for (int i = 0; i < numOfChars; i++) { 
	         res[i] = -1; 
	    }
	  
	    // Fill the actual value of last occurrence  
	    // of a character 
	    for (int i = 0; i < len; i++) { 
	         res[(int) str.charAt(i)] = i;
	    }
	    
	    return res;
	}
	
	/* A pattern searching function that uses Bad 
	   Character Heuristic of Boyer Moore Algorithm */
	public static List<Integer> search(String txt, String pat) {
		
		List<Integer> res=new ArrayList<Integer>();
		
	    int patlen = pat.length(); 
	    int txtlen = txt.length(); 

	    /* Fill the bad character array by calling  
	       the preprocessing function badCharHeuristic()  
	       for given pattern */
	    int[] badchars=badCharHeuristic(pat, NUM_OF_CHARS);

	    int s = 0;  // s is shift of the pattern with respect to text 
	    
	    while(s <= (txtlen - patlen)) { 
	        
	    	int j = patlen-1; //start from the last patternchar 
	  
	        /* Keep reducing index j of pattern while  
	           characters of pattern and text are  
	           matching at this shift s */
	        while(j >= 0 && pat.charAt(j) == txt.charAt(s+j)) {
	            j--; 
	        }
	  
	        /* If the pattern is present at current 
	           shift, then index j will become -1 after 
	           the above loop */
	        if (j < 0) { //pattern found! 
	        	res.add(s);
	        	System.out.println("Pattern found at index " + s); 
//	            printf("\n pattern occurs at shift = %d", s); 
	  
	            /* Shift the pattern so that the next character in text aligns with the last  
	               occurrence of it in pattern. 
	               The condition s+m < n is necessary for the case when pattern occurs at the end  
	               of text */
	            s += (s+patlen < txtlen)? patlen-badchars[txt.charAt(s+patlen)] : 1; 
	  
	        } 
	        else { //pattern not found!
	            /* Shift the pattern so that the bad character in text aligns with the last occurrence of 
	               it in pattern. The max function is used to make sure that we get a positive shift.  
	               We may get a negative shift if the last occurrence  of bad character in pattern 
	               is on the right side of the current character. */
	            s += max(1, j - badchars[txt.charAt(s+j)]);
	        }
	    } 
	    return res;
	}
	
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("------------------- BoyerMoore --------------");
		String txt = "ZhopaSruchkoy zhopa Zhopa asdaZhoZhopaasd"; 
        String pat = "Zhopa";
        search(txt, pat);
	}
	
}
