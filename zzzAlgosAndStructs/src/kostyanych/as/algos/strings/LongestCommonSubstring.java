package kostyanych.as.algos.strings;

public class LongestCommonSubstring {
	
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		lcs("zhopa123","popa1456");
	}

	static void lcs(String s1, String s2) {
		
//		System.out.println("zhopa123   "+("zhopa123".substring(2, 5)));
		
		int s1len=s1.length();
		int s2len=s2.length();
		
		int maxlen=0;
		int maxinds1=-1;
		
		int[][] table = new int[s1len+1][s2len+1];
		for (int i=0;i<s1len;i++) {
			for (int j=0;j<s2len;j++) {
				if (s1.charAt(i)==s2.charAt(j)) {
					table[i+1][j+1]=table[i][j]+1;
					if (table[i+1][j+1]>maxlen) {
						maxlen=table[i+1][j+1];
						maxinds1=i;
					}
				}
			}
		}
		if (maxlen==0)
			System.out.println("---- no common substrings ----");
		else
			System.out.println("LCSubstring is "+s1.substring(maxinds1-maxlen+1,maxinds1+1));
	}
}
