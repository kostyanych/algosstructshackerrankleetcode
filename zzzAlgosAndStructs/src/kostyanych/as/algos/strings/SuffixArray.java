package kostyanych.as.algos.strings;

import java.nio.file.attribute.AclEntry.Builder;
import java.util.Arrays;
import java.util.Comparator;

class Suffix {
	public int index; 				// To store original index 
    public int rank[]=new int[2];	// To store ranks and next rank pair 
}

public class SuffixArray {
	
	// A comparison function used by sort() to compare two suffixes 
	// Compares two pairs, returns 1 if first pair is smaller
	static Comparator<Suffix> comp = 
					(a,b) -> 
						(a.rank[0] == b.rank[0]) ?
								(a.rank[1] < b.rank[1] ? 1: 0)
								:
								(a.rank[0] < b.rank[0] ? 1: 0);
								

	static int getIndex(char chr) {
		return chr-'a';
	}

	// This is the main function that takes a string 'txt' of size n as an 
	// argument, builds and return the suffix array for the given string 
	static int[] buildSuffixArray(String txt) {
		int len=txt.length();

		// A structure to store suffixes and their indexes
		Suffix[] suffixes=new Suffix[len];

	    // Store suffixes and their indexes in an array of structures. 
	    // The structure is needed to sort the suffixes alphabatically 
	    // and maintain their old indexes while sorting 
	    for (int i = 0; i < len; i++) {
	    	suffixes[i]=new Suffix();
	        suffixes[i].index = i; 
	        suffixes[i].rank[0] = getIndex(txt.charAt(i)); 
	        suffixes[i].rank[1] = ((i+1) < len) ? getIndex(txt.charAt(i+1)): -1; 
	    } 
	  
	    // Sort the suffixes using the comparison function 
	    // defined above.
	    Arrays.sort(suffixes, comp);

	    // At this point, all suffixes are sorted according to first 
	    // 2 characters.  Let us sort suffixes according to first 4 
	    // characters, then first 8 and so on 
	    int ind[]=new int[len];  // This array is needed to get the index in suffixes[] 
	                 // from original index.  This mapping is needed to get 
	                 // next suffix. 
	    for (int k = 4; k < 2*len; k = k*2) { 
	        // Assigning rank and index values to first suffix 
	        int rank = 0; 
	        int prev_rank = suffixes[0].rank[0]; 
	        suffixes[0].rank[0] = rank; 
	        ind[suffixes[0].index] = 0; 
	  
	        // Assigning rank to suffixes 
	        for (int i = 1; i < len; i++) { 
	            // If first rank and next ranks are same as that of previous 
	            // suffix in array, assign the same new rank to this suffix 
	            if (suffixes[i].rank[0] == prev_rank && suffixes[i].rank[1] == suffixes[i-1].rank[1]) { 
	                prev_rank = suffixes[i].rank[0]; 
	                suffixes[i].rank[0] = rank; 
	            } 
	            else { // Otherwise increment rank and assign 
	                prev_rank = suffixes[i].rank[0]; 
	                suffixes[i].rank[0] = ++rank; 
	            } 
	            ind[suffixes[i].index] = i; 
	        } 
	  
	        // Assign next rank to every suffix 
	        for (int i = 0; i < len; i++) { 
	            int nextindex = suffixes[i].index + k/2; 
	            suffixes[i].rank[1] = nextindex < len ? 
	                                  suffixes[ind[nextindex]].rank[0] : -1; 
	        } 
	  
	        // Sort the suffixes according to first k characters 
	        Arrays.sort(suffixes, comp); 
	    } 
	  
	    // Store indexes of all sorted suffixes in the suffix array 
	    int[] suffixArr = new int[len]; 
	    for (int i = 0; i < len; i++) 
	        suffixArr[i] = suffixes[i].index; 
	  
	    // Return the suffix array 
	    return  suffixArr; 
	}
	
	static int compareStringsHelper(String txt, String pat, int txtind) {
		int txtlen=txt.length();
		int patlen=pat.length();
		if (txtlen-txtind+1>=patlen)
			return txt.substring(txtind,txtind+patlen).compareTo(pat);
		else
			return txt.substring(txtind,txtlen).compareTo(pat);
	}
	
	public static void searchSubstring(String txt, String pat) {
		int[] suffixArr=buildSuffixArray(txt);
		
		int txtlen=txt.length();

	    // Do simple binary search for the pat in txt using the 
	    // built suffix array 
	    int l = 0, r = txtlen-1;  // Initilize left and right indexes 
	    while (l <= r) { 
	        // See if 'pat' is prefix of middle suffix in suffix array 
	        int mid = l + (r - l)/2; 
	        int res = compareStringsHelper(txt, pat, mid); 
	  
	        // If match found at the middle, print it and return 
	        if (res == 0) { 
	            System.out.println("Pattern found at index "+suffixArr[mid]); 
	            return; 
	        } 
	  
	        // Move to left half if pattern is alphabtically less than 
	        // the mid suffix 
	        if (res < 0)
	        	r = mid - 1; 
	  
	        // Otherwise move to right half 
	        else
	        	l = mid + 1; 
	    } 
	  
	    // We reach here if return statement in loop is not executed 
	    System.out.println("Pattern not found"); 		
	}

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		System.out.println("------------------- SuffixArray --------------");

		String txt = "ZhopaSruchkoy zhopa Zhopa asdaZhoZhopaasd"; 
        String pat = "Zhopa";
        searchSubstring(txt, pat);
	}
	
}
