package kostyanych.as.algos.strings;

public class SuffixesPrefixes {


	/**
	 * Computes Longest Prefix which is also Suffix array
	 * lps[i] stores length of the maximum matching proper prefix 
	 * which is also a suffix of the sub-pattern pat[0..i].
	 * @param pat pattern to process
	 * @return LPS array
	 */
	public static int[] computeLPSArray(String pat) {
		int patlen=pat.length();
		int[] lps=new int[patlen];
		
        // length of the previous longest prefix suffix 
        int len = 0; 
        int i = 1; 
        lps[0] = 0; // lps[0] is always 0 
  
        // the loop calculates lps[i] for i = 1 to patlen-1 
        while (i < patlen) { 
            if (pat.charAt(i) == pat.charAt(len)) { 
                len++; 
                lps[i] = len; 
                i++; 
            }
            else { // (pat[i] != pat[len])
                // This is tricky. Consider the example. 
                // AAACAAAA and i = 7. The idea is similar 
                // to search step.
                if (len != 0) {
                    len = lps[len - 1];
                    // Also, note that we do not increment i here
                }
                else {// if (len == 0) 
                    lps[i] = 0;
                    i++;
                }
            }
        }
		return lps;
    } 
}
