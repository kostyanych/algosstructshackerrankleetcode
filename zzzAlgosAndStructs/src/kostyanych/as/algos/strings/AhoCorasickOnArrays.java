package kostyanych.as.algos.strings;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class AhoCorasickOnArrays {

	// Maximum number of characters in input alphabet 
	static final int ALPHAB_LEN = 26; 
	  
	// OUTPUT FUNCTION IS IMPLEMENTED USING out[] 
	// Bit i in this mask is one if the word with index i 
	// appears when the machine enters this state. 
	static int[] out; 
	  
	// FAILURE FUNCTION IS IMPLEMENTED USING fail[] 
	static int[] fails; 

	// GOTO FUNCTION (OR TRIE) IS IMPLEMENTED USING trie[][]
	static int[][] trie;
	 
	static int getIndex(char c) {
		return c-'a';
	}
	  
	// Builds the string matching machine. 
	// arr -   array of words. The index of each keyword is important: 
//	         "out[state] & (1 << i)" is > 0 if we just found word[i] 
//	         in the text. 
	// Returns the number of states that the built machine has. 
	// States are numbered 0 up to the return value - 1, inclusive. 
	static int buildMatchingMachine(String[] patterns) {
		int patcount=patterns.length;
		int maxstates=0;
		for (int i=0; i < patcount; i++) {
			maxstates+=patterns[i].length();
		}
		out = new int[++maxstates];
		trie = new int[maxstates][ALPHAB_LEN];
		for (int i=0;i<maxstates;i++) {
			Arrays.fill(trie[i], -1);
		}
	  
	    // Initially, we just have the 0 state 
	    int states = 0; 
	  
	    // Construct values for goto function, i.e., fill trie[][] 
	    // This is same as building a Trie for arr[]
	    for (int i = 0; i < patcount; ++i) { 
	        int currentState = 0; 
	  
	        // Insert all characters of current word in arr[] 
	        for (int j = 0; j < patterns[i].length(); ++j) { 
	            int ch = getIndex(patterns[i].charAt(j)); 
	  
	            // Allocate a new node (create a new state) if a 
	            // node for ch doesn't exist. 
	            if (trie[currentState][ch] == -1) 
	                trie[currentState][ch] = ++states; 
	  
	            currentState = trie[currentState][ch]; 
	        } 
	  
	        // Add current word in output function 
	        out[currentState] |= (1 << i); 
	    } 
	  
	    // For all characters which don't have an edge from root (or state 0) in Trie, 
	    // add a goto edge to state 0 itself 
	    for (int ch = 0; ch < ALPHAB_LEN; ch++) { 
	        if (trie[0][ch] == -1) 
	            trie[0][ch] = 0; 
	    }
	  
	    // Now, let's build the failure function 
	  
	    // Initialize values in fail function 
	    fails = new int[states+1];
	    Arrays.fill(fails, -1);
	  
	    // Failure function is computed in breadth first order 
	    // using a queue 
	    Queue<Integer> q=new LinkedList<Integer>(); 
	  
	    // Iterate over every possible input
	    // All nodes of depth 1 have failure function value as 0.
	    for (int ch = 0; ch < ALPHAB_LEN; ch++) { 
	        if (trie[0][ch] != 0) { 
	            fails[trie[0][ch]] = 0; 
	            q.add(trie[0][ch]); 
	        } 
	    } 
 
	    while (q.size()>0) { 
	        // Remove the front state from queue 
	        int state = q.poll(); 
 	  
	        // For the removed state, find failure function for 
	        // all those characters for which goto function is 
	        // not defined. 
	        for (int ch = 0; ch < ALPHAB_LEN; ++ch) { 
	            // If goto function is defined for character 'ch' 
	            // and 'state' 
	            if (trie[state][ch] != -1) { 
	                // Find failure state of removed state 
	                int failure = fails[state]; 
	  
	                // Find the deepest node labeled by proper suffix of string 
	                // from root to current state. 
	                while (trie[failure][ch] == -1) { 
	                	failure = fails[failure]; 
	                }
	  
	                failure = trie[failure][ch]; 
	                fails[trie[state][ch]] = failure; 
	  
	                // Merge output values 
	                out[trie[state][ch]] |= out[failure]; 
	  
	                // Insert the next level node (of Trie) in Queue 
	                q.add(trie[state][ch]); 
	            } 
	        } 
	    } 
	    return states; 
	} 
	  
	// Returns the next state the machine will transition to using goto 
	// and failure functions. 
	// currentState - The current state of the machine. Must be between 
//	                0 and the number of states - 1, inclusive. 
	// nextInput - The next character that enters into the machine. 
	static int findNextState(int currentState, char nextInput) { 
	    int answer = currentState; 
	    int ch = getIndex(nextInput); 
	  
	    // If goto is not defined, use failure function 
	    while (trie[answer][ch] == -1) 
	        answer = fails[answer]; 
	  
	    return trie[answer][ch]; 
	} 
	  
	// This function finds all occurrences of all array words 
	// in text. 
	static void searchWords(String text, String[] patterns) { 
	    // Preprocess patterns. 
	    // Build machine with goto, failure and output functions 
	    int states=buildMatchingMachine(patterns); 
	  
	    // Initialize current state 
	    int currentState = 0; 
	    int patcount=patterns.length;
	    int txtlen=text.length();
	  
	    // Traverse the text through the built machine to find 
	    // all occurrences of words in patterns[] 
	    for (int i = 0; i < txtlen; i++) { 
	        currentState = findNextState(currentState, text.charAt(i)); 
	  
	        // If match not found, move to next state 
	        if (out[currentState] == 0) 
	             continue; 
	  
	        // Match found, print all matching words of arr[] 
	        // using output function. 
	        for (int j = 0; j < patcount; ++j) { 
	            if ((out[currentState] & (1 << j)) > 0) {
	            	System.out.println("Word "+patterns[j]+" appears from "+(i - patterns[j].length() + 1)+
	            			" to "+i);
	            } 
	        } 
	    } 
	} 	

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;

	    String[] patterns = {"he", "she", "hers", "his", "is"}; 
	    String text = "ahishehers"; 
	    searchWords(text, patterns); 
	} 
}
