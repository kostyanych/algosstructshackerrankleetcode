package kostyanych.as.algos;

public class ExpRussianPeasant {

	public static long expRP(int x, int n) {
		long res;
		while (n%2==0) {
			x=x*x;
			n=n/2;
		}
		res=x;
		n=n/2;
		while (n>0) {
			x=x*x;
			if (n%2==1)
				res=res*x;
			n=n/2;
		}
		return res;
	}
	
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(expRP(2,9));
	}
	
}
