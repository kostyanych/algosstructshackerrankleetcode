package kostyanych.as.algos;

public class ArrayMaxRegions {

	static public long arrayMaxSum(int[] arr) {
		if (arr==null || arr.length<1)
			return 0;
		long max_so_far=0;
		long max_ending_here=0;
		boolean allNegatives=true;
		long maxInArray=arr[0];
		
		for (int i=0;i<arr.length;i++) {
			if (arr[i]>maxInArray)
				maxInArray=arr[i];
			
			max_ending_here+=arr[i];
			
			if (max_ending_here < 0)
				max_ending_here=0;
			if (max_so_far < max_ending_here)
				max_so_far = max_ending_here;
			
			if (arr[i] > 0)
				allNegatives=false;
		}
		
		if (allNegatives)
			return maxInArray;
		return max_so_far;
	}
	
	/**
     * To find maxSum in 1d array
     * 
     * return {maxSum, left, right}
     */
    public static long[] kadane(long[] a) {
        //result[0] == maxSum, result[1] == start, result[2] == end;
        long[] result = new long[]{Long.MIN_VALUE, 0, -1};
        int currentSum = 0;
        int localStart = 0;
     
        for (int i = 0; i < a.length; i++) {
        	currentSum += a[i];
            if (currentSum < 0) {
            	currentSum = 0;
                localStart = i + 1;
            }
            else if (currentSum > result[0]) {
                result[0] = currentSum;
                result[1] = localStart;
                result[2] = i;
            }
        }
         
        //all numbers in a are negative, just return max value and its indices
        if (result[2] == -1) {
            result[0] = 0;
            for (int i = 0; i < a.length; i++) {
                if (a[i] > result[0]) {
                    result[0] = a[i];
                    result[1] = i;
                    result[2] = i;
                }
            }
        }
         
        return result;
    }
    
    /**
     * To find maxSum, (left, top),(right, bottom)
     * return {maxSum, left, top, right, bottom}
     * 
     */
    public static long[] findMaxSubMatrix(int[][] a) {
    	//result[0] == maxSum, result[1] == left, result[2] == top, result[3] == right, result[4] == bottom;
    	int cols = a[0].length;
        int rows = a.length;
        long[] currentResult;
        long maxSum = Long.MIN_VALUE;
        long left = 0;
        long top = 0;
        long right = 0;
        long bottom = 0;
         
        for (int leftCol = 0; leftCol < cols; leftCol++) {
        	long[] tmp = new long[rows];
     
            for (int rightCol = leftCol; rightCol < cols; rightCol++) {
         
            	for (int i = 0; i < rows; i++) {
            		tmp[i] += a[i][rightCol];
                }
            	
                currentResult = kadane(tmp);
                if (currentResult[0] > maxSum) {
                	maxSum = currentResult[0];
                    left = leftCol;
                    top = currentResult[1];
                    right = rightCol;
                    bottom = currentResult[2];
                }
            }
        }
        
        long[] res=new long[5];
        res[0]=maxSum;
        res[1]=left;
        res[2]=top;
        res[3]=right;
        res[4]=bottom;
//              
//        System.out.println("MaxSum: " + maxSum + 
//                                ", range: [(" + left + ", " + top + 
//                                  ")(" + right + ", " + bottom + ")]");
        return res;
    }
}
