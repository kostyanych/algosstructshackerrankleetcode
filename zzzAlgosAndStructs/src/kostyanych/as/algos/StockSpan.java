package kostyanych.as.algos;

import java.util.Stack;

public class StockSpan {

	public static int[] getSpans(int[] stocks) {
		if (stocks==null || stocks.length<1)
			return new int[0];
		
		int n=stocks.length;
		
		int[] res=new int[n];
		res[0]=1;
		Stack<Integer> st=new Stack<Integer>();
		st.push(0);
		
		for (int i=1;i<n;i++) {
			if (stocks[i]<stocks[st.peek()]){
				res[i]=i-st.peek();
			}
			else {
				while (!st.isEmpty() && stocks[i]>stocks[st.peek()]) {
					st.pop();
				}
				if (st.isEmpty())
					res[i]=i+1;
				else
					res[i]=i-st.peek();
			}
			st.push(i);
		}
		
		return res;
	}
}
