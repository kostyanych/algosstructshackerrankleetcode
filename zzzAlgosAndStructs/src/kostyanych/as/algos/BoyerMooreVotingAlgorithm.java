package kostyanych.as.algos;

import java.security.InvalidParameterException;

public class BoyerMooreVotingAlgorithm {

	static class NoMajorException extends Exception{
		public NoMajorException() {super();}
		public NoMajorException(String msg) {super(msg);}
	};

	private static int findMajorityElement(int[] arr) /*throws NoMajorException*/ {
		if (arr == null || arr.length < 1)
			throw new InvalidParameterException("Null or empty array");
		int count=1;
		int preElement=arr[0];
		for (int i=1;i<arr.length;i++) {
			if (preElement==arr[i])
				count++;
			else {
				count--;
				if (count==0) {
					count=1;
					preElement = arr[i];
				}
			}
		}
//		if (count>arr.length/2)
			return preElement;
//		throw new NoMajorException("No major element found!");
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {3,1,2,1,3,2,3,2,1,3};
		try {
			System.out.println(findMajorityElement(arr));
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
