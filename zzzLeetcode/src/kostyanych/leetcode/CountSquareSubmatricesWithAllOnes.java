package kostyanych.leetcode;

/**
 * Given a m * n matrix of ones and zeros, return how many square submatrices have all ones.
 *
 */
public class CountSquareSubmatricesWithAllOnes {

	private static int countSquares(int[][] mx) {
        int rows=mx.length;
        int cols=mx[0].length;

		int memo[][] = new int[rows+1][cols + 1];
        int sum = 0;

        for(int r = 1; r <= rows; r++) {
        	for(int c = 1; c <= cols; c++) {
        		if(mx[r - 1][c - 1] == 1) { //we shifted memo down and right by 1
        			memo[r][c] = Math.min( Math.min(memo[r - 1][c], memo[r][c - 1]),memo[r - 1][c - 1]) + 1;
        			sum+=memo[r][c];
        		}
        	}

        }
        return sum;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[][] arr= {{1,0,1,0,1},{1,0,0,1,1},{0,1,0,1,1},{1,0,0,1,1}};
		System.out.println(countSquares(arr));
	}

}
