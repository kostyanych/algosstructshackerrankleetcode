package kostyanych.leetcode.linkedlists;

import java.util.Stack;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Sort a linked list using insertion sort.
 *
 */
public class InsertionSortList {

	static private ListNode insertionSortListStack(ListNode head) {
        if(head == null || head.next == null)
            return head;

        Stack<ListNode> st = new Stack<>();
        st.push(head);
        ListNode cur = head.next;

        while (cur!=null) {
            if (cur.val>=st.peek().val) {
                st.push(cur);
                cur=cur.next;
                continue;
            }
            ListNode tmpNext=cur.next;
            while (true) {
                ListNode lastInStack=st.peek();
                if (cur.val<lastInStack.val) {
                    lastInStack.next=tmpNext;
                    tmpNext=lastInStack;
                    st.pop();
                    if (tmpNext==head) {
                        head=cur;
                        cur.next=tmpNext;
                        st.push(head);
                        break;
                    }
                }
                else {
                    cur.next=tmpNext;
                    lastInStack.next=cur;
                    st.push(cur);
                    break;
                }
            }
            cur=cur.next;
        }
        return head;
    }

	private static ListNode insertionSortList(ListNode head) {
        if(head == null || head.next == null)
            return head;

        ListNode prev = head;
        ListNode cur = prev.next;

        while(cur != null) {
            if(cur.val < prev.val) {
                ListNode tmp = new ListNode(0,head);
                while(tmp.next.val < cur.val) { //looking for a place where cur would fit
                    tmp = tmp.next;
                }

                prev.next = cur.next;

                if(tmp.next == head) { //cur < head
                    cur.next = head;
                    head = cur;
                }
                else { //cur goes after tmp
                    cur.next = tmp.next;
                    tmp.next = cur;
                }

                cur = prev.next;

            }
            else {
                prev=cur;
                cur=cur.next;
            }
        }
        return head;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
    	ListNode l1= ListNode.fromArray(new int[]{-1,5,3,4,0});
    	l1.print();
    	l1=insertionSortList(l1);
    	l1.print();
    }
}
