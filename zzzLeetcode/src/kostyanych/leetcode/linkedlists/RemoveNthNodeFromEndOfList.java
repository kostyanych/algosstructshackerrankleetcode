package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Given a linked list, remove the n-th node from the end of list and return its head.
 * Given n will always be valid.
 */
public class RemoveNthNodeFromEndOfList {

	/**
	 * In one pass
	 */
	private static ListNode removeNthFromEnd(ListNode head, int n) {
		if (head == null || head.next == null)
			return null;
		int len = 1;
		ListNode ln = head;
		while (ln.next != null) {
			ln = ln.next;
			len++;
		}
		if (len==n)
			return head.next;
		ln = head;
		for (int i = 1; i < len - n; i++) {
			ln = ln.next;
		}
		ln.next = ln.next.next;
		return head;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		ListNode ln = ListNode.fromArray(new int[] {1,2});
		ln.print();
		removeNthFromEnd(ln,1).print();
	}
}
