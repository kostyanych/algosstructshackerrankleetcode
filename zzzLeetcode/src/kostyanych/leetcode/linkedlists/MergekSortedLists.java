package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

public class MergekSortedLists {
	
//	static class ListNode {
//		 int val;
//		 ListNode next;
//		 ListNode(int x) { val = x; }
//	}	

	private static ListNode mergeKLists(ListNode[] lists) {
        ListNode res=null;
        ListNode root=null;
        while (true) {
            int i=findNextList(lists,res);
            if (i<0)
                break;
            ListNode next=new ListNode(lists[i].val);
            if (res==null) {
                res=next;
                root=res;
            }
            else {
                res.next=next;
                res=next;
            }
            lists[i]=lists[i].next;
            while (lists[i]!=null) {
                if (lists[i].val==next.val) {
                    ListNode nxt=new ListNode(lists[i].val);
                    res.next=nxt;
                    res=nxt;
                    lists[i]=lists[i].next;
                }
                else
                    break;
            }
        }
        return root;
    }
    
    private static int findNextList(ListNode[] lists, ListNode cur) {
        int mini=-1;
        int min=Integer.MAX_VALUE;
        for (int i=0;i<lists.length;i++) {
            if (lists[i]==null)
                continue;
            if (cur!=null && lists[i].val==cur.val)
                return i;
            if (min>lists[i].val) {
                mini=i;
                min=lists[i].val;
            }
        }
        return mini;
    }
    
    private static ListNode generateFromArray(int[] arr) {
    	ListNode root = new ListNode(arr[0]);
    	ListNode res=root;
    	for (int i=1;i<arr.length;i++) {
    		ListNode nxt=new ListNode(arr[i]);
    		res.next=nxt;
    		res=nxt;
    	}
    	return root;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
    	ListNode l1= generateFromArray(new int[]{1,4,5});
    	ListNode l2= generateFromArray(new int[]{1,3,4});
    	ListNode l3= generateFromArray(new int[]{2,6});
    	ListNode[] arr={l1,l2,l3};
    	ListNode res=mergeKLists(arr);
    	printListNode(res);
    }
    
    private static void printListNode(ListNode ln) {
    	System.out.print(ln.val);
    	ln=ln.next;
    	while (ln!=null) {
    		System.out.print("->"+ln.val);
    		ln=ln.next;
    	}
    }
}
