package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

public class MergeTwoSortedLists {

	private static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode head = null;
        if (l1==null)
            return l2;
        if (l2==null)
            return l1;
        if (l1.val<l2.val) {
            head=l1;
            l1=l1.next;
        }
        else {
            head=l2;
            l2=l2.next;
        }
        ListNode cur = head;
        while (l1!=null || l2!=null) {
            if (l1==null) {
                cur.next=l2;
                l2=l2.next;
            }
            else if (l2==null) {
                cur.next=l1;
                l1=l1.next;
            }
            else if (l1.val<l2.val) {
                cur.next=l1;
                l1=l1.next;
            }
            else {
                cur.next=l2;
                l2=l2.next;
            }
            cur=cur.next;
        }
        cur.next=null;
        return head;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		ListNode l1= ListNode.fromArray(new int[]{1});
		ListNode l2= ListNode.fromArray(new int[]{2});
	    ListNode res=mergeTwoLists(l1, l2);
	    res.print();
	}
}
