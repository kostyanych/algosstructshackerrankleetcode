package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Given a linked list, swap every two adjacent nodes and return its head. You
 * may not modify the values in the list's nodes, only nodes itself may be
 * changed.
 *
 */
public class SwapNodesInPairs {

	private static ListNode swapPairs(ListNode head) {
		if (head == null || head.next == null)
			return head;
		ListNode node1 = head;
		ListNode node2 = node1.next;
		ListNode next = node2.next;
		head = node2;
		head.next = node1;
		node1.next = next;
		while (next != null && next.next != null) {
			node2 = next.next;
			node1.next = node2;
			node1 = next;
			next = node2.next;
			node2.next = node1;
		}
		node1.next=next;
		return head;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		ListNode ln=ListNode.fromArray(new int[] {1,2,3});
		ln.print();
		swapPairs(ln).print();
	}
}
