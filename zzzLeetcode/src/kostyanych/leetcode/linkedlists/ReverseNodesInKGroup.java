package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Given a linked list, reverse the nodes of a linked list k at a time and
 * return its modified list.
 *
 * k is a positive integer and is less than or equal to the length of the linked
 * list. If the number of nodes is not a multiple of k then left-out nodes in
 * the end should remain as it is.
 *
 */
public class ReverseNodesInKGroup {

	private static ListNode reverseKGroup(ListNode head, int k) {
		if (head == null || head.next == null)
			return head;

		ListNode ln1=head;
		int count=1;
		while (ln1.next!=null) {
			count++;
			ln1=ln1.next;
		}

		ListNode preHead = new ListNode(0);
	    preHead.next = head;
	    ListNode pre=preHead;

	    for (int i = 0; i < count/k ; i++) {
	    	ListNode runner = pre.next.next;
	        ListNode tail = pre.next;
	        ListNode prev=tail;

	        for (int j = 1; j < k; j++) {
	            ListNode tmp=runner.next;
	            runner.next=prev;
	            prev=runner;
	            runner=tmp;
	        }
	        pre.next=prev;
	        tail.next=runner;
	        pre=tail;
	    }
		return preHead.next;
	}


	private static ListNode reverseKGroup111(ListNode head, int k) {
		if (head == null || head.next == null)
			return head;

		ListNode ln1=head;
		int count=1;
		while (ln1.next!=null) {
			count++;
			ln1=ln1.next;
		}

		ListNode preHead = new ListNode(0);
	    preHead.next = head;
	    ListNode res = preHead;

	    for (int i = 0; i < count/k ; i++) {
	    	ListNode runner = preHead.next.next;
	        ListNode tail = preHead.next;

	        for (int j = 1; j < k; j++) {
	        	ListNode tmp = preHead.next;
	            preHead.next = runner;
	            runner = runner.next;
	            preHead.next.next = tmp;
	            tail.next = runner;
	        }
	        preHead = tail;
	    }


//		ListNode preHead=new ListNode(0);
//		preHead.next=head;
//		ListNode res=head;
//		for (int i=0;i<count/k;i++) {
//            ListNode prev=null;
//            ListNode cur=preHead.next;
//            for (int j=1;j<k;j++) {
//            	ListNode next=cur.next;
//            	cur.next=prev;
//    			prev=cur;
//    			cur=next;
//            }
//            preHead.next=cur.next;
//            cur.next=prev;
//            if (i==0)
//            	res=cur;
//            preHead=preHead.next;
//		}
		return res;
	}



	private static ListNode reverseKGroupShit(ListNode head, int k) {

		if (head == null || head.next == null)
			return head;

		ListNode start = head;
		ListNode cur = head;
		while (true) {
			ListNode tail = cur;
			for (int i = 1; i < k; i++) {
				if (tail.next == null)
					return head;
				tail = tail.next;
			}

			if (tail.next==null)
				return head;

			if (start == head) {
				head = tail;
			}
			cur = start;
			start = tail.next;
			ListNode prev = start;
			while (cur.next != start) {
				ListNode next = (ListNode) cur.next;
				cur.next = prev;
				prev = cur;
				cur = next;
			}
			cur = start;
			if (1==2)
				break;
		}
		return head;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		ListNode ln=ListNode.fromArray(new int[] {1,2,3, 4, 5});
		ln.print();
		reverseKGroup(ln,2).print();

	}


}
