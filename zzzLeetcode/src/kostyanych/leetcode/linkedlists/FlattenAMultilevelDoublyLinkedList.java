package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

public class FlattenAMultilevelDoublyLinkedList {

	static class Node {
	    public int val;
	    public Node prev;
	    public Node next;
	    public Node child;

	    public Node (int val, Node prev, Node next) {
	    	this.val=val;
	    	this.prev=prev;
	    	this.next=next;
	    }

	    static Node fromString(String data) {
	    	if (data==null)
				return null;
			data=data.trim();
			int len=data.length();
			if (len<2)
				return null;
			if (data.charAt(0)!='[' && data.charAt(len-1)!=']')
				return null;
			data=data.substring(1,len-1);
			String[] snodes=data.split(",");
			len = snodes.length;
			if ("null".equals(snodes[0]))
				return null;
			Node head=null;
			Node cur=null;
			int i=0;
			for (;i<len;i++) {
				if ("null".equals(snodes[i]))
					break;
				Node buf=new Node(Integer.parseInt(snodes[i]),cur,null);
				if (cur!=null)
					cur.next=buf;
				cur=buf;
				if (head==null)
					head=cur;
			}
			if (i!=len-1)
				fillLevels(head,snodes,i+1);
			return head;
	    }

	    static void fillLevels(Node parentRoot, String[] snodes, int ind) {
	    	int len = snodes.length;
	    	int i=ind;
	    	boolean heads=true;
			Node head=null;
			Node cur=null;
			int parentInd=0;
	    	for (;i<len;i++) {
				if ("null".equals(snodes[i])) {
					if (heads)
						continue;
					break;
				}
				if (heads)
					parentInd=i-ind;
				heads=false;
				Node buf=new Node(Integer.parseInt(snodes[i]),cur,null);
				if (cur!=null)
					cur.next=buf;
				cur=buf;
				if (head==null)
					head=cur;
			}
	    	if (head!=null) {
		    	Node parent=parentRoot;
		    	for (int j=0;j<parentInd;j++) {
		    		parent=parent.next;
		    	}
		    	parent.child=head;
				if (i!=len-1)
					fillLevels(head,snodes,i+1);
	    	}
	    }

	    public String toString() {
	    	StringBuilder sb = new StringBuilder();
	    	sb.append("[").append(val);
	    	Node cur=this.next;
	    	while (cur!=null) {
	    		sb.append(", ").append(cur.val);
	    		cur=cur.next;
	    	}
	    	sb.append("]");
	    	return sb.toString();
	    }
	};

	private static Node flatten(Node head) {
		if (head==null)
			return null;
		processLevelAndGetLast(head);
		return head;
	}

	private static Node processLevelAndGetLast(Node head) {
		Node prev=head;
		Node next=null;
		do {
			if (head.child==null) {
				if (head.next==null)
					break;
				head=head.next;
				continue;
			}
			next=head.next;
			head.next=head.child;
			head.child.prev=head;
			head.child=null;
			Node lastChild=processLevelAndGetLast(head.next);
			lastChild.next=next;
			if (next!=null)
				next.prev=lastChild;
			head=lastChild;
		} while (head.next!=null);
		return head;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		String s="[1,2,3,4,5,6,null,null,null,7,8,9,10,null,null,11,12]";
		String s="[1,2,null,3]";
		Node root = Node.fromString(s);
		System.out.println(s);
		System.out.println(flatten(root));
	}

}
