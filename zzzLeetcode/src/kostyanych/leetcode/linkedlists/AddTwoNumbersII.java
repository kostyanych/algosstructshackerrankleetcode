package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

/**
 * You are given two non-empty linked lists representing two non-negative integers.
 * The most significant digit comes first and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
 *
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 *
 * Follow up:
 * What if you cannot modify the input lists? In other words, reversing the lists is not allowed.
 *
 */
public class AddTwoNumbersII {
	static private ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		if(l1 == null)
			return l2;

		if(l2 == null)
			return l1;

		int len1 = getLength(l1);
		int len2 = getLength(l2);
		if (len1<len2)
			return addTwoNumbers(l2, l1);

		ListNode res = helper(l1,l2,len1-len2);
		if (res.val==0) //artificial left addition without carry
			res=res.next;
		return res;
	}

	static private int getLength(ListNode l) {
		if (l==null)
			return 0;
		int res=1;
		while(l.next != null){
			res++;
			l = l.next;
		}
		return res;
	}

	static private ListNode helper(ListNode l1, ListNode l2, int shift) {
		if (l2 == null)
			return new ListNode(0);
        ListNode left=new ListNode(0);
        ListNode curDigit;
        if (shift>0)
        	curDigit=helper(l1.next, l2, shift - 1);
        else
        	curDigit=helper(l1.next, l2.next, 0);
        curDigit.val+=l1.val;
        if (shift==0)
        	curDigit.val+=l2.val;
        if (curDigit.val>=10) {
        	left.val=1;
        	curDigit.val%=10;
        }
        left.next=curDigit;
        return left;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
    	ListNode l1= ListNode.fromArray(new int[]{7,2,4,3});
    	ListNode l2= ListNode.fromArray(new int[]{5,6,4});
    	l1.print();
    	l2.print();
    	ListNode l3=addTwoNumbers(l1, l2);
    	l3.print();
    }
}
