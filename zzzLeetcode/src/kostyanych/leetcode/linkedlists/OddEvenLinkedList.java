package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Given a singly linked list, group all odd nodes together followed by the even nodes.
 * Please note here we are talking about the node number and not the value in the nodes.
 *
 * You should try to do it in place. The program should run in O(1) space complexity and O(nodes) time complexity.
 *
 * Example:
 * Input: 1->2->3->4->5->NULL
 * Output: 1->3->5->2->4->NULL
 *
 */
public class OddEvenLinkedList {

	private static ListNode oddEvenList(ListNode head) {
        if (head==null)
            return head;
        ListNode even=head.next;
        ListNode evenRunner=head.next;
        ListNode odd=head;
        while (evenRunner!=null) {
            odd.next=evenRunner.next;
            if (odd.next!=null)
            	odd=odd.next;
            if (odd!=null)
                evenRunner.next=odd.next;
            else
                evenRunner.next=null;
            evenRunner=evenRunner.next;
        }
        odd.next=even;
        return head;

    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {1,2,3,4,5,6,7};
		ListNode ln = ListNode.fromArray(arr);
		ln = oddEvenList(ln);
		ln.print();
	}

}
