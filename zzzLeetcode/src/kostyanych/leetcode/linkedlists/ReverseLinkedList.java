package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

public class ReverseLinkedList {

	private static ListNode reverseIterative(ListNode ln) {
		if (ln==null || ln.next==null)
			return ln;
		ListNode cur = ln;
		ListNode prev=null;
		while (cur!=null) {
			ListNode next=(ListNode)cur.next;
			cur.next=prev;
			prev=cur;
			cur=next;
		}
		return prev;
	}

	private static ListNode reverseRecursive(ListNode ln) {
		if (ln==null || ln.next==null)
			return ln;
		return reverseRecursiveHelper(ln, null);
	}

	private static ListNode reverseRecursiveHelper(ListNode ln, ListNode prev) {
		ListNode next=ln.next;
		ln.next=prev;
		if (next==null)
			return ln;
		return reverseRecursiveHelper(next,ln);
	}


	/**
	 * Reverses linked list from start-th node to end-th node (both inclusive in reverse).
	 * Both indexes are 1-based.
	 * If end > list.size, reverses the list from start-th to the end
	 * @param ln
	 * @param start
	 * @param end
	 * @return
	 */
	private static ListNode reverse(ListNode ln, int start, int end) {
		if (ln==null || ln.next==null)
			return ln;
		if (start==end)
			return ln;
		ListNode head = ln;
		ListNode cur=ln;
		for (int i=1;i<start-1;i++) {
			cur=cur.next;
		}
		ListNode prevTail=cur;
		ListNode newTail=cur;
		ListNode prev=null;
		int ind=start;
		if (start>1) {
			cur=cur.next;
			newTail=cur;
		}
		while (cur!=null && ind<=end) {
			ListNode next=(ListNode)cur.next;
			cur.next=prev;
			prev=cur;
			cur=next;
			ind++;
		}
		if (start==1)
			head=prev;
		else
			prevTail.next=prev;
		if (cur!=null)
			newTail.next=cur;
		return head;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		ListNode ln=ListNode.fromArray(new int[] {1,2,3,4,5});
		ln.print();
		ListNode rev=reverseRecursive(ln);
		//ListNode rev=reverse(ln,1,3);
		rev.print();
	}

}
