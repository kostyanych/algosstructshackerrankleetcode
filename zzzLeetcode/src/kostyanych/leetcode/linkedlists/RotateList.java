package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;
import kostyanych.leetcode.helpers.Printer;

/**
 * Given a linked list, rotate the list to the right by k places, where k is non-negative.
 *
 * Input: 1->2->3->4->5->NULL, k = 2
 * Output: 4->5->1->2->3->NULL
 */
public class RotateList {

	private static ListNode rotateRight(ListNode head, int k) {
        if (head==null || head.next==null)
            return head;
        //in case k>list.size, we'll need to normalize k
        int count=1;
        ListNode cur = head;
        ListNode oldTail=null;
        while(cur.next!=null) {
            count++;
            cur=cur.next;
        }
        k=k%count;
        if (k==0)
            return head;
        oldTail=cur;

        cur=head;
        count=count-k;
        for (int i=1;i<count;i++) {
            cur=cur.next;
        }
        oldTail.next=head;
        head=cur.next;
        cur.next=null;
        return head;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		ListNode ln=ListNode.fromArray(new int[]{1,2,3,4,5});
		int k=2;
		System.out.println(ln);
		System.out.println(rotateRight(ln, k));
	}
}
