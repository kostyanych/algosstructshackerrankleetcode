package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;
import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given the head of a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.
 *
 * For this problem, a height-balanced binary tree is defined as
 * a binary tree in which the depth of the two subtrees of every node never differ by more than 1.
 *
 * Constraints:
 *
 * The number of nodes in head is in the range [0, 2 * 10^4].
 * -10^5 <= Node.val <= 10^5
 *
 */
public class ConvertSortedListToBinarySearchTree {

	static private TreeNode sortedListToBST(ListNode head) {
		if (head==null)
			return null;
		return process(head);
    }

	static private TreeNode process(ListNode head) {
		if (head == null)
			return null;
		if (head.next == null)
			return new TreeNode(head.val);
		ListNode beforeMid = beforeMiddle(head);
		ListNode mid = beforeMid.next;
		ListNode afterMid = mid.next;
		beforeMid.next = null;
		TreeNode res = new TreeNode(mid.val);
		res.left = process(head);
		res.right = process(afterMid);

		return res;
	}

	static private ListNode beforeMiddle(ListNode head) {
		ListNode fast = head;
		ListNode slow = head;
		ListNode res = slow;
		while(fast.next != null && fast.next.next != null) {
			res = slow;
			fast = fast.next.next;
			slow = slow.next;
		}

		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		ListNode head = ListNode.fromArray(new int[] {-10,-3,0,5,9});
		TreeNode tn = sortedListToBST(head);
		System.out.println(tn);
	}

}
