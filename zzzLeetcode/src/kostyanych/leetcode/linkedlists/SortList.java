package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Sort a linked list in O(n log n) time using constant space complexity.
 */
public class SortList {

	private static ListNode sortList(ListNode head) {

		//return sortListSlow(head);
		return mergeSort(head);
	}

	private static ListNode mergeSort(ListNode head) {
        if (head==null || head.next==null)
            return head;
		ListNode fast=head;
		ListNode slow=head;
		ListNode buf=null;
		while (fast!=null && fast.next!=null) {
			fast=fast.next.next;
			buf=slow;
			slow=slow.next;
		}
		//slow now points to the middle of the list (well, +-)
		buf.next=null; //cut original list before middle (slow)

		return mergeLists(mergeSort(head),mergeSort(slow));
	}

	private static ListNode mergeLists(ListNode n1, ListNode n2) {
		ListNode preHead=new ListNode(0);
	    ListNode pre=preHead;
	    while(n1!=null && n2!=null) {
	    	if(n1.val<=n2.val) {
	    		pre.next=n1;
                n1=n1.next;
            }
            else {
	    		pre.next=n2;
                n2=n2.next;
            }
	    	pre=pre.next;
        }
	    if (n1!=null)
        	pre.next=n1;
	    if (n2!=null)
        	pre.next=n2;

        return preHead.next;
	}

//------------------------------------------------------------------------------------------
	private static ListNode sortListSlow(ListNode head) {
        if (head==null || head.next==null)
            return head;

        ListNode ph=new ListNode(-1);
        ListNode min, max, last;

        ph.next=head;
        min=head;
        max=head;
        last=head;
        ListNode cur=head.next;
        head.next=null;

        while (cur!=null) {
//System.out.println("min ="+min.val+", max="+max.val+", last="+last.val+", cur="+cur.val );
            if (cur.val<=min.val) { //current minimum
            	min=cur;
            	cur=cur.next;
            	min.next=ph.next;
            	ph.next=min;
            }
            else if (cur.val>=max.val) { //current maximum
                max.next=cur;
                max=cur;
                cur=cur.next;
                max.next=null;
            }
            else if (cur.val>=last.val) { //between Last and MAX
                ListNode runner=last;
                while (runner!=null && runner.next!=null //no chance of happening really
                        && runner.next.val<=cur.val) {
                    runner=runner.next;
                }
                last = cur.next;
                cur.next=runner.next;
                runner.next=cur;
                cur=last;
                last=runner.next;
            }
            else { //between min and Last
                ListNode runner=min;
                while (runner!=null && runner.next!=null //no chance of happening really
                        && runner.next.val<=cur.val) {
                    runner=runner.next;
                }
                last = cur.next;
                cur.next=runner.next;
                runner.next=cur;
                cur=last;
                last=runner.next;
            }
        }
        return ph.next;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {-1,5,3,4,0};
		ListNode ln=ListNode.fromArray(arr);
		ln.print();
		ln=sortList(ln);
		ln.print();
	}
}
