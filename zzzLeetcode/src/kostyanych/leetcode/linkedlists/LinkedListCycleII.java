package kostyanych.leetcode.linkedlists;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
 *
 * There is a cycle in a linked list if there is some node in the list that can be reached again by continuously following the next pointer.
 * Internally, pos is used to denote the index of the node that tail's next pointer is connected to. Note that pos is not passed as a parameter.
 *
 * Notice that you should not modify the linked list.
 *
 * Follow up:
 *
 * Can you solve it using O(1) (i.e. constant) memory?
 *
 * Constraints:
 * 	The number of the nodes in the list is in the range [0, 104].
 * 	-10^5 <= Node.val <= 10^5
 * 	pos is -1 or a valid index in the linked-list.
 *
 * Soultion:
 *  1. use slow (steps in 1 increment) and fast (steps in 2 increments) pointers.
 *  2. if fast pointer reaches NULL, then no cycles
 *  3. if there is a cycle, both pointers will enter it at some points.
 *  4. when in cycle fast pointer will definitely catch up with the slow pointer (its relative speed is 1 against relative slow speed of 0)
 *  5. assume the cycle starts at position S, it's cycle length is L
 *  6. when fast pointer and slow pointer first meet (!!except head!!), assume they meet T nodes after S
 *  7. the total steps of Slow Pointer is
 *    T_slow = S + T                        (1)
 *  8. the total steps of Fast Pointer is
 *     2*T_slow = S + T +n*L (where n>=1)   (2)
 *  9. (2)-(1) =>  T_slow = n*L,
 * 10. then from (1) => n*L = S + T => S = n*L - T
 * 		which means we lack S (+n*L) steps to reach point S from the current meeting point
 * 11. so, if we move the fast pointer BY ONE NOW from the meeting point and the slow pointer from the head, till they meet again.
     * So fromt node(x+t), we can run k cycle to get x.
 *
 */
public class LinkedListCycleII {

	static private ListNode detectCycle(ListNode head) {
		if (head==null || head.next==null)
			return null;

		ListNode slow = head.next;
		ListNode fast = head.next.next;

		while (true) {
			//if fast reaches null, then no cycles
			if (fast==null || fast.next==null)
				return null;
			if (fast==slow)
				break;
			slow=slow.next;
			fast=fast.next.next;
		}

		slow=head;
		while (slow!=fast) {
			slow=slow.next;
			fast=fast.next;
		}
		return slow;
    }

	 public static void testMe(boolean isNeeded) {
			if (!isNeeded)
				return;
			ListNode head = new ListNode(3);
	    	ListNode ln = new ListNode(2);
	    	head.next=ln;
	    	ListNode ln2 = new ListNode(0);
	    	ln.next=ln2;
	    	ln=ln2;
	    	ln2 = new ListNode(-4);
	    	ln.next=ln2;
	    	ln2.next=head.next;
	    	System.out.println(detectCycle(head));
	    }

}
