package kostyanych.leetcode.linkedlists;

/**
 * Design a queue that supports push and pop operations in the front, middle, and back.
 *
 * Implement the FrontMiddleBack class:
 * 		FrontMiddleBack() Initializes the queue.
 * 		void pushFront(int val) Adds val to the front of the queue.
 * 		void pushMiddle(int val) Adds val to the middle of the queue.
 * 		void pushBack(int val) Adds val to the back of the queue.
 * 		int popFront() Removes the front element of the queue and returns it. If the queue is empty, return -1.
 * 		int popMiddle() Removes the middle element of the queue and returns it. If the queue is empty, return -1.
 * 		int popBack() Removes the back element of the queue and returns it. If the queue is empty, return -1.
 *
 *  Notice that when there are two middle position choices, the operation is performed on the frontmost middle position choice. For example:
 *  	Pushing 6 into the middle of [1, 2, 3, 4, 5] results in [1, 2, 6, 3, 4, 5].
 *  	Popping the middle from [1, 2, 3, 4, 5, 6] returns 3 and results in [1, 2, 4, 5, 6].
 *
 *  Constraints:
 *
 *  1 <= val <= 10^9
 *  At most 1000 calls will be made to pushFront, pushMiddle, pushBack, popFront, popMiddle, and popBack.
 *
 */
public class DesignFrontMiddleBackQueue {

	static class FrontMiddleBackQueue {

		private static class Node {
			int val;
			Node next;
			Node prev;

			public Node(int v, Node n, Node p) {
				val = v;
				next = n;
				prev = p;
			}

			public String toString() {
				return "<" + val + ">";
			}
		}

		private Node first = null;
		private Node premid = null;
		private Node last = null;
		private int size = 0;

		public FrontMiddleBackQueue() {

		}

		public void pushFront(int val) {
			size++;
			if (first == null) {
				first = new Node(val, null, null);
				last = first;
				premid = first;
			}
			else {
				Node n = new Node(val, first, null);
				first.prev = n;
				first = n;
				if (size % 2 != 0)
					premid = premid.prev;
			}
		}

		public void pushMiddle(int val) {
			size++;
			if (first == null) {
				first = new Node(val, null, null);
				last = first;
				premid = first;
			}
			else {
				Node n = new Node(val, premid, premid.prev);
				if (premid.prev != null)
					premid.prev.next = n;
				premid.prev = n;
				if (size % 2 != 0)
					premid = n;
				if (n.next == first)
					first = n;
			}
		}

		public void pushBack(int val) {
			size++;
			if (first == null) {
				first = new Node(val, null, null);
				last = first;
				premid = first;
			}
			else {
				Node n = new Node(val, null, last);
				last.next = n;
				last = n;
				if (size % 2 == 0)
					premid = premid.next;
			}
		}

		public int popFront() {
			if (first == null)
				return -1;
			size--;
			int res = first.val;
			if (size == 0) {
				first = null;
				last = null;
				premid = null;
			}
			else {
				first = first.next;
				if (first != null)
					first.prev = null;
				if (size == 1) {
					last = first;
					premid = first;
				}
				else if (size % 2 == 0)
					premid = premid.next;
			}
			return res;
		}

		public int popMiddle() {
			if (first == null)
				return -1;
			size--;
			if (size == 0) {
				int res = first.val;
				first = null;
				last = null;
				premid = null;
				return res;
			}

			if (size % 2 != 0) {
				Node p = premid.prev;
				int res = p.val;
				if (p.prev == null) {
					first = p.next;
					first.prev = null;
					premid = first;
				}
				else {
					p.prev.next = p.next;
					p.next.prev = p.prev;
					premid = p.next;
				}
				return res;
			}
			else {
				int res = premid.val;
				premid.prev.next = premid.next;
				premid.next.prev = premid.prev;
				premid = premid.next;
				return res;
			}
		}

		public int popBack() {
			if (first == null)
				return -1;
			size--;
			int res = last.val;
			if (size == 0) {
				first = null;
				last = null;
				premid = null;
			}
			else {
				last = last.prev;
				if (last != null)
					last.next = null;
				if (size == 1) {
					first = last;
					premid = last;
				}
				else if (size % 2 != 0)
					premid = premid.prev;
			}
			return res;
		}

		public void print() {
			if (size == 0) {
				System.out.println("[]");
				return;
			}
			System.out.print("[");
			Node b = first;
			while (b != null) {
				System.out.print(b.val + ",");
				b = b.next;
			}
			System.out.println("]");
			System.out.println("{" + first.val + " - " + premid.val + " - " + last.val + "}");
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		FrontMiddleBackQueue q = new FrontMiddleBackQueue();
		System.out.println(q.popMiddle());
//q.print();
		q.pushMiddle(5422);
//q.print();
		q.pushMiddle(532228);
//q.print();
		System.out.println(q.popBack());
//q.print();
		q.pushMiddle(206486);
//q.print();
		q.pushBack(351927);
//q.print();
		System.out.println(q.popFront());
		System.out.println(q.popFront());
		q.pushMiddle(73577);
		q.pushMiddle(785914);
		q.pushMiddle(765630);
		System.out.println(q.popMiddle());
		q.pushMiddle(208060);
		System.out.println(q.popMiddle());
		q.pushMiddle(592866);

		/*
		 * FrontMiddleBackQueue q= new FrontMiddleBackQueue(); q.print(); //
		 * q.pushFront(1); // q.print(); // q.popBack(); // q.print(); //
		 * q.pushMiddle(1); // q.print(); // q.popFront(); // q.print(); //
		 * q.pushBack(1); // q.print(); // q.popMiddle(); // q.print();
		 *
		 * q.pushFront(3); q.print(); q.pushFront(1); q.print(); q.pushBack(4);
		 * q.print(); q.pushMiddle(2); q.print();
		 *
		 * System.out.println(q.popMiddle()); q.print();
		 * System.out.println(q.popMiddle()); q.print();
		 * System.out.println(q.popMiddle()); q.print();
		 * System.out.println(q.popMiddle()); q.print();
		 *
		 * System.out.println(q.popFront()); q.print(); System.out.println(q.popBack());
		 * q.print(); System.out.println(q.popMiddle()); q.print();
		 */
	}
}
