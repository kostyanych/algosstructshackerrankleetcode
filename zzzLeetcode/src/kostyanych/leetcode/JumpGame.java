package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given an array of non-negative integers, you are initially positioned at the first index of the array.
 *
 * Each element in the array represents your maximum jump length at that position.
 *
 * Determine if you are able to reach the last index.
 *
 * INPORTANT!!! We only need to reach the last index, not jump out of the array!
 * So, arr[1] is automatically true, and for other lengths we need to reach index len-1
 */
public class JumpGame {

	/**
	 * For every index for 0 we calculate the max index that can be reached from it.
	 * And we only iterate array up to that index
	 * But on every iteration that max reachable index may be updated, depending on arr[index]
	 */
	private static boolean canJump(int[] nums) {
		int len=nums.length;
		if (len<2)
            return true;
		int maxInd=0;
		for (int i=0; i<=maxInd; i++) {
			int curMax=i+nums[i];
			if (curMax>maxInd)
				maxInd=curMax;
			if (maxInd>=len-1)
				return true;
		}
		return false;
	}

//
//
//        int nextIndex=0;
//        int cur=0;
//        while (true) {
//            int testInd=0;
//            if (cur+nums[cur]>=len-1)
//            	return true;
//
//            for (int i=cur+1;i<=cur+nums[cur];i++) {
//                if (testInd<=i+nums[i]) {
//					testInd=i+nums[i];
//					nextIndex=i;
//				}
//            }
//            if (testInd<=cur+nums[cur])
//                return false;
//            if (testInd>=len-1)
//                return true;
//            cur=nextIndex;
//        }
//    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {2,3,1,1,4};
		System.out.println(Arrays.toString(arr));
		System.out.println(canJump(arr));

		arr = new int[] {3,2,1,0,4};
		System.out.println(canJump(arr));

		arr = new int[] {1,1,0,1};
		System.out.println(canJump(arr));
	}
}
