package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Alice and Bob take turns playing a game, with Alice starting first.
 *
 * There are n stones in a pile. On each player's turn, they can remove a stone from the pile and receive points based on the stone's value.
 * Alice and Bob may value the stones differently.
 *
 * You are given two integer arrays of length n, aliceValues and bobValues.
 * Each aliceValues[i] and bobValues[i] represents how Alice and Bob, respectively, value the ith stone.
 *
 * The winner is the person with the most points after all the stones are chosen.
 * If both players have the same amount of points, the game results in a draw. Both players will play optimally.
 *
 * Determine the result of the game, and:
 * 		If Alice wins, return 1.
 * 		If Bob wins, return -1.
 * 		If the game results in a draw, return 0.
 *
 * n == aliceValues.length == bobValues.length
 * 1 <= n <= 10^5
 * 1 <= aliceValues[i], bobValues[i] <= 100
 *
 * I can see no proof of that, but apparently optimal way of playing that is that each player takes the stone that has the max sum of values for both players:
 *  max(aliceValues[i]+bobValues[i]).
 * Probably because taking value from the opponent (by taking the stone with that value) is somehow adding this value to yourself;
 */
public class StoneGameVI {

	static private int stoneGameVI(int[] aliceValues, int[] bobValues) {
		int len=aliceValues.length;
		List<Integer> lst=new ArrayList<>();
		for (int i=0;i<len;i++) {
			lst.add(i);
		}
		lst.sort((i1,i2)->Integer.compare(bobValues[i2]+aliceValues[i2], bobValues[i1]+aliceValues[i1]));
		boolean alice=true;
		int asum=0;
		int bsum=0;
		for (int i=0;i<len;i++) {
			if (alice)
				asum+=aliceValues[lst.get(i)];
			else
				bsum+=bobValues[lst.get(i)];
			alice=!alice;
		}
		if (asum>bsum)
			return 1;
		if (asum==bsum)
			return 0;
		return -1;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] aliceValues = {1,2};
		int[] bobValues = {3,1};
		System.out.println(stoneGameVI(aliceValues, bobValues));
	}

}
