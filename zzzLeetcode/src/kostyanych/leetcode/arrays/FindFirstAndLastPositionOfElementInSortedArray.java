package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.
 *
 * Your algorithm's runtime complexity must be in the order of O(log n).
 *
 * If the target is not found in the array, return [-1, -1].
 *
 */
public class FindFirstAndLastPositionOfElementInSortedArray {

	private static int[] searchRange(int[] nums, int target) {
		int[] res = { -1, -1 };
		int len = nums.length;
		if (len < 1)
			return res;

		int l = 0;
		int r = len - 1;
		// find range start
		int leftInd = -1;
		int firstTarget = -1;
		int upToR = -1;
		while (l <= r) {
			int mid = l + (r - l) / 2;
			if (target < nums[mid])
				r = mid - 1;
			else if (target > nums[mid])
				l = mid + 1;
			else {
				if (firstTarget < 0) {
					firstTarget = mid;
					upToR = r;
				}
				leftInd = mid;
				r = mid - 1;
			}
		}
		res[0] = leftInd;

		// find range end
		l = firstTarget;
		r = upToR;
		int rightInd = -1;
		while (l <= r) {
			int mid = l + (r - l) / 2;
			if (target < nums[mid])
				r = mid - 1;
			else if (target > nums[mid])
				l = mid + 1;
			else {
				rightInd = mid;
				l = mid + 1;
			}
		}
		res[1] = rightInd;
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {5,7,7,8,8,10};
		int target = 6;
		System.out.println(Arrays.toString(searchRange(arr,target)));
	}
}
