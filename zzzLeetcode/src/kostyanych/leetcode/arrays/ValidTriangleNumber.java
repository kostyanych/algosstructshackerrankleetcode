package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given an integer array nums, return the number of triplets chosen from the array that can make triangles if we take them as side lengths of a triangle.
 *
 * Every side of a triangle is less than sum of other sides and larger than difference between others
 */
public class ValidTriangleNumber {

	static private int triangleNumber(int[] nums) {
		int res = 0;
		int n=nums.length;
        Arrays.sort(nums);
        for (int i = 0; i < n - 2; i++) {
        	if (nums[i]==0)
        		continue;

        	for (int j = i + 1; j < n - 1; j++) {
        		if (nums[j]==0)
        			continue;
        		int sum=nums[i]+nums[j];
        		for (int k = j+1; k<n; k++ ) {
        			if (nums[k]==0)
        				continue;
        			if (nums[k]>=sum)
        				break;
        			res++;
        		}
            }
        }
        return res;
    }

	static private int triangleNumberFaster(int[] nums) {
        int count = 0;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
        	if (nums[i]==0)
        		continue;
        	int k = i + 2;
            for (int j = i + 1; j < nums.length - 1; j++) {
                while (k < nums.length && nums[i] + nums[j] > nums[k])
                    k++;
                count += k - j - 1;
            }
        }
        return count;
    }



	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] nums={4,2,3,4};
		System.out.println(triangleNumber(nums));
	}

}
