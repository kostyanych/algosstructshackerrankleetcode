package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given an array nums and a value val, remove all instances of that value in-place and return the new length.
 *
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
 *
 * The order of elements can be changed. It doesn't matter what you leave beyond the new length.
 *
 * Example 1:
 * Given nums = [3,2,2,3], val = 3,
 * Your function should return length = 2
 */
public class RemoveElement {

	private static int removeElement(int[] nums, int val) {
		int len = nums.length;
		if (len<1)
			return len;

		int slow=0;
		int fast=0;
		int res=0;
		//for (int i=0;i<len;i++) {
		while (fast<len) {
			if (nums[fast]==val) {
				fast++;
				continue;
			}
			if (fast!=slow)
				nums[slow]=nums[fast];
			slow++;
			fast++;
			res++;
		}
		return res;
    }

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 int[] arr = {3,2,2,3};
		 int val = 3;
		 System.out.println(removeElement(arr, val)+": "+Arrays.toString(arr));
	 }

}
