package kostyanych.leetcode.arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an integer array of size n, find all elements that appear more than int(n/3) times.
 *
 * Note: The algorithm should run in linear time and in O(1) space.
 *
 * There can be no more than 2 values that appear more than n/3 times.
 *
 * We will use modified Boyer Moore Voting algorithm (changed to handle 2 major values)
 */
public class MajorityElementII {

	private static List<Integer> majorityElement(int[] nums) {
		List<Integer> res = new ArrayList<>();
		int len=nums.length;
		if (len<1)
			return res;

		int count1=0;
		int count2=0;
//it's really not important what we initialize pr1 and pre2 with, 'cause the initial counters are zeroed
		int pre1=0;
		int pre2=0;

		for (int i=0;i<len;i++) {
			if (nums[i]==pre1)
				count1++;
			else if (nums[i]==pre2)
				count2++;
			else {
				if (count1==0) {
					pre1=nums[i];
					count1=1;
				}
				else if (count2==0) {
					pre2=nums[i];
					count2=1;
				}
				else {
					count1--;
					count2--;
				}
			}
		}

		//now check if those numbers are really in majority and not just the last elements
		count1=0;
		count2=0;
		for (int n : nums) {
			if (n==pre1)
				count1++;
			else if (n==pre2)
				count2++;
		}

		if (count1>len/3)
			res.add(pre1);
		if (count2>len/3)
			res.add(pre2);
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {2,2,9,3,9,3,9,3,9,3,9,3,9,3,9,3,9};
		System.out.println(majorityElement(arr));
	}

}
