package kostyanych.leetcode.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a matrix and a target, return the number of non-empty submatrices that sum to target.
 *
 * A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x <= x2 and y1 <= y <= y2.
 *
 * Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if they have some coordinate that is different: for example, if x1 != x1'.
 *
 * Constraints:
 *
 * 1 <= matrix.length <= 100
 * 1 <= matrix[0].length <= 100
 * -1000 <= matrix[i] <= 1000
 * -10^8 <= target <= 10^8
 *
 * SOLUTION: use a prefix sum for columns OR rows
 *
 */
public class NumberOfSubmatricesThatSumToTarget {

	static private int numSubmatrixSumTarget(int[][] matrix, int target) {
        int rows = matrix.length;
        int cols = matrix[0].length;
        int res = 0;
        Map<Integer, Integer> memo = new HashMap<>();

        for (int[] row : matrix) {
            for (int c=1;c<cols;c++) {
                row[c] += row[c-1];
            }
        }

        for (int c = 0; c < cols; c++) {
            for (int cc = c; cc < cols; cc++) {
                memo.clear();
                memo.put(0,1);
                int sum = 0;
                for (int r = 0; r < rows; r++) {
                    sum += matrix[r][cc] - (c > 0 ? matrix[r][c-1] : 0);
                    res += memo.getOrDefault(sum - target, 0);
                    memo.compute(sum, (k,v) -> v==null ? 1 : v+1);
                }

            }
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] m= {{1,1,1,0},{1,2,3,1},{1,4,5,1},{1,1,1,1}};
		System.out.println(numSubmatrixSumTarget(m,14));
	}

}
