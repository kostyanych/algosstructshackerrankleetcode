package kostyanych.leetcode.arrays;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * You are given an array of positive integers nums and want to erase a subarray containing unique elements.
 * The score you get by erasing the subarray is equal to the sum of its elements.
 *
 * Return the maximum score you can get by erasing exactly one subarray.
 *
 * An array b is called to be a subarray of a if it forms a contiguous subsequence of a, that is, if it is equal to a[l],a[l+1],...,a[r] for some (l,r).
 *
 * Constraints:
 *
 * 1 <= nums.length <= 10^5
 * 1 <= nums[i] <= 10^4
 *
 */
public class MaximumErasureValue {

	static private int maximumUniqueSubarray(int[] nums) {
        int len = nums.length;
        if (len==1)
            return nums[0];

        Set<Integer> set = new HashSet<>();
        int prevStart=0;
        int cur=0;
        int max=0;
        for (int i=0;i<len;i++) {
            int next=nums[i];
            if (set.contains(next)) {
            	for (;prevStart<i;prevStart++) {
                    int prev=nums[prevStart];
                    cur-=prev;
                    set.remove(prev);
                    if (prev==next)
                        break;
                }
                prevStart++;
            }
            cur+=next;
            set.add(next);
            max=Math.max(max,cur);
        }
//        max=Math.max(max,cur);
        return max;
    }

	private static int maximumUniqueSubarraySlow(int[] nums) {
        int len = nums.length;
        if (len==1)
            return nums[0];
        Map<Integer,Integer> map = new HashMap<>();
        Set<Integer> set = new HashSet<>();
        int prevStart=0;
        int cur=0;
        int max=0;
        for (int i=0;i<len;i++) {
            int next=nums[i];
            if (!set.contains(next)) {
                cur+=next;
                set.add(next);
                map.put(next,i);
                continue;
            }
            max=Math.max(max,cur);
            int prevInd=map.get(next);
            for (int j=prevStart;j<=prevInd;j++) {
                cur-=nums[j];
                map.remove(nums[j]);
                set.remove(nums[j]);
            }
            prevStart=prevInd+1;
            cur+=next;
            set.add(next);
            map.put(next,i);
        }
        max=Math.max(max,cur);
        return max;
    }


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {4,2,4,5,6};
		//int[] arr = {449,154,934,526,429,732,784,909,884,805,635,660,742,209,742,272,669,449,766,904,698,434,280,332,876,200,333,464,12,437,269,355,622,903,262,691,768,894,929,628,867,844,208,384,644,511,908,792,56,872,275,598,633,502,894,999,788,394,309,950,159,178,403,110,670,234,119,953,267,634,330,410,137,805,317,470,563,900,545,308,531,428,526,593,638,651,320,874,810,666,180,521,452,131,201,915,502,765,17,577,821,731,925,953,111,305,705,162,994,425,17,140,700,475,772,385,922,159,840,367,276,635,696,70,744,804,63,448,435,242,507,764,373,314,140,825,34,383,151,602,745};
		System.out.println(maximumUniqueSubarray(arr));
	}
}
