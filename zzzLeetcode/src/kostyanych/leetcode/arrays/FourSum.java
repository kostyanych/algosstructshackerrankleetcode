package kostyanych.leetcode.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FourSum {

	private static List<List<Integer>> fourSum(int[] nums, int target) {
		List<List<Integer>> res = new ArrayList<>();
		Arrays.sort(nums);

		int len = nums.length;
		int maxAdd = 1 + target / 4;

		for (int i = 0; i < len - 3 && nums[i] < maxAdd; i++) {
			if (i > 0 && nums[i] == nums[i - 1]) // nothing new here
				continue;

			for (int j = i + 1; j < len - 2 && (nums[i] + nums[j] + nums[j + 1] + nums[j + 2]) <= target; j++) {
				if (j > i + 1 && nums[j] == nums[j - 1]) // nothing new here
					continue;

				// While parameters
				int left = j + 1;
				int right = len - 1;
				while (left < right) {
					int sum = nums[i] + nums[j] + nums[left] + nums[right];
					if (sum > target) {
						right--;
						while (nums[right] == nums[right + 1] && left < right)
							right--;
					}
					else if (sum < target) {
						left++;
						while (nums[left] == nums[left - 1] && left < right)
							left++;
					}
					else { // got result!
						res.add(Arrays.asList(nums[i], nums[j], nums[left], nums[right]));
						right--;
						left++;
						while (nums[right] == nums[right + 1] && left < right)
							right--;
						while (nums[left] == nums[left - 1] && left < right)
							left++;
					}
				}
			}
		}

		return res;
	}

	private static void printRes(List<List<Integer>> res) {
		System.out.println("----------------");
		for (List<Integer> lst : res) {
			System.out.println(Arrays.toString(lst.toArray()));
		}
		System.out.println("----------------");
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = { -3, -1, 0, 2, 4, 5 };
		printRes(fourSum(arr, 1));
	}
}
