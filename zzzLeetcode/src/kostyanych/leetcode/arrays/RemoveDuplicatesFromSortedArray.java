package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given a sorted array nums, remove the duplicates in-place such that each
 * element appear only once and return the new length.
 *
 * Do not allocate extra space for another array, you must do this by modifying
 * the input array in-place with O(1) extra memory.
 *
 */
public class RemoveDuplicatesFromSortedArray {

	private static int removeDuplicates(int[] nums) {
		int len = nums.length;
		if (len < 2)
			return len;

		int slow = 1;
		int fast = 1;
		int res = 1;
		while (fast < len) {
			if (nums[fast] == nums[slow-1]) {
				fast++;
				continue;
			}
			if (fast != slow)
				nums[slow] = nums[fast];
			slow++;
			fast++;
			res++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {1,1,2};
		System.out.println(removeDuplicates(arr) + ": " + Arrays.toString(arr));
	}

}
