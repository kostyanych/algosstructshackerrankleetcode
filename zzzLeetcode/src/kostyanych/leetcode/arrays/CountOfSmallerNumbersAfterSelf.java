package kostyanych.leetcode.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * You are given an integer array nums and you have to return a new counts array.
 * The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].
 *
 * Example 1:
 * Input: nums = [5,2,6,1]
 * Output: [2,1,1,0]
 *
 * Constraints:
 * 1 <= nums.length <= 10^5
 * -10^4 <= nums[i] <= 10^4
 *
 * SOLUTION:
 * The simple solution is to go from the end of nums and keep the SORTED list of the processed nums. Then we can use bianry search to find where the new number should go in the list
 * 		and thus - how many nums smaller that it are already there.
 *
 * С ФЕНВИКОМ РАЗОБРАТЬСЯ!!!!!
 * We could use a data structure such as Fenwick Tree or Segment Tree along with Offline Queries to tackle these kinds of problems.
 * Since Fenwick Tree is easier to implement, we would discuss the solution using Fenwick Tree instead of Segment Tree, but the same results could be acheived using Segment Tree also.
 * The main steps involved in the algorithm are discussed below :
 * 	1. We would be creating a Fenwick Tree that would store the count of the elements in a perticular segment
 * 		(obviously, since it's a Fenwick tree, the segment would be from [0 - r], where r is the current index.
 * 		Had this been a Segment Tree, the segment would be some [l - r], where l would be the left and r would be the right boundary of the segment).
 * 		This is very much similar to the frequency array which we create.
 *  2. Now, if let's say we encounter an element, say 24, we would increase the count of 24 in our fenwick tree by 1.
 *  3. Now, when we see a new element, say 30, we would first find how many elements have we seen in the range [0 - 29] till now,
 *  		which we could easily get from our fenwick tree, by getting the value at fen[29], since fen[29] would store the total count of elements from [0 - 29].
 *  4. We will do the above steps from the end of the array, so while querying, we are getting the count of elemnts in the right of the array.
 *  		And after querying, we would increase the current element's frequency and again move to the left and repeat the same process.
 *
 *  Note: Since the constraints in the question say the range of the elements would be : -10^4 <= nums[i] <= 10^4, we can't directly keep track of their frequency
 *  	(as their are no -ve indices to keep count of -ve numbers).
 *  	So we need to remap them to non-negative numbers and that also in such a way that they don't loose their relative odering if they were to be sorted,
 *  	i.e. if a number was 5th smallest number before remapping, it should remain the 5th smallest number after remapping.
 *
 *  To do this, we would first sort the numbers, and then remap them starting from smallest one to 0, then the next larger one to 1, then next larger to 2 and so on.
 *
 */
public class CountOfSmallerNumbersAfterSelf {

	static private List<Integer> countSmaller(int[] nums) {
		//return countSmallerBinary(nums);
		return countSmallerFenwick(nums);
    }

	static private int[] fentree;
	static private List<Integer> countSmallerFenwick(int[] nums) {
        int n=nums.length;
        int[] resarr=new int[n];

        // remap the numbers to whole numbers range
        Map<Integer, Integer> map = new TreeMap<>();
        for(int i : nums) {
            map.put(i, 0);
        }

        int val = 0;
        for(Map.Entry<Integer, Integer> entry : map.entrySet()) {
            map.put(entry.getKey(), val);
            val++;
        }

        int sortedIndices[] = new int[n];
        for(int i =0 ; i < n; i++) {
        	sortedIndices[i] = map.get(nums[i]);
        }

        // making fenwick tree
        fentree = new int[n];

        for(int i = n - 1; i >= 0; i--) {
            resarr[i] = find(sortedIndices[i] - 1);
            update(sortedIndices[i], 1);
        }

        List<Integer> res = new ArrayList<>();
        for (int i : resarr) {
            res.add(Integer.valueOf(i));
        }

        return res;
    }

	static private int find(int x) {
        if(x < 0)
        	return 0;
        int i = x + 1;
        int sum = 0;
        while (i > 0) {
            sum += fentree[i - 1];
            i -= (i & (-i));
        }
        return sum;
    }

	static void update(int x, int val) {
        int i = x + 1;
        int n = fentree.length;
        while (i <= n) {
        	fentree[i - 1] += val;
            i += (i & (-i));
        }
    }

	static private List<Integer> countSmallerBinary(int[] nums) {
        int n=nums.length;

        int[] resarr=new int[n];
        resarr[n-1]=0;
        List<Integer> lst = new ArrayList<>(n);
        lst.add(nums[n-1]);
        for (int i=n-2;i>=0;i--) {
            resarr[i]=binaryHelper(nums[i], 0, lst.size(), lst);
        }

        List<Integer> res = new ArrayList<>();
        for (int i : resarr) {
            res.add(Integer.valueOf(i));
        }

        return res;
    }

   static private int binaryHelper(int val, int low, int high, List<Integer> list) {
       // Binary search
       int mid = low + (high - low)/2;
       while (low<high) {
    	   if (val <= list.get(mid))
    		   high=mid;
    	   else
    		   low=mid+1;
    	   mid = low + (high - low)/2;
       }
       list.add(mid, val);
       return mid;
   }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {5,2,6,1};
		System.out.println(Arrays.toString(arr));
		System.out.println(countSmaller(arr));
	}

}
