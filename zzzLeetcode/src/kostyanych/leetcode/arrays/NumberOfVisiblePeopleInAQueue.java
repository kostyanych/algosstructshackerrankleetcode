package kostyanych.leetcode.arrays;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Stack;

/**
 * There are n people standing in a queue, and they numbered from 0 to n - 1 in left to right order.
 * You are given an array heights of distinct integers where heights[i] represents the height of the ith person.
 *
 * A person can see another person to their right in the queue if everybody in between is shorter than both of them.
 * More formally, the ith person can see the jth person if i < j and min(heights[i], heights[j]) > max(heights[i+1], heights[i+2], ..., heights[j-1]).
 *
 * Return an array answer of length n where answer[i] is the number of people the ith person can see to their right in the queue.
 *
 * Constraints:
 * 	n == heights.length
 * 	1 <= n <= 10^5
 *  1 <= heights[i] <= 10^5
 *  All the values of heights are unique.
 *
 *  SOLUTION:
 *  1. last person always will see 0 people
 *  2. the person i only sees the monotonically increasing other people starting from i+1 and ending at the first person bigger than himself
 *  3. consider 5 3 4 2 1 7:
 *  2a. 1 will only see 7
 *  2b. 2 will see 1 and 7
 *  2c. 4 will see 2 and 7 (1 is obscured by 2)
 *  2d. 3 will see 4 (2, 1 and 7 are obscured by 4)
 *  2e. 5 will see 3, 4 and 7 (2 and 1 are obscured by 4)
 *  3.  thus we only need to keep the descending stack of value going from right to left
 *  4.  when we get a person who's less than top of the stack, he can only see 1. and we add him to the stack
 *  5.  if he's >= than top of the stack we pop people off till we come to an empty stack or to 4.
 *  5a. number of popped people + 1 if the stack is not empty is the number of people that ith person can see
 *
 */
public class NumberOfVisiblePeopleInAQueue {

	static private int[] canSeePersonsCount(int[] heights) {
		int len = heights.length;
		int[] res = new int[len];
		res[len-1]=0;
		ArrayDeque<Integer> st = new ArrayDeque<>();

		for (int i = len-1; i >= 0; i--) {
			while (!st.isEmpty()) {
				if (heights[i]<st.peek())
					break;
				st.pop();
				res[i]++;
			}
			if (!st.isEmpty())
	        	res[i]++;
			st.push(heights[i]);
		}
	    return res;


//		st.push(new int[]{heights[len-1],len-1});
//
//		for (int i=len-2;i>=0;i--) {
//			while (!st.isEmpty()) {
//				int[] prev=st.peek();
//				if (prev[0]>heights[i])
//					break;
//				st.pop();
//			}
//			if (st.isEmpty())
//				res[i]=len-1-i;
//			else {
//				int[] prev=st.peek();
//				res[i]=prev[1]-i-1;
//				if (res[i]==0)
//					res[i]=1;
//			}
//			st.push(new int[]{heights[i],i});
//		}
//
//		return res;


    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {5, 3, 4, 2, 1, 7};
//		int[] arr = {10,6,8,5,11,9};
		System.out.println(Arrays.toString(arr));
		System.out.println(Arrays.toString(canSeePersonsCount(arr)));

	}

}
