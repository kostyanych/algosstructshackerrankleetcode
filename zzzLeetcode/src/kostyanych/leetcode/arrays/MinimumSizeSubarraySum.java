package kostyanych.leetcode.arrays;

import java.util.TreeMap;

/**
 * Given an array of n positive integers and a positive integer s,
 * find the minimal length of a contiguous subarray of which the sum ≥ s. If there isn't one, return 0 instead.
 *
 */
public class MinimumSizeSubarraySum {

	static private int minSubArrayLen(int s, int[] nums) {
		int len=nums.length;
		int min=2*len;
		int left = 0;
		int sum = 0;
		for (int i = 0; i < len; i++) {
			sum += nums[i];
			while (sum >= s) {
				min = Math.min(min, i + 1 - left);
				sum -= nums[left++];
			}
		}
		if (min>len)
			return 0;
		return min;
    }

	static private int minSubArrayLenSlow(int s, int[] nums) {
		TreeMap<Integer,Integer> map = new TreeMap<>();
		int sum=0;
		int len=nums.length;
		int min=2*len;
		for (int i=0; i<len; i++) {
			sum+=nums[i];
			map.put(sum,i);
			if (sum>=s) {
				Integer key=map.floorKey(sum-s);
				if (key!=null)
					min=Math.min(min,i-map.get(key));
				else
					min=Math.min(min,i+1);
			}
		}
		if (min>len)
			return 0;
		return min;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {1,2,3,4,5};
		System.out.println(minSubArrayLen(11, arr));
	}
}
