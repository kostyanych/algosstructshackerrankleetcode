package kostyanych.leetcode.arrays;

import java.util.Arrays;

public class MedianOfTwoArrays {
	
	static private double findMedian(int[] shrt, int[] lng) {
		if (shrt.length>lng.length)
			return findMedian(lng, shrt);
		//0.
		int lenShort=shrt.length;
		int lenLong=lng.length;
		boolean even = ((lenShort+lenLong)%2)==0;
		int center=(lenShort+lenLong)/2;
		
		if (lenShort==0)
			return median(lng);
		if (lenLong==0)
			return median(shrt);
		
		//1. edge cases
		if (shrt[lenShort-1]<=lng[0]) { //the whole short arrays is not greater than long[0]
			if (!even)
				return lng[center-lenShort];
			else {
				if (center==lenShort)
					return ((double)shrt[lenShort-1]+lng[0])/2;
				else
					return ((double)lng[center-lenShort]+lng[center-lenShort-1])/2;
			}
		}
		else if (lng[lenLong-1]<=shrt[0]) { //the whole long arrays is not greater than short[0]
			if (!even)
				return lng[center];
			else {
				if (center>=lenLong)
					return ((double)lng[lenLong-1]+shrt[0])/2;
				else
					return ((double)lng[center]+lng[center-1])/2;
			}
			
		}

		//2. normal processing
		int start = 0;
		int end = lenShort;
		while (start<=end) {
			int posShort = (start+end)/2;
			int posLong = (lenShort+lenLong+1)/2 - posShort;
			
			int maxLeftShort = posShort==0?Integer.MIN_VALUE:shrt[posShort-1];
			int minRightShort = posShort==lenShort?Integer.MAX_VALUE:shrt[posShort];
			
			int maxLeftLong= posLong==0?Integer.MIN_VALUE : lng[posLong-1];
			int minRightLong= posLong==lenLong?Integer.MAX_VALUE : lng[posLong];
			
			if (maxLeftShort<=minRightLong && maxLeftLong<=minRightShort) { //division is ok!
				if (even)
					return (max(maxLeftLong,maxLeftShort)+min(minRightLong,minRightShort))/2;
				else
					return max(maxLeftLong,maxLeftShort);
			}
			if (maxLeftShort>minRightLong) //move to the left
				end=posShort-1;
			else
				start=posShort+1;
		}
		return -1.;
	}
	
	private static double median(int[] arr) {
		int len=arr.length;
		if (len%2!=0)
			return (double)arr[len/2];
		return ((double)arr[len/2]+arr[len/2 - 1])/2;
	}
	
	private static double min(int a, int b) {
		if (a<b)
			return (double)a;
		return (double)b;
	}

	private static double max(int a, int b) {
		if (a>b)
			return (double)a;
		return (double)b;
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr1 = {1, 3};
		//int[] arr2 = {2};
		int[] arr2 = {};
		printResults(arr1,arr2);
		arr1 = new int[] {1, 2};
		arr2 = new int[] {3, 4};
		printResults(arr1,arr2);
		arr1 = new int[] {1, 2};
		arr2 = new int[] {3, 4, 5};
		printResults(arr1,arr2);

		arr2 = new int[] {1, 2};
		arr1 = new int[] {3, 4};
		printResults(arr1,arr2);
		arr2 = new int[] {1, 2, 3};
		arr1 = new int[] {4, 5};
		printResults(arr1,arr2);
		
		arr1 = new int[] {-5, 3, 6, 12, 15};
        arr2 = new int[] {-12, -10, -6, -3, 4, 10};
		printResults(arr1,arr2);

		arr1 = new int[] {2, 3, 5, 8};
        arr2 = new int[] {10, 12, 14, 16, 18, 20};
		printResults(arr1,arr2);
	}
	
	private static void printResults(int[] arr1, int[] arr2) {
		System.out.println("-----------------");
		System.out.println(Arrays.toString(arr1));
		System.out.println(Arrays.toString(arr2));
		System.out.println(findMedian(arr1,arr2));
		System.out.println("-----------------");
	}
	
}
