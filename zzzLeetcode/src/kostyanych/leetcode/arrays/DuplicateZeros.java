package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given a fixed length array arr of integers, duplicate each occurrence of zero, shifting the remaining elements to the right.
 *
 * Note that elements beyond the length of the original array are not written.
 *
 * Do the above modifications to the input array in place, do not return anything from your function.
 */
public class DuplicateZeros {

	private static void duplicateZeros(int[] arr) {
		int len=arr.length;
		if (len<2)
			return;

		int firstZero=-1;
		int curInd;
		int farInd = -1;
		for (curInd=0;curInd<len;curInd++) {
			if (arr[curInd]==0) {
				if (firstZero<0)
					firstZero=curInd;
				farInd+=2;
			}
			else
				farInd++;
			if (farInd>=len-1)
				break;
		}
		System.out.println(""+curInd+", "+farInd+", "+firstZero);
		int[] arr2=Arrays.copyOf(arr, curInd);
		System.out.println(Arrays.toString(arr2));
		if (firstZero<0)
			return;
		for (int i=farInd;i>=0 && curInd>=firstZero;) {
			if (i>len-1) {
				if (arr[curInd]!=0) {
					System.out.println(""+curInd+", "+farInd+" sheeeeeeeeet");
					return;
				}
				i--;
				arr[i]=arr[curInd];
				i--;
				curInd--;
			}
			else {
				if (arr[curInd]!=0) {
					arr[i]=arr[curInd];
					i--;
					curInd--;
				}
				else {
					arr[i]=0;
					arr[i-1]=0;
					i-=2;
					curInd--;
				}
			}

		}
	}

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 int[] arr= {9, 9, 9, 4, 8, 0, 0, 3, 7, 0, 0, 0};
//		 int[] arr= {9,9,9,4,8,0,0,3,7,2,0,0,0,0,9,1,0,0,1,1,0,5,6,3,1,6,0,0,2,3,4,7,0,3,9,3,6,5,8,9,1,1,3,2,0,0,7,3,3,0,5,7,0,8,1,9,6,3,0,8,8,8,8,0,0,5,0,0,0,3,7,7,7,7,5,1,0,0,8,0,0};
		 System.out.println(Arrays.toString(arr));
		 duplicateZeros(arr);
		 System.out.println(Arrays.toString(arr));
	 }



}
