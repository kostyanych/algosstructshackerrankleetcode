package kostyanych.leetcode.arrays;

/**
 * Given a non-empty array nums containing only positive integers, find if the array
 * can be partitioned into two subsets such that the sum of elements in both subsets is equal.
 *
 * Constraints:
 * 1 <= nums.length <= 200
 * 1 <= nums[i] <= 100
 *
 */
public class PartitionEqualSubsetSum {

	static private int[] arr;
	static private Boolean[][] memo;

	static private boolean canPartition(int[] nums) {
		int len = nums.length;

		arr=nums;

		int sum=0;
		for (int n : nums)
			sum+=n;

		if (sum%2!=0)
			return false;

		int target = sum/2;

		memo = new Boolean[len][target + 1];
		return dfs(len - 1, target);
	}

	static private boolean dfs(int n, int target) {
		if (target == 0)
			return true;
		if (n == 0 || target < 0)
			return false;

		if (memo[n][target] != null)
			return memo[n][target];
		boolean result = dfs(n - 1, target - arr[n - 1]) ||  dfs(n - 1, target);
		memo[n][target] = result;
		return result;
	}

	static private boolean canPartitionSLOW(int[] nums) {
		int len = nums.length;

		arr=nums;

		int sum=0;
		for (int n : nums)
			sum+=n;

		if (sum%2!=0)
			return false;

		int target = sum/2;
		for (int i=0;i<len;i++) {
			if (process(i, target))
				return true;
		}
		return false;
    }

	static private boolean process(int start, int target) {
		int len = arr.length;
		if (start==len-1) {
			if (arr[start]==target)
				return true;
			return false;
		}

		int newtarget=target-arr[start];
		for (int i=start+1;i<len;i++) {
			if (process(i, newtarget))
				return true;
		}
		return false;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={1,3,4,4};
		System.out.println(canPartition(arr));
	}

}
