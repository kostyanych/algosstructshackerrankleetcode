package kostyanych.leetcode.arrays;

public class NumberOfSubArraysWithOddSum {

	private static final int MOD = 1000000007;

	private static int numOfSubarrays(int[] arr) {
        int n=arr.length;
		if (n<2) {
			if (arr[0]%2==1)
				return 1;
			return 0;
		}
		int[] sums=new int[n];
		int[] evenCount = new int[n];
		sums[0]=arr[0];
		if (arr[0]%2==0)
			evenCount[0]=1;
		for (int i=1;i<n;i++) {
			sums[i]=sums[i-1]+arr[i];
			if (sums[i]%2==0)
				evenCount[i]=evenCount[i-1]+1;
			else
				evenCount[i]=evenCount[i-1];
		}
		int res=0;
		if (arr[0]%2==1)
			res++;
		for (int i=1;i<n;i++) {
			if (sums[i]%2==1) {
				res=(res+1)%MOD;
				res=(res+evenCount[i-1]%MOD)%MOD;
			}
			else
				res=(res+(i-evenCount[i-1])%MOD)%MOD;
		}
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {1,3,5};
		System.out.println(numOfSubarrays(arr));
	}

}
