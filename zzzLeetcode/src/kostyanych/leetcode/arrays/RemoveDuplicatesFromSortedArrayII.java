package kostyanych.leetcode.arrays;

/**
 * Given a sorted array nums, remove the duplicates in-place such that
 * duplicates appeared at most twice and return the new length.
 *
 * Do not allocate extra space for another array, you must do this by modifying
 * the input array in-place with O(1) extra memory.
 */
public class RemoveDuplicatesFromSortedArrayII {

	private static int removeDuplicates(int[] nums) {
		int len = nums.length;
		if (len < 2)
			return len;
		int writeInd=1;
		int count=1;
		int prev=nums[0];
		for (int readInd=1;readInd<len;readInd++) {
			if (nums[readInd]!=prev) {//new num
				nums[writeInd]=nums[readInd];
				prev = nums[readInd];
				count = 1;
				writeInd++;
				continue;
			}
			if (count == 1) {//same num but only 2nd time
				nums[writeInd]=nums[readInd];
				count++;
				writeInd++;
				continue;
			}
			//same num more than 2 times
			readInd++;
			while(readInd<len) {
				if (nums[readInd]==prev) {
					readInd++;
					continue;
				}
				nums[writeInd]=nums[readInd];
				prev = nums[readInd];
				count = 1;
				writeInd++;
				break;
			}
		}
		return writeInd;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={0,0,1,1,1,1,2,3,3};
		int len=removeDuplicates(arr);
		printArrByLen(arr,len);
	}

	private static void printArrByLen(int[] arr, int len) {
		System.out.print("[");
		boolean needed=false;
		for (int i=0;i<len;i++) {
			if (needed)
				System.out.print(", ");
			else {
				System.out.print(" ");
				needed=true;
			}
			System.out.print(""+arr[i]);
		}
		System.out.println("]");
	}

}
