package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given the array nums consisting of n positive integers.
 * You computed the sum of all non-empty continous subarrays from the array and then sort them in non-decreasing order,
 * creating a new array of n * (n + 1) / 2 numbers.
 *
 * Return the sum of the numbers from index left to index right (indexed from 1), inclusive, in the new array.
 * Since the answer can be a huge number return it modulo 10^9 + 7.
 *
 * Constraints:
 * 1 <= nums.length <= 10^3
 * nums.length == n
 * 1 <= nums[i] <= 100
 * 1 <= left <= right <= n * (n + 1) / 2
 *
 */
public class RangeSumOfSortedSubarraySums {

	private static final int MOD = 1000000007;

    private static int rangeSum(int[] nums, int n, int left, int right) {
        if (n<2)
			 return nums[0];
		 int sumlen=n*(n+1)/2;
		 int[] arr=new int[sumlen];
		 arr[0]=nums[0];
		 for (int i = 1; i<n;i++) {
			 arr[i]=arr[i-1]+nums[i];
		 }
		 int ind=n;
		 for (int i=1;i<n;i++) {
			 for (int j=0;j<n-i;j++) {
				 arr[ind+j]=arr[i+j]-arr[i-1];
			 }
			 ind+=(n-i);
		 }
//		 System.out.println(Arrays.toString(arr));
		 Arrays.sort(arr);
		 int res=0;
		 for (int i=left-1;i<right;i++) {
			 res+=(arr[i]%MOD);
			 res%=MOD;
		 }
		 return res;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {1,2,3,4};
		System.out.println(rangeSum(arr, 4, 1, 10));
	}

}
