package kostyanych.leetcode.arrays;

import kostyanych.leetcode.helpers.Printer;

/**
 * Given a 2D integer matrix M representing the gray scale of an image,
 * you need to design a smoother to make the gray scale of each cell becomes the average gray scale (rounding down)
 * of all the 8 surrounding cells and itself. If a cell has less than 8 surrounding cells, then use as many as you can.
 *
 */
public class ImageSmoother {

	static private int[][] imageSmoother(int[][] M) {
		int rows = M.length;
		int cols = M[0].length;
		int[][] res = new int[rows][cols];
		for (int r=0;r<rows;r++) {
			for (int c=0;c<cols;c++) {
				res[r][c]=getAvg(M,r,c,rows,cols);
			}
		}
		return res;
	}

	static private int getAvg(int[][] m, int r, int c, int rows, int cols) {
		int res=m[r][c];
		int count=1;
		if (r>0) {
			res+=m[r-1][c];
			count++;
		}
		if (r<rows-1) {
			res+=m[r+1][c];
			count++;
		}
		if (c+1<cols) {
			res+=m[r][c+1];
			count++;
			if (r>0) {
				res+=m[r-1][c+1];
				count++;
			}
			if (r<rows-1) {
				res+=m[r+1][c+1];
				count++;
			}
		}
		if (c>0) {
			res+=m[r][c-1];
			count++;
			if (r>0) {
				res+=m[r-1][c-1];
				count++;
			}
			if (r<rows-1) {
				res+=m[r+1][c-1];
				count++;
			}
		}
		return res/count;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr= {{2,3,4},{5,6,7},{8,9,10},{11,12,13},{14,15,16}};
		Printer.print2DArray(arr,false);
		Printer.print2DArray(imageSmoother(arr),false);
	}
}
