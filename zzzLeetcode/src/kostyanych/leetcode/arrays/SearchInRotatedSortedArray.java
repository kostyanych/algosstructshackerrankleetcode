package kostyanych.leetcode.arrays;

/**
 * Suppose an array sorted in ascending order is rotated at some pivot unknown
 * to you beforehand. (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
 *
 * You are given a target value to search. If found in the array return its
 * index, otherwise return -1.
 *
 * You may assume no duplicate exists in the array.
 *
 * Your algorithm's runtime complexity must be in the order of O(log n).
 */
public class SearchInRotatedSortedArray {

	private static int search(int[] nums, int target) {
		int len = nums.length;
		int left = 0;
		int right = len-1;

		//edge cases (empty array, 1 element arr, simlpe case of not existence)
		if (len < 1)
			return -1;
		if (len == 1) {
			if (nums[0] == target)
				return 0;
			return -1;
		}
		if (target < nums[left] && target > nums[right])
			return -1;

		while (left <= right) {
			int center = (left + right)/2;
			int elLeft=nums[left];
			int elRight=nums[right];
			int elCenter=nums[center];

			if(elCenter == target)
				return center;

            if(elCenter < elLeft){
    			// if center element is less than the current left, then center is to the RIGHT of the pivot point (or the pivot point itself)
                if (target <= elRight && target > elCenter){
                    // target is on the right of center
                    left = center +1;
                }
                else right = center-1;
            }
            else if(elCenter > elRight){
                // if center element is greater than the current right, then center is to the LEFT of the pivot point
                if (target >= elLeft && target < elCenter){
                    // target is on the left of midElement
                    right = center -1;
                }
                else left = center +1;
            }
            else{
                // no rotation BETWEEN LEFT and RIGHT
                if (target > elCenter)
                	left = center + 1;
                else
                	right = center -1;
            }
		}
		return -1;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {4,5,6,7,0,1,2};
		int t = 8;
		System.out.println(search(arr, t));
		arr = new int[] {4,5,6,7,0,1,2};
		t = 0;
		System.out.println(search(arr, t));
		arr = new int[] {3,1};
		t = 3;
		System.out.println(search(arr, t));
	}
}
