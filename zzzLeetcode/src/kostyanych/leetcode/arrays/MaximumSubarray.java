package kostyanych.leetcode.arrays;

/**
 * Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
 *
 */
public class MaximumSubarray {

	private static int maxSubArray(int[] nums) {
        int len=nums.length;
        if (len==0)
            return 0;
        if (len<2)
            return nums[0];
        int curSum=nums[0];
        int max=curSum;
        for (int i=1;i<len;i++) {
            curSum=Math.max(nums[i],nums[i]+curSum);
            max=Math.max(max,curSum);
        }
        return max;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int arr[] = {-2,1,-3,4,-1,2,1,-5,4};
		System.out.println(maxSubArray(arr));
	}
}
