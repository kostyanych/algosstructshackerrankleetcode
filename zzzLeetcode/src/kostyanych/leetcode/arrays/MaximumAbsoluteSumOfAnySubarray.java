package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * You are given an integer array nums.
 * The absolute sum of a subarray [numsl, numsl+1, ..., numsr-1, numsr] is abs(numsl + numsl+1 + ... + numsr-1 + numsr).
 *
 * Return the maximum absolute sum of any (possibly empty) subarray of nums.
 *
 * Note that abs(x) is defined as follows:
 *
 * If x is a negative integer, then abs(x) = -x.
 * If x is a non-negative integer, then abs(x) = x.
 *
 * SOLUTION:
 * just use Kadane's algo twice: for positive maximum and for negative*(-1) maximum
 *
 */
public class MaximumAbsoluteSumOfAnySubarray {

	static private int maxAbsoluteSum(int[] nums) {
		int len = nums.length;
        int max = 0;
        int endingHere = 0;
        int negativeMax = 0;
        int negativeEndingHere = 0;

        for (int i = 0; i < len; i++) {
        	endingHere = endingHere + nums[i];
        	negativeEndingHere = negativeEndingHere +(-1*nums[i]);
        	max=Math.max(max, endingHere);
        	negativeMax=Math.max(negativeMax, negativeEndingHere);
        	if (endingHere<0)
        		endingHere=0;
        	if (negativeEndingHere<0)
        		negativeEndingHere=0;
        }
        return Math.max(max, negativeMax);
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] nums = {1,-3,2,3,-4};
		System.out.println(Arrays.toString(nums));
		System.out.println(maxAbsoluteSum(nums));
		nums = new int[]{2,-5,1,-4,3,-2};
		System.out.println(Arrays.toString(nums));
		System.out.println(maxAbsoluteSum(nums));
	}

}
