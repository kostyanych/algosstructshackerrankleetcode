package kostyanych.leetcode.arrays;

public class NumberOfSubArraysSizeAndAverage {

	private static int numOfSubarrays(int[] arr, int k, int threshold) {
		int len = arr.length;
		long prevSum = 0;
		double avg = -1;
		int res = 0;
		for (int i = 0; i < k; i++) {
			prevSum += arr[i];
		}
		long lt=threshold*1l*k;
//		avg = (double)prevSum / k;
//		if (avg >= threshold)
		if (prevSum >= lt)
			res++;

		for (int i = 1; i < len - k + 1; i++) {
			prevSum = prevSum - arr[i - 1] + arr[i + k - 1];
//			avg = (double)prevSum / k;
//			if (avg >= threshold)
			if (prevSum >= lt)
				res++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {2,2,2,2,5,5,5,8};
		int k = 3;
		int threshold = 4;
		System.out.println(numOfSubarrays(arr, k, threshold));
		
		arr = new int[] {1,1,1,1,1};
		k = 4;
		threshold = 1;
		System.out.println(numOfSubarrays(arr, k, threshold));
		
		arr = new int[] {11,13,17,23,29,31,7,5,2,3};
		k = 3;
		threshold = 5;
		System.out.println(numOfSubarrays(arr, k, threshold));
		
		arr = new int[] {7,7,7,7,7,7,7};
		k = 7;
		threshold = 7;
		System.out.println(numOfSubarrays(arr, k, threshold));		


		arr = new int[] {4,4,4,4};
		k = 4;
		threshold = 1;
		System.out.println(numOfSubarrays(arr, k, threshold));		

	}
}
