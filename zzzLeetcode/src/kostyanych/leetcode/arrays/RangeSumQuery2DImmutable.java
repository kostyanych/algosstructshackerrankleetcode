package kostyanych.leetcode.arrays;

/**
 * Given a 2D matrix matrix, handle multiple queries of the following type:
 * Calculate the sum of the elements of matrix inside the rectangle defined by its upper left corner (row1, col1) and lower right corner (row2, col2).
 *
 * Implement the NumMatrix class:
 * NumMatrix(int[][] matrix) Initializes the object with the integer matrix matrix.
 * int sumRegion(int row1, int col1, int row2, int col2) Returns the sum of the elements of matrix inside the rectangle defined by
 * its upper left corner (row1, col1) and lower right corner (row2, col2).
 *
 * Constraints:
 *
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 200
 * -10^5 <= matrix[i][j] <= 10^5
 * 0 <= row1 <= row2 < m
 * 0 <= col1 <= col2 < n
 * At most 10^4 calls will be made to sumRegion.
 */
public class RangeSumQuery2DImmutable {

	final private int[][] sum;
    final private int rows;
    final private int cols;

    public RangeSumQuery2DImmutable(int[][] matrix) {
        rows=matrix.length;
        cols=matrix[0].length;
        sum=new int[rows+1][cols+1];

        for (int r=0;r<rows;r++) {
            for (int c=0;c<cols;c++) {
                sum[r+1][c+1] = sum[r+1][c]+sum[r][c+1]-sum[r][c]+matrix[r][c];
            }
        }
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        int whole=sum[row2+1][col2+1];
        int left=sum[row2+1][col1];
        int upper=sum[row1][col2+1];
        int intersection=sum[row1][col1];
        int res = whole-left-upper+intersection;
        return res;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] mx={{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}};
		RangeSumQuery2DImmutable o = new RangeSumQuery2DImmutable(mx);
		System.out.println(o.sumRegion(2, 1, 4, 3));
		System.out.println(o.sumRegion(1, 1, 2, 2));
		System.out.println(o.sumRegion(1, 2, 2, 4));
    }
}
