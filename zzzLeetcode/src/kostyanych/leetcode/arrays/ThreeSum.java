package kostyanych.leetcode.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0?
 * Find all unique triplets in the array which gives the sum of zero.
 *
 * Note:
 *
 * The solution set must not contain duplicate triplets.
 *
 */
public class ThreeSum {

	private static Map<Integer,List<Integer>> indices = new HashMap<>();

	private static List<List<Integer>> threeSum(int[] nums) {
		List<List<Integer>> res= new ArrayList<>();
		int len=nums.length;
		Arrays.sort(nums);
		if (len<1 || nums[0]>0 || nums[len-1]<0)
			return res;

		for (int i=0;i<len;i++) {
			if ( i > 0 && nums[i] == nums[i-1])	//already checked that number
				continue;

			int left=i+1;
			int right=len-1;
			while (left<right) {
				int s = nums[i]+nums[left]+nums[right];
				if (s<0) {
					left++;
					while (nums[left]==nums[left-1] && left<right) //skipping the same number for the left
						left++;
				}
				else if (s>0) {
					right--;
					while (nums[right]==nums[right+1] && left<right) //skipping the same number for the left
						right--;

				}
				else { //got the 0 result!
					res.add(Arrays.asList(nums[i], nums[left], nums[right]));
					left++;
					right--;
					while (nums[left]==nums[left-1] && left<right) //skipping the same number for the left
						left++;
					while (nums[right]==nums[right+1] && left<right) //skipping the same number for the left
						right--;
				}
			}
		}
		return res;
	}


	private static List<List<Integer>> threeSumSlow(int[] nums) {
		indices.clear();
		Set<List<Integer>> set = new HashSet<>();
		int len=nums.length;
		for (int i=0;i<len;i++) {
			List<Integer> lst = indices.get(nums[i]);
			if (lst==null) {
				lst=new ArrayList<>();
				indices.put(nums[i],lst);
			}
			lst.add(i);
		}

		for (int i=0;i<len;i++) {
			int target=0-nums[i];
			for (int j = 0; j < len; j++) {
				if (j == i)
					continue;
				int compl = (target - nums[j]);
				if (indices.containsKey(compl)) {
					for (Integer ind : indices.get(compl)) {
						if (ind==i || ind==j)
							continue;
						List<Integer> buf = new ArrayList<>();
						buf.add(nums[i]);
						buf.add(nums[j]);
						buf.add(nums[ind]);
						Collections.sort(buf);
						set.add(buf);
					}
				}
			}
		}
		List<List<Integer>> res=new ArrayList<>();
		res.addAll(set);
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={-1, 0, 1, 2, -1, -4};
//		printRes(threeSum(arr));

		arr=new int[]{-1, 0, -1, -2, -1, -4};
		printRes(threeSum(arr));

		arr=new int[]{-1, 0, 1, -1, 0, 1, 1, 0, -1};
		printRes(threeSum(arr));

		arr=new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
		printRes(threeSum(arr));
	}

	private static void printRes(List<List<Integer>> res) {
		System.out.println("----------------");
		for (List<Integer> lst : res) {
			System.out.println(Arrays.toString(lst.toArray()));
		}
		System.out.println("----------------");
	}
}
