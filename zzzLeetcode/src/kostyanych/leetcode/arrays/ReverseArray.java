package kostyanych.leetcode.arrays;

import java.util.Arrays;

public class ReverseArray {

	static public void reverseArray(int[] arr) {
		int len=arr.length;
		if (len<2)
			return;
		reverseArray(arr,0,len-1);
	}

	/**
	 * Reverses part of the array from startInd inclusive to endInd inclusive
	 * @param arr
	 * @param startInd
	 * @param endInd
	 */
	static public void reverseArray(int[] arr, int startInd, int endInd) {
		int len=arr.length;
		if (len<2)
			return;
		if (startInd==endInd)
			return;
		if (endInd>len-1)
			endInd=len-1;
		int left=startInd;
		int right=endInd;
		while(left<right) {
			int tmp=arr[left];
			arr[left]=arr[right];
			arr[right]=tmp;
			left++;
			right--;
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int arr[] = {1,2,3,4,5};
		System.out.println(Arrays.toString(arr));
		reverseArray(arr);
		System.out.println(Arrays.toString(arr));
		reverseArray(arr,1,3);
		System.out.println(Arrays.toString(arr));
	}

}
