package kostyanych.leetcode.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * There is a long and thin painting that can be represented by a numbered line.
 * The painting was painted with multiple overlapping segments where each segment was painted with a unique color.
 * You are given a 2D integer array segments,
 * 		where segments[i] = [starti, endi, colori] represents the half-closed segment [starti, endi) with colori as the color.
 *
 * The colors in the overlapping segments of the painting were mixed when it was painted.
 * When two or more colors mix, they form a new color that can be represented as a set of mixed colors.
 * For example, if colors 2, 4, and 6 are mixed, then the resulting mixed color is {2,4,6}.
 * For the sake of simplicity, you should only output the sum of the elements in the set rather than the full set.
 *
 * You need to describe the painting with the minimum number of non-overlapping half-closed segments of these mixed colors.
 * These segments can be represented by the 2D array painting
 * 		where painting[j] = [leftj, rightj, mixj] describes a half-closed segment [leftj, rightj) with the mixed color sum of mixj.
 *
 * For example, the painting created with segments = [[1,4,5],[1,7,7]] can be described by painting = [[1,4,12],[4,7,7]] because:
 * 		[1,4) is colored {5,7} (with a sum of 12) from both the first and second segments.
 * 		[4,7) is colored {7} from only the second segment.
 *
 * Return the 2D array painting describing the finished painting (excluding any parts that are not painted).
 * You may return the segments in any order.
 *
 * A half-closed segment [a, b) is the section of the number line between points a and b including point a and not including point b.
 *
 * Constraints:
 * 	1 <= segments.length <= 2 * 10^4
 * 	segments[i].length == 3
 * 	1 <= starti < endi <= 10^5
 * 	1 <= colori <= 10^9
 * 	Each colori is distinct.
 *
 * SOLUTION. Use a "sweep line" approach - just iterate thru all the segment starts and ends.
 * OR we can use a difference array
 * Segment start increases the color value by segment's color, segment end decreases the color value by segment's color
 *
 */
public class DescribeThePainting {

	static final int MAX_POINT=100_001;

	static List<List<Long>> splitPaintingTree(int[][] segments) {

		Map<Integer, Long> map = new TreeMap<>();
        for (int[] s : segments) {
        	map.put(s[0], map.getOrDefault(s[0], 0L) + s[2]);
        	map.put(s[1], map.getOrDefault(s[1], 0L) - s[2]);
        }
        List<List<Long>> res = new ArrayList<>();
        long i = 0;
        long sum = 0;
        for (int j : map.keySet()) {
            if (sum > 0)
                res.add(Arrays.asList(i, (long)j, sum));
            sum += map.get(j);
            i = j;
        }
        return res;
	}

	static private List<List<Long>> splitPainting(int[][] segments) {
	    long mix[] = new long[MAX_POINT];
	    long sum=0;
	    long prevI=0;

	    boolean ends[] = new boolean[MAX_POINT];

	    List<List<Long>> res = new ArrayList<>();

	    for (int[] s : segments) {
	        mix[s[0]] += s[2];
	        mix[s[1]] -= s[2];
	        ends[s[0]] = true;
	        ends[s[1]] = true;
	    }

	    for (int i = 1; i < MAX_POINT; ++i) {
	        if (ends[i]) {
	        	if (sum > 0)
	        		res.add(Arrays.asList(prevI, (long)i, sum));
	        	prevI=i;
	        	sum += mix[i];
	        }
	    }
	    return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr={{4,5,9},{8,12,5},{4,7,19},{14,15,1},{3,10,8},{17,20,18},{7,19,14},{8,16,6},{14,17,7},{11,13,3}};
		System.out.println(splitPainting(arr));
	}

}
