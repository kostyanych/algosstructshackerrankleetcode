package kostyanych.leetcode.arrays;

import java.util.Arrays;

public class RotateArray {

	static private void rotateRight(int[] arr, int k) {
		int len=arr.length;
		if (len<2)
			return;
		//if k>len, we'll just do k/len rounds of full rotation. this is useless
		k = k % len;
		//if k%len==0, we'll do full rotation, which is useless
		if (k==0)
			return;
		int moves=0;
		for (int start=0;moves<len;start++) {
			int curInd=start;
			int prev=arr[start];

			while (true) {
				int nextInd=(curInd + k)%len;
				int buf=arr[nextInd];
				arr[nextInd]=prev;
				prev=buf;
				curInd=nextInd;
				moves++;
				if (curInd==start)
					break;
			}
		}
	}

	static private void rotateLeft(int[] arr, int k) {
		int len=arr.length;
		if (len<2)
			return;
		//if k>len, we'll just do k/len rounds of full rotation. this is useless
		k = k % len;
		//if k%len==0, we'll do full rotation, which is useless
		if (k==0)
			return;
		int moves=0;
		for (int start=0;moves<len;start++) {
			int curInd=start;
			int prev=arr[start];

			while (true) {
				int tmp=curInd-k;
				int nextInd=tmp>=0?tmp:len+tmp;
				int buf=arr[nextInd];
				arr[nextInd]=prev;
				prev=buf;
				curInd=nextInd;
				moves++;
				if (curInd==start)
					break;
			}
		}
	}

	static private void rotateLeftViaReverse(int[] arr, int k) {
		int len=arr.length;
		if (len<2)
			return;
		k = k % len;
		if (k==0)
			return;
		ReverseArray.reverseArray(arr);
		ReverseArray.reverseArray(arr,0,len-k-1);
		ReverseArray.reverseArray(arr,len-k,len-1);
	}

	static private void rotateRightViaReverse(int[] arr, int k) {
		int len=arr.length;
		if (len<2)
			return;
		k = k % len;
		if (k==0)
			return;
		ReverseArray.reverseArray(arr);
		ReverseArray.reverseArray(arr,0,k-1);
		ReverseArray.reverseArray(arr,k,len-1);
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int arr[] = {1,2,3,4,5,6};
		int k=2;
		System.out.println(Arrays.toString(arr));
		rotateRightViaReverse(arr,k);
		System.out.println(Arrays.toString(arr));
	}
}
