package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * You are given an integer array nums sorted in non-decreasing order.
 *
 * Build and return an integer array result with the same length as nums such that result[i] is equal to the summation of absolute differences
 * between nums[i] and all the other elements in the array.
 *
 * In other words, result[i] is equal to sum(|nums[i]-nums[j]|) where 0 <= j < nums.length and j != i (0-indexed).
 *
 * Constraints:
 *
 * 	2 <= nums.length <= 105
 * 	1 <= nums[i] <= nums[i + 1] <= 104
 *
 * SOLUTION:
 * 1. array is sorted (but with duplicates) so for every i the differences nums[j]-nums[i] to the left of i would be negative or zero
 * 2. array is sorted (but with duplicates) so for every i the differences nums[j]-nums[i] to the right of i would be positive or zero
 * 3. calculate prefix sums for every i
 * 4. for part on the left we'll have (nums[i]-nums[j1])+(nums[i]-nums[j2])+...+(nums[i]-nums[0]) = i*nums[i]-(nums[j1]+nums[j2]+...+nums[0]) =
 * 		= i*nums - prefixSum[j1], where j1=i-1;
 * 5. similar logic leads to the right part where we'll have prefixSum[len-1]-prefixSum[i] - (len-1-i)*nums[i]
 *
 */
public class SumOfAbsoluteDifferencesInASortedArray {

	static private int[] getSumAbsoluteDifferences(int[] nums) {
		int len=nums.length;
		int[] sums = new int[len];
		sums[0]=nums[0];
		for (int i=1;i<len;i++) {
			sums[i]=nums[i]+sums[i-1];
		}
		int wholeSum=sums[len-1];
		int[] res = new int[len];
		res[0]=wholeSum-nums[0]*len;
		res[len-1]=len*nums[len-1]-wholeSum;
		for (int i=1;i<len-1;i++) {
			res[i]=i*nums[i]-sums[i-1];
			res[i]+=(wholeSum-sums[i])-nums[i]*(len-1-i);
		}
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] nums = {2,3,3,5,7};
		System.out.println(Arrays.toString(nums));
		System.out.println(Arrays.toString(getSumAbsoluteDifferences(nums)));
	}

}
