package kostyanych.leetcode.arrays;

/**
 * Given a circular array C of integers represented by A, find the maximum possible sum of a non-empty subarray of C.
 *
 * Here, a circular array means the end of the array connects to the beginning of the array.  (Formally, C[i] = A[i] when 0 <= i < A.length, and C[i+A.length] = C[i] when i >= 0.)
 *
 * Also, a subarray may only include each element of the fixed buffer A at most once.  (Formally, for a subarray C[i], C[i+1], ..., C[j], there does not exist i <= k1, k2 <= j with k1 % A.length = k2 % A.length.)
 *
 * Note:
 *
 * -30000 <= A[i] <= 30000
 * 1 <= A.length <= 30000
 *
 */
public class MaximumSumCircularSubarray {

	/**
	 * 1. Every consequitive pair of positive numbers can be substituted by their sum.
	 *    That will not affect the max subarray sum. Nor the min subarray sum.
	 * 2. Same goes for negative numbers.
	 * 3. If a negative x lies between positve a and b, it will contribute to they common maximization iff |x|<=a+b.
	 *    Otherwise they will yield a negative number.
	 * 4. We will be left with a sequence of positive and negative numbers that cannot lead to increasing of max subsequence on any side of itself.
	 * 5. This sequence will be eventually reduced to min or negative subarray
	 * 7. In the end we will be left with these cases:
	 * 7a. The whole array is the max subarray (Smax=Sarr)
	 * 7b. The max subarray is in the middle, two negative subarrays are on its sides. One of them is the min subarray. So: Smin|Smax|Sneg
	 * 7c. The negative subarray is in the middle, 2 positive subarrays on its sides. Negative one is obviously min subarray, one of positives is max subarray.
	 *        So: Smax|Smin|Spos
	 * 7d. A case of both 7b and 7c: One positive (max) subarray and one negative (min) subarray.
	 * 7e. If all the numbers in the array are negative, Sarr = Smin. So, Sarr-Smin==0. Result is the max number in array.
	 * 8. So, for circular subarrays cases 7a, 7b, 7d, 7e will yield a simple result: Smax will be the result for circular subarray.
	 *    For the case 7c the result will be the sum of two positive subarrays. Obviously that would be equal to Sarr-Smin (min is the one array in the middle).
	 * 9. So, in the end the result will be Max(Smax, Sarr-Smin)
	 *
	 * @param A
	 * @return
	 */
	private static int maxSubarraySumCircular(int[] A) {
		int len=A.length;
		int min=A[0];
		int max=A[0];
		int currMaxArr=A[0];
		int currMinArr=A[0];
		int sum=A[0];
		for (int i=1;i<len;i++) {
			sum+=A[i];
			int buf=A[i]+currMaxArr;
			if (buf>=A[i])
				currMaxArr=buf;
			else
				currMaxArr=A[i];
			buf=A[i]+currMinArr;
			if (buf<=A[i])
				currMinArr=buf;
			else
				currMinArr=A[i];
			max=Math.max(max, currMaxArr);
			min=Math.min(min, currMinArr);
		}
		return min==sum?max:Math.max(max, sum-min);
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {-2,-3,-1};
		System.out.println(maxSubarraySumCircular(arr));
	}
}
