package kostyanych.leetcode.arrays;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * There is a party where n friends numbered from 0 to n - 1 are attending.
 * There is an infinite number of chairs in this party that are numbered from 0 to infinity.
 * When a friend arrives at the party, they sit on the unoccupied chair with the smallest number.
 *
 * For example, if chairs 0, 1, and 5 are occupied when a friend comes, they will sit on chair number 2.
 * When a friend leaves the party, their chair becomes unoccupied at the moment they leave.
 * If another friend arrives at that same moment, they can sit in that chair.
 *
 * You are given a 0-indexed 2D integer array times where times[i] = [arrivali, leavingi],
 * 		indicating the arrival and leaving times of the ith friend respectively, and an integer targetFriend.
 * All arrival times are distinct.
 *
 * Return the chair number that the friend numbered targetFriend will sit on.
 *
 */
public class NumberOfTheSmallestUnoccupiedChair {

	static class Time {
	    int idx, time;
	    Time(int idx, int time){
	        this.idx = idx;
	        this.time = time;
	    }
	}

	static int smallestChair(int[][] times, int targetFriend) {

		int len = times.length;

        Time arr[] = new Time[len];
        Time dept[] = new Time[len];

        for(int i=0; i<len; i++){
            arr[i] = new Time(i, times[i][0]);
            dept[i] = new Time(i, times[i][1]);
        }

        Arrays.sort(arr, (a, b) -> a.time - b.time);
        Arrays.sort(dept, (a, b) -> a.time - b.time);

        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for(int i=0; i<len; i++){
            pq.add(i);
        }

        int used[] = new int[len];

        int down = 0;
        int up = 0;

        while(down<len && up<len){
            if(arr[down].time < dept[up].time){
                used[arr[down].idx] = pq.poll();
                if (arr[down].idx==targetFriend)
                	return used[arr[down].idx];
                down++;
            }
            else if(arr[down].time >= dept[up].time){
                pq.add(used[dept[up].idx]);
                up++;
            }
        }
        return used[targetFriend];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr = {{1,4},{2,3},{4,6}};
		System.out.println(smallestChair(arr,1));
	}

}
