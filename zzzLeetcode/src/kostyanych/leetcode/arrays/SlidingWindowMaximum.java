package kostyanych.leetcode.arrays;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.TreeMap;

/**
 * You are given an array of integers nums,
 * there is a sliding window of size k which is moving from the very left of the array to the very right.
 * You can only see the k numbers in the window. Each time the sliding window moves right by one position.
 *
 * Return the array consisting of maxes of sliding window in each position.
 *
 * 1 <= nums.length <= 10^5
 * -10^4 <= nums[i] <= 10^4
 * 1 <= k <= nums.length
 *
 */
public class SlidingWindowMaximum {

	static private class Counter {
		int val=1;
		public int increase() {
			val++;
			return val;
		}

		public int decrease() {
			val--;
			return val;
		}
	}

	/**
	 * 1. Keep processed INDICES in deque;
	 * 2. on each iteration trim deque, removing all indices that have gone out of the window
	 * 3. for the right remove all indicest that has array elements LESS than current, 'cause they won't be a max anymore
	 * 4. ALWAYS put current index to the right
	 * 5. MAX will ALWAYS be on the left of the deque
	 * @param nums
	 * @param k
	 * @return
	 */
	static private int[] maxSlidingWindow(int[] nums, int k) {
        int len = nums.length;
        int[] res=new int[len-k+1];

        Deque<Integer> dq = new ArrayDeque<>(); // store indices

        for(int i=0;i<len;i++){
            // removing the indices that are out of our window already
            while (!dq.isEmpty() && dq.peekFirst() <= i-k){
                dq.pollFirst();
            }

            // those will never can be max anymore
            while(!dq.isEmpty() && nums[dq.peekLast()]<nums[i]){
                dq.pollLast();
            }

            // put current
            dq.offerLast(i);
            if (i >= k - 1) {
                 res[i - k + 1] = nums[dq.peekFirst()];
            }
        }
        return res;
    }

	static private int[] maxSlidingWindowSlow(int[] nums, int k) {
		int len=nums.length;
		int[] res=new int[len-k+1];
		TreeMap<Integer, Counter> tm = new TreeMap<>(Comparator.reverseOrder());
		for (int i=0;i<k;i++) {
			Counter val=tm.get(nums[i]);
			if (val==null ) {
				val = new Counter();
				tm.put(nums[i], val);
			}
			else
				val.increase();
		}
		res[0]=tm.firstKey();
		for (int i=1;i<=len-k;i++) {
			int ind=i+k-1;
			Counter val=tm.get(nums[ind]);
			if (val==null ) {
				val = new Counter();
				tm.put(nums[ind], val);
			}
			else
				val.increase();

			ind=i-1;
			val=tm.get(nums[ind]);
			if (val.decrease()==0)
				tm.remove(nums[ind]);
			res[i]=tm.firstKey();
		}
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int arr[]= {1,3,-1,-3,5,3,6,7};
		System.out.println(Arrays.toString(arr));
		System.out.println(Arrays.toString(maxSlidingWindow(arr,3)));
	}

}
