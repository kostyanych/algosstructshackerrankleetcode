package kostyanych.leetcode.arrays;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

/**
 * Given an integer array nums and an integer k,
 * return the maximum sum of a non-empty subsequence of that array such that
 *   for every two consecutive (in subsequence) integers in the subsequence,
 *    nums[i] and nums[j], where i < j, the condition j - i <= k is satisfied. (i and j are original array's indices, not ones of subsequence)
 *
 * A subsequence of an array is obtained by deleting some number of elements (can be zero) from the array,
 * leaving the remaining elements in their original order.
 *
 */
public class ConstrainedSubsequenceSum {

	private static int constrainedSubsetSum(int[] nums, int k) {
		int len=nums.length;
		// memo table to store the max sum of subsequence ending with current index
        int[] memo = new int[len];
        // Deque to store the indices of memo values in strictly decreasing order
        Deque<Integer> deque = new ArrayDeque<>();

        for (int i = 0; i < nums.length; ++i) {
            memo[i] = nums[i];
            //throw away those indexes that are already went out of the window
            while (!deque.isEmpty() && i - deque.peekFirst() > k) {
                deque.pollFirst();
            }
            //the max is always at the head, so we only need to check against (this + head)
            if (!deque.isEmpty())
                memo[i] = Math.max(memo[i], memo[i] + memo[deque.peekFirst()]);
            //now we should throw out anything that is less then current value, so that the deque would would be strictly decreasing
            //other way to look at it - we can't get max value with those nums that are less than current. max will be made from current
            while (!deque.isEmpty() && memo[deque.peekLast()] <= memo[i]) {
                deque.pollLast();
            }
            deque.addLast(i);
        }
        int res = Integer.MIN_VALUE;
        for (int i = 0; i < memo.length; ++i)
            res = Math.max(memo[i], res);
        return res;
    }

	private static int constrainedSubsetSumWrong(int[] nums, int k) {
		int sum=0;
        int len=nums.length;
        int res=Math.max(nums[0],nums[len-1]); // we have to start somewhere

        int curInd=-1;
        for(int i=0;i<len;i++) {

            //skip all heading negatives
            if(curInd==-1 && nums[i]<0)
                continue;

            if(nums[i]>=0) {// for positive value just add it and update the curInd
                sum+=nums[i];
                curInd=i;
            }
            else {
                int j=i;
                int max=Integer.MIN_VALUE;
                int bufInd=0;

				// if nums[i]<0, iterate through the permitted interval (j-curInd)<=k and find the max_number
                // or if number encountered is positive, break out of the loop.
                while( j<len && j-curInd<=k && nums[j]<0) {
                    if(max<nums[j]) {
                        bufInd=j;
                        max=nums[j];
                    }
                    j++;
                }

                if (j==len) //we reached the end of the array iterating thru negative values
                    break;
                else if (j-curInd<=k) { //&& j<len //we're out of the previous loop before k values, it means nums[j]>=0
                    sum+=nums[j];
                    curInd=j;
                }
                else { //we're out of the previous loop after k values, it means all k values were negative, but nums[bufInd] is the max negative value we've encountered
                    sum+=nums[bufInd];
                    curInd=bufInd; // add the maximum negative number so far.
                }
                i=curInd;

            }
            res=Math.max(res,sum); // update the answer accordingly
            if(sum<0)
                sum=0; // Typical Kadane's step, if sum becomes negative, just start afresh i.e make sum=0;
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		//int[] arr= {10,2,-10,-10, -10, 5,20};
		int[] arr= {100,-1,-10,-100, 100};
		int k=2;
		System.out.println(Arrays.toString(arr));
		System.out.println(constrainedSubsetSum(arr, k));
	}
}
