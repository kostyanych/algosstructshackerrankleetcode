package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 */
public class MoveZeroes {

	private static void moveZeroes(int[] nums) {
		int len = nums.length;
		if (len < 2)
			return;
		int digitPos = 0;
		for (int i = 0; i < len; i++) {
			if (nums[i] != 0) {
				nums[digitPos] = nums[i];
				digitPos++;
			}
		}
		for (int i = digitPos; i < len; i++) {
			nums[i] = 0;
		}

	}

	private static void moveZeroesAverage(int[] nums) {
		int len = nums.length;
		if (len < 2)
			return;
		int digitPos = 0;
		int zeroPos = 0;

		while (zeroPos < len) {
			if (nums[zeroPos] != 0) {
				zeroPos++;
				continue;
			}
			digitPos = zeroPos + 1;
			while (digitPos < len && nums[digitPos] == 0) {
				digitPos++;
			}
			if (digitPos > len - 1)
				break;
			nums[zeroPos] = nums[digitPos];
			nums[digitPos] = 0;
			digitPos = zeroPos;
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = { 0, 1, 0, 3, 12 };
		System.out.println(Arrays.toString(arr));
		moveZeroes(arr);
		System.out.println(Arrays.toString(arr));
	}
}
