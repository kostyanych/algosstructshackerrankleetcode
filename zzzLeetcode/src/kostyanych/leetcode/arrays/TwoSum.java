package kostyanych.leetcode.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {

	private static int[] twoSum(int[] nums, int target) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < nums.length; i++) {
			int compl = (target - nums[i]);
			if (map.containsKey(compl))
				return new int[] { map.get(compl), i };
			map.put(nums[i], i);
		}
		return new int[0];
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(Arrays.toString(twoSum(new int[]{-2, 7, 11, 15}, 9)));
	}
}
