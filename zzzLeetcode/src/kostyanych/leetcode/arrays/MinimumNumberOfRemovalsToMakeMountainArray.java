package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * An array arr is a mountain array if and only if:
 *  arr.length >= 3
 *  There exists some index i (0-indexed) with 0 < i < arr.length - 1 such that:
 *  arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 *  arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 *
 *  Given an integer array nums​​​, return the minimum number of elements to remove to make nums​​​ a mountain array.
 *
 *  Constraints:
 *  	3 <= nums.length <= 1000
 *  	1 <= nums[i] <= 109
 *  	It is guaranteed that you can make a mountain array out of nums.
 *
 */
public class MinimumNumberOfRemovalsToMakeMountainArray {

	static private int minimumMountainRemovals(int[] nums) {
        int len = nums.length;
        int[] dpleft = new int[len];
        int[] dpright = new int[len];
        Arrays.fill(dpleft, 1);
        Arrays.fill(dpright, 1);
        for(int i = 1; i < len; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[j] < nums[i])
                    dpleft[i] = Math.max(dpleft[i], dpleft[j] + 1);

                if(nums[len - 1 - j] < nums[len - 1 - i])
                    dpright[len - 1 - i] = Math.max(dpright[len - 1 - i], dpright[len - 1 - j] + 1);
            }
        }

        int max = 0;
        for(int i = 1; i < len - 1; i++) {
            if(dpleft[i] > 1 && dpright[i] > 1) {
                max = Math.max(max, dpleft[i] + dpright[i] - 1);
            }
        }
        return len-max;
    }

	/**
	 * SOLUTION:
	 * for every i from 1 to len-2 calculate longest increasing subsequence up to that i and longest decreasing subsequence from that i
	 * @return
	 */
	static private int minimumMountainRemovalsSlow(int[] nums) {
		int len=nums.length;
		int max=0;
		for (int pivot=1; pivot<=len-2;pivot++) {
			int lis=lis(nums,pivot);
			if (lis<2)
				continue;
			int lds=lds(nums,pivot);
			if (lds<2)
				continue;
			max=Math.max(max, lis+lds-1);
		}
		return len-max;
    }

	static private int lis(int[]nums, int upTo) {
		int[] dp=new int[upTo+1];
		int res=0;
		for (int i=0;i<=upTo;i++) {
			int num=nums[i];
			int ind = Arrays.binarySearch(dp, 0, res, num);
			if (ind < 0)
				ind = -(ind + 1);
			dp[ind] = num;
			if (ind == res)
				res++;
		}
		return res;
	}

	static private int lds(int[]nums, int downFrom) {
		int len=nums.length;
		int[] dp=new int[len-downFrom];
		int res=0;
		for (int i=len-1;i>=downFrom;i--) {
			int num=nums[i];
			int ind = Arrays.binarySearch(dp, 0, res, num);
			if (ind < 0)
				ind = -(ind + 1);
			dp[ind] = num;
			if (ind == res)
				res++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {1,2,3,4,4,3,2,1};
		//Arrays.sort(arr);
		System.out.println(Arrays.toString(arr));
		System.out.println(minimumMountainRemovals(arr));
	}

}
