package kostyanych.leetcode.arrays;

/**
 * You are given a sorted array consisting of only integers where every element appears exactly twice,
 * except for one element which appears exactly once. Find this single element that appears only once.
 *
 * Note: Your solution should run in O(log n) time and O(1) space.
 */
public class SingleElementInASortedArray {

	/**
	 * If the current index in the binary search is I (0 index based), the we can decide the next search space based on:
	 *
	 * a. If I is even, and nums[I] == nums[I + 1], then size of subarray [0...I-1] (left side) is even
	 *    (since the element count is I), so left side does not contain the single occurance element.
	 *    Hence we need to check in [I + 1, ..., N] subarray (right side of I)
	 *    Otherwise we need to check the subarray up to I.
	 *
	 * b. If I is odd, and nums[I] == nums[I - 1], then size of subarray [0...I-2] (left side) is even
	 *    (since the element count is I - 1, and I-1 is even since I is odd), so left side does not contain the single occurance element.
	 *    Hence we need to check in [I + 1, ..., N] subarray (right side of I)
	 *    Otherwise we need to check subarray up to I.
	 *
	 * Note: How can you implement the check operation. Remember the xor operation, and if you xor any number with 1, then it flips the LSB (Least significant bit)
	 * Ex. 1) 100^1=101 [i.e 4^1=5, flips the first bit from right side]
	 *     2) 101^1=100 [i.e 5^1=4, again flips the first bit from right side]
	 * So, if I is even, to find the next element we do I^1 and if I is odd, to find the previous element we do I^1
	 */
	private static int singleNonDuplicate(int[] nums) {
        int len=nums.length;
        if (len==1)
            return nums[0];

		int l = 0;
        int r = len - 1;
        while (l < r) {
            int mid = l + (r-l)/2;
            if (nums[mid] == nums[mid ^ 1])
                l = mid + 1;
            else
                r = mid;
        }
        return nums[r];
    }


	private static int singleNonDuplicateLong(int[] nums) {
        int len=nums.length;
        if (len==1)
            return nums[0];
        int l=0;
        int r=len-1;
        while (l<=r) {
            int mid=(l+(r-l)/2);
            //if mid is even, then it's duplicate should've been on the right, if there were only duplicates
            //if mid is odd, then it's duplicate should've been on the left, if there were only duplicates
            if (mid%2==0) {
            	if (mid==len-1 || mid==0)
            		return nums[mid];
            	if (nums[mid+1]==nums[mid])
            		l=mid+2;
            	else {
            		if (nums[mid-1]==nums[mid])
            			r=mid-2;
                	else
                		return nums[mid];
            	}
            }
            else {

            	if (nums[mid-1]==nums[mid]) {
            		if (mid+1<len)
            			l=mid+1;
            		else return -2;
            	}
            	else if (nums[mid+1]==nums[mid]) {
            		if (mid-1>=0)
            			r=mid-1;
            		else
            			return -3;
            	}
            	else
            		return nums[mid];
            }
        }
        return -1;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {3,7,7};
		System.out.println(singleNonDuplicate(arr));
	}

}
