package kostyanych.leetcode.arrays;

/**
 * Given a sorted array and a target value, return the index if the target is
 * found. If not, return the index where it would be if it were inserted in
 * order.
 * 
 * You may assume no duplicates in the array.
 *
 */
public class SearchInsertPosition {

	private static int searchInsert(int[] nums, int target) {
		int right = nums.length - 1;
		int left = 0;
		if (target <= nums[0])
			return 0;
		if (target == nums[right])
			return right;
		if (target > nums[right])
			return right + 1;
		while (left < right) {
			int ind = (right + left) / 2;
			if (nums[ind] == target)
				return ind;
			if (nums[ind] < target) {
				if (ind == left)
					left++;
				else
					left = ind;
			} else {
				if (ind == right)
					right--;
				else
					right = ind;
			}
		}
		if (nums[right]>target)
			return right;
		return right+1;
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={1,3,5,6,13};
		System.out.println(searchInsert(arr,4));
	}

}
