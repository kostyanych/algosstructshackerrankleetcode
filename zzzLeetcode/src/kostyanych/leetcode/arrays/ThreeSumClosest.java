package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * Given an array nums of n integers and an integer target, 
 * find three integers in nums such that the sum is closest to target. 
 * Return the sum of the three integers.
 */
public class ThreeSumClosest {

	private static int threeSumClosest(int[] nums, int target) {
		
		int len=nums.length;
		if (len<3)
			return 0;
		if (len==3)
			return nums[0]+nums[1]+nums[2];
		Arrays.sort(nums);
		
		int res=0;
		int min=Integer.MAX_VALUE;
		
		for (int i=0;i<len-2;i++) {
			int left=i+1;
			int right=len-1;
			while (left<right) {
				int sum=nums[i]+nums[left]+nums[right];
				if (sum==target)
					return sum;
				int buf=Math.abs(sum-target);
				if (buf<min) {
					res=sum;
					min=buf;
				}
				if (sum>target)
					right--;
				else
					left++;
			}
		}
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={-1, 2, 1, -4};
		System.out.println(threeSumClosest(arr, 1));
	}

}
