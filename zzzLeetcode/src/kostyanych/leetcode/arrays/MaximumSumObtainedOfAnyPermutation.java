package kostyanych.leetcode.arrays;

import java.util.Arrays;

/**
 * We have an array of integers, nums, and an array of requests where requests[i] = [starti, endi].
 * The ith request asks for the sum of nums[starti] + nums[starti + 1] + ... + nums[endi - 1] + nums[endi].
 * Both starti and endi are 0-indexed.
 *
 * Return the maximum total sum of all requests among all permutations of nums.
 *
 * Since the answer may be too large, return it modulo 10^9 + 7.
 *
 * Constraints:
 *
 * n == nums.length
 * 1 <= n <= 10^5
 * 0 <= nums[i] <= 10^5
 * 1 <= requests.length <= 10^5
 * requests[i].length == 2
 * 0 <= starti <= endi < n
 *
 */
public class MaximumSumObtainedOfAnyPermutation {

	private static final int MOD = 1_000_000_007;

	static private int maxSumRangeQuery(int[] nums, int[][] requests) {
        int len = nums.length;
        int reqIndexes[] = new int[len];
        //increase counter at request start - each request increases the usage of indices starting at the request start
        //decrease counter at the index right after request end - usage of indices decreases after the request end
        for (int[] r : requests) {
        	reqIndexes[r[0]]++;
            if (r[1]+1 < len)
            	reqIndexes[r[1]+1]--;
        }

        //simple approach to count how many times every index is used in all requests
        for (int i=1; i<len; i++) {
        	reqIndexes[i]+=reqIndexes[i-1];
        }
        Arrays.sort(reqIndexes);
        Arrays.sort(nums);
        long ans = 0;
        //now just put the max num at the maximally used index, and so on
        for(int i=len-1; i>=0 && reqIndexes[i]>0; i--) {
            ans += ((long)(reqIndexes[i]%MOD)*nums[i])%MOD;
        }
        return (int)(ans%MOD);
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] nums = {1,2,3,4,5};
		int[][] reqs = {{1,3},{0,1}};
		System.out.println(maxSumRangeQuery(nums, reqs));
	}
}
