package kostyanych.leetcode.arrays;

import java.util.ArrayList;
import java.util.List;

import kostyanych.leetcode.helpers.Printer;

public class Permutations {

	private static List<List<Integer>> permute(int[] nums) {
		List<List<Integer>> res;
		if (nums.length==1) {
			res = new ArrayList<>();
			List<Integer> lst=new ArrayList<Integer>();
			lst.add(nums[0]);
			res.add(lst);
		}
		else
			res=perm(nums);

		return res;
    }

	private static List<List<Integer>> perm(int[] arr) {
		int len=arr.length;
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		if (len==2) {
			List<Integer> lst=new ArrayList<>();
			res.add(lst);
			lst.add(arr[0]);
			lst.add(arr[1]);
			lst=new ArrayList<>();
			res.add(lst);
			lst.add(arr[1]);
			lst.add(arr[0]);
			return res;
		}

		for (int i=0;i<len;i++) {
			int[] newarr=new int[len-1];
			fillArr(newarr,arr,i);
			List<List<Integer>> subLists = perm(newarr);
			for (List<Integer> sublist : subLists) {
				List<Integer> lst = new ArrayList<>();
				res.add(lst);
				lst.add(arr[i]);
				lst.addAll(sublist);
			}
		}
		return res;
	}

	private static void fillArr(int[] newArr, int[] oldArr, int ind) {
		if (ind>0) {
			System.arraycopy(oldArr, 0, newArr, 0, ind);
		}

		int len=oldArr.length;
		if (ind<(len-1)) {
			System.arraycopy(oldArr, ind+1, newArr, ind, len-ind-1);
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={1, 2, 3};
		Printer.print2DList(permute(arr), true);

		int[] asd1 = {2,1,1};
		List<Integer> ASD1 = new ArrayList<Integer>();
		ASD1.add(2);
		ASD1.add(1);
		ASD1.add(1);
		int[] asd2 = {2,1,1};
		List<Integer> ASD2 = new ArrayList<Integer>();
		ASD2.add(2);
		ASD2.add(1);
		ASD2.add(1);

		System.out.println(asd1.equals(asd2));
		System.out.println(ASD1.equals(ASD2));
	}
}
