package kostyanych.leetcode.arrays;

/**
 * Given two integer arrays nums1 and nums2, return the maximum length of a subarray that appears in both arrays.
 *
 * This is simply Longest Common Substring dp algo
 *
 */
public class MaximumLengthOfRepeatedSubarray {

	private static int findLength(int[] nums1, int[] nums2) {
		int len1=nums1.length;
		int len2=nums2.length;

		int[] prev = new int[len1+1];
		int[] cur = new int[len1+1];

		int max=0;

		for (int i2=0;i2<len2;i2++) {
			for (int i1=0;i1<len1;i1++) {
				if (nums2[i2]==nums1[i1]) {
					cur[i1+1]=1+prev[i1];
					max=Math.max(max, cur[i1+1]);
				}
				else
					cur[i1+1]=0;
			}
			int[] buf=prev;
			prev=cur;
			cur=buf;
		}
		return max;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] nums1 = {1,2,3,2,1};
		int[] nums2 = {3,2,1,4,7};
		System.out.println(findLength(nums1, nums2));
	}


}
