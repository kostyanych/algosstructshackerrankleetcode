package kostyanych.leetcode.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of positive integers nums, remove the smallest subarray (possibly empty)
 * 	such that the sum of the remaining elements is divisible by p. It is not allowed to remove the whole array.
 *
 * Return the length of the smallest subarray that you need to remove, or -1 if it's impossible.
 * A subarray is defined as a contiguous block of elements in the array.
 *
 * Constraints:
 *
 * 	1 <= nums.length <= 10^5
 * 	1 <= nums[i] <= 10^9
 * 	1 <= p <= 10^9
 *
 *  Solution:
 *  1. (a+b)%c = (a%c + b%c)%c
 *  2. (a-b)%c = (a%c - b%c + c)%c  (+c is for the case when a%c<b%c, otherwise it will cancel itself when %c)
 *
 *  3. S(0)=nums[0]
 *  4. S(i)=nums[0]+nums[1]+...+nums[i]=S(i-1)+nums[i]
 *  5. S(n)%p = rem (remainder)
 *  6. if rem==0, the whole array is ok, we don't need to remove anything
 *  7. else we need to find range [i;j], so that (S(n)-S(i,j))%p=0
 *  7a. where S(i,j)=S(j)-S(i)
 *  8. from 7: S(n)-S(ij) = p*n
 *  9. S(ij)=S(n)-p*n
 *  10. S(i,j)%p = (S(n)-p*n)%p
 *  11. (p*n)%p=0 => S(i,j)%p = S(n)%p
 *  12. from 7a, 11, 5: (S(j)-S(i))%p = S(n)%p = rem
 *  13. S(j)-S(i) = p*m+rem
 *  14. S(i) = S(j) - p*m - rem
 *  15. (S(i))%p = (S(j) - p*m - rem)%p
 *  16. (p*m)%p = 0
 *  16a. rem%p = rem
 *  17. from 2,15,16,16a: (S(i))%p = ((S(j)%p - rem%p + p)%p
 *
 *  18. calculate rem as per 5
 *  19. for every j calculate (S(j))%p = (S(j-1)+nums[j])%p
 *  20. calculate (S(i))%p for (S(j))%p and rem from 19 and 18
 *  20a. if no i exists for such value, go to next j
 *  20b. if such i exists, check if (j-i) is minimum of all such ranges for every j
 *  20c. obvoiusly we only need to consider the last (greatest) i on every step, 'cause only that will yield the min range
 */
public class MakeSumDivisibleByP {

	static private int minSubarray(int[] nums, int p) {
		int len = nums.length;
		int res = len;
		int rem = 0;
		int cur = 0;
        for (int a : nums) {
            rem = (rem + a) % p;
        }
        if (rem==0)
        	return 0;

        Map<Integer, Integer> lastIndex = new HashMap<>();
        lastIndex.put(0, -1); //remove the whole range up to j
        for (int i = 0; i < len; i++) {
            cur = (cur + nums[i]) % p;
            lastIndex.put(cur, i);
            int lookFor = (cur - rem + p) % p;
            res = Math.min(res, i - lastIndex.getOrDefault(lookFor, -len));
        }
        return res < len ? res : -1;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {3,1,2,2};
		System.out.println(Arrays.toString(arr));
		System.out.println(minSubarray(arr, 6));

	}

}
