package kostyanych.leetcode;

import java.util.Arrays;

import kostyanych.leetcode.helpers.Printer;

/**
 * You are given an n x n 2D matrix representing an image.
 *
 * Rotate the image by 90 degrees (clockwise).
 *
 * Note:
 *
 * You have to rotate the image in-place, which means you have to modify
 * the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.
 *
 */
public class RotateImage {

	private static void rotate(int[][] matrix) {
        int len=matrix.length;
//        boolean odd = (len%2==1);
		int buf1, buf2, buf3, buf4;
		int maxY = len/2;

		for (int i=0;i<maxY;i++) {
			for (int j=i;j<(len-i)-1;j++) {
				buf1=matrix[i][j];
				buf2=matrix[j][(len-i-1)];
				buf3=matrix[(len-i-1)][(len-j-1)];
				buf4=matrix[(len-j-1)][i];
				matrix[j][(len-i-1)]=buf1;
				matrix[(len-i-1)][(len-j-1)]=buf2;
				matrix[(len-j-1)][i]=buf3;
				matrix[i][j]=buf4;
			}
		}
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr={{1,1,1,1,1},{2,2,2,2,2},{3,3,3,3,3},{4,4,4,4,4},{5,5,5,5,5}};
		Printer.print2DArray(arr, false);
		rotate(arr);
		Printer.print2DArray(arr, false);
		int[][] arr1 = {{ 5, 1, 9,11},{ 2, 4, 8,10},{13, 3, 6, 7},{15,14,12,16}	};
		Printer.print2DArray(arr1, true);
		rotate(arr1);
		Printer.print2DArray(arr1, true);

	}
}
