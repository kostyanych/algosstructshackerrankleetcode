package kostyanych.leetcode;

/**
 * Given a string s, the power of the string is the maximum length of a non-empty substring that contains only one unique character.
 *
 * Return the power of the string.
 *
 * Constraints:
 *    1 <= s.length <= 500
 *    s contains only lowercase English letters.
 *
 */
public class ConsecutiveCharacters {

	private static int maxPower(String s) {
		char[] arr=s.toCharArray();
		int len = arr.length;
		int curLen=1;
		int max=1;
		char cur=arr[0];
		for (int i=1;i<len;i++) {
			if (arr[i]==cur) {
				curLen++;
				if (curLen>max)
					max=curLen;
			}
			else {
				if (curLen>max)
					max=curLen;
				curLen=1;
				cur=arr[i];
			}
		}
		return max;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("leetcode"+" -> "+maxPower("leetcode"));
		System.out.println("abbcccddddeeeeedcba"+" -> "+maxPower("abbcccddddeeeeedcba"));
		System.out.println("triplepillooooow"+" -> "+maxPower("triplepillooooow"));
		System.out.println("hooraaaaaaaaaaaa"+" -> "+maxPower("hooraaaaaaaaaaaa"));
		System.out.println("touristaa"+" -> "+maxPower("touristaa"));
	}

}
