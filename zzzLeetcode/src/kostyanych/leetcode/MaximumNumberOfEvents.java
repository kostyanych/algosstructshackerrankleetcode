package kostyanych.leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Given an array of events where events[i] = [startDayi, endDayi]. 
 * Every event i starts at startDayi and ends at endDayi.
 * You can attend an event i at any day d where startTimei <= d <= endTimei. 
 * Notice that you can only attend one event at any time d.
 * Return the maximum number of events you can attend.
 */
public class MaximumNumberOfEvents {

	private static final Comparator<int[]> c = new Comparator<int[]>(){
        public int compare(int[] ev1, int[] ev2) {
            if (ev1[0]==ev2[0])
                return ev1[1]-ev2[1];
            return ev1[0]-ev2[0];
        }
    };

    private static final Comparator<int[]> c2 = new Comparator<int[]>(){
        public int compare(int[] ev1, int[] ev2) {
            return ev1[1]-ev2[1];
        }
    };
    
    private final static int MAX_DAY=100000;
    
    private static final PriorityQueue<int[]> pq = new PriorityQueue<>(c2);
    
    private static int maxEvents(int[][] events) {
        int len=events.length;
        int res=0;
        Arrays.sort(events,c);
        
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
        
        int i = 0;
        int missed=0;
        int curDay;
        for (curDay = events[0][0]; curDay <= MAX_DAY; curDay++) {
            while (!pq.isEmpty() && pq.peek() < curDay) {
                pq.remove();
                missed++;
            }
            while (i < len && events[i][0] == curDay) {
                pq.add(events[i][1]);
                i++;
            }
            if (!pq.isEmpty()) {
                pq.remove();
                res++;
            }
            if (missed+res>=len)
            	break;
        }
        System.out.println("res="+res+" missed="+missed+"  curDay="+curDay);
        return res;        
    }

    public static void testMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	
    	int[][] arr = {{7,11},{7,11},{7,11},{9,10},{9,11}};
    	printArr(arr);
    	System.out.println(maxEvents(arr));
    }

    private static void printArr(int[][] arr) {
    	for (int[] a : arr)
    		System.out.print(Arrays.toString(a)+", ");
    	System.out.println();
    }
}
