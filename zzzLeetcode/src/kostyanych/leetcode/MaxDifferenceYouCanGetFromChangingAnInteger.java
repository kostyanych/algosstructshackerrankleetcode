package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * You are given an integer num. You will apply the following steps exactly two times:
 *
 *    Pick a digit x (0 <= x <= 9).
 *    Pick another digit y (0 <= y <= 9). The digit y can be equal to x.
 *    Replace all the occurrences of x in the decimal representation of num by y.
 *
 * The new integer cannot have any leading zeros, also the new integer cannot be 0.
 * Let a and b be the results of applying the operations to num the first and second times, respectively.
 *
 * Return the max difference between a and b.
 *
 */
public class MaxDifferenceYouCanGetFromChangingAnInteger {

	private static int maxDiff(int num) {
		if (num<10)
			return 8;

		List<Integer> lst = digits(num);
		int src=-1;
		int big=0;
		boolean allSame=true;
		for (int i=0;i<lst.size();i++) {
			int d = lst.get(i);
			if (i>0 && d!=lst.get(i-1))
				allSame=false;
			if (src==-1) {
				if (d!=9) {
					src=d;
					d=9;
				}
			}
			else {
				if (d==src)
					d=9;
			}
			big=big*10+d;
		}
		if (allSame)
			return eights(num);

		src=-1;
		if (lst.get(0)==1) {
			for (int i=0;i<lst.size();i++) {
				if (lst.get(i)>1) {
					src=lst.get(i);
					break;
				}
			}
		}
		else
			src=lst.get(0);
		if (src==-1)
			return big-num;
		int small=0;
		int subst=0;
		for (int i=0;i<lst.size();i++) {
			if (src==1 && i==0)
				continue;
			int d = lst.get(i);
			if (src==d) {
				if (i==0)
					subst=1;
				d=subst;
			}
			small=small*10+d;
		}
		return big-small;
	}

	private static List<Integer> digits(int num) {
		List<Integer> lst = new ArrayList<>();
		while (num>0) {
			lst.add(num%10);
			num/=10;
		}
		Collections.reverse(lst);
		return lst;
	}

	private static int eights(int num) {
		int res=0;
		while (num>0) {
			res=res*10+8;
			num/=10;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int num=3709453;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=1101057;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=9;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=123456;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=10000;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=9288;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=99;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=991;
		System.out.println(""+num+" -> "+maxDiff(num));
		num=1991;
		System.out.println(""+num+" -> "+maxDiff(num));

	}

}
