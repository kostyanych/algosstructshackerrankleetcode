package kostyanych.leetcode;

import java.util.Arrays;

/**
 * You are given an array of network towers towers and an integer radius,
 * 	where towers[i] = [xi, yi, qi] denotes the ith network tower with location (xi, yi) and quality factor qi.
 * All the coordinates are integral coordinates on the X-Y plane, and the distance between two coordinates is the Euclidean distance.
 *
 * The integer radius denotes the maximum distance in which the tower is reachable.
 * The tower is reachable if the distance is less than or equal to radius.
 * Outside that distance, the signal becomes garbled, and the tower is not reachable.
 *
 * The signal quality of the ith tower at a coordinate (x, y) is calculated with the formula (floor)(qi / (1 + d)),
 * 	 where d is the distance between the tower and the coordinate.
 *
 * The network quality at a coordinate is the sum of the signal qualities from all the reachable towers.
 *
 * Return the integral coordinate where the network quality is maximum.
 * If there are multiple coordinates with the same network quality, return the lexicographically minimum coordinate.
 *
 * Note:
 * A coordinate (x1, y1) is lexicographically smaller than (x2, y2) if either x1 < x2 or x1 == x2 and y1 < y2.
 *
 * Constraints:
 * 	1 <= towers.length <= 50
 * 	towers[i].length == 3
 * 	0 <= xi, yi, qi <= 50
 * 	1 <= radius <= 50
 */
public class CoordinateWithMaximumNetworkQuality {

	//this solution is a very slightly optimized bruteforce
	//NOTE: some say that max quality will always be at station locations, but I can't see the proof of that, so fuck it
	static private int[] bestCoordinate(int[][] towers, int radius) {
        int[][] mx = new int[51][51];

        for (int[] tower : towers) {
        	for (int x=0;x<=50;x++) {
        		int dx=Math.abs(tower[0]-x);
        		for (int y=0;y<=50;y++) {
        			int dy=Math.abs(tower[1]-y);
        			double dist =Math.sqrt(dx*dx+dy*dy);
        			if (dist>radius) {
        				if (y>tower[1])
        					break;
        				continue;
        			}
            		double qq=1+dist;
            		int q=(int)(tower[2]/qq);
            		if (q > 0) {
            			mx[x][y]+=q;
            		}
        		}
        	}
        }
        int[] res = new int[2];
        int max=0;
    	for (int x=0;x<=50;x++) {
    		for (int y=0;y<=50;y++) {
    			if (max<mx[x][y]) {
    				res[0]=x;
    				res[1]=y;
    				max=mx[x][y];
    			}
    		}
    	}
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

//		int[][] arr = {{1,2,5}, {2,1,7}, {3,1,9}};
//		System.out.println(Arrays.toString(bestCoordinate(arr,2)));

		int[][] arr = {{36,33,42},{30,1,49},{16,14,2},{29,8,0},{5,5,22},{15,14,31},{15,32,12},{6,17,31},{1,38,18},{49,48,18},{49,15,0},{32,23,32},{50,32,28},{33,12,30},{28,30,44},{5,27,38},{5,6,32},{22,49,17},{40,37,28}};
		int r=49;
		System.out.println(Arrays.toString(bestCoordinate(arr,r)));
	}


}
