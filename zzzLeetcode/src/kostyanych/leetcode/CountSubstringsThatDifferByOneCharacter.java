package kostyanych.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Given two strings s and t, find the number of ways you can choose a non-empty substring of s
 * 		and replace a single character by a different character such that the resulting substring is a substring of t.
 * In other words, find the number of substrings in s that differ from some substring in t by exactly one character.
 *
 * For example, the capitalized substrings in "COMPUTEr" and "COMPUTAtion" only differ by the 'e'/'a', so this is a valid way.
 *
 * Return the number of substrings that satisfy the condition above.
 * A substring is a contiguous sequence of characters within a string.
 *
 * Constraints:
 * 	1 <= s.length, t.length <= 100
 * 	s and t consist of lowercase English letters only.
 *
 */
public class CountSubstringsThatDifferByOneCharacter {

	private static int countSubstrings(String s, String t) {
		int tlen=t.length();
		int slen=Math.min(s.length(), tlen);
		int res=0;
		Map<String, Integer> map = new HashMap<>();
		for (int i=slen;i>0;i--) {
			map.clear();
			for (int j=0;j<=tlen-i;j++) {
				String ss = t.substring(j,j+i);
				map.compute(ss, (k,v)->v==null ? 1 : v+1);
			}

			for (int j=0;j<=slen-i;j++) {
				String ss = s.substring(j,j+i);
				res+=check(map,ss);
			}
		}
		return res;
    }

	private static int check(Map<String,Integer> map, String ss) {
		int res=0;
		for (String data : map.keySet()) {
			if (substOk(ss,data))
				res+=map.get(data);
		}
		return res;
	}

	private static boolean substOk(String a, String b) {
		char[] ca = a.toCharArray();
		char[] cb = b.toCharArray();
		boolean changed=false;
		for (int i=0;i<ca.length;i++) {
			if (ca[i]==cb[i])
				continue;
			if (changed)
				return false;
			changed=true;
		}
		if (!changed)
			return false;
		return true;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		System.out.println(countSubstrings("aba","baba"));
	}

}
