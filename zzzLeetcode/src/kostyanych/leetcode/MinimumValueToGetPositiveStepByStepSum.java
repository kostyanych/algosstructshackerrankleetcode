package kostyanych.leetcode;

/**
 * Given an array of integers nums, you start with an initial positive value startValue.
 *
 * In each iteration, you calculate the step by step sum of startValue plus elements in nums (from left to right).
 *
 * Return the minimum positive value of startValue such that the step by step sum is never less than 1.
 */
public class MinimumValueToGetPositiveStepByStepSum {

	private static int minStartValue(int[] nums) {
		int len = nums.length;
		int sum = nums[0];
		int min = nums[0];
		for (int i = 1; i < len; i++) {
			sum += nums[i];
			if (sum < min)
				min = sum;
		}
		if (min > 0)
			return 1;
		else
			return 1 - min;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {-3,2,-3,4,2};
		System.out.println(minStartValue(arr));
		arr = new int[]{1,2};
		System.out.println(minStartValue(arr));
		arr = new int[]{1,-2,-3};
		System.out.println(minStartValue(arr));
		arr = new int[]{0,-1,1};
		System.out.println(minStartValue(arr));
	}

}
