package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

import kostyanych.leetcode.helpers.Printer;

/**
 * A city's skyline is the outer contour of the silhouette formed by all the buildings in that city when viewed from a distance.
 * Now suppose you are given the locations and height of all the buildings as shown on a cityscape photo (Figure A),
 * write a program to output the skyline formed by these buildings collectively (Figure B).
 *
 * Buildings Skyline Contour
 * The geometric information of each building is represented by a triplet of integers [Li, Ri, Hi],
 * where Li and Ri are the x coordinates of the left and right edge of the ith building, respectively, and Hi is its height.
 * It is guaranteed that 0 ≤ Li, Ri ≤ INT_MAX, 0 < Hi ≤ INT_MAX, and Ri - Li > 0.
 * You may assume all buildings are perfect rectangles grounded on an absolutely flat surface at height 0.
 *
 * For instance, the dimensions of all buildings in Figure A are recorded as: [ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ] .
 *
 * The output is a list of "key points" (red dots in Figure B) in the format of [ [x1,y1], [x2, y2], [x3, y3], ... ]
 * that uniquely defines a skyline. A key point is the left endpoint of a horizontal line segment.
 * Note that the last key point, where the rightmost building ends, is merely used to mark the termination of the skyline, and always has zero height.
 * Also, the ground in between any two adjacent buildings should be considered part of the skyline contour.
 *
 * For instance, the skyline in Figure B should be represented as:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ].
 *
 * Notes:
 *
 * The number of buildings in any input list is guaranteed to be in the range [0, 10000].
 * The input list is already sorted in ascending order by the left x position Li.
 * The output list must be sorted by the x position.
 * There must be no consecutive horizontal lines of equal height in the output skyline. For instance, [...[2 3], [4 5], [7 5], [11 5], [12 7]...] is not acceptable; the three lines of height 5 should be merged into one in the final output as such: [...[2 3], [4 5], [12 7], ...]
 *
 */
public class TheSkylineProblem {

	static private List<List<Integer>> getSkyline(int[][] buildings) {
		List<List<Integer>> res = new ArrayList<>();
		if (buildings==null || buildings.length<1)
			return res;

		// sort building by x1, then by height in ascending
		PriorityQueue<int[]> pqleft = new PriorityQueue<>((a, b) -> a[0] <= b[0] ? -1 : 1);
		for(int[] b : buildings)
			pqleft.add(b);

		int[] prev = null;
		while(!pqleft.isEmpty()) {
			int[] curB = pqleft.poll();

			while(!pqleft.isEmpty() && pqleft.peek()[0] < curB[1]) { // next one starts before current one ends
				int[] nextB = pqleft.poll();
				int xl1 = curB[0];
				int xr1 = curB[1];
				int h1 = curB[2];

				int xl2 = nextB[0];
				int xr2 = nextB[1];
				int h2 = nextB[2];

				if(h1 <= h2) {//next one is taller (and ends before this one)
					if(xr1 > xr2)
						pqleft.add(new int[]{xr2, xr1, h1}); //add a fake building starting where next one ends and ending where current one ends. with height of current

					if(xl1 == xl2) //current and net astart at the same point
						curB = nextB;
					else {
						curB = new int[]{xl1, xl2, h1}; //will only look at current as a part of it from it's start to next one's start
						pqleft.add(nextB); //but it back, process it separately
					}
				}
				else {
					if(xr1 < xr2) //this one is taller (or equal) and ends before the other one ends
						pqleft.add(new int[]{xr1, xr2, h2}); //add a fake building starting where current one ends and ending where the next one ends. with height of next
				}
			}

			if(prev == null) //no prev building. Add FRONT
				res.add(Arrays.asList(curB[0], curB[2]));
			else if(curB[0] > prev[1]) { //current one has an x gap after prev one. Add 0 BACK to prev and FRONT to current
				res.add(Arrays.asList(prev[1], 0));
				res.add(Arrays.asList(curB[0], curB[2]));
			}
			else { // curr and prev stand side to side (we cannot have overlaps with uor algo)
				if(curB[2] != prev[2])
					res.add(Arrays.asList(curB[0], curB[2])); //we always use Y of the NEXT building of different height. it can either be FRONT or BACK
			}
			prev = curB;
		}

		if(prev != null) //if there's a prev (at least one building was processed) we need to register it's BACK
			res.add(Arrays.asList(prev[1], 0));

		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr = { {2,9,10}, {3,7,15}, {5,12,12}, {15,20,10}, {19,24,8}};
		Printer.print2DList(getSkyline(arr), true);
	}
}
