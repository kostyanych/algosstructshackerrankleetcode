package kostyanych.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A sequence X_1, X_2, ..., X_n is fibonacci-like if:
 *
 * - n >= 3
 * - X_i + X_{i+1} = X_{i+2} for all i + 2 <= n
 *
 * Given a strictly increasing array A of positive integers forming a sequence,
 * find the length of the longest fibonacci-like subsequence of A.  If one does not exist, return 0.
 *
 * (Recall that a subsequence is derived from another sequence A by deleting any number of elements (including none) from A,
 *     without changing the order of the remaining elements.
 *     For example, [3, 5, 8] is a subsequence of [3, 4, 5, 6, 7, 8])
 *
 *  3 <= A.length <= 1000
 *  1 <= A[0] < A[1] < ... < A[A.length - 1] <= 10^9
 */
public class LengthOfLongestFibonacciSubsequence {

	/**
	 * For every 3 fibo-like numbers we store sequnce length indexing by (last, last-1)
	 * Then we can just add 1 if thie sequence has next values
	 *
	 * For example :
	 * {1,2,3,4,5,6,7,8}
	 * 1,2,3 are fibo-like and have length of 3. we store it and [2,3]
	 * then, when we arrive to 5, we check 5 against 3 and find that we have 2 as a candidate start
	 * we then check storage at [2,3] and see that we already have length of 3 then.
	 * So at [3,5] we have the len of 4
	 * Then, when we arrive to 8, we check 8 against 5 and find that we have 3 as a candidate start
	 * we then check storage at [3,5] and see that we already have length of 4 then.
	 * So at [5,8] we have the len of 5
	 *
	 * @return
	 */
	private static int lenLongestFibSubseq(int[] A) {
		int len=A.length;

		Map<Integer,Integer> map = new HashMap<>();
		for (int i=0;i<len;i++) {
			map.put(A[i],i);
		}

		int[][] prev = new int[len][len];

		int res=0;
		for (int i=2;i<len;i++) {
			for (int j=1;j<i;j++) {
				int prevNum=A[i]-A[j];
				int prevNumInd = map.getOrDefault(prevNum, -1);
				if (prevNumInd>=0 && prevNumInd<j) {
					if (prev[prevNumInd][j]==0)
						prev[j][i]=3;
					else
						prev[j][i]=prev[prevNumInd][j]+1;
					if (prev[j][i]>res)
						res=prev[j][i];
				}
			}
		}
		return res;

	}

	private static int lenLongestFibSubseqBruteFaster(int[] A) {
		int len=A.length;

		Map<Integer,Integer> map = new HashMap<>();
		for (int i=0;i<len;i++) {
			map.put(A[i],i);
		}

		boolean[][] used = new boolean[len][len];

		int res=0;
		int upperBound=len-2;

		for (int i=0;i<upperBound;i++) {
			for (int j=i+1;j<upperBound+1;j++) {
				if (used[i][j])
					continue;
				int curLen=2;
				int first=A[i];
				int second=A[j];
				int next;
				while (map.containsKey(next=first+second)) {
					curLen++;
					first=second;
					second=next;
					used[map.get(first)][map.get(second)]=true;
				}
				if (curLen>2 && curLen>res) {
					res=curLen;
					upperBound=len-res;
				}
			}
		}

		return res;
	}

	private static int lenLongestFibSubseqBruteForce(int[] A) {
		Set<Integer> set = new HashSet<>();
		for (int i : A) {
			set.add(i);
		}

		int len=A.length;

		int res=0;
		int upperBound=len-2;

		for (int i=0;i<upperBound;i++) {
			for (int j=i+1;j<upperBound+1;j++) {
				int curLen=2;
				int first=A[i];
				int second=A[j];
				int next;
				while (set.contains(next=first+second)) {
					curLen++;
					first=second;
					second=next;
				}
				if (curLen>2 && curLen>res) {
					res=curLen;
					upperBound=len-res;
				}
			}
		}

		return res;
	}

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;

		 //int[] arr = {1,2,3,4,5,6,7,8};
		 int[] arr= {2,4,7,8,9,10,14,15,18,23,32,50};
		 System.out.println(Arrays.toString(arr));
		 System.out.println(lenLongestFibSubseq(arr));
		 arr = new int[] {1,3,7,11,12,14,18};
		 System.out.println(Arrays.toString(arr));
		 System.out.println(lenLongestFibSubseq(arr));
	 }

}
