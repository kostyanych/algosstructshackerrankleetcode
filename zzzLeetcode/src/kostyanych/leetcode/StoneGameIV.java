package kostyanych.leetcode;

/**
 * Alice and Bob take turns playing a game, with Alice starting first.
 *
 * Initially, there are n stones in a pile.
 * On each player's turn, that player makes a move consisting of removing any non-zero square number of stones in the pile.
 *
 * Also, if a player cannot make a move, he/she loses the game.
 *
 * Given a positive integer n return True if and only if Alice wins the game otherwise return False, assuming both players play optimally.
 *
 * SOLUTION:
 * Build an list of results from 1 to n;
 * 1 - player wins
 * 2 - player loses
 * 3 and further n - analyze all possible Alice moves (n - perfectSquare). If any of them leads to second player "lose" result, then at n Alice wins.
 *
 */
public class StoneGameIV {

	static int[] squares=null;

	private static boolean winnerSquareGame(int n) {
		if (n==1)
			return true;
		if (n==2)
			return false;

		if (squares==null)
			initSquares();

		boolean[] res=new boolean[n+1];
		res[1]=true;
		res[2]=false;
		for (int i=3;i<=n;i++) {
			res[i]=calc(res,i);
		}
		return res[n];
	}

	private static void initSquares() {
		int end=(int)Math.sqrt(100000)+1;
		squares=new int[end];
		for (int i=1;i<=end;i++) {
			squares[i-1]=i*i;
		}
	}

	private static boolean calc(boolean[] res, int cur) {
		if (isSquare(cur))
			return true;
		for (int sq : squares) {
			if (sq>cur)
				break;
			if (!res[cur-sq])
				return true;
		}
		return false;
	}

	private static boolean isSquare(int n) {
		int s=(int)Math.sqrt(n);
		if (s*s==n)
			return true;
		return false;
	}


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n=1;
		System.out.println(n+" -> "+winnerSquareGame(n));
		n=2;
		System.out.println(n+" -> "+winnerSquareGame(n));
		n=3;
		System.out.println(n+" -> "+winnerSquareGame(n));
		n=4;
		System.out.println(n+" -> "+winnerSquareGame(n));
		n=7;
		System.out.println(n+" -> "+winnerSquareGame(n));
		n=17;
		System.out.println(n+" -> "+winnerSquareGame(n));
	}

}
