package kostyanych.leetcode;

/**
 * Given an integer array nums, return the sum of floor(nums[i] / nums[j]) for all pairs of indices 0 <= i, j < nums.length in the array.
 * Since the answer may be too large, return it modulo 109 + 7.
 *
 * The floor() function returns the integer part of the division.
 *
 * Constraints:
 * 1 <= nums.length <= 10^5
 * 1 <= nums[i] <= 10^5
 *
 */
public class SumOfFlooredPairs {

	public static final int MOD = 1_000_000_007;

    static private int sumOfFlooredPairs(int[] nums) {
    	int max = 0;
    	for (int n : nums) {
    		max = Math.max(max, n);
    	}

    	int[] counts = new int[max + 1];
        for (int num : nums) {
            counts[num]++;
        }

        for (int i = 1; i <= max; i++) {
            counts[i] += counts[i - 1];
        }

        long res = 0;

        for (int i = 1; i <= max; i++) {
        	long count_i=counts[i]-counts[i-1];
        	if (count_i<1)
        		continue;
        	long sum = 0;
            //now consider all intervals like X in [i;2i), [2i;3i), [3i;4i) where the floor of X/i would be 1,2,3 etc
            //so we'll go thru [j*i;(j+1)*i) where j will start with 1
            for (int j = 1; i * j <= max; j++) {
            	int lower = i * j;
            	int upper = i * (j + 1) - 1;
                upper = Math.min(upper,  max); //upper may exceed max, but we only have nummbers up to max
                int count = counts[upper] - counts[lower-1]; //we're 1-indexed, so for 1 we have counts[0]=0
                sum += (count * (long)j);
            }

            res = (res + (sum % MOD) * count_i) % MOD;
        }
        return (int)res;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {7,7,7};
		System.out.println(sumOfFlooredPairs(arr));
	}
}
