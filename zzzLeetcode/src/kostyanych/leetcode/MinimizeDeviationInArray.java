package kostyanych.leetcode;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.TreeSet;

/**
 * You are given an array nums of n positive integers.
 *
 * You can perform two types of operations on any element of the array any number of times:
 *
 * If the element is even, divide it by 2.
 * 		For example, if the array is [1,2,3,4], then you can do this operation on the last element, and the array will be [1,2,3,2].
 * If the element is odd, multiply it by 2.
 * 		For example, if the array is [1,2,3,4], then you can do this operation on the first element, and the array will be [2,2,3,4].
 *
 * The deviation of the array is the maximum difference between any two elements in the array.
 *
 * Return the minimum deviation the array can have after performing some number of operations.
 *
 * SOLUTION:
 * 1. we can minize even number many times (16->8->4->2->1), but can only maximize odd number once (1->2, 9->18)
 * 		so we maximize all we can
 * 2. while doing this we note the max and min values in a new sequence
 * 3. the resulting diff is a starting point
 * 4. then take the max value in a new seqeunce: if it's even, we can divide it by 2 and check if this improve matters:
 * 		first divide it by 2 and place it back into the sequence
 * 		it can now become the overall min, so update your min  (3,5 -> 6,10 (min=6) -> 6,5 (min=5))
 * 		then check Max(sequence)-min
 * 5. repeat that while max(sequence) is even
 * 6. for easy handling of the new sequeunce use a PriorityQueue (in descending order)
 *
 */
public class MinimizeDeviationInArray {

	static private int minimumDeviation(int[] nums) {
		PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.reverseOrder());
		int max=nums[0];
		int min=Integer.MAX_VALUE;
        for(int i: nums){
        	int tmp=i;
            if(i % 2 != 0)
                tmp*=2;
            max=Math.max(max, tmp);
            min=Math.min(min, tmp);
            pq.add(tmp);
        }
        int res=max-min;

        while(!pq.isEmpty() && pq.peek() % 2 == 0){
            int tmp = pq.poll();
            tmp/=2;
            pq.add(tmp);
            min=Math.min(min, tmp);
            res= Math.min(res, Math.abs(pq.peek() - min));
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[] arr= {2,3,1,1,4};
		int[] arr= {3,5};
		System.out.println(minimumDeviation(arr));

	}

}
