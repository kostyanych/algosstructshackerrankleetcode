package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * There are N network nodes, labelled 1 to N. Given times, a list of travel
 * times as directed edges times[i] = (u, v, w), where u is the source node, v
 * is the target node, and w is the time it takes for a signal to travel from
 * source to target.
 * 
 * Now, we send a signal from a certain node K. How long will it take for all
 * nodes to receive the signal? If it is impossible, return -1.
 *
 */
public class NetworkDelayTime {

	private static int networkDelayTime(int[][] times, int N, int K) {
		int[] visited = new int[N + 1];
		Arrays.fill(visited, -1);
		Map<Integer, List<int[]>> map = new HashMap<>();
		int len = times.length;
		for (int i = 0; i < len; i++) {
			List<int[]> targets = map.computeIfAbsent(times[i][0], (x) -> new ArrayList<int[]>());
			targets.add(new int[] { times[i][1], times[i][2] });
		}
		Queue<int[]> q = new LinkedList<>();
		q.add(new int[] { K, 0 });
		while (!q.isEmpty()) {
			int[] cur = q.remove();
			if (visited[cur[0]]>=0 && visited[cur[0]] <= cur[1])
				continue;
			List<int[]> nexts = map.get(cur[0]);
			if (nexts != null && nexts.size() >0) {
				for (int i = 0; i < nexts.size(); i++) {
					int[] next = nexts.get(i);
					if (visited[cur[0]]>=0 && visited[cur[0]] <= cur[1])
						continue;
					q.add(new int[] { next[0], next[1] + cur[1] });
				}
			}
			visited[cur[0]] = cur[1];
		}

		Arrays.sort(visited);
		if (visited[1] < 0)
			return -1;
		return visited[N];
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr={{1,2,1},{2,3,7},{1,3,4},{2,1,2}};
		System.out.println(networkDelayTime(arr,3,2));
	}
}
