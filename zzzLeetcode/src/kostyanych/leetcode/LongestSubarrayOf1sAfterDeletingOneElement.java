package kostyanych.leetcode;

/**
 * Given a binary array nums, you should delete one element from it.
 *
 * Return the size of the longest non-empty subarray containing only 1's in the resulting array.
 *
 * Return 0 if there is no such subarray.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 10^5
 * nums[i] is either 0 or 1.
 *
 */
public class LongestSubarrayOf1sAfterDeletingOneElement {

	private static int longestSubarray(int[] nums) {
		int len = nums.length;
		if (len<1)
			return 0;
		int[] memo = new int[len];
		memo[0]=nums[0];
		for (int i=1;i<len;i++) {
			if (nums[i]==0)
				memo[i]=0;
			else
				memo[i]=memo[i-1]+1;
		}

		for (int i=len-2;i>=0;i--) {
			if (nums[i]==0 || nums[i+1]==0)
				continue;
			memo[i]=memo[i+1];
		}

		boolean wasDelete=false;
		int res=memo[0];
		for (int i=1;i<len;i++) {
			if (memo[i]==0) {
				int cur=memo[i-1];
				if (i<len-1)
					cur+=memo[i+1];
				if (cur>=res) {
					res=cur;
					wasDelete=true;
				}
			}
			else {
				if (res==0)
					res=memo[i];
			}
		}

		return wasDelete?res:res-1;

    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {1,1,0,0,1,1,0,0,1};
		System.out.println(longestSubarray(arr));
	}

}
