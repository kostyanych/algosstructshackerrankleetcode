package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * You want to form a target string of lowercase letters.
 *
 * At the beginning, your sequence is target.length '?' marks.  You also have a stamp of lowercase letters.
 *
 * On each turn, you may place the stamp over the sequence, and replace every letter in the sequence with the corresponding letter from the stamp.
 * You can make up to 10 * target.length turns.
 *
 * For example, if the initial sequence is "?????", and your stamp is "abc",
 * 		then you may make "abc??", "?abc?", "??abc" in the first turn.
 * (Note that the stamp must be fully contained in the boundaries of the sequence in order to stamp.)
 *
 * If the sequence is possible to stamp, then return an array of the index of the left-most letter being stamped at each turn.
 * If the sequence is not possible to stamp, return an empty array.
 *
 * For example, if the sequence is "ababc", and the stamp is "abc", then we could return the answer [0, 2], corresponding to the moves "?????" -> "abc??" -> "ababc".
 *
 * Also, if the sequence is possible to stamp, it is guaranteed it is possible to stamp within 10 * target.length moves.
 * Any answers specifying more than this number of moves will not be accepted.
 *
 * Note:
 *
 * 1 <= stamp.length <= target.length <= 1000
 * stamp and target only contain lowercase letters.
 *
 */
public class StampingTheSequence {

	static private int targetLen;
	static private int stampLen;
	static private char[] cs;
	static private boolean[] visited;

	static private char START = '?';

	static private int[] movesToStamp(String stamp, String target) {
        List<Integer> reverseIndices = new ArrayList<>();
        targetLen = target.length();
        stampLen = stamp.length();
        cs = stamp.toCharArray();
        char[] curr = target.toCharArray();
        char[] empty = new char[targetLen];
        visited = new boolean[targetLen];
        Arrays.fill(empty, START);

        while (!Arrays.equals(curr, empty)) {
        	int stampFound = fetchStampIndex(curr);
            if(stampFound<0)
                return new int[0];
            else
                update(curr, stampFound);
            reverseIndices.add(stampFound);
            visited[stampFound]=true;
        }

        int[] res = new int[reverseIndices.size()];

        for(int i=0;i<res.length;i++){
            res[i] = reverseIndices.get(reverseIndices.size()-i-1);
        }
        return res;
    }

    static private int fetchStampIndex(char[] curr){
//System.out.println( new String(curr));
        for( int i=0; i<=targetLen-stampLen; i++){
        	if (visited[i])
        		continue;
            int stampInd=0;
            int targetInd = i;

            boolean atLeastOneChar = false;

            while( stampInd < stampLen
            		&& targetInd < targetLen
            		&& (curr[targetInd] == START || curr[targetInd] == cs[stampInd])
            		) {

            	if(curr[targetInd] != START)
            		atLeastOneChar = true;
                targetInd++;
                stampInd++;
            }

            if (stampInd == stampLen && atLeastOneChar)
                return i;
        }
        return -1;
    }

    static private void update(char[] curr, int i){
        for (int j=0;j<stampLen;j++){
            curr[j+i] = START;
        }
    }


/*
	static private int[] movesToStamp(String stamp, String target) {
        char[] cs = stamp.toCharArray();
        char[] ct = target.toCharArray();
        int slen = cs.length;
        int tlen = ct.length;
        List<Integer> l = work(cs, ct, slen, 0, tlen-1);
        if (l==null)
            return new int[0];
        int[] res = new int[l.size()];
        for (int i=0;i<l.size();i++) {
            res[i]=l.get(i);
        }
        return res;
    }

	static private List<Integer> work(char[] cs, char[] ct, int slen, int left, int right) {
        int curlen=right-left+1;
        if (slen > curlen)
            return null;
        if (ct[left]!=cs[0] && ct[right]!=cs[slen-1])
            return null;
        List<Integer> res = new ArrayList<>();
        if (ct[left]==cs[0]) {//start from left
            int l=1;
            while(l<slen) {
            	char charSt=cs[l];
            	char charTg=ct[left+l];
                if (ct[left+l]==cs[l])
                    l++;
                else
                    break;
            }
            if (l==slen) { //this is the topmost
                if (l!=curlen) { //there's something underneath
                    res = work(cs,ct,slen,left+l,right);
                    if (res==null)
                        return null;
                }
                res.add(left);
            }
            else { //this is under something else
                res.add(left);
                List<Integer> over = work(cs,ct,slen,left+l,right);
                if (over==null)
                    return null;
                res.addAll(over);
            }
        }
        else {// could not start from left, start from right
            int l=1;
            while(l<slen) {
                if (ct[right-1]==cs[slen-1-l])
                    l++;
                else
                    break;
            }
            if (l==slen) { //this is the topmost
                if (l!=curlen) { //there's something underneath
                    res = work(cs,ct,slen,left,right-l);
                    if (res==null)
                        return null;
                }
                res.add(right-l+1);
            }
            else { //this is under something else
                res.add(right-l+1);
                List<Integer> over = work(cs,ct,slen,left,right-l);
                if (over==null)
                    return null;
                res.addAll(over);
            }
        }
        return res;
    }
*/
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(Arrays.toString(movesToStamp("abca",				"aabcaca")));
	}

}
