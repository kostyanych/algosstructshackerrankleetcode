package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an integer n, return a list of all simplified fractions between 0 and 1 (exclusive)
 * such that the denominator is less-than-or-equal-to n. The fractions can be in any order.
 *
 * Constraints:
 *
 * 1 <= n <= 100
 */
public class SimplifiedFractions {

	private static List<String> simplifiedFractions(int n) {
		List<String> res = new ArrayList<>();
		if (n<2)
			return res;
		for (int den=2;den<=n;den++) {
			for (int nom=1;nom<den;nom++ ) {
				if (nom%2==0 && den%2==0)
					continue;
				if (den%3==0 && nom%3==0)
					continue;
				if (den%5==0 && nom%5==0)
					continue;
				if (den%7==0 && nom%7==0)
					continue;
				if (den%11==0 && nom%11==0)
					continue;
				if (den%13==0 && nom%13==0)
					continue;
				if (den%17==0 && nom%17==0)
					continue;
				if (eucl(den,nom)!=1)
					continue;
				res.add(nom+"/"+den);
			}
		}
		return res;
    }

	static int eucl(int p, int q) {
        int temp;
        while (q != 0) {
            temp = q;
            q = p % q;
            p = temp;
        }
        return p;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("30 -> "+simplifiedFractions(30));
	}
}
