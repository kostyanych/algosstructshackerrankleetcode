package kostyanych.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * Suppose you have a long flowerbed in which some of the plots are planted and some are not.
 * However, flowers cannot be planted in adjacent plots - they would compete for water and both would die.
 *
 * Given a flowerbed (represented as an array containing 0 and 1, where 0 means empty and 1 means not empty),
 * and a number n, return if n new flowers can be planted in it without violating the no-adjacent-flowers rule.
 *
 * Note:
 *    The input array won't violate no-adjacent-flowers rule.
 * 	  The input array size is in the range of [1, 20000].
 *    n is a non-negative integer which won't exceed the input array size.
 */
public class CanPlaceFlowers {

	private final static Set<String> failedSet = new HashSet<>();

	private static boolean canPlaceFlowers(int[] flowerbed, int n) {
		if (n==0)
			return true;
		int len = flowerbed.length;
		for (int i=0;i<len;i++) {
			if (flowerbed[i]==1)
				continue;
			if (i==0 || flowerbed[i-1]!=1 ) {
				if (i==len-1 || flowerbed[i+1]!=1) {
					if (backtrack(flowerbed, i, n))
						return true;
				}
			}
		}
		return false;

	}

	private static boolean backtrack(int[] flowerbed, int ind, int n) {
		if (n==0)
			return true;
		String key=ind+"|"+n;
		if (failedSet.contains(key))
			return false;
		int len = flowerbed.length;
		for (int i=ind;i<len;i++) {
			if (flowerbed[i]==1)
				continue;
			if (i==0 || flowerbed[i-1]!=1 ) {
				if (i==len-1 || flowerbed[i+1]!=1) {
					flowerbed[i]=1;
					if (backtrack(flowerbed, i, n-1))
						return true;
					flowerbed[i]=0;
				}
			}
		}
		failedSet.add(key);
		return false;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] flowerbed = {1,0,0,0,0,0,1};
		int n = 3;
		System.out.println(canPlaceFlowers(flowerbed, n));
	}

}
