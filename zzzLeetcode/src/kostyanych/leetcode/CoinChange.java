package kostyanych.leetcode;

import java.util.Arrays;

/**
 * You are given coins of different denominations and a total amount of money amount.
 * Write a function to compute the fewest number of coins that you need to make up that amount.
 * If that amount of money cannot be made up by any combination of the coins, return -1.
 *
 * Note:
 * You may assume that you have an infinite number of each kind of coin.
 */
public class CoinChange {

	/**
	 * we use array[amount+1] to store the optimal coins count
	 * 1. fill it with something like INF (anything > amount will do)
	 * 2. arr[0]=0
	 * 3. iterate thru coins
	 * 4. for each coin iterate thru 1 to amount (j)
	 * 5. at each step:
	 * 5a. if coin is bigger than j, we can do nothing
	 * 5b. if coin==j, optimal is obviously 1
	 * 5c. if coin<j, the optimal coin count would be min(arr[j],1+arr[j-coin])
	 *    arr[j] - whatever optimal we achieved with previous coins at j
	 *    1+arr[j-coin] - this 1 coin and whatever optimal we achieved with previous coins at amount = j-coin
	 * @param coins
	 * @param amount
	 * @return
	 */
	private static int coinChange(int[] coins, int amount) {
		if (coins==null)
			return -1;

		if (amount==0)
			return 0;

		int len=coins.length;

		if (len==0)
			return -1;

		int[] memo = new int[amount + 1]; //from 0 to amount inclusive
		Arrays.fill(memo, amount + 1);  // set to something unreachably big
		memo[0] = 0; // as we need 0 of any coin to make 0 amount

		for(int i = 0; i < len; i++){
			int coin=coins[i];
			for(int j = 1; j <= amount; j++){
				// if coin is equal to current amount, then min coins = 1
				// if coin is less than current amount, then we can use 1 this coin + whatever we used at [j-coin]
				//    or don't use this coin if it's optimal without it already
				// if the coin is greater than current amount, we can do nothing with it
				if (coin==j)
					memo[j]=1;
				else if(coin < j)
					memo[j] = Math.min(memo[j], memo[j - coin] + 1);
			}
		}
		return memo[amount] > amount ? -1 : memo[amount];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] coins = {186,419,83,408};
		int amount = 6249;
		System.out.println(coinChange(coins,amount));
	}

}
