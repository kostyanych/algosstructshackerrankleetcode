package kostyanych.leetcode.geometry;

/**
 * You are given an array coordinates, coordinates[i] = [x, y], where [x, y]
 * represents the coordinate of a point. Check if these points make a straight
 * line in the XY plane.
 *
 * Constraints:
 *
 * * 2 <= coordinates.length <= 1000 * coordinates[i].length == 2 * -10^4 <=
 * coordinates[i][0], coordinates[i][1] <= 10^4 * coordinates contains no
 * duplicate point.
 */
public class CheckIfItIsAStraightLine {

	private final static double SIGMA = 0.00000000000000001;

	/**
	 * Cross product (векторное произведение) of collinear vectors equals zero.
	 * Collinear vectors starting from the same point lie on the same line.
	 * @param coordinates
	 * @return
	 */
	private static boolean checkStraightLineWithCrossProduct(int[][] coordinates) {
		int len = coordinates.length;
		if (len == 2)
			return true;
		int[] point1 = coordinates[0];
		int[] vector1 = {coordinates[1][0]-point1[0],coordinates[1][1]-point1[1]};
		for (int i=2;i<len;i++) {
			int[] vector2 = {coordinates[i][0]-point1[0],coordinates[i][1]-point1[1]};
			int crossProduct=vector1[0]*vector2[1]-vector1[1]*vector2[0];
			if (crossProduct!=0)
				return false;
		}
		return true;
	}

	private static boolean checkStraightLine(int[][] coordinates) {
		if (coordinates.length == 2)
			return true;
		int[] point1 = coordinates[0];
		int[] point2;
		if (coordinates[1][0] < point1[0]) {
			point2 = point1;
			point1 = coordinates[1];
		} else
			point2 = coordinates[1];

		boolean vertical = false;
		boolean horizontal = false;
		if (point1[0] == point2[0])
			vertical = true;
		if (point1[1] == point2[1])
			horizontal = true;

		if (horizontal)
			return checkHorizontal(coordinates, point1[1]);
		if (vertical)
			return checkVertical(coordinates, point1[0]);
		final double k = ((double) (point2[1] - point1[1])) / (point2[0] - point1[0]);
		final double b = point1[1] - k * point1[0];
		return checkLine(coordinates, k, b);
	}

	private static boolean checkHorizontal(int[][] coordinates, int y) {
		for (int i = 2; i < coordinates.length; i++) {
			if (coordinates[i][1] != y)
				return false;
		}
		return true;
	}

	private static boolean checkVertical(int[][] coordinates, int x) {
		for (int i = 2; i < coordinates.length; i++) {
			if (coordinates[i][0] != x)
				return false;
		}
		return true;
	}

	private static boolean checkLine(int[][] coordinates, double k, double b) {
		for (int i = 2; i < coordinates.length; i++) {
			double y = coordinates[i][0] * k + b;
			if (Math.abs(y-coordinates[i][1])>SIGMA)
				return false;
		}
		return true;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		//int[][] coordinates = {{1,2},{2,3},{3,4},{4,5},{5,6},{6,7}};
		int[][] coordinates = {{-3,-2},{-1,-2},{2,-2},{-2,-2},{0,-2}};
		System.out.println(checkStraightLine(coordinates));
		System.out.println(checkStraightLineWithCrossProduct(coordinates));
		coordinates = new int[][] {{1,1},{2,2},{3,4},{4,5}, {5,6},{7,7}};
		System.out.println(checkStraightLine(coordinates));
		System.out.println(checkStraightLineWithCrossProduct(coordinates));
	}

}
