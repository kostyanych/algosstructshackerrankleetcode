package kostyanych.leetcode.geometry;

/**
 * Given a circle represented as (radius, x_center, y_center) and an
 * axis-aligned rectangle represented as (x1, y1, x2, y2), where (x1, y1) are
 * the coordinates of the bottom-left corner, and (x2, y2) are the coordinates
 * of the top-right corner of the rectangle.
 *
 * Return True if the circle and rectangle are overlapped otherwise return
 * False.
 *
 * In other words, check if there are any point (xi, yi) such that belongs to
 * the circle and the rectangle at the same time.
 */
public class CircleAndRectangleOverlapping {

	private static boolean checkOverlap(int radius, int x_center, int y_center, int x1, int y1, int x2, int y2) {
		// 0. normalize coordinates, so that center's radius would be at (0;0)
		x1 -= x_center;
		x2 -= x_center;
		y1 -= y_center;
		y2 -= y_center;
		x_center = 0;
		y_center = 0;
		// circle's center is between left and right square's coordinates
		boolean centerIsBetweenX = x1 <= 0 && x2 >= 0;
		// circle's center is between top and bottom square's coordinates
		boolean centerIsBetweenY = y1 <= 0 && y2 >= 0;
		int rsq = radius * radius;

		// 1. check if the square is positioned around the circle's center (0;0)
		// that can mean the circle is within the square, or the square is within the
		// circle
		if (centerIsBetweenX && centerIsBetweenY)
			return true;

		//2. check for every corner if its distance <= radius.
		// That would mean that the corner is ON or WITHIN the circle
		if (getSquaredDistance(x1,y1)<=rsq
			|| getSquaredDistance(x1,y2)<=rsq
			|| getSquaredDistance(x2,y2)<=rsq
			|| getSquaredDistance(x2,y1)<=rsq)
			return true;

		//3. check for every square side if it intersects the cirlce
		//for side at x1 to intersect the circle, there must:
		// - centerIsBetweenY (otherwise square is above or beneath the circle)
		// - distance between the side and the circle's center <= radius

		if (centerIsBetweenY && Math.abs(x1)<=radius)
			return true;
		if (centerIsBetweenY && Math.abs(x2)<=radius)
			return true;
		if (centerIsBetweenX && Math.abs(y1)<=radius)
			return true;
		if (centerIsBetweenX && Math.abs(y2)<=radius)
			return true;

		return false;
	}

	/**
	 * Calculates the squared distance from (x;y) to (0;0)
	 */
	private static int getSquaredDistance(int x, int y) {
		return x*x+y*y;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(checkOverlap(1, 0, 0, 1, -1, 3, 1));
		System.out.println(checkOverlap(1, 0, 0, -1, 0, 0, 1));
		System.out.println(checkOverlap(1, 1, 1, -3, -3, 3, 3));
	}

}
