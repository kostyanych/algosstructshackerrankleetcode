package kostyanych.leetcode.geometry;

import java.util.HashMap;
import java.util.Map;

import kostyanych.leetcode.helpers.Printer;

/**
 * Given n points in the plane that are all pairwise distinct,
 * 		a "boomerang" is a tuple of points (i, j, k) such that
 * 			the distance between i and j equals the distance between i and k (the order of the tuple matters).
 *
 * Find the number of boomerangs. You may assume that n will be at most 500 and coordinates of points are all
 * 		in the range [-10000, 10000] (inclusive).
 *
 */
public class NumberOfBoomerangs {

	private static int numberOfBoomerangs(int[][] points) {

        int res = 0;
        int len = points.length;
        if (len<3)
            return 0;
        for (int i = 0; i < len; i++) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int j = 0; j < len; j++) {
                if (i == j)
                    continue;
                int dx=points[i][0]-points[j][0];
                int dy=points[i][1]-points[j][1];
                int d=dx*dx+dy*dy;

                if (map.containsKey(d)){
                    int val = map.get(d);
                    res += 2 * val;
                    map.put(d, val + 1);
                }
                else
                    map.put(d, 1);
            }
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr = {{1,8},{7,9},{2,0},{2,3},{7,5},{9,2},{2,8},{9,7},{3,6},{1,2}};
		Printer.print2DArray(arr, true);
		System.out.println(numberOfBoomerangs(arr));
	}

}
