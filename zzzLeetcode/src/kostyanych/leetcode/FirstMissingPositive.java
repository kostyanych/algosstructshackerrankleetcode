package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given an unsorted integer array, find the smallest missing positive integer.
 *
 * Note:
 *
 * Your algorithm should run in O(n) time and uses constant extra space.
 */
public class FirstMissingPositive {

	/**
	 * 1. The idea is to mark array indexes that correspond to positive numbers in the array.
	 * 2. We'll mark the index by storing a negative value there.
	 * 3. But what if there's already negative value there?
	 * 4. To remedy this we'll first find whatever positive number there exists in array
	 *       and substitute all negatives and zeroes with that value.
	 *       This way we will not introduce a NEW positive number.
	 *       Obviously, if there is no positive number found at this step, the result will be 1.
	 * 5. After getting rid of negatives and zeroes we'll run thru array and for every number in the array
	 *       we will make the number at the corresponding index (number-1) a negative one.
	 *       Should check that this index is valid for the array.
	 * 6. If we hit a position where the number is already negative, that would mean there was a number before
	 *      that corresponds to that position. But we know that me made all numbers in the array to be positive.
	 *      So we just ignore the minus sign and execute step 5 on this position.
	 * 7. In the end we'll have one of two cases:
	 * 7a. All numbers in the array are negative.
	 *       That means that we have numbers in the array corresponding to all indexes.
	 *       Obviously that means that we have ALL the numbers from 1 to nums.length (both inclusive).
	 *       Obviously we can have more numbers in this case, so the result is nums.length+1
	 * 7b. Some numbers are negative, some are positive in the array.
	 *       The first index with the positive number is our result - because there no such number that would
	 *       correspond to this index.
	 * 8. Mind the fact that array indexes are zero based
	 * 9. If there were a explicitly stated constraint that nums.length<Integer.MAX_VALUE
	 *       (or whatever equivalent is there for this in your preferred language)
	 *       we could have simply changed all negatives and zeroes to nums.length+1.
	 *       This way they couldn't have meddle at step 5.
	 * @return
	 */
	private static int firstMissingPositive(int[] nums) {
		int len = nums.length;
		if (len<1)
			return 1;
		int minpos=0;
		for (int i=0;i<len;i++) {
			if (nums[i]>0) {
				if (minpos==0)
					minpos=nums[i];
				else if (minpos>nums[i])
					minpos=nums[i];
			}
		}

		if (minpos<1)
			return 1;

		for (int i=0;i<len;i++) {
			if (nums[i]<=0)
				nums[i]=minpos;
		}

		for (int i=0;i<len;i++) {
			int ind = nums[i];
			if (ind<0)
				ind=-ind;
			ind--;
			if (ind>=0 && ind<len) {
				if (nums[ind]>0)
					nums[ind]=-nums[ind];
				else if (nums[ind]==0) {
					nums[ind]=-ind;
				}
			}
		}

		System.out.println(Arrays.toString(nums));
		for (int i=0;i<len;i++) {
			if (nums[i]>0)
				return i+1;
		}
		return len+1;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {0,1,2,0};
		System.out.println(Arrays.toString(arr)+" -> "+firstMissingPositive(arr));
	}
}
