package kostyanych.leetcode;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Base64;
import java.util.Base64.Decoder;

public class Base64FileSaver {

	private static void decodeFile(String srcPath, String destPath) {

		try (BufferedReader reader = new BufferedReader(new FileReader(srcPath));
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destPath))) {

			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			String ls = System.getProperty("line.separator");
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
//				stringBuilder.append(ls);
			}
			// delete the last new line separator
//			stringBuilder.deleteCharAt(stringBuilder.length() - 1);


			Decoder dec = Base64.getMimeDecoder();
			byte[] bytes=dec.decode(stringBuilder.toString());
			bos.write(bytes);

		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		decodeFile("h:\\base64.txt","h:\\base.pdf");
	}


}
