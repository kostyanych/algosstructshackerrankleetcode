package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The n-queens puzzle is the problem of placing n queens on an n×n chessboard such that no two queens attack each other.
 * Given an integer n, return all distinct solutions to the n-queens puzzle.
 * Each solution contains a distinct board configuration of the n-queens' placement,
 *    where 'Q' and '.' both indicate a queen and an empty space respectively.
 *
 */
public class NQueens {

	private static List<List<String>> res = new ArrayList<>();

	private static List<List<String>> solveNQueens(int n) {
		int[] cur = new int[n];
		Arrays.fill(cur, -1);
		step(cur, 0, n);
		return res;
	}

	private static void step(int[] cur, int step, int n) {
		for (int i = 0; i < n; i++) {
			if (isOk(cur, step, i, n)) {
				if (step == n-1)
					res.add(convert(cur, i, n));
				else {
					cur[step] = i;
					step(cur, step + 1, n);
					cur[step] = -1;
				}
			}
		}
	}

	private static boolean isOk(int[] cur, int row, int col, int n) {
		for (int r = 0; r < row; r++) {
			if (cur[r] == col)
				return false;
			if (Math.abs(r - row) == Math.abs(cur[r] - col))
				return false;
		}
		return true;
	}

	private static List<String> convert(int[] cur, int lastRowCol, int n) {
		List<String> lst = new ArrayList<>(n);
		for (int i = 0; i < n - 1; i++) {
			char[] cc = new char[n];
			Arrays.fill(cc, '.');
			cc[cur[i]] = 'Q';
			lst.add(new String(cc));
		}
		char[] cc = new char[n];
		Arrays.fill(cc, '.');
		cc[lastRowCol] = 'Q';
		lst.add(new String(cc));
		return lst;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(solveNQueens(4));
	}


}
