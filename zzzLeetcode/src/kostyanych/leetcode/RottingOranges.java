package kostyanych.leetcode;

import kostyanych.leetcode.helpers.Printer;

public class RottingOranges {

	private static int orangesRotting(int[][] grid) {
		int rows = grid.length;
		int cols = grid[0].length;
		int[][] next;
		int res = 0;
		boolean rot = true;
		boolean hasFresh = false;
		while (rot) {
			next = new int[rows][cols];
			rot = false;
			hasFresh = false;
			for (int r = 0; r < rows; r++) {
				for (int c = 0; c < cols; c++) {
					if (grid[r][c] == 0 || grid[r][c] == 2)
						next[r][c] = grid[r][c];
					else {
						boolean gonnaRot = check(grid, r, c, rows, cols);
						if (gonnaRot) {
							rot = true;
							next[r][c] = 2;
						} else {
							next[r][c] = grid[r][c];
							hasFresh = true;
						}
					}
				}
			}
			grid = next;
			if (rot)
				res++;
		}
		if (hasFresh)
			return -1;
		return res;
	}

	private static boolean check(int[][] grid, int r, int c, int rows, int cols) {
        if (r>0 && grid[r-1][c]==2)
            return true;
        if (c>0 && grid[r][c-1]==2)
            return true;
        if (r<rows-1 && grid[r+1][c]==2)
            return true;
        if (c<cols-1 && grid[r][c+1]==2)
            return true;
        return false;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		// int[] arr= {2,3,1,1,4};
		int[][] arr = { {1} };
		System.out.println(orangesRotting(arr));

	}

}
