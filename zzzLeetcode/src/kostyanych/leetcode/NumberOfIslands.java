package kostyanych.leetcode;

/**
 * Given a 2d grid map of '1's (land) and '0's (water), count the number of islands.
 * An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
 * You may assume all four edges of the grid are all surrounded by water.
 *
 */
public class NumberOfIslands {

	private static int numIslands(char[][] grid) {
		int rows=grid.length;
		if (rows<1)
			return 0;
		int cols=grid[0].length;
		if (cols<1)
			return 0;
		boolean[][] visited=new boolean[rows][cols];
		int res=0;
		for (int i=0;i<rows;i++) {
			for (int j=0;j<cols;j++) {
				if (visited[i][j])
					continue;
				if (grid[i][j]=='0')
					continue;
				res++;
				process(i,j,grid,visited);
			}
		}
		return res;
    }

	private static void process(int row, int col, char[][] grid, boolean[][] visited) {
		visited[row][col]=true;
		if (grid[row][col]=='0')
			return;
		int rows=grid.length;
		int cols=grid[0].length;

		if (row>0 && !visited[row-1][col])
			process(row-1,col,grid,visited);
		if (col>0 && !visited[row][col-1])
			process(row,col-1,grid,visited);
		if (row<rows-1 && !visited[row+1][col])
			process(row+1,col,grid,visited);
		if (col<cols-1 && !visited[row][col+1])
			process(row,col+1,grid,visited);
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		char[][] arr1 = new char[4][];
		arr1[0] = "11110".toCharArray();
		arr1[1] = "11010".toCharArray();
		arr1[2] = "11000".toCharArray();
		arr1[3] = "00000".toCharArray();
		System.out.println(numIslands(arr1));

		arr1 = new char[4][];
		arr1[0] = "11000".toCharArray();
		arr1[1] = "11000".toCharArray();
		arr1[2] = "00100".toCharArray();
		arr1[3] = "00011".toCharArray();
		System.out.println(numIslands(arr1));
	}
}
