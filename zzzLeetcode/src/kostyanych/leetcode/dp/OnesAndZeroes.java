package kostyanych.leetcode.dp;

import java.util.ArrayList;
import java.util.List;

/**
 * You are given an array of binary strings strs and two integers m and n.
 *
 * Return the size of the largest subset of strs such that there are at most m 0's and n 1's in the subset.
 *
 * A set x is a subset of a set y if all elements of x are also elements of y.
 *
 * Constraints:
 *
 * 1 <= strs.length <= 600
 * 1 <= strs[i].length <= 100
 * strs[i] consists only of digits '0' and '1'.
 * 1 <= m, n <= 100
 *
 */
public class OnesAndZeroes {

	static private int findMaxForm(String[] strs, int m, int n) {
        List<int[]> lst = new ArrayList();
        for (String s : strs) {
            int[] oz = calc(s);
            if (oz[0]<=m && oz[1]<=n)
                lst.add(oz);
        }
        int len = lst.size();
        int[][] dp = new int[m+1][n+1];
        for (int i=0;i<len;i++) {
            int[] str = lst.get(i);
            int[][] cur = new int[m+1][n+1];
            for (int zeroes=0;zeroes<=m;zeroes++) {
                for (int ones=0;ones<=n;ones++) {
                    int max = dp[zeroes][ones]; //don't use this str
                    if (str[0]<=zeroes && str[1]<=ones) {
                        if (i==0)
                            max=1;
                        else {
//                            max=dp[zeroes][ones]; //dont'use this
                            int nz=zeroes-str[0];
                            int no=ones-str[1];
                            if (nz>=0 && no>=0)
                                max=Math.max(max, 1+dp[nz][no]);
                        }
                    }
                    cur[zeroes][ones]=max;
                }
            }
            dp=cur;
        }
        return dp[m][n];
    }

    static private int[] calc(String s) {
        int[] res = new int[2];
        for (char c : s.toCharArray()) {
            if (c=='0')
                res[0]++;
            else
                res[1]++;
        }
        return res;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String[] arr = {"10","0001","111001","1","0"};
		System.out.println(findMaxForm(arr,3,4));
	}

}
