package kostyanych.leetcode.dp;

/**
 * Given n balloons, indexed from 0 to n-1.
 * Each balloon is painted with a number on it represented by array nums.
 * You are asked to burst all the balloons.
 * If the you burst balloon i you will get nums[left] * nums[i] * nums[right] coins.
 * Here left and right are adjacent indices of i. After the burst, the left and right then becomes adjacent.
 *
 * Find the maximum coins you can collect by bursting the balloons wisely.
 *
 * Note:
 * You may imagine nums[-1] = nums[n] = 1. They are not real therefore you can not burst them.
 * 0 ≤ n ≤ 500
 * 0 ≤ nums[i] ≤ 100
 *
 * SOLUTION:
 *
 * 1. in interval [i;j] let K be the last baloon to burst.
 * 2. for bursting this last balloon we'll get nums[K]*nums[i-1]*nums[j+1] (since all ballons in [i;j] except for K are burst)
 * 3. so, the max coins we can get from [i;j] is coins for bursting K + max coins for interval [i;k-1] + max coins for interval [k+1;j]
 * 4. so, we iterate thru all Ks for all intervals looking for max value
 * 5. NOTE that the soultion for [a;i-1] don't overlap with [i;j] or [j+1;b]. So we can calculate them independently
 * 6. and we just store max values for all needed intervals in memo table so we won't have to recalculate them
 *
 */
public class BurstBalloons {

	static private Integer[][] memo;
	static private int maxCoins(int[] nums) {
        int len = nums.length;
        if (len==0)
        	return 0;
        if (len==1)
        	return nums[0];
        memo = new Integer[len][len];
        return calculate(nums,0,len-1);
    }

	static private int calculate(int[] nums, int start, int end) {
		if (end<start)
			return 0;
		if (memo[start][end]!=null)
			return memo[start][end];
		int max=0;
		for (int i=start;i<=end;i++) {
			int left=i<=start?0:calculate(nums, start, i-1);
			int middle = nums[i];
			if (start-1>=0)
				middle*=nums[start-1];
			if (end+1<=nums.length-1)
				middle*=nums[end+1];
			int right=i>=end?0:calculate(nums, i+1, end);
			max=Math.max(max, left+middle+right);
		}
		memo[start][end]=max;
		return max;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {3,2,2,4};
		System.out.println(maxCoins(arr));
	}
}
