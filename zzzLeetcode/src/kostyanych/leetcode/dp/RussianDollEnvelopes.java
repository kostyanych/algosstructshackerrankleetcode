package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * You have a number of envelopes with widths and heights given as a pair of integers (w, h).
 * One envelope can fit into another if and only if both the width and height of one envelope is greater than the width and height of the other envelope.
 *
 * What is the maximum number of envelopes can you Russian doll? (put one inside other)
 *
 * Note:
 * Rotation is not allowed.
 *
 */
public class RussianDollEnvelopes {

	/**
	 * The idea is to sort array by [0] ascending and by [1] descending.
	 * Then going left to right we'll fill memo with heights.
	 * If next height is bigger than last, that means the width is also got bigger (we sorted arr this way).
	 * So this next entry contributes to increasing sequence.
	 * So we put this height as the next entry in memo.
	 * If height is smaller, than the entry does not contribute to increasing current sequence, but may be part of another, more lengthy sequence.
	 * So we put in into the memo accroding to it's value (sorted ascending).
	 * @param envelopes
	 * @return
	 */
	private static int maxEnvelopes(int[][] envelopes) {
		int len = envelopes.length;
		if (len < 2)
			return len;

        Arrays.sort(envelopes, (a1, a2) -> a1[0] == a2[0] ? a2[1] - a1[1] : a1[0] - a2[0]);

        int[] memo = new int[len];
        int max = 0;
        for (int i = 0; i < len; i++) {
            int height = envelopes[i][1];
            int left = 0;
            int right = max;
            while (left < right) {
                int mid = left + (right - left) / 2;
                if (memo[mid] < height)
                    left = mid + 1;
                else
                    right = mid;
            }
            memo[left] = height;
            if (left == max)
            	max++;
        }
        return max;
	}


	private static int maxEnvelopesDpStraight(int[][] envelopes) {
		int len = envelopes.length;
		if (len < 2)
			return len;

		Arrays.sort(envelopes, (ar1, ar2) -> ar1[0]==ar2[0] ? ar1[1]-ar2[1] : ar1[0]-ar2[0]);

		int[] memo = new int[len];
		Arrays.fill(memo, 1);
		int max = 1;
		for (int r = 1; r < len; r++) {
			for (int l = 0; l < r; l++) {
				int[] envl=envelopes[l];
				int[] envr=envelopes[r];
				if (envr[0]<=envl[0])
					continue;
				if (envr[1]>envl[1]) {
					memo[r] = Math.max(memo[r], memo[l] + 1);
					max = Math.max(max, memo[r]);
				}
			}
		}
		return max;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[][] arr = {{2,4},{6,4},{6,7},{2,3}};
		//int[][] arr= {{2,100},{3,200},{4,300},{5,500},{5,400},{5,250},{6,370},{6,360},{7,380}};
		System.out.println(maxEnvelopes(arr));
	}



}
