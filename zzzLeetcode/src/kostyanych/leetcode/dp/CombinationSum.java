package kostyanych.leetcode.dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Given a set of candidate numbers (candidates) (without duplicates) and a target number (target),
 * find all unique combinations in candidates where the candidate numbers sums to target.
 *
 * The same repeated number may be chosen from candidates unlimited number of times.
 *
 * Note:
 *
 *  * All numbers (including target) will be positive integers.
 *  * The solution set must not contain duplicate combinations.
 *
 * NOTE: this is actually coin-change problem
 *
 */
public class CombinationSum {

	private final static List<List<Integer>> res = new ArrayList<>();
	private final static Map<String,List<List<Integer>>> map = new HashMap<>();

	private static List<List<Integer>> combinationSum(int[] candidates, int target) {
		if (target==0) {
            res.add(new ArrayList<>());
            return res;
        }
		Arrays.sort(candidates);
		process(candidates, 0, target, new ArrayList<Integer>());
		return res;
	}

	private static void process(int[] candidates, int curIndex, int target, List<Integer> currList) {
		int len = candidates.length;
		if (target<0 || curIndex>=len)
			return;

		if (target==0) {
			res.add(currList);
			return;
		}

		int thisNum=candidates[curIndex];

		if (thisNum>target)
			return;

		//we use 0 of candidates[curIndex] and skip to next num
		List<Integer> curIterationList = new ArrayList<>(currList);
		if (curIndex+1<len) {
			process(candidates, curIndex+1, target, new ArrayList<>(curIterationList));
		}
		//now we use 1 of candidates[curIndex]
		curIterationList.add(thisNum);
		process(candidates, curIndex, target-thisNum, new ArrayList<>(curIterationList));
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {2,3,7};
		System.out.println(combinationSum(arr,7));

//		arr = new int[] {2,3,5};
//		System.out.println(combinationSum(arr,8));
	}

}
