package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * Given an unsorted array of integers, find the length of longest increasing subsequence.
 *
 * Note:
 *
 * There may be more than one LIS combination, it is only necessary for you to return the length.
 * Your algorithm should run in O(n2) complexity.
 * Follow up: Could you improve it to O(n log n) time complexity?
 *
 */
public class LongestIncreasingSubsequence {

	/**
	 * curLen is the current max len of lis
	 * dp is of length of the original array.
	 * it's filled up with 0 at the start;
	 * then at every new number from nums we (binary) search curLen numbers in dp.
	 * If it fits into curLen numbers in dp, then it doesn't increase current seq, but is a part of a new one
	 * So we enter it into dp at its proper position We don't care if we throw away the old number at that position,
	 *    because there are bigger numbers to the right of it, and we're interested in those. Or it could be the last number
	 *    but then it would be smaller than the old last number ('cause otherwise it would not fit in curLen but rather extended curLen further).
	 * If this number doesn't fit into curLen, then it's bigger than the last one in dp. Then it increases current LIS and goes in the next dp index.
	 *
	 * We're using Arrays.binarySearch for the range of an array.
	 * It returns the index of the search key, if it is contained in the array;
	 *   otherwise, (-(insertion point) - 1).
	 *   The insertion point is defined as the point at which the key would be inserted into the array:
	 *      the index of the first element greater than the key,
	 *      or a.length if all elements in the array are less than the specified key.
	 *      Note that this guarantees that the return value will be >= 0 if and only if the key is found.
	 */
	private static int lengthOfLIS(int[] nums) {
		int[] dp = new int[nums.length];
		int curLen = 0;

		for (int num : nums) {
			int i = Arrays.binarySearch(dp, 0, curLen, num);
			if (i < 0)
				i = -(i + 1);
			dp[i] = num;
			if (i == curLen)
				curLen++;
		}
		return curLen;
	}

	private static int lengthOfLISDpStraight(int[] nums) {
		int len = nums.length;
		if (len < 2)
			return len;
		int[] memo = new int[len];
		Arrays.fill(memo, 1);
		int max = 1;
		for (int r = 1; r < len; r++) {
			for (int l = 0; l < r; l++) {
				if (nums[r] > nums[l]) {
					memo[r] = Math.max(memo[r], memo[l] + 1);
					max = Math.max(max, memo[r]);
				}
			}
		}
		return max;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {10,9,2,5,1,7,101,18};
		System.out.println(lengthOfLIS(arr));
	}

}
