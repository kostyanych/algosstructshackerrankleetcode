package kostyanych.leetcode.dp;

/**
 * You have an array for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete as many transactions as you like
 * (i.e., buy one and sell one share of the stock multiple times).
 *
 * Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).
 *
 * Return the maximum profit available.
 *
 */
public class BestTimeToBuyAndSellStockII {

	private static int maxProfit(int[] prices) {
		int len=prices.length;
		if (len<2)
			return 0;
		//if we have a price drop on the next day that means
		//either no profit at all (if price won't go up at all)
		//or profit from price(day+2)-price(day+1) - since price(day+1)<price(day) => price(day+2)-price(day+1) > price(day+2)-price(day)

		//if we have a continuous price increase: price(d+2)>price(d+1)>price(d) that means that
		//price(d2)-price(d)=(price(d2)-price(d1))+(price(d1)-price(d))

		//so we disregard days on which price drops and count every (price(d1)-price(d)) where price increases

		int res=0;
		for (int i=1;i<len;i++) {
			int dayProfit=prices[i]-prices[i-1];
			if (dayProfit>0)
				res+=dayProfit;
		}
		return res;
    }

	private static int maxProfitWithOneArray(int[] prices) {
		int len=prices.length;
		if (len<2)
			return 0;
		if (len==2)
			return Math.max(prices[1]-prices[0], 0);
		int[] dp = new int[len];

		dp[0] = 0;

		for (int i=0;i<len-1;i++) {
			int prevMax=0;
			if (i>1)
				prevMax=dp[i-1];
			for (int j=i+1;j<len;j++) {
				int prof=Math.max(prices[j]-prices[i],0)+prevMax;
				dp[j]=Math.max(Math.max(prof, dp[j-1]),dp[j]);
			}
		}

		return dp[len-1];
    }


	private static int maxProfit2(int[] prices) {
		int len=prices.length;
		if (len<2)
			return 0;
		if (len==2)
			return Math.max(prices[1]-prices[0], 0);
		int[][] dp = new int[2][len];

		dp[0][0] = 0;
		for (int j=1;j<len;j++) {
			dp[0][j] = Math.max(dp[0][j-1], prices[j]-prices[0]);
		}


		for (int i=1;i<len-1;i++) {
			for (int j=0;j<=i;j++) {
				dp[1][j]=dp[0][j];
			}
			int prevMax=0;
			if (i>1)
				prevMax=dp[1][i-1];
			for (int j=i+1;j<len;j++) {
				int prof=Math.max(prices[j]-prices[i],0)+prevMax;
				dp[1][j]=Math.max(Math.max(prof, dp[1][j-1]),dp[0][j]);
			}
			for (int j=1;j<len;j++) {
				dp[0][j]=dp[1][j];
			}
		}

		return dp[0][len-1];
    }

	private static int maxProfitBigMemory(int[] prices) {
		int len=prices.length;
		if (len<2)
			return 0;
		if (len==2)
			return Math.max(prices[1]-prices[0], 0);
		int[][] dp = new int[len-1][len];

		dp[0][0] = 0;
		for (int j=1;j<len;j++) {
			dp[0][j] = Math.max(dp[0][j-1], prices[j]-prices[0]);
		}


		for (int i=1;i<len-1;i++) {
			for (int j=0;j<=i;j++) {
				dp[i][j]=dp[i-1][j];
			}
			int prevMax=0;
			if (i>1)
				prevMax=dp[i][i-1];
			for (int j=i+1;j<len;j++) {
				int prof=Math.max(prices[j]-prices[i],0)+prevMax;
				dp[i][j]=Math.max(Math.max(prof, dp[i][j-1]),dp[i-1][j]);
			}
		}

		return dp[len-2][len-1];
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={1,2,3,4,5};
		System.out.println(maxProfit(arr));
	}
}
