package kostyanych.leetcode.dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * You are given an array target that consists of distinct integers and another integer array arr that can have duplicates.
 *
 * In one operation, you can insert any integer at any position in arr.
 * For example, if arr = [1,4,1,2], you can add 3 in the middle and make it [1,4,3,1,2].
 * Note that you can insert the integer at the very beginning or end of the array.
 *
 * Return the minimum number of operations needed to make target a subsequence of arr.
 *
 * A subsequence of an array is a new array generated from the original array by deleting some elements (possibly none)
 * without changing the remaining elements' relative order.
 * For example, [2,7,4] is a subsequence of [4,2,3,7,2,1,4] (the underlined elements), while [2,4,2] is not.
 *
 * Constraints:
 * 	1 <= target.length, arr.length <= 10^5
 * 	1 <= target[i], arr[i] <= 10^9
 * 	target contains no duplicates.
 *
 * SOLUTION:
 * 1. we could have gone for LCS, but we don't have to
 * 2. values in target are distinct, so we can easily convert them into their indices
 * 3. since we need a subsequence, the order of the values cannot be changed. So the indices have to be ascending in the result
 * 4. now we change the values in arr into indices of corresponding values in target. We discard values that are not in target.
 * 5. now we should only find the longest increasing subsequence in our modified arr (see 3).
 * 		this LIS will correspond to the most number needed values already present in arr in the correct order
 * 6. We only need to insert target.lengt-LIS.length values
 * 7. we'll do 4 and 5 on the fly
 *
 */
public class MinimumOperationsToMakeASubsequence {

	static private int minOperations(int[] target, int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        int len=target.length;
        for (int i = 0; i < len; ++i)
            map.put(target[i], i);

        List<Integer> lst = new ArrayList<>();
        for (int a : arr) {
        	Integer ind=map.get(a);
            if (ind==null)
            	continue;
            if (lst.size() == 0 || ind > lst.get(lst.size() - 1)) {
            	lst.add(ind);
                continue;
            }

            int left = 0;
            int right = lst.size() - 1;
            int mid;
            while (left < right) {
                mid = left + (right-left) / 2;
                if (lst.get(mid) < ind)
                    left = mid + 1;
                else
                    right = mid;
            }
            lst.set(left, ind);
        }
        return len - lst.size();
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] target = {6,4,8,1,3,2};
		int[] arr = {4,7,6,2,3,8,6,1};
		System.out.println(Arrays.toString(target));
		System.out.println(Arrays.toString(arr));
		System.out.println(minOperations(target,arr));
	}


}
