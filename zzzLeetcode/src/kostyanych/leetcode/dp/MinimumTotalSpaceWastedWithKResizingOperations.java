package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * You are currently designing a dynamic array.
 * You are given a 0-indexed integer array nums, where nums[i] is the number of elements that will be in the array at time i.
 * In addition, you are given an integer k, the maximum number of times you can resize the array (to any size).
 *
 * The size of the array at time t, sizet, must be at least nums[t] because there needs to be enough space in the array to hold all the elements.
 * The space wasted at time t is defined as sizet - nums[t], and the total space wasted is the sum of the space wasted across every time t
 * 		where 0 <= t < nums.length.
 *
 * Return the minimum total space wasted if you can resize the array at most k times.
 *
 * Note: The array can have any size at the start and does not count towards the number of resizing operations.
 *
 * Constraints:
 * 1 <= nums.length <= 200
 * 1 <= nums[i] <= 10^6
 * 0 <= k <= nums.length - 1
 *
 * SOLUTION :
 * 1.  let the minWaste(i,j,k) be a function returning the min waste on interval [i;j) with k "resizes"
 * 2.  then we need to check all combinations of minWaste(0,r,k)+minWaste(r,len,k-1), where r=1...len-1, for min result
 * 3.  obviously minWaste is a recursive function where minWaste(r,len,k-1) turns into checking all combinations of minWaste(r,r2,k-1)+minWaste(r2,len,k-2) etc
 * 4.  the special case is k=0: we just "resize" the interval to the max value in it and directly calculate waste
 * 5.  simple way of calculating waste on an interval is keep the rolling sum of the interval and max value this far
 * 		then the current waste would be (maxVal*lenThisFar - curSum)
 * 6. let use dp[i][k] for memoization of results of minWaste() function - min waste of interval [i;len) with k possible "resizes"
 *
 */
public class MinimumTotalSpaceWastedWithKResizingOperations {

	private static int[][] dp;
	private static int len;

	private static int minSpaceWastedKResizing(int[] nums, int k) {
		len = nums.length;
		dp=new int[len][k+1];
		for (int i=0;i<len;i++) {
			for (int j=0;j<=k;j++) {
				dp[i][j]=-1;
			}
		}
		return helper(nums, 0, k);
	}

	private static int helper(int[] nums, int i, int k) {
		if (i + k >= len)
	        return 0;
	    if (dp[i][k] >= 0)
    		return dp[i][k];

	    if (k==0) {
		    int curIntervalMax=nums[i];
		    int curSum=nums[i];
		    for (int j=i+1; j<len; j++) {
		    	curIntervalMax = Math.max(curIntervalMax, nums[j]);
		    	curSum += nums[j];
		    }
		    dp[i][k]=curIntervalMax*(len-i)-curSum;
		    return dp[i][k];
	    }

	    int nextIntervalMinWaste=helper(nums, i+1, k-1);
	    dp[i][k] = nextIntervalMinWaste;

	    int curIntervalMax=nums[i];
	    int curSum=nums[i];
	    for (int j=i+1; j<len; j++) {
	    	curIntervalMax = Math.max(curIntervalMax, nums[j]);
	    	curSum += nums[j];
	        int thisIntervalWaste = curIntervalMax * (j - i + 1) - curSum;
	        nextIntervalMinWaste=helper(nums, j + 1, k - 1);
	        dp[i][k] = Math.min(dp[i][k], thisIntervalWaste + nextIntervalMinWaste);
	    }
	    return dp[i][k];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] nums = {10,20,15,30,20};
		int k = 2;
		System.out.println(Arrays.toString(nums));
		System.out.println(minSpaceWastedKResizing(nums, k));
	}
}
