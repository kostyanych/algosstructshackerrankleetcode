package kostyanych.leetcode.dp;

/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete at most TWO transactions.
 *
 * Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).
 */
public class BestTimeToBuyAndSellStockIII {

	/**
	 * same as in k transactions, k=2
	 * @param prices
	 * @return
	 */
	private static int maxProfit(int[] prices) {
		int len = prices.length;
		if (len<2)
			return 0;
		int k=2;

		int[][] dp = new int[k+1][len];
		int maxDiff=0;
		for (int i=1;i<k+1;i++) {
			maxDiff=0-prices[0];
			for (int j=1;j<len;j++) {
				int prevTranMax=j>1?dp[i-1][j-2]:0;
				maxDiff=Math.max(maxDiff, prevTranMax-prices[j-1]);
				int curProf=prices[j]+maxDiff;
				dp[i][j]=Math.max(dp[i][j-1],curProf);
			}
		}
		return dp[k][len-1];
	}


	private static int maxProfitSlow(int[] prices) {

		int len=prices.length;
		if (len<2)
			return 0;
		int[] maxOneTime=new int[prices.length];

		int curMax=0;
		for (int i=0;i<len-1;i++) {
			int curBuy=prices[i];
			int maxPrev=i==0?0:maxOneTime[i-1];
			for (int j=i+1;j<len;j++) {
				int curProf=Math.max(0, prices[j]-curBuy);
				curMax=Math.max(curMax, maxPrev+curProf);
				maxOneTime[j]=Math.max(maxOneTime[j-1], Math.max(maxOneTime[j], curProf));
			}
		}
		return curMax;
	}


	static private void checkMaxes(int[] maxes, int newMax) {
		if (newMax==0)
			return;
		if (newMax>maxes[0]) {
			maxes[1]=maxes[0];
			maxes[0]=newMax;
		}
		else if (newMax>maxes[1]) {
			maxes[1]=newMax;
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[] arr= {1,2,4,2,5,7,2,4,9,0};
		int[] arr= {3,2,6,5,0,3};
		System.out.println(maxProfit(arr));
	}

}
