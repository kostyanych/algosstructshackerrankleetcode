package kostyanych.leetcode.dp;

/**
 * Say you have an array for which the i-th element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete at most k transactions.
 *
 * Note:
 * You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 *
 */
public class BestTimeToBuyAndSellStockIV {

	private static int maxProfit(int k, int[] prices) {
		int len = prices.length;
		if (len<2)
			return 0;

		//if K is big enough we can see it as unlimited number of transactions (we can't do more than days/2 transactons anyway)
		if (k>len/2)
			return maxProfitWithInfiniteTransactions(prices);

		int[][] dp = new int[k+1][len];
		int maxDiff=0;
		for (int i=1;i<k+1;i++) {
			maxDiff=0-prices[0];
			for (int j=1;j<len;j++) {
				int prevTranMax=j>1?dp[i-1][j-2]:0;
				maxDiff=Math.max(maxDiff, prevTranMax-prices[j-1]);
				int curProf=prices[j]+maxDiff;
				dp[i][j]=Math.max(dp[i][j-1],curProf);
			}
		}
		return dp[k][len-1];
	}

	private static int maxProfitBAAAAD(int k, int[] prices) {
		int len = prices.length;
		if (len<2)
			return 0;

		//if K is big enough we can see it as unlimited number of transactions (we can't do more than days/2 transactons anyway)
		if (k>len/2)
			return maxProfitWithInfiniteTransactions(prices);

		int[] dp = new int[len];
		int maxDiff=0;
		for (int i=0;i<k;i++) {
			maxDiff=dp[0]-prices[0];
			int prevTranMax=0;
			for (int j=1;j<len;j++) {
				int curProf=prices[j]+maxDiff;
				maxDiff=Math.max(maxDiff, prevTranMax-prices[j]);
				prevTranMax=j>1?dp[j]:0;
				dp[j]=Math.max(dp[j-1],curProf);
			}
		}
		return dp[len-1];
	}

	private static int maxProfitWithInfiniteTransactions(int[] prices) {
		int len=prices.length;
		if (len<2)
			return 0;
		int res=0;
		for (int i=1;i<len;i++) {
			int dayProfit=prices[i]-prices[i-1];
			if (dayProfit>0)
				res+=dayProfit;
		}
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[] arr= {1,2,4,2,5,7,2,4,9,0};
		int[] arr= {3,2,6,5,0,3};
		System.out.println(maxProfit(2, arr));
	}
}
