package kostyanych.leetcode.dp;

import java.util.TreeSet;

/**
 * Given a non-empty 2D matrix matrix and an integer k, find the max sum of a rectangle in the matrix such that its sum is no larger than k.
 *
 * The problem is badly formulated because it doesn't say what to return if there is no proper answer
 *
 */
public class MaxSumOfRectangleNoLargerThanK {

	static private int maxSumSubmatrix(int[][] matrix, int k) {
        int rows=matrix.length;
        int cols=matrix[0].length;

        int[][] memo=new int[rows+1][cols+1];

        for (int r=1;r<=rows;r++) {
        	for (int c=1;c<=cols;c++) {
        		memo[r][c] = memo[r][c - 1] + matrix[r - 1][c - 1];
            }
        }

        int res = Integer.MIN_VALUE;
        //1. for every combination of colstart, colend calculate rowsums
        //2. for every rowsum X, check if it's equal to k (then we're done)
        //3. for every rowsum X, check if it there's a previously calculated rowsum Y such as (X-Y)<=k and (X-Y) is a current max
        //4. for handling 3 fast we'll use TreeSet so that we could easily find Ys
        for (int cstart = 1; cstart <= cols; cstart++) {
            for(int cend = cstart; cend <= cols; cend++) {

                int runningSum = 0;
                TreeSet<Integer> rowsums = new TreeSet<>();
                rowsums.add(0);

                for(int r = 1; r <= rows; r++) {
                    runningSum += memo[r][cend] - memo[r][cstart - 1];
                    if (runningSum==k)
                    	return runningSum;
                   	int delta = runningSum-k;
                   	Integer somePrevRowSum = rowsums.ceiling(delta);
                    if (somePrevRowSum != null && runningSum - somePrevRowSum <= k)
                    	res = Math.max(runningSum - somePrevRowSum, res);
                    rowsums.add(runningSum);
                }
            }
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		int[][] matrix = {{5,-4,-3,4},{-3,-4,4,5},{5,1,5,-4}};
//		System.out.println(maxSumSubmatrix(matrix,8));
		int[][] matrix = {{-1}};
		System.out.println(maxSumSubmatrix(matrix,-2));

	}
}
