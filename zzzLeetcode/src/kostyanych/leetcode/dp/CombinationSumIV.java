package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * Given an array of distinct integers nums and a target integer target, return the number of possible combinations that add up to target.
 *
 * The answer is guaranteed to fit in a 32-bit integer.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 200
 * 1 <= nums[i] <= 1000
 * All the elements of nums are unique.
 * 1 <= target <= 1000
 *
 * Follow up: What if negative numbers are allowed in the given array?
 * How does it change the problem? What limitation we need to add to the question to allow negative numbers?
 *
 */
public class CombinationSumIV {

	static private int combinationSum4(int[] nums, int target) {
        int[] memo = new int[target+1];
        Arrays.sort(nums);
        for (int i=1; i<=target;i++) {
            for (int n : nums) {
                if (n>i)
                    break;
                int r=0;
                if (i==n)
                    r=1;
                else {
                    if (i-n > 0) {
                        if (memo[i-n]!=0)
                            r=memo[i-n];
                    }
                }
                memo[i]+=r;
            }
        }
        return memo[target];
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {1,2,3};
		System.out.println(combinationSum4(arr, 4));
	}

}
