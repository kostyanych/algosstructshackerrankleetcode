package kostyanych.leetcode.dp;

import java.util.HashMap;
import java.util.Map;

/**
 * In a N x N grid representing a field of cherries, each cell is one of three possible integers.
 *
 * 0 means the cell is empty, so you can pass through;
 * 1 means the cell contains a cherry, that you can pick up and pass through;
 * -1 means the cell contains a thorn that blocks your way.
 *
 * Your task is to collect maximum number of cherries possible by following the rules below:
 *
 *  Starting at the position (0, 0) and reaching (N-1, N-1) by moving right or down through valid path cells (cells with value 0 or 1);
 *  After reaching (N-1, N-1), returning to (0, 0) by moving left or up through valid path cells;
 *  When passing through a path cell containing a cherry, you pick it up and the cell becomes an empty cell (0);
 *  If there is no valid path between (0, 0) and (N-1, N-1), then no cherries can be collected.
 *
 *  Note:
 *    grid is an N by N 2D array, with 1 <= N <= 50.
 *    Each grid[i][j] is an integer in the set {-1, 0, 1}.
 *    It is guaranteed that grid[0][0] and grid[N-1][N-1] are not -1.
 *
 *
 * Solution:
 *
 * Going forward and back in our case is equal as going forward with 2 pickers at once (since backward motion rules are mirroring the forward motion rules).
 * So, for every cell where pickers could go make all dfs and get max of them all.
 * For every combination of {i1,j1} (picker1 coordinates) and {i2,j2} (picker2 coordinates)
 * 	 there are 4 combinations to go :
 * 	   i1 + 1, j1, i2 + 1, j2 -> both go down
 * 	   i1 + 1, j1, i2, j2 + 1 -> first goes down, second goes to the right
 * 	   i1, j1 + 1, i2 + 1, j2 -> first goes to the right, second goes down
 * 	   i1, j1 + 1, i2, j2 + 1 -> first goes to the right, second goes to the right
 *
 * Memoize the interim results in Map with key made up from {i1,j1}, {i2,j2}
 *
 */
public class CherryPickup {

	private static Map<String, Integer> map = new HashMap<>();
	private static int[][] ggrid;
	private static int N;
//	private static int[][][] memo;

	private static int cherryPickup(int[][] grid) {
		ggrid=grid;
		N=grid.length;
//		memo=new int[N][N][N];
//		for (int i=0;i<N;i++) {
//			for (int j=0;j<N;j++) {
//				Arrays.fill(memo[i][j],-1);
//			}
//		}
	    return Math.max(0, travelForward(0, 0, 0, 0));
	}

	static private int travelForward(int i1, int j1, int i2, int j2) {
		if(i1 > N-1 || j1 > N-1
				|| i2 > N-1 || j2 > N-1
				|| ggrid[i1][j1] == -1 || ggrid[i2][j2] == -1)
			return -1;
			//return Integer.MIN_VALUE;

		String key = i1 + "|" + j1 + "|" + i2 + "|" + j2;
		if(map.containsKey(key))
			return map.get(key);

		if (i1 == N-1 && j1 == N-1)
			return ggrid[i1][j1];

		if(i2 == N-1 && j2 == N-1)
			return ggrid[i2][j2];

		int res = (i1 == i2 && j1 == j2) ? ggrid[i1][j1] : ggrid[i1][j1] + ggrid[i2][j2];

		int max = Math.max(
				Math.max(travelForward(i1+1, j1, i2+1, j2),travelForward(i1+1, j1, i2, j2+1)),
				Math.max(travelForward(i1, j1+1, i2+1, j2), travelForward(i1, j1+1, i2, j2+1)));
		res = max<0 ? -1 : res+max;
		map.put(key, res);
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] grid = {{1,1,-1},{1,-1,1},{-1,1,1}}; //{{0, 1, -1}, {1, 0, -1}, {1, 1,  1}};
		System.out.println(cherryPickup(grid));
	}

}
