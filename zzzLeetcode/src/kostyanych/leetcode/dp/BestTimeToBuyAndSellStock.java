package kostyanych.leetcode.dp;

/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction
 * (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.
 *
 * Note that you cannot sell a stock before you buy one.
 */
public class BestTimeToBuyAndSellStock {

	private static int maxProfit(int[] prices) {

		int len=prices.length;
		if (len<2)
			return 0;
		if (len==2)
			return Math.max(0, prices[1]-prices[0]);

		int min=prices[0];
		int max=0;
		for (int i=1;i<len;i++) {
			if (prices[i]<min) {
				min=prices[i];
				continue;
			}
			int curProf=Math.max(0, prices[i]-min);
			if (curProf>max)
				max=curProf;
		}
		return max;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {7,1,5,3,6,4};
		System.out.println(maxProfit(arr));
		arr = new int[]{7,6,4,3,1};
		System.out.println(maxProfit(arr));
	}

}
