package kostyanych.leetcode.dp;

/**
 * The demons had captured the princess (P) and imprisoned her in the
 * bottom-right corner of a dungeon. The dungeon consists of M x N rooms laid
 * out in a 2D grid. Our valiant knight (K) was initially positioned in the
 * top-left room and must fight his way through the dungeon to rescue the
 * princess.
 *
 * The knight has an initial health point represented by a positive integer. If
 * at any point his health point drops to 0 or below, he dies immediately.
 *
 * Some of the rooms are guarded by demons, so the knight loses health (negative
 * integers) upon entering these rooms; other rooms are either empty (0's) or
 * contain magic orbs that increase the knight's health (positive integers).
 *
 * In order to reach the princess as quickly as possible, the knight decides to
 * move only rightward or downward in each step.
 *
 * Write a function to determine the knight's minimum initial health so that he
 * is able to rescue the princess.
 *
 */
public class DungeonGame {

	/**
	 * make a memo[][] grid holding min health value needed at this cell to continue forward
	 * start filling it with last cell
	 * fill last row and last column starting with last cell
	 * then fill all other cells backwards
	 *
	 * filling logic is simple:
	 *    for last cell - if it's healing cell, min health = 1, otherwise min h = 1-cellDamage
	 *    for last row and col: we have to go further (left for row and down for col), so we do nextCellMinHealth-cellVaue
	 *        if it was a damaging cell, we'll have minHealth needed
	 *        if it was a healing cell, we'll either have a positive number (then it would be minHealth)
	 *            or a negative number - healing was excessive, so we can enter this sell at min health = 1
	 *    for any other cell, pick the cell from left and down with min health required, then do the math from previous point
	 *
	 * @return
	 */
	private static int calculateMinimumHP(int[][] dungeon) {
		int rows = dungeon.length;
		int cols = dungeon[0].length;

		int[][] memo = new int[rows][cols];

		int h = 1 - dungeon[rows - 1][cols - 1];
		memo[rows - 1][cols - 1] = h > 0 ? h : 1;

		for (int c = cols - 2; c >= 0; c--) {
			h = memo[rows - 1][c + 1] - dungeon[rows - 1][c];
			memo[rows - 1][c] = h > 0 ? h : 1;
		}

		for (int r = rows - 2; r >= 0; r--) {
			h = memo[r + 1][cols - 1] - dungeon[r][cols - 1];
			memo[r][cols - 1] = h > 0 ? h : 1;
		}

		for (int r = rows - 2; r >= 0; r--) {
			for (int c = cols - 2; c >= 0; c--) {
				h = Math.min(memo[r + 1][c], memo[r][c + 1]) - dungeon[r][c];
				memo[r][c] = h > 0 ? h : 1;
			}
		}
		return memo[0][0];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] dungeon = {{-2,-3,3},{-5,-10,1},{10,30,-5}};
		System.out.println(calculateMinimumHP(dungeon));
	}
}
