package kostyanych.leetcode.dp;

/**
 * You are given an array prices where prices[i] is the price of a given stock on the ith day, and an integer fee representing a transaction fee.
 *
 * Find the maximum profit you can achieve. You may complete as many transactions as you like, but you need to pay the transaction fee for each transaction.
 *
 * Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).
 *
 * SOLUTION:
 * we have 2 possible states: we have stock at day i or we don't. If we have stock we can sell it or we can hold it. If we don't we can buy or we can skip.
 *
 * More importantly we can arrive on day i+1 without stock if we had stock on i and sold it or we didn't have it
 *
 * NONE[i]  ->  NONE[i+1]
 *              /
 *            /
 * HAVE[i]
 *
 * Similarly we can arrive on day i+1 with stock if we kept in from day i or bought it on day i:
 *
 * NONE[i]
 *         \
 *           \
 * HAVE[i]  ->  HAVE[i+1]
 *
 * Obviously we have to maximize our cash for both points, but consider them separate, because we need to consider tme potential profit from bying stock on day i.
 *
 * so, we'll keep 2 values for every given day: max profit of state NONE and max profit of state HAVE.
 *
 *  Obviously, in the end we should only consider the max profit of the NONE state (the last profitable sell or 0 if won't buy stock at all)
 *
 *
 */
public class BestTimeToBuyAndSellStockWithTransactionFee {

	static private int maxProfit(int[] prices, int fee) {
        int len = prices.length;
        int cashWithNoStock = 0;
        int cashWithStock = -prices[0];
        for (int i = 1; i < len; i++) {
        	cashWithNoStock = Math.max(cashWithNoStock, cashWithStock + prices[i] - fee);
        	cashWithStock = Math.max(cashWithStock, cashWithNoStock - prices[i]);
        }
        return cashWithNoStock;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr={1,3,2,8,4,9};
		System.out.println(maxProfit(arr,2));
	}
}
