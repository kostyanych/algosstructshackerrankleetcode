package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * You are a professional robber planning to rob houses along a street.
 * Each house has a certain amount of money stashed,
 *  the only constraint stopping you from robbing each of them is that adjacent houses have security system connected
 *  and it will automatically contact the police if two adjacent houses were broken into on the same night.
 *
 *  Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.
 *
 */
public class HouseRobber {

	private static int rob(int[] nums) {
		int len=nums.length;
		if (len==0)
			return 0;
		if (len==1)
			return nums[0];
		if (len==2)
			return Math.max(nums[0],nums[1]);

		int k=(len+1)/2;

		int[] prev=new int[len];
        prev[0]=nums[0];
		for (int i=1;i<len;i++) {
			prev[i]=Math.max(nums[i], nums[i-1]);
		}
		for (int i=2;i<len;i++) {
			prev[i]=Math.max(prev[i-1], prev[i-2]+nums[i]);
		}
		return prev[len-1];
    }

	private static int robSlower(int[] nums) {
		int len=nums.length;
		if (len==0)
			return 0;
		if (len==1)
			return nums[0];
		if (len==2)
			return Math.max(nums[0],nums[1]);

		int k=(len+1)/2;

		int[] prev=new int[len];
		int[] cur=new int[len];
        prev[0]=nums[0];
		for (int i=1;i<len;i++) {
			prev[i]=Math.max(nums[i], nums[i-1]);
		}
		for (int i=1;i<=k;i++) {
			for (int j=i*2;j<len;j++) {
				cur[j]=Math.max(Math.max(prev[j-1], prev[j-2]+nums[j]), cur[j-1]);
			}
			prev=cur;
		}
		return cur[len-1];
    }


	private static int robManyArrays(int[] nums) {
		int len=nums.length;
		if (len==0)
			return 0;
		if (len==1)
			return nums[0];
		if (len==2)
			return Math.max(nums[0],nums[1]);

		int k=(len+1)/2;

		int[][] prof= new int[k][len];
        prof[0][0]=nums[0];
		for (int i=1;i<len;i++) {
			prof[0][i]=Math.max(nums[i], nums[i-1]);
		}
		for (int i=1;i<=k;i++) {
			for (int j=i*2;j<len;j++) {
				prof[i][j]=Math.max(Math.max(prof[i-1][j-1], prof[i-1][j-2]+nums[j]), prof[i][j-1]);
			}
		}
		return prof[k-1][len-1];
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {2,1,1,2};
		System.out.println(Arrays.toString(arr));
		System.out.println(rob(arr));
		arr = new int[] {1,2,3,1};
		System.out.println(Arrays.toString(arr));
		System.out.println(rob(arr));
		arr = new int[] {2,7,9,3,1};
		System.out.println(Arrays.toString(arr));
		System.out.println(rob(arr));
	}

}
