package kostyanych.leetcode.dp;

import java.util.HashMap;
import java.util.Map;

/**
 * There is a donuts shop that bakes donuts in batches of batchSize.
 * They have a rule where they must serve all of the donuts of a batch before serving any donuts of the next batch.
 * You are given an integer batchSize and an integer array groups, where groups[i] denotes that there is a group of groups[i] customers that will visit the shop.
 * Each customer will get exactly one donut.
 *
 * When a group visits the shop, all customers of the group must be served before serving any of the following groups.
 * A group will be happy if they all get fresh donuts.
 * That is, the first customer of the group does not receive a donut that was left over from the previous group.
 *
 * You can freely rearrange the ordering of the groups. Return the maximum possible number of happy groups after rearranging the groups.
 *
 * Constraints:
 *
 * 1 <= batchSize <= 9
 * 1 <= groups.length <= 30
 * 1 <= groups[i] <= 10^9
 *
 */
public class MaximumNumberOfGroupsGettingFreshDonuts {

	static private final Map<Integer,Map<Integer, Integer>> memo = new HashMap<>();
	static private int myBatchSize;
	static private int[] myGroups;

	static private int maxHappyGroupsBAAAAAAAD(int batchSize, int[] groups) {
		memo.clear();

		myBatchSize = batchSize;
		myGroups = groups;

		int used=0;
		int res=0;
		for (int i=0;i<myGroups.length;i++) {
			if (myGroups[i]%batchSize==0) {
				res++;
				used = markUsed(used,i);
			}
		}

		int max = 0;
		for (int i=0;i<myGroups.length;i++) {
			if (!isUsed(used,i)) {
				int newUsed=markUsed(used,i);
				max = Math.max(max, work(newUsed, i, 0));
			}
		}
		return max+res;
    }

	static private int work(int used, int nextG, int rem) {
		int thisGroup = 0;
		if (rem==0)
			thisGroup = 1;
		int newUsed = markUsed(used, nextG);

		int hungry=0;
		if (myGroups[nextG]>rem) {
			hungry=myGroups[nextG]-rem;
			rem=hungry%myBatchSize;
		}
		else
			rem-=myGroups[nextG];

		Map<Integer, Integer> remMap = memo.get(newUsed);
		if (remMap==null) {
			remMap = new HashMap<>();
			memo.put(used, remMap);
		}
		Integer res = remMap.get(rem);
		if (res==null) {
//			int newUsed = markUsed(used, nextG);
			int max = 0;
			for (int i=0;i<myGroups.length;i++) {
				if (i==nextG || isUsed(newUsed, i))
					continue;
				max = Math.max(max, work(newUsed, i, rem));
			}
			res = max;
			remMap.put(rem, res);
		}
		res+=thisGroup;
		return res;
	}

	static private boolean isUsed(int used, int indG) {
		int m = 1 << indG;
		return (used&m)!=0;
	}

	static private int markUsed(int used, int nextG) {
		int m = 1 << nextG;
		return used | m;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int batchSize = 2;
		int[] groups = {652231582,818492002,823729239,2261354,747144855,478230860,285970257,774747712,860954510,245631565,634746161,109765576,967900367,340837477,32845752,23968185};
		System.out.println(maxHappyGroupsBAAAAAAAD(batchSize, groups));
	}

}
