package kostyanych.leetcode.dp;

/**
 * You have the task of delivering some boxes from storage to their ports using only one ship.
 * However, this ship has a limit on the number of boxes and the total weight that it can carry.
 *
 * You are given an array boxes, where boxes[i] = [ports​​i​, weighti], and three integers portsCount, maxBoxes, and maxWeight.
 * 	ports​​i is the port where you need to deliver the ith box and weightsi is the weight of the ith box.
 * 	portsCount is the number of ports.
 * maxBoxes and maxWeight are the respective box and weight limits of the ship.
 *
 * The boxes need to be delivered in the order they are given. The ship will follow these steps:
 * 	The ship will take some number of boxes from the boxes queue, not violating the maxBoxes and maxWeight constraints.
 * 	For each loaded box in order, the ship will make a trip to the port the box needs to be delivered to and deliver it.
 * 	If the ship is already at the correct port, no trip is needed, and the box can immediately be delivered.
 * 	The ship then makes a return trip to storage to take more boxes from the queue.
 * 	The ship must end at storage after all the boxes have been delivered.
 *
 * Return the minimum number of trips the ship needs to make to deliver all boxes to their respective ports.
 *
 * Constraints:
 * 	1 <= boxes.length <= 10^5
 * 	1 <= portsCount, maxBoxes, maxWeight <= 10^5
 * 	1 <= ports​​i <= portsCount
 * 	1 <= weightsi <= maxWeight
 *
 * SOLUTION:
 * Observations:
 * 	(1) Boxes must be handled in order from left to right, this is a signal to use DP.
 * 	(2) Since it is 10^5. DP must be 1-dimension, i.e, for every index i we need to find a single index j and apply DP[i] = f(DP[j]) for some DP function f.
 *
 * Let dp[i+1] be the minimum cost (# of ships) to process all boxes from 0 to i and return to the storage.
 * From observation (2), we need to find the best position start where we can load up all boxes from start to i and then go in a single voyage, then dp[i+1] = dp[start] + something.
 *
 * To find start, we use slidling window. We slide start forward for 3 reasons:
 * 	limit # of boxes in 1 voyage
 * 	limit total weight in 1 voyage
 * 	No benefit to load the box at position start (i.e dp[start] == dp[start+1]), because the cost to keep the box on the ship is at most 1.
 *
 */
public class DeliveringBoxesFromStorageToPorts {

	static private int boxDelivering(int[][] boxes, int portsCount, int maxBoxes, int maxWeight) {
		int len = boxes.length;
        boolean[] consecutivePorts = new boolean[len]; // consecutive ports are different or not
        for (int i = 0; i< len-1; i++) {
            if (boxes[i][0] == boxes[i+1][0])
            	consecutivePorts[i] = true;
        }

        int[] memo = new int[len+1];

        int curWeight = 0; // total current weight on the ship
        int start = 0; // load all boxes from start to i in one voyage
        int differentPorts = 0; // # different consecutive ports between start and i

        for (int i = 0; i< len; i++){
            if (i-start == maxBoxes) { // drop 1 box out of the window because of # boxes constraint
                curWeight-= boxes[start][1];
                if (!consecutivePorts[start])
                	differentPorts--;
                start++;
            }

            // Add box i, update current weight and diff
            curWeight+= boxes[i][1];
            if (i> 0 && !consecutivePorts[i-1])
            	differentPorts++;

            while (curWeight > maxWeight){ // drop more boxes (starting from the window start) because of weight constraint
                curWeight-= boxes[start][1];
                if (!consecutivePorts[start])
                	differentPorts--;
                start++;
            }

            while(start < i && memo[start] == memo[start+1]){
                // drop more boxes if there is no point to carry them
            	curWeight-= boxes[start][1];
                if (!consecutivePorts[start])
                	differentPorts--;
                start++;
            }

            memo[i+1] = differentPorts+2 + memo[start];
        }
        return memo[len];
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] boxes = {{1,2},{3,3},{3,1},{3,1},{2,4}};
		int portsCount = 3;
		int maxBoxes = 3;
		int maxWeight = 6;

		System.out.println(boxDelivering(boxes, portsCount, maxBoxes, maxWeight));
	}

}
