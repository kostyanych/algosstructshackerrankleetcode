package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * Alice and Bob continue their games with piles of stones.
 * There are several stones arranged in a row, and each stone has an associated value which is an integer given in the array stoneValue.
 *
 * Alice and Bob take turns, with Alice starting first.
 * On each player's turn, that player can take 1, 2 or 3 stones from the first remaining stones in the row.
 *
 * The score of each player is the sum of values of the stones taken. The score of each player is 0 initially.
 *
 * The objective of the game is to end with the highest score, and the winner is the player with the highest score and there could be a tie.
 * The game continues until all the stones have been taken.
 *
 * Assume Alice and Bob play optimally.
 *
 * Return "Alice" if Alice will win, "Bob" if Bob will win or "Tie" if they end the game with the same score.
 *
 * SOLUTION:
 * The idea is simple:
 * 1.  everyone takes max of what he can take (there is a little thing: shouldn't they no only maximize their profit but also take into account minimizing other's profit?)
 * 2.  go from hte end
 * 3a. from point i see what max value we can take by taking 1, 2 or 3 stones.
 * 3b. the next guy will take what max is there for i+1, i+2 or i+3 points
 * 3c. but our real max is what we took here + max we'd take after the next guy took his max
 * 3d. so we track AT EACH POINT the max 1st guy takes and the max the 2nd guy takes after him
 *
 */
public class StoneGameIII {

	static private String stoneGameIII(int[] stoneValue) {
        int len = stoneValue.length;
        int[][] memo = new int[len][2];
        memo[len-1][0]=stoneValue[len-1];

        for (int i=len-2;i>=0;i--) {
            //1 stone
            int max11=stoneValue[i]+memo[i+1][1];
            int max12=memo[i+1][0];

            //2 stones - we can always get 2 stones now
            int max21=stoneValue[i]+stoneValue[i+1];
            int max22=0;
            if (i+2<len) {
            	max21+=memo[i+2][1];
            	max22=memo[i+2][0];
            }
            int curMax1=max11;
            int curMax2=max12;
            if (max21>max11) {
                curMax1=max21;
                curMax2=max22;
            }

            //3 stones - check if we can reach there
            if (i+2<len) {
                int max31=stoneValue[i]+stoneValue[i+1]+stoneValue[i+2];
                int max32=0;
                if (i+3<len) {
                	max31+=memo[i+3][1];
                	max32=memo[i+3][0];
                }
                if (max31>curMax1) {
                    curMax1=max31;
                    curMax2=max32;
                }
            }

            memo[i][0]=curMax1;
            memo[i][1]=curMax2;
        }

        if (memo[0][0]>memo[0][1])
            return "Alice";
        if (memo[0][0]==memo[0][1])
            return "Tie";
        return "Bob";
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {1,-2,3,-4,5,-6,7};
		System.out.println(Arrays.toString(arr));
		System.out.println(stoneGameIII(arr));
	}

}
