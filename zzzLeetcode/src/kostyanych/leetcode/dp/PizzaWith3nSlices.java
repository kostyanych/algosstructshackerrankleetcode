package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * There is a pizza with 3n slices of varying size, you and your friends will take slices of pizza as follows:
 *
 * You will pick any pizza slice.
 * Your friend Alice will pick next slice in anti clockwise direction of your pick.
 * Your friend Bob will pick next slice in clockwise direction of your pick.
 * Repeat until there are no more slices of pizzas.
 *
 * Sizes of Pizza slices is represented by circular array slices in clockwise direction.
 *
 * Return the maximum possible sum of slice sizes which you can have.
 *
 * Solution: with given constraints if we want get n non-adjacent pieces, there is a way to do that
 * Proof: Proceed by induction.
 *   For  n=1 , the result is obvious.
 *   For general  n  it's sufficient to show that we can remove one slice without creating any adjacent slices among our remaining chosen  n−1  slices,
 *   thus restoring the inductive hypothesis.
 *   To see this, note that the are  n  "gaps" of unwanted slices between the  n  chosen slices, and the gaps are made of  2n  slices of pizza.
 *   Thus the average gap size is 2 and thus there must exist a gap of size at least 2.
 *   Remove a chosen slice next to this gap. One of the gap slices will be removed, leaving at least one remaining,
 *   therefore ensuring that the two chosen pieces nearest to the removed slice do not end up adjacent.
 *
 * Thus we can reduce the problem to "pick n non-adjacent (in the original configuration) pices to maximize value"
 * Since the array is circular and head and tail are adjacent, use the reasoning from HouseRobber2:
 * 		Calculate the max for indexes [0 , (3n-2)] and max for [1, (3n-1)] and choose the max between them
 *
 *
 * https://www.quora.com/Given-a-pizza-with-3n-slices-e-g-9-12-repeatedly-pick-a-slice-save-the-size-of-this-slice-When-you-do-this-the-slice-on-the-left-goes-to-someone-on-the-left-and-the-slice-on-the-right-goes-to-someone-on-the-right-Repeat-this-process-until-no-slices-are-left-How-can-you-write-a-program-to-find-a-list
 *
 */
public class PizzaWith3nSlices {

	private static int maxSizeSlices(int[] slices) {
		int len = slices.length;
		int k = len / 3;

		int[] a0 = Arrays.copyOf(slices, len-1);
		int[] a1 = Arrays.copyOfRange(slices, 1, len);

		return Math.max(linear(a0, k), linear(a1, k));
    }

	private static int linear(int[] arr, int k) {
		int len=arr.length+1;
		//we'll add both zeroed row and column to signify "there was value 0 before everything"

		int[][] memo = new int[len][k+1];

        int res = 0;
        //for the first pick we can get exactly that much as ther is in an src array cell
        for (int i = 1; i< len; i++) {
        	memo[i][1] = arr[i-1];
        	res = Math.max(res, memo[i][1]);
        }

        //whichever picks we do, we cannot pick more than first arr element
        for (int i = 1; i<= k; i++) {
        	memo[1][i] = arr[0];
        }

      //no we're starting with 2nd array's value (first one is already in the memo)
        for (int i=2; i<memo.length; i++) {
            for (int j=1; j<k+1; j++) {
                memo[i][j] = Math.max(memo[i-1][j], memo[i-2][j-1] + arr[i-1]);
            }
            res = Math.max(res, memo[i][k]);
        }
        return res;
    }

/*
	private static int linear(int[] arr, int k) {
		int[][] memo = new int[arr.length+2][k+1];

        int res = 0;
        for (int i=0; i<arr.length; i++)
        	memo[i][0] = 0;
        for (int i=2; i<memo.length; i++) {
            for (int j=1; j<k+1; j++) {
                memo[i][j] = Math.max(memo[i-1][j], memo[i-2][j-1] + arr[i-2]);
            }
            if (res<memo[i][k])
            	res=memo[i][k];
        }
        return res;
    }
*/
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[] arr = {8,9,8,6,1,1};
		int[] arr = {1,2,3,4,5,6};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxSizeSlices(arr));
	}

}
