package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * Given an array of integers A and let n to be its length.
 *
 * Assume Bk to be an array obtained by rotating the array A k positions clock-wise, we define a "rotation function" F on A as follow:
 * F(k) = 0 * Bk[0] + 1 * Bk[1] + ... + (n-1) * Bk[n-1].
 *
 * Calculate the maximum value of F(0), F(1), ..., F(n-1).
 *
 * Note: n is guaranteed to be less than 10^5.
 *
 * Another explanation:
 *
 * Given an array arr[] of n integers, find the maximum that maximizes the sum of the value of i*arr[i] where i varies from 0 to n-1.
 *
 * Example:
 *
 * Input: arr[] = {8, 3, 1, 2}
 * Output: 29
 * Explanation: Lets look at all the rotations,
 * {8, 3, 1, 2} = 8*0 + 3*1 + 1*2 + 2*3 = 11
 * {3, 1, 2, 8} = 3*0 + 1*1 + 2*2 + 8*3 = 29
 * {1, 2, 8, 3} = 1*0 + 2*1 + 8*2 + 3*3 = 27
 * {2, 8, 3, 1} = 2*0 + 8*1 + 3*2 + 1*1 = 17
 *
 */
public class RotateFunction {

	private static int maxRotateFunction(int[] arr) {
		int len=arr.length;
		if (len<2)
			return 0;
		int sumAll=0;
		int sum0=0;
		for (int i=0;i<len;i++) {
			sumAll+=arr[i];
			sum0+=(i)*arr[i];
		}

		int max=sum0;
		for (int i=len-1;i>0;i--) {
			int delta=sumAll-len*arr[i];
			sum0=sum0+delta;
			max=Math.max(max, sum0);
		}
		return max;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {4, 3, 2, 6};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxRotateFunction(arr));
		arr = new int[]{8, 3, 1, 2};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxRotateFunction(arr));
		arr = new int[]{3, 2, 1};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxRotateFunction(arr));

	}
}
