package kostyanych.leetcode.dp;

import java.util.Arrays;

/**
 * You are a professional robber planning to rob houses along a street.
 * Each house has a certain amount of money stashed.
 * All houses at this place are arranged in a circle.
 * That means the first house is the neighbor of the last one.
 * Meanwhile, adjacent houses have security system connected and it will automatically contact the police
 * 		if two adjacent houses were broken into on the same night.
 *
 * Given a list of non-negative integers representing the amount of money of each house,
 * determine the maximum amount of money you can rob tonight without alerting the police.
 *
 * Solution is based on the idea that we should use 2 runs to treat both ends of an array:
 *
 * A,B,C,D,E,F
 *
 * A - B - C
 * |       |
 * F - E - D
 *
 * Since it's a circle, it doesn't matter where we start from (we'll come round anyway).
 * So, if we stand with A, we cannot use house F.
 * If we want to use house F, we'll have to start with house B.
 *
 * So we'll run from A to E. And then from B to F. And will choose the max of those 2.
 *
 * The max of a single run is simple:
 * 1. we can skip the house (use the max we have so far - at the previous house)
 * 2. or rob the house and add it to the max we had at the last VALID house (this index - 2)
 *
 *
 */
public class HouseRobberII {

	static private int rob(int[] nums) {
		int len = nums.length;
		if (len==0)
			return 0;
		if (len==1)
			return nums[0];
		if (len==2)
			return Math.max(nums[0],nums[1]);
		int[] first=new int[len-1];
		int[] second=new int[len-1];

		first[0]=nums[0];
		first[1]=Math.max(nums[0],nums[1]);
		for (int i=2;i<len-1;i++) {
			first[i]=Math.max(nums[i]+first[i-2],first[i-1]);
		}
		second[0]=nums[1];
		second[1]=Math.max(nums[1],nums[2]);
		for (int i=2;i<len-1;i++) {
			second[i]=Math.max(nums[i+1]+second[i-2],second[i-1]);
		}

		return Math.max(first[len-2], second[len-2]);
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {1,2,3,1};
		System.out.println(Arrays.toString(arr));
		System.out.println(rob(arr));
	}

}
