package kostyanych.leetcode.dp;

/**
 * A message containing letters from A-Z is being encoded to numbers using the following mapping:
 *
 * 'A' -> 1
 * 'B' -> 2
 * ...
 * 'Z' -> 26
 *
 * Given a non-empty string containing only digits, determine the total number of ways to decode it.
 */
public class DecodeWays {

	final static int Z=26;

	 private static int numDecodings(String s) {
		 int len=s.length();
		 if (len==0)
			 return 0;
		 int[] arr=new int[len];
		 if (s.charAt(len-1)!='0')
			 arr[len-1]=1;
		 else
			 arr[len-1]=0;

		 for (int pos=len-2;pos>=0;pos--) {
			 int curVal=0;
			 char ccur=s.charAt(pos);
			 char cnext=s.charAt(pos+1);
			 if (ccur!='0') {
				 curVal=arr[pos+1];
				 int code=(ccur-'0')*10+(cnext-'0');
				 if (code<=Z) {
					 if (pos+2>=len)
						 curVal+=1;
					 else
						 curVal+=arr[pos+2];
				 }
			 }
			 arr[pos]=curVal;
		 }

		 return arr[0];
	 }

	 public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="12";
		System.out.println(s);
		System.out.println(numDecodings(s));
		s="226";
		System.out.println(s);
		System.out.println(numDecodings(s));
		s="0";
		System.out.println(s);
		System.out.println(numDecodings(s));
	}

}
