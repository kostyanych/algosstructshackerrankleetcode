package kostyanych.leetcode.dp;

/**
 * Given n orders, each order consist in pickup and delivery services.
 *
 * Count all valid pickup/delivery possible sequences such that delivery(i) is always after of pickup(i).
 *
 * Since the answer may be too large, return it modulo 10^9 + 7.
 *
 * Constraints:
 * 1 <= n <= 500
 *
 * SOLUTION:
 * 1.  at any stage we can do P pickups and D deliveries. Let's denote the stage as [P;D]
 * 2.  at first we can only do n pickups and no deliveries yet : [n,0]
 * 3.  at every next stage we can do a pickup thus [P,D] -> [P-1, D+1] or do a delivery:[P,D] -> [P, D-1]
 *         (every new pickup leads to the number of deliveries increasing, and number of available pickups decreasing;
 *          every delivery only leads to the number of pickups decreasing)
 * 4a. how many different pickups we can do at any stage? P
 * 4b. how many different deliveries? D
 * 4c. So (from 3, 4a and 4b) the ans([P,D])=P*ans([P-1,D+1])+D*ans([P, D-1])
 * 5.  Obvious corner cases:
 * 5a. [1,0] = 1
 * 5b. [0,n] = n! (we need to count every order of deliveries - all permutations of n numbers)
 */
public class CountAllValidPickupAndDeliveryOptions {

	static private long[][] memo=null;

    static final int MOD = 1_000_000_007;

    static private int countOrders(int n) {

        if (n==1)
            return 1;

        memo=new long[n+1][n+1];
        memo[0][1]=1;
        for (int i=2;i<=n;i++) {
            memo[0][i]=(memo[0][i-1]*i)%MOD;
        }
        return (int)((n*calc(n-1,1))%MOD);
    }

    static private int countOrdersIterative(int n) {
        if (n==1)
            return 1;

        memo=new long[n+1][n+1];
        memo[0][1]=1;
        memo[0][0]=1;
        for (int i=2;i<=n;i++) {
            memo[0][i]=(memo[0][i-1]*i)%MOD;
        }
        for (int p=1;p<=n;p++) {
        	for (int d=0;d<=n-p;d++) {
        		memo[p][d]=(p*memo[p-1][d+1])%MOD;
        		if (d>0)
        			memo[p][d]=(memo[p][d]+d*memo[p][d-1])%MOD;
        	}
        }
        return (int)(memo[n][0]);
    }

    static private long calc(int toPick, int toDeliver) {
    	long[][] mmm = memo;
        if (memo[toPick][toDeliver]!=0)
            return memo[toPick][toDeliver];

        if (toPick==1 && toDeliver==0)
        	return 1;
        if (toPick==0 && toDeliver==1)
        	return 1;

        if (toDeliver==0) {
    		if (toPick==1)
    			return 1;
    		memo[toPick][toDeliver]=(toPick*calc(toPick-1,1))%MOD;
    		return memo[toPick][toDeliver];
    	}
        long buf=0;
        if (toPick>0)
        	buf=(toPick*calc(toPick-1,toDeliver+1))%MOD;
        if (toDeliver>0)
        	buf=(buf+toDeliver*calc(toPick,toDeliver-1))%MOD;
        memo[toPick][toDeliver]=buf;
        return buf;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(countOrders(500));
		System.out.println(countOrdersIterative(500));
    }

}
