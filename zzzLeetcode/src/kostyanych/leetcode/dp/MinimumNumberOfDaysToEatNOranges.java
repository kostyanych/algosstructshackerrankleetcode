package kostyanych.leetcode.dp;

import java.util.HashMap;
import java.util.Map;

/**
 * There are n oranges in the kitchen and you decided to eat some of these oranges every day as follows:
 *
 * 1. Eat one orange.
 * 2. If the number of remaining oranges (n) is divisible by 2 then you can eat  n/2 oranges.
 * 3. If the number of remaining oranges (n) is divisible by 3 then you can eat  2*(n/3) oranges.
 *
 * You can only choose one of the actions per day.
 * Return the minimum number of days to eat n oranges.
 *
 * Constraints:
 * 1 <= n <= 2*10^9
 *
 * Solution:
 * obviously eating oranges by 1 takes more time.
 * So our betters choices are (n/2)+n%2 and (n/3)+n%3. Take minimum time of those and we're done.
 */
public class MinimumNumberOfDaysToEatNOranges {

	private static Map<Integer,Integer> memo = new HashMap<>();

    private static int minDays(int n) {
        if (n==0)
			return 0;
		if (n==1)
			return 1;
		if (n<4)
			return 2;
		return helper(n);
    }

    private static int helper(int n) {
		if (n==0)
			return 0;
		if (n==1)
			return 1;
		if (n<4)
			return 2;
        Integer m = memo.get(n);
		if (m!=null)
			return m;
		int a = n % 2 + helper(n / 2);
		int b = n % 3 + helper(n / 3);
		m = 1 + Math.min(a, b);
        memo.put(n,m);
		return m;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("10: "+minDays(10));
		System.out.println("56: "+minDays(56));
		System.out.println("6: "+minDays(6));
		System.out.println("9209408: "+minDays(9209408));
	}

}
