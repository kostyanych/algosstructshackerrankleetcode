package kostyanych.leetcode.dp;

/**
 * You are given coins of different denominations and a total amount of money.
 * Write a function to compute the number of combinations that make up that amount.
 * You may assume that you have infinite number of each kind of coin.
 */
public class CoinChange2 {

	/**
	 * Going thru memo array from minCoin up to amount;
	 * memo[0]=1 (we can only have 1 way to make 0 amount)
	 * Then iterate thru every coin this way.
	 * memo[i] = memo[i-coin]+memo[i];
	 * On the first run we'll have 1 only at those indices that correspond to x*coin;
	 * On successive runs we'll have memo[i] increased at those indices that correspond to x*currentCoin and was set on previous runs.
	 *
	 * For example [2,3] and the amount is 6.
	 * memo on init: [1,0,0,0,0,0,0]
	 *
	 * memo on the first run: [1,0,1,0,1,0,1]
	 *
	 * memo on the first run: [1,0,1,1,1,0,2]
	 *
	 * Return memo[amount] then.
	 *
	 * @param amount
	 * @param coins
	 * @return
	 */
	private static int change(int amount, int[] coins) {
		if (amount == 0)
			return 1;
		int[] memo = new int[amount + 1];
		memo[0]=1;

		for (int coin : coins) {
			for (int a = coin; a <= amount; a++) {
				memo[a] += memo[a - coin];
			}
		}
		return memo[amount];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {11,5,7,8,9,10,3};
		System.out.println(change(500,arr));
	}

}
