package kostyanych.leetcode.dp;

/**
 * A message containing letters from A-Z can be encoded into numbers using the following mapping:
 *
 * 'A' -> "1"
 * 'B' -> "2"
 * ...
 * 'Z' -> "26"
 *
 * To decode an encoded message, all the digits must be grouped then mapped back into letters using the reverse of the mapping above
 * 		(there may be multiple ways). For example, "11106" can be mapped into:
 * 			"AAJF" with the grouping (1 1 10 6)
 * 			"KJF" with the grouping (11 10 6)
 * 			Note that the grouping (1 11 06) is invalid because "06" cannot be mapped into 'F' since "6" is different from "06".
 *
 * In addition to the mapping above, an encoded message may contain the '*' character, which can represent any digit from '1' to '9' ('0' is excluded).
 * For example, the encoded message "1*" may represent any of the encoded messages "11", "12", "13", "14", "15", "16", "17", "18", or "19".
 * Decoding "1*" is equivalent to decoding any of the encoded messages it can represent.
 *
 * Given a string s containing digits and the '*' character, return the number of ways to decode it.
 *
 * Since the answer may be very large, return it modulo 10^9 + 7.
 *
 * Constraints:
 *  1 <= s.length <= 10^5
 *  s[i] is a digit or '*'.
 */
public class DecodeWaysII {

	private static int MOD = 1_000_000_007;

	private static int numDecodings(String s) {
        int len = s.length();
        long[] arr=new long[len+1];

        arr[0]=1;

        int cprev=s.charAt(0);
        if (cprev=='*')
            arr[1]=9;
        else {
            int ic=cprev-'0';
            if (ic<1 || ic>9)
                return 0;
            arr[1]=1;
        }

        for (int i=1;i<len;i++) {
            char c=s.charAt(i);
            int ci=c-'0';

            //consider previous
            if (cprev=='*') {//0. if previous is '*'
                if (c=='*') {
                    arr[i+1]=(arr[i-1]*15)%MOD;
                }
                else {
                    if (ci==0)
                        arr[i+1]=(arr[i-1]*2)%MOD;
                    else if (ci<7)
                        arr[i+1]=(arr[i-1]*2)%MOD;
                    else
                        arr[i+1]=arr[i-1];
                }
            }
            else if (cprev=='1') {//1. previous is 1
                if (c=='*')
                    arr[i+1]=(arr[i-1]*9)%MOD;
                else
                    arr[i+1]=arr[i-1];
            }
            else if (cprev=='2') {//2. previous is 2
                if (c=='*')
                    arr[i+1]=(arr[i-1]*6)%MOD;
                else if (ci<7)
                    arr[i+1]=arr[i-1];
            }
            //don't consider previous
            if (c=='*')
                arr[i+1]=(arr[i+1]+(arr[i]*9)%MOD)%MOD;
            if (ci>0 && ci<=9)
                arr[i+1]=(arr[i+1]+arr[i])%MOD;

            cprev=c;
        }

        return (int)arr[len];
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(numDecodings("2639"));
	}

}
