package kostyanych.leetcode.dp;

public class BestTimeToBuyAndSellStockWithCooldown {

	/**
	 * STATE 1: cooldown. We don't have stock. We doing nothing today because
	 *   1a. we sold yesterday
	 *   1b. we just don't feel like it, but (since we don't have stock) yesterday was also a cooldown day
	 *
	 *   from STATE 1 we can go into STATE 2
	 *
	 * STATE 2: we got stock
	 *   2a. we just bought stock today
	 *   2b. we're holding on whatever we bout some time in the past.
	 *
	 *   from STATE 2 we can go into TRANSITIONAL pseudoSTATE 3
	 *
	 * STATE 3: we sell our stock
	 *
	 *   from STATE 3 we MUST go to STATE 1
	 *
	 * @param prices
	 * @return
	 */
	private static int maxProfit(int[] prices) {
		int len=prices.length;

		//edge case:
		//less than 2 days
		if(len<2)
        	return 0;

		int coolDownProfit=0;
		int sellProfit=0;
		int boughtOrHoldProfit=Integer.MIN_VALUE;

		for (int price : prices) {

			int prevCoolDownProfit=coolDownProfit;
			int prevSellProfit=sellProfit;
			int prevBoughtOrHoldProfit=boughtOrHoldProfit;

            // Max profit of cooldown comes from either 1a (sold yesterday) or 1b (was a cooldown yesterday)
			coolDownProfit = Math.max(prevSellProfit, prevCoolDownProfit);

            // Max profit of hold on comes from either 2a (bought stock today) or 2b (keep holding what you had yesterday)
			boughtOrHoldProfit = Math.max(prevCoolDownProfit - price, prevBoughtOrHoldProfit);

			// Profit of sell comes from what we had on boughtOrHold yesterday + price of selling stock today
			sellProfit = prevBoughtOrHoldProfit + price;

		}
		//we can only maximally profit on cooldowns or sells, 'cause for boughtOrHoldProfit the last action was a buy that actually lowers the profit
		return Math.max(sellProfit, coolDownProfit);
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {1,2,3,0,2};
		System.out.println(maxProfit(arr));
	}

}
