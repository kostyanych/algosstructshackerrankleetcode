package kostyanych.leetcode.dp;

import kostyanych.leetcode.helpers.Printer;

/**
 * Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.
 *
 * You have the following 3 operations permitted on a word:
 *  Insert a character
 *  Delete a character
 *  Replace a character
 *
 *  build a DP matrix.
 *  edit from "horse" to "ros"
 *  1st row (and 1st column) shows how many operation we need to convert first i characters to an empty string
 *    (or how many operation we need to convert empty string to first i characters of the target string)
 *  then we go left to right downwards
 *  if the char on the left is the same that the char on top, then the min edit distance at that point is the same as at previous diagonal point (as if there were no new characters)
 *  it the chars are different, then we have to replace the char (that's one op) and add it to min distance of any previous points
 *
 *
 *  - - h o r s e
 *  - 0 1 2 3 4 5
 *  r 1 1 2 3 4 5
 *  o 2 2 1 2 3 4
 *  s 3 3 2 2 2 3
 */
public class EditDistance {

	private static int minDistance(String word1, String word2) {
		int fromLen=word1.length();
		int toLen=word2.length();

		int[][] memo = new int[toLen+1][fromLen+1];
		for (int i=0;i<fromLen+1;i++) {
			memo[0][i] = i;
		}
		for (int j=0;j<toLen+1;j++) {
			memo[j][0] = j;
		}

		for (int r=1;r<=toLen;r++) {
			for (int c=1;c<=fromLen;c++) {

				if (word1.charAt(c-1)!=word2.charAt(r-1))
					memo[r][c]=Math.min(memo[r-1][c-1], Math.min(memo[r][c-1],memo[r-1][c]))+1;
				else
					memo[r][c]=memo[r-1][c-1];
			}
		}

		return memo[toLen][fromLen];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("zoo -> zo");
		System.out.println(minDistance("zoo", "zo"));

		System.out.println("horse -> ros");
		System.out.println(minDistance("horse", "ros"));
		System.out.println("intention -> execution");
		System.out.println(minDistance("intention", "execution"));
	}


}
