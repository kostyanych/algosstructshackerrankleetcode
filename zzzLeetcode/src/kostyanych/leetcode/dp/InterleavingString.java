package kostyanych.leetcode.dp;

/**
 * There is a strange printer with the following two special requirements:
 *
 * The printer can only print a sequence of the same character each time.
 * At each turn, the printer can print new characters starting from and ending at any places, and will cover the original existing characters.
 * Given a string consists of lower English letters only, your job is to count the minimum number of turns the printer needed in order to print it.
 *
 * see https://www.youtube.com/watch?v=ih2OZ9-M3OM   ("String Interleaving Dynamic Programming")
 *
 * NOTE: turning string into char array to iterate thru its characters actually improves speed
 *
 */
public class InterleavingString {

	private static boolean isInterleave(String s2, String s1, String s3) {
		int l1=s1.length();
		int l2=s2.length();
		int l3=s3.length();

		if (l1+l2!=l3)
			return false;

		char[] c1 = s1.toCharArray();
		char[] c2 = s2.toCharArray();
		char[] c3 = s3.toCharArray();

		boolean[][] mx=new boolean[l1+1][l2+1];
		mx[0][0]=true;
		for (int i=1;i<=l2;i++) {
			if (c2[i-1]==c3[i-1] && mx[0][i-1])
				mx[0][i]=true;
		}
		for (int i=1;i<=l1;i++) {
			if (c1[i-1]==c3[i-1] && mx[i-1][0])
				mx[i][0]=true;
		}

		for (int i1=0;i1<l1;i1++) {
			int ind=i1+1;
			for (int i2=0;i2<l2;i2++) {
				char c=c3[ind+i2];
				if (c==c1[i1] && mx[i1][i2+1])
					mx[i1+1][i2+1]=true;
				else if (c==c2[i2] && mx[i1+1][i2])
					mx[i1+1][i2+1]=true;
			}
		}

		return mx[l1][l2];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s1 = "aabcc";
		String s2 = "dbbca";
		String s3 = "aadbbcbcac";
		System.out.println(isInterleave(s1,s2,s3));
	}

}
