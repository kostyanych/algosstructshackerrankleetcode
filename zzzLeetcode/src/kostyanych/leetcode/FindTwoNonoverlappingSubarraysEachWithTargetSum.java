package kostyanych.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of integers arr and an integer target.
 *
 * You have to find two non-overlapping sub-arrays of arr each with sum equal target.
 * There can be multiple answers so you have to find an answer where the sum of the lengths of the two sub-arrays is minimum.
 *
 * Return the minimum sum of the lengths of the two required sub-arrays, or return -1 if you cannot find such two sub-arrays.
 *
 * Constraints:
 *   1 <= arr.length <= 10^5
 *   1 <= arr[i] <= 1000
 *   1 <= target <= 10^8
 */
public class FindTwoNonoverlappingSubarraysEachWithTargetSum {

	public static int minSumOfLengths(int[] arr, int target) {
		int[] minsf = new int[arr.length];
		Map<Integer, Integer> map = new HashMap<>();
		int sum = 0;
		int res=-1;
		for (int i = 0; i < arr.length; i++) {
			sum+=arr[i];
			if (sum<target)
				minsf[i]=i>0?minsf[i-1] : 0;
			else if (sum==target) {
				minsf[i]=i+1;
			}
			else if (sum>target) {
				Integer prevIndex = map.get(sum-target);
				if (prevIndex!=null) {
					int len=i-prevIndex;
					minsf[i]=minsf[i-1]==0? len : Math.min(minsf[i-1], len);
					if (minsf[prevIndex]!=0) {
						int cand=len+minsf[prevIndex];
						if (res<0 || cand<res)
							res=cand;
					}
				}
				else
					minsf[i]=i>0?minsf[i-1] : 0;
				if (res==2)
					return res;
			}
			else
				minsf[i]=i>0?minsf[i-1] : 0;
			map.put(sum, i);
		}
//		System.out.println(Arrays.toString(arr));
//		System.out.println(Arrays.toString(minsf));
		return res;
	}

	public static void testMe(boolean isNeeded) {
		int[] arr = {2,2,2,1,1,1,1};
		int target = 3;
		System.out.println(minSumOfLengths(arr, target));
	}

}
