package kostyanych.leetcode;

import java.util.Arrays;

/**
 * We distribute some number of candies, to a row of n = num_people people in the following way:
 *
 * We then give 1 candy to the first person, 2 candies to the second person, and so on until we give n candies to the last person.
 * Then, we go back to the start of the row, giving n + 1 candies to the first person, n + 2 candies to the second person,
 *    and so on until we give 2 * n candies to the last person.
 *
 * This process repeats (with us giving one more candy each time, and moving to the start of the row after we reach the end)
 *   until we run out of candies.
 * The last person will receive all of our remaining candies (not necessarily one more than the previous gift).
 *
 * Return an array (of length num_people and sum candies) that represents the final distribution of candies.
 *
 */
public class DistributeCandiesToPeople {

	private static int[] distributeCandies(int candies, int num_people) {
		int fulls=0;
//		int sum=0;
//		int delta=(num_people-1)*num_people/2;
//		while (true) {
//			int newsum=(fulls*num_people+1)*num_people+delta;
//			if (newsum+sum<candies){
//				sum+=newsum;
//				fulls++;
//			}
//			else
//				break;
//		}
//System.out.println(delta);
//System.out.println(fulls);

		int s1=(num_people+1)*num_people/2;
		int ns=num_people*num_people;
		double a = ns/2.;
		double b = s1-a;
		double c = -candies;
		double D = b*b-2*ns*c;
		double sqrt=Math.sqrt(D);
		fulls=(int)((sqrt-b)/ns);

		int sum=fulls*s1+ns*((fulls-1)*fulls)/2;

		int[] res=new int[num_people];
		if (fulls>0) {
			for (int i=0;i<num_people;i++) {
				//int px=fulls==0?0:(fulls)*num_people;
				int px=fulls*(fulls-1)/2*num_people;
				res[i]=px+fulls*(i+1);
			}
		}
		//System.out.println(Arrays.toString(res));
		candies-=sum;
		int ind=0;
		while (candies>0) {
			int togive=(fulls)*num_people+(ind+1);
			if (candies<=togive) {
				res[ind]+=candies;
				break;
			}
			res[ind]+=togive;
			candies-=togive;
			ind++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] res=distributeCandies(1000000000,1000);
		System.out.println(Arrays.toString(res));
	}

}
