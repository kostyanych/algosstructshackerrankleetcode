package kostyanych.leetcode;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * Given an array of integers, find out whether there are two distinct indices i and j in the array such that
 * 	the absolute difference between nums[i] and nums[j] is at most valDistance
 *  and the absolute difference between i and j is at most indDistance.
 *
 */
public class ContainsDuplicateIII {

	private static boolean containsNearbyAlmostDuplicate(int[] nums, int indDistance, int valDistance) {
		if (valDistance<0 || indDistance<=0)
            return false;

        //we're using long instead of int, because we cannot know for sure that nums[i]+valDistance will fit into Integer.MAX_VALUE
        TreeSet<Long> set = new TreeSet<>();

        for (int i = 0; i < nums.length; i++) {
            // |nums[i] - nums[j]| <= valDistance
            // if nums[j] <= nums[i] then:
            // nums[i] - valDistance <= nums[j]

            // if nums[j] >= nums[i] then:
            // nums[j] <= valDistance + nums[i]

            // thus:
            // nums[i] - valDistance <= nums[j] <= valDistance + nums[i]

            //for every i check is there anything in the range [num - valDistance; num + valDistance]
            //in the size from [i-k;i+k]

            //but actually we don't need to look ahead of us (i+k part) - when we get to i+k we'll get [i;i+k] part by looking back
            //so we only care the back part of the sliding window

            //NOTE: we're useing set and removing the element from it, which falls out from the sliding window
            //WHY we don't care that there can be a duplicate of this element still in a sliding window?
            //BECAUSE if it were the condition that we're looking for would be met and we'd have returned true already

            if (set.size() > indDistance)
                set.remove((long)nums[i-indDistance-1]);

            long min=nums[i] - (long)valDistance;
            long max=nums[i] + (long)valDistance;
            System.out.println(nums[i]);
            System.out.println(min);
            System.out.println(max);
            Set<Long> subset = set.subSet(min,true,max,true);
            if (!subset.isEmpty())
                return true;

            set.add((long)nums[i]);
        }
        return false;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {1,5,9,1,5,9};
		int indDistance=2;
		int valDistance=3;
		System.out.println(Arrays.toString(arr));
		System.out.println(containsNearbyAlmostDuplicate(arr, indDistance, valDistance));
	}

}
