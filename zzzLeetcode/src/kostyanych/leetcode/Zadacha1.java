package kostyanych.leetcode;

public class Zadacha1 {

	private static int sumOddLengthSubarrays(int[] arr) {
		int len = arr.length;
		int[] sums = new int[len];
		sums[0] = arr[0];
		for (int i=1; i< len; i++) {
			sums[i]=sums[i-1]+arr[i];
		}
		int res=sums[len-1];
		for (int sl=3;sl<len+1;sl+=2) {
			for (int j=len-1;j>=sl-1;j--) {
				int start=j-sl;
				int before = start<0 ? 0 : sums[start];
				res+=(sums[j]-before);
				if (start<0)
					break;
			}
		}
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[]arr = {10,11,12,13};
		System.out.println(sumOddLengthSubarrays(arr));
	}


}
