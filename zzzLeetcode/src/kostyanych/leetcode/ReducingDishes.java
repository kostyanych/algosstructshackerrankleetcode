package kostyanych.leetcode;

import java.util.Arrays;

/**
 * A chef has collected data on the satisfaction level of his n dishes. Chef can cook any dish in 1 unit of time.
 *
 * Like-time coefficient of a dish is defined as the [time taken to cook that dish PLUS previous dishes] multiplied by its satisfaction level
 *    i.e.  time[i]*satisfaction[i]
 *
 * Return the maximum sum of Like-time coefficient that the chef can obtain after dishes preparation.
 *
 * Dishes can be prepared in any order and the chef can discard some dishes to get this maximum value.
 *
 * Example: satisfaction = [-1,-8,0,5,-9] => exlude -8 and -9 and rearrange => [-1,0,5] => -1*1+2*0+5*3=14
 */
public class ReducingDishes {

	private static int maxSatisfaction(int[] satisfaction) {
		int len=satisfaction.length;
		Arrays.sort(satisfaction);

		int curSum=0;
		int curMax=0;
		for (int i=len-1;i>=0;i--) {
			if (satisfaction[i]+curSum<0)
				break;
			curSum+=satisfaction[i];
			curMax+=curSum;
		}
		return curMax;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {-1,-8,0,5,-9};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxSatisfaction(arr));

		arr = new int[] {4,3,2};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxSatisfaction(arr));

		arr = new int[] {-1,-4,-5};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxSatisfaction(arr));

		arr = new int[] {-2,5,-1,0,3,-3};
		System.out.println(Arrays.toString(arr));
		System.out.println(maxSatisfaction(arr));
	}


}
