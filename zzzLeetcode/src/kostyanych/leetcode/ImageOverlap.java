package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Two images A and B are given, represented as binary, square matrices of the same size.  (A binary matrix has only 0s and 1s as values.)
 *
 * We translate one image however we choose (sliding it left, right, up, or down ANY number of units), and place it on top of the other image.
 * After that, the overlap of this translation is the number of positions that have a 1 in both images.
 *
 * (Note also that a translation does not include any kind of rotation.)
 *
 * What is the largest possible overlap?
 *
 *
 * Notes:
 *
 * 1 <= A.length = A[0].length = B.length = B[0].length <= 30
 * 0 <= A[i][j], B[i][j] <= 1
 *
 */
public class ImageOverlap {

	static class Coord {
		int x;
		int y;
		public Coord(int x, int y) {
			this.x=x;
			this.y=y;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Coord other = (Coord) obj;
			if (x != other.x)
				return false;
			if (y != other.y)
				return false;
			return true;
		}


	}

	static private final List<Coord> onesA = new ArrayList<>();
	static private final List<Coord> onesB = new ArrayList<>();
	static private final Map<Coord, Integer> shiftCount = new HashMap<>();

	//private static int largestOverlapShiftCount(int[][] A, int[][] B) {
	private static int largestOverlap(int[][] A, int[][] B) {
		int len = A.length;
		for (int r = 0; r < len; r++) {
            for (int c = 0; c < len; c++) {
            	if (A[r][c]==1)
            		onesA.add(new Coord(r,c));
            	if (B[r][c]==1)
            		onesB.add(new Coord(r,c));
            }
		}

		int maxOverlaps = 0;

		for (Coord a : onesA) {
			for (Coord b : onesB) {
				Coord shift = new Coord (a.x-b.x, a.y-b.y);
				Integer count=shiftCount.get(shift);
				int icount=1;
				if (count!=null)
					icount=count+1;
				shiftCount.put(shift, icount);
				maxOverlaps=Math.max(maxOverlaps, icount);
			}
		}

		return maxOverlaps;
	}




	private static int largestOverlapMine(int[][] A, int[][] B) {
		int len = A.length;
		int res=0;
		int max=0;

		for (int[] arr : A) {
			for (int el : arr) {
				if (el==1)
					max++;
			}
		}

		for (int[] arr : B) {
			for (int el : arr) {
				if (el==1)
					res++;
			}
		}
		max = Math.min(max, res);
		res=0;
		if (max==0)
			return 0;

		int s=1-len;
		for (int r=s; r<len;r++) {
			for (int c=s;c<len;c++) {
				res=Math.max(res, count(A, B, r, c, len));
				if (res==max)
					break;
			}
		}

		return res;
	}

	private static int count(int[][] m, int[][] s, int row, int col, int len) {
		int res=0;
		for (int r=0;r<len;r++) {
			int br=r+row;
			if (br<0 || br>=len)
				continue;
			for (int c=0;c<len;c++) {
				int bc=c+col;
				if (bc<0 || bc>=len)
					continue;
				if (m[r][c]==1 && s[br][bc]==1)
					res++;
			}
		}
		return res;
	}



	private static int largestOverlapSome(int[][] A, int[][] B) {
		int res = 0;
        int len = A.length;
        int[][] count = new int[len * 2][len * 2];
        for (int i = 0; i < len; i++) {
        	for (int j = 0; j < len; j++) {
        		for (int m = 0; m < len; m++) {
        			for (int n = 0; n < len; n++) {
        				if (A[i][j] == 0 || B[m][n] == 0)
        					continue;
        				res = Math.max(res, ++count[len + i - m][len + j - n]);
        			}
        		}
        	}
        }
        return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {0,1,2,0};
		int[][] A = {{1,1,0}, {0,1,0}, {0,1,0}};
		int[][] B = {{0,0,0}, {0,1,1}, {0,0,1}};
		System.out.println(largestOverlap(A, B));
	}


}
