package kostyanych.leetcode;

import java.util.Arrays;

import kostyanych.leetcode.helpers.Printer;

/**
 * Winter is coming! Your first job during the contest is to design a standard heater with fixed warm radius to warm all the houses.
 *
 * Now, you are given positions of houses and heaters on a horizontal line,
 * find out minimum radius of heaters so that all houses could be covered by those heaters.
 *
 * So, your input will be the positions of houses and heaters seperately, and your expected output will be the minimum radius standard of heaters.
 *
 * Note:
 *   Numbers of houses and heaters you are given are non-negative and will not exceed 25000.
 *   Positions of houses and heaters you are given are non-negative and will not exceed 10^9.
 *   As long as a house is in the heaters' warm radius range, it can be warmed.
 *   All the heaters follow your radius standard and the warm radius will the same.
 *
 */
public class Heaters {

	private static int findRadius(int[] houses, int[] heaters) {
        Arrays.sort(houses);
        Arrays.sort(heaters);
System.out.println(Arrays.toString(houses));
System.out.println(Arrays.toString(heaters));
        int hslen=houses.length;
        int htlen=heaters.length;
        if (htlen==1)
            return Math.max(Math.abs(houses[0]-heaters[0]),Math.abs(houses[hslen-1]-heaters[0]));
        int left=-1;
        int right=-1;
        int rightind=0;
        int res=0;
        if (houses[0]<=heaters[0]) {
            right=heaters[0];
            rightind=0;
            res=right-houses[0];
        }
        else {
        	int newRight=findNewRight(heaters, houses[0], 0);
        	if (newRight<htlen) {
        		rightind=newRight;
                right=heaters[rightind];
                left=rightind>0 ? heaters[rightind-1] : -1;
            }
            else {
            	left=heaters[htlen-1];
                rightind=-1;
                right=-1;
            }
        }

        if (houses[0]>=heaters[0]) {
            left=heaters[0];
            right=heaters[1];
            rightind=1;
            res=Math.min(houses[0]-left,right-houses[0]);
        }
        else {
            right=heaters[0];
            rightind=0;
            res=right-houses[0];
        }
        for (int i=1;i<hslen;i++) {
            //1. we're overstepping previous right heater
            if (rightind>=0 && houses[i]>right) {
                int newRight=findNewRight(heaters, houses[i], rightind++);
                if (newRight<htlen) {
                    rightind=newRight;
                    right=heaters[rightind];
                    left=rightind>0 ? heaters[rightind-1] : -1;
                }
                else {
                    left=heaters[htlen-1];
                    rightind=-1;
                    right=-1;
                }
            }

            //2. all heaters are to the left
            if (rightind<0) { //no heaters to the right
                int dist=houses[i]-left;
                if (res<dist)
                    res=dist;
                continue;
            }
            //2. we're left of previous right heater
            if (houses[i]<=right) {
            	int minDist=right-houses[i];
            	if (left>=0)
                	minDist=Math.min(houses[i]-left,minDist);
            	res=Math.max(res,minDist);
                continue;
            }

        }

        return res;
    }

    private static int findNewRight(int[] heaters, int house, int startFrom) {
        int ind = Arrays.binarySearch(heaters, startFrom, heaters.length, house);
        if (ind>=0)
            return ind;
        return -ind-1;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] houses = {25921153,510616708};
		int[] heaters = {771515668,357571490,44788124,927702196,952509530};
		System.out.println(findRadius(houses, heaters));
	}

}
