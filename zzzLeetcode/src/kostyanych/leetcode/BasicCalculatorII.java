package kostyanych.leetcode;

import java.util.Stack;

/**
 * Implement a basic calculator to evaluate a simple expression string.
 *
 * The expression string contains only non-negative integers, +, -, *, / operators and empty spaces . The integer division should truncate toward zero.
 *
 * Note:
 * You may assume that the given expression is always valid.
 * Do not use the eval built-in library function.
 *
 */
public class BasicCalculatorII {

	static enum OpType {ADD, SUBSTR, MULT, DIV, NONE};

	private static class Op {
		final int firstOperand;
		final OpType opType;
		public Op(int operand, OpType type) {
		firstOperand=operand;
			opType=type;
		}

		public int calculate(int secondOperand) {
			switch (opType) {
				case ADD:
					return firstOperand+secondOperand;
				case SUBSTR:
					return firstOperand-secondOperand;
				case MULT:
					return firstOperand*secondOperand;
				case DIV:
					return firstOperand/secondOperand;
				default:
					return firstOperand;
			}
		}
	}

	static private int calculate(String s) {
		char[] chars = s.toCharArray();
		Stack<Op> st = new Stack<>();
		int operand=0;
		OpType lastType=null;
		for (char c : chars) {
			lastType=null;
			if (c>='0' && c<='9') {
				operand=collectDigits(operand,c-'0');
				continue;
			}
			if (c==' ')
				continue;
			if (c=='+')
				lastType=OpType.ADD;
			else if (c=='-')
				lastType=OpType.SUBSTR;
			else if (c=='*')
				lastType=OpType.MULT;
			else if (c=='/')
				lastType=OpType.DIV;
			process(st, operand, lastType);
			operand=0;
		}
		process(st, operand, OpType.NONE);
		return st.pop().firstOperand;
	}

	static private void process(Stack<Op> st, int right, OpType type) {
		if (st.isEmpty()) {
			st.push(new Op(right, type));
			return;
		}
		if (type==OpType.ADD || type==OpType.SUBSTR || type==OpType.NONE) { //do everything before that
			while (!st.isEmpty()) {
				Op prev = st.pop();
				right=prev.calculate(right);
			}
			st.push(new Op(right,type));
		}
		else { //only calculate if previous was * or /, otherwise just add operation
			Op prev = st.peek();
			if (prev.opType==OpType.ADD || prev.opType==OpType.SUBSTR)
				st.push(new Op(right,type));
			else {
				prev=st.pop();
				right=prev.calculate(right);
				st.push(new Op(right,type));
			}
		}
	}

	static private int collectDigits(int curNum, int digit) {
		return curNum*10+digit;
	}

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String str="3+2*2";
		System.out.println(str);
		System.out.println(calculate(str));
	}
}
