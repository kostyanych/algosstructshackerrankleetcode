package kostyanych.leetcode;

import java.util.Arrays;

/**
 * You are given a list of strings of the same length words and a string target.
 *
 * Your task is to form target using the given words under the following rules:
 *
 * 		target should be formed from left to right.
 * 		To form the ith character (0-indexed) of target, you can choose the kth character of the jth string in words if target[i] = words[j][k].
 * 		Once you use the kth character of the jth string of words, you can no longer use the xth character of any string in words where x <= k.
 * 			In other words, all characters to the left of or at index k become unusuable for every string.
 *
 *  Repeat the process until you form the string target.
 *  Notice that you can use multiple characters from the same string in words provided the conditions above are met.
 *  Return the number of ways to form target from words. Since the answer may be too large, return it modulo 10^9 + 7.
 *
 *  Constraints:
 *
 *  	1 <= words.length <= 1000
 *  	1 <= words[i].length <= 1000
 *  	All strings in words have the same length.
 *  	1 <= target.length <= 1000
 *  	words[i] and target contain only lowercase English letters.
 *
 */
public class NumberOfWaysToFormATargetStringGivenADictionary {

	private static int MOD=1_000_000_007;

	private static int[][] allWordChars;
	private static char[] tchars;
	private static int[][] memo;

	private static int numWays(String[] words, String target) {
		int wlen=words[0].length();
		tchars=target.toCharArray();
		int tlen=tchars.length;
		memo=new int[wlen][tlen];
		for (int i=0;i<wlen;i++) {
			Arrays.fill(memo[i], -1);
		}
		allWordChars=new int[wlen][26];
		for (String s : words) {
			char[] wc=s.toCharArray();
			for (int i=0;i<wlen;i++) {
				allWordChars[i][wc[i]-'a']++;
			}
		}

		return (int)(findFromLast(tlen-1, wlen-1));
	}

	private static long findFromLast(int neededInd, int maxInWord) {
		if (memo[maxInWord][neededInd]>=0)
			return memo[maxInWord][neededInd];
		long res=0;
		for (int i=maxInWord;i>=neededInd;i--) {
			int suchchars=allWordChars[i][tchars[neededInd]-'a'];
			if (suchchars==0)
				continue;
			if (neededInd==0)
				res=(res+suchchars)%MOD;
			else
				res=(res+(suchchars*findFromLast(neededInd-1, i-1))%MOD)%MOD;
		}
		memo[maxInWord][neededInd]=(int)res;
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(numWays(new String[] {"acca","bbbb","caca"},"aba"));
	}

}
