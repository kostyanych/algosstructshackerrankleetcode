package kostyanych.leetcode;

import java.util.HashMap;
import java.util.Map;

public class ThueMorseSequence {

	private static final Map<Integer, Integer> map = new HashMap<>();
	static {
		map.put(0, 0);
		map.put(1, 1);
		map.put(2, 1);
		map.put(3, 0);
	}

	private static String getFrom1basedIndex(int index) {
		String res="";
		for (int i=index-1;i<index+4;i++) {
			res=res+getDigit(i);
		}
		return res;
	}

	private static int getDigit(int idx) {
		if (map.containsKey(idx))
			return map.get(idx);
		int res = 3;
		if (idx%2==0)
			res=getDigit(idx/2);
		else
			res=1-getDigit((idx-1)/2);
		map.put(idx, res);
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("17: "+getFrom1basedIndex(17));
		System.out.println("60: "+getFrom1basedIndex(60));
		System.out.println("100: "+getFrom1basedIndex(100));
		System.out.println("200: "+getFrom1basedIndex(200));
		System.out.println("2050: "+getFrom1basedIndex(2050));
	}

}
