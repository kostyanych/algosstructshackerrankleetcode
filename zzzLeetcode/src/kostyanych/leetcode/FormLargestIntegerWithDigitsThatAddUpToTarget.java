package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given an array of integers cost and an integer target. Return the maximum
 * integer you can paint under the following rules:
 *
 * * The cost of painting a digit (i+1) is given by cost[i] (0 indexed).
 * * The total cost used must be equal to target.
 * * Integer does not have digits 0.
 * * Since the answer may be too large, return it as string.
 *
 * If there is no way to paint any integer given the condition, return "0".
 *
 * Constraints:
 *
 * * cost.length == 9
 * * 1 <= cost[i] <= 5000
 * * 1 <= target <= 5000
 *
 */
public class FormLargestIntegerWithDigitsThatAddUpToTarget {

//	static java.math.BigInteger max = new java.math.BigInteger("0");
//	private final static Map<String, List<List<Integer>>> map = new HashMap<>();

//	private final static int MAX_COST=5000;

	private static String largestNumber(int[] cost, int target) {
		//1. calculate maximum digits that we can use to get up to target
		int[] maxDigits = new int[target + 1];
        for (int t = 1; t <= target; t++) {
        	//maxDigits[t] = -(2*MAX_COST+1);
        	maxDigits[t] = Integer.MIN_VALUE;
            for (int i = 0; i < 9; i++) {
                if (t >= cost[i])
                	maxDigits[t] = Math.max(maxDigits[t], 1 + maxDigits[t - cost[i]]);
            }
        }

        if (maxDigits[target] < 0)
        	return "0";

        //2. now from target get needed digits
        //start with max digit (9) and see if this digit contributed to maxDigits[target]
        //that would mean maxDigits[target] == maxDigits[target - cost[i]] + 1 (see previous step)
        StringBuilder res = new StringBuilder();
        for (int i = 8; i >= 0; --i) {
            while (target >= cost[i] && maxDigits[target] == maxDigits[target - cost[i]] + 1) {
                res.append(1 + i);  //cost[] indices are zero based!
                target -= cost[i];
            }
        }
        return res.toString();
    }

/*
	private static String[] dp ;

    private static String largestNumber(int[] cost, int target) {

		dp = new String[target+1]; //string dp array to help us memoise

        find(cost, target);
System.out.println(Arrays.toString(dp));
        return dp[target] == ""?"0":dp[target]; // we check if the answer is "" then no string could be found
    }

    private static String find(int[] cost, int target){

        if (target == 0)
        	return ""; // base case. If target becomes 0, then it is possible to form a number

        if (dp[target]!=null)
        	return dp[target]; // memoisation to make the algorithm faster

        String curr = "0"; // base string
        for(int i = 8; i >= 0; i--){ // note that we loop backwards, thus any number formed is the maximum possible number of that length

            if (target - cost[i] >= 0){ // we only try to add the digit if its possible to choose from the cost array
                String x = find(cost, target - cost[i]); // this return the maximum number that can be formed using the remaing target once we have choosen digit d
                if (x.equals("0"))
                	continue; // if 0 is returned that means we couldnot form a number
                x = String.valueOf((i+1))+x;

                if(curr.equals("0") || x.length() > curr.length())
                	curr = x; // we make sure we choose that digit which forms the largest number

            }
        }

        dp[target] = curr; //store in dp array
        return dp[target];
    }



	private static String largestNumberMy(int[] cost, int target) {
		String[] memo = new String[target + 1];
		memo[0] = "";
		for (int i=1;i<=target;i++) {
			findMax(cost, i, memo);
		}
System.out.println(Arrays.toString(memo));
		return memo[target];
	}

	private static void findMax(final int[] cost, final int target, final String[] memo) {
		int len = cost.length;
		String max="";
		String curStr="";
		for (int i=0;i<len;i++) {
			if (cost[i]>target)
				continue;
			if (cost[i]==target)
				curStr=Integer.toString(i + 1);
			else {
				curStr=memo[target-cost[i]];
				if (!curStr.isEmpty())
					curStr=Integer.toString(i + 1)+curStr;
			}
			if ( (max.isEmpty() && curStr.isEmpty())
					|| max.length()<curStr.length()
					|| (max.length()==curStr.length() && max.compareTo(curStr) < 0))
				max=curStr;
		}
		memo[target]=max;
	}



	private static String largestNumberSome(int[] cost, int target) {
		String[] dp = new String[target + 1];
		String res = func(cost, target, dp);
		return res;
	}

	private static String func(int[] cost, int target, String[] dp) {
		if (target == 0)
			return "";
		if (dp[target] != null)
			return dp[target];
		String max = "0";
		for (int i = 0; i < 9; i++) {
			if (target >= cost[i]) {
				String curr = func(cost, target - cost[i], dp);
				if (curr.equals("0"))
					continue;
				curr = Integer.toString(i + 1) + curr;
				if (max.length() > curr.length())
					continue;
				if (max.equals("0") || curr.length() > max.length()
						|| curr.length() == max.length() && max.compareTo(curr) < 0)
					max = curr;
			}
		}
		return dp[target] = max;
	}

	private static String largestNumberSlow(int[] cost, int target) {
		process(cost, 0, target, new ArrayList<Integer>());
		return max.toString();
	}

	static private void checkMax(List<Integer> lst) {
		if (lst == null || lst.size() < 1)
			return;
		Collections.sort(lst, Collections.reverseOrder());
		StringBuilder sb = new StringBuilder(lst.size());
		for (Integer i : lst) {
			sb.append(i);
		}
		java.math.BigInteger buf = new java.math.BigInteger(sb.toString());
		if (buf.compareTo(max) > 0)
			max = buf;
	}

	private static List<List<Integer>> process(int[] cost, int curIndex, int target, List<Integer> currList) {
		int len = cost.length;
		if (target < 0 || curIndex >= len)
			return null;

		String key = curIndex + "|" + target;
		if (map.containsKey(key))
			return map.get(key);

		int thisNum = curIndex + 1;

		// we use 0 of thisNums and skip to next num
		List<Integer> curIterationList = new ArrayList<>(currList);
		List<List<Integer>> res = new ArrayList<>();
		List<List<Integer>> lst = null;
		if (curIndex + 1 < len) {
			lst = process(cost, curIndex + 1, target, new ArrayList<>(curIterationList));
		}
		if (cost[curIndex] < target) {
			// now we use 1 of candidates[curIndex]
			curIterationList.add(thisNum);
			List<List<Integer>> buf = process(cost, curIndex, target - cost[curIndex],
					new ArrayList<>(curIterationList));
			if (lst != null) {
				if (buf != null)
					lst.addAll(buf);
			} else if (buf != null)
				lst = buf;
		} else if (target == 0) {
			checkMax(currList);
			List<Integer> lll = new ArrayList<>();
			lll.add(thisNum);
			res.add(lll);
			return res;
		}
		if (lst != null) {
			for (List<Integer> li : lst) {
				li.addAll(0, currList);
			}
		}
		map.put(key, lst);
		return lst;
	}
*/
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		int[] arr = { 4,3,2,5,6,7,2,5,5 };
//		int target = 9;
		//int[] arr = { 20,6,20,10,5,20,20,15,20 };
		int[] arr = { 5,2,5,5,5,5,5,5,5 };
		int target = 4;
		System.out.println(Arrays.toString(arr));
		System.out.println(largestNumber(arr, target));
	}
}
