package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class JumpGameIV {

	
	private static int minJumps(int[] arr) {
		int len=arr.length;
		if (len==1)
			return 0;
		if (len==2)
			return 1;
		if (arr[0]==arr[len-1])
			return 1;
		
		Map<Integer,List<Integer>> map=new HashMap<>();
		
		for (int i=0;i<len;i++) {
			int n=arr[i];
			List<Integer> lst=map.get(n);
			if (lst==null) {
				lst=new ArrayList<>();
				map.put(n, lst);
			}
			lst.add(i);
		}
		
		boolean[] visited = new boolean[len];
		visited[0]=true;
		Queue<int[]> q = new LinkedList<>();
		q.add(new int[] {0,0});
		int res=-1;
		while (!q.isEmpty()) {
			int[] el=q.remove();
			int ind=el[0];
			int cost=el[1];
			if (ind==len-1) {
				res=cost;
				break;
			}
			if (ind>1 && !visited[ind-1])
				q.add(new int[] {ind-1,cost+1});
			if (ind<len-1 && !visited[ind+1])
				q.add(new int[] {ind+1,cost+1});
			List<Integer> lst=map.get(arr[ind]);
			if (lst!=null) {
				for (Integer ig : lst) {
					if (ig!=ind && !visited[ig])
						q.add(new int[] {ig,cost+1});
				}
				map.put(arr[ind], null);
			}
			visited[ind]=true;
		}
//		
//		
//		
//		int[] scores=new int[len];
//		Arrays.fill(scores, -1);
//		scores[len-1]=0;
//		List<Integer> lst=map.get(arr[len-1]);
//		for (Integer i : lst) {
//			if (i!=len-1)
//				scores[i]=1;
//		}
//		scores[len-2]=1;
//		lst=map.get(arr[len-2]);
//		for (Integer i : lst) {
//			if (i!=len-2)
//				scores[i]=2;
//		}
//		if (scores[0]>0)
//			return scores[0];
//
//		for (int i=len-3;i>=0;i--) {
//			if (scores[i]>0)
//				continue;
//
//			//int curi=i+1;
//			int score=Math.min(len-i-1,scores[i+1]+1);
//			int delta=1;
//			for (int j=i-1;j>=0;j--,delta++) {
//				if (scores[j]>0)
//					score=Math.min(score, delta+scores[j]);
//				if (delta>score)
//					break;
//			}
//			scores[i]=score;
//			lst=map.get(arr[i]);
//			for (Integer ig : lst) {
//				if (ig!=i)
//					scores[ig]=score+1;
//			}
//		}
//System.out.println(Arrays.toString(arr));
//System.out.println(Arrays.toString(scores));
//		return scores[0];
		return res;
    }
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {100,-23,-23,404,100,23,23,23,3,404};
		System.out.println(minJumps(arr));
		
		arr = new int[]{7};
		System.out.println(minJumps(arr));
		
		arr = new int[]{7,6,9,6,9,6,9,7};
		System.out.println(minJumps(arr));		

		arr = new int[]{6,1,9};
		System.out.println(minJumps(arr));		

		arr = new int[]{11,22,7,7,7,7,7,7,7,22,13};
		System.out.println(minJumps(arr));		

		arr = new int[]{68,-94,-44,-18,-1,18,-87,29,-6,-87,-27,37,-57,7,18,68,-59,29,7,53,-27,-59,18,-1,18,-18,-59,-1,-18,-84,-20,7,7,-87,-18,-84,-20,-27};
		System.out.println(minJumps(arr));		
		
	}
	
	
}
