package kostyanych.leetcode;

/**
 * You are given a binary string binary consisting of only 0's or 1's. You can apply each of the following operations any number of times:
 *
 * Operation 1: If the number contains the substring "00", you can replace it with "10".
 * For example, "00010" -> "10010"
 * Operation 2: If the number contains the substring "10", you can replace it with "01".
 * For example, "00010" -> "00001"
 *
 * Return the maximum binary string you can obtain after any number of operations.
 * Binary string x is greater than binary string y if x's decimal representation is greater than y's decimal representation.
 *
 * Constraints:
 *
 * 1 <= binary.length <= 10^5
 * binary consist of '0' and '1'.
 *
 * SOLUTION:
 * 1. all consecutive left 1s go straight to the result.
 * 2. then we need to move all zeroes to the left: 01010 -> 01001 -> 00101 -> 00011
 * 3. then all but one left consectutive 0s changes to 1: 000->100->110
 * 4. so, in fullness: 1101010 -> 11|01010 -> 11|00011-> 11|000|11 ->11|110|11 -> 1111011
 *
 */
public class MaximumBinaryStringAfterChange {

	static private String maximumBinaryString(String binary) {
		int len=binary.length();
		StringBuilder sb=new StringBuilder(len);
		int cnt=0;
		while (cnt<len && binary.charAt(cnt)=='1') {
			sb.append('1');
			cnt++;
		}
		int rem=len-cnt;
		if (rem==0)
			return sb.toString();
		int zeroes=1;
		for (int i=cnt+1;i<len;i++) {
			if (binary.charAt(i)=='0')
				zeroes++;
		}
		for (int i=0;i<zeroes-1;i++) {
			sb.append('1');
		}
		sb.append('0');
		rem-=zeroes;
		for (int i=0;i<rem;i++) {
			sb.append('1');
		}
        return sb.toString();
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s = "11";
		System.out.println(s);
		System.out.println(maximumBinaryString(s));
	}

}
