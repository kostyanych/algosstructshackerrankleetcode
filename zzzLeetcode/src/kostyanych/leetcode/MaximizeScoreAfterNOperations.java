package kostyanych.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * You are given nums, an array of positive integers of size 2 * n. You must perform n operations on this array.
 *
 * In the ith operation (1-indexed), you will:
 *
 * Choose two elements, x and y.
 * Receive a score of i * gcd(x, y).
 * Remove x and y from nums.
 * Return the maximum score you can receive after performing n operations.
 *
 * The function gcd(x, y) is the greatest common divisor of x and y.
 *
 * Constraints:
 *
 * 1 <= n <= 7
 * nums.length == 2 * n
 * 1 <= nums[i] <= 10^6
 *
 */
public class MaximizeScoreAfterNOperations {

	static private Map<String, Integer> map = new HashMap<>();

	static private int maxScore(int[] nums) {
		int len = nums.length;
		//int n = nums.length/2;
		int[][] gcds=new int[len][len];
		for (int i=0;i<len-1;i++) {
			for (int j=i+1;j<len;j++) {
				gcds[i][j]=gcd(nums[i],nums[j]);
			}
		}
		boolean[] restricted = new boolean[len];
		return calc(gcds, restricted, len, 1);
    }

	static private int calc(int[][] gcd, boolean[] restricted, int len, int iteration) {
		int rkey=genKey(restricted);
		String skey=iteration+"|"+rkey;
		Integer res = map.get(skey);
		if (res!=null)
			return res;

		res=0;
		for (int i=0;i<len;i++) {
			if (restricted[i])
				continue;
			for (int j=0;j<len;j++) {
				if (i==j || restricted[j] )
					continue;
				//boolean[] restCopy=Arrays.copyOf(restricted, restricted.length);
				boolean[] restCopy=restricted;
				restCopy[i]=true;
				restCopy[j]=true;
				int delta=0;
				if (i<j)
					delta = iteration*gcd[i][j];
				else
					delta = iteration*gcd[j][i];
				res=Math.max(res, delta+calc(gcd,restCopy,len,iteration+1));
				restCopy[i]=false;
				restCopy[j]=false;
			}
		}
		map.put(skey,res);
		return res;
	}

	static private int genKey(boolean[] rest) {
		int l = rest.length;
		int res=0;
		for (int i=0;i<l;i++) {
			res<<=1;
			if (rest[i])
				res+=1;
		}
		return res;
	}

	static private int gcd(int p, int q) {
        while (q != 0) {
            int temp = q;
            q = p % q;
            p = temp;
        }
        return p;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {773274,313112,131789,222437,918065,49745,321270,74163,900218,80160,325440,961730};
		System.out.println(maxScore(arr));
	}

}
