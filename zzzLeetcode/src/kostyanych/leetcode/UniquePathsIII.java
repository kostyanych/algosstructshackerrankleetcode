package kostyanych.leetcode;

import kostyanych.leetcode.helpers.Printer;

/**
 * On a 2-dimensional grid, there are 4 types of squares:
 *
 * 1 represents the starting square.  There is exactly one starting square.
 * 2 represents the ending square.  There is exactly one ending square.
 * 0 represents empty squares we can walk over.
 * -1 represents obstacles that we cannot walk over.
 * Return the number of 4-directional walks from the starting square to the ending square, that walk over EVERY non-obstacle square exactly once.
 *
 * Note:
 *
 * 1 <= grid.length * grid[0].length <= 20
 *
 */
public class UniquePathsIII {

	private static final int START = 1;
	private static final int FINISH = 2;
	private static final int OBST = -1;
	private static final int EMPTY = -0;

	private static int uniquePathsIII(int[][] grid) {
		int rows=grid.length;
		int cols=grid[0].length;
		int startR=-1;
		int startC=-1;
		for (int i=0;i<rows;i++) {
			for (int j=0;j<cols;j++) {
				if (grid[i][j]==START) {
					startR=i;
					startC=j;
					break;
				}
			}
		}
		if (startR<0)
			return 0;
		return count(grid, startR, startC);
    }

	private static int count(int[][] grid, int r, int c) {
		int rows=grid.length;
		int cols=grid[0].length;
//		if (grid[r][c]==FINISH)
//			return 1;
//		if (grid[r][c]==OBST)
//			return 0;
		if (grid[r][c]==EMPTY)
			grid[r][c]=OBST;

		int up=0;
		if (r-1>=0) {
			if (grid[r-1][c]==EMPTY)
				up=count(grid,r-1,c);
			else if (grid[r-1][c]==FINISH && isEverySquareUsed(grid))
				up=1;
		}

		int down=0;
		if (r+1<rows) {
			if (grid[r+1][c]==EMPTY)
				down=count(grid,r+1,c);
			else if (grid[r+1][c]==FINISH && isEverySquareUsed(grid))
				down=1;
		}

		int left=0;
		if (c-1>=0) {
			if (grid[r][c-1]==EMPTY)
				left=count(grid,r,c-1);
			else if (grid[r][c-1]==FINISH && isEverySquareUsed(grid))
				left=1;
		}

		int right=0;
		if (c+1<cols) {
			if (grid[r][c+1]==EMPTY)
				right=count(grid,r,c+1);
			else if (grid[r][c+1]==FINISH && isEverySquareUsed(grid))
				right=1;
		}

		if (grid[r][c]==OBST)
			grid[r][c]=EMPTY;

		return up+down+left+right;
	}

	private static boolean isEverySquareUsed(int[][] grid) {
		int rows=grid.length;
		int cols=grid[0].length;
		for (int i=0;i<rows;i++) {
			for (int j=0;j<cols;j++) {
				if (grid[i][j]==EMPTY)
					return false;
			}
		}
		return true;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] grid = {{1,0,0,0},{0,0,0,0},{0,0,0,2}};
		System.out.println(uniquePathsIII(grid));
	}

}
