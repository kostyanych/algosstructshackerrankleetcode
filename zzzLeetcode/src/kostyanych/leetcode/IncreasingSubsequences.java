package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Given an integer array, your task is to find all the different possible increasing subsequences of the given array,
 * and the length of an increasing subsequence should be at least 2.
 *
 * Example:
 * Input: [4, 6, 7, 7]
 * Output: [[4, 6], [4, 7], [4, 6, 7], [4, 6, 7, 7], [6, 7], [6, 7, 7], [7,7], [4,7,7]]
 *
 * Note:
 * The length of the given array will not exceed 15.
 * The range of integer in the given array is [-100,100].
 * The given array may contain duplicates, and two equal integers should also be considered as a special case of increasing sequence.
 *
 */
public class IncreasingSubsequences {

	 private static List<List<Integer>> findSubsequences(int[] nums) {
		 Map<Integer,List<List<Integer>>> map = new HashMap<>();
		 int len=nums.length;
		 for (int i=1;i<len;i++) {
			 for (int j=i-1;j>=0;j--) {
				 if (nums[i]>=nums[j]) {
					 List<List<Integer>> prevLists = map.get(j);
					 List<List<Integer>> cur = map.computeIfAbsent(i, key -> new ArrayList<List<Integer>>());
					 List<Integer> ccur = new ArrayList<>();
					 ccur.add(nums[j]);
					 ccur.add(nums[i]);
					 cur.add(ccur);
					 if (prevLists!=null) {
						 for (List<Integer> prevL : prevLists) {
							 ccur = new ArrayList<>(prevL);
							 ccur.add(nums[i]);
							 cur.add(ccur);
						 }
					 }
				 }
				 if (nums[i]==nums[j] && (j-1>0 && nums[i]!=nums[j-1]))
					 break;
			 }
		 }

		 Set<List<Integer>> res = new HashSet<>();
		 for (List<List<Integer>> l : map.values()) {
			 res.addAll(l);
		 }
		 return new ArrayList<>(res);
	 }

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 int[] arr = {4, 6, 7, 7, 7};
		 System.out.println(findSubsequences(arr));
	 }

}
