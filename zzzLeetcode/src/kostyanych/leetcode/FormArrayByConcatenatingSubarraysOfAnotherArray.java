package kostyanych.leetcode;

/**
 * You are given a 2D integer array groups of length n. You are also given an integer array nums.
 *
 * You are asked if you can choose n disjoint subarrays from the array nums such that the ith subarray is equal to groups[i] (0-indexed),
 * and if i > 0, the (i-1)th subarray appears before the ith subarray in nums (i.e. the subarrays must be in the same order as groups).
 *
 * Return true if you can do this task, and false otherwise.
 *
 * Note that the subarrays are disjoint if and only if there is no index k such that nums[k] belongs to more than one subarray.
 * A subarray is a contiguous sequence of elements within an array.
 *
 * Constraints:
 * groups.length == n
 * 1 <= n <= 103
 * 1 <= groups[i].length, sum(groups[i].length) <= 10^3
 * 1 <= nums.length <= 10^3
 * -10^7 <= groups[i][j], nums[k] <= 10^7
 *
 */
public class FormArrayByConcatenatingSubarraysOfAnotherArray {

	static boolean canChoose(int[][] groups, int[] nums) {
        return canDo(groups, nums, 0, 0, groups.length);
    }

	static private boolean canDo(int[][] groups, int[] nums, int grpInd, int start, int left) {
		if (left==0)
			return true;

		int ind = findInd(nums, groups[grpInd], start);
		if (ind<0)
			return false;
		if (left==1)
			return true;

		start=ind+groups[grpInd].length;
		return canDo(groups, nums, grpInd+1, start, left-1);
	}

	static private int findInd(int[] nums, int[] grp, int startWith) {
		int len=nums.length;
		int grpLen=grp.length;
		if (len-startWith<grpLen)
			return -1;
		for (int i=startWith;i<=len-grpLen;i++) {
			if (nums[i]!=grp[0])
				continue;
			boolean notFound=false;
			for (int j=1;j<grpLen;j++) {
				if (grp[j]!=nums[i+j]) {
					notFound=true;
					break;
				}
			}
			if (!notFound)
				return i;
		}
		return -1;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[][] grps = {{1,-1,-1},{3,-2,0}};
		//int[] nums = {1,-1,0,1,-1,-1,3,-2,0};
		int[][] grps = {{1,2,3},{3,4}};
		int[] nums = {7,7,1,2,3,4,7,};
		//int[] nums = {7,7,1,2,3,4,7,7};
		System.out.println(canChoose(grps, nums));

	}

}
