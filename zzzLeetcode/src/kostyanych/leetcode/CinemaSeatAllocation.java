package kostyanych.leetcode;

import java.util.Arrays;
import java.util.Comparator;

/**
 * A cinema has n rows of seats, numbered from 1 to n and there are ten seats in each row, labelled from 1 to 10.
 * Aisles are between 3rd and 4th seats and between 7th and 8th seats;
 * row 1: 1 2 3   4 5 6 7   8 9 10
 * row 2: 1 2 3   4 5 6 7   8 9 10
 * row 3: 1 2 3   4 5 6 7   8 9 10
 * etc.
 *
 * Given the array reservedSeats containing the numbers of seats already reserved,
 * for example, reservedSeats[i]=[3,8] means the seat located in row 3 and labelled with 8 is already reserved.
 *
 *  Return the maximum number of four-person families you can allocate on the cinema seats.
 *  A four-person family occupies fours seats in one row, that are next to each other.
 *  Seats across an aisle (such as [3,3] and [3,4]) are not considered to be next to each other,
 *  however, It is permissible for the four-person family to be separated by an aisle, but in that case,
 *  exactly two people have to sit on each side of the aisle.
 *
 */
public class CinemaSeatAllocation {

	private static Comparator<int[]> cmp = new Comparator<int[]>() {
		public int compare(int[] o1, int[] o2) {
			if (o1[0]==o2[0])
				return o1[1]-o2[1];
			return o1[0]-o2[0];
		};
	};

	private static int maxNumberOfFamilies(int n, int[][] rs) {
		int res=0;
		int curRow=1;
		Arrays.sort(rs,cmp);
		int len=rs.length;
		for (int i=0;i<len;i++) {
			if (rs[i][0]>curRow) {
				res=res+(rs[i][0]-curRow)*2;
				curRow=rs[i][0];
			}

			boolean l=true;
			boolean c=true;
			boolean r=true;

			while (true) {
				int pl=rs[i][1];
				switch (pl) {
					case 2:
					case 3:
						l=false;
						break;
					case 4:
					case 5:
						l=false;
						c=false;
						break;
					case 6:
					case 7:
						r=false;
						c=false;
						break;
					case 8:
					case 9:
						r=false;
						break;
				}

				if (i<len-1 && rs[i+1][0]==curRow)
					i++;
				else {
					int curcount=0;
					if (l)
						curcount++;
					if (r)
						curcount++;
					if (!l && !r && c)
						curcount=1;
					res+=curcount;
					curRow++;
					break;
				}
			}
		}
		if (curRow<=n)
			res=res+(n-curRow+1)*2;
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int n=2;
		int[][] reservedSeats = {{2,3}};
		System.out.println(maxNumberOfFamilies(n, reservedSeats));

//		int n = 2;
//		int[][] reservedSeats = {{2,1},{1,8},{2,6}};
//		System.out.println(maxNumberOfFamilies(n, reservedSeats));
//
//		 n = 4;
//		 reservedSeats = new int[][] {{4,3},{1,4},{4,6},{1,7}};
//		 System.out.println(maxNumberOfFamilies(n, reservedSeats));
	}

}
