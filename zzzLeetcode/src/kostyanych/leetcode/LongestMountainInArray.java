package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Let's call any (contiguous) subarray B (of A) a mountain if the following properties hold:
 *
 * B.length >= 3
 *   There exists some 0 < i < B.length - 1 such that B[0] < B[1] < ... B[i-1] < B[i] > B[i+1] > ... > B[B.length - 1]
 *   (Note that B could be any subarray of A, including the entire array A.)
 * Given an array A of integers, return the length of the longest mountain.
 * Return 0 if there is no mountain.
 *
 * Note:
 *   0 <= A.length <= 10000
 *   0 <= A[i] <= 10000
 *
 * Follow up:
 *   Can you solve it using only one pass?
 *   Can you solve it in O(1) space?
 *
 */
public class LongestMountainInArray {

	private static enum Slope{NONE, LEFT, RIGHT};

	static private int longestMountain(int[] A) {
        int len=A.length;
        if (len<3)
            return 0;
        int left=0;
        int right=0;
        int prev=A[0];
        int res=0;
        Slope slope = Slope.NONE;
        for (int i=1;i<len;i++) {
            if (A[i]>prev) {
                if (left>0 && right >0)
                    res=Math.max(res,left+right+1);
                if (slope!=Slope.LEFT) {
                	slope=Slope.LEFT;
                	left=1;
                	right=0;
                }
                else
                	left++;
            }
            else if (A[i]<prev) {
                if (slope!=Slope.RIGHT) {
                	slope=Slope.RIGHT;
                	right=1;
                }
                else
                	right++;
            }
            else {// A[i]==prev
                if (left>0 && right >0)
                    res=Math.max(res,left+right+1);
                left=0;
                right=0;
                slope=Slope.NONE;
            }
            prev=A[i];
        }
        if (left>0 && right >0)
            res=Math.max(res,left+right+1);
        return res;
    }

	public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 int[] arr = {2,2,2};
		 System.out.println(Arrays.toString(arr));
		 System.out.println(longestMountain(arr));
	}
}
