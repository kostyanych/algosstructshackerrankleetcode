package kostyanych.leetcode.helpers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TreeNode {
	public int val;
	public TreeNode left;
	public TreeNode right;
	public TreeNode(int x) { val = x; }
	public String toString() {return ""+val;};

	public static TreeNode fromNormalArray(Integer[] arr) {
		if (arr==null || arr.length<1 || arr[0]==null)
			return null;
		TreeNode root = new TreeNode(arr[0]);
		insertNodeFromArray(arr, root, 0);
		return root;
	}

	private static void insertNodeFromArray(Integer[] arr, TreeNode root, int arrIndex) {
		int lastIndex=arr.length-1;
		int newInd = arrIndex*2+1;
		if (newInd>lastIndex)
			return;
		Integer arrVal=arr[newInd];
		if (arrVal==null)
			return;
		root.left = new TreeNode(arrVal);
		insertNodeFromArray(arr, root.left, newInd);
		newInd = arrIndex*2+2;
		if (newInd>lastIndex)
			return;
		arrVal=arr[newInd];
		if (arrVal==null)
			return;
		root.right = new TreeNode(arrVal);
		insertNodeFromArray(arr, root.right, newInd);
	}

	private final static String NULL="null";

	public static String toLeetcodeString(TreeNode root) {
		if (root==null)
			return "[]";
		List<String> lst=new ArrayList<>();
		Queue<TreeNode> q = new LinkedList<>();
		q.add(root);
		while (!q.isEmpty()) {
			TreeNode tn=q.remove();
			if (tn==null)
				lst.add(NULL);
			else {
				lst.add(Integer.toString(tn.val));
				q.add(tn.left);
				q.add(tn.right);
			}
		}
		//remove trailing nulls
		for (int i=lst.size()-1;i>=0;i--) {
			if (!NULL.equals(lst.get(i)))
				break;
			lst.remove(i);
		}
		StringBuilder sb=new StringBuilder();
		int len=lst.size();
		if (len>0)
			sb.append(lst.get(0));
		for (int i=1;i<len;i++)
			sb.append(",").append(lst.get(i));
		return "["+sb.toString()+"]";
	}

	// Decodes your encoded data to tree.
	public static TreeNode fromLeetcodeString(String data) {
		if (data==null)
			return null;
		data=data.trim();
		int len=data.length();
		if (len<2)
			return null;
		if (data.charAt(0)!='[' && data.charAt(len-1)!=']')
			return null;
		data=data.substring(1,len-1);
		String[] snodes=data.split(",");
		len = snodes.length;

		TreeNode root = null;
		try {
			root = new TreeNode(getIntFromStr(snodes[0]));
			Queue<TreeNode> q = new LinkedList<>();
			q.add(root);
			for (int i=1;i<len;i++) {
				String sleft=snodes[i];
				i++;
				String sright=null;
				if (i<len)
					sright=snodes[i];
				if (sleft!=null && NULL.equals(sleft))
					sleft=null;
				if (sright!=null && NULL.equals(sright))
					sright=null;
				TreeNode tn=q.remove();
				if (sleft!=null) {
					TreeNode left=new TreeNode(getIntFromStr(sleft));
					tn.left=left;
					q.add(left);
				}
				if (sright!=null) {
					TreeNode right=new TreeNode(getIntFromStr(sright));
					tn.right=right;
					q.add(right);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return root;
	}

	private static int getIntFromStr(String s) throws NumberFormatException {
		s=s.trim();
		return Integer.parseInt(s);
	}
}
