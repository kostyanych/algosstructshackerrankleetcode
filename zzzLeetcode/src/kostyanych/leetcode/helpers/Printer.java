package kostyanych.leetcode.helpers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Printer {

	public static void print2DArray(int[][] arr, boolean oneLine) {
		if (arr==null)
			return;
		System.out.print("[");
		boolean first=true;
		for (int[] ia : arr) {
			if (first)
				first=false;
			else
				System.out.print(", ");
			if (!oneLine)
				System.out.print("\r\n");
			System.out.print(Arrays.toString(ia));
		}
		System.out.print("]");
		System.out.println("");
	}

	public static void print2DArrayStream(int[][] arr, boolean oneLine) {
		if (arr==null)
			return;
		Stream<int[]> stream = Arrays.stream(arr);
		String linesDelim=",\r\n";
		if (oneLine)
			linesDelim=", ";

		String result = stream
							.map((int[] el) -> IntStream.of(el)
													.mapToObj(i -> Integer.toString(i))
													.collect(Collectors.joining(", ", "[", "]")))
							.collect(Collectors.joining(linesDelim, "[", "]"));
		System.out.println(result);
	}


	public static void print2DArray(char[][] arr, boolean oneLine) {
		if (arr==null)
			return;
		System.out.print("[");
		boolean first=true;
		for (char[] ia : arr) {
			if (first)
				first=false;
			else
				System.out.print(", ");
			if (!oneLine)
				System.out.print("\r\n");
			System.out.print(Arrays.toString(ia));
		}
		System.out.print("]");
		System.out.println("");
	}

	public static <T> void print2DList(List<List<T>> lists, boolean oneLine) {
		if (lists==null)
			return;
		System.out.print("[");
		boolean first=true;
		for (List<T> lst : lists) {
			if (first)
				first=false;
			else
				System.out.print(", ");
			if (!oneLine)
				System.out.print("\r\n");
			System.out.print(Arrays.toString(lst.toArray()));
		}
		System.out.print("]");
		System.out.println("");
	}

}
