package kostyanych.leetcode.helpers;

public class ListNode {
	public int val;
	public ListNode next;
	public ListNode(int x) { val = x; }
	public ListNode(int x, ListNode nxt) { val = x; next=nxt;}

	public void print() {
		System.out.print(""+val);
		ListNode ln=this.next;
		while (ln!=null) {
			System.out.print("->"+ln.val);
			ln=ln.next;
		}
		System.out.println();
	}

	public String toString() {
		return ""+val;
	}

	public static ListNode fromArray(int[] arr) {
		ListNode res=new ListNode(arr[0]);
		ListNode cur=res;
		for (int i=1;i<arr.length;i++) {
			ListNode next=new ListNode(arr[i]);
			cur.next=next;
			cur=next;
		}
		return res;
	}
}
