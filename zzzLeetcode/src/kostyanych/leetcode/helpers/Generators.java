package kostyanych.leetcode.helpers;

import java.util.ArrayList;
import java.util.List;

public class Generators {

	public static List<Integer> arrToList(int[] arr) {
		int len=arr.length;
		List<Integer> res = new ArrayList<>(len);
		for (int i : arr)
			res.add(i);
		return res;
	}

	public static List<List<Integer>> arrToList(int[][] arr) {
		int len=arr.length;
		List<List<Integer>> res = new ArrayList<>(len);
		for (int[] i : arr)
			res.add(arrToList(i));
		return res;
	}

}
