package kostyanych.leetcode;

/**
 * Given two integers dividend and divisor, divide two integers without using multiplication, division and mod operator.
 *
 * Return the quotient after dividing dividend by divisor.
 *
 * The integer division should truncate toward zero, which means losing its fractional part.
 * For example, truncate(8.345) = 8 and truncate(-2.7335) = -2.
 *
 * Note:
 *
 * - Both dividend and divisor will be 32-bit signed integers.
 * - The divisor will never be 0.
 * - Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−2^31,  23^1 − 1].
 *   For the purpose of this problem, assume that your function returns 2^31 − 1 when the division result overflows.
 */
public class DivideTwoIntegers {

	/**
	 * The key observation is that the quotient of a division is just the number of times that we can subtract the divisor from the dividend
	 *    without making it negative.
	 *
	 * Suppose dividend = 15 and divisor = 3, 15 - 3 > 0.
	 * We now try to subtract more by shifting 3 to the left by 1 bit (6).
	 * Since 15 - 6 > 0, shift 6 again to 12.
	 * Now 15 - 12 > 0, shift 12 again to 24, which is larger than 15.
	 * So we can at most subtract 12 from 15.
	 * Since 12 is obtained by shifting 3 to left twice, it is 1 << 2 = 4 times of 3.
	 * We add 4 to an answer variable (initialized to be 0).
	 * The above process is like 15 = 3 * 4 + 3. We now get part of the quotient (4), with a remaining dividend 3.
	 * Then we repeat the above process by subtracting divisor = 3 from the remaining dividend = 3 and obtain 0. We are done.
	 * In this case, no shift happens. We simply add 1 << 0 = 1 to the answer variable.
	 *
	 * This is the full algorithm to perform division using bit manipulations.
	 * The sign also needs to be taken into consideration. And we still need to handle one overflow case: dividend = INT_MIN and divisor = -1.
	 */
	private static int divide(int dividend, int divisor) {
		if (dividend==0)
			return 0;
		if (dividend == Integer.MIN_VALUE && divisor == -1)
	        return Integer.MAX_VALUE;
	    if (divisor==1)
	    	return dividend;
	    if (divisor==dividend)
	    	return 1;
	    if (divisor==Integer.MIN_VALUE)
	    	return 0;

	    int res=0;
	    int quot;

	    int sign=1;
	    if ( (dividend<0 && divisor>0) || (dividend>0 && divisor<0)) {
	    	sign=-1;
	    	divisor=-divisor;
	    }

	    while ( (quot=divByShift(dividend, divisor)) > 0) {
	    	res+=quot;
	    	dividend=dividend-(quot*divisor);
	    }

	    return res*sign;
	}

	private static int divByShift(int dividend, int divisor) {
		if (dividend==0)
			return 0;
		if (dividend==divisor)
			return 1;
		if (dividend>0 && dividend-divisor<0)
			return 0;
		if (dividend<0 && dividend-divisor>0)
			return 0;
		int res=1;
		while (dividend!=divisor) {
			divisor<<=1;
			int delta=dividend-divisor;
			if ( (dividend>0 && delta<0)
				|| (dividend<0 && delta>0) )
				break;
			res*=2;
		}
		return res;
	}

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 int dividend = 7;
		 int divisor = -3;
		 System.out.println(""+dividend+" / "+divisor+" = "+divide(dividend, divisor));

		 dividend=-2147483648;
		 divisor=2;
		 System.out.println(""+dividend+" / "+divisor+" = "+divide(dividend, divisor));

		 dividend=-33;
		 divisor=-2;
		 System.out.println(""+dividend+" / "+divisor+" = "+divide(dividend, divisor));

	 }

}
