package kostyanych.leetcode;

/**
 * Given an array of positive integers target and an array initial of same size with all zeros.
 *
 * Return the minimum number of operations to form a target array from initial if you are allowed to do the following operation:
 * Choose any subarray from initial and increment each value by one.
 *
 * The answer is guaranteed to fit within the range of a 32-bit signed integer.
 *
 */
public class MinimumNumberOfIncrementsOnSubarraysToFormTargetArray {

	/**
	 * Start with first position a[0]. We know that it will take a[0] operations to reach a[0] from 0.
	 *
	 * Now, 2 things can happen from here:
	 * 1. At next index we encounter a number less than a[0] (a[1] < a[0]):
	 *     In this case we can simply reuse the same operations that we did for a[0].
	 *        i.e If array was (3, 2), we can first perform 3 operations and then use 2 of the same operations at next index.
	 *     However, going forward, we will only have a[1] operations available for reuse.
	 *
	 * 2. We encounter a number greater than a[0] (a[1] > a[0]):
	 *   In this case we can simply reuse the same operations that we did for a[0].
	 *   And additionally, we will perform a[1] - a[0] more operation to reach a[1].
	 *    Again, going forward, we will have a[1] operations available for reuse.
	 */
	public static int minNumberOperations(int[] target) {
		int res = target[0];

		for (int i=1; i<target.length;i++) {
			if (target[i]>target[i-1])
				res+=target[i]-target[i-1];
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] target = {3,1,5,4,2};
		System.out.println(minNumberOperations(target));

	}

}
