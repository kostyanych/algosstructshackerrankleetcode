package kostyanych.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Zadacha3 {

	static private long[] sums;
	static private Map<Long,Integer> map;

	private static int minSubarray(int[] nums, int p) {
		int len = nums.length;
		map = new HashMap<>();
		sums = new long[len];
		sums[0] = nums[0];
		map.put(sums[0], 0);
		for (int i=1;i<len;i++) {
			sums[i]=sums[i-1]+nums[i];
			map.put(sums[i], i);
		}
System.out.println(Arrays.toString(sums));
		int rem=(int)(sums[len-1]%p);
        if (rem==0)
        	return 0;

        int res=-1;


        long wholeSum=sums[len-1];
        int r=rem;
        while (r<wholeSum) {
        	if (r>0)
        		break;
        	int curres=-1;
        	for (int i=0;i<len;i++) {
        		long curNum=sums[i];
    			if (curNum<r)
    				continue;
    			if (curNum==r)
    				curres=i+1;
    			else {
    				Integer possible=map.get(curNum-r);
    				if (possible!=null)
    					curres=i-possible;
    			}
    			if (curres>=0)
    				res=Math.min(curres, res);
        	}
        	if (res>0)
        		break;
        	r+=p;
        }
        return res;
    }

	static private int check(int ind, int p, int rem) {
		int res = -1;
		long wholeSum=sums[sums.length-1];
		long num=rem;
		long curNum=sums[ind];
		while (num<wholeSum) {
			if (curNum<num)
				break;
			if (curNum==num) {
				res=ind+1;
				break;
			}
			Integer possible=map.get(curNum-num);
			if (possible!=null) {
				res=ind-possible;
				break;
			}
			num+=p;
		}
		return res;
	}

/* previous
	private static int minInsertions(String s) {
		int len=s.length();
		if (len==1)
			return 1;
		int pairs=0;
		boolean issingle=false;
		int singles=0;
		int res=0;
		if (s.charAt(len-1)==')')
			issingle=true;
		else
			res+=2;
		for (int i=len-2;i>=0;i--) {
			if (s.charAt(i)==')') {
				if (s.charAt(i+1)==')') {
					if (issingle) {//1. completes a pair
						pairs++;
						issingle=false;
					}
					else //2. becomes a single
						issingle=true;
				}
				else { //3. becomes a single
					issingle=true;
				}
			}
			else {
				if (issingle)
					singles++;
				issingle=false;
				if (pairs>0)
					pairs--;
				else {
					if (singles>0) {
						singles--;
						res+=1;
					}
					else
						res+=2;
				}
			}
		}
		if (issingle)
			singles++;
		res+=2*singles;
		res+=pairs;
		return res;
    }
*/
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {5,4,8,5,5};
		int p=7;
		System.out.println(minSubarray(arr,p));



//		String s=")))))))";
//		System.out.println(s);
//		System.out.println(minInsertions(s));
	}

}
