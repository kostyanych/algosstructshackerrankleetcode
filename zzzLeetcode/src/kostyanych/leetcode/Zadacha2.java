package kostyanych.leetcode;

import java.util.Arrays;

public class Zadacha2 {

	private static final int MOD = 1_000_000_007;

	private static int maxSumRangeQuery(int[] nums, int[][] requests) {
		int len = nums.length;
		int[] reqs = new int[len];
		int[][] dreqs = new int[len][2];
		for (int[] req : requests) {
			dreqs[req[0]][0]++;
			dreqs[req[1]][1]++;
		}
		int am=0;
		for (int i=len-1;i>=0;i--) {
			am+=dreqs[i][1];
			reqs[i]=am;
			am-=dreqs[i][0];
		}

System.out.println(Arrays.toString(reqs));
		Arrays.sort(reqs);
		Arrays.sort(nums);

		int res=0;

		for (int i = len-1;i>=0;i--) {
			if (reqs[i]==0)
				break;
			//(reqs[i]*nums[i])%MOD
			int add = ((reqs[i]%MOD)*(nums[i]%MOD))%MOD;
			res=(res+add)%MOD;
		}
		return res;
	}

/*previous
	private static boolean canConvertString(String s, String t, int k) {
		int len=s.length();
		if (len!=t.length())
			return false;
		//int [] diff = new int[len];
		Set<Integer> set = new HashSet<>();
		int max=0;
		for (int i=0;i<len;i++) {
			int diff=calc(t.charAt(i), s.charAt(i));
			if (diff==0)
				continue;
			while (set.contains(diff)) {
				diff+=26;
			}
			set.add(diff);
			max=Math.max(diff, max);
		}
		if (max>k)
			return false;
		return true;
    }

	private static int calc(char c1, char c2) {
		int res=c1-c2;
		if (res>=0)
			return res;
		return 26+res;
	}

*/
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {1,2,3,4,5};
		int[][] requests = {{1,3}, {0,1}};
		System.out.println(maxSumRangeQuery(arr,requests));


//		String s = "aab";
//		String t = "bbb";
//		int k = 27;
//		System.out.println(canConvertString(s,t,k));
	}


}
