package kostyanych.leetcode;

/**
 * Given a parentheses string s containing only the characters '(' and ')'.
 *
 * A parentheses string is balanced if:
 * Any left parenthesis '(' has a corresponding two consecutive right parenthesis '))'.
 * Left parenthesis '(' must go before the corresponding two consecutive right parenthesis '))'.
 *
 * For example, "())", "())(())))" and "(())())))" are balanced, ")()", "()))" and "(()))" are not balanced.
 *
 * You can insert the characters '(' and ')' at any position of the string to balance it if needed.
 *
 * Return the minimum number of insertions needed to make s balanced.
 *
 */
public class MinimumInsertionsToBalanceAParenthesesString {

	private static int minInsertions(String s) {
		int len=s.length();
		if (len==1)
			return 1;
		int pairs=0;
		boolean issingle=false;
		int singles=0;
		int res=0;
		if (s.charAt(len-1)==')')
			issingle=true;
		else
			res+=2;
		for (int i=len-2;i>=0;i--) {
			if (s.charAt(i)==')') {
				if (s.charAt(i+1)==')') {
					if (issingle) {//1. completes a pair
						pairs++;
						issingle=false;
					}
					else //2. becomes a single
						issingle=true;
				}
				else { //3. becomes a single
					issingle=true;
				}
			}
			else {
				if (issingle)
					singles++;
				issingle=false;
				if (pairs>0)
					pairs--;
				else {
					if (singles>0) {
						singles--;
						res+=1;
					}
					else
						res+=2;
				}
			}
		}
		if (issingle)
			singles++;
		res+=2*singles;
		res+=pairs;
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s=")))))))";
		System.out.println(s);
		System.out.println(minInsertions(s));
	}

}
