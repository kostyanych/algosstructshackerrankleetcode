package kostyanych.leetcode;

import java.util.Arrays;

/**
 * There is an integer array perm that is a permutation of the first n positive integers, where n is always odd.
 *
 * It was encoded into another integer array encoded of length n - 1, such that encoded[i] = perm[i] XOR perm[i + 1].
 * For example, if perm = [1,3,2], then encoded = [2,1].
 *
 * Given the encoded array, return the original array perm. It is guaranteed that the answer exists and is unique.
 *
 */
public class DecodeXORedPermutation {

	static private int[] decode(int[] encoded) {

		int len = encoded.length;
		int[] src = new int[len+1];

		/*initialiy we have
		encoded[0] = src[0]^src[1]
		encoded[1] = src[1]^src[2]
		encoded[2] = src[2]^src[3]
		...
		..
		*/

		for (int i=1;i<len;i++) {
			encoded[i]=encoded[i-1]^encoded[i];
		}
		/*now we have
		encoded[0] = src[0]^src[1]
		encoded[1] = src[0]^src[2]
		encoded[2] = src[0]^src[3]
		...
		..
		*/

		//len is even! so if we xor all encoded[i] now, src0 will be cancelled out
		int withoutSrc0=0;
		for (int i=0;i<len;i++) {
			withoutSrc0^=encoded[i];
		}

		//now let's xor all numbers in the src array.
		//we know that they are all the numbers from 1 to len+1;
		int srcXor=0;
		for (int i=1;i<=len+1;i++) {
			srcXor^=i;
		}

		//now obviously if we xor srcXor and withoutSrc0, we'll get the src[0] !
		src[0]=srcXor^withoutSrc0;

		//and now it's super simple: we know src[i-1],
		//if we'd save the initial enc array then it would be: from enc[i]=src[i-1]^src[i] follows src[i]=enc[i-1]^src[i-1]
		//but we screwd it up a little, so we now have enc[i]=src[0]^src[i+1] => src[i]=enc[i-1]^src[0]
		for (int i=1;i<=len;i++) {
			src[i]=encoded[i-1]^src[0];
		}

		return src;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] enc = {6,5,4,6};
		System.out.println(Arrays.toString(enc));
		System.out.println(Arrays.toString(decode(enc)));
	}


}
