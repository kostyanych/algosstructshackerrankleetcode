package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given an array of digits, you can write numbers using each digits[i] as many times as we want.
 * For example, if digits = ['1','3','5'], we may write numbers such as '13', '551', and '1351315'.
 *
 * Return the number of positive integers that can be generated that are less than or equal to a given integer n.
 *
 * Constraints:
 * 	1 <= digits.length <= 9
 * 	digits[i].length == 1
 * 	digits[i] is a digit from '1' to '9'.
 *	All the values in digits are unique.
 *	1 <= n <= 109
 *
 * Solution:
 * 1. we can only use digits from digits[]
 * 2. Let n has K digits. Every number with less digits will be less then n.
 * 3. Let us build a number of k digits (k<K). Then there are (digits.length)^k numbers we can build from digits[].
 * 4. every number with k>K will obviously be greater than n, thus invalid.
 * 5. we are left with numbers of exactly K digits.
 * 6. If the first digit is less than the first digit of n, then for other K-1 digits we can use whatever combination. See 3.
 * 7. If the first digit is greater than the first digit of n, then we're out of luck, nothing to do here.
 * 8. If the first digit is equal to the the first digit of n, then we need to consider next digit of n. See points 6-8.
 *
 * Let memo[i] be the number of ways to write a valid number if N became N[i], N[i+1], ....
 * For example, if N = 2345, then
 *    memo[0] would be the number of valid numbers at most 2345,
 *    memo[1] would be the ones at most 345,
 *    memo[2] would be the ones at most 45,
 *    memo[3] would be the ones at most 5.
 *
 * Then memo[i] = (number of d in digits with d < n[i]) * ((digits.length)^(K-i-1)) + memo[i+1] if n[i] is in digits.
 * n[i] - iht digit of the number n
 *
 */
public class NumbersAtMostNGivenDigitSet {

	static private int atMostNGivenDigitSet(String[] digits, int n) {
		int nlen=numLen(n);
		char[] nchars=String.valueOf(n).toCharArray();
		int[] memo=new int[nlen+1];
		memo[nlen]=1;
		int dcount=digits.length;

		for (int i = nlen-1; i >= 0; i--) {
            // compute dp[i]
            int ndigit = nchars[i] - '0';
            for (String sd: digits) {
            	int digit=Integer.valueOf(sd);
                if (digit < ndigit)
                    memo[i] += Math.pow(dcount, nlen-i-1);
                else if (digit == ndigit)
                    memo[i] += memo[i+1];
            }
        }

        for (int i = 1; i < nlen; i++)
            memo[0] += Math.pow(dcount, i);
        return memo[0];
	}

	static private int numLen(int num) {
		int res=0;
		while (num>0) {
			res++;
			num/=10;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String[] arr = {"1","4","9"};
		int k = 2345;
		System.out.println(Arrays.toString(arr));
		System.out.println(atMostNGivenDigitSet(arr,k));
	}
}
