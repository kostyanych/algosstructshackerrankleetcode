package kostyanych.leetcode;

public class ContainerWithMostWater {

	private static int maxAreaSlow(int[] height) {
		int max=0;
		int len=height.length;
		for (int i=0;i<len;i++) {
			int hi=height[i];
			for (int j=0;j<i;j++) {
				if (height[j]<hi)
					continue;
				int buf=(i-j)*hi;
				if (max<buf)
					max=buf;
				break;
			}
			for (int j=len-1;j>i;j--) {
				if (height[j]<hi)
					continue;
				int buf=(j-i)*hi;
				if (max<buf)
					max=buf;
				break;
			}
		}
		return max;
    }
	
	private static int maxArea(int[] height) {
		int max=0;
		int len=height.length;
		int left=0;
		int right=len-1;
		while (left<right) {
			int h=Math.min(height[left],height[right]);
			int v=h*(right-left);
			if (max<v)
				max=v;
			if (height[left]<=height[right])
				left++;
			else
				right--;
		}
		return max;
    }
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(maxArea(new int[]{1,2}));
	}
}
