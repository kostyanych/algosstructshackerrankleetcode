package kostyanych.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a non-empty array of unique positive integers A, consider the following graph:
 *
 * There are A.length nodes, labelled A[0] to A[A.length - 1];
 * There is an edge between A[i] and A[j] if and only if A[i] and A[j] share a common factor greater than 1.
 * Return the size of the largest connected component in the graph.
 *
 * Note:
 * 1 <= A.length <= 20000
 * 1 <= A[i] <= 100000
 *
 */
public class LargestComponentSizeByCommonFactor {

	//we only need primes up to sqrt(max(A[i)), i.e. sqrt(100000) =~ 317
	static int[] primes = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,
							73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,
							179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,
							283,293,307,311,313,317};
/*
	static Map<Integer,Integer> parentMap;

	private static int largestComponentSize(int[] A) {
		parentMap = new HashMap<>();

		for (int num : A) {
			for (int prime : primes) {
				if (prime*prime>num)
					break;
                if (num % prime == 0){
                    union(num, prime);
                    union(num, num/prime);
                }
			}
		}

		int max = 1;
        Map<Integer, Integer> freqMap = new HashMap<>();
        for (Integer v : A) {
            int f = findParent(v);
            Integer freq=freqMap.get(f);
            if (freq==null)
            	freqMap.put(f,1);
            else {
            	int newfreq=freq+1;
            	freqMap.put(f, newfreq);
            	max=Math.max(max,newfreq);
            }
        }
        return max;
    }

	private static void union(int n, int m) {
		Integer findN = findParent(n);
		Integer findM = findParent(m);
        if (findN < findM)
        	parentMap.put(findM,findN);
        else
        	parentMap.put(findN,findM);
    }

	private static Integer findParent(Integer i) {
		Integer parent=parentMap.get(i);
        if (parent == null) {
        	parentMap.put(i, i);
        	return i;
        }
        while (!parent.equals(i)) {
        	i=parent;
        	parent=parentMap.get(i);
        }
        return i;
    }
*/

	private static int largestComponentSize(int[] A) {
		int maxVal=0;
		for (int i : A) {
			maxVal=Math.max(maxVal,  i);
		}

		//make disjoint set of ALL numbers up to maxVal
		UnionFind uf = new UnionFind(maxVal);

		//now union sets depending on common factors
		for (int num : A) {
			for (int fact=2;fact*fact<=num;fact++) {
				if(num%fact==0) {
					uf.union(num, fact);
					uf.union(num, num/fact);
				}
			}
        }

		int res = 1;
		Map<Integer, Integer> freqMap = new HashMap<>();

        for (int num: A) {
            int parent = uf.find(num);
            if(freqMap.containsKey(parent)) {
            	int freq=freqMap.get(parent)+1;
                freqMap.put(parent,freq);
                res = Math.max(res,freq);
            }
            else
            	freqMap.put(parent, 1);
        }

        return res;
	}

	static class UnionFind {
	    int parent[];
	    int rank[];

	    UnionFind(int maxCount) {
	        this.parent = new int[maxCount+1];
	        this.rank = new int[maxCount+1];

	        for(int i=0;i<=maxCount;i++) {
	            parent[i] = i;
	        }
	    }

	    public int find(int val) {
	        while(val!=parent[val]) {
	            parent[val]= parent[parent[val]]; //let's compress path along the way
	            val = parent[val];
	        }
	        return val;
	    }

	    public void union(int u, int v) {
	        int pau = find(u);
	        int pav = find(v);

	        if(pau==pav)
	            return;

	        if(rank[pau]<rank[pav])
	            parent[pau]= pav;
	        else if(rank[pau]>rank[pav])
	            parent[pav]= pau;
	        else {
	            parent[pau] = pav;
	            rank[pav]++;
	        }
	    }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[] arr= {2,3,1,1,4};
		int[] arr= {83,99,39,11,19,30,31};
		System.out.println(largestComponentSize(arr));
	}

}
