package kostyanych.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * You have a lock in front of you with 4 circular wheels. Each wheel has 10 slots: '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'.
 * The wheels can rotate freely and wrap around: for example we can turn '9' to be '0', or '0' to be '9'.
 * Each move consists of turning one wheel one slot.
 *
 * The lock initially starts at '0000', a string representing the state of the 4 wheels.
 *
 * You are given a list of deadends dead ends, meaning if the lock displays any of these codes,
 * 		the wheels of the lock will stop turning and you will be unable to open it.
 *
 * Given a target representing the value of the wheels that will unlock the lock, return the minimum total number of turns required to open the lock,
 * 		or -1 if it is impossible.
 *
 *  Constraints:
 *  	1 <= deadends.length <= 500
 *  	deadends[i].length == 4
 *  	target.length == 4
 *  	target will not be in the list deadends.
 *  	target and deadends[i] consist of digits only.
 *
 */
public class OpenTheLock {

	private static class Combo {
		final int[] cc;

		public Combo(String s) {
			char[] c = s.toCharArray();
			cc = new int[] { c[0] - '0', c[1] - '0', c[2] - '0', c[3] - '0' };
		}

		public Combo(int[] arr) {
			cc = new int[] { arr[0], arr[1], arr[2], arr[3] };
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Combo))
				return false;
			Combo other = (Combo) o;
			if (this.cc[0] != other.cc[0])
				return false;
			if (this.cc[1] != other.cc[1])
				return false;
			if (this.cc[2] != other.cc[2])
				return false;
			if (this.cc[3] != other.cc[3])
				return false;
			return true;
		}

		@Override
		public int hashCode() {
			return Arrays.hashCode(cc);
		}

		@Override
		public String toString() {
			return ""+(cc[0]*1000+cc[1]*100+cc[2]*10+cc[3]);
		}
	}

	static private int openLock(String[] deadends, String target) {
		Combo t = new Combo(target);
		Set<Combo> restricted = new HashSet<>();
		for (String d : deadends) {
			restricted.add(new Combo(d));
		}
		Set<Combo> visited = new HashSet<>();
		Queue<Combo> q = new LinkedList<>();
		Combo cmb=new Combo("0000");
        if (restricted.contains(cmb))
            return -1;
		q.add(cmb);
		visited.add(cmb);
		int res = -1;
		boolean found = false;
		while (!q.isEmpty() && !found) {
			res++;
			int s = q.size();
			for (int i = 0; i < s; i++) {
				cmb = q.remove();
				if (cmb.equals(t)) {
					found = true;
					break;
				}
				addNext(cmb, q, restricted, visited);
			}
		}
		if (found)
			return res;
		return -1;
	}

	static private void addNext(Combo cmb, Queue<Combo> q, Set<Combo> restricted, Set<Combo> visited) {
		int[] src = cmb.cc;
		for (int i = 0; i < 4; i++) {
			int buf = src[i];
			int n = src[i] + 1;
			if (n > 9)
				n = 0;
			src[i] = n;
			Combo next = new Combo(src);
			if (!restricted.contains(next) && !visited.contains(next)) {
				q.add(next);
				visited.add(next);
			}
			src[i] = buf;
		}
		for (int i = 0; i < 4; i++) {
			int buf = src[i];
			int n = src[i] - 1;
			if (n < 0)
				n = 9;
			src[i] = n;
			Combo next = new Combo(src);
			if (!restricted.contains(next) && !visited.contains(next)) {
				q.add(next);
				visited.add(next);
			}
			src[i] = buf;
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		String[] deadends = {"0201","0101","0102","1212","2002"};
//		String target = "0202";
		String[] deadends = {"0000"};
		String target = "8888";
		System.out.println(openLock(deadends, target));
	}
}
