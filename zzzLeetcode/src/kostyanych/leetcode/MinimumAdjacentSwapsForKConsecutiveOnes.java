package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * You are given an integer array, nums, and an integer k. nums comprises of only 0's and 1's.
 * In one move, you can choose two adjacent indices and swap their values.
 *
 * Return the minimum number of moves required so that nums has k consecutive 1's.
 *
 * Constraints:
 * 	1 <= nums.length <= 10^5
 * 	nums[i] is 0 or 1.
 * 	1 <= k <= sum(nums)
 *
 * SOLUTION
 *
 * 1.  obviously that only the 1s that are closer to each other would help minimize the moves, so we only need to consider the 1s in groups of k
 * 1a. this gives us a sliding window which includes k 1s and some amount of zeroes
 * 1b. we want to get rid of zeroes and make sliding window to have constant size
 * 2.  we'll make an array/list of indices which have 1s in them: 1,0,0,0,1,1,0,1,0,0,1 -> 0,4,5,7,10
 * 2a. now we have only 1s to deal with and our sliding window will be of size k
 * 3.  it's not difficult to show that we need to move 1s closer to the median 1.
 * 3a. For odd k it's the median 1 itself
 * 3b. for even k it's any point between and including 2 median 1s. So we'll just pick the left median 1 for that case
 * 4.  The algorithm is to calculate the cost of moving all 1s to the median position and then subtract the extra-cost we used, bearing in mind that 1s should be close to each other:
 * 	   	for k=5 and 101000100101,
 * 		we'll first make all 1s to go to the median 1: 000000500000 (that would take 18 moves)
 * 		then we'll retract 2 1s to the left and 2 1s to the right: 000011111000 (that would take 6 moves, that would mean we've ALREADY MADE 6 unneeded moves, so the overall cost would be 12)
 * 5. for faster calculating we'll use prefix sum array
 * 5a. 1,0,0,0,1,1,0,1,0,0,1 -> 0,4,5,7,10 -> [] ps = 0,0,4,9,16,26
 * 		let's move all 1s to index 5 (median). The cost will be (5-0)+(5-4)+(5-5)+(7-5)+(10-5) = (7+10) - (0+4) = > (ps(10)-ps(median)) - (ps(median-1)-ps(0-1))
 * 5b. for even k (k=4):
 * 		1. cost1 (0-7 => 4): (4-0) + (4-4) + (5-4)+(7-4) = (5+7) - (4) = (ps(7)-ps(median)) - (ps(median)-ps(0-1)) = (16-4)-(4-0) = 8
 * 		2. cost2 (4-10 => 5): (5-4) + (5-5) + (7-5)+(10-5) = (7+10) - (4+5) = (ps(10)-ps(median)) - (ps(median)-ps(4-1)) = (26-9) - (9-0) = 8
 * 6.  The extra cost (see 4) is calculated like this:
 * 6a. Odd k: 3210123 (k=7). Obviously the cost of moving 1s from median to their proper places is the arithmetic progression sum from 1 to (k-1)/2 on both sides.
 * 		That will give us (k-1)*(k+1)/4. Do the math if you will. I did it and it is just that.
 * 6b. Even k: 210123 (k=6). Obviously the cost is the sum of left and right parts.
 * 		Left part is the sum of arithmetic progression from 1 to k/2 - 1
 * 		Right part is the sum of arithmetic progression from 1 to k/2
 * 		The integral sum is k*k/4. Do the math if you will. I did it and it is just that.
 * 7. the extra cost is constant. We don't need to consider it while moving the window searching for min moves. Only apply it in the end, when min moves for getting all 1s to the median is found.
 *
 *
 */
public class MinimumAdjacentSwapsForKConsecutiveOnes {

	static private int minMoves(int[] nums, int k) {
		List<Long> goodIndices = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1)
            	goodIndices.add((long)i);
        }

		List<Long> prefixSums = new ArrayList<>();
		prefixSums.add(0L);
        for (int i = 0; i < goodIndices.size(); i++) {
        	prefixSums.add(prefixSums.get(i) + goodIndices.get(i));
        }

        boolean targetIsOdd=k%2!=0;
        long res = Long.MAX_VALUE;
        for (int i = 0; i <  goodIndices.size() - k + 1; i++) {
        	//cost to move all 1s to median position: sum of all costs to the right + sum of all costs to the left
        	//with prefix sums that would be (prefixSums[rightmost]-prefixSums[median]) - (prefixSums[median]-prefixSums[leftmost-1]) for even target and slightly more complicated for odd target
        	int median=i+k/2;
        	long curCost=0;
        	if (targetIsOdd)
        		curCost=prefixSums.get(i+k) - prefixSums.get(median+1) -( prefixSums.get(median) -prefixSums.get(i) );
        	else
        		curCost=prefixSums.get(i+k) - prefixSums.get(median) -( prefixSums.get(median) -prefixSums.get(i) );
        	res = Math.min(res, curCost);
        }
        //now account for the fact that we don't need to move all 1s to one place
        if (targetIsOdd)
        	res-=( (k-1l)*(k+1)/4 );
        else
        	res-=( 1l*k*k/4 );
        return (int)res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {1,0,0,0,0,0,1,1};
		int k = 3;
		System.out.println((minMoves(arr, k)));
	}

}
