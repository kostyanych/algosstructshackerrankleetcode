package kostyanych.leetcode;

import java.util.Stack;

/**
 * You are given a string s and two integers x and y. You can perform two types of operations any number of times.
 *
 * Remove substring "ab" and gain x points.
 * For example, when removing "ab" from "cabxbae" it becomes "cxbae".
 * Remove substring "ba" and gain y points.
 * For example, when removing "ba" from "cabxbae" it becomes "cabxe".
 * Return the maximum points you can gain after applying the above operations on s.
 *
 * Constraints:
 * 1 <= s.length <= 10^5
 * 1 <= x, y <= 10^4
 * s consists of lowercase English letters.
 *
 * Let's assume that y (cost of "ba") is gerater than x (xost of "ab").
 * Let's show that greedy approach is valid - take all bas first, then abs.
 * 1. bab/aba - obviously taking ba is preferable than taking ab
 *
 * ba**:
 * 2. baba - obvious (2y > x)
 * 3. babb - y > x
 * 4. baaa - y >0
 *
 * **ba
 * 5. abba - y+x
 * 6. aaba -> aba -> y>x
 * 7. bbba -> y>0
 *
 * *ba*:
 *
 * 8. abab -> 2x < y+x
 * 9. bbab -> y>x
 * 10.abaa ->y>x
 * 11.bbaa ->2y>0
 *
 */
public class MaximumScoreFromRemovingSubstrings {

	static private int maximumGain(String s, int x, int y) {
		if (s == null || s.length() <= 1)
			return 0;
		//substring with larger cost
		char lrg0='b';
		char lrg1='a';
		//substring with smaller cost
		char sml0='a';
		char sml1='b';
		int biggerCost=y;
		int smallerCost=x;
		if (x>=y) {
			lrg1='b';
			lrg0='a';
			sml1='a';
			sml0='b';
			biggerCost=x;
			smallerCost=y;
		}

        int res = 0;
        Stack<Character> stack = new Stack<>();
	    char[] c = s.toCharArray();

	    //get all bigger sequences
	    for (char ch : c) {
	    	if (stack.isEmpty() || stack.peek() != lrg0 || ch != lrg1)
	    		stack.push(ch);
	    	else {
	    		stack.pop();
	            res += biggerCost;
	    	}
	    }

	    StringBuilder sb = new StringBuilder();
	    while (!stack.isEmpty()) {
	    	sb.append(stack.pop());
	    }

	    String n = sb.reverse().toString();
	    //get all bigger sequences
	    c = n.toCharArray();
	    for (char ch : c) {
	    	if (stack.isEmpty() || stack.peek() != sml0 || ch != sml1)
	    		stack.push(ch);
	    	else {
	    		stack.pop();
	            res += smallerCost;
	    	}
	    }
	    return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//String s = "cdbcbbaaabab";
		String s = "bababaab";
		int x = 4;
		int y = 5;
		System.out.println(maximumGain(s,x,y));
	}

}
