package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * You are given equations in the format A / B = k, where A and B are variables represented as strings,
 * 		and k is a real number (floating-point number).
 * Given some queries, return the answers. If the answer does not exist, return -1.0.
 *
 * The input is always valid. You may assume that evaluating the queries will result in no division by zero and there is no contradiction.
 *
 * Input: equations = [["a","b"],["b","c"]], values = [2.0,3.0], queries = [["a","c"],["b","a"],["a","e"],["a","a"],["x","x"]]
 * Output: [6.00000,0.50000,-1.00000,1.00000,-1.00000]
 * Explanation:
 * Given: a / b = 2.0, b / c = 3.0
 * queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ?
 * return: [6.0, 0.5, -1.0, 1.0, -1.0 ]
 *
 *  Constraints:
 *  	1 <= equations.length <= 20
 *  	equations[i].length == 2
 *  	1 <= equations[i][0], equations[i][1] <= 5
 *  	values.length == equations.length
 *  	0.0 < values[i] <= 20.0
 *  	1 <= queries.length <= 20
 *  	queries[i].length == 2
 *  	1 <= queries[i][0], queries[i][1] <= 5
 *  	equations[i][0], equations[i][1], queries[i][0], queries[i][1] consist of lower case English letters and digits.
 */
public class EvaluateDivision {

	static final private Map<String,Map<String, Double>> map = new HashMap<>();

	static private final Double NORESULT = new Double(-1);

	static private double[] calcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {

		List<Double> res = new ArrayList<>();

		int len = equations.size();
		for (int i=0;i<len;i++) {
			List<String> eq = equations.get(i);
			Double d = values[i];
			insertIntoMap(eq.get(0),eq.get(0),1.);
			insertIntoMap(eq.get(1),eq.get(1),1.);
			insertIntoMap(eq.get(0),eq.get(1),d);
			insertIntoMap(eq.get(1),eq.get(0),1/d);
		}

		for (List<String> q : queries) {
			res.add(calcVal(q.get(0),q.get(1)));
		}

		double[] r = new double[res.size()];
		for (int i=0; i<res.size(); i++)
			r[i]=res.get(i);
		return r;
    }

	static private void insertIntoMap(String l, String r, Double k) {
		Map<String, Double> eq = map.get(l);
		if (eq==null) {
			eq = new HashMap<>();
			eq.put(r,k);
			map.put(l, eq);
		}
		else {
			if (!eq.containsKey(r))
				eq.put(r,k);
		}
	}

	static private Double calcVal(String l, String r) {
		Map<String, Double> eq = map.get(l);
		Double res = null;
		if (eq!=null) {
			res = eq.get(r);
			if (res!=null)
				return res;
		}
		else {
			eq = new HashMap<>();
			map.put(l,eq);
		}

		Set<String> set = new HashSet<>();
		set.add(l);
		dfs(eq, r, set);
		return eq.get(r);
	}

	static private void dfs(Map<String, Double> eq, String dest, Set<String> set) {
		Queue<String> q = new LinkedList<>();
		for (String c : eq.keySet()) {
			if (eq.get(c)<0)
				continue;
			q.add(c);
		}
		while (!q.isEmpty()) {
			String c = q.remove();
			Map<String, Double> next = map.get(c);
			if (next==null)
				continue;
			for (String nextC : next.keySet()) {
				if (set.contains(nextC))
					continue;
				if (eq.containsKey(nextC))
					continue;
				if (next.get(nextC)<0)
					continue;
				eq.put(nextC, eq.get(c)*next.get(nextC));
				if (nextC.equals(dest))
					return;
				q.add(nextC);
			}
			set.add(c);
		}
		eq.put(dest, NORESULT);
	}

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;

		 List<List<String>> equations = new ArrayList<>();
		 equations.add(Arrays.asList(new String[] {"a","b"}));
		 equations.add(Arrays.asList(new String[] {"c","d"}));
		 equations.add(Arrays.asList(new String[] {"e","f"}));
		 equations.add(Arrays.asList(new String[] {"g","h"}));
		 double[] values = {4.5,2.3,8.9,0.44};
		 List<List<String>> queries = new ArrayList<>();
		 queries.add(Arrays.asList(new String[] {"a","c"}));
		 queries.add(Arrays.asList(new String[] {"d","f"}));
		 queries.add(Arrays.asList(new String[] {"h","e"}));
		 queries.add(Arrays.asList(new String[] {"b","e"}));
		 queries.add(Arrays.asList(new String[] {"d","h"}));
		 queries.add(Arrays.asList(new String[] {"g","f"}));
		 queries.add(Arrays.asList(new String[] {"c","g"}));
		 System.out.println(Arrays.toString(calcEquation(equations, values, queries)));
	 }

}
