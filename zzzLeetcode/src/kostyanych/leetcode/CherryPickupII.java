package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given a rows x cols matrix grid representing a field of cherries. Each cell in grid represents the number of cherries that you can collect.
 * You have two robots that can collect cherries for you, Robot #1 is located at the top-left corner (0,0),
 * 	and Robot #2 is located at the top-right corner (0, cols-1) of the grid.
 *
 * Return the maximum number of cherries collection using both robots  by following the rules below:
 *
 * 	From a cell (i,j), robots can move to cell (i+1, j-1) , (i+1, j) or (i+1, j+1).
 *  When any robot is passing through a cell, It picks it up all cherries, and the cell becomes an empty cell (0).
 *  When both robots stay on the same cell, only one of them takes the cherries.
 *  Both robots cannot move outside of the grid at any moment.
 *  Both robots should reach the bottom row in the grid.
 *
 */
public class CherryPickupII {

	private static int rows;
	private static int cols;
	private static int[][] ggrid;
	//static Map<String,Integer> map = new HashMap<>();
	private static int[][][] memo;

	/**
	 * @param grid
	 * @return
	 */
	private static int cherryPickup(int[][] grid) {
		rows=grid.length;
		cols=grid[0].length;
		ggrid=grid;
		memo=new int[rows][cols][cols];
		for (int i=0;i<rows;i++) {
			for (int j=0;j<cols;j++) {
				Arrays.fill(memo[i][j],-1);
			}
		}

//		int[] r1= {0,0};
//		int[] r2= {0,cols-1};
		return step(0, 0, cols-1);

    }

	private static int step(int row, int col1, int col2) {
		if (row<0|| row>=rows)
			return 0;
		if (col1>=cols || col2>=cols || col1<0 || col2<0)
			return 0;

		if (memo[row][col1][col2]>=0)
			return memo[row][col1][col2];

		int cur=0;
		if (col1==col2)
			cur+=ggrid[row][col1];
		else
			cur=cur+ggrid[row][col1]+ggrid[row][col2];
		if (row==rows-1) {
			memo[row][col1][col2]=cur;
			return cur;
		}

		int buf1=step(row+1,col1-1, col2-1);
		int buf2=step(row+1,col1-1, col2);
		int buf3=step(row+1,col1-1, col2+1);

		int buf4=step(row+1,col1, col2-1);
		int buf5=step(row+1,col1, col2);
		int buf6=step(row+1,col1, col2+1);

		int buf7=step(row+1,col1+1, col2-1);
		int buf8=step(row+1,col1+1, col2);
		int buf9=step(row+1,col1+1, col2+1);

		int max = Math.max(buf1, Math.max(buf2,  buf3));
		max = Math.max(max, Math.max(buf4, Math.max(buf5,  buf6)));
		max = Math.max(max, Math.max(buf7, Math.max(buf8,  buf9)));
		cur+=max;
		memo[row][col1][col2]=cur;
		return cur;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr = {{1,0,0,3},{0,0,0,3},{0,0,3,3},{9,0,3,3}};
		System.out.println(cherryPickup(arr));
	}

}
