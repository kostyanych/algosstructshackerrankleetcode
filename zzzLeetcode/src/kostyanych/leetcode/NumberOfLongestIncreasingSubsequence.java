package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given an integer array nums, return the number of longest increasing subsequences.
 *
 * Example 1:
 * Input: nums = [1,3,5,4,7]
 * Output: 2
 * Explanation: The two longest increasing subsequences are [1, 3, 4, 7] and [1, 3, 5, 7].
 *
 * Constraints:
 * 0 <= nums.length <= 2000
 * -106 <= nums[i] <= 106
 *
 */
public class NumberOfLongestIncreasingSubsequence {

	private static int findNumberOfLIS(int[] nums) {
		int len=nums.length;
		if (len<2)
			return len;
		int[] lengths = new int[len];	// contains LIS at this index
		int[] counts = new int[len];	// contains count of LIS ending at index

	    int maxlen = 1;
	    int maxcount = 0;

	    for(int i=0; i<len; i++){
	    	lengths[i] = 1;  // can't be less than 1
	    	counts[i] = 1;
	    	for(int prev=0; prev<i; prev++){
	    		if (nums[i] > nums[prev]) {
	    			if(lengths[prev]+1 > lengths[i]) { //adding this num to prev makes sequence longer
	    				lengths[i] = lengths[prev] + 1;
	    				counts[i]=counts[prev];
                    }
		    		else if(lengths[prev]+1 == lengths[i])  //adding this num to prev makes the same sequence length, so add whatever count was at prev - we can make that much sequences with it
		    			counts[i] += counts[prev];
	    		}
	    	}
	    	if (maxlen < lengths[i]) {
	    		maxlen=lengths[i];
	    		maxcount=counts[i];
	    	}
	    	else if (maxlen == lengths[i])
	    		maxcount+=counts[i];
	    }
	    return maxcount;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		//int[] arr= {1,3,5,4,7};
		//int[] arr= {2,2,2,2,2};
		int[] arr= {7,9,3,5,4};
		System.out.println(Arrays.toString(arr));
		System.out.println(findNumberOfLIS(arr));
	}

}
