package kostyanych.leetcode;

/**
 * We stack glasses in a pyramid, where the first row has 1 glass,
 * the second row has 2 glasses, and so on until the 100th row.
 *
 * Then, some champagne is poured in the first glass at the top.
 * When the topmost glass is full, any excess liquid poured will fall equally to the glass immediately to the left and right of it.
 * When those glasses become full, any excess champagne will fall equally to the left and right of those glasses, and so on.
 * (A glass at the bottom row has its excess champagne fall on the floor.)
 *
 * For example, after one cup of champagne is poured, the top most glass is full.
 * After two cups of champagne are poured, the two glasses on the second row are half full.
 * After three cups of champagne are poured, those two cups become full - there are 3 full glasses total now.
 * After four cups of champagne are poured, the third row has the middle glass half full, and the two outside glasses are a quarter full.
 *
 * Constraints:
 *
 * 0 <= poured <= 109
 * 0 <= query_glass <= query_row < 100
 *
 */
public class ChampagneTower {

	static private double champagneTower(int poured, int query_row, int query_glass) {
        double[] cups = new double[101];
        cups[0] = poured;
        for (int row = 1; row <= query_row; row++)
            for (int i = row; i >= 0; i--) {
                cups[i] = Math.max(0.0, (cups[i] - 1) / 2);
                cups[i + 1] += cups[i];
            }

        return Math.min(cups[query_glass], 1.0);
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(champagneTower(5,4,2));
	}

}
