package kostyanych.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * In a town, there are N people labelled from 1 to N.  There is a rumor that one of these people is secretly the town judge.
 *
 * If the town judge exists, then:
 * 1. The town judge trusts nobody.
 * 2. Everybody (except for the town judge) trusts the town judge.
 * 3. There is exactly one person that satisfies properties 1 and 2.
 *
 * You are given trust, an array of pairs trust[i] = [a, b] representing that the person labelled a trusts the person labelled b.
 *
 * If the town judge exists and can be identified, return the label of the town judge.  Otherwise, return -1.
 *
 * 1 <= N <= 1000
 * trust.length <= 10000
 * trust[i] are all different
 * trust[i][0] != trust[i][1]
 * 1 <= trust[i][0], trust[i][1] <= N
 *
 */
public class FindTheTownJudge {

	private static int findJudge(int N, int[][] trust) {
        Map<Integer,Set<Integer>> map = new HashMap<>();
        for (int[] t : trust) {
            Set<Integer> lst = map.computeIfAbsent(t[0], key -> new HashSet<Integer>());
            lst.add(t[1]);
        }
        if (map.keySet().size()!=N-1)
            return -1;
        int res=-1;
        for (int i=1;i<=N;i++) {
            if (!map.containsKey(i)) {
                res=i;
                continue;
            }
        }
        if (res!=-1) {
        	for (int i=1;i<=N;i++) {
        		if (i==res)
        			continue;
        		if (!map.get(i).contains(res)) {
        			return-1;
        		}
        	}
        }
        return res;
    }

	private static int findJudgeFast(int N, int[][] trust) {
        if(trust.length <  N-1)
           return -1;

       int [] peopleITrust  = new int[N+1];
       int [] peopleThatTrustMe = new int[N+1];

       for(int[] t : trust){
    	   peopleITrust[t[0]]++;
    	   peopleThatTrustMe[t[1]]++;
       }
       for (int i = 1; i <= N; i++) {
    	   if (peopleThatTrustMe[i] == N - 1 && peopleITrust[i] == 0) {
    		   return i;
    	   }
       }
       return -1;
   }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[][] arr= {{1,3},{2,3}};
		int N=3;
		System.out.println(findJudgeFast(N, arr));
	}

}
