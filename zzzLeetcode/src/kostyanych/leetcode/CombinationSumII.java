package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Given a collection of candidate numbers (candidates) and a target number (target),
 * find all unique combinations in candidates where the candidate numbers sums to target.
 *
 * Each number in candidates may only be used once in the combination.
 *
 * Note:
 *
 * All numbers (including target) will be positive integers.
 * The solution set must not contain duplicate combinations.
 *
 */
public class CombinationSumII {

	private final static List<List<Integer>> res = new ArrayList<>();
//	private final static Map<String,List<List<Integer>>> map = new HashMap<>();

	private static List<List<Integer>> combinationSum2(int[] candidates, int target) {
		if (target==0) {
            res.add(new ArrayList<>());
            return res;
        }
        Arrays.sort(candidates);
        process(candidates, 0, target, new ArrayList<Integer>());
        return res;
	}

	private static void process(int[] candidates, int curIndex, int target, List<Integer> currList) {
		if(target <0 || curIndex >candidates.length)
			return;

		if (target==0) {
			res.add(new ArrayList<>(currList));
			return;
		}

		int run = curIndex;
        while (run < candidates.length){
            if(target - candidates[run] < 0){
                run++;
                continue;
            }

            if( run>curIndex && candidates[run]==candidates[run-1]){
                run++;
                continue;
            }

            target -=candidates[run];
            currList.add(candidates[run]);
            process(candidates, run+1, target, currList);
            target +=candidates[run];
            currList.remove(currList.size()-1);
            run++;
        }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {2,5,2,1,2};
		System.out.println(combinationSum2(arr,5));
	}
}
