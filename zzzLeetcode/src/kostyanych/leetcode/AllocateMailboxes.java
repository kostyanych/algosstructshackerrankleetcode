package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given the array houses and an integer k. where houses[i] is the location of the ith house along a street,
 * your task is to allocate k mailboxes in the street.
 *
 * Return the minimum total distance between each house and its nearest mailbox.
 *
 * The answer is guaranteed to fit in a 32-bit signed integer.
 *
 * Constraints:
 *   n == houses.length
 *   1 <= n <= 100
 *   1 <= houses[i] <= 10^4
 *   1 <= k <= n
 *   Array houses contain unique integers.
 *
 * Soultion:
 * 1. where to place a single mailbox?
 * 1a. if n is odd, then it's the position of the middle house:
 *     if you move mailbox to either side you decrease the distance for the odd number of houses on this side,
 *     but increase for the same odd number + 1 (+1 is for this middle house from whence we moved out)
 * 1b. if n is even, then it's the position of ANY one of the 2 middle houses:
 * 	   if we move to the left of the left middle house or to the right of the right middle house we degrading the result as descrbed in 1a
 *     if we move from one of the middle houses towards the other, we increase distance for n/2 houses on the source side
 *     and decrease distance for same n/2 houses on the target side. Thus, the overall distance won't change
 * 2. how to find min distance for k mailboxes?
 *     Basically, by a brute force:
 *     a. place 1st mb at the first house
 *     b. then consider n-1 houses starting with the second and k-1 mailboxes
 *     c. calculate the min distance for that configuration
 *     d. place 1st mb at the second house
 *     e. then consider n-2 houses starting with the second and k-1 mailboxes
 *     and so on
 *     Memoize found minimum for n-x houses and k-y mailboxes, because there will be repeats in the recursion
 *
 */
public class AllocateMailboxes {

	private static final int SUPERMAX=100*1000*10;	//more than the distance would ever be

	private static int minDistance(int[] houses, int k) {
		int n = houses.length;
		if (k>=n)
			return 0;

		Arrays.sort(houses);

		int[][] memo=new int[n+1][k+1];
	    for (int i=0;i<=n;i++){
	        Arrays.fill(memo[i],-1);
	    }
	    return  helper(houses, 0, k, n, memo);
	}

	private static int helper(int[] houses , int curmb, int k, int n, int[][] memo){

		if (memo[curmb][k]!=-1)
	        return memo[curmb][k];

		if (k==1) {
			int ind=curmb+(n-curmb)/2;
			int mid=houses[ind];
			int cost=0;
	        for (int i=curmb; i<n; i++) {
	            cost+=Math.abs(mid-houses[i]);
	        }
	        memo[curmb][k]=cost;
	        return cost;
		}

		int res=SUPERMAX;

		for (int i=curmb; i<n-1; i++){
	        int mid=houses[(curmb+i)/2];
	        int cost=0;

	        for (int j=curmb; j<=i; j++) {
	            cost+=Math.abs(mid-houses[j]);
	        }
	        res=Math.min(res,helper(houses, i+1, k-1, n, memo)+cost);
	    }
	    return memo[curmb][k]=res;
	}

	private static int helperSLOW(int[] houses , int curmb, int k, int n, int[][] memo){

		if (k==0 && curmb==n )
            return 0;

		int res=SUPERMAX;

		if (curmb==n || k==0)
	        return res;

		if (memo[curmb][k]!=-1)
	        return memo[curmb][k];

	    for (int i=curmb; i<n; i++){
	        int mid=houses[(curmb+i)/2];
	        int cost=0;

	        for (int j=curmb; j<=i; j++) {
	            cost+=Math.abs(mid-houses[j]);
	        }
	        res=Math.min(res,helper(houses, i+1, k-1, n, memo)+cost);
	    }
	    return memo[curmb][k]=res;
	}

	public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 int[] arr= {0,1,4,6};
		 int k=2;
		 System.out.print(Arrays.toString(arr));
		 System.out.println(", k="+k);
		 System.out.print(minDistance(arr,k));
	 }

}
