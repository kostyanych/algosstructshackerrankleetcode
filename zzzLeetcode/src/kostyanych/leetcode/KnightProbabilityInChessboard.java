package kostyanych.leetcode;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

/**
 * This solution exceeds memory!!!
 *
 */
public class KnightProbabilityInChessboard {

	static private int[][] moves = {{1,2},{1,-2},{-1,2},{-1,-2},{2,1},{2,-1},{-2,1},{-2,-1}};

	static private double knightProbability(int n, int k, int row, int column) {

		Queue<int[]> q = new ArrayDeque<>();
		double res=1.;
		q.add(new int[] {row, column});
		while (!q.isEmpty() && k>0) {
			int s = q.size();
			k--;

			for (int i=0;i<s;i++) {
				int[] coords=q.poll();

				for (int[] m : moves) {
					int r=coords[0]+m[0];
					int c=coords[1]+m[1];
					if (r<0 || c<0 || r>=n || c>=n)
						continue;
					q.add(new int[] {r,c});
				}
			}

			res*=q.size();
			res/=(s*8);
		}
		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(knightProbability(3, 2, 0, 0));
	}

}
