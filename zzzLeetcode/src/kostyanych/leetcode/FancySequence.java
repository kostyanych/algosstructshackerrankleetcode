package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Write an API that generates fancy sequences using the append, addAll, and multAll operations.
 *
 * Implement the Fancy class:
 *
 * Fancy() Initializes the object with an empty sequence.
 * void append(val) Appends an integer val to the end of the sequence.
 * void addAll(inc) Increments all existing values in the sequence by an integer inc.
 * void multAll(m) Multiplies all existing values in the sequence by an integer m.
 * int getIndex(idx) Gets the current value at index idx (0-indexed) of the sequence modulo 10^9 + 7.
 * If the index is greater or equal than the length of the sequence, return -1.
 *
 * Constraints:
 * 	1 <= val, inc, m <= 100
 * 	0 <= idx <= 10^5
 * 	At most 10^5 calls total will be made to append, addAll, multAll, and getIndex.
 *
 */
public class FancySequence {

	static class Fancy {

		final static private int MOD = 1_000_000_007;
		final static int[] modInverses = new int[101];

		static {
			for (int i=1;i<100;i++) {
				modInverses[i] = modInverse(i);
			}
		}

		 // Modular multiplicative inverse x => a * x % MOD == 1
		private static int modInverse(int a) {
			int y = 0;
			int x = 1;

			int rem = MOD;
			while (a > 1) {
				// q is quotient
		        int q = a / rem;

		        int t = rem;

		        // rem is the remainder, process same as Euclid's algo
		        rem = a % rem;
		        a = t;
		        t = y;

		        // Update x and y
		        y = x - q * y;
		        x = t;
			}

		    // Make x positive
		    if (x < 0)
		    	x += MOD;

		    return x;
	    }

		final private List<Integer> vals = new ArrayList<>();
		//we make them long, not because they can really become long after %MOD
		//but at addition and multiplication with big ints they can overflow. we don't wanna mess with manual type conversion for that
		//so le them be long, just (int) the result every time
		private long add=0;
		private long mult=1;
		private long invMult = 1; // inverse cumulative multiplication (%MOD)

		public Fancy() {
		}

		//we store vals in mod-normalized form of (val-add)/mult, so we can apply add and mult to any value - older ones will yield (oldVal+add)*mult and latter vals will yield just val
		public void append(int val) {
			long buf = (val - add +MOD) % MOD;
			val = (int)((buf*invMult )%MOD);
			vals.add(val);
		}

		public void addAll(int inc) {
			//we should have done +inc%MOD, but we know that inc<=100, so why bother?
			add = (add + inc) % MOD;
		}

		public void multAll(int m) {
			//we should have done *m%MOD, but we know that m<=100, so why bother?
			mult = (mult * m) % MOD;
			add = (add * m) % MOD;
			invMult = (invMult * modInverses[m]) % MOD;
		}

		public int getIndex(int idx) {
			if (idx<0 || idx >= vals.size())
				return -1;
			int val=vals.get(idx);
			val=(int)( ((val*mult)%MOD + add)%MOD) ;
			return val;
		}
	}



	//TOOOOOOO SLOOOOOOOOOW
	static class FancySlow {

		final static private int MOD = 1_000_000_007;

		static private class Wrapper {
			Wrapper previous;
			List<Integer> values = new ArrayList<>();
			int add = 0;
			int mult = 1;
			int startingIndex = 0;

			private boolean hasOperations() {
				if (add!=0 || mult!=1)
					return true;
				return false;
			}

			public void add(int val) {
				previous.add = (previous.add+val%MOD)%MOD;
			}

			public void mult(int val) {
				previous.mult = (int)( ((long)previous.mult*val%MOD)%MOD );
				previous.add = (int)( ((long)previous.add*val%MOD)%MOD );
			}

			public void append(int val) {
				if (!previous.hasOperations()) {
					previous.values.add(val%MOD);
					return;
				}
				Wrapper newWrap = new Wrapper();
				newWrap.startingIndex = previous.startingIndex+previous.values.size();
				newWrap.values.add(val%MOD);
				newWrap.previous=this.previous;
				this.previous=newWrap;
			}

			public int get(int ind) {
				Stack<Wrapper> st = new Stack<>();
				st.add(previous);
				Wrapper buf = previous;
				while (buf!=null && ind < buf.startingIndex) {
					buf = buf.previous;
					st.add(buf);
				}
				if (buf==null)
					return -1;
				int val=buf.values.get(ind-buf.startingIndex);
				while (!st.isEmpty()) {
					Wrapper ops = st.pop();
					val = (int)(((long)ops.mult*val)%MOD);
					val = (int)(((long)ops.add+val)%MOD);
				}
				return val;
			}
		}


		final private Wrapper wrap;
		int counter = 0;


		public FancySlow() {
			wrap = new Wrapper();
			wrap.previous = new Wrapper();
		}

		public void append(int val) {
			wrap.append(val);
			counter++;
		}

		public void addAll(int inc) {
			wrap.add(inc);

		}

		public void multAll(int m) {
			wrap.mult(m);
		}

		public int getIndex(int idx) {
			if (idx<0)
				return -1;
			if (idx>=counter)
				return -1;
			return wrap.get(idx);
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String[] comms= {"append","append","getIndex","append","getIndex","addAll","append","getIndex","getIndex","append","append","getIndex","append","getIndex","append","getIndex","append","getIndex","multAll","addAll","getIndex","append","addAll","getIndex","multAll","getIndex","multAll","addAll","addAll","append","multAll","append","append","append","multAll","getIndex","multAll","multAll","multAll","getIndex","addAll","append","multAll","addAll","addAll","multAll","addAll","addAll","append","append","getIndex"};
		int[] params= {12,8,1,12,0,12,8,2,2,4,13,4,12,6,11,1,10,2,3,1,6,14,5,6,12,3,12,15,6,7,8,13,15,15,10,9,12,12,9,9,9,9,4,8,11,15,9,1,4,10,9};
		int len = comms.length;
		Fancy fancy = new Fancy();
		for (int i=0;i<len;i++) {
			if ("append".equals(comms[i]))
				fancy.append(params[i]);
			else if ("addAll".equals(comms[i]))
				fancy.addAll(params[i]);
			else if ("multAll".equals(comms[i]))
				fancy.multAll(params[i]);
			else if ("getIndex".equals(comms[i]))
				System.out.println(fancy.getIndex(params[i]));
		}
//		fancy.append(2);   // fancy sequence: [2]
//		fancy.addAll(3);   // fancy sequence: [2+3] -> [5]
//		fancy.append(7);   // fancy sequence: [5, 7]
//		fancy.multAll(2);  // fancy sequence: [5*2, 7*2] -> [10, 14]
//		System.out.println(fancy.getIndex(0)); // return 10
//		fancy.addAll(3);   // fancy sequence: [10+3, 14+3] -> [13, 17]
//		fancy.append(10);  // fancy sequence: [13, 17, 10]
//		fancy.multAll(2);  // fancy sequence: [13*2, 17*2, 10*2] -> [26, 34, 20]
//		System.out.println(fancy.getIndex(0)); // return 26
//		System.out.println(fancy.getIndex(1)); // return 34
//		System.out.println(fancy.getIndex(2)); // return 20
	}
}
