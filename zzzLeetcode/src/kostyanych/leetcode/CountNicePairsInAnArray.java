package kostyanych.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * You are given an array nums that consists of non-negative integers.
 * Let us define rev(x) as the reverse of the non-negative integer x.
 * For example, rev(123) = 321, and rev(120) = 21. A pair of indices (i, j) is nice if it satisfies all of the following conditions:
 *
 * 0 <= i < j < nums.length
 * nums[i] + rev(nums[j]) == nums[j] + rev(nums[i])
 *
 * Return the number of nice pairs of indices. Since that number can be too large, return it modulo 10^9 + 7.
 *
 * Constraints:
 *
 * 1 <= nums.length <= 10^5
 * 0 <= nums[i] <= 10^9
 *
 */
public class CountNicePairsInAnArray {

	static private final int MOD = 1_000_000_007;

	static final private Map<Integer,Integer> counter = new HashMap<>();

	@SuppressWarnings("unchecked")
	static final private Map<Integer,Integer>[] Cm = new Map[3];

	static {
		Cm[1] = new HashMap<>();
		Cm[2] = new HashMap<>();
	}

	static private int countNicePairs(int[] nums) {
		for (int i : nums) {
			int diff=i-rev(i);
			counter.compute(diff, (k,v) -> v == null ? 1 : v+1);
		}
		int res=0;
		for (Integer k : counter.keySet()) {
			Integer count = counter.get(k);
			if (count<2)
				continue;
			res=(res+getC(count,2))%MOD;
		}
        return res;
    }

	static private int getC(int m, int k) {
		if (m==1 && k==1)
			return 1;
		if (m==0)
			return 0;
		if (k==0)
			return 1;
		Map<Integer,Integer> map = Cm[k];
		Integer c = map.get(m);
		if (c!=null)
			return c;
		int a = getC(m-1, k-1);
		int b = getC(m-1, k);
		c = (a+b)%MOD;
		map.put(m,  c);
		return c;
	}

	static private int rev(int n) {
		int res=0;
		while(n>0) {
			int d = n%10;
			res=res*10+d;
			n/=10;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {13,10,35,24,76};
		System.out.println(countNicePairs(arr));
	}

}
