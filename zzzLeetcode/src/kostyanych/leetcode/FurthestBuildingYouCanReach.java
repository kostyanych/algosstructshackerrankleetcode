package kostyanych.leetcode;

import java.util.PriorityQueue;

/**
 * You are given an integer array heights representing the heights of buildings, some bricks, and some ladders.
 *
 * You start your journey from building 0 and move to the next building by possibly using bricks or ladders.
 *
 * While moving from building i to building i+1 (0-indexed),
 *
 * If the current building's height is greater than or equal to the next building's height, you do not need a ladder or bricks.
 * If the current building's height is less than the next building's height, you can either use one ladder or (h[i+1] - h[i]) bricks.
 * Return the furthest building index (0-indexed) you can reach if you use the given ladders and bricks optimally.
 *
 * Constraints:
 * 1 <= heights.length <= 10^5
 * 1 <= heights[i] <= 10^6
 * 0 <= bricks <= 10^9
 * 0 <= ladders <= heights.length
 *
 * SOLUTION:
 * 1. Assume we CAN cover N gaps. Some of them with l ladders, others by filling them with bricks optimally.
 * 1a. Optimal filling will mean "most quantity of gaps with same amount of bricks". That will mean we have to fill smallest gaps first.
 * 2. So we need to to sort gaps and take those smallest that we can cover with bricks and then l gaps to cover with ladders.
 * 3. The problem is we can't just sort the whole array.
 * 4. So we'll build our array to sort by adding gaps one by one as they come in the initial array
 * 4a. Sort this new array it at every step
 * 4b. check if we still have bricks to fill it up - if not, we're done, if yes - go back to step 4.
 * 5. Optimal structure to sort array on every addition to it is a heap / priority queue (PQ).
 * 6. Since we can cover any gap with a ladder we can initially put l first gaps to our new array.
 * 7. Checking for available bricks is getting the head of the PQ and subtracting it from remaining bricks. If bricks remaining >=0, then this step went OK.
 *
 */
public class FurthestBuildingYouCanReach {

	static private int furthestBuilding(int[] heights, int bricks, int ladders) {
		int len = heights.length;
		if (ladders>=len)
			return len-1;
		PriorityQueue<Integer> queue = new PriorityQueue<>();
        for(int i=0;i<len-1;i++) {
            int gap = heights[i+1]-heights[i];
            if(gap>0){
                queue.add(gap);
                if(queue.size()>ladders) {
                    bricks -= queue.poll();
                    if (bricks<0)
                    	return i;
                }
            }
        }
        return len-1;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {4,12,2,7,3,18,20,3,19};
		System.out.println(furthestBuilding(arr, 10,2));
	}

}
