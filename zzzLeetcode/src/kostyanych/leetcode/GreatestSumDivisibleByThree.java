package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Given an array nums of integers, we need to find the maximum possible sum of elements of the array such that it is divisible by three.
 *
 * Constraints:
 * 1 <= nums.length <= 4 * 10^4
 * 1 <= nums[i] <= 10^4
 *
 */
public class GreatestSumDivisibleByThree {

	static private int maxSumDivThree(int[] nums) {
		int rem1=10001; // we'll store the minimum sum of max 2 numbers whose sum % 3 = 1 (it would be either smallest num -> num%3=1, or the smallest sum of num1, num2 -> num1%3 = num2%3 = 2
		int rem2=10001; // we'll store the minimum sum of max 2 numbers whose sum % 3 = 2 (it would be either smallest num -> num%3=2, or the smallest sum of num1, num2 -> num1%3 = num2%3 = 1
		int res=0;
        for (int i : nums) {
            res += i;

            int rem=i%3;
            if (rem == 1) {
            	rem2=Math.min(rem2, rem1+i);
            	rem1=Math.min(rem1, i);
            }
            if (i%3 == 2) {
                rem1 = Math.min(rem1, rem2+i);
                rem2 = Math.min(rem2, i);
            }
        }

        if (res%3 == 0)
            return res;
        else if (res%3 == 1)
            return res-rem1;
        return res-rem2;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {2,2};
		System.out.println(maxSumDivThree(arr));
	}

}
