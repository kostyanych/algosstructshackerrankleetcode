package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import kostyanych.leetcode.helpers.Printer;

/**
 * Given an m x n matrix, return a new matrix answer where answer[row][col] is the rank of matrix[row][col].
 *
 * The rank is an integer that represents how large an element is compared to other elements. It is calculated using the following rules:
 * 		The rank is an integer starting from 1.
 * 		If two elements p and q are in the same row or column, then:
 * 			If p < q then rank(p) < rank(q)
 * 			If p == q then rank(p) == rank(q)
 * 			If p > q then rank(p) > rank(q)
 * 		The rank should be as small as possible.
 *
 * It is guaranteed that answer is unique under the given rules.
 *
 * Constraints:
 * m == matrix.length
 * n == matrix[i].length
 * 1 <= m, n <= 500
 * -10^9 <= matrix[row][col] <= 10^9
 *
 */
public class RankTransformOfAMatrix {

	private static int[][] matrixRankTransform(int[][] matrix) {
		 int m = matrix.length;
		 int n = matrix[0].length;

		 int[][] res = new int[m][n];

		 int[] maxRankRow = new int[m];
	     int[] maxRankCol = new int[n];

	     TreeMap<Integer, List<int[]>> map = new TreeMap<>();
	     for (int r = 0; r < m; r++) {
	    	 for (int c = 0; c < n; c++) {
	    		 map.computeIfAbsent(matrix[r][c], k -> new ArrayList<>()).add(new int[] { r, c });
	    	 }
	     }

	     // go from the lowest value key to the highest
	     for (int k : map.keySet()) {

	    	 // repeat for each value until we used all cells with the same number
	    	 // on each step we find cells connected by row/column and calculate their rank
	         while (map.get(k).size() > 0) {
	        	 Set<Integer> rowsUsed = new HashSet<>(m);
	             Set<Integer> colsUsed = new HashSet<>(n);
	             List<int[]> curValCoords = map.get(k);
	             int curValQuantity = curValCoords.size();

	             // get the first cell as the root and find all connected cells
	             int[] root = curValCoords.get(0);
	             rowsUsed.add(root[0]);
	             colsUsed.add(root[1]);
	             boolean[] used = new boolean[curValQuantity];
	             used[0] = true;
	             // continue until we found all connected
	             while (true) {
	            	 boolean addedAnything = false;
	                 for (int i = 1; i < curValQuantity; i++) {
	                	 if (used[i])
	                		 continue;
	                	 int[] curCoords = curValCoords.get(i);

	                	 // if the cell is in the same row OR column with the root or any one that is already connected with the root
	                     if (rowsUsed.contains(curCoords[0]) || colsUsed.contains(curCoords[1])) {
	                    	 rowsUsed.add(curCoords[0]);
	                         colsUsed.add(curCoords[1]);
	                         used[i] = true;
	                         addedAnything=true;
	                     }
	                 }
	                 if (!addedAnything)
	                	 break;
	             }

	             List<int[]> connected = new ArrayList<>();
	             List<int[]> left = new ArrayList<>();
	             for (int i = 0; i < curValQuantity; i++) {
	            	 if (used[i])
	            		 connected.add(curValCoords.get(i));
	            	 else
	            		 left.add(curValCoords.get(i));
	             }

	             //put all that are not connected back to the map
	             map.put(k, left);

	             int rank = Integer.MIN_VALUE;

	             //calculate the maximum rank of all connected cells
	             for (int[] conn : connected) {
	            	 rank = Math.max(rank, Math.max(maxRankRow[conn[0]], maxRankCol[conn[1]]) + 1);
	             }

	             // update maxRank for all cols and rows and set the rank as answer for each connected cell
	             for (int[] conn : connected) {
	            	 maxRankRow[conn[0]] = rank;
	            	 maxRankCol[conn[1]] = rank;
	            	 res[conn[0]][conn[1]] = rank;
	             }
	         }
	     }
	     return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] m = { {1,2,2,1}, {3,4,1,6}, {2,1,2,1} };
		Printer.print2DArray(m, false);
		Printer.print2DArray(matrixRankTransform(m), false);
	}

}
