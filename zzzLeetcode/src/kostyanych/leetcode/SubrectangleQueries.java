package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the class SubrectangleQueries which receives a rows x cols rectangle as a matrix of integers in the constructor and supports two methods:
 *
 * 1. updateSubrectangle(int row1, int col1, int row2, int col2, int newValue)
 *     Updates all values with newValue in the subrectangle whose upper left coordinate is (row1,col1) and bottom right coordinate is (row2,col2).
 * 2. getValue(int row, int col)
 *     Returns the current value of the coordinate (row,col) from the rectangle.
 *
 * Constraints:
 *    There will be at most 500 operations considering both methods: updateSubrectangle and getValue.
 *    1 <= rows, cols <= 100
 *    rows == rectangle.length
 *    cols == rectangle[i].length
 *    0 <= row1 <= row2 < rows
 *    0 <= col1 <= col2 < cols
 *    1 <= newValue, rectangle[i][j] <= 10^9
 *    0 <= row < rows
 *    0 <= col < cols
 *
 */
public class SubrectangleQueries {

	static class Data {
		int row1;
		int col1;
		int row2;
		int col2;
		int newValue;

		public Data(int row1, int col1, int row2, int col2, int newValue) {
			this.row1 = row1;
			this.col1 = col1;
			this.row2 = row2;
			this.col2 = col2;
			this.newValue = newValue;
		}
	}

	private final int[][] rect;
	private final List<Data> lst = new ArrayList<>();

	public SubrectangleQueries(int[][] rectangle) {
		rect=rectangle;
    }

    public void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
    	lst.add(new Data(row1, col1, row2, col2, newValue));

    }

    public int getValue(int row, int col) {
    	for (int i=lst.size()-1; i>=0; i--) {
    		Data dt=lst.get(i);
    		if (row>=dt.row1 && row<=dt.row2 && col>=dt.col1 && col<=dt.col2)
    			return dt.newValue;
    	}
    	return rect[row][col];
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr = {{1,2,1},{4,3,4},{3,2,1},{1,1,1}};
		SubrectangleQueries z = new SubrectangleQueries(arr);
		System.out.println(z.getValue(0,2));
		z.updateSubrectangle(0,0,3,2,5);
		System.out.println(z.getValue(0,2));
		System.out.println(z.getValue(3,1));
		z.updateSubrectangle(3,0,3,2,10);
		System.out.println(z.getValue(3,1));
		System.out.println(z.getValue(0,2));
	}

}
