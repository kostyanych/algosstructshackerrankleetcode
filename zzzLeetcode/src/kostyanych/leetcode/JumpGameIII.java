package kostyanych.leetcode;
import java.util.LinkedList;
import java.util.Queue;

public class JumpGameIII {

	private static boolean canReach(int[] arr, int start) {
        int len = arr.length;
		if (arr[start]==0)
			return true;
		Queue<Integer> q = new LinkedList<>();
		boolean[] visited = new boolean[arr.length];
		q.add(start);
		while (!q.isEmpty()) {
			int ind=q.remove();
			visited[ind]=true;
			int next=ind-arr[ind];
			if (next>=0 && !visited[next]) {
				if (arr[next]==0)
					return true;
				q.add(next);
			}
			next=ind+arr[ind];
			if (next<len && !visited[next]) {
				if (arr[next]==0)
					return true;
				q.add(next);
			}
		}
		return false;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		int[] arr = {4,2,3,0,3,1,2};
		int start=5;
		System.out.println(canReach(arr,start));
		
		arr = new int[] {4,2,3,0,3,1,2};
		start=0;
		System.out.println(canReach(arr,start));
		
		arr = new int[] {3,0,2,1,2};
		start=2;
		System.out.println(canReach(arr,start));
	}
}
