package kostyanych.leetcode;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class ImplementRand10UsingRand7 {

	private static ThreadLocalRandom rand = ThreadLocalRandom.current();

	private static int rand10() {
		int rand15=0;
        do {
            rand15=rand7();
        } while(rand15>5);
        int rand16=0;
        do {
            rand16=rand7();
        } while(rand16>6);
        return 2*rand15-(rand16%2);
    }

	private static int rand7() {
		return 1+rand.nextInt(7);
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = new int[10];
		for (int i=0;i<10000;i++) {
			arr[rand10()-1]++;
		}
		System.out.println(Arrays.toString(arr));

		Arrays.fill(arr, 0);
		for (int i=0;i<10000;i++) {
			arr[rand10()-1]++;
		}
		System.out.println(Arrays.toString(arr));

		Arrays.fill(arr, 0);
		for (int i=0;i<10000;i++) {
			arr[rand10()-1]++;
		}
		System.out.println(Arrays.toString(arr));

	}

}
