package kostyanych.leetcode.strings;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Given a string s of '(' , ')' and lowercase English characters.
 *
 *  Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions )
 *  so that the resulting parentheses string is valid and return any valid string.
 *
 *  Formally, a parentheses string is valid if and only if:
 *
 *  It is the empty string, contains only lowercase characters, or
 *  It can be written as AB (A concatenated with B), where A and B are valid strings, or
 *  It can be written as (A), where A is a valid string.

 *
 */
public class MinimumRemoveToMakeValidParentheses {

	static private String minRemoveToMakeValid(String s) {
        Deque<Integer> st = new LinkedList<>();
        char[] chars=s.toCharArray();
        int len=chars.length;
        for (int i=0;i<len;i++) {
            if (chars[i]=='(')
                st.addLast(i);
            else if (chars[i]==')') {
                if (!st.isEmpty() && chars[st.peekLast()]=='(')
                    st.removeLast();
                else
                	st.addLast(i);
            }
        }

        if (st.isEmpty())
            return s;

        StringBuilder sb = new StringBuilder();
        for (int i=0;i<len;i++) {
            if (!st.isEmpty() && i==st.peekFirst()) {
                st.removeFirst();
                continue;
            }
            sb.append(chars[i]);
        }

        return sb.toString();
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="lee(t(c)o)de)";
		System.out.println(s);
		System.out.println(minRemoveToMakeValid(s));
	}
}
