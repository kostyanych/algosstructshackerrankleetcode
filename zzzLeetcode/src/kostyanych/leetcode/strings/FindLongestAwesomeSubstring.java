package kostyanych.leetcode.strings;

/**
 * Given a string s. An awesome substring is a non-empty substring of s such that we can make any number of swaps in order to make it palindrome.
 *
 * Return the length of the maximum length awesome substring of s.
 *
 * Constraints:
 * 	1 <= s.length <= 10^5
 * 	s consists only of digits.
 *
 */
public class FindLongestAwesomeSubstring {

	private static int longestAwesome(String s) {
		int len=s.length();
		if (len<2)
			return len;
		int ans = 1;
		// we only have characters from 0 to 9
        // so we have 0 for all evens (don't need that explicitly)
		// 1 for odd 0 and all others even
		// 2 for odd 1 and all others even
		// 4 for odd 2 and all others even
		// etc up to 2^9
	    int[] masks = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512 };

	    //calculate prefixes
	    //cumulative count of odds (1) and evens(0) up to and including this position
	    int[] pre = new int[len + 1];
	    for (int i = 1; i <= len; i++) {
	    	pre[i] = pre[i - 1] ^ masks[s.charAt(i-1) - '0'];
	    }

	    // calculate last indexes of prefixes
	    int[] lastpos = new int[1024]; //2^10 - all possible masks
	    for (int i = 0; i < len; i++) {
	    	lastpos[pre[i + 1]] = i;
	    }


	    // check max length between last index (+ any variations(10)) and current index
	    for (int i = 0; i < len; i++) {
		    // if prefix[i] equals prefix[j]
		    //		that means that between these 2 prefixes difference is 0,
		    //		that means we only have even occurrences of digits between i and j
		    // but we only interested in the longest such substring so we only compare with j that is the LAST index with such a prefix
	    	// we have prefix A at i and the last position of prefix A is lastpos[pre[i]]
	    	int prefAtI=pre[i];
	    	ans = Math.max(ans, lastpos[prefAtI] - i + 1);

	    	// now we want to check if we have some j at which we have a prefix that is prefix[i] XOR mask m
	    	// that would mean that we have m between j an i.
	    	// and having a value m means that we have all evens or 1 odd at that interval (meaning a palindrome!)
	    	// so we check prefix[i] against all masks
	    	// once again we only checking LAST index where that j can occur, since we're looking for _longest_ substring
	    	for (int mask : masks) {
	    		ans = Math.max(ans, lastpos[prefAtI ^ mask] - i + 1);
	    	}
	    }

	    return ans;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="1343211";
		System.out.println(s);
		System.out.println(longestAwesome(s));
	}
}
