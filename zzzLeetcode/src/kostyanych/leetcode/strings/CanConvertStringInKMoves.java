package kostyanych.leetcode.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Given two strings s and t, your goal is to convert s into t in k moves or less.
 *
 * During the ith (1 <= i <= k) move you can:
 * 	- Choose any index j (1-indexed) from s, such that 1 <= j <= s.length and j has not been chosen in any previous move,
 * 		and shift the character at that index i times.
 * 	- Do nothing.
 *
 * Shifting a character means replacing it by the next letter in the alphabet (wrapping around so that 'z' becomes 'a').
 * Shifting a character by i means applying the shift operations i times.
 *
 * Remember that any index j can be picked at most once.
 *
 * Return true if it's possible to convert s into t in no more than k moves, otherwise return false.
 *
 * Constraints:
 * 	1 <= s.length, t.length <= 10^5
 * 	0 <= k <= 10^9
 * 	s, t contain only lowercase English letters.
 *
 */
public class CanConvertStringInKMoves {

	static private boolean canConvertString(String s, String t, int k) {
		int len=s.length();
		if (len!=t.length())
			return false;
		//int [] diff = new int[len];
		int[] moves = new int[26];

		int max=0;
		for (int i=0;i<len;i++) {
			int diff=calc(t.charAt(i), s.charAt(i));
			if (diff==0)
				continue;
			moves[diff]++;
		}
		for (int i=1;i<26;i++) {
			int maxMovesHere = i+26*(moves[i]-1);
			if (maxMovesHere>k)
				return false;
		}
		return true;
	}

	static private boolean canConvertStringMap(String s, String t, int k) {
		int len=s.length();
		if (len!=t.length())
			return false;
		//int [] diff = new int[len];
		Map<Integer, Integer> map = new HashMap<>();

		int max=0;
		for (int i=0;i<len;i++) {
			int diff=calc(t.charAt(i), s.charAt(i));
			if (diff==0)
				continue;
			Integer move = map.get(diff);
			if (move==null) {
				map.put(diff, diff);
				move=diff;
			}
			else {
				move += 26;
				map.put(diff, move);
			}
			max=Math.max(move, max);
			if (max>k)
				return false;
		}
		if (max>k)
			return false;
		return true;
	}

	static private int calc(char c1, char c2) {
		int res=c1-c2;
		if (res>=0)
			return res;
		return 26+res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s = "input";
		String t = "ouput";
		int k = 9;
		System.out.println(canConvertString(s, t, k));
	}

}
