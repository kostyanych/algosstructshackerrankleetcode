package kostyanych.leetcode.strings;

/**
 * A happy string is a string that:
 *
 * - consists only of letters of the set ['a', 'b', 'c'].
 * - s[i] != s[i + 1] for all values of i from 1 to s.length - 1 (string is 1-indexed).
 *
 * For example, strings "abc", "ac", "b" and "abcbabcbcb" are all happy strings
 *      and strings "aa", "baa" and "ababbc" are not happy strings.
 *
 * Given two integers n and k, consider a list of all happy strings of length n sorted in lexicographical order.
 * Return the kth string of this list or return an empty string if there are less than k happy strings of length n.
 *
 */
public class KthLexicographicalStringOfAllHappyStringsLengthN {

	private static String getHappyString(int n, int k) {
		int count=pow(n-1);
		if (k>count*3)
			return "";
		StringBuilder sb=new StringBuilder();
		char lastChar;
		if (k<=count) {
			lastChar='a';
		}
		else if (k<=2*count) {
			lastChar='b';
			k-=(count);
		}
		else {
			lastChar='c';
			k-=(2*count);
		}
		sb.append(lastChar);
		while (count>1) {
			count/=2;
			if (k<=count) {
				lastChar=getFirstChar(lastChar);
			}
			else {
				lastChar=getSecondChar(lastChar);
				k-=count;
			}
			sb.append(lastChar);
		}
		return sb.toString();
    }

	private static char getFirstChar(char c) {
		if (c=='a')
			return 'b';
		return 'a';
	}

	private static char getSecondChar(char c) {
		if (c=='a')
			return 'c';
		if (c=='b')
			return 'c';
		return 'b';
	}

	private static int pow(int n) {
		int res=1<<n;
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n = 3, k = 1;
		System.out.println(getHappyString(n, k));
	}
}
