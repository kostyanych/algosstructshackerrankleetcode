package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an array of strings products and a string searchWord.
 * We want to design a system that suggests at most three product names from products after each character of searchWord is typed.
 * Suggested products should have common prefix with the searchWord.
 * If there are more than three products with a common prefix return the three lexicographically minimums products.
 *
 * Return list of lists of the suggested products after each character of searchWord is typed.
 *
 * Constraints:
 * 1 <= products.length <= 1000
 * There are no repeated elements in products.
 * 1 <= Σ products[i].length <= 2 * 10^4
 * All characters of products[i] are lower-case English letters.
 * 1 <= searchWord.length <= 1000
 * All characters of searchWord are lower-case English letters.
 *
 */
public class SearchSuggestionsSystem {

	private static class TrieNode {
        TrieNode[] children = new TrieNode[26];
        boolean isEndOfWord = false;
        int childrenCount = 0;

        public TrieNode getTN(int index) {
        	return children[index];
        }
    };

    private static class Trie {
	    public final TrieNode root = new TrieNode();

	    public void insert(String key) {
	        int length = key.length();
	        int index;

	        TrieNode pCrawl = root;

	        for (int level = 0; level < length; level++) {
	            index = key.charAt(level)-'a';
	            if (pCrawl.children[index] == null)
	                pCrawl.children[index] = new TrieNode();
//	            else
//	            	pCrawl.children[index].count++;
	            pCrawl.childrenCount++;
	            pCrawl = pCrawl.children[index];
	        }
	        pCrawl.isEndOfWord = true;
	    }
	}

    static private List<List<String>> suggestedProducts(String[] products, String searchWord) {
    	Trie trie=new Trie();

    	for (String s : products) {
    		trie.insert(s);
    	}

    	TrieNode tn=trie.root;
    	int len=searchWord.length();
    	List<List<String>> res = new ArrayList<>(len);
    	String prefix="";
    	for (int i=0; i<len; i++) {
    		List<String> lst = new ArrayList<>();
    		if (tn==null) {
    			res.add(lst);
    			continue;
    		}
    		char c=searchWord.charAt(i);
    		prefix+=c;
    		int ind=c-'a';
    		tn=tn.getTN(ind);
    		if (tn==null) {
    			res.add(lst);
    			continue;
    		}
        	if (tn.isEndOfWord)
        		lst.add(prefix);
    		get3(tn, lst, prefix);
    		res.add(lst);
    	}
    	return res;
    }

    private static void get3(TrieNode tn, List<String> lst, String prefix) {
    	if (tn.childrenCount<1)
    		return;
    	for (int i=0;i<26;i++) {
        	if (lst.size()==3)
        		return;
    		TrieNode curtn=tn.getTN(i);
    		if (curtn==null)
    			continue;
    		String curPrefix=prefix+(char)('a'+i);
    		if (curtn.isEndOfWord) {
    			lst.add(curPrefix);
    			if (lst.size()==3)
    				return;
    		}
    		get3(curtn, lst, curPrefix);
    	}
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String[] products= {"mobile","mouse","moneypot","monitor","mousepad"};
		String searchWord = "mouse";
		List<List<String>> res = suggestedProducts(products, searchWord);
		for (List<String> l : res ) {
			System.out.println(l);
		}
    }

}
