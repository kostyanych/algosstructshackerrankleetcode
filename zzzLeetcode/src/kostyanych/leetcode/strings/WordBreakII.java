package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Given a non-empty string s and a dictionary wordDict containing a list of non-empty words,
 * add spaces in s to construct a sentence where each word is a valid dictionary word.
 * Return all such possible sentences.
 *
 * Note:
 *
 * The same word in the dictionary may be reused multiple times in the segmentation.
 * You may assume the dictionary does not contain duplicate words.
 *
 */
public class WordBreakII {

	private static List<String> res = new ArrayList<>();
	private static final Trie trie=new Trie();
	private static List<String>[] memo;

	private static List<String> wordBreak(String s, List<String> wordDict) {
		int wlen=wordDict.size();
		memo = new ArrayList[s.length()];
		int len=s.length();
		if (wlen<1 || len<1)
			return res;

		for (String w : wordDict) {
			trie.insert(w);
		}

		return breakString(s, 0, trie.root, new StringBuilder());
   }

	private static List<String> breakString(String s, int index, TrieNode parent, StringBuilder word) {
		int len=s.length();
		if (index>=len)
			return null;
		List<String> res=memo[index];
		if (res==null) {
			res=new ArrayList<String>();
			memo[index] = res;
			while (index<len) {
				char c=s.charAt(index);
				parent=parent.getTN(c-'a');
				if (parent==null)
					break;
				word.append(c);

				if (parent.isEndOfWord) {
					if (index==len-1) {
						res.add(word.toString());
						return res;
					}
					List<String> rest=breakString(s,index+1,trie.root,new StringBuilder());
					if (rest!=null && rest.size()>0) {
						String prefix=word.toString()+(word.length()>0?" ":"");
						for (String str : rest) {
							res.add(prefix+str);
						}
					}
				}
				index++;
			}
		}
		return res;
	}

	static final int ALPHABET_SIZE = 26;

	static class TrieNode {
        TrieNode[] children = new TrieNode[ALPHABET_SIZE];
        boolean isEndOfWord = false;

        public TrieNode getTN(int index) {
        	return children[index];
        }
    };

	static class Trie {
	    public final TrieNode root = new TrieNode();

	    public void insert(String key) {
	        int length = key.length();
	        int index;

	        TrieNode pCrawl = root;

	        for (int level = 0; level < length; level++) {
	            index = key.charAt(level)-'a';
	            if (pCrawl.children[index] == null)
	                pCrawl.children[index] = new TrieNode();
	            pCrawl = pCrawl.children[index];
	        }
	        pCrawl.isEndOfWord = true;
	    }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s = "pineapplepenapple";
		List<String >wordDict = Arrays.asList(new String[] {"apple", "pen", "applepen", "pine", "pineapple"});
		System.out.println(wordBreak(s, wordDict));
	}

}
