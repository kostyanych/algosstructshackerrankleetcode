package kostyanych.leetcode.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * A valid encoding of an array of words is any reference string s and array of indices indices such that:
 * words.length == indices.length
 * The reference string s ends with the '#' character.
 * For each index indices[i], the substring of s starting from indices[i] and up to (but not including) the next '#' character is equal to words[i].
 * Given an array of words, return the length of the shortest reference string s possible of any valid encoding of words.
 *
 * Constraints:
 * 1 <= words.length <= 2000
 * 1 <= words[i].length <= 7
 * words[i] consists of only lowercase letters.
 *
 */
public class ShortEncodingOfWords {

	static private int minimumLengthEncoding(String[] words) {
		TrieNode trie = new TrieNode();
	    Map<TrieNode, Integer> nodes = new HashMap<>();

	    for (int i = 0; i < words.length; ++i) {
	    	String word = words[i];
	        TrieNode cur = trie;
	        for (int j = word.length() - 1; j >= 0; --j) {
	        	cur = cur.get(word.charAt(j));
	        }
	        nodes.put(cur, i);
	    }

        int res = 0;
        for (TrieNode node: nodes.keySet()) {
        	if (node.count == 0)
        		res += words[nodes.get(node)].length() + 1;
        }
	    return res;
    }

	static final int ALPHABET_SIZE = 26;
	static class TrieNode {
	    TrieNode[] children;
	    int count;
	    TrieNode() {
	        children = new TrieNode[ALPHABET_SIZE];
	        count = 0;
	    }
	    public TrieNode get(char c) {
	        if (children[c-'a'] == null) {
	            children[c-'a'] = new TrieNode();
	            count++;
	        }
	        return children[c - 'a'];
	    }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String[] words = {"time", "me", "bell"};
		System.out.println(minimumLengthEncoding(words));
	}
}
