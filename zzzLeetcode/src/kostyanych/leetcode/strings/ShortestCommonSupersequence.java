package kostyanych.leetcode.strings;

/**
 * Given two strings str1 and str2, return the shortest string that has both str1 and str2 as subsequences.
 * If multiple answers exist, you may return any of them.
 *
 * (A string S is a subsequence of string T if deleting some number of characters from T
 * (possibly 0, and the characters are chosen anywhere from T) results in the string S.)
 *
 *
 * 1. find the length of SCS
 *
 */
public class ShortestCommonSupersequence {

	private static String shortestCommonSupersequence(String str1, String str2) {
		int len1=str1.length();
		int len2=str2.length();

        int[][] lenmemo = new int[len1 + 1][len2 + 1];
        for (int i=1;i<=len1;i++) {
        	lenmemo[i][0]=i;
        }
        for (int i=1;i<=len2;i++) {
        	lenmemo[0][i]=i;
        }

        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (str1.charAt(i - 1) == str2.charAt(j - 1))
                	lenmemo[i][j] = 1 + lenmemo[i - 1][j - 1];	//what it was up to those 2 characters + 1, since the 2 are equal
                else
                	lenmemo[i][j] = Math.min(lenmemo[i - 1][j], lenmemo[i][j - 1])+1;
            }
        }

        StringBuilder sb = new StringBuilder(lenmemo[len1][len2]);
        // Start from the bottom right corner and one by one
        // push characters in output string
        int i = len1, j = len2;
        while (i > 0 && j > 0) {
            // If current character in str1 and str2 are same, then
            // current character is part of shortest supersequence
        	char c1=str1.charAt(i - 1);
        	char c2=str2.charAt(j - 1);
        	if (c1 == c2) {
                // Put current character in result
        		sb.insert(0, c1);

                // reduce values of i, j and index
                i--;
                j--;
            }

            // If current character in str1 and str2 are different
            else if (lenmemo[i - 1][j] > lenmemo[i][j - 1]) {
                // Put current character of str2 in result
            	sb.insert(0, c2);
                // reduce values of j and index
                j--;
            }
            else {
                // Put current character of str1 in result
            	sb.insert(0, c1);
                // reduce values of i and index
                i--;
            }
        }

        // If str2 reaches its end, put remaining characters
        // of str1 in the result string
        while (i > 0) {
        	sb.insert(0, str1.charAt(i - 1));
            i--;
        }

        // If str1 reaches its end, put remaining characters
        // of str2 in the result string
        while (j > 0) {
        	sb.insert(0, str2.charAt(j - 1));
            j--;
        }

        // reverse the string and return it
        return sb.toString();
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String text2="arca";
		String text1="brac";
		System.out.println(text1+", "+text2+" -> "+shortestCommonSupersequence(text1,text2));
	}

}
