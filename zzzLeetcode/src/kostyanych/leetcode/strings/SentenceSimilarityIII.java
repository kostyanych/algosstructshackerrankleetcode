package kostyanych.leetcode.strings;

/**
 * A sentence is a list of words that are separated by a single space with no leading or trailing spaces.
 * For example, "Hello World", "HELLO", "hello world hello world" are all sentences. Words consist of only uppercase and lowercase English letters.
 *
 * Two sentences sentence1 and sentence2 are similar if it is possible to insert an arbitrary sentence (possibly empty)
 * inside one of these sentences such that the two sentences become equal.
 * For example, sentence1 = "Hello my name is Jane" and sentence2 = "Hello Jane" can be made equal by inserting "my name is" between "Hello" and "Jane" in sentence2.
 *
 * Given two sentences sentence1 and sentence2, return true if sentence1 and sentence2 are similar. Otherwise, return false.
 *
 * Constraints:
 * 1 <= sentence1.length, sentence2.length <= 100
 * sentence1 and sentence2 consist of lowercase and uppercase English letters and spaces.
 * The words in sentence1 and sentence2 are separated by a single space.
 *
 */
public class SentenceSimilarityIII {

	static private boolean areSentencesSimilar(String sentence1, String sentence2) {
		if (sentence1.equals(sentence2))
			return true;
		String[] arr1=sentence1.split("\\s");
		String[] arr2=sentence2.split("\\s");

		if (arr1.length == arr2.length)
			return false;

		String[] longer = arr1.length > arr2.length ? arr1 : arr2;
		String[] shorter = arr1.length > arr2.length ? arr2 : arr1;

		int firstIndex = findFirstIndex(longer, shorter[0], 0);
		if (firstIndex<0)
			return false;
		if (shorter.length==1) {
			if (firstIndex==longer.length-1 || firstIndex==0)
				return true;
			return false;
		}

		if (firstIndex==0) { //shorter is at the beginning of longer
			int lastIndex = firstIndex;
			for (int i=1;i<shorter.length;i++) {
				if (longer[i].equals(shorter[i]))
					lastIndex=i;
				else
					break;
			}
			if (lastIndex == shorter.length-1) //the whole shorter is at the beginning of longer
				return true;
			int newFirstIndex=findFirstIndex(longer, shorter[lastIndex+1], lastIndex+1);
			return isTail(longer, newFirstIndex, shorter, lastIndex+1);
		}
		else {
			return isTail(longer, firstIndex, shorter, 0);
		}
    }

	static private int findFirstIndex(String[] arr, String str, int startIndex) {
		int firstIndex = -1;
		for (int i=startIndex;i<arr.length;i++) {
			if (arr[i].equals(str)) {
				firstIndex = i;
				break;
			}
		}
		return firstIndex;
	}

	static private boolean isTail(String[] longer, int il, String[] shorter, int is) {
		int llen=longer.length;
		int slen=shorter.length;
		int i=0;
		while (il+i<llen && is+i<slen) {
			if (longer[il+i].equals(shorter[is+i]))
				i++;
			else
				break;
		}
		if (is+i==slen && i+il==llen)
			return true;
		return false;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s1="a b c d e";
		String s2="a e";
		System.out.println(areSentencesSimilar(s1,s2));
		System.out.println(areSentencesSimilar(s2,s1));

	}
}
