package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * You are given a string, s, and a list of words, Words, that are ALL OF THE SAME LENGTH.
 * Find all starting indices of substring(s) in s that is a concatenation of each word in Words
 *     exactly once and without any intervening characters.
 * There can be duplicates in Words
 */
public class SubstringWithConcatenationOfAllWords {

	private static List<Integer> findSubstring(String s, String[] words) {
		int len=words.length;
		int slen = s.length();

		List<Integer> res = new ArrayList<>();

		if (len==0 || slen==0)
				return res;

		int wlen=words[0].length();
		if (wlen * len > slen)
			return res;

		Map<String, Integer> map = new HashMap<>();
		for (String w : words) {
			map.put(w, map.getOrDefault(w,0)+1);
		}

		int maxValid=slen-(wlen*len);
		for (int i=0;i<=maxValid;i++) {
			if (isOk(s, new HashMap<>(map), i, wlen, len))
				res.add(i);
		}
		return res;
	}

	private static boolean isOk(String s, Map<String, Integer> map, int start, int wlen, int wordCount) {
		int slen=s.length();
		while (start<=slen-wlen) {
			String cand=s.substring(start,start+wlen);
			Integer ii=map.get(cand);
			if (ii==null) //no such word
				return false;
			if (ii==0)    //not enough such words in the Words array
				return false;
			wordCount--;
			if (wordCount==0)
				return true;
			map.put(cand, ii-1);
			start+=wlen;
		}
		if (wordCount==0)
			return true;
		return false;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		String s = "wordgoodgoodgoodbestword";
		String[] words = {"word","good","best","word"};
		System.out.println(findSubstring(s, words));
	}
}
