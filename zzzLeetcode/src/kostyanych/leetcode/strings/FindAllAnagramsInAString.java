package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
 *
 * Strings consists of _lowercase English letters_ only and the length of both strings s and p will not be larger than 20,100 (WTF?).
 *
 * The order of output does not matter.
 *
 */
public class FindAllAnagramsInAString {

	/**
	 * We don't really care about anagramming.
	 * We only care for char frequency in the sliding window of the length p.length.
	 * If it's the same as char frequency in p, then we have an anagram in our sliding window
	 * @param s
	 * @param p
	 * @return
	 */
	private static List<Integer> findAnagrams(String s, String p) {
		List<Integer> res = new ArrayList<>();
		int plen = p.length();
		int slen = s.length();
		if (slen==0 || plen==0 ||plen>slen)
			return res;

		int[] pfreq = new int[26];
		int[] wfreq = new int[26];

		char[] pchar = p.toCharArray();
		for (char c : pchar) {
			pfreq[c-'a']++;
		}

		char[] schar = s.toCharArray();
		for (int i=0;i<plen;i++) {
			int sind=schar[i]-'a';
			if (pfreq[sind]>0)
				wfreq[sind]++;
		}
		if (cheqFreq(pfreq,wfreq))
			res.add(0);
		for (int i=plen;i<slen;i++) {
			int sind=schar[i-plen]-'a';
			if (pfreq[sind]>0)
				wfreq[sind]--;
			sind=schar[i]-'a';
			if (pfreq[sind]>0)
				wfreq[sind]++;
			if (cheqFreq(pfreq,wfreq))
				res.add(i-plen+1);
		}
		return res;
    }

	private static boolean cheqFreq(int[] pfreq, int[] wfreq) {
		for (int i=0;i<26;i++) {
			if (pfreq[i]!=wfreq[i])
				return false;
		}
		return true;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="cbaebabacd";
		String p="abc";
		System.out.println(findAnagrams(s, p));
	}


}
