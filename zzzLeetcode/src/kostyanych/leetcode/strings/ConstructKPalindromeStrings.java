package kostyanych.leetcode.strings;

/**
 * Given a string s and an integer k. You should construct k non-empty palindrome strings, which are "subsequences" of s,
 *    using all the characters in s.
 *
 * Example: s = "annabelle", k = 2.  annabelle => "anbna" + "elle" or "anellena" + "b".
 *
 * Return True if you can use all the characters in s to construct k palindrome strings or False otherwise.
 *
 */
public class ConstructKPalindromeStrings {

	private static boolean canConstruct(String s, int k) {
		//1. There must be <=k characters with odd count (can't make a palindrome with more than one odd-counted char)
		//2. s.length must be >= k (so we can at least make an palindrome out of 1 char

		if (s.length()<k)
			return false;

		int[] chars=new int[26];
		for (int i=0;i<s.length();i++) {
			char c=s.charAt(i);
			chars[c-'a']++;
		}
		int odd=0;
		for (int i=0;i<26;i++) {
			if (chars[i]%2!=0)
				odd++;
		}
		if (odd>k)
			return false;

		return true;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		System.out.println("annabelle, 2   =>    "+canConstruct("annabelle",2));
		System.out.println("leetcode, 3   =>    "+canConstruct("leetcode",3));
		System.out.println("yzyzyzyzyzyzyzy, 2   =>    "+canConstruct("yzyzyzyzyzyzyzy",2));

		//System.out.println(checkOverlap(1, 0, 0, 1, -1, 3, 1));
//		System.out.println(aaa());

	}

}
