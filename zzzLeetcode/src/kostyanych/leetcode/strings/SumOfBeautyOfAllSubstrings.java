package kostyanych.leetcode.strings;

import java.util.Arrays;

/**
 * The beauty of a string is the difference in frequencies between the most frequent and least frequent characters.
 * For example, the beauty of "abaacc" is 3 - 1 = 2.
 *
 * Given a string s, return the sum of beauty of all of its substrings.
 *
 * Constraints:
 * 1 <= s.length <= 500
 * s consists of only lowercase English letters.
 *
 */
public class SumOfBeautyOfAllSubstrings {

	static private int res=0;
	static private char[] str;
	static private boolean[][] visited;

	static private int beautySum(String s) {
		int[] freq=new int[26];
		str=s.toCharArray();
		int len=str.length;
		visited = new boolean[len][len];
		for (char c : str) {
			freq[c-'a']++;
		}

        res+=beauty(freq);
        visited[0][len-1]=true;

        int[] ffreq=Arrays.copyOf(freq, 26);
        ffreq[str[0]-'a']--;
        calc(1,len-1,ffreq);
        ffreq=Arrays.copyOf(freq, 26);
        ffreq[str[len-1]-'a']--;
        calc(0,len-2,ffreq);
        return res;
    }

	static private void calc(int l, int r, int[] freq) {
		if (l>=r)
			return;
		if (visited[l][r])
			return;

		if (visited[l][r])
			return;
		res+=beauty(freq);
		visited[l][r]=true;

        int[] ffreq=Arrays.copyOf(freq, 26);
        ffreq[str[l]-'a']--;
        calc(l+1,r,ffreq);
        ffreq=Arrays.copyOf(freq, 26);
        ffreq[str[r]-'a']--;
        calc(l,r-1,ffreq);
	}

	static private int beauty(int[] freq) {
		int min=Integer.MAX_VALUE;
		int max=0;
		for (int f : freq) {
			if (f==0)
				continue;
			min=Math.min(min, f);
			max=Math.max(max, f);
		}
		return max-min;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(beautySum("a"));
	}

}
