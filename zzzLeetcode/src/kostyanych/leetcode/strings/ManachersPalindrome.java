package kostyanych.leetcode.strings;

import java.util.Arrays;

public class ManachersPalindrome {

	/**
	 * No expansion
	 * @param text
	 * @return
	 */
	private static String findLongestPalindromicString1(String text) { 
	    int n = text.length(); 
	    if (n == 0) 
	        return ""; 
	    n = 2*n + 1; //Position count
	    int[] lps = new int[n]; //LPS Length Array 
	    lps[0] = 0; 
	    lps[1] = 1; 
	    int centerPos = 1; //centerPosition  
	    int centerR = 2; //centerRightPosition 
//	    int curR = 2; //centerRightPosition
	    int curL;
//	    int i = 0; //currentRightPosition 
//	    int iMirror; //currentLeftPosition 
	    int diff = -1; 
	    int maxLPSLength = 1; 
	    int maxLPSCenterPosition = 2; 

	    for (int curR = 2; curR < n; curR++) { 
	        //get currentLeftPosition for currentRightPosition i 
	    	curL  = 2*centerPos-curR; 
	        //Reset expand - means no expansion required 
	        boolean expand = false; 
	        diff = centerR - curR; 
	        //If currentRightPosition i is within range to centerRightPosition 
	        if (diff > 0) { 
	            if (lps[curL] < diff) // Case 1 
	                lps[curR] = lps[curL]; 
	            else if (lps[curL] == diff && curR == n-1) // Case 2 
	            	lps[curR] = lps[curL];
	            else if(lps[curL] == diff && curR < n-1)  {// Case 3 
	            	lps[curR] = lps[curL]; 
	            	expand = true;  // expansion required 
	            } 
	            else if(lps[curL] > diff) { // Case 4 
	            	lps[curR] = diff; 
	            	expand = true;  // expansion required 
	            } 
	        } 
	        else { 
	        	lps[curR] = 0; 
	        	expand = true;  // expansion required 
	        } 
	          
	        if (expand) { 
	            //Attempt to expand palindrome centered at currentRightPosition curR 
	            //Here for odd positions, we compare characters and  
	            //if match then increment LPS Length by ONE 
	            //If even position, we just increment LPS by ONE without  
	            //any character comparison 
	        	while (true) {
	        		int rightmost=curR + lps[curR];
	        		int leftmost=curR - lps[curR];
	        		if (rightmost>=n-1) //nowhere to expand to the right
	        			break;
	        		if (leftmost <= 0 ) //nowhere to expand to the left
	        			break;
	        		if ((rightmost+1)%2 == 0)	//expansion with surrogate char (always the same on the other side)
	        			lps[curR]++;
	        		else if (text.charAt( (rightmost+1)/2 )==text.charAt( (leftmost-1)/2) )  //expanded chars are the same!!
	        			lps[curR]++;
	        		else
	        			break; //not a palindrome if expanded
	        	}
	        } 
	  
	        if(maxLPSLength < lps[curR]) { // Track maxLPSLength 
	            maxLPSLength = lps[curR]; 
	            maxLPSCenterPosition = curR; 
	        } 
	  
	        // If palindrome centered at currentRightPosition curR  
	        // expand beyond centerRightPosition centerR, 
	        // adjust centerPosition C based on expanded palindrome. 
	        if (curR + lps[curR] > centerR)  { 
	            centerPos = curR; 
	            centerR = curR + lps[curR]; 
	        } 
	        System.out.println(Arrays.toString(lps));
	    }
	    //if care to know the string itself:
	    int start = (maxLPSCenterPosition - maxLPSLength)/2; 
	    System.out.println("LPS of string "+text);
	    System.out.println("is "+text.substring(start, start+maxLPSLength));
	    System.out.println("Max LPS is "+maxLPSLength);
	    return text.substring(start, start+maxLPSLength);
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		findLongestPalindromicString1("a");
		findLongestPalindromicString1("aa");
		findLongestPalindromicString1("babad");
//		findLongestPalindromicString1("cbbd");
//		findLongestPalindromicString1("babcbabcbaccba");
//		findLongestPalindromicString1("forgeeksskeegfor");
//		findLongestPalindromicString1("abacdfgdcabba");
//		findLongestPalindromicString1("abacdedcaba");
		
	}
}
