package kostyanych.leetcode.strings;

import java.util.Arrays;

/**
 * Given a string s and an integer k, return the length of the longest substring of s such that
 * the frequency of each character in this substring is greater than or equal to k.
 *
 * Constraints:
 *
 * 1 <= s.length <= 10^4
 * s consists of only lowercase English letters.
 * 1 <= k <= 10^5
 */
public class LongestSubstringWithAtLeastKRepeatingCharacters {

	static private int longestSubstring(String s, int k) {
		int len = s.length();
		if (k>len)
			return 0;
		int maxUnique=0;
		int freq[] = new int[26];
		char[] chars = s.toCharArray();
		for(char c: chars){
			int index =c -'a';
			if (freq[index]++ == 0)
				maxUnique++;
		}

		int result = 0;
        for (int currUnique = 1; currUnique <= maxUnique; currUnique++) {
            // reset countMap
            Arrays.fill(freq, 0);
            int start = 0;
            int end = 0;
            int charind = 0;
            int unique = 0;
            int uniquesWithKFreq = 0;
            while (end < len) {
            	if (unique <= currUnique) {// expand the sliding window
            		charind = chars[end] - 'a';
            		if (freq[charind] == 0)
            			unique++;
            		freq[charind]++;
            		if (freq[charind] == k)
            			uniquesWithKFreq++;
            		end++;
            	}
                else {// shrink the sliding window
                	charind = chars[start] - 'a';
                    if (freq[charind] == k)
                    	uniquesWithKFreq--;
                    freq[charind]--;
                    if (freq[charind] == 0)
                    	unique--;
                    start++;
                }
            	if (unique == currUnique && unique == uniquesWithKFreq)
            		result = Math.max(end - start, result);
	        }
        }
        return result;
	}

	static private int longestSubstringSlow(String s, int k) {
		int len = s.length();
		if (k>len)
			return 0;

		int freq[] = new int[26];
		char[] chars = s.toCharArray();
		for(char c: chars){
			int index =c -'a';
			freq[index]++;
		}

		boolean allValid=true;
		int left=0;
		int right=len-1;
		int mid=left;
		for (int i=left; i<=right;i++) {
			mid=i;
			if (freq[chars[i]-'a']<k) {
				allValid=false;
				break;
			}
		}
		if (mid==right) {
			if (allValid)
				return right-left+1;
			return right-left;
		}

		int maxlen=Math.max(longestSubstring(s.substring(left,mid), k),
				longestSubstring(s.substring(mid+1,len), k));
		return maxlen;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("ababbc   "+longestSubstring("ababbc",2));
	}

}
