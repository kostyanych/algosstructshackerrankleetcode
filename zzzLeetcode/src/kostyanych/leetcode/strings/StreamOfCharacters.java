package kostyanych.leetcode.strings;

/**
 * Implement the StreamChecker class as follows:
 *
 * 1. StreamChecker(words): Constructor, init the data structure with the given words.
 * 2. query(letter): returns true if and only if for some k >= 1, the last k characters queried
 * 	(in order from oldest to newest, including this letter just queried) spell one of the words in the given list.
 *
 * Note:
 * 1 <= words.length <= 2000
 * 1 <= words[i].length <= 2000
 * Words will only consist of lowercase English letters.
 * Queries will only consist of lowercase English letters.
 * The number of queries is at most 40000.
 *
 */
public class StreamOfCharacters {

	private ReverseTrie trie = new ReverseTrie();
    private StringBuilder sb=new StringBuilder();

    public StreamOfCharacters(String[] words) {
        for (String w : words) {
			trie.insert(w);
		}
    }

    public boolean query(char letter) {
        TrieNode tn = trie.root;
        sb.append(letter);

		boolean res = false;
        for (int i=sb.length()-1;i>=0;i--) {
            int ind=sb.charAt(i)-'a';
            tn=tn.getTN(ind);
            if (tn==null)
                break;
            if (tn.isEndOfWord) {
                res=true;
                break;
            }
        }
        return res;
    }

    static class TrieNode {
		static final int ALPHABET_SIZE = 26;

		TrieNode[] children = new TrieNode[ALPHABET_SIZE];
		boolean isEndOfWord = false;

		public TrieNode getTN(int index) {
			return children[index];
		}
	};

	static class ReverseTrie {
		public final TrieNode root = new TrieNode();

		public void insert(String key) {
			int length = key.length();
			int index;

			TrieNode pCrawl = root;

			for (int level = 0; level < length; level++) {
				index = key.charAt(length-level-1)-'a';
				if (pCrawl.children[index] == null)
					pCrawl.children[index] = new TrieNode();
				pCrawl = pCrawl.children[index];
			}
			pCrawl.isEndOfWord = true;
		}
	}

/*
	private Trie trie = new Trie();
	private Queue<TrieNode> q = new LinkedList<>();

	public StreamOfCharacters(String[] words) {
		for (String w : words) {
			trie.insert(w);
		}
	}

	public boolean query(char letter) {
		int ind=letter-'a';
		boolean res = false;
		int size=q.size();
		for (int i=0;i<size;i++) {
			TrieNode cur=q.poll();
			TrieNode newCur=cur.getTN(ind);
			if (newCur!=null) {
				q.add(newCur);
				if (newCur.isEndOfWord)
					res=true;
			}
		}
		TrieNode forThis = trie.root.getTN(ind);
		if (forThis!=null) {
			if (forThis.isEndOfWord)
				res=true;
			q.add(forThis);
		}

		return res;
	}

	static class TrieNode {
		static final int ALPHABET_SIZE = 26;

		TrieNode[] children = new TrieNode[ALPHABET_SIZE];
		boolean isEndOfWord = false;

		public TrieNode getTN(int index) {
			return children[index];
		}
	};

	static class Trie {
		public final TrieNode root = new TrieNode();

		public void insert(String key) {
			int length = key.length();
			int index;

			TrieNode pCrawl = root;

			for (int level = 0; level < length; level++) {
				index = key.charAt(level)-'a';
				if (pCrawl.children[index] == null)
					pCrawl.children[index] = new TrieNode();
				pCrawl = pCrawl.children[index];
			}
			pCrawl.isEndOfWord = true;
		}
	}
*/
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		StreamOfCharacters streamChecker = new StreamOfCharacters(new String[] {"cd","f","kl"});
		System.out.println(streamChecker.query('a'));
		System.out.println(streamChecker.query('b'));
		System.out.println(streamChecker.query('c'));
		System.out.println(streamChecker.query('d'));
		System.out.println(streamChecker.query('e'));
		System.out.println(streamChecker.query('f'));
		System.out.println(streamChecker.query('g'));
		System.out.println(streamChecker.query('h'));
		System.out.println(streamChecker.query('i'));
		System.out.println(streamChecker.query('j'));
		System.out.println(streamChecker.query('k'));
		System.out.println(streamChecker.query('l'));

	}
}
