package kostyanych.leetcode.strings;

public class StrToInt {

	final static private int SPACE = 32;
	final static private int MINUS = 45;
	final static private int PLUS = 43;
	final static private int ZERO = 48;

	private static int myAtoi(String str) {
		int len = str.length();
		if (len < 1)
			return 0;
		long buf = 0;
		int sign=1;
		boolean invert = false;
		boolean started = false;

		for (int i = 0; i < len; i++) {
			int c = (int) str.charAt(i);
			if (!started && c == SPACE)
				continue;
			if (!started && c == PLUS) {
				started = true;
				continue;
			}
			if (!started && c == MINUS) {
				started = true;
				invert = true;
				continue;
			}
			if (c < ZERO || c > (ZERO + 9))
				break;
			buf = 10 * buf + sign*(c - ZERO);
			started = true;
			if (buf > 0 && invert) {
				buf = -1 * buf;
				sign=-1;
				invert = false;
			}
			if (buf > Integer.MAX_VALUE) {
				buf = Integer.MAX_VALUE;
				break;
			}
			if (buf < Integer.MIN_VALUE) {
				buf = Integer.MIN_VALUE;
				break;
			}
		}
		return (int) buf;
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		String s="9223372036854775808";
//		System.out.println(s+" -> "+myAtoi(s));
//		s="2147483647";
//		System.out.println(s+" -> "+myAtoi(s));
		String s="-2147483648";
		System.out.println(s+" -> "+myAtoi(s));
	}
}
