package kostyanych.leetcode.strings;

import java.util.Arrays;

/**
 * Given two strings s and t, determine if they are isomorphic.
 *
 * Two strings are isomorphic if the characters in s can be replaced to get t.
 *
 * All occurrences of a character must be replaced with another character while preserving the order of characters.
 * No two characters may map to the same character but a character may map to itself.
 *
 */
public class IsomorphicStrings {

	static private boolean isIsomorphic(String s, String t) {
		int len1=s.length();
        int len2=s.length();
        if (len1!=len2)
            return false;
        if (len1==0)
            return true;
        char[] arr1 = s.toCharArray();
        char[] arr2 = t.toCharArray();
        int[] mpas=new int[256];
        int[] mpat=new int[256];
        Arrays.fill(mpas,-1);
        Arrays.fill(mpat,-1);

        for (int i=0;i<len1;i++) {
            int c1=arr1[i];
            int c2=arr2[i];
            if (mpas[c1]!=-1) {
                c1=mpas[c1];
                if (c1!=c2)
                    return false;
                continue;
            }
            if (mpat[c2]!=c1 && mpat[c2]!=-1)
                return false;
            mpas[c1]=c2;
            mpat[c2]=c1;
        }
        return true;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("foo, bar");
		System.out.println(isIsomorphic("foo","bar"));
	}
}
