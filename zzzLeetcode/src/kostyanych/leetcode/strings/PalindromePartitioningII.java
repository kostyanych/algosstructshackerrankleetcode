package kostyanych.leetcode.strings;

/**
 * Given a string s, partition s such that every substring of the partition is a palindrome.
 * Return the minimum cuts needed for a palindrome partitioning of s.
 * If a string is itself a palindrome, return 0.
 *
 */
public class PalindromePartitioningII {

	private static int minCut(String s) {
		int len = s.length();
		boolean[][] dp = new boolean[len][len]; //shows if s[i,j] is a palindrome
		for (int l=len-1;l>=0;l--) { //be sure to move left point from right to left for simpler isPalindrome checking
			dp[l][l] = true;
			for (int r=l+1;r<len;r++) {
				if (s.charAt(l)==s.charAt(r)) {
					if (r==l+1)
						dp[l][r]=true;
					else
						dp[l][r]=dp[l+1][r-1]; //we couldn't do that if we've been moving left point from left to right
				}
			}
		}

		int[] minPalindromes = new int[len]; //min amount of palindromes in s[i,len-1]
		for (int i=len-1; i>=0; i--) {
			minPalindromes[i] = len-i; //max amount of palindromes at is the 1-based index of i - the case when all palindromes are of len 1
			for (int j=len-1;j>=i;j--) {
				if (dp[i][j]) {
					if (j==len-1) { //the whole substring from i to the end is a palindrome. no need to dig further
						minPalindromes[i] = 1;
						break;
					}
					minPalindromes[i]=Math.min(minPalindromes[i], 1+minPalindromes[j+1]);
				}
			}
		}

		return minPalindromes[0]-1;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="aab";
		System.out.println(minCut(s));

	}

}
