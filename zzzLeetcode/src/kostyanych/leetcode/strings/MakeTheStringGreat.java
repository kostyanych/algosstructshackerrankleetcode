package kostyanych.leetcode.strings;

/**
 * Given a string s of lower and upper case English letters.
 *
 * A good string is a string which doesn't have two adjacent characters s[i] and s[i + 1] where:
 *
 * 0 <= i <= s.length - 2
 * s[i] is a lower-case letter and s[i + 1] is the same letter but in upper-case or vice-versa.
 *
 * To make the string good, you can choose two adjacent characters that make the string bad and remove them.
 * You can keep doing this until the string becomes good.
 *
 * Return the string after making it good. The answer is guaranteed to be unique under the given constraints.
 * Notice that an empty string is also good.
 *
 */
public class MakeTheStringGreat {

	private static String makeGood(String s) {
		int len=s.length();
		if (len<2)
			return s;
		StringBuffer sb=new StringBuffer(s);
	    int c1 = 0;
	    int c2 = 0;
	    int ind=0;
		while(ind<len-1) {
			c1=(int)sb.charAt(ind);
			c2=(int)sb.charAt(ind+1);
			if( (c1==c2-32) || (c2==c1-32) ) {
				sb.deleteCharAt(ind);
				sb.deleteCharAt(ind);
				if(ind!=0)
					ind--;
				len=sb.length();
			}
			else
				ind++;
		}
	    return sb.toString();
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(makeGood("RLlr"));
	}


}
