package kostyanych.leetcode.strings;

/**
 * A string s is nice if, for every letter of the alphabet that s contains, it appears both in uppercase and lowercase.
 * For example, "abABB" is nice because 'A' and 'a' appear, and 'B' and 'b' appear. However, "abA" is not because 'b' appears, but 'B' does not.
 *
 * Given a string s, return the longest substring of s that is nice.
 * If there are multiple, return the substring of the earliest occurrence. If there are none, return an empty string.
 *
 * Constraints:
 *
 * 1 <= s.length <= 100
 * s consists of uppercase and lowercase English letters.
 *
 */
public class LongestNiceSubstring {

	private static int HAVENT_SEEN=0;
    private static int LOW_ONLY=1;
    private static int UP_ONLY=2;
    private static int BOTH=3;

    static private String longestNiceSubstring(String s) {
        int n = s.length();
        if (n < 2)
            return "";
        int[] count=new int[26];
        char[] chars = s.toCharArray();
        //Set<Character> set = new HashSet<>();
        for (char c : chars) {
            if (c>='a' && c<='z') {
            	if (count[c-'a']==BOTH)
            		continue;
                if (count[c-'a']==UP_ONLY)
                    count[c-'a']=BOTH;
                else
                    count[c-'a']=LOW_ONLY;
            }
            else {
            	if (count[c-'A']==BOTH)
            		continue;
            	if (count[c-'A']==LOW_ONLY)
                    count[c-'A']=BOTH;
                else
                    count[c-'A']=UP_ONLY;
            }
        }

        for (int i = 0; i < n; i++) {
            char c = chars[i];
            int ind = c-'A';
            if (c>='a' && c<='z')
                ind = c-'a';
            if (count[ind]==BOTH)
                continue;
            String sub1 = longestNiceSubstring(s.substring(0, i));
            String sub2 = longestNiceSubstring(s.substring(i+1));
            return sub1.length() >= sub2.length() ? sub1 : sub2;
        }
        return s;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(longestNiceSubstring("YazaAay"));
	}

}
