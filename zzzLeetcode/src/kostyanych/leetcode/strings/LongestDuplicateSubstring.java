package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Given a string S, consider all duplicated substrings: (contiguous) substrings of S that occur 2 or more times.  (The occurrences may overlap.)
 *
 * Return any duplicated substring that has the longest possible length.  (If S does not have a duplicated substring, the answer is "".)
 *
 * Note:
 *  2 <= S.length <= 10^5
 *  S consists of lowercase English letters.
 *
 *  Rabin-Karp with polynomial rolling hash.
 *  The trick is to use such a hash function that is easy to recalculate on the window slide (head leaves window, tail enters window)
 *  Also, until we proved that the hash would be unique, we should assume that there could be different substrings with the same hash
 *  So we would have to check the candidates.
 *
 */
public class LongestDuplicateSubstring {

	private static final int BASE = 26;
	private static final int MOD = 19260817;
	private static final Map<Integer, Long> OFFSETS = new HashMap<>();

	private static String res = "";

	public static String longestDupSubstring(String S) {
		int l = 1;
		int r = S.length();
		res = "";

		while (l <= r) {
			int mid = l + (r - l)/2;
			if (check(S,mid))
				l=mid+1;
			else
				r=mid-1;
		}

		return res;
	}

	private static boolean check(String s, int len) {
		Map<Long, List<Integer>> map = new HashMap<>();
		long hash = hashStartString(s,len);
		List<Integer> indices = new ArrayList<>();
		indices.add(0);
		map.put(hash, indices);
		int last=s.length()-len;
		for (int i=1;i<=last;i++) {
			hash = updateHash(hash, len, s.charAt(i-1), s.charAt(i+len-1));
			if (map.containsKey(hash)) {
				indices = map.get(hash);
				if (checkSubsrtings(s, len, i, indices))
					return true;
				indices.add(i);
			}
			else {
				indices = new ArrayList<>();
				indices.add(i);
				map.put(hash, indices);
			}
		}

		return false;
	}

	private static boolean checkSubsrtings(String s, int len, int i, List<Integer> indices) {
		String src=s.substring(i, i+len);
		for (Integer ind : indices) {
			if (src.equals(s.substring(ind, ind+len))) {
				res=src;
				return true;
			}
		}
		return false;
	}

	private static long hashStartString(String s, int len) {
		long hash = 0;
		for(int i = 0; i < len; i++) {
			int num=s.charAt(i)-'a'+1;
			hash = (hash * BASE + num) % MOD;
		}
		return hash;
	}

	private static long updateHash(long hash, int len, char headToRemove, char tailToAdd) {
		int head=headToRemove-'a'+1;
		int tail=tailToAdd-'a'+1;

		long hrem = 1;
		if (OFFSETS.containsKey(len))
			hrem=OFFSETS.get(len);
		else {
			for (int i = 1; i <= len; ++i) {
				hrem = (hrem * BASE) % MOD;
			}
			OFFSETS.put(len, hrem);
		}

		hash = (hash * BASE - head * hrem % MOD + MOD) % MOD;
		hash = (hash + tail)%MOD;
		return hash;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("banana -> "+longestDupSubstring("banana"));
		System.out.println("abcd -> "+longestDupSubstring("abcd"));
		System.out.println("thissimplefunctionworksbutwillresultinstatementbeingexecutedmoreoftenthanothermoresophisticatedrollinghashfunctionssuchsthosediscussedinthenextsection -> "+longestDupSubstring("thissimplefunctionworksbutwillresultinstatementbeingexecutedmoreoftenthanothermoresophisticatedrollinghashfunctionssuchsthosediscussedinthenextsection"));

	}

}
