package kostyanych.leetcode.strings;

import java.util.Arrays;
import java.util.Stack;

/**
 * Given a balanced parentheses string S, compute the score of the string based on the following rule:
 *
 * () has score 1
 * AB has score A + B, where A and B are balanced parentheses strings.
 * (A) has score 2 * A, where A is a balanced parentheses string.
 *
 */
public class ScoreOfParentheses {

	static private int scoreOfParentheses(String S) {

		int res = 0;
		int bal = 0;
        for (int i = 0; i < S.length(); i++) {
            if (S.charAt(i) == '(') {
                bal++;
            }
            else {
                bal--;
                if (S.charAt(i-1) == '(')
                    res += 1 << bal; //2^bal
            }
        }

        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		String s = "(()(()))";
		System.out.println(s);
		System.out.println(scoreOfParentheses(s));

	}
}
