package kostyanych.leetcode.strings;

public class ReverseWordsInAStringIII {

	private static String reverseWordsSlower(String s) {
		if (s == null)
			return s;
		int len = s.length();
		StringBuilder sb = new StringBuilder(len);
		boolean first = true;
		int start = -1;
		for (int i = 0; i < len+1; i++) {
			char c = i>len-1?' ':s.charAt(i);
			if (c != ' ' && start < 0)
				start = i;
			if (c == ' ' && start >= 0) {
				if (!first)
					sb.append(' ');
				else
					first = false;
				for (int j = i - 1; j >= start; j--) {
					sb.append(s.charAt(j));
				}
				start = -1;
			}
		}
		return sb.toString();
	}

	private static String reverseWords(String s) {
		if (s == null)
			return s;
		String[] words = s.split(" ");
		if (words.length < 1)
			return s;
		StringBuilder sb = new StringBuilder(reverse(words[0]));
		for (int i = 1; i < words.length; i++) {
			if (words[i] == null || words[i].length() < 1)
				continue;
			sb.append(" ").append(reverse(words[i]));
		}
		return sb.toString();
	}

	private static String reverse(String s) {
		char[] arr = s.toCharArray();
		int len = arr.length;
		if (len < 2)
			return s;
		int left = 0;
		int right = len - 1;
		while (left < right) {
			char tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
			left++;
			right--;
		}
		return new String(arr);
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="Let's take LeetCode contest";
		System.out.println(s);
		System.out.println(reverseWords(s));
	}
}
