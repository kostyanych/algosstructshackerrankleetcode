package kostyanych.leetcode.strings;

import java.util.ArrayDeque;

public class RemoveAllAdjacentDuplicatesInString {

	static private String removeDuplicates(String s) {
        int len=s.length();
        if (len<2)
            return s;
        char[] cs = s.toCharArray();
        ArrayDeque<Integer> ad = new ArrayDeque<>();
        ad.add(Integer.valueOf((int)cs[0]));
        for (int i=1;i<len;i++) {
        	int cur=cs[i];
            if (ad.isEmpty()) {
            	ad.add(Integer.valueOf(cur));
                continue;
            }
            int prev=ad.getLast();
            if (prev==cur)
            	ad.removeLast();
            else
            	ad.add(Integer.valueOf(cur));
        }
        StringBuilder sb = new StringBuilder(ad.size());
        while (!ad.isEmpty()) {
            sb.append( (char)(ad.removeFirst().intValue()));
        }
        return sb.toString();
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="abbaca";
		System.out.println(s);
		System.out.println(removeDuplicates(s));
	}

}
