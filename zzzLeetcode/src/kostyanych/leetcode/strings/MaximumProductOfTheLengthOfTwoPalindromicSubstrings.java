package kostyanych.leetcode.strings;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

/**
 * You are given a 0-indexed string s and are tasked with finding two non-intersecting palindromic substrings of ODD LENGTH
 * 		such that the product of their lengths is maximized.
 *
 * More formally, you want to choose four integers i, j, k, l
 * 		such that 0 <= i <= j < k <= l < s.length and both the substrings s[i...j] and s[k...l] are palindromes
 * 		and have odd lengths. s[i...j] denotes a substring from index i to index j inclusive.
 *
 * Return the maximum possible product of the lengths of the two non-intersecting palindromic substrings.
 *
 * A palindrome is a string that is the same forward and backward. A substring is a contiguous sequence of characters in a string.
 *
 * Constraints:
 *
 * 	2 <= s.length <= 10^5
 * 	s consists of lowercase English letters.
 *
 * SOLUTION:
 * 1.  for odd-length palindromic substrings just use simple Manacher algorithm. That's what it's for. It's O(n)
 * 1a. we'll have an array like [1,2,1,3,4....]
 * 		The numbers are "how many palindomes are centered around i", not their actual length.
 * 		This also shows how far they can reach :
 * 			i-arr[i]+1 is the left index of longest palindrome centered at i
 *			i+arr[i]-1 is the right index of longest palindrome centered at i
 * 2. Then another part - at every point i in the string we have to know
 * 		what's the most length of pali to the left inclusive and what's the most length of pali to the right exclusive.
 * 		Because we need non-overlapping palindromes
 * 2a. We'll make an array of length of the LAST pali ending on i, going from right to left.
 * 2b. And we similarly go from left to right to calc the length of LONGEST pali ending at i.
 * 2c. Then we'll just multiply 2b for i by 2a for (i+1)
 * 2d. We'll use the queue to track longest reaching pali for 2b and 2c
 */
public class MaximumProductOfTheLengthOfTwoPalindromicSubstrings {

	private static long maxProduct(String s) {
		int len = s.length();

		int[] memo = findOddPalindromesManacher(s);
		//memo actually contains "how many palindomes are centered around i"
		//i-memo[i]+1 is the left index of longest palindrome centered at i
		//i+memo[i]-1 is the right index of longest palindrome centered at i

		//now we'll calculate the max pali len for every i and set the the j=(edge of that pali) to
		//MAX of (its length, whatever we put there before (if it's the edge of a longer pali already))
		//go both ways in on on run
		int[] ltr=new int[len];
		int[] rtl=new int[len];
		ltr[0]=1;
		rtl[len-1]=1;
		for (int i=1;i<len-1;i++) {
			int pnum=memo[i];
			int plen=pnum*2-1;
			ltr[i+pnum-1]=Math.max(ltr[i+pnum-1], plen);
			rtl[i-pnum+1]=Math.max(rtl[i-pnum+1], plen);
		}
//		System.out.println(Arrays.toString(memo));
//		System.out.println(Arrays.toString(ltr));
//		System.out.println(Arrays.toString(rtl));

		//before we only updated the edges of palis
		//now we'll go from ithem backwards to palis' centers and update empty indexes decreasing the length by 2 (7-5-3-1)
		//go both ways in on on run
		for (int i=1;i<len-1;i++) {
			int lind=len-i;
			if (ltr[lind]==0)
				ltr[lind]=1;
			if (ltr[lind]>1)
				ltr[lind-1]=Math.max(ltr[lind-1], ltr[lind]-2);
			if (rtl[i]==0)
				rtl[i]=1;
			if (rtl[i]>1)
				rtl[i+1]=Math.max(rtl[i+1], rtl[i]-2);
		}
//		System.out.println(Arrays.toString(ltr));
//		System.out.println(Arrays.toString(rtl));

		//now just update both arrays to hold running maximum going either way
		for (int i=1;i<len;i++) {
			ltr[i]=Math.max(ltr[i], ltr[i-1]);
			int rind=len-i-1;
			rtl[rind]=Math.max(rtl[rind], rtl[rind+1]);
		}
//		System.out.println(Arrays.toString(ltr));
//		System.out.println(Arrays.toString(rtl));

		//now all we have to do is calculate for every i the max for left inclusive * max for right exclusive
		//and choose max value
		long res=0;
		for (int i=0;i<len-1;i++) {
			res=Math.max(res, ((long)ltr[i])*rtl[i+1]);
		}

		return res;
	}

	private static long maxProductWithQueue(String s) {
		int len = s.length();

		int[] memo = findOddPalindromesManacher(s);
		//memo actually contains "how many palindomes are centered around i"
		//i-memo[i]+1 is the left index of longest palindrome centered at i
		//i+memo[i]-1 is the right index of longest palindrome centered at i

		int[] maxRightIncl=new int[len];
		Queue<int[]> q = new ArrayDeque<>();
		for (int i = len - 1; i >= 0; i--) {
			while (!q.isEmpty()) {
				int[] buf=q.peek();
				if (buf[0]-buf[1] <=i-1)
					break;
				q.remove();
			}
			maxRightIncl[i] = 1;
			if (!q.isEmpty()) {
				int[] buf=q.peek();
				maxRightIncl[i] += (buf[0]-i)*2;
			}
			q.add(new int[] {i, memo[i]});
		}
		// Left-to-right to track max palindrome in [0, i].
		long res=1l;
		long maxFromLeft=0l;
		q.clear();
		for (int i = 0; i < len - 1; i ++) {
			while (!q.isEmpty()) {
				int[] buf=q.peek();
				if (buf[0]+buf[1] >= i + 1)
					break;
				q.remove();
			}
			long curMaxFromLeft=1l;
			if (!q.isEmpty()) {
				int[] buf=q.peek();
				curMaxFromLeft += (i-buf[0])*2;
			}
			maxFromLeft = Math.max(maxFromLeft, curMaxFromLeft);
	        res = Math.max(res, maxFromLeft * maxRightIncl[i + 1]);
	        q.add(new int[] {i, memo[i]});
		}
		return res;
	}

	private static int[] findOddPalindromesManacher(String s) {
		int len=s.length();
		int[] res = new int[len];
		int l=0;
		int r=-1;
		for (int i = 0; i < len; i++) {
		    int palNum = (i > r) ? 1 : Math.min(res[l + r - i], r - i + 1);
		    int lefti=i-palNum;
		    int righti=i+palNum;
		    while (lefti >= 0 && righti < len && s.charAt(lefti) == s.charAt(righti)) {
		    	palNum++;
		    	lefti=i-palNum;
		    	righti=i+palNum;
		    }
		    res[i] = palNum;
		    if (i + palNum - 1 > r) {
		        l = i - palNum + 1; //+1 and -1 on the next line accounts for proper bounds calculation
		        r = i + palNum - 1;
		    }
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		String s="ababbb";
		//String s = "wtbptdhbjqsrwkxccxkwrsqjbhdtpbtw";
		String s = "ggbswiymmlevedhkbdhntnhdbkhdevelmmyiwsbgg";
		//String s = "zaaaxbbby";
		//String s = "aaabaaaba";
		System.out.println(maxProduct(s));

	}
}
