package kostyanych.leetcode.strings;

/**
 * Design a data structure that supports the following two operations:
 *
 * void addWord(word)
 * bool search(word)
 *
 * search(word) can search a literal word or a regular expression string containing only letters a-z or '.'. A '.' means it can represent any one letter.
 *
 * search(".ad") should return true for "bad", "mad", "pad"
 *
 * You may assume that all words are consist of lowercase letters a-z.
 *
 */
public class AddAndSearchWordDataStructure {

	private static final Trie trie=new Trie();

	static class WordDictionary {

	    /** Initialize your data structure here. */
	    public WordDictionary() {

	    }

	    /** Adds a word into the data structure. */
	    public void addWord(String word) {
	    	trie.insert(word);
	    }

	    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
	    public boolean search(String word) {
	    	return searchTrie(trie.root, 0, word);

	    }

		private boolean searchTrie(TrieNode tn, int ind, String word) {
			int len=word.length();
			if (ind>=len)
				return false;

	        char c=word.charAt(ind);
	        if (c != '.') {
	        	tn=tn.getTN(c-'a');
				if (tn==null)
					return false;
				if (ind==len-1)
					return tn.isEndOfWord;
	            return searchTrie(tn, ind + 1, word);
	        }

	        for (TrieNode child : tn.children) {
	        	if (child==null)
	        		continue;
	        	if (ind==len-1) {
	        		if (child.isEndOfWord)
	        			return true;
	        		continue;
	        	}
	            if (searchTrie(child, ind + 1, word))
	                return true;
	        }

	        return false;
	    }

	}


	static final int ALPHABET_SIZE = 26;

	static class TrieNode {
        TrieNode[] children = new TrieNode[ALPHABET_SIZE];
        boolean isEndOfWord = false;

        public TrieNode getTN(int index) {
        	return children[index];
        }
    };

	static class Trie {
	    public final TrieNode root = new TrieNode();

	    public void insert(String key) {
	        int length = key.length();
	        int index;

	        TrieNode pCrawl = root;

	        for (int level = 0; level < length; level++) {
	            index = key.charAt(level)-'a';
	            if (pCrawl.children[index] == null)
	                pCrawl.children[index] = new TrieNode();
	            pCrawl = pCrawl.children[index];
	        }
	        pCrawl.isEndOfWord = true;
	    }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		WordDictionary obj = new WordDictionary();
		obj.addWord("bad");
		obj.addWord("dad");
		obj.addWord("mad");
		System.out.println(obj.search("pad"));
		System.out.println(obj.search("bad"));
		System.out.println(obj.search(".ad"));
		System.out.println(obj.search("b.."));
	}
}
