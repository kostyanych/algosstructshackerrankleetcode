package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Given a 2D board and a list of words from the dictionary, find all words in the board.
 *
 * Each word must be constructed from letters of sequentially adjacent cell,
 * where "adjacent" cells are those horizontally or vertically neighboring.
 *
 * The same letter cell may not be used more than once in a word.
 *
 * Note:
 *    1. All inputs are consist of lowercase letters a-z.
 *    2. The values of words are distinct.
 *
 */
public class WordSearchII {

	private static final Trie trie=new Trie();
	private static boolean[][] visited;
	private static char[][] b;
	private static final Set<String> res = new HashSet<>();

	private static List<String> findWords(char[][] board, String[] words) {
		int wlen=words.length;
		int rows=board.length;
		if (wlen<1 || rows<1)
			return new ArrayList<>();
		int cols=board[0].length;
		if (cols<1)
			return new ArrayList<>();

		for (String w : words) {
			trie.insert(w);
		}

		visited=new boolean[rows][cols];
		b=board;

		for (int r=0;r<rows;r++) {
			for (int c=0;c<cols;c++) {
				visited=new boolean[rows][cols];
				StringBuilder sb = new StringBuilder();
				check(r,c,trie.root, sb);
			}
		}

		return new ArrayList<>(res);
    }

	private static void check(int row, int col, TrieNode parent, StringBuilder sb) {
		if (visited[row][col])
			return;
		visited[row][col]=true;
		int ind = b[row][col]-'a';
		TrieNode tn=parent.getTN(ind);
		if (tn!=null) {
			sb.append(b[row][col]);
			if (tn.isEndOfWord)
				res.add(sb.toString());
			if (row>0)
				check(row-1,col,tn,sb);
			if (col>0)
				check(row,col-1,tn,sb);
			if (row<b.length-1)
				check(row+1,col,tn,sb);
			if (col<b[0].length-1)
				check(row,col+1,tn,sb);
			sb.deleteCharAt(sb.length()-1);
		}
		visited[row][col]=false;
	}

	static final int ALPHABET_SIZE = 26;

	static class TrieNode {
        TrieNode[] children = new TrieNode[ALPHABET_SIZE];
        boolean isEndOfWord = false;

        public TrieNode getTN(int index) {
        	return children[index];
        }
    };

	static class Trie {
	    public final TrieNode root = new TrieNode();

	    public void insert(String key) {
	        int length = key.length();
	        int index;

	        TrieNode pCrawl = root;

	        for (int level = 0; level < length; level++) {
	            index = key.charAt(level)-'a';
	            if (pCrawl.children[index] == null)
	                pCrawl.children[index] = new TrieNode();
	            pCrawl = pCrawl.children[index];
	        }
	        pCrawl.isEndOfWord = true;
	    }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		char[][] board = {
							{'o','a','a','n'},
							{'e','t','a','e'},
							{'i','h','k','r'},
							{'i','f','l','v'}
						};
		String[] words = {"oath","pea","eat","rain"};
//		char[][] board = {{'a','b'},{'a','a'}};
//		String[] words = {"aba","baa","bab","aaab","aaa","aaaa","aaba"};
		System.out.println(findWords(board, words));
	}
}
