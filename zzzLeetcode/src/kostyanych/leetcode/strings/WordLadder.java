package kostyanych.leetcode.strings;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Given two words beginWord and endWord, and a dictionary wordList,
 * return the length of the shortest transformation sequence from beginWord to endWord, such that:
 * 		Only one letter can be changed at a time.
 * 		Each transformed word must exist in the word list.
 *
 * Return 0 if there is no such transformation sequence.
 *
 * Constraints:
 * 	1 <= beginWord.length <= 100
 * 	endWord.length == beginWord.length
 * 	1 <= wordList.length <= 5000
 * 	wordList[i].length == beginWord.length
 * 	beginWord, endWord, and wordList[i] consist of lowercase English letters.
 * 	beginWord != endWord
 * 	All the strings in wordList are unique.
 *
 */
public class WordLadder {

	private static int ladderLength(String beginWord, String endWord, List<String> wordList) {
        int len=wordList.size();
        if (beginWord.equals(endWord))
            return 1;
        boolean contains=false;
        int endWordIndex=-1;
        boolean[] visited = new boolean[len];
        for (int i=0;i<len;i++) {
            String word = wordList.get(i);
            if (word.equals(beginWord))
            	visited[i]=true;
            else if (!contains && word.equals(endWord)) {
                contains=true;
                endWordIndex=i;
            }
        }
        if (!contains)
            return 0;

        Queue<String> q = new LinkedList<>();
        q.add(beginWord);
        int res=1;
        while (!q.isEmpty()) {
            int size=q.size();
            res++;
            for (int qi=0;qi<size;qi++) {
                String w=q.remove();
                for (int i=0;i<len;i++) {
                    if (visited[i])
                        continue;
                    String ww=wordList.get(i);
                    if (wordOk(ww,w)) {
                        if (i==endWordIndex)
                            return res;
                        visited[i]=true;
                        q.add(ww);
                    }
                }
            }
        }
        return 0;
    }

	static private boolean wordOk(String w1, String w2) {
        int len1=w1.length();
        int len2=w2.length();
        if (len1!=len2)
        	return false;
        int diff=0;
        for (int i=0;i<len1;i++) {
        	if (w1.charAt(i)==w2.charAt(i))
        		continue;
        	diff++;
        	if (diff>1)
        		return false;
        }
        if (diff==1)
        	return true;
        return false;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		String b="leet";
		String e="code";
		String[] arr= {"lest","leet","lose","code","lode","robe","lost"};
		List<String> l = Arrays.asList(arr);
		System.out.println(ladderLength(b,e,l));
	}

}
