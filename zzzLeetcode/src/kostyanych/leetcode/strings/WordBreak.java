package kostyanych.leetcode.strings;

import java.util.Arrays;
import java.util.List;

/**
 * Given a non-empty string s and a dictionary wordDict containing a list of non-empty words,
 * determine if s can be segmented into a space-separated sequence of one or more dictionary words.
 *
 * Note:
 *
 *   The same word in the dictionary may be reused multiple times in the segmentation.
 *   You may assume the dictionary does not contain duplicate words.
 *
 */
public class WordBreak {

	static enum MEMO {NA, YES, NO};

	private static final Trie trie=new Trie();
	private static MEMO[] memo;
    private static String str;
    private static int len;

    private static boolean wordBreak(String s, List<String> wordDict) {
        int wlen=wordDict.size();
	    memo = new MEMO[s.length()];
	    Arrays.fill(memo, MEMO.NA);
		str=s;
        len=s.length();
		if (wlen<1 || len<1)
			return false;

		for (String w : wordDict) {
			trie.insert(w);
		}

		breakString(0, trie.root);
        return memo[0]==MEMO.YES;
    }

     private static boolean breakString(int index, TrieNode parent) {
		if (index>=len)
			return false;
        if (memo[index]==MEMO.YES)
            return true;
        if (memo[index]==MEMO.NO)
            return false;

        int originalIndex=index;

        while (index<len) {
		    char c=str.charAt(index);
			parent=parent.getTN(c-'a');
			if (parent==null)
			    break;
			if (parent.isEndOfWord) {
				if (index==len-1) {
                    memo[originalIndex]=MEMO.YES;
                    return true;
				}
				boolean rest=breakString(index+1,trie.root);
				if (rest) {
                    memo[originalIndex]=MEMO.YES;
                    return true;
				}
			}
			index++;
		}
        memo[originalIndex]=MEMO.NO;
		return false;
	}
	static final int ALPHABET_SIZE = 26;

	static class TrieNode {
        TrieNode[] children = new TrieNode[ALPHABET_SIZE];
        boolean isEndOfWord = false;

        public TrieNode getTN(int index) {
        	return children[index];
        }
    };

	static class Trie {
	    public final TrieNode root = new TrieNode();

	    public void insert(String key) {
	        int length = key.length();
	        int index;

	        TrieNode pCrawl = root;

	        for (int level = 0; level < length; level++) {
	            index = key.charAt(level)-'a';
	            if (pCrawl.children[index] == null)
	                pCrawl.children[index] = new TrieNode();
	            pCrawl = pCrawl.children[index];
	        }
	        pCrawl.isEndOfWord = true;
	    }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//String s = "leetcode";

		String s="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab";
		List<String >wordDict = Arrays.asList(new String[] {"a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"});
		System.out.println(wordBreak(s, wordDict));
	}

}
