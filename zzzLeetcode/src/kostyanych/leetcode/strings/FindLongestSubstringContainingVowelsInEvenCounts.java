package kostyanych.leetcode.strings;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Given the string s, return the size of the longest substring containing EACH vowel an even number of times.
 * That is, 'a', 'e', 'i', 'o', and 'u' must appear an even number of times.
 */
public class FindLongestSubstringContainingVowelsInEvenCounts {
	
//	static Character[] sourceArray = {  'a', 'e', 'i', 'o', 'u' };
//	static Set<Character> set = new HashSet<>(Arrays.asList(sourceArray));
	
	private static int findTheLongestSubstring(String s) {
		int len=s.length();
		//mask would hold bits for 'a', 'e', 'i', 'o', 'u'
		//if bit is set, then there is odd count of a vowel, if it's not - even odd
		//the count is running
		int mask=0;	//no vowels at all, all evens
		
		//here we'll keep index of the FIRST occurrence of mask
		//because only if the current mask and the first previous occurence are the same, then we know that all counts are even
		Map<Integer, Integer> firstOccurrence = new HashMap<>();
		
		//for strings with consonants only we will always get mask=0;
		firstOccurrence.put(0, -1);
		
		int res=0;
		
		for (int i=0;i<len;i++) {
			char c=s.charAt(i);
			if (c=='a')
				mask^=1;	//if that's the fist or even A then 0 xor 1 = 1 (odd), if there was an odd then 1 xor 1 = 0 (even now)
			else if (c=='e')
				mask^=2;
			else if (c=='i')
				mask^=4;
			else if (c=='o')
				mask^=8;
			else if (c=='u')
				mask^=16;
			
			firstOccurrence.putIfAbsent(mask, i);
			res = Math.max(res, i - firstOccurrence.get(mask));
		}
		return res;
	}
	
	private static int findTheLongestSubstringBad(String s) {
		int len=s.length();
		int[] vowCount=countVowels(s);
		if (isGood(vowCount))
			return len;
		int max=0;
		int[] rightBuf=Arrays.copyOf(vowCount, 5);
		for (int right=len-1;right>0;right--) {
			if (isGood(rightBuf)) {
				int curlen=right+1;
				if (curlen>max) {
					max=curlen;
				}
			}
			int left=0;
			int[] buf=Arrays.copyOf(rightBuf, 5);
			while (left<right) {
				for (int i=left;i<right;i++) {
					int[] running=Arrays.copyOf(buf, 5);
					char c=s.charAt(i);
					if (c=='a')
						running[0]--;
					else if (c=='e')
						running[1]--;
					else if (c=='i')
						running[2]--;
					else if (c=='o')
						running[3]--;
					else if (c=='u')
						running[4]--;
					else
						continue;
					if (isGood(running)) {
						int curlen=right-i;
						if (curlen>max) {
							max=curlen;
						}
						break;
					}
				}
				char c=s.charAt(left);
				if (c=='a')
					buf[0]--;
				else if (c=='e')
					buf[1]--;
				else if (c=='i')
					buf[2]--;
				else if (c=='o')
					buf[3]--;
				else if (c=='u')
					buf[4]--;
				left++;
			}
			char c=s.charAt(right);
			if (c=='a')
				rightBuf[0]--;
			else if (c=='e')
				rightBuf[1]--;
			else if (c=='i')
				rightBuf[2]--;
			else if (c=='o')
				rightBuf[3]--;
			else if (c=='u')
				rightBuf[4]--;
		}
		return max;
	}
	
	private  static boolean isGood(int[] count) {
		if (count[0]%2==0 
				&& count[1]%2==0
				&& count[2]%2==0
				&& count[3]%2==0
				&& count[4]%2==0)
			return true;
		return false;
	}
	
	private static int[] countVowels(String s) {
		int len=s.length();
		int[] res=new int[5];
		for (int i=0;i<len;i++) {
			char c=s.charAt(i);
			if (c=='a')
				res[0]++;
			else if (c=='e')
				res[1]++;
			else if (c=='i')
				res[2]++;
			else if (c=='o')
				res[3]++;
			else if (c=='u')
				res[4]++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		String str="abeaa";
		System.out.println(str);
		System.out.println(findTheLongestSubstring(str));

//		String str="eleetminicoworoep";
//		System.out.println(str);
//		System.out.println(findTheLongestSubstring(str));
//		str="leetcodeisgreat";
//		System.out.println(str);
//		System.out.println(findTheLongestSubstring(str));
//		str="bcbcbc";
//		System.out.println(str);
//		System.out.println(findTheLongestSubstring(str));
//		str="a";
//		System.out.println(str);
//		System.out.println(findTheLongestSubstring(str));
//		str="";
//		System.out.println(str);
//		System.out.println(findTheLongestSubstring(str));
	}
}
