package kostyanych.leetcode.strings;

/**
 * Given an input string (s) and a pattern (p), implement regular expression
 * matching with support for '.' and '*'.
 *
 * '.' Matches any single character. '*' Matches zero or more of the preceding
 * element. ACTUALLY * doesn't match anything, it just modifies previous p char
 * quantity-wise NOTE that . is ANY character when there's more than on: .*
 * matches "abcdf", not only "aaaaa"
 *
 * The matching should cover the entire input string (not partial).
 *
 * Note:
 *
 * s could be empty and contains only lowercase letters a-z. p could be empty
 * and contains only lowercase letters a-z, and characters like . or *.
 *
 */
public class RegularExpressionMatchingDotStar {

	private static boolean isMatch(String s, String p) {
		int slen = s.length();
		int plen = p.length();
		if (slen == 0 && plen == 0)
			return true;

		boolean[][] tbl = new boolean[slen + 1][plen + 1];
		tbl[0][0] = true;
		// fill line for an empty string
		for (int i = 2; i <= plen; i++) { // * can't come in as the first char
			if (p.charAt(i-1) == '*') // account to this being zero occurrences of previous chars, then we can have true from (i-2) val
				tbl[0][i] = tbl[0][i - 2];
		}

		for (int row = 1; row <= slen; row++) {
			for (int col = 1; col <= plen; col++) {
				char schar = s.charAt(row - 1);
				char pchar = p.charAt(col - 1);

				if (schar == pchar || pchar == '.') {
					// last characters match, so the overall matching equals matching of the
					// string/pattern with those chars removed from string/pattern
					tbl[row][col] = tbl[row - 1][col - 1];
				} else if (pchar == '*') {
					if (tbl[row][col - 2]) {// account to this being zero occurrences of previous chars, then we can have true from (i-2) val
						tbl[row][col] = true;
					} else { // zero occurrence didn't work, see if repeating 1 or more times works
						char pcharPrev = p.charAt(col - 2); // what exactly are we repeating here?
						if (pcharPrev == schar || pcharPrev == '.') {// ok the string's char can be repeated here
							// so if it's repeated, we don't account for it, see if the string without it
							// matches pattern at that point
							tbl[row][col] = tbl[row - 1][col];
						}
					}
				}
			}
		}
		return tbl[slen][plen];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String str="aa";
		String pattern="a";
//		System.out.println(str+"  %   "+pattern+"  -> "+isMatch(str,pattern));
//		str="aa";
//		pattern="a*";
//		System.out.println(str+"  %   "+pattern+"  -> "+isMatch(str,pattern));
//		str="ab";
//		pattern=".*";
//		System.out.println(str+"  %   "+pattern+"  -> "+isMatch(str,pattern));
//		str="aab";
//		pattern="c*a*b";
//		System.out.println(str+"  %   "+pattern+"  -> "+isMatch(str,pattern));
//		str="mississippi";
//		pattern="mis*is*p*.";
//		System.out.println(str+"  %   "+pattern+"  -> "+isMatch(str,pattern));
		str="mississippi";
		pattern="mis*is*ip*.";
		System.out.println(str+"  %   "+pattern+"  -> "+isMatch(str,pattern));

	}

}
