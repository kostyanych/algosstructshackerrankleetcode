package kostyanych.leetcode.strings;

/**
 * Given a string s, remove duplicate letters so that every letter appears once and only once.
 * You must make sure your result is the smallest in lexicographical order among all possible results.
 *
 */
public class RemoveDuplicateLetters {

	static private String removeDuplicateLetters(String s) {
		int[] count = new int[26];
		boolean[] present=new boolean[26];
		char[] chars = s.toCharArray();
		for (char c : chars) {
			count[c-'a']++;
		}

		StringBuilder sb=new StringBuilder(26);

		int lastChar=0;
		for(char c : chars){
			count[c-'a']--;
			if(present[c-'a'])
				continue;
			//if last char war bigger than this one, and there is more like last ones
			//so we can add it later, while removing it now from the end of the string
			while(sb.length()>0 && lastChar>c && count[lastChar-'a']>0) {
				present[lastChar-'a']=false;
				sb.deleteCharAt(sb.length()-1);
				if (sb.length()==0)
					lastChar=0;
				else
					lastChar=sb.charAt(sb.length()-1);
			}

			sb.append(c);
			present[c-'a']=true;
			lastChar=c;
		}
		return sb.toString();
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s= "bcabc";
		System.out.println(s);
		System.out.println(removeDuplicateLetters(s));
	}
}
