package kostyanych.leetcode.strings;

import java.util.Arrays;

/**
 * You are given a list of strings of the same length words and a string target.
 * Your task is to form target using the given words under the following rules:
 *
 * 		target should be formed from left to right.
 * 		To form the ith character (0-indexed) of target, you can choose the kth character of the jth string in words if target[i] = words[j][k].
 * 		Once you use the kth character of the jth string of words, you can no longer use the xth character of any string in words where x <= k.
 * 	In other words, all characters to the left of or at index k become unusuable for every string.
 * Repeat the process until you form the string target.
 * Notice that you can use multiple characters from the same string in words provided the conditions above are met.
 *
 * Return the number of ways to form target from words. Since the answer may be too large, return it modulo 10^9 + 7.
 *
 */
public class NumberOfWaysToFormATargetStringGivenADictionary {

	private static int MOD=1_000_000_007;

	static private int[][] allWordChars;
	static private char[] tchars;
	static private int[][] memo;

	static private int numWaysSlow(String[] words, String target) {
		int wlen=words[0].length();
		char[] tchrs=target.toCharArray();
		int tlen=tchrs.length;
		int ccount[][] = new int[wlen][tlen];
		for (String w : words) {
			for (int i=0;i<wlen;i++) {
				for (int t=0;t<tlen;t++) {
					if (w.charAt(i)==tchrs[t])
						ccount[i][t]++;
				}
			}
		}
		return (int)count(ccount, tchrs, tlen-1, wlen-1);
	}

	static private long count(int[][] ccount, char[] tchrs, int tlast, int endIndex) {
		if (endIndex<tlast)
			return 0;
		long res=0;
		for (int i=endIndex; i>=tlast;i--) {
			int c=ccount[i][tlast];
			if (c!=0) {
				if (tlast==0)
					res=(res+c%MOD)%MOD;
				else
					res=(res+c%MOD*count(ccount,tchrs,tlast-1,i-1))%MOD;
			}
		}
		return res;
	}

    static private int numWays(String[] words, String target) {
        int wlen=words[0].length();
		tchars=target.toCharArray();
		int tlen=tchars.length;
		memo=new int[wlen][tlen];
		for (int i=0;i<wlen;i++) {
			Arrays.fill(memo[i], -1);
		}
		allWordChars=new int[wlen][26];
		for (String s : words) {
			char[] wc=s.toCharArray();
			for (int i=0;i<wlen;i++) {
				allWordChars[i][wc[i]-'a']++;
			}
		}

		return (int)(findFromLast(tlen-1, wlen-1));
    }

    static private long findFromLast(int neededInd, int maxInWord) {
		if (memo[maxInWord][neededInd]>=0)
			return memo[maxInWord][neededInd];
		long res=0;
		for (int i=maxInWord;i>=neededInd;i--) {
			int suchchars=allWordChars[i][tchars[neededInd]-'a'];
			if (suchchars==0)
				continue;
			if (neededInd==0)
				res=(res+suchchars)%MOD;
			else
				res=(res+(suchchars*findFromLast(neededInd-1, i-1))%MOD)%MOD;
		}
		memo[maxInWord][neededInd]=(int)res;
		return res;
	}

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String[] words = {"acca","bbbb","caca"};
		String target = "aba";
		System.out.println(numWays(words,target));
	}

}
