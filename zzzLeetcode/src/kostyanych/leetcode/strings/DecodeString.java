package kostyanych.leetcode.strings;

import java.util.Stack;

/**
 * Given an encoded string, return its decoded string.
 *
 * The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times.
 * Note that k is guaranteed to be a positive integer.
 *
 * You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.
 *
 * Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k.
 * For example, there won't be input like 3a or 2[4].
 *
 * Constraints:
 *   1 <= s.length <= 30
 *   s consists of lowercase English letters, digits, and square brackets '[]'.
 *   s is guaranteed to be a valid input.
 *   All the integers in s are in the range [1, 300].
 *
 */
public class DecodeString {

	private static class Layer {
        private StringBuilder sb = new StringBuilder();
        private int repeat = 0;
        public void process(Layer inner) {
            while (repeat > 0) {
                this.sb.append(inner.sb);
                repeat--;
            }
        }

        public void append(char c) {
        	sb.append(c);
        }

        public String toString() {
        	return sb.toString();
        }
    }

	static private String decodeString(String s) {
		Stack<Layer> st = new Stack<>();
        st.push(new Layer()); //we have at least one layer
        for (char ch : s.toCharArray()) {
            if (ch>='0' && ch<='9') {
            	Layer cur=st.peek();
                int num = cur.repeat;
                num= num*10 + (int)(ch-'0');
                cur.repeat= num;
            }
            else if (ch == '[')
            	st.push(new Layer());
            else if (ch == ']') {
                Layer last = st.pop();
                Layer cur = st.peek();
                cur.process(last);
            }
            else {
            	Layer cur = st.peek();
            	cur.append(ch);
            }
        }
        Layer last = st.pop();
        return last.toString();
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String text1="2[abc]3[cd]ef";
		System.out.println(text1);
		System.out.println(decodeString(text1));
	}

}
