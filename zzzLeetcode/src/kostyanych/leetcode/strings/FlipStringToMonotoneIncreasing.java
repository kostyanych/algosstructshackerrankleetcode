package kostyanych.leetcode.strings;

/**
 * A binary string is monotone increasing if it consists of some number of 0's (possibly none), followed by some number of 1's (also possibly none).
 *
 * You are given a binary string s. You can flip s[i] changing it from 0 to 1 or from 1 to 0.
 *
 * Return the minimum number of flips to make s monotone increasing.
 *
 * Constraints:
 * 	1 <= s.length <= 10^5
 * 	s[i] is either '0' or '1'
 *
 */
public class FlipStringToMonotoneIncreasing {

	private static int minFlipsMonoIncrSlow(String s) {
        int len=s.length();
        if (len==1)
            return 0;
        int[] tozeroes=new int[len+1];
        //tozeroes[0] = s.charAt(0)=='0' ? 0 : 1;
        for (int i=0;i<s.length();i++) {
            if (s.charAt(i)=='1')
                tozeroes[i+1]=tozeroes[i]+1;
            else
                tozeroes[i+1]=tozeroes[i];
        }
        if (tozeroes[len-1]==0 || tozeroes[len-1]==len)
            return 0;

        int res=0;
        int toones=0;
        if (s.charAt(len-1)=='0')
            toones=1;
        res=Math.min(tozeroes[len-1], toones+tozeroes[len-2]);

        for (int i=len-2;i>0;i--) {
            if (s.charAt(i)=='0')
                toones++;
            res=Math.min(res, toones+tozeroes[i-1]);
        }
        return res;
    }

	private static int minFlipsMonoIncr(String s) {
        int len=s.length();
        if (len==1)
            return 0;
        int ones = 0;
        int zeroes = 0;
        int res = 0;
        for (int i=0;i<len;i++) {
        	if (s.charAt(i)=='1')
        		ones++;
        	else {
        		if (ones>0) {//we only care about 0 AFTER 1
        			zeroes++;

        			if (zeroes==ones) { //the cost of flipping 0s so far == the cost of flipping 1s so far
        								//but we're going left to right, so we want 0s to the left of the next c
        								//so we flip all ones to zeroes
        				res+=ones;
        				ones=0;
        				zeroes=0;
        			}
        		}
        	}
        }
        //if we still have some zeroes AFTER some ones left, we should flip them
        res+=zeroes;
        return res;
    }


	private static int minFlipsMonoIncrA(String s) {
        int len=s.length();
        int[] arr = new int[len + 1];
        for (int i = 0; i < len; i++) {
            arr[i+1] = arr[i] + (s.charAt(i) == '1' ? 1 : 0);
        }

        int res = Integer.MAX_VALUE;
        for (int j = 0; j <= len; ++j) {
            res = Math.min(res, arr[j] + len-j-(arr[len]-arr[j]));
        }

        return res;
    }

	public int minFlipsMonoIncrFastA(String s) {
        char[] ch = s.toCharArray();
        int onescount =0;
        int flipcount =0;

        for(int i =0 ; i<s.length() ; i++){
            if(ch[i] == '0'){
                if(onescount ==0)continue;
                flipcount++;
            }
            else{
                onescount++;
            }
            if(flipcount > onescount){
                flipcount = onescount;
            }
        }

        return flipcount;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		String s = "11011";
		System.out.println(s);
		System.out.println(minFlipsMonoIncr(s));

	}

}
