package kostyanych.leetcode.strings;

import kostyanych.leetcode.helpers.ListNode;

/**
 * Implement strStr().
 * 
 * Return the index of the first occurrence of needle in haystack, 
 * or -1 if needle is not part of haystack.
 * 
 * Return 0 if haystack is an empty string
 *
 */
public class StrStr {
	
	private static int strStr(String haystack, String needle) {
        if (needle.equals(""))
            return 0;
        if (haystack.equals(""))
            return -1;
        int possible=-1;
        int hslen=haystack.length();
        int nlen=needle.length();
        for (int i=0;i<hslen-nlen+1;i++) {
            if (haystack.charAt(i)==needle.charAt(0)) {
                possible=i;
                for (int j=1;j<nlen-1;j++) {
                    if (haystack.charAt(i+j)!=needle.charAt(j)) {
                        possible=-1;
                        break;
                    }
                }
                if (possible>=0)
                    return possible;
            }
        }
        return -1;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(strStr("zhopa","pa"));
	}
	
}
