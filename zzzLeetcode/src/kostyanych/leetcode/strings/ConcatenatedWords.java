package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a list of words (without duplicates), please write a program that returns all concatenated words in the given list of words.
 * A concatenated word is defined as a string that is comprised entirely of at least two shorter words in the given array.
 *
 * Note:
 * 	The number of elements of the given array will not exceed 10,000
 * 	The length sum of elements in the given array will not exceed 600,000.
 * 	All the input string will only include lower case letters.
 * 	The returned elements order does not matter.
 */
public class ConcatenatedWords {

	private static Trie trie = new Trie();

	private static List<String> findAllConcatenatedWordsInADict(String[] words) {
		for (String s : words) {
			trie.insert(s);
		}

		List<String> res = new ArrayList<>();
		for (String s : words) {
			if (num(s, 0)>1)
				res.add(s);
		}
		return res;
	}

	private static int num(String s, int startInd) {
		int len=s.length();
		TrieNode parent = trie.root;
		for (int i=startInd;i<len;i++) {
			char c=s.charAt(i);
			parent=parent.getTN(c-'a');
			if (parent==null)
			    return 0;
			if (parent.isEndOfWord) {
				if (i==len-1)
					return 1;
				int nextNum=num(s,i+1);
				if (nextNum>0)
					return nextNum+1;
			}
		}
		return 0;
	}

	static class TrieNode {
		static final int ALPHABET_SIZE = 26;

        TrieNode[] children = new TrieNode[ALPHABET_SIZE];
        boolean isEndOfWord = false;

        public TrieNode getTN(int index) {
        	return children[index];
        }
    };

	static class Trie {
	    public final TrieNode root = new TrieNode();

	    public void insert(String key) {
	        int length = key.length();
	        int index;

	        TrieNode pCrawl = root;

	        for (int level = 0; level < length; level++) {
	            index = key.charAt(level)-'a';
	            if (pCrawl.children[index] == null)
	                pCrawl.children[index] = new TrieNode();
	            pCrawl = pCrawl.children[index];
	        }
	        pCrawl.isEndOfWord = true;
	    }
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//String s = "leetcode";

		String[] arr= {"cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"};
		System.out.println(findAllConcatenatedWordsInADict(arr));
	}
}
