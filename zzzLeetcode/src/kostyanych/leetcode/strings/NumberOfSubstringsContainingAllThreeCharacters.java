package kostyanych.leetcode.strings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Given a string s consisting only of characters a, b and c.
 *
 * Return the number of substrings containing at least one occurrence of all these characters a, b and c.
 */
public class NumberOfSubstringsContainingAllThreeCharacters {

	private static int numberOfSubstrings(String s) {
		return numberOfSubstringsQueue(s);
	}

	private static int numberOfSubstringsQueue(String s) {
		int len = s.length();
		if (len < 3)
			return 0;

		Queue<Integer> as = new LinkedList<>();
		Queue<Integer> bs = new LinkedList<>();
		Queue<Integer> cs = new LinkedList<>();

		for (int i = 0; i < len; i++) {
			if (s.charAt(i) == 'a')
				as.add(i);
			else if (s.charAt(i) == 'b')
				bs.add(i);
			else if (s.charAt(i) == 'c')
				cs.add(i);
		}

		if (as.isEmpty() || bs.isEmpty() || cs.isEmpty())
			return 0;

		int a = as.remove();
		int b = bs.remove();
		int c = cs.remove();
		int min = Math.min(a, Math.min(b, c));
		int max = Math.max(a, Math.max(b, c));
		int delta = len - max;
		int res = delta;

		while (true/*!as.isEmpty() && !bs.isEmpty() && !cs.isEmpty()*/) {
			while (min < max) {
				if (s.charAt(min + 1) == s.charAt(min)) {
					res += delta;
					min++;
				} else
					break;
			}

			if (s.charAt(min) == 'a') {
				a = findNextQ(as, min + 1);
				if (a < 0)
					break;
			} else if (s.charAt(min) == 'b') {
				b = findNextQ(bs, min + 1);
				if (b < 0)
					break;
			} else if (s.charAt(min) == 'c') {
				c = findNextQ(cs, min + 1);
				if (c < 0)
					break;
			}
			min = Math.min(a, Math.min(b, c));
			max = Math.max(a, Math.max(b, c));
			delta = len - max;
			res += delta;
		}
		return res;
	}

	private static int findNextQ(Queue<Integer> q, int start) {
		while (!q.isEmpty()) {
			int i = q.remove();
			if (i >= start)
				return i;
		}
		return -1;
	}

	private static int numberOfSubstringsFast(String s) {
		int len = s.length();
		if (len < 3)
			return 0;
		int a = -1;
		int b = -1;
		int c = -1;

		for (int i = 0; i < len; i++) {
			if (a >= 0 && b >= 0 && c >= 0)
				break;
			if (s.charAt(i) == 'a' && a < 0)
				a = i;
			else if (s.charAt(i) == 'b' && b < 0)
				b = i;
			else if (s.charAt(i) == 'c' && c < 0)
				c = i;
		}
		if (a < 0 || b < 0 || c < 0)
			return 0;
		int min = Math.min(a, Math.min(b, c));
		int max = Math.max(a, Math.max(b, c));
		int res = 0;
		res += (len - max);
		while (true) {
			if (s.charAt(min) == 'a') {
				a = findNext(s, 'a', min + 1);
				if (a < 0)
					break;
			} else if (s.charAt(min) == 'b') {
				b = findNext(s, 'b', min + 1);
				if (b < 0)
					break;
			} else if (s.charAt(min) == 'c') {
				c = findNext(s, 'c', min + 1);
				if (c < 0)
					break;
			}
			min = Math.min(a, Math.min(b, c));
			max = Math.max(a, Math.max(b, c));
			res += (len - max);
		}
		return res;
	}

	private static int findNext(String s, char c, int start) {
		for (int i = start; i < s.length(); i++) {
			if (s.charAt(i) == c)
				return i;
		}
		return -1;
	}

	private static int numberOfSubstringsSlow(String s) {
		int len = s.length();
		if (len < 3)
			return 0;

		List<Integer> as = new ArrayList<>();
		List<Integer> bs = new ArrayList<>();
		List<Integer> cs = new ArrayList<>();

		for (int i = 0; i < len; i++) {
			if (s.charAt(i) == 'a')
				as.add(i);
			else if (s.charAt(i) == 'b')
				bs.add(i);
			else if (s.charAt(i) == 'c')
				cs.add(i);
		}

		if (as.size() < 1 || bs.size() < 1 || cs.size() < 1)
			return 0;

		int res = 0;
		int prevRes = 0;
		int pos = -1;
		while (pos < len - 2) {
			pos++;
			if (pos > 0) {
				if (s.charAt(pos) == s.charAt(pos - 1)) {
					res += prevRes;
					continue;
				}
				prevRes = 0;
			}

			int posNext = -1;
			if (s.charAt(pos) == 'a') {
				int nextb = findFromInd(bs, pos + 1, pos);
				int nextc = findFromInd(cs, pos + 1, pos);
				if (nextb > 0 && nextc > 0)
					posNext = Math.max(nextb, nextc);
				else
					continue;
			} else if (s.charAt(pos) == 'b') {
				int nexta = findFromInd(as, pos + 1, pos);
				int nextc = findFromInd(cs, pos + 1, pos);
				if (nexta > 0 && nextc > 0)
					posNext = Math.max(nexta, nextc);
				else
					continue;
			} else if (s.charAt(pos) == 'c') {
				int nexta = findFromInd(as, pos + 1, pos);
				int nextb = findFromInd(bs, pos + 1, pos);
				if (nexta > 0 && nextb > 0)
					posNext = Math.max(nexta, nextb);
				else
					continue;
			}

			prevRes = (len - posNext);
			res += prevRes;
		}
		return res;
	}

	private static int findFromInd(List<Integer> lst, int from, int deleteFrom) {
		Iterator<Integer> it = lst.iterator();
		while (it.hasNext()) {
			int res = it.next();
			if (deleteFrom != -1 && res <= deleteFrom) {
				it.remove();
				continue;
			}
			return res;
		}
		return -1;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("abcabc");
		System.out.println(numberOfSubstrings("abcabc"));

		System.out.println("aaaaaaabaaaaaaaac");
		System.out.println(numberOfSubstrings("aaaaaaabaaaaaaaac"));

		System.out.println("bbb");
		System.out.println(numberOfSubstrings("bbb"));

		System.out.println("bbba");
		System.out.println(numberOfSubstrings("bba"));

	}


}
