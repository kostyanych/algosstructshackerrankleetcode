package kostyanych.leetcode.strings;

/**
 * Given two strings text1 and text2, return the length of their longest common subsequence.
 *
 * A subsequence of a string is a new string generated from the original string
 *     with some characters(can be none) deleted without changing the relative order of the remaining characters.
 *     (eg, "ace" is a subsequence of "abcde" while "aec" is not).
 *
 * A common subsequence of two strings is a subsequence that is common to both strings.
 *
 * If there is no common subsequence, return 0.
 *
 * Constraints:
 * 1 <= text1.length <= 1000
 * 1 <= text2.length <= 1000
 * The input strings consist of lowercase English characters only.
 *
 */
public class LongestCommonSubsequence {

	private static int longestCommonSubsequence(String text1, String text2) {
		int len1=text1.length();
		int len2=text2.length();

		int[][] mx=new int[len1+1][len2+1];
		for (int i=1;i<len1+1;i++) {
			char c1=text1.charAt(i-1);
			for (int j=1;j<len2+1;j++) {
				char c2=text2.charAt(j-1);
				if (c2==c1)
					mx[i][j]=mx[i-1][j-1]+1;
				else
					mx[i][j]=Math.max(mx[i][j-1],mx[i-1][j]);
			}
		}
		return mx[len1][len2];
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String text1="abcde";
		String text2="ace";
		System.out.println(text1+", "+text2+" -> "+longestCommonSubsequence(text1,text2));
	}
}
