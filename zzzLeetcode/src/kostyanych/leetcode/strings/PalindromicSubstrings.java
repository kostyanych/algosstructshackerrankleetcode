package kostyanych.leetcode.strings;

/**
 * Given a string, your task is to count how many palindromic substrings in this string.
 *
 * The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.
 *
 */
public class PalindromicSubstrings {

	static private int countSubstrings(String s) {
        char[] chars = s.toCharArray();
        int len = chars.length;
        if (len==0)
            return 0;
        if (len==1)
            return 1;
        int res=len;
        for (int i=1;i<len-1;i++) {
            res+=fromCenter(chars,len,i-1,i+1);
        }
        if (chars[1]==chars[0])
            res++;
        if (len!=2 && chars[len-1]==chars[len-2])
            res++;
        for (int i=1;i<len-2;i++) {
            if (chars[i]==chars[i+1]) {
                res++;
                res+=fromCenter(chars,len,i-1,i+2);
            }
        }
        return res;
    }

    static private int fromCenter(char[] cc, int len, int l, int r) {
        int res=0;
        while (l>=0 && r<len) {
            if (cc[l]==cc[r])
                res++;
            else
                break;
            l--;
            r++;
        }
        return res;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(countSubstrings("aaaaa"));
    }

}
