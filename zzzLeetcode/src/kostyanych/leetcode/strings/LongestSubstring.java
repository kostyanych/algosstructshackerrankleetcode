package kostyanych.leetcode.strings;

import java.util.Arrays;

/*
 * Given a string, find the length of the longest substring without repeating characters.
 * Let a character be anything starting from SPACE in the ascii table
 */
public class LongestSubstring {

	private static int lengthOfLongestSubstring(String s) {
        int[] chars=new int[100];
        Arrays.fill(chars,-1);
        int curlen=0;
        int maxlen=0;
        int strstart=0;
        for (int i=0;i<s.length();i++) {
            char c=s.charAt(i);
            int index=(int)c-32;

            if (chars[index]<strstart)
                curlen++;
            else {
                if (maxlen<curlen)
                    maxlen=curlen;
                curlen=i-chars[index];
                strstart=chars[index]+1;
            }
            chars[index]=i;
        }
        if (maxlen<curlen)
            maxlen=curlen;
        return maxlen;

    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("abcabcbb   "+lengthOfLongestSubstring("abcabcbb"));
		System.out.println("bbbbb   "+lengthOfLongestSubstring("bbbbb"));
		System.out.println("' '    "+lengthOfLongestSubstring(" "));
		System.out.println("pwwkew    "+lengthOfLongestSubstring("pwwkew"));
		System.out.println("nfpdmpi    "+lengthOfLongestSubstring("nfpdmpi"));
	}
}
