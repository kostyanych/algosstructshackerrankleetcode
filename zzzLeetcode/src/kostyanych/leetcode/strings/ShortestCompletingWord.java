package kostyanych.leetcode.strings;

import java.util.Arrays;

/**
 * Given a string licensePlate and an array of strings words, find the shortest completing word in words.
 *
 * A completing word is a word that contains all the letters in licensePlate.
 * Ignore numbers and spaces in licensePlate, and treat letters as case insensitive.
 * If a letter appears more than once in licensePlate, then it must appear in the word the same number of times or more.
 *
 * For example, if licensePlate = "aBc 12c", then it contains letters 'a', 'b' (ignoring case), and 'c' twice.
 * Possible completing words are "abccdef", "caaacab", and "cbca".
 *
 * Return the shortest completing word in words. It is guaranteed an answer exists.
 * If there are multiple shortest completing words, return the first one that occurs in words.
 *
 */
public class ShortestCompletingWord {

	static private String shortestCompletingWord(String licensePlate, String[] words) {
        int wlen=words.length;


        licensePlate=licensePlate.toLowerCase();
        int[] pltc = new int[26];
        for (int i=0;i<licensePlate.length();i++) {
            char c = licensePlate.charAt(i);
            if (c<='z' && c>='a')
                pltc[c-'a']++;
        }

        String res=null;
        Arrays.sort(words, (w1,w2) -> w1.length()-w2.length());
        for (int i=0;i<wlen;i++) {
            String w=words[i];
            int[] chars=new int[26];
            char[] chs=w.toCharArray();
            for (char c : chs) {
                chars[c-'a']++;
            }
            boolean isOk=true;
            for (int ci=0;ci<26;ci++) {
            	if (pltc[ci]>chars[ci]) {
            		isOk=false;
            		break;
            	}
            }
            if (isOk) {
            	res=w;
            	break;
            }
        }


        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String[] arr= {"claim","consumer","student","camera","public","never","wonder","simple","thought","use"};
		String plate = "iMSlpe4";
		System.out.println(shortestCompletingWord(plate, arr));
	}
}
