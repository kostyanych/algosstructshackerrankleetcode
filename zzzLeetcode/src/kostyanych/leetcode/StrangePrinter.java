package kostyanych.leetcode;

/**
 * There is a strange printer with the following two special requirements:
 *   1. The printer can only print a sequence of the same character each time.
 *   2. At each turn, the printer can print new characters starting from and ending at any places, and will cover the original existing characters.
 *
 * Given a string consists of lower English letters only, your job is to count the minimum number of turns the printer needed in order to print it.
 *
 * Length of the given string will not exceed 100.
 *
 *
 *
 *
 * cabcbbc
 * check the range:
 * 1. if first and last chars are the same, they could be done in a single move (ccccccc)
 *         so, check the moves needed to get range [first, last-1] (cabcbb)
 * 2. if first and last chars are different, the result will be
 * 		the result for shorter (at any side) range + 1
 *      ie result(cabcb)+1 or result(abcbb)+1
 *      But we're checking the last chars, so we'll use the left range.
 * 3. BUT if we have a character in this range that is the same as the last one
 * 		Then the possible result would be a sum of results for ranges broken at this middle char:
 * 		result(ca)+result(bcb)
 * 4. we just need to get the minimum between 2 and all possible 3s in the range
 *     ie for range abcbcb we'll need to consider aBCBCB and abcBCB (first one will give min result)
 *
 */
public class StrangePrinter {

	static private int[][] memo;
	static private char[] chars;

	private static int strangePrinter(String s) {
		int len = s.length();
		if (len<2)
			return len;

		//optimization 1: merge continuous letters into 1: abbbbbbbasas->abasas
		StringBuilder sb = new StringBuilder();
		char pre=s.charAt(0);
		sb.append(pre);
		for (int i=1;i<len;i++) {
			char buf=s.charAt(i);
			if (buf!=pre ) {
				sb.append(buf);
				pre=buf;
			}
		}
		chars = sb.toString().toCharArray();
		len=chars.length;
		memo = new int[len][len];

		for(int i = 0 ; i < len; ++i) {
            for(int j = 0 ; j < len; ++j) {
                if(i==j)
                    memo[i][j] = 1;
            }
        }

        return calculateForRange(0, len-1);
	}

	private static int calculateForRange(int start, int end) {
        if(memo[start][end] > 0)
            return memo[start][end];

        // if (last character in a range is the same as first character, the solution is the same as for [first, last-1]
        if(chars[start] == chars[end])
            memo[start][end] = calculateForRange(start, end-1);
        else {
			//result for a shorter range +1
            memo[start][end] = calculateForRange(start, end-1) + 1;

            for(int i = start+1; i < end; i++) {
                if(chars[i] == chars[end]) { // same char (as the last one) inside the range!!

					//(solution to print the string starting at index start and ending at i-1) + (solution to print the string starting at index i and ending at end-1)
                    memo[start][end] = Math.min(memo[start][end], calculateForRange(start, i-1) + calculateForRange(i, end-1));
                }
            }
        }

        return memo[start][end];
    }

//		//init moves array with 1,2,3,4,5....
//		int[] moves=new int[len];
//		for (int i=0;i<len;i++) {
//			moves[i]=i+1;
//		}
//		for (int i=0;i<len;i++) {
//			for (int j=i+1;j<len;j++) {
//				if (chars[i]==chars[j]) {
//					moves[j]=Math.min(i+1, moves[j]); //we need i+1 to get from 0-based index to 1-based number of moves
//				}
//			}
//		}
//		Arrays.sort(moves);
//		int res=1;
//		for (int i=1;i<len;i++) {
//			if (moves[i]==moves[i-1])
//				continue;
//			res++;
//		}
//		return res;
//	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="abcbcb";
		System.out.println(s);
		System.out.println(strangePrinter(s));
	}
}
