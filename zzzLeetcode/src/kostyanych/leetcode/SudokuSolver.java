package kostyanych.leetcode;

import kostyanych.leetcode.helpers.Printer;

/**
 * Write a program to solve a Sudoku puzzle by filling the empty cells.
 *
 * A sudoku solution must satisfy all of the following rules:
 *
 *   Each of the digits 1-9 must occur exactly once in each row.
 *   Each of the digits 1-9 must occur exactly once in each column.
 *   Each of the the digits 1-9 must occur exactly once in each of the 9 3x3 sub-boxes of the grid.
 *
 * Empty cells are indicated by the character '.'.
 *
 * Note:
 *
 *  The given board contain only digits 1-9 and the character '.'
 *  You may assume that the given Sudoku puzzle will have a single unique solution.
 *  The given board size is always 9x9.
 *
 */
public class SudokuSolver {

	private final static char EMPTY = '.';

	private static void solveSudoku(char[][] board) {
		int n = board.length;
		solveSudoku(board,n);
	}

	private static boolean solveSudoku(char[][] board, int n) {

		int row = -1;
		int col = -1;
//check board and find 1st empty space (at [row,col])
//if no such place, sudoku is done
		boolean isFilled = true;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (board[i][j] == EMPTY) {
					row = i;
					col = j;
					// we still have some remaining missing values in Sudoku
					isFilled = false;
					break;
				}
			}
			if (!isFilled)
				break;
		}

		// no empty space left, sudoku IS DONE
		if (isFilled)
			return true;

		// else for each-row backtrack
		for (char num = 1; num <= n; num++) {
			char c=(char)('0'+num);
			if (canPutItHere(board, row, col, c)) {
				board[row][col] = c;
				if (solveSudoku(board, n))
					return true;
				else
					board[row][col] = EMPTY; // replace it back to EMPTY
			}
		}
		return false;
	}

	private static boolean canPutItHere(char[][] board, int row, int col, char num) {
		int blen = board.length;

		// check for row-clash
		for (int c = 0; c < blen; c++) {
			// if the number we are trying to place is already present in that row, return false;
			if (board[row][c] == num)
				return false;
		}

		// check for column-clash
		for (int r = 0; r < blen; r++) {
			// if the number we are trying to place is already present in that column, return false;
			if (board[r][col] == num)
				return false;
		}

		// check for box-clash
		int sqrt = (int) Math.sqrt(board.length);
		int boxRowStart = row - row % sqrt;
		int boxColStart = col - col % sqrt;

		for (int r = boxRowStart; r < boxRowStart + sqrt; r++) {
			for (int d = boxColStart; d < boxColStart + sqrt; d++) {
				if (board[r][d] == num)
					return false;
			}
		}

		// if there was no clash, it's ok
		return true;
	}




	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		char[][] board = {
							{'5','3','.','.','7','.','.','.','.'},
							{'6','.','.','1','9','5','.','.','.'},
							{'.','9','8','.','.','.','.','6','.'},
							{'8','.','.','.','6','.','.','.','3'},
							{'4','.','.','8','.','3','.','.','1'},
							{'7','.','.','.','2','.','.','.','6'},
							{'.','6','.','.','.','.','2','8','.'},
							{'.','.','.','4','1','9','.','.','5'},
							{'.','.','.','.','8','.','.','7','9'}
						};
		solveSudoku(board);
		Printer.print2DArray(board, false);
	}
}
