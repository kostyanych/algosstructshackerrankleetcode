package kostyanych.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * On a social network consisting of m users and some friendships between users,
 * two users can communicate with each other if they know a common language.
 *
 * You are given an integer n, an array languages, and an array friendships where:
 *
 * There are n languages numbered 1 through n,
 * languages[i] is the set of languages the i​​​​​​th​​​​ user knows, and
 * friendships[i] = [u​​​​​​i​​​, v​​​​​​i] denotes a friendship between the users u​​​​​​​​​​​i​​​​​ and vi.
 * You can choose one language and teach it to some users so that all friends can communicate with each other.
 * Return the minimum number of users you need to teach.
 *
 * Note that friendships are not transitive, meaning if x is a friend of y and y is a friend of z, this doesn't guarantee that x is a friend of z.
 *
 */
public class MinimumNumberOfPeopleToTeach {

	static private int minimumTeachings(int n, int[][] languages, int[][] friendships) {
        int m=languages.length;

        //fill mapLang <person, set of languages>
        Map<Integer, Set<Integer>> langMap = new HashMap<>();
        for(int i=0;i<m;i++){
            Set<Integer> langs=new HashSet<>();
            langMap.put(i+1, langs);
            for(int l : languages[i]) {
                langs.add(l);
            }
        }

        //make graph <person, set of friends>
        Map<Integer, Set<Integer>> graph = new HashMap<>();
        for (int[] fr : friendships) {
            int fr1=fr[0];
            int fr2=fr[1];
            Set<Integer> set = graph.compute(fr1,(k,v) -> v!=null ? v : new HashSet<>());
            set.add(fr2);
            set = graph.compute(fr2,(k,v) -> v!=null ? v : new HashSet<>());
            set.add(fr1);
        }


        //check if we need to teach someone at all
        //for every person check all of his friends. if there is at least one common lang in the pair, it's ok
        //we need to count how many persons have all of their friends all set
        //collect bad users into set
        Set<Integer> unlearneds = new HashSet<>();
        for (int f1=1;f1<=m;f1++) {
        	boolean ok=true;
        	Set<Integer> curLangs=langMap.get(f1);
        	Set<Integer> curFriends=graph.get(f1);

        	if (curFriends!=null) {
	        	for (Integer f2: curFriends) {
	        		boolean f2ok=false;
	        		for (Integer l : curLangs) {
	        			if (langMap.get(f2).contains(l)) {
	        				f2ok=true;
	        				break;
	        			}
	        		}
	        		if (!f2ok) {
	        			ok=false;
	        			break;
	        		}
	        	}
        	}
        	if (!ok)
        		unlearneds.add(f1);
        }

        if (unlearneds.size()<1)
        	return 0;


        //count how many people don't know every a language (for every language)
        int[] missingLangs = new int[n+1];
        Arrays.fill(missingLangs, 0);
        for (int i=1;i<=n;i++) {
        	for (Integer f1 : unlearneds) {
        		Set<Integer> s1=langMap.get(f1);
        		if (!s1.contains(i))
        			missingLangs[i]++;
        	}
        }

        int res=unlearneds.size();
        for (int i=1;i<=n;i++) {
            res=Math.min(res,missingLangs[i]);
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n=17;
		int[][] langs = {{4,7,2,14,6},{15,13,6,3,2,7,10,8,12,4,9},{16},{10},{10,3},{4,12,8,1,16,5,15,17,13},{4,13,15,8,17,3,6,14,5,10},{11,4,13,8,3,14,5,7,15,6,9,17,2,16,12},{4,14,6},{16,17,9,3,11,14,10,12,1,8,13,4,5,6},{14},{7,14},{17,15,10,3,2,12,16,14,1,7,9,6,4}};
		int[][] frs = {{4,11},{3,5},{7,10},{10,12},{5,7},{4,5},{3,8},{1,5},{1,6},{7,8},{4,12},{2,4},{8,9},{3,10},{4,7},{5,12},{4,9},{1,4},{2,8},{1,2},{3,4},{5,10},{2,7},{1,7},{1,8},{8,10},{1,9},{1,10},{6,7},{3,7},{8,12},{7,9},{9,11},{2,5},{2,3}};
		System.out.println(minimumTeachings(n, langs, frs));
	}

}
