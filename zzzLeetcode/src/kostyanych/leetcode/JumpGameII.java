package kostyanych.leetcode;

public class JumpGameII {
	
	private static int jump(int[] nums) {
		int len = nums.length;
		if (len < 1)
			return 0;
		if (len == 1)
			return 0;
		
		int res=0;
		int curIndex=0;
		int nextIndex=0;
		
		while (nextIndex<len-1) {
			res++;
			int testIndex=0;
			if (curIndex+nums[curIndex]>=len-1)
				break;
			for (int i=curIndex+1;i<=curIndex+nums[curIndex];i++) {
				if (testIndex<=i+nums[i]) {
					testIndex=i+nums[i];
					nextIndex=i;
				}
			}
			curIndex=nextIndex;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[] arr= {2,3,1,1,4};
		int[] arr= {1,2,3};
		System.out.println(jump(arr));
				
	}
}
