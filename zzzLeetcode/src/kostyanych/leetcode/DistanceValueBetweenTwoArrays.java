package kostyanych.leetcode;

/**
 * Given two integer arrays arr1 and arr2, and the integer d, return the distance value between the two arrays.
 *
 * The distance value is defined as the number of elements arr1[i] such that there is not any element arr2[j] where |arr1[i]-arr2[j]| <= d.
 *
 * (not that distance is only accounts for values in arr1 that satisfy the condition!)
 *
 */
public class DistanceValueBetweenTwoArrays {

	private static int findTheDistanceValue(int[] arr1, int[] arr2, int d) {
		//Arrays.sort(arr1);
		//Arrays.sort(arr2);
		int res=0;
		for (int i=0;i<arr1.length;i++) {
			boolean ok=true;
			for (int j=0;j<arr2.length;j++) {
				if (Math.abs(arr1[i]-arr2[j])<=d) {
					ok=false;
					break;
				}
			}
			if (ok)
				res++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr1 = {4,5,8};
		int[] arr2 = {10,9,1,8};
		int d = 2;
		System.out.println(findTheDistanceValue(arr1, arr2, d));
		arr1 = new int[] {1,4,2,3};
		arr2 = new int[] {-4,-3,6,10,20,30};
		d = 3;
		System.out.println(findTheDistanceValue(arr1, arr2, d));
		arr1 = new int[] {2,1,100,3};
		arr2 = new int[] {-5,-2,10,-3,7};
		d = 6;
		System.out.println(findTheDistanceValue(arr1, arr2, d));
	}

}
