package kostyanych.leetcode.graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Given a directed, acyclic graph of N nodes,
 * find all possible paths from node 0 to node N-1, and return them in any order.
 *
 * The graph is given as follows: the nodes are 0, 1, ..., graph.length - 1.
 *   graph[i] is a list of all nodes j for which the edge (i, j) exists.
 *
 *   Note:
 *   The number of nodes in the graph will be in the range [2, 15].
 *   You can print different paths in any order, but you should keep the order of nodes inside one path.
 *
 *   Example:
 *   Input: [[1,2], [3], [3], []]
 *   Output: [[0,1,3],[0,2,3]]
 *   Explanation: The graph looks like this:
 *   0--->1
 *   |    |
 *   v    v
 *   2--->3
 *
 *   There are two paths: 0 -> 1 -> 3 and 0 -> 2 -> 3.
 *
 */
public class AllPathsFromSourceToTarget {

	private List<List<Integer>> res = new ArrayList<>();
	int target;

	public List<List<Integer>> allPathsSourceTarget(int[][] graph) {

		target = graph.length-1;
		LinkedList<Integer> path = new LinkedList<>();
		path.add(0);
		for (int next : graph[0]) {
			dsf(graph, next, path);
		}
		return res;
	}

	private void dsf(int[][] graph, int cur, LinkedList<Integer> curPath) {
		if (cur==target) {
			curPath.add(cur);
			res.add(new ArrayList<>(curPath));
			curPath.removeLast();
			return;
		}
		if (graph[cur].length<1)
			return;
		curPath.add(cur);
		for (int next : graph[cur]) {
			dsf(graph,next,curPath);
		}
		curPath.removeLast();
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n = 5;
		AllPathsFromSourceToTarget obj = new AllPathsFromSourceToTarget();
		int[][] graph = {{1,2},{3},{3},{}};
		System.out.println(obj.allPathsSourceTarget(graph));
	}


}
