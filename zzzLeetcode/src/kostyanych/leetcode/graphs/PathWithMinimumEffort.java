package kostyanych.leetcode.graphs;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * You are a hiker preparing for an upcoming hike.
 * You are given heights, a 2D array of size rows x columns, where heights[row][col] represents the height of cell (row, col).
 * You are situated in the top-left cell, (0, 0), and you hope to travel to the bottom-right cell, (rows-1, columns-1) (i.e., 0-indexed).
 * You can move up, down, left, or right, and you wish to find a route that requires the minimum effort.
 *
 * A route's effort is the maximum absolute difference in heights between two consecutive cells of the route.
 * Return the minimum effort required to travel from the top-left cell to the bottom-right cell.
 *
 * Constraints:
 * 	rows == heights.length
 * 	columns == heights[i].length
 * 	1 <= rows, columns <= 100
 * 	1 <= heights[i][j] <= 10^6
 *
 * Solution: Use Dijkstra algo with priority queue
 */
public class PathWithMinimumEffort {

	static int[][] dirs = new int[][]{{1,0},{-1,0},{0, 1}, {0, -1}};

    static public int minimumEffortPath(int[][] heights) {
        int rows = heights.length;
        int cols = heights[0].length;

        boolean[][] visited = new boolean[rows][cols];

        int[][] dist = new int[rows][cols];
        for(int i = 0; i < rows; i++) {
            Arrays.fill(dist[i], Integer.MAX_VALUE);
        }

        dist[0][0] = 0;

        PriorityQueue<int[]> pq = new PriorityQueue<>((int[] a, int[] b) -> a[2] - b[2]);

        pq.add(new int[]{0, 0, 0});

        while(pq.size() > 0) {
            int[] cell = pq.poll();
            int r = cell[0];
            int c = cell[1];
            int maxdiff = cell[2];

            visited[r][c] = true;

            if (r == rows-1 && c == cols-1)
                return maxdiff;

            for(int[] d: dirs) {
                int newr = r+d[0];
                int newc = c+d[1];
                if(isValid(rows, cols, newr, newc) && !visited[newr][newc]) {
                    int newdiff = Math.abs(heights[r][c] - heights[newr][newc]);
                    int max = Math.max(dist[r][c], newdiff);
                    if(dist[newr][newc] > max && max<=dist[rows-1][cols-1]) {
                        dist[newr][newc] = max;
                        pq.add(new int[]{newr, newc, max});
                    }
                }
            }
        }

        return dist[rows-1][cols-1];
    }

    static public boolean isValid(int rows, int cols, int r, int c) {
        if(r < 0 || r >= rows || c < 0 || c >= cols)
            return false;
        return true;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] h = {{1,2,2},{3,8,2},{5,3,5}};
		System.out.println(minimumEffortPath(h));
	}

}
