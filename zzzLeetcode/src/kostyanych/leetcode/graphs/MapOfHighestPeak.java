package kostyanych.leetcode.graphs;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * You are given an integer matrix isWater of size m x n that represents a map of land and water cells.
 *
 * If isWater[i][j] == 0, cell (i, j) is a land cell.
 * If isWater[i][j] == 1, cell (i, j) is a water cell.
 *
 * You must assign each cell a height in a way that follows these rules:
 *
 * The height of each cell must be non-negative.
 * If the cell is a water cell, its height must be 0.
 *
 * Any two adjacent cells must have an absolute height difference of at most 1.
 * A cell is adjacent to another cell if the former is directly north, east, south, or west of the latter (i.e., their sides are touching).
 *
 * Find an assignment of heights such that the maximum height in the matrix is maximized.
 *
 * Return an integer matrix height of size m x n where height[i][j] is cell (i, j)'s height. If there are multiple solutions, return any of them.
 *
 */
public class MapOfHighestPeak {

	private static final int[] trav = {0, 1, 0, -1, 1, 0, -1, 0};

	static private int[][] highestPeak(int[][] isWater) {
        int rows = isWater.length;
        int cols = isWater[0].length;

        Queue<int[]> q = new LinkedList<>();
        int[][] res = new int[rows][cols];

        for (int r=0;r<rows;r++) {
        	for (int c=0;c<cols;c++) {
        		if (isWater[r][c]!=1)
        			res[r][c]=-1;
        		else
        			q.add(new int[]{r,c});
        	}
        }

        int curLevel=1;
        while (!q.isEmpty() ) {
        	int size=q.size();
        	for (int i=0;i<size;i++) {
        		int[] cell=q.remove();
        		for (int t=0;t<4;t++) {
        			int rr=cell[0]+trav[t*2];
        			int cc=cell[1]+trav[t*2+1];
        			if (rr<0 || rr>=rows || cc<0 || cc>=cols || res[rr][cc]>=0)
        				continue;
        			res[rr][cc]=curLevel;
        			q.add(new int[]{rr,cc});
        		}
        	}
        	curLevel++;
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[][] arr = {{0,1},{0,0}};
		System.out.println(Arrays.deepToString(highestPeak(arr)));

	}

}
