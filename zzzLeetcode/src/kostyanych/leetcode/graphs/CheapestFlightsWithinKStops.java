package kostyanych.leetcode.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * There are n cities connected by m flights. Each flight starts from city u and arrives at v with a price w.
 * Now given all the cities and flights, together with starting city src and the destination dst,
 * 	your task is to find the cheapest price from src to dst with up to k stops.
 * If there is no such route, output -1.
 *
 * Constraints:
 *   The number of nodes n will be in range [1, 100], with nodes labeled from 0 to n - 1.
 *   The size of flights will be in range [0, n * (n - 1) / 2].
 *   The format of each flight will be (src, dst, price).
 *   The price of each flight will be in the range [1, 10000].
 *   k is in the range of [0, n - 1].
 *   There will not be any duplicated flights or self cycles.
 *
 */
public class CheapestFlightsWithinKStops {

	//int[] = {cityTo, cost, stops}
	private static int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
		// create a graph of all flights
        List<int[]>[] graph = new List[n];
        for (int[] flight : flights) {
            int from = flight[0];
            int to = flight[1];
            int price = flight[2];
            if (graph[from] == null)
            	graph[from] = new ArrayList<>();
            graph[from].add(new int[] {to, price, 0});
        }

        int[] minPrices = new int[n];
        Arrays.fill(minPrices, Integer.MAX_VALUE);

        // BFS traverse the graph - starting from the given src
        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[] {src, 0, 0});
        while (!queue.isEmpty()) {
            int[] currStop = queue.poll();
            if (currStop[0] == dst)
                minPrices[dst] = Math.min(minPrices[dst], currStop[1]);
            else {
                if (currStop[2] > K || graph[currStop[0]]==null)
                	continue;

                for (int[] nextStop : graph[currStop[0]]) {
                    int price = currStop[1] + nextStop[1];
                    if (price >= minPrices[nextStop[0]])
                    	continue;
                    minPrices[nextStop[0]] = price;
                    queue.add(new int[] {nextStop[0], price, currStop[2] + 1});
                }
            }
        }

        return minPrices[dst] == Integer.MAX_VALUE ? -1 : minPrices[dst];
	}

	private static Map<Integer, List<int[]>> graph = new HashMap<>();
	private static int minPathCostSoFar=-1;

	private static int findCheapestPriceSlow(int n, int[][] flights, int src, int dst, int K) {
		for (int[] arr : flights) {
			List<int[]> to = graph.computeIfAbsent(arr[0], k -> new ArrayList<int[]>());
			to.add(new int[] { arr[1], arr[2] });
//			to = graph.computeIfAbsent(arr[1], k -> new ArrayList<int[]>());
//			to.add(new int[] { arr[0], arr[2] });
		}

		dfs(new HashSet<Integer>(), src, dst, K+1, 0);
		return minPathCostSoFar;
	}

	private static void dfs(Set<Integer> visited, int start, int dst, int left, int curCost) {
		if (left<0)
			return;
		if (start==dst) {
			if (minPathCostSoFar<0 || minPathCostSoFar>curCost)
				minPathCostSoFar=curCost;
		}

		List<int[]> to = graph.get(start);
		if (to==null)
			return;
		visited.add(start);
		for (int[] trip : to) {
			if (visited.contains(trip[0]))
				continue;
			if (minPathCostSoFar>=0 && curCost+trip[1]>minPathCostSoFar)
				continue;
			dfs(visited, trip[0], dst, left - 1, curCost+trip[1]);
		}
		visited.remove(start);
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n=3;
		int[][] arr = {{0,1,100}, {1,2,100}, {0,2,500}};
		int src=0;
		int dst=2;
		int k=1;


//		int n=5;
//		int[][] arr= {{4,1,1},{1,2,3},{0,3,2},{0,4,10},{3,1,1},{1,4,3}};
//		int src=2;
//		int dst=1;
//		int k=1;
		System.out.println(findCheapestPrice(n, arr, src, dst, k));
	}

}
