package kostyanych.leetcode.graphs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * There is a tree (i.e., a connected, undirected graph that has no cycles) consisting of n nodes numbered from 0 to n - 1 and exactly n - 1 edges.
 * Each node has a value associated with it, and the root of the tree is node 0.
 *
 * To represent this tree, you are given an integer array nums and a 2D array edges.
 * Each nums[i] represents the ith node's value, and each edges[j] = [uj, vj] represents an edge between nodes uj and vj in the tree.
 *
 * Two values x and y are coprime if gcd(x, y) == 1 where gcd(x, y) is the greatest common divisor of x and y.
 *
 * An ancestor of a node i is any other node on the shortest path from node i to the root. A node is not considered an ancestor of itself.
 *
 * Return an array ans of size n, where ans[i] is the closest ancestor to node i such that nums[i] and nums[ans[i]] are coprime,
 * or -1 if there is no such ancestor.
 *
 * Constraints:
 *
 * nums.length == n
 * 1 <= nums[i] <= 50
 * 1 <= n <= 10^5
 * edges.length == n - 1
 * edges[j].length == 2
 * 0 <= uj, vj < n
 * uj != vj
 *
 */
public class TreeOfCoprimes {

	static private int[] getCoprimes(int[] nums, int[][] edges) {
        int len = nums.length;
        int[] res = new int[len];
        res[0] = -1;

        if(len == 1)
            return res;

        // build graph
        Map<Integer, Set<Integer>> graph = new HashMap<>();
        for(int i = 0; i < edges.length; i++){
            int n1 = edges[i][0];
            int n2 = edges[i][1];
            graph.compute(n1, (k, v) -> v==null ? new HashSet<>() : v).add(n2);
            graph.compute(n2, (k, v) -> v==null ? new HashSet<>() : v).add(n1);
        }

        // note parent of each node in tree
        int[] parent = new int[len];
        boolean[] visited = new boolean[len];
        parent[0] = -1;
        visited[0] = true;
        findParents(0, graph, parent, visited);

        //we make Integer so we can have null values for undefined elements
        Integer[][] memo = new Integer[51][len];

        // for each node find its closest ancestor which is co prime
        for(int i = 1; i < nums.length; i++){
            res[i] = getClosestAncestor(i, parent, nums, memo);
        }
        return res;
    }

    static private int getClosestAncestor(int nodeIndex, int[] parent, int[] nums, Integer[][] memo){
        int cur = parent[nodeIndex];
        int nodeVal=nums[nodeIndex];
        int coPrime = -1;
        int needToMemoizeUpTo = -1;
        while(cur != -1){
            if (memo[nodeVal][cur] != null){
                coPrime = memo[nodeVal][cur];
                needToMemoizeUpTo = cur;
                break;
            }

            if(gcd(nodeVal, nums[cur]) == 1){
                coPrime = cur;
                needToMemoizeUpTo = cur;
                break;
            }
            cur = parent[cur];
        }

        // memoize the result
        cur = parent[nodeIndex];
        while(cur != needToMemoizeUpTo){
            memo[nodeVal][cur] = coPrime;
            cur = parent[cur];
        }
        return coPrime;
    }

    static private int gcd(int p, int q) {
        while (q != 0) {
            int temp = q;
            q = p % q;
            p = temp;
        }
        return p;
    }

    static private void findParents(int node, Map<Integer, Set<Integer>> graph, int[] parent, boolean[] visited){
        Set<Integer> adj = graph.get(node);
        for (Integer x : adj){
            if(visited[x] == false){
                parent[x] = node;
                visited[x] = true;
                findParents(x, graph, parent, visited);
            }
        }
    }

/*
	//TLE

	private static class TreeNode {
		int index;
		int val;
		TreeNode parent;

		public TreeNode(int i, int v, TreeNode p) {
			index=i;
			val=v;
			parent=p;
		}
	}

	static private int[] getCoprimes(int[] nums, int[][] edges) {

		int[][] gcds=new int[51][51];
        for (int i=1;i<=50;i++) {
            for (int j=1;j<=i;j++) {
                gcds[i][j]=gcd(i,j);
                gcds[j][i]=gcds[i][j];
            }
        }

		Map<Integer,List<Integer>> graph = new HashMap<>();
		int len = nums.length;
		for (int i=0;i<len;i++) {
			graph.put(i, new ArrayList<>());
		}
		for (int[] e : edges) {
			graph.get(e[0]).add(e[1]);
			graph.get(e[1]).add(e[0]);
		}
		TreeNode tree=new TreeNode(0,nums[0],null);
		TreeNode[] treearray=new TreeNode[len];
		treearray[0]=tree;
		Queue<TreeNode> q= new LinkedList<>();
		q.add(tree);
		while(!q.isEmpty()) {
			int s = q.size();
			for (int i=0;i<s;i++) {
				TreeNode tn=q.remove();
				List<Integer> children=graph.get(tn.index);
				for (Integer child : children) {
					if (tn.parent==null || !child.equals(tn.parent.index)) {
						TreeNode tnc = new TreeNode(child, nums[child], tn);
						treearray[child] = tnc;
						q.add(tnc);
					}
				}
			}
		}
        int[] res = new int[len];
        Arrays.fill(res, -1);
        for (int i=1;i<len;i++) {
        	res[i]=calc(treearray[i], gcds);
        }
        return res;
    }

	static private int calc(TreeNode tn, int[][] gcds) {
		if (tn==null)
			return -2;
		if (tn.parent==null)
			return -1;
		int val=tn.val;
		while (tn.parent!=null) {
			tn=tn.parent;
			if (gcds[val][tn.val]==1)
				return tn.index;
		}
		return -1;
	}

	static private int gcd(int p, int q) {
        int temp;
        while (q != 0) {
            temp = q;
            q = p % q;
            p = temp;
        }
        return p;
    }
*/
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] nums= {5,6,10,2,3,6,15};
		int[][] edges = {{0,1},{0,2},{1,3},{1,4},{2,5},{2,6}};
		System.out.println(Arrays.toString(getCoprimes(nums, edges)));
	}

}
