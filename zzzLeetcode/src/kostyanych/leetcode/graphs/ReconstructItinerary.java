package kostyanych.leetcode.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Given a list of airline tickets represented by pairs of departure and arrival airports [from, to], reconstruct the itinerary in order.
 * All of the tickets belong to a man who departs from JFK. Thus, the itinerary must begin with JFK.
 *
 * Note:
 *  If there are multiple valid itineraries, you should return the itinerary that has the smallest lexical order when read as a single string.
 *  For example, the itinerary ["JFK", "LGA"] has a smaller lexical order than ["JFK", "LGB"].
 *  All airports are represented by three capital letters (IATA code).
 *  You may assume all tickets form at least one valid itinerary.
 *  One must use all the tickets once and only once.
 *
 *
 *  https://en.wikipedia.org/wiki/Eulerian_path
 *
 */
public class ReconstructItinerary {

	private static List<String> findItinerary(List<List<String>> tickets) {
		Map<String, PriorityQueue<String>> graph = new HashMap<>();
		LinkedList<String> res = new LinkedList<>();

		// 1. Build Graph
		for (List<String> ticket : tickets) {
			String from = ticket.get(0);
			String to = ticket.get(1);
			PriorityQueue<String> pq = graph.computeIfAbsent(from, key -> new PriorityQueue<>());
			pq.add(to);
		}

		// 2. Call DFS from JFK
		dfs(graph, res, "JFK");
		return res;
	}

	private static void dfs(Map<String, PriorityQueue<String>> graph, LinkedList<String> res, String from) {
		PriorityQueue<String> arrivals = graph.get(from);
		while (arrivals != null && !arrivals.isEmpty()) {
			dfs(graph, res, arrivals.poll());
		}
		res.addFirst(from);
	}

//	["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		List<List<String>> tickets = new ArrayList<>();
		tickets.add(Arrays.asList(new String[] {"JFK","ATL"}));
		tickets.add(Arrays.asList(new String[] {"JFK","ATL"}));
		tickets.add(Arrays.asList(new String[] {"SFO","JFK"}));
		tickets.add(Arrays.asList(new String[] {"ATL","SFO"}));
		tickets.add(Arrays.asList(new String[] {"JFK","SFO"}));
		tickets.add(Arrays.asList(new String[] {"ATL","BOS"}));
		tickets.add(Arrays.asList(new String[] {"BOS","JFK"}));

		System.out.println(findItinerary(tickets));
	}

}
