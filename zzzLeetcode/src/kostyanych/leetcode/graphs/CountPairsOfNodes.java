package kostyanych.leetcode.graphs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * You are given an undirected graph represented by an integer n, which is the number of nodes, and edges,
 * where edges[i] = [ui, vi] which indicates that there is an undirected edge between ui and vi.
 *
 * You are also given an integer array queries.
 *
 * The answer to the jth query is the number of pairs of nodes (a, b) that satisfy the following conditions:
 * 		a < b
 * 		cnt is strictly greater than queries[j], where cnt is the number of edges incident to a or b.
 *
 * Return an array answers such that answers.length == queries.length and answers[j] is the answer of the jth query.
 *
 * Note that there can be repeated edges.
 *
 * Constraints:
 * 2 <= n <= 2 * 10^4
 * 1 <= edges.length <= 10^5
 * 1 <= ui, vi <= n
 * ui != vi
 * 1 <= queries.length <= 20
 * 0 <= queries[j] < edges.length
 *
 */
public class CountPairsOfNodes {

	static private int[] countPairs(int n, int[][] edges, int[] queries) {
        int[] degree = new int[n];
        Map<Integer, Integer> edgesByNodes = new HashMap<>();
        for (int[] ed : edges) {
            degree[--ed[0]] ++;
            degree[--ed[1]] ++;
            if (ed[0] > ed[1]) {
                int temp = ed[0];
                ed[0] = ed[1];
                ed[1] = temp;
            }
			// store the number of edges for each node pair
            // since n<2*10^4 we can combine both node indexes into one number
            int key=ed[0]*20000 + ed[1];  //2*10^4 * 2*10^4 = 4*10^8 < 2^30, so we're good to go
            edgesByNodes.compute(key, (k,v) -> v==null ? 1 : v+1);
        }

        int[] sorted = degree.clone();
        Arrays.sort(sorted);

        int[] res = new int[queries.length];
        for (int i = 0; i < queries.length; ++i) {
            int ans = 0;
			// using two pointers to find how many potential node pairs
            for (int l = 0, r = sorted.length - 1; l < r;) {
                if (sorted[l] + sorted[r] > queries[i]) {
                    ans += (r - l);
                    r --;
                }
                else
                    l ++;
            }

			// remove all invalid pairs
            for (int key : edgesByNodes.keySet()) {
                int first = key / 20000;
                int second = key % 20000;
                int sum = degree[first] + degree[second];
                if (sum > queries[i] && sum - edgesByNodes.get(key) <= queries[i])
                    ans --;
            }
            res[i] = ans;
        }
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[][] e={{4,5},{1,3},{1,4}};
		int[] q = {0,1,0,0,1,1,0,0,0,0,1,0,0,1,0,1,1,2};
		System.out.println(Arrays.toString(countPairs(5,e,q)));
	}
}
