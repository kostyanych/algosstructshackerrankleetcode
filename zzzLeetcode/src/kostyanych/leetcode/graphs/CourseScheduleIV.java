package kostyanych.leetcode.graphs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * There are a total of n courses you have to take, labeled from 0 to n-1.
 *
 * Some courses may have direct prerequisites, for example, to take course 0 you have first to take course 1, which is expressed as a pair: [1,0]
 *
 * Given the total number of courses n, a list of direct prerequisite pairs and a list of queries pairs.
 *
 * You should answer for each queries[i] whether the course queries[i][0] is a prerequisite of the course queries[i][1] or not.
 *
 * Return a list of boolean, the answers to the given queries.
 *
 * Please note that if course a is a prerequisite of course b and course b is a prerequisite of course c, then, course a is a prerequisite of course c.
 *
 * Constraints:
 *    2 <= n <= 100
 *    0 <= prerequisite.length <= (n * (n - 1) / 2)
 *    0 <= prerequisite[i][0], prerequisite[i][1] < n
 *    prerequisite[i][0] != prerequisite[i][1]
 *    The prerequisites graph has no cycles.
 *    The prerequisites graph has no repeated edges.
 *    1 <= queries.length <= 10^4
 *    queries[i][0] != queries[i][1]
 */
public class CourseScheduleIV {

	static class Vertex {
		public Set<Integer> ownPreqs=new HashSet<>();
		public Set<Integer> incurredPreqs=new HashSet<>();
	}

	private static List<Boolean> checkIfPrerequisite(int n, int[][] prerequisites, int[][] queries) {
        boolean[][] graph = new boolean[n][n];

		int plen=prerequisites.length;

        for (int i = 0; i < plen; i++) {
            graph[prerequisites[i][0]][prerequisites[i][1]] = true;
        }

        for (int k = 0; k < n; k++) {
            for (int i = 0 ; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    graph[i][j] = graph[i][j] || graph[i][k]&& graph[k][j];
                }
            }
        }

        List<Boolean> res = new ArrayList<>();
        for (int[] query : queries) {
            res.add(graph[query[0]][query[1]]);
        }
        return res;
    }

	private static List<Boolean> checkIfPrerequisiteSlow(int n, int[][] prerequisites, int[][] queries) {
		int len=prerequisites.length;
		int qlen=queries.length;
		List<Boolean> res = new ArrayList<>();
		if (len==0) {
			for (int i=0;i<qlen;i++) {
				res.add(false);
			}
			return res;
		}

		Vertex[] arr=new Vertex[n];
		for (int i=0;i<n;i++) {
			arr[i]=new Vertex();
		}
		for (int i=0;i<len;i++) {
			int[] p = prerequisites[i];
			arr[p[0]].ownPreqs.add(p[1]);
		}
		for (int i=0;i<n;i++) {
			Vertex thisV=arr[i];
			Queue<Integer> q = new LinkedList<>();
			for (Integer ii : thisV.ownPreqs) {
				q.add(ii);
			}
			while (!q.isEmpty()) {
				Vertex otherV = arr[q.remove()];
				if (otherV.incurredPreqs.size()>0) {
					thisV.incurredPreqs.addAll(otherV.incurredPreqs);
					thisV.incurredPreqs.addAll(otherV.ownPreqs);
				}
				else {
					for (int ii : otherV.ownPreqs) {
						if (thisV.ownPreqs.contains(ii)
								|| thisV.incurredPreqs.contains(ii))
							continue;
						q.add(ii);
						thisV.incurredPreqs.add(ii);
					}
				}
			}
		}
		for (int i=0;i<qlen;i++) {
			Vertex thisV=arr[queries[i][0]];
			if (thisV.ownPreqs.contains(queries[i][1]) || thisV.incurredPreqs.contains(queries[i][1]))
				res.add(true);
			else
				res.add(false);
		}
		return res;



	}


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n = 5;
		int[][] prerequisites = {{4,3},{4,1},{4,0},{3,2},{3,1},{3,0},{2,1},{2,0},{1,0}};
		int[][] queries = {{1,4},{4,2},{0,1},{4,0},{0,2},{1,3},{0,1}};
		System.out.println(checkIfPrerequisite(n, prerequisites, queries));
	}

}
