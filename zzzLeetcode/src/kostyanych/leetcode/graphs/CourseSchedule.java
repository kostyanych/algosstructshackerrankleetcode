package kostyanych.leetcode.graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * There are a total of numCourses courses you have to take, labeled from 0 to numCourses-1.
 * Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]
 * Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?
 *
 * Constraints:
 *    The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a graph is represented.
 *    You may assume that there are no duplicate edges in the input prerequisites.
 *    1 <= numCourses <= 10^5
 *
 * NOTE: This is kind of a topological sort
 */
public class CourseSchedule {

	private static boolean canFinish(int numCourses, int[][] prerequisites) {

		@SuppressWarnings("unchecked")
		List<Integer>[] edgesFrom = new ArrayList[numCourses];
		for (int i = 0; i < numCourses; i++) {
			edgesFrom[i] = new ArrayList<>();
		}

		int[] dependents = new int[numCourses]; // for counting dependents
		for (int[] preqs : prerequisites) {
			edgesFrom[preqs[0]].add(preqs[1]); // adding directed edge
			dependents[preqs[1]]++;
		}

		// we'll start with those nodes that has no dependents
		Queue<Integer> queue = new LinkedList<>();
		for (int i = 0; i < numCourses; i++) {
			if (dependents[i] == 0)
				queue.add(i);
		}

		// for every N in the q visit all nodes that N depends on
		// decrease their counters
		// if counters goes to 0, the node does not have dependents anymore, add it to
		// the processing queue
		while (!queue.isEmpty()) {
			int N = queue.poll();
			for (int c : edgesFrom[N]) {
				dependents[c]--;
				if (dependents[c] == 0)
					queue.add(c);
			}
			numCourses--;
		}

		// if some nodes still have dependents, they haven't been to the queue
		// we have a cycle somewhere

		if (numCourses == 0)
			return true;

		return false;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n = 2;
		int[][] prerequisites = {{1,0},{0,1}};
		System.out.println(canFinish(n, prerequisites));
	}
}
