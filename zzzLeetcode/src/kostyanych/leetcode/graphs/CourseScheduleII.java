package kostyanych.leetcode.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * There are a total of n courses you have to take, labeled from 0 to n-1.
 *
 * Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]
 * Given the total number of courses and a list of prerequisite pairs, return the ordering of courses you should take to finish all courses.
 *
 * There may be multiple correct orders, you just need to return one of them. If it is impossible to finish all courses, return an empty array.
 *
 * Note:
 *
 *   The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a graph is represented.
 *   You may assume that there are no duplicate edges in the input prerequisites.
 *
 * THIS IS A TOPOLOGICAL SORT AS IT IS
 *
 */
public class CourseScheduleII {


	private static int[] findOrder(int numCourses, int[][] prerequisites) {
		Stack<Integer> order = new Stack<>();

		@SuppressWarnings("unchecked")
		List<Integer>[] edgesTo = new ArrayList[numCourses];
		for (int i = 0; i < numCourses; i++) {
			edgesTo[i] = new ArrayList<>();
		}

		int[] dependents = new int[numCourses]; // for counting dependents
		for (int[] preqs : prerequisites) {
			edgesTo[preqs[0]].add(preqs[1]); // adding directed edge
			dependents[preqs[1]]++;
		}

		// we'll start with those nodes that has no dependents
		Queue<Integer> queue = new LinkedList<>();
		for (int i = 0; i < numCourses; i++) {
			if (dependents[i] == 0)
				queue.add(i);
		}

		int counter=numCourses;
		// for every N in the q visit all nodes that N depends on
		// decrease their counters
		// if counters goes to 0, the node does not have dependents anymore, add it to
		// the processing queue
		while (!queue.isEmpty()) {
			int N = queue.poll();
			order.add(N);
			for (int c : edgesTo[N]) {
				dependents[c]--;
				if (dependents[c] == 0)
					queue.add(c);
			}
			counter--;
		}

		if (counter!=0)
			return new int[0];

		int[] res = new int[numCourses];
		for (int i=0;i<numCourses;i++) {
			res[i]=order.pop();
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n=4;
		int[][] arr = {{1,0},{2,0},{3,1},{3,2}};
		System.out.println(Arrays.toString(findOrder(n, arr)));
	}

}
