package kostyanych.leetcode;

public class NumberOfStepsToReduceToZero {
	private static int numberOfSteps (int num) {
        if (num==0)
            return 0;
        int res=1;
        while (num>1) {
            if (num%2==0)
                num=num/2;
            else
                num--;
            res++;
        }
        return res;
    }
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int num=14;
		System.out.println(""+num+" -> "+numberOfSteps(num));
		num=8;
		System.out.println(""+num+" -> "+numberOfSteps(num));
		num=123;
		System.out.println(""+num+" -> "+numberOfSteps(num));
		num=0;
		System.out.println(""+num+" -> "+numberOfSteps(num));
		num=1;
		System.out.println(""+num+" -> "+numberOfSteps(num));
		num=2;
		System.out.println(""+num+" -> "+numberOfSteps(num));
	}
}
