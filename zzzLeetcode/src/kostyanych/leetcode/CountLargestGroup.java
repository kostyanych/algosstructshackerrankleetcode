package kostyanych.leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Given an integer n. Each number from 1 to n is grouped according to the sum of its digits.
 *
 *  Return how many groups have the largest size.
 */
public class CountLargestGroup {

	private static int countLargestGroup(int n) {
		if (n<10)
			return n;
		Map<Integer,AtomicInteger> map = new HashMap<>();
		int max=0;
		for (int i=0;i<10;i++) {
			map.put(i, new AtomicInteger(1));
		}
		for (int i=10;i<=n;i++) {
			int sum=calc(i);
			AtomicInteger ai=map.computeIfAbsent(sum, (k)->new AtomicInteger(0));
			int cur=ai.incrementAndGet();
			if (max<cur)
				max=cur;
		}
		int res=0;
		for (AtomicInteger ai : map.values()) {
			if (ai.get()==max)
				res++;
		}
		return res;
    }

	private static int calc(int n) {
		int res=0;
		while (n>0) {
			res+=(n%10);
			n/=10;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(countLargestGroup(2));

	}

}
