package kostyanych.leetcode.trees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree, return the vertical order traversal of its nodes values.
 *
 * For each node at position (X, Y), its left and right children respectively will be at positions (X-1, Y-1) and (X+1, Y-1).
 *
 * Running a vertical line from X = -infinity to X = +infinity,
 *   whenever the vertical line touches some nodes, we report the values of the nodes in order from top to bottom (decreasing Y coordinates).
 *
 * If two nodes have the same position, then the value of the node that is reported first is the value that is smaller.
 *
 * Return an list of non-empty reports in order of X coordinate.  Every report will have a list of values of nodes.
 *
 * Note:
 *   The tree will have between 1 and 1000 nodes.
 *   Each node's value will be between 0 and 1000.
 *
 * Example:
 *
 *        3
 *       / \
 *      9   20
 *         /  \
 *        15   7
 *
 * Output: [[9],[3,15],[20],[7]]
 *
 */
public class VerticalOrderTraversalOfBinaryTree {

	static class TreeNodeWrap {
		final TreeNode tn;
		final int x;
		final int y;

		public TreeNodeWrap(TreeNode tn, int x, int y) {
			this.tn=tn;
			this.x=x;
			this.y=y;
		}
	}

	static class Comp implements Comparator<TreeNodeWrap> {
		@Override
		public int compare(TreeNodeWrap t1, TreeNodeWrap t2) {
			if (t1.y==t2.y)
				return t1.tn.val-t2.tn.val;
			return t1.y-t2.y;
		}
	}

	private static List<List<Integer>> verticalTraversal(TreeNode root) {

		Map<Integer, List<TreeNodeWrap>> map = new HashMap<>();
		Comp cc=new Comp();

        Queue<TreeNodeWrap> q = new LinkedList<>();
        q.add(new TreeNodeWrap(root,0, 0));

        int minX=0;
        int maxX=0;
        while(!q.isEmpty()) {
        	TreeNodeWrap wrap = q.remove();
        	List<TreeNodeWrap> lst=map.computeIfAbsent(wrap.x, k->new ArrayList<>());
        	lst.add(wrap);

            if(wrap.tn.left != null) {
            	int x=wrap.x-1;
            	minX=Math.min(minX, x);
            	maxX=Math.max(maxX, x);
            	q.add(new TreeNodeWrap(wrap.tn.left,x, wrap.y+1));
            }
            if(wrap.tn.right != null) {
            	int x=wrap.x+1;
            	minX=Math.min(minX, x);
            	maxX=Math.max(maxX, x);
            	q.add(new TreeNodeWrap(wrap.tn.right,x, wrap.y+1));
            }
        }

        List<List<Integer>> res = new ArrayList<>();
        for (int i=minX;i<=maxX;i++) {
        	List<TreeNodeWrap> lst = map.get(i);
        	if (lst!=null) {
        		Collections.sort(lst,cc);
        		List<Integer> l = new ArrayList(lst.size());
        		for (TreeNodeWrap w : lst)
        			l.add(w.tn.val);
        		res.add(l);
        	}
        }

        return res;
    }


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String t = "[3,9,20,null,null,15,7]";
		TreeNode root=TreeNode.fromLeetcodeString(t);
		List<List<Integer>> ret = verticalTraversal(root);
		for (List<Integer> lst : ret) {
			System.out.print(lst);
			System.out.print(", ");
		}
		System.out.println();
	}


}
