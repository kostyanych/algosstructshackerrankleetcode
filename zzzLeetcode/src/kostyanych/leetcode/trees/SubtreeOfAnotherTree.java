package kostyanych.leetcode.trees;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given two non-empty binary trees s and t, check whether tree t has exactly the same structure and node values with a subtree of s.
 * A subtree of s is a tree consists of a node in s and all of this node's descendants.
 * The tree s could also be considered as a subtree of itself.
 *
 */
public class SubtreeOfAnotherTree {

	private static boolean isSubtree(TreeNode s, TreeNode t) {
		if (s==null && t==null)
			return true;
		if (s==null || t==null)
			return false;

		 return checkFromNodes(s,t);
	}

	private static boolean compareFull(TreeNode s,TreeNode t) {
        if(s==null && t==null)
            return true;
        if(s==null || t==null)
            return false;
        if(s.val!=t.val)
            return false;
        return compareFull(s.left,t.left) && compareFull(s.right,t.right);
    }

	private static boolean checkFromNodes(TreeNode s,TreeNode t) {
		if (s==null)
			return false;

		//if the subtrees are the same from given nodes
		if (compareFull(s,t))
			return true;

		//if left or right subtree of s is what we need
		return checkFromNodes(s.left,t) || checkFromNodes(s.right,t);
	}


	public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 String sroot = "[3,4,5,1,null,2]";
		 String troot = "[3,1,2]";
		 TreeNode s = TreeNode.fromLeetcodeString(sroot);
		 TreeNode t = TreeNode.fromLeetcodeString(troot);
		 System.out.println(isSubtree(s,t));
	 }
}
