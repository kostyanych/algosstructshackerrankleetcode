package kostyanych.leetcode.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Use Leetcode serialization format
 * [1,2,3,null,null,4,5] is
 *       1
 *     /   \
 *   2       3
 *          / \
 *         4   5
 *
 * [] is empty tree (null)
 *
 *
 * [5,4,7,3,null,2,null,-1,null,9] is
 *
 *              5
 *           /    \
 *         4       7
 *        /       /
 *      3        2
 *     /        /
 *   -1        9
 *
 *
 */
public class SerializeAndDeserializeBinaryTree {

	private final static String NULL="null";

	public static String serialize(TreeNode root) {
		if (root==null)
			return "[]";
		List<String> lst=new ArrayList<>();
		Queue<TreeNode> q = new LinkedList<>();
		q.add(root);
		while (!q.isEmpty()) {
			TreeNode tn=q.remove();
			if (tn==null)
				lst.add(NULL);
			else {
				lst.add(Integer.toString(tn.val));
				q.add(tn.left);
				q.add(tn.right);
			}
		}
		//remove trailing nulls
		for (int i=lst.size()-1;i>=0;i--) {
			if (!NULL.equals(lst.get(i)))
				break;
			lst.remove(i);
		}
		StringBuilder sb=new StringBuilder();
		int len=lst.size();
		if (len>0)
			sb.append(lst.get(0));
		for (int i=1;i<len;i++)
			sb.append(",").append(lst.get(i));
		return "["+sb.toString()+"]";
	}

	// Decodes your encoded data to tree.
	public static TreeNode deserialize(String data) {
		if (data==null)
			return null;
		data=data.trim();
		int len=data.length();
		if (len<2)
			return null;
		if (data.charAt(0)!='[' && data.charAt(len-1)!=']')
			return null;
		data=data.substring(1,len-1);
		String[] snodes=data.split(",");
		len = snodes.length;

		TreeNode root = null;
		try {
			root = new TreeNode(getIntFromStr(snodes[0]));
			Queue<TreeNode> q = new LinkedList<>();
			q.add(root);
			for (int i=1;i<len;i++) {
				String sleft=snodes[i];
				i++;
				String sright=null;
				if (i<len)
					sright=snodes[i];
				if (sleft!=null && NULL.equals(sleft))
					sleft=null;
				if (sright!=null && NULL.equals(sright))
					sright=null;
				TreeNode tn=q.remove();
				if (sleft!=null) {
					TreeNode left=new TreeNode(getIntFromStr(sleft));
					tn.left=left;
					q.add(left);
				}
				if (sright!=null) {
					TreeNode right=new TreeNode(getIntFromStr(sright));
					tn.right=right;
					q.add(right);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return root;
	}

	private static int getIntFromStr(String s) throws NumberFormatException {
		s=s.trim();
		return Integer.parseInt(s);
	}


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String t = "[1,null,2,3]";
		TreeNode root=deserialize(t);
		String s = serialize(root);
		System.out.println(t);
		System.out.println(s);
		System.out.println("equals="+t.equals(s));
		System.out.println("[1,2,3,null,null,4,5]");
		System.out.println(serialize(deserialize("[1,2,3,null,null,4,5]")));
	}
}
