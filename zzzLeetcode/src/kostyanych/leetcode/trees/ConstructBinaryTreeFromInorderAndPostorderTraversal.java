package kostyanych.leetcode.trees;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given inorder and postorder traversal of a tree, construct the binary tree.
 *
 * Note:
 * You may assume that duplicates do not exist in the tree.
 *
 * SOLUTION:
 * 1. Inorder traversal:  First visit the left subtree, followed by the root node, followed by the right subtree.
 * 2. Postorder traversal: First visit left subtree, followed by the right subtree, followed by the root node.
 *
 * 3. From 1 -> root divides left and right subtree: (left subtree, ROOT, right subtree)
 * 4. From 2 -> root is always the last node
 *
 * 5. since no duplicates exist in the tree, we may simply map values to their indices so as not to scan arrays
 *
 */

//inorder = [9,3,15,20,7]
//postorder = [9,15,7,20,3]
public class ConstructBinaryTreeFromInorderAndPostorderTraversal {

	private static Map<Integer, Integer> inMap;

	private static TreeNode buildTree(int[] inorder, int[] postorder) {
        inMap = new HashMap<>();
        int len=inorder.length;
        for (int i=0;i<len;i++) {
        	inMap.put(inorder[i], i);
        }
        return build(postorder, len-1, 0, len-1);
    }

	private static TreeNode build(int[] post, int index, int inStart, int inEnd) {
		if (inStart>inEnd)
			return null;

		int val=post[index];
		TreeNode tn = new TreeNode(val);
		int inIndex = inMap.get(val); // where it is in the inorder array

		index--; //that would be the right subtree root

		int rightLength=inEnd-inIndex;
		int leftRoot=index-rightLength;	//where left subtree ends in the postorder array

		tn.right = build(post, index, inIndex+1, inEnd);
		tn.left = build(post, leftRoot, inStart, inIndex-1);
		return tn;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] inorder = {9,3,15,20,7};
		int[] postorder = {9,15,7,20,3};
		System.out.println(Arrays.toString(inorder));
		System.out.println(Arrays.toString(postorder));
		TreeNode root = buildTree(inorder, postorder);
		System.out.println(TreeNode.toLeetcodeString(root));
	}


}
