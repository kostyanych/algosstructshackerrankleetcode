package kostyanych.leetcode.trees;

import java.util.Stack;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.
 *
 * Note: A leaf is a node with no children.
 */
public class PathSum {

	static class Pair {
		public TreeNode tn;
		public int sumLeft;

		public Pair(TreeNode tn, int sl) {
			this.tn=tn;
			this.sumLeft=sl;
		}
	}

	 private static boolean hasPathSum(TreeNode root, int sum) {
		 if (root==null) {
			 return false;
		 }


		 Stack<Pair> st = new Stack<>();
		 st.push(new Pair(root,sum));
		 while (!st.isEmpty()) {
			 Pair p = st.pop();
			 if (p.tn.val==p.sumLeft && p.tn.left==null && p.tn.right==null)
				 return true;
			 if (p.tn.left!=null)
				 st.push(new Pair(p.tn.left, p.sumLeft-p.tn.val));
			 if (p.tn.right!=null)
				 st.push(new Pair(p.tn.right, p.sumLeft-p.tn.val));
		 }
		 return false;
	 }

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 String sroot = "[5,4,8,11,null,13,4,7,2,null,null,null,1]";
		 int val = 22;
		 TreeNode root = TreeNode.fromLeetcodeString(sroot);
		 System.out.println(hasPathSum(root,val));
	 }
}
