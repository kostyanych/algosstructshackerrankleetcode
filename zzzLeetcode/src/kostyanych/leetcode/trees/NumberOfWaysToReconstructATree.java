package kostyanych.leetcode.trees;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * You are given an array pairs, where pairs[i] = [xi, yi], and:
 * 	There are no duplicates.
 * 	xi < yi
 *
 * Let ways be the number of rooted trees that satisfy the following conditions:
 *
 * 	The tree consists of nodes whose values appeared in pairs.
 * 	A pair [xi, yi] exists in pairs if and only if xi is an ancestor of yi or yi is an ancestor of xi.
 * 	Note: the tree does not have to be a binary tree.
 *
 * Two ways are considered to be different if there is at least one node that has different parents in both ways.
 *
 * Return:
 * 	0 if ways == 0
 * 	1 if ways == 1
 * 	2 if ways > 1
 *
 * A rooted tree is a tree that has a single root node, and all edges are oriented to be outgoing from the root.
 *
 * An ancestor of a node is any node on the path from the root to that node (excluding the node itself). The root has no ancestors.
 *
 * Constraints:
 * 	1 <= pairs.length <= 10^5
 * 	1 <= xi < yi <= 500
 *  The elements in pairs are unique.
 *
 * NOTE: this statement is very badly worded. Apparently what it meant to say is that:
 *
 * 1. you are given the array arr = [x,y]
 * 2. all values (not pairs of values, but values themselves!) in arr ar unique
 * 3. you must construct a tree with only one root
 * 4. if in this tree some val x is a descendant of y there must be an index i where arr[i]==[xi,yi] or arr[i]==[yi,xi]
 *
 * SOLUTION:
 * 0. prepare the adjacency list for our tree
 * 1. a root has to be connected with ALL its descendants
 * 1a. if there is no such root, then we can't make a tree
 * 1b. FOR SOME REASON if we have more than one such root, they will have smae result, so pick up any of them (last)
 * 2. remove this root from adj list
 * 3. for all other nodes calculate connected groups of nodes. Those would be subtrees
 * 4. recursively go from 1 to 3 on all those subtrees
 *
 *
 */
public class NumberOfWaysToReconstructATree {

	static private final Map<Integer,Set<Integer>> edges = new HashMap<>();

	static int checkWays(int[][] pairs) {
		Set<Integer> nodes = new HashSet<>();
		for (int[] pair : pairs) {
			putInEdges(pair[0],pair[1]);
			putInEdges(pair[1],pair[0]);
			nodes.add(pair[0]);
			nodes.add(pair[1]);
		}
		return helper(nodes);

    }

	static private void putInEdges(int a, int b) {
		if (a==b)
			return;
		Set<Integer> lst = edges.get(a);
		if (lst==null) {
			lst=new HashSet<>();
			edges.put(a, lst);
		}
		lst.add(b);
	}

	static private int helper(Set<Integer> curNodes){
		int ncount = curNodes.size();
		if (ncount==1)
			return 1;
        int cnt = 0;
        int root = -1;
        for (Integer n : curNodes) {
			Set<Integer> set = edges.get(n);
			if (set==null || set.size()<ncount-1)
				continue;
			root=n;
			cnt++;
			if (cnt>1)
				break;
		}
        if (cnt<1)
        	return cnt;

        Set<Integer> nodes = edges.keySet();
		for(Integer n  : nodes){
            edges.get(n).remove(root);
        }
        List<Set<Integer>> tmp = new ArrayList<>();
        Set<Integer> visited = new HashSet<>();

        for(Integer n : curNodes) {
            if(!visited.contains(n) && n != root){
                tmp.add(new HashSet<>());
                dfs(n, tmp, visited);
            }
        }

        boolean canBeTwo=false;
        for(int i = 0; i < tmp.size(); i++){
            int x = helper(tmp.get(i));
            if (x == 2)
            	canBeTwo=true;
            if (x==0)
            	return 0;
        }
        return (canBeTwo||cnt>1)?2:1;
    }

	static private void dfs(int node, List<Set<Integer>> comp, Set<Integer> visited){
        if (visited.contains(node))
        	return;
        visited.add(node);
        comp.get(comp.size()-1).add(node);
        for (int n : edges.get(node)){
            dfs(n, comp, visited);
        }
    }

	public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 //int[][] pairs = {{1,2},{1,3},{1,4},{1,5},{2,4},{3,5}};
		 //int[][] pairs = {{1,2},{1,3},{2,3}};
		 int[][] pairs = getFromFile();
		 System.out.println(checkWays(pairs));
	}

	static private int[][] getFromFile() {
		int[][] res = null;
		try (FileReader fr = new FileReader(new File("h:\\nikita.txt"));
				BufferedReader br = new BufferedReader(fr)) {
			List<int[]> lst = new ArrayList<>();
			String l;
			while ((l=br.readLine())!=null) {
				String[] buf=l.split(",");
				int[] arr = new int[] {Integer.parseInt(buf[0]), Integer.parseInt(buf[1])};
				lst.add(arr);
			}
			res=new int[lst.size()][];
			for (int i=0;i<lst.size();i++)
				res[i]=lst.get(i);

		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return res;
	}


}
