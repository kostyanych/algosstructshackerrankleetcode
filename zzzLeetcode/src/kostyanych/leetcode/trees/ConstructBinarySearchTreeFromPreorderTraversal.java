package kostyanych.leetcode.trees;

import kostyanych.leetcode.helpers.TreeNode;

public class ConstructBinarySearchTreeFromPreorderTraversal {

	private static int ind=1;

    private static TreeNode bstFromPreorder(int[] preorder) {
    	ind=1;
        int len = preorder.length;
        if (len<1)
            return null;
        TreeNode root = new TreeNode(preorder[0]);
        if (len==1)
            return root;
        root.left=insert(preorder, root.val, len);
        root.right=insert(preorder, Integer.MAX_VALUE, len);
        return root;
    }

    private static TreeNode insert(int[] preorder, int parentVal, int len) {
        if (ind>=len)
            return null;
        int curVal=preorder[ind];
        if (curVal>parentVal)
            return null;
        TreeNode tn = new TreeNode(curVal);
        ind++;
        tn.left=insert(preorder,curVal,len);
        tn.right=insert(preorder,parentVal,len);
        return tn;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {8,5,1,7,10,12};
		TreeNode tree = bstFromPreorder(arr);
		System.out.println(TreeNode.toLeetcodeString(tree));
	}
}
