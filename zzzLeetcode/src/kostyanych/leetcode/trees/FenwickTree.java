package kostyanych.leetcode.trees;

import java.security.InvalidParameterException;

/**
 * A Fenwick tree or binary indexed tree is a data structure that can efficiently update elements and calculate prefix sums in a table of numbers.
 *
 * A binary-indexed tree.
 *
 * Uses an array to hold partial prefix sums:
 * arr[i] holds
 * 1. sum of 1-i, *if i is a power of 2* (arr[8] holds sum(1-8)
 * 2. value of i-th element *if i is odd* (arr[15] holds value(15))
 * 3. sum of (j-i) where j is the next number after the greatest power of 2 less than i ( arr[12] holds sum(9-12) )
 * 		*if i is a sum of 2 powers of 2*
 * 4. sum of (j-i) where j is the next number after the (i - (i & -i))
 * 		*if i is even but is not a sum of 2 powers of 2*
 *
 * basically we go by removing the rightmost 1 in the binary representation of the initial index:
 *
 * 10101011 -> 10101010 -> 10101000 -> 10100000 -> 10000000 ( 171->170->168->160->128 )
 *
 * for finding the last 1 we use i&(-i)
 *
 * x&(-x) gives the last set bit in a number x. How?
 * Let’s say x = a1b(in binary) is the number whose last set bit we want to isolate.
 * Here a is some binary sequence of any length of 1’s and 0’s and b is some sequence of any length but of 0’s only.
 * Remember we said we want the LAST set bit, so for that tiny intermediate 1 bit sitting between a and b to be the last set bit,
 * b should be a sequence of 0’s only of length zero or more.
 * -x = 2’s complement of x = (a1b)’ + 1 = a’0b’ + 1 = a’0(0….0)’ + 1 = a’0(1...1) + 1 = a’1(0…0) = a’1b
 *
 * a’ is a with all bits wlipped so a'&a will yield 0s, b&b will yield 0s, because b is all zeroes already. So we're only left with one 1.
 *
 *
 * Implementation:
 * 1. we use 1-based array
 * 2. The idea is based on the fact that all positive integers can be represented as the sum of powers of 2.
 * 		For example 19 can be represented as 16 + 2 + 1.
 * 		Every node of the BITree stores the sum of n elements where n is a power of 2.
 * 		For example, the sum of the first 12 elements can be obtained by the sum of the last 4 elements (from 9 to 12) plus the sum of 8 elements (from 1 to 8).
 * 		The number of set bits in the binary representation of a number n is O(Logn).
 * 		Therefore, we traverse at-most O(Logn) nodes in both getSum() and update() operations.
 * 		The time complexity of the construction is O(nLogn) as it calls update() for all n elements.
 * 3. The quick way of calculating needed indices:
 * 		Suppose we call query(14), initially sum = 0
 *
 * 		x is 14(1110) we add BIT[14] to our sum variable, thus sum = BIT[14] = (a[14] + a[13])
 *
 * 		now we isolate the last set bit from x = 14(1110) and subtract it from x
 * 		last set bit in 14(1110) is 2(10), thus x = 14 – 2 = 12
 * 		we add BIT[12] to our sum variable, thus sum = BIT[14] + BIT[12] = (a[14] + a[13]) + (a[12] + … + a[9])
 *
 * 		again we isolate last set bit from x = 12(1100) and subtract it from x
 * 		last set bit in 12(1100) is 4(100), thus x = 12 – 4 = 8
 * 		we add BIT[8] to our sum variable, thus
 * 		sum = BIT[14] + BIT[12] + BIT[8] = (a[14] + a[13]) + (a[12] + … + a[9]) + (a[8] + … + a[1])
 *
 * 		once again we isolate last set bit from x = 8(1000) and subtract it from x
 * 		last set bit in 8(1000) is 8(1000), thus x = 8 – 8 = 0
 * 		since x = 0, the for loop breaks and we return the prefix sum.
 *
 * 		Talking about complexity, again we can see that the loop iterates at most the number of bits in x
 * 				which will be at most n (the size of the given array).
 * 		Thus the *query operation takes O(log2(n)) time *.
 *
 */
public class FenwickTree {

	final private int[] BITree;
	final private int size;

	public FenwickTree(int size) {
		if (size<1 || size==Integer.MAX_VALUE)
			throw new InvalidParameterException("Size must be positive and less than Integer.MAX_VALUE");
		BITree = new int[size+1];
		this.size=size;
	}

	// Returns sum of arr[0..index]. This function
    // assumes that the array is preprocessed and
    // partial sums of array elements are stored
    // in BITree[].
    int getSum(int index) {
    	if (index>=size)
    		throw new IndexOutOfBoundsException();

        int sum = 0; // Initialize result

        // index in BITree[] is 1 more than the index in arr[] (because it's 1-based)
        index = index + 1;

        // Traverse ancestors of BITree[index]
        while(index>0) {
            // Add current element of BITree  to sum
            sum += BITree[index];

            // Move index to parent node in  getSum View
            index -= index & (-index);
        }
        return sum;
    }

    // Updates a node in Binary Index Tree (BITree)
    // at given index in BITree. The given value
    // 'val' is added to BITree[i] and all of
    // its ancestors in tree.
    public void addValToBIT(int index, int val) {
    	if (index>=size)
    		throw new IndexOutOfBoundsException();

    	// index in BITree[] is 1 more than the index in arr[] (because it's 1-based)
        index = index + 1;

        // Traverse all ancestors and add 'val'
        while(index <= size+1)  {
        	// Add 'val' to current node of BIT Tree
        	BITree[index] += val;

        	// Update index to that of parent
        	// in update View
        	index += index & (-index);
        }
    }

    /* Function to construct fenwick tree from given array.*/
/*    void constructBITree(int arr[], int n) {
        // Initialize BITree[] as 0
        for(int i=1; i<=n; i++)
            BITree[i] = 0;

        // Store the actual values in BITree[]
        // using update()
        for(int i = 0; i < n; i++)
            updateBIT(n, i, arr[i]);
    }
*/
}
