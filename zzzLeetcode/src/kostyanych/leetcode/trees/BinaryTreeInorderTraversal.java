package kostyanych.leetcode.trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree, return the inorder traversal of its nodes' values.
 * Recursive solution is trivial, could you do it iteratively?
 *
 */
public class BinaryTreeInorderTraversal {

	private static List<Integer> inorderTraversal(TreeNode root) {
		// return inorderTraversalRecursive(root);
		return inorderTraversalIterative(root);
	}

	private static List<Integer> inorderTraversalRecursive(TreeNode root) {
		List<Integer> res = new ArrayList<>();
		if (root == null)
			return res;
		if (root.left != null)
			res.addAll(inorderTraversal(root.left));
		res.add(root.val);
		if (root.right != null)
			res.addAll(inorderTraversal(root.right));
		return res;
	}

	private static List<Integer> inorderTraversalIterative(TreeNode root) {
		List<Integer> res = new ArrayList<>();
		if (root == null)
			return res;
		Stack<TreeNode> st = new Stack<>();
		TreeNode cur = root;
		while (!st.empty() || cur != null) {
			if (cur != null) {
				if (cur.left != null) {
					st.push(cur);
					cur = cur.left;
				}
				else {
					res.add(cur.val);
					if (cur.right != null)
						cur = cur.right;
					else
						cur = null;
				}
			}
			else {
				cur = st.pop();
				res.add(cur.val);
				if (cur.right != null)
					cur = cur.right;
				else
					cur = null;
			}
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String arr = "[1,null,2,3]";
		TreeNode root=TreeNode.fromLeetcodeString(arr);
		List<Integer> res= inorderTraversalRecursive(root);
		System.out.println(res);
		res=inorderTraversal(root);
		System.out.println(res);
	}

}
