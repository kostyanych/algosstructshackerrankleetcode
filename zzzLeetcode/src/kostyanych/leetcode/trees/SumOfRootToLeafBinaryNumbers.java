package kostyanych.leetcode.trees;

import java.util.List;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree, each node has value 0 or 1.  Each root-to-leaf path represents a binary number starting with the most significant bit.
 * For example, if the path is 0 -> 1 -> 1 -> 0 -> 1, then this could represent 01101 in binary, which is 13.
 * For all leaves in the tree, consider the numbers represented by the path from the root to that leaf.
 * Return the sum of these numbers.
 */
public class SumOfRootToLeafBinaryNumbers {

	private static int sumRootToLeaf(TreeNode root) {
        int res=0;
        if (root.left==null && root.right==null)
        	return root.val;
        if (root.left!=null)
            res+=sumOf(root.left, root.val);
        if (root.right!=null)
            res+=sumOf(root.right, root.val);
        return res;
    }

    private static int sumOf(TreeNode tn, int prevVal) {
        int res=0;
        int curVal=prevVal*2+tn.val;
        if (tn.left==null && tn.right==null)
            return curVal;
        if (tn.left!=null)
            res=sumOf(tn.left,curVal);
        if (tn.right!=null)
            res+=sumOf(tn.right,curVal);
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String arr = "[1,0,1,0,1,0,1]";
		TreeNode root=TreeNode.fromLeetcodeString(arr);
		System.out.println(sumRootToLeaf(root));
	}

}
