package kostyanych.leetcode.trees;

import java.util.Comparator;
import java.util.Map;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree root, the task is to return the maximum sum of all keys of any sub-tree which is also a Binary Search Tree (BST).
 *
 * Assume a BST is defined as follows:
 * The left subtree of a node contains only nodes with keys less than the node's key.
 * The right subtree of a node contains only nodes with keys greater than the node's key.
 * Both the left and right subtrees must also be binary search trees.
 *
 * !!!! It doesn't mentioned in the description, but negative sums don't count, return 0
 *
 */
public class MaximumSumBstInBinaryTree {

	private final static int BST_YES=1;
	private final static int BST_NO=0;

	static Comparator<int[]> cmp = new Comparator<int[]>() {
		@Override
		public int compare(int[] o1, int[] o2) {
			if (o1[1]==o2[1])
				return o2[0]-o1[0];
			if (o1[1]==BST_NO)
				return 1;
			else
				return -1;
		}
	};

	private static int max=0;

	private static int maxSumBST(TreeNode root) {
//		Map<TreeNode, int[]> map = new HashMap<>();
		max=0;
		calculateSums(root);
//		List<int[]> list = new ArrayList<>(map.values());
//		Collections.sort(list, (r0, r1) -> r0[1]==BST_NO ? 1 : (r0[0]<r1[0] ? 1 : -1));
//		Collections.sort(list, (r0, r1) -> r0[1]==r1[1] ? (r1[0] - r0[0])  : (r0[1]==BST_NO) ? 1 : -1);
//		Collections.sort(list, cmp);
//		int[] res=list.get(0);
//		if (res[1]==BST_NO)
//			return 0;
//		return res[0]<0? 0 : res[0];
		return max;
	}

	private static int[] calculateSums(TreeNode node) {
		if (node==null)
			return null;
		int[] res=new int[2];
		if (node.left==null && node.right==null) {
			res[0]=node.val;
			res[1]=BST_YES;
			if (max<res[0])
				max=res[0];
			return res;
		}
		int[] left=null;
		int[] right=null;

		int this_is_bst=BST_YES;
		int sum=node.val;
		if (node.left!=null) {
			left=calculateSums(node.left);
			if (left[1]==BST_NO)
				this_is_bst=BST_NO;
			if (node.left.val>=node.val)
				this_is_bst=BST_NO;
			sum+=left[0];
		}
		if (node.right!=null) {
			right=calculateSums(node.right);
			if (right[1]==BST_NO)
				this_is_bst=BST_NO;
			if (node.right.val<=node.val)
				this_is_bst=BST_NO;
			sum+=right[0];
		}
		res[0]=sum;
		res[1]=this_is_bst;
		if (this_is_bst==BST_YES) {
			if (max<sum)
				max=sum;
		}
		return res;
	}

	private static int[] calculateSums(TreeNode node, Map<TreeNode, int[]> map) {
		if (node==null)
			return null;
		int[] res=new int[2];
		if (node.left==null && node.right==null) {
			res[0]=node.val;
			res[1]=BST_YES;
			map.put(node, res);
			return res;
		}
		int[] left=null;
		int[] right=null;

		int this_is_bst=BST_YES;
		int sum=node.val;
		if (node.left!=null) {
			left=calculateSums(node.left, map);
			if (left[1]==BST_NO)
				this_is_bst=BST_NO;
			if (node.left.val>=node.val)
				this_is_bst=BST_NO;
			sum+=left[0];
		}
		if (node.right!=null) {
			right=calculateSums(node.right, map);
			if (right[1]==BST_NO)
				this_is_bst=BST_NO;
			if (node.right.val<=node.val)
				this_is_bst=BST_NO;
			sum+=right[0];
		}
		res[0]=sum;
		res[1]=this_is_bst;
		map.put(node, res);
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

//		Integer[] arr = {1,4,3,2,4,2,5,null,null,null,null,null,null,4,6};
//		TreeNode root = TreeNode.fromLeetcodeArray(arr);
//		System.out.println(maxSumBST(root));

		String arr = "[{4,3,null,1,2]";
		TreeNode root = TreeNode.fromLeetcodeString(arr);
		System.out.println(maxSumBST(root));
	}
}
