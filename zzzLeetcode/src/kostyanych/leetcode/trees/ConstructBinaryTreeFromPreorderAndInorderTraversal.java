package kostyanych.leetcode.trees;

import java.util.HashMap;
import java.util.Map;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given two integer arrays preorder[] and inorder[] where preorder[] is the preorder traversal of a binary tree
 * and inorder[] is the inorder traversal of the same tree, construct and return the binary tree.
 *
 * Constraints:
 * 1 <= preorder.length <= 3000
 * inorder.length == preorder.length
 * -3000 <= preorder[i], inorder[i] <= 3000
 * preorder and inorder consist of unique values.
 * Each value of inorder also appears in preorder.
 * preorder is guaranteed to be the preorder traversal of the tree.
 * inorder is guaranteed to be the inorder traversal of the tree.
 *
 * SOLUTION
 * The two key observations are:
 *
 * Preorder traversal follows Root -> Left -> Right, therefore, given the preorder array preorder, we have easy access to the root which is preorder[0].
 *
 * Inorder traversal follows Left -> Root -> Right, therefore if we know the position of Root, we can recursively split the entire array into two subtrees.
 *
 * Now the idea should be clear enough.
 * We will design a recursion function: it will set the first element of preorder as the root, and then construct the entire tree.
 * To find the left and right subtrees, it will look for the root in inorder,
 * 		so that everything on the left should be the left subtree, and everything on the right should be the right subtree.
 * Both subtrees can be constructed by making another recursion call.
 *
 * It is worth noting that, while we recursively construct the subtrees, we should choose the next element in preorder to initialize as the new roots.
 * This is because the current one has already been initialized to a parent node for the subtrees.
 */
public class ConstructBinaryTreeFromPreorderAndInorderTraversal {

	static private int preorderIndex;
	static private final Map<Integer, Integer> inorderIndexMap=new HashMap<>();

    static private TreeNode buildTree(int[] preorder, int[] inorder) {
        int nodes=preorder.length;
        if (nodes==1)
        	return new TreeNode(preorder[0]);

    	preorderIndex = 0;

    	// build a hashmap to store value -> its index relations
        inorderIndexMap.clear();

        for (int i = 0; i < inorder.length; i++) {
            inorderIndexMap.put(inorder[i], i);
        }

        return arrayToTree(preorder, 0, nodes-1);
    }

    static private TreeNode arrayToTree(int[] preorder, int left, int right) {
        // if there are no elements to construct the tree
        if (left > right)
        	return null;

        // select the preorder_index element as the root and increment it
        int rootValue = preorder[preorderIndex++];
        TreeNode root = new TreeNode(rootValue);

        // build left and right subtree
        // excluding inorderIndexMap[rootValue] element because it's the root
        root.left = arrayToTree(preorder, left, inorderIndexMap.get(rootValue) - 1);
        root.right = arrayToTree(preorder, inorderIndexMap.get(rootValue) + 1, right);
        return root;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] preorder = {3,9,20,15,7};
		int[] inorder = {9,3,15,20,7};
		TreeNode tree = buildTree(preorder, inorder);
		System.out.println(TreeNode.toLeetcodeString(tree));
	}
}
