package kostyanych.leetcode.trees;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree root, a ZigZag path for a binary tree is defined as follow:
 * 
 * Choose any node in the binary tree and a direction (right or left).
 * If the current direction is right then move to the right child of the current node otherwise move to the left child.
 * Change the direction from right to left or right to left.
 * Repeat the second and third step until you can't move in the tree.
 * Zigzag length is defined as the number of nodes visited - 1. (A single node has a length of 0).
 * 
 * Return the length of the longest ZigZag path contained in that tree.
 *
 */
public class LongestZigZagPathinBinaryTree {

	private static Map<TreeNode, int[]> map=new HashMap<>();
	
	private static int longestZigZag(TreeNode root) {
		calculateForNode(root, true);
		calculateForNode(root, false);
		Set<TreeNode> nodes=map.keySet();
		int max=0;
		for (TreeNode node : nodes) {
			int[] cnt=map.get(node);
			if (cnt[0]>max)
				max=cnt[0];
			if (cnt[1]>max)
				max=cnt[1];
		}
		return max;
	}

	private static int calculateForNode(TreeNode node, boolean isForRight) {
		if (node==null)
			return -1;
		int[] cnt=map.get(node);
		if (cnt==null) {
			cnt=new int[2];
			cnt[0]=calculateForNode(node.left, true)+1;
			cnt[1]=calculateForNode(node.right, false)+1;
			map.put(node,cnt);
		}
		if (isForRight)
			return cnt[1];
		return cnt[0];
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		TreeNode root=new TreeNode(1);
//		TreeNode n2=new TreeNode(2);
//		root.right=n2;
//		TreeNode n3=new TreeNode(3);
//		n2.left=n3;
//		TreeNode n4=new TreeNode(4);
//		n2.right=n4;
//		TreeNode n5=new TreeNode(5);
//		n4.right=n5;
//		TreeNode n6=new TreeNode(6);
//		n4.left=n6;
//		TreeNode n7=new TreeNode(7);
//		n6.left=n7;
//		TreeNode n8=new TreeNode(8);
//		n6.right=n8;
		System.out.println(longestZigZag(root));
	}
}
