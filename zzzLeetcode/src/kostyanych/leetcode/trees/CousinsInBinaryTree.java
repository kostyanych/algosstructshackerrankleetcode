package kostyanych.leetcode.trees;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * In a binary tree, the root node is at depth 0, and children of each depth k node are at depth k+1.
 *
 * Two nodes of a binary tree are cousins if they have the same depth, but have different parents.
 *
 * We are given the root of a binary tree with unique values, and the values x and y of two different nodes in the tree.
 *
 * Return true if and only if the nodes corresponding to the values x and y are cousins.
 *
 */
public class CousinsInBinaryTree {

	static class ParentLevel {
		final public int level;
		final public TreeNode parent;
		final public TreeNode tn;

		public ParentLevel(int l, TreeNode p, TreeNode tn) {
			this.level=l;
			this.parent=p;
			this.tn=tn;
		}
	}

	public static boolean isCousins(TreeNode root, int x, int y) {
        if (root==null)
            return false;

        ParentLevel plx=null;
        ParentLevel ply=null;
        Queue<ParentLevel> q = new LinkedList<>();
        if (root.left!=null)
        	q.add(new ParentLevel(1, root, root.left));
        if (root.right!=null)
        	q.add(new ParentLevel(1, root, root.right));
        while (!q.isEmpty() && (plx==null || ply==null)) {
        	ParentLevel p = q.remove();
        	if (p.tn.val==x)
        		plx=p;
        	else if (p.tn.val==y)
        		ply=p;
        	if (p.tn.left!=null)
        		q.add(new ParentLevel(p.level+1, p.tn, p.tn.left));
        	if (p.tn.right!=null)
        		q.add(new ParentLevel(p.level+1, p.tn, p.tn.right));
        }

        if (plx==null || ply==null)
        	return false;
        if (plx.level!=ply.level)
        	return false;
        if (plx.parent==ply.parent)
        	return false;
        return true;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		TreeNode tn=TreeNode.fromLeetcodeString("[1,2,3,null,4,null,5]");
		System.out.println(isCousins(tn, 5, 4));
	}
}
