package kostyanych.leetcode.trees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import kostyanych.leetcode.helpers.TreeNode;

public class SmallestSubtreeWithAllTheDeepestNodes {

	static class ND {
        TreeNode n;
        int depth;
        public ND(TreeNode _n, int _d) {
            n=_n;
            depth=_d;
        }
    }

    static private TreeNode subtreeWithAllDeepest(TreeNode root) {
        if (root==null)
            return null;
        if (root.left==null && root.right==null)
            return root;
        Queue<ND> qq = new LinkedList<>();
        Stack<ND> st1 = new Stack<>();
        int level=1;
        ND nd = new ND(root,level);
        qq.add(nd);
        st1.add(nd);
        while (!qq.isEmpty()) {
            int s = qq.size();
            level++;
            for (int i=0;i<s;i++) {
                ND nnd=qq.remove();
                TreeNode tn=nnd.n;
                if (tn.left!=null) {
                    ND nnnd=new ND(tn.left,level);
                    qq.add(nnnd);
                    st1.add(nnnd);
                }
                if (tn.right!=null) {
                    ND nnnd=new ND(tn.right,level);
                    qq.add(nnnd);
                    st1.add(nnnd);
                }
            }
        }
        int maxlevel=st1.peek().depth;
        Queue<ND> q = new LinkedList<>();
        while (st1.peek().depth==maxlevel) {
            q.add(st1.pop());
        }

        if (q.size()==1)
            return q.remove().n;
        while (!st1.isEmpty() && !q.isEmpty()) {
            int qs=q.size();
            maxlevel--;
            while (qs>0) {
                TreeNode head=q.peek().n;
                ND nnd = st1.pop();
                boolean added=false;
                if (nnd.n.right==head) {
                    q.add(nnd);
                    added=true;
                    q.remove();
                    qs--;
                }
                head=q.peek().n;
                if (nnd.n.left==head) {
                    if (!added)
                        q.add(nnd);
                    q.remove();
                    qs--;
                }
            }
            if (q.size()==1)
                return q.remove().n;
        }
        return null;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		TreeNode root = TreeNode.fromLeetcodeString("[0,2,1,null,null,3,null,4]");
		TreeNode tn = subtreeWithAllDeepest(root);
		System.out.println(tn.toLeetcodeString(root));
	}
}
