package kostyanych.leetcode.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * A tree is an undirected graph in which any two vertices are connected by exactly one path. In other words,
 * any connected graph without simple cycles is a tree.
 *
 * Given a tree of n nodes labelled from 0 to n - 1,
 * and an array of n - 1 edges where edges[i] = [ai, bi] indicates that there is an undirected edge between the two nodes ai and bi in the tree,
 * you can choose any node of the tree as the root.
 *
 * When you select a node x as the root, the result tree has height h.
 * Among all possible rooted trees, those with minimum height (i.e. min(h))  are called minimum height trees (MHTs).
 *
 * Return a list of all MHTs' root labels. You can return the answer in any order.
 *
 * The height of a rooted tree is the number of edges on the longest downward path between the root and a leaf.
 *
 * Constraints:
 * 	1 <= n <= 2 * 10^4
 * 	edges.length == n - 1
 *  0 <= ai, bi < n
 *  ai != bi
 *  All the pairs (ai, bi) are distinct.
 *  The given input is guaranteed to be a tree and there will be no repeated edges.
 *
 *  SOLUTION:
 *  1. if there is only one node, the result is this node
 *  2. make a graph struct List({node -> List(adjacent noeds)}). Fill it from edges
 *  3. for every node calculate the number of edges from it
 *  4. starting from nodes with edgeCount==1 delete these edges and decrease edgeCount for corresponding nodes
 *  	You thus will be going from the outer 'layer' of the graph to the 'center' of it
 *  5. repeat 4 until there are no more than 2 nodes left
 *  6. when you come to only 1 node this node is the solution
 *  6a. when you come to 2 nodes those are the solution (2 nodes with all outer edges removed, leaving just the edge between them)
 *  6b. there can be no more than 2 'center' nodes because we have no cycles by the problem statement
 */
public class MinimumHeightTrees {

	private static List<Integer> findMinHeightTrees(int n, int[][] edges) {
		List<Integer> res = new ArrayList<>();
		if (n==1) {
			res.add(0);
			return res;
		}

		List<Integer>[] graph = new ArrayList[n];
		//how many edges there are from given node
        int[] edgeCount = new int[n];
        for (int i = 0; i < n; i++) {
            graph[i] = new ArrayList<>();
        }

        for (int[] edge: edges) {
            graph[edge[0]].add(edge[1]);
            graph[edge[1]].add(edge[0]);
            edgeCount[edge[0]]++;
            edgeCount[edge[1]]++;
        }

        //start with leaves. Since there are no cycles, there must be leaves! q will never turn up empty
        Queue<Integer> q = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            if (edgeCount[i] == 1){
                q.add(i);
            }
        }

        int nodesLeft = n;
        while(!q.isEmpty()) {
            //We found answer if queue left with <=2 nodes in it
            if (nodesLeft <= 2) {
                break;
            }
            int currentLeafNodes = q.size();
            for (int i=0;i<currentLeafNodes;i++) {
                int curNode = q.poll();
                nodesLeft--;
                for (int adj: graph[curNode]) {
                    // edge is removed so edgeCount will decrease for both nodes
                	edgeCount[adj]--;
                    if (edgeCount[adj] == 1) {
                        q.add(adj);
                    }
                }
            }
        }
        res.addAll(q);
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n=6;
		int[][] edges= {{3,0},{3,1},{3,2},{3,4},{5,4}};
		System.out.println(findMinHeightTrees(n, edges));
    }

}
