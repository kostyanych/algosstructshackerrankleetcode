package kostyanych.leetcode.trees;

import java.util.Stack;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree where each path going from the root to any leaf form a valid sequence,
 * check if a given string is a valid sequence in such binary tree.
 *
 *  We get the given string from the concatenation of an array of integers arr and the concatenation of all values of the nodes along
 *  a path results in a sequence in the given binary tree.
 *
 */
public class CheckIfStringIsValidSequenceInBinaryTree {

	static class Pair {
		public TreeNode tn;
		public int index;
		public Pair(TreeNode tn, int i) {
			this.tn=tn;
			this.index=i;
		}
	}

	private static boolean isValidSequence(TreeNode root, int[] arr) {

		 int len = arr.length;
		 if (len<1)
			 return false;
		 if (root==null)
			 return false;
		 if (root.val!=arr[0])
			 return false;
		 Stack<Pair> st = new Stack<>();
		 st.push(new Pair(root,0));
		 while (!st.isEmpty()) {
			 Pair p=st.pop();
			 if (p.index>=len)
				 continue;
			 if (p.tn.val!=arr[p.index])
				 continue;
			 if (p.tn.left!=null)
				 st.push(new Pair(p.tn.left, p.index+1));
			 if (p.tn.right!=null)
				 st.push(new Pair(p.tn.right, p.index+1));
			 if (p.index==len-1
					 && p.tn.right==null
					 && p.tn.left==null)
				 return true;
		 }
		 return false;
	 }

	public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 String sroot = "[0,1,0,0,1,0,null,null,1,0,0]";
		 int[] arr = {0,1,0,1};
		 TreeNode root = TreeNode.fromLeetcodeString(sroot);
		 System.out.println(isValidSequence(root,arr));
	 }
}
