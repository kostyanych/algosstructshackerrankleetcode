package kostyanych.leetcode.trees;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree where node values are digits from 1 to 9.
 * A path in the binary tree is said to be pseudo-palindromic if at least one permutation of the node values in the path is a palindrome.
 *
 * Return the number of pseudo-palindromic paths going from the root node to leaf nodes.
 *
 * Constraints:
 * 	The given binary tree will have between 1 and 10^5 nodes.
 * 	Node values are digits from 1 to 9.
 */
public class PseudoPalindromicPathsInABinaryTree {

    static private int[] vals = new int[10];
    static private int res=0;

	static private int pseudoPalindromicPaths(TreeNode root) {
        if (root==null)
            return 0;
        if (root.left==null && root.right==null)
            return 1;
        dfs(root);
        return res;
    }

	static private void dfs(TreeNode tn) {
        vals[tn.val]++;
        if (tn.left==null && tn.right==null) { //leaf node
            if (pali(vals))
                res++;
            vals[tn.val]--;
            return;
        }
        if (tn.left!=null)
            dfs(tn.left);
        if (tn.right!=null)
            dfs(tn.right);
        vals[tn.val]--;
    }

	static private boolean pali(int[] arr) {
        boolean hasOdd=false;
        for (int i : arr) {
            if (i%2!=0) {
                if (hasOdd)
                    return false;
                hasOdd=true;
            }
        }
        return true;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		TreeNode root = TreeNode.fromLeetcodeString("[2,1,1,1,3,null,null,null,null,null,1]");
		System.out.println(pseudoPalindromicPaths(root));
	}


}
