package kostyanych.leetcode.trees;

import java.util.ArrayList;
import java.util.List;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given the root of a binary tree, determine if it is a valid binary search tree (BST).
 *
 * A valid BST is defined as follows:
 * 		The left subtree of a node contains only nodes with keys less than the node's key.
 * 		The right subtree of a node contains only nodes with keys greater than the node's key.
 * 		Both the left and right subtrees must also be binary search trees.
 *
 * Constraints:
 * 		The number of nodes in the tree is in the range [1, 104].
 * 		-231 <= Node.val <= 231 - 1
 *
 */
public class ValidateBinarySearchTree {

	static boolean isValidBST(TreeNode root) {
        if (root==null)
            return true;
        if (root.left==null && root.right==null)
            return true;
        if (root.left!=null && root.left.val>=root.val)
            return false;
        if (root.right!=null && root.right.val<=root.val)
            return false;
/*
        List<Integer> lst = new ArrayList<>();
        inorder(root,lst);
        for (int i=1;i<lst.size();i++) {
        	if (lst.get(i-1)>=lst.get(i))
        		return false;
        }
        return true;
*/
        return helper(root, null, null);
    }

	private static boolean helper(TreeNode node, Integer lower, Integer upper) {

        if (node == null)
            return true;

        if (lower != null && node.val <= lower)
            return false;
        if (upper != null && node.val >= upper)
            return false;

        if (!helper(node.right, node.val, upper))
            return false;

        if (!helper(node.left, lower, node.val))
            return false;

        return true;
    }

	static private void inorder(TreeNode tn, List<Integer> lst) {
		if (tn.left!=null)
			inorder(tn.left,lst);
		lst.add(tn.val);
		if (tn.right!=null)
			inorder(tn.right,lst);
	}

    static private boolean dfs(int rootval, TreeNode tn, boolean isleft) {
        if (tn==null)
            return true;
        if (tn.left==null && tn.right==null)
            return true;
        if (tn.left!=null) {
        	if (tn.left.val>=tn.val)
        		return false;
        	if (!isleft && tn.left.val<=rootval)
        		return false;
        }
        if (tn.right!=null) {
        	if (tn.right.val<=tn.val)
        		return false;
        	if(isleft && tn.right.val>=rootval)
        		return false;
        }
        return dfs(tn.val,tn.left, true) && dfs(tn.val,tn.right, false);
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		TreeNode tn = TreeNode.fromLeetcodeString("[3,1,5,0,2,4,6]");
		System.out.println(isValidBST(tn));
	}

}
