package kostyanych.leetcode.trees;

import java.util.ArrayList;
import java.util.List;

import kostyanych.leetcode.helpers.Printer;
import kostyanych.leetcode.helpers.TreeNode;

/**
 * You are given the root of a binary search tree (BST), where exactly two nodes of the tree were swapped by mistake.
 * Recover the tree without changing its structure.
 *
 * Follow up: A solution using O(n) space is pretty straight forward. Could you devise a constant space solution?
 *
 * Constraints:
 * 	The number of nodes in the tree is in the range [2, 1000].
 * 	-2^31 <= Node.val <= 2^31 - 1
 *
 */
public class RecoverBinarySearchTree {

	private static List<TreeNode> nodeList = new ArrayList<>();

	//this is the last node of traversal
	//it must be less than the other
	//if it isn't then it's our candidate for swapping
	static private TreeNode prev = null;
	//we only need to swap 1st prev node that is greater than the current node
	//with the last node that is less than prev
	//(consider cases of (1-2-4-3-5) and (1-5-3-4-2)
	//this is the first candidate for swapping
	static private TreeNode tmp1 = null;
	//this is the 2nd candidate for swapping
	static private TreeNode tmp2 = null;

	static private void recoverTree(TreeNode root) {
		inorder(root);

        int tmpVal = tmp1.val;
        tmp1.val = tmp2.val;
        tmp2.val = tmpVal;
    }

	static private void inorder(TreeNode node){
		if (node == null)
			return;

        inorder(node.left);

        if (prev != null && prev.val > node.val) {
            if (tmp1 == null)
            	tmp1 = prev;
            tmp2 = node;
        }

        prev = node;
        inorder(node.right);
    }

	static private void recoverTreeSLOW(TreeNode root) {
		inOrderTraversal(root);
		List<Integer> copy = new ArrayList<>();
		for (TreeNode tn : nodeList) {
			copy.add(tn.val);
		}
		//copy.sort((n1, n2) -> n1.val<=n2.val ? -1 : 1);
		copy.sort(Integer::compareTo);
		for (int i=0;i<copy.size();i++) {
			nodeList.get(i).val=copy.get(i);
		}
	}

	static private void inOrderTraversal(TreeNode node) {
		if (node.left!=null)
			inOrderTraversal(node.left);
		nodeList.add(node);
		if (node.right!=null)
			inOrderTraversal(node.right);
	}

	public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 String sroot = "[1,3,null,null,2]";
		 TreeNode root = TreeNode.fromLeetcodeString(sroot);
		 recoverTree(root);
		 System.out.println(TreeNode.toLeetcodeString(root));
	 }

}
