package kostyanych.leetcode.trees;

import kostyanych.leetcode.helpers.Printer;
import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given the root of a binary search tree and the lowest and highest boundaries as low and high,
 * 		trim the tree so that all its elements lies in [low, high].
 * Trimming the tree should not change the relative structure of the elements that will remain in the tree
 * 		(i.e., any node's descendant should remain a descendant). It can be proven that there is a unique answer.
 *
 * Return the root of the trimmed binary search tree. Note that the root may change depending on the given bounds.
 *
 */
public class TrimBinarySearchTree {

	private static TreeNode trimBST(TreeNode root, int low, int high) {
		if(root == null)
			return null;

		if(root.val > high) //only need lower part - that's on the left
			return trimBST(root.left, low,high);

		if(root.val < low) //only need higher part - that's on the right
			return trimBST(root.right,low,high);

		root.left = trimBST(root.left, low, high);
		root.right = trimBST(root.right, low, high);

		return root;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		TreeNode tn=TreeNode.fromLeetcodeString("[3,0,4,null,2,null,null,1]");
		System.out.println(trimBST(tn, 1, 3));
	}
}
