package kostyanych.leetcode.trees;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * The thief has found himself a new place for his thievery again.
 * There is only one entrance to this area, called the "root."
 * Besides the root, each house has one and only one parent house.
 * After a tour, the smart thief realized that "all houses in this place forms a binary tree".
 * It will automatically contact the police if two directly-linked houses were broken into on the same night.
 *
 * Determine the maximum amount of money the thief can rob tonight without alerting the police.
 *
 */
public class HouseRobberIII {

    static private int rob(TreeNode root) {
        int[] res = process(root);
        return Math.max(res[0], res[1]);
    }

    static private int[] process(TreeNode node) {
    	int[] res = new int[2];
    	res[0] = node.val;
    	int[] left=null;
        if (node.left!=null)
            left=process(node.left);
    	int[] right=null;
        if (node.right!=null)
        	right=process(node.right);
        int leftAmount = 0;
        int leftChildren = 0;
        if (left!=null) {
        	leftAmount=Math.max(left[0], left[1]);
        	leftChildren=left[1];
        }

        int rightAmount = 0;
        int rightChildren = 0;
        if (right!=null) {
        	rightAmount=Math.max(right[0], right[1]);
        	rightChildren=right[1];
        }

        res[0]+=(leftChildren+rightChildren);
        res[1]=Math.max(leftAmount+rightAmount, res[0]-node.val);
        return res;
    }

    public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 String sroot = "[4,1,null,2,null,3]";
		 TreeNode root = TreeNode.fromLeetcodeString(sroot);
		 System.out.println(rob(root));
	 }

}
