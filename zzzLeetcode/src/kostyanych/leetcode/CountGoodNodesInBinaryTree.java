package kostyanych.leetcode;

import java.util.LinkedList;
import java.util.Queue;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a binary tree root, a node X in the tree is named good if in the path from root to X there are no nodes with a value greater than X.
 *
 * Return the number of good nodes in the binary tree.
 */
public class CountGoodNodesInBinaryTree {

	static class Wrap {
		TreeNode tn;
		int max;
		public Wrap(TreeNode tn, int max) {
			this.tn=tn;
			this.max=max;
		}
	}

	private static int goodNodes(TreeNode root) {
		int res=1;
		Queue<Wrap> q = new LinkedList<>();
		if (root.left!=null)
			q.add(new Wrap(root.left, root.val));
		if (root.right!=null)
			q.add(new Wrap(root.right, root.val));
		while (!q.isEmpty()) {
			Wrap w=q.remove();
			if (w.tn.val>=w.max)
				res++;
			if (w.tn.left!=null)
				q.add(new Wrap(w.tn.left, Math.max(w.tn.val, w.max)));
			if (w.tn.right!=null)
				q.add(new Wrap(w.tn.right, Math.max(w.tn.val, w.max)));
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String st="[1]";
		TreeNode root = TreeNode.fromLeetcodeString(st);
		System.out.println(goodNodes(root));
	}


}
