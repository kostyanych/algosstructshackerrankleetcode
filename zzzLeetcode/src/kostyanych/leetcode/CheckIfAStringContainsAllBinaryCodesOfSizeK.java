package kostyanych.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * Given a binary string s and an integer k.
 * Return True if all binary codes of length k is a substring of s. Otherwise, return False.
 *
 */
public class CheckIfAStringContainsAllBinaryCodesOfSizeK {

static String[] zeroPaddings;

    private static boolean hasAllCodes(String s, int k) {
        if(k > s.length())
            return false;

        int len=s.length();
        int subStringNum=len-k+1;
        int allnums=(int)Math.pow(2, k);
        if (allnums>subStringNum)
        	return false;
        int leftBitNum = (int)Math.pow(2, k - 1);

        Set<Integer> set = new HashSet<>();
        int num = 0;
        for(int i = 0; i < k; i++) {
            num=num<<1;
            if(s.charAt(i) == '1')
                num += 1;
        }
        set.add(num);
        for(int i = k; i < len; i++) {
            //left digit goes out of the window
            if(s.charAt(i - k) == '1')
                num -= leftBitNum;
            num=num<<1;
            if(s.charAt(i) == '1')
                num += 1;
            set.add(num);
        }
        return set.size() == allnums;
    }

    private static boolean hasAllCodesSubstr(String s, int k) {
        if (k>s.length())
			return false;
        int len=s.length();
        Set<String> set = new HashSet<>();
        for (int i=0;i<=len-k;i++) {
            set.add(s.substring(i,i+k));
        }
        int num=(int)Math.pow(2,k);
        if (set.size()==num)
            return true;
        return false;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(hasAllCodes("00110110",2));
	}
}
