package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given an integer n, find a sequence that satisfies all of the following:
 *
 * The integer 1 occurs once in the sequence.
 * Each integer between 2 and n occurs twice in the sequence.
 * For every integer i between 2 and n, the distance between the two occurrences of i is exactly i.
 * The distance between two numbers on the sequence, a[i] and a[j], is the absolute difference of their indices, |j - i|.
 *
 * Return the lexicographically largest sequence. It is guaranteed that under the given constraints, there is always a solution.
 *
 * A sequence a is lexicographically larger than a sequence b (of the same length) if in the first position where a and b differ,
 * sequence a has a number greater than the corresponding number in b.
 * For example, [0,1,9,0] is lexicographically larger than [0,1,5,6] because the first position they differ is at the third number, and 9 is greater than 5.
 *
 */
public class ConstructLexicographicallyLargestValidSequence {

	static private int[] res = null;
	static private boolean[] visited = null;

	static private int[] constructDistancedSequence(int n) {
		int len=1+2*(n-1);
		if (len==1)
			return new int[] {1};
		else if (len==2)
			return new int[] {2,1,2};

        res = new int[len];
        visited = new boolean[n + 1];
        calc(0, n);
        return res;
    }

    static private boolean calc(int index, int n) {
        if (index == res.length)
            return true;

        if (res[index] != 0)
        	return calc(index + 1, n); // index is already used, go further
        else {
            for (int i = n; i >= 1; i--) {
                if (visited[i])
                	continue;
                visited[i] = true;
                res[index] = i;
                if (i == 1) {
                    if (calc(index + 1, n))
                    	return true;
                }
                else if (index + i < res.length && res[index + i] == 0) {
                    res[i + index] = i; // if i!=1 we need to set 2nd index
                    if (calc(index + 1, n))
                    	return true;
                    res[index + i] = 0;
                }
                res[index] = 0;
                visited[i] = false;
            }

        }
        return false;
    }

	static private int[] constructDistancedSequenceSlow(int n) {
		int len=1+2*(n-1);
		if (len==1)
			return new int[] {1};
		else if (len==2)
			return new int[] {2,1,2};
		res=new int[len];
		process(new int[len], n);
		return res;
    }

	static private void process(int[] arr, int n) {
		int len=arr.length;
		if (n==1) {
			for (int i=0;i<len;i++) {
				if (arr[i]==0) {
					arr[i]=1;
					max(arr);
					arr[i]=0;
				}
			}
		}
		else {
			for (int i=0;i<len-n;i++) {
				if (arr[i]==0 && arr[i+n]==0) {
					arr[i]=n;
					arr[i+n]=n;
					process(arr,n-1);
					arr[i]=0;
					arr[i+n]=0;
				}
			}
		}
	}

	static private void max(int[] arr) {
		int len=res.length;
		for (int i=0;i<len;i++) {
			if (res[i]>arr[i])
				break;
			if (res[i]<arr[i]) {
				res=Arrays.copyOf(arr, len);
				return;
			}
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(Arrays.toString(constructDistancedSequence(14)));
	}
}
