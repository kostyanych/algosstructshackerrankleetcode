package kostyanych.leetcode;

import java.util.Arrays;

/**
 * You are given a string s, a split is called good if you can split s into 2 non-empty strings p and q
 *    where its concatenation is equal to s and the number of distinct letters in p and q are the same.
 *
 * Return the number of good splits you can make in s.
 *
 * Example:
 * Input: s = "aacaba"
 * Output: 2
 * Explanation: There are 5 ways to split "aacaba" and 2 of them are good.
 * ("a", "acaba") Left string and right string contains 1 and 3 different letters respectively.
 * ("aa", "caba") Left string and right string contains 1 and 3 different letters respectively.
 * ("aac", "aba") Left string and right string contains 2 and 2 different letters respectively (good split).
 * ("aaca", "ba") Left string and right string contains 2 and 2 different letters respectively (good split).
 * ("aacab", "a") Left string and right string contains 3 and 1 different letters respectively.
 *
 * Constraints:
 * s contains only lowercase English letters.
 * 1 <= s.length <= 10^5
 *
 */
public class NumberOfGoodWaysToSplitAString {

	public static int numSplits(String s) {
		int len = s.length();
		if (len<2)
			return 0;
		int[][] cnt = new int[len][];
		int[] buf=new int[26];
		int c=s.charAt(0)-'a';
		buf[c]++;
		cnt[0]=buf;

		for (int i=1;i<len;i++) {
			cnt[i]=Arrays.copyOf(cnt[i-1], 26);
			c=s.charAt(i)-'a';
			cnt[i][c]++;
		}

		int res=0;
		for (int i=1;i<len;i++) {
			if (countInOne(cnt[i-1]) == countInTwo(cnt[len-1],cnt[i-1]))
				res++;
		}
		return res;
	}

	private static int countInOne(int[] arr) {
		int res=0;
		for (int i : arr) {
			if (i!=0)
				res++;
		}
		return res;
	}

	private static int countInTwo(int[] last, int[] prev) {
		int res=0;
		for (int i=0;i<26;i++) {
			if (last[i]-prev[i]!=0)
				res++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s = "aacaba";
		System.out.println(s + "-> " + numSplits(s));
		s = "abcd";
		System.out.println(s + "-> " + numSplits(s));
		s = "aaaaa";
		System.out.println(s + "-> " + numSplits(s));
		s = "acbadbaada";
		System.out.println(s + "-> " + numSplits(s));

	}

}
