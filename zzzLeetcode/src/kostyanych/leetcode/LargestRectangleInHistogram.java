package kostyanych.leetcode;

import java.util.Stack;

/**
 * Given n non-negative integers representing the histogram's bar height where the width of each bar is 1,
 * find the area of largest rectangle in the histogram.
 *
 */
public class LargestRectangleInHistogram {

	static private int largestRectangleArea(int[] heights) {
		if(heights == null || heights.length == 0)
			return 0;
		int len = heights.length;

		Stack<Integer> st = new Stack<>();
		st.push(0);
		int top=-1;
		int res=heights[0];
		//using the stack to find the last number which smaller than heights[i]
		for(int i=1; i<len; i++) {
			if (st.isEmpty() || heights[st.peek()]<=heights[i]) {
				st.push(i);
				continue;
			}

			while(!st.isEmpty() && heights[st.peek()]>heights[i]) {
				top=st.pop();
				int cur = 0;
				if (st.isEmpty())
					cur=heights[top]*i;
				else
					cur=heights[top]*(i-st.peek()-1);
				res=Math.max(res, cur);
			}
			st.push(i);
		}

		while (!st.isEmpty()) {
			top=st.pop();
			int cur = 0;
			if (st.isEmpty())
				cur=heights[top]*len;
			else
				cur=heights[top]*(len-st.peek()-1);
			res=Math.max(res, cur);
		}

		return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {2,4,4,1};
		System.out.println(largestRectangleArea(arr));
	}

}
