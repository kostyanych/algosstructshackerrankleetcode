package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A string S of lowercase English letters is given.
 * We want to partition this string into as many parts as possible so that each letter appears in at most one part,
 * and return a list of integers representing the size of these parts.
 *
 * Note:
 * S will have length in range [1, 500].
 * S will consist of lowercase English letters ('a' to 'z') only.
 */
public class PartitionLabels {

	private static List<Integer> partitionLabels(String S) {
		int[] last = new int[26];
		int len=S.length();
		for (int i=0;i<len;i++) {
			last[S.charAt(i)-'a']=i;
		}

		List<Integer> res = new ArrayList<>();
		int curLast=last[S.charAt(0)-'a'];
		int curFirst=0;
		for (int i=1;i<len;i++) {
			int ind=S.charAt(i)-'a';
			if (i>curLast) {
				res.add(curLast-curFirst+1);
				curFirst=i;
				curLast=last[ind];
			}
			else
				curLast=Math.max(curLast, last[ind]);
		}
		res.add(curLast-curFirst+1);
		return res;
	}


	private static List<Integer> partitionLabelsSlow(String S) {
		List<Integer> res = new ArrayList<>();
		Map<Character, int[]> intervals = new HashMap<>();
		int len=S.length();
		for (int i=0;i<len;i++) {
			char c=S.charAt(i);
			int[] intvl=intervals.get(c);
			if (intvl==null) {
				intvl=new int[2];
				intvl[0]=i;
				intvl[1]=i;
				intervals.put(c, intvl);
			}
			else
				intvl[1]=i;
		}
		List<int[]> li = new ArrayList<>(intervals.values());
		int size=li.size();
		if (size<1)
			return res;
		li.sort((i1, i2) -> i1[0] == i2[0] ? i1[1] - i2[2] : i1[0] - i2[0]);
		int[] last = li.get(0);
		for (int i=1;i<size;i++) {
			int[] cur=li.get(i);
			if (cur[0]>last[1]) {
				res.add(last[1]-last[0]+1);
				last=cur;
			}
			else if (cur[1]>last[1])
				last[1]=cur[1];
		}
		res.add(last[1]-last[0]+1);
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("ababcbacadefegdehijhklij "+partitionLabels("ababcbacadefegdehijhklij"));
	}

}
