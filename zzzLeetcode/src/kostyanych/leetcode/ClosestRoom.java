package kostyanych.leetcode;

import java.util.Arrays;
import java.util.TreeSet;

/**
 * There is a hotel with n rooms.
 * The rooms are represented by a 2D integer array rooms where rooms[i] = [roomIdi, sizei] denotes that there is a room with room number roomIdi
 * and size equal to sizei.
 * Each roomIdi is guaranteed to be unique.
 *
 * You are also given k queries in a 2D array queries where queries[j] = [preferredj, minSizej].
 * The answer to the jth query is the room number id of a room such that:
 *
 * The room has a size of at least minSizej, and
 * abs(id - preferredj) is minimized, where abs(x) is the absolute value of x.
 *
 * If there is a tie in the absolute difference, then use the room with the smallest such id. If there is no such room, the answer is -1.
 *
 * Return an array answer of length k where answer[j] contains the answer to the jth query.
 *
 * Constraints:
 *
 * n == rooms.length
 * 1 <= n <= 10^5
 * k == queries.length
 * 1 <= k <= 10^4
 * 1 <= roomIdi, preferredj <= 10^7
 * 1 <= sizei, minSizej <= 10^7
 *
 */
public class ClosestRoom {

	static private int[] closestRoom(int[][] rooms, int[][] queries) {
		int n = rooms.length;
		int k = queries.length;
        Integer[] qIndices = new Integer[k];
        for (int i = 0; i < k; i++)
        	qIndices[i] = i;

        Arrays.sort(rooms, (r1, r2) -> Integer.compare(r2[1], r1[1])); //Sort by room size desc
        Arrays.sort(qIndices, (i1, i2) -> Integer.compare(queries[i2][1], queries[i1][1])); // Sort by query minSize desc

        TreeSet<Integer> availableRoomIds = new TreeSet<>();
        int[] res = new int[k];
        int i = 0;
        for (int index : qIndices) {
        	int qMinSize=queries[index][1];
        	int qPreferredId=queries[index][0];
            while (i < n && rooms[i][1] >= qMinSize) { // Add ids of the rooms with valid size
            	availableRoomIds.add(rooms[i][0]);
            	i++;
            }
            res[index] = findClosetRoomId(availableRoomIds, qPreferredId);
        }
        return res;
    }

	static private int findClosetRoomId(TreeSet<Integer> treeSet, int preferredId) {
		Integer floor = treeSet.floor(preferredId);
		Integer ceiling = treeSet.ceiling(preferredId);
		// int ansAbs = Integer.MAX_VALUE, ans = -1;
		Integer absDiff = null;
		int res = -1;
		if (floor != null) {
			res = floor;
			absDiff = preferredId - floor;
		}
		if (ceiling != null) {
			if (absDiff == null || absDiff > ceiling - preferredId)
				res = ceiling;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] rooms = {{1,4},{2,3},{3,5},{4,1},{5,2}};
		int[][] queries = {{2,3},{2,4},{2,5}};
		System.out.println(Arrays.toString(closestRoom(rooms, queries)));
	}

}
