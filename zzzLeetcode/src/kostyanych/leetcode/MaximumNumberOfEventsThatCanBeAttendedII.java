package kostyanych.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * You are given an array of events where events[i] = [startDayi, endDayi, valuei].
 * The ith event starts at startDay[i] and ends at endDay[i], and if you attend this event, you will receive a value of value[i].
 * You are also given an integer k which represents the maximum number of events you can attend.
 *
 * You can only attend one event at a time.
 * If you choose to attend an event, you must attend the entire event.
 * Note that the end day is inclusive: that is, you cannot attend two events where one of them starts and the other ends on the same day.
 *
 * Return the maximum sum of values that you can receive by attending events.
 *
 * Solution:
 * 1. Order events by startDay
 * 2. then just go from the 1st one and see (recursively) what maximum sum you can get from events that start after the current event's end
 * 3a. for some optimization use memoization (we use map for ith event and k)
 * 3b. for some more optimization use fast method of finding what events start after the current event's end (we use TreeMap for that)
 */
public class MaximumNumberOfEventsThatCanBeAttendedII {

	static private final Map<Integer,Map<Integer,Integer>> memo = new HashMap<>();
	static private final TreeMap<Integer,Integer> tmap = new TreeMap<>( (d1, d2)->(Integer.compare(d1, d2)));

	static private int maxValue(int[][] events, int k) {
		memo.clear();
		tmap.clear();
		Arrays.sort(events,(ev1, ev2)->(Integer.compare(ev1[0], ev2[0])));

        for (int i=0;i<events.length;i++) {
        	if (i==0)
        		tmap.put(events[i][0], i);
        	if (i>0 && events[i][0]!=events[i-1][0])
        		tmap.put(events[i][0], i);
        }

		return getMax(events,0,k);
    }

	static private int getMax(int[][] events, int index, int k) {
		if (k<1)
			return 0;
		int len=events.length;
		if (index==len-1)
			return events[index][2];
		Map<Integer,Integer> mmap=memo.get(index);
		Integer res=null;
		if (mmap!=null) {
			res=mmap.get(k);
			if (res!=null)
				return res;
		}
		int max=0;
		for (int i=index;i<len;i++) {
        	int[] event=events[i];
        	int cur=event[2];
        	Integer nextKey=tmap.higherKey(events[i][1]);
        	if (nextKey!=null)
        		cur+=getMax(events,tmap.get(nextKey),k-1);
        	max=Math.max(max, cur);
		}
		if (mmap==null) {
			mmap=new HashMap<>();
			memo.put(index, mmap);
		}
		mmap.put(k, max);
		return max;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] events= {{1,1,1},{2,2,2},{3,3,3},{4,4,4}};
		System.out.println(maxValue(events,3));
	}

}
