package kostyanych.leetcode;

import java.util.Arrays;
import java.util.Stack;

/**
 * Given an array of n integers nums, a 132 pattern is
 * 	a subsequence of three integers nums[i], nums[j] and nums[k] such that
 * 	i < j < k and
 * nums[i] < nums[k] < nums[j].
 *
 * Return true if there is a 132 pattern in nums, otherwise, return false.
 *
 * Follow up: The O(n^2) is trivial, could you come up with the O(n logn) or the O(n) solution?
 *
 * Constraints:
 * 	n == nums.length
 * 	1 <= n <= 10^4
 * -10^9 <= nums[i] <= 10^9
 *
 */
public class A132Pattern {

	static private boolean find132pattern(int[] nums) {
        int len=nums.length;
        if (len<3)
            return false;

        Stack<Integer> st = new Stack<>();
        int middle = Integer.MIN_VALUE;

        for (int i = len - 1; i >= 0; i--) {
            if (nums[i] < middle)
                return true;
            while (!st.isEmpty()) {
                if (nums[i] <= st.peek ())
                    break;
                middle = st.pop();
            }
            st.push(nums [i]);
        }
        return false;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {7,6,4,1,2};
		System.out.println(Arrays.toString(arr));
		System.out.println(find132pattern(arr));
	}
}
