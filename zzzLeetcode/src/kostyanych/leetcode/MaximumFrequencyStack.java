package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Implement FreqStack, a class which simulates the operation of a stack-like data structure.
 *
 * FreqStack has two functions:
 *
 * push(int x), which pushes an integer x onto the stack.
 * pop(), which removes and returns the most frequent element in the stack.
 * If there is a tie for most frequent element, the element closest to the top of the stack is removed and returned.
 *
 * Note:
 *
 * Calls to FreqStack.push(int x) will be such that 0 <= x <= 10^9.
 * It is guaranteed that FreqStack.pop() won't be called if the stack has zero elements.
 * The total number of FreqStack.push calls will not exceed 10000 in a single test case.
 * The total number of FreqStack.pop calls will not exceed 10000 in a single test case.
 * The total number of FreqStack.push and FreqStack.pop calls will not exceed 150000 across all test cases.
 *
 */
public class MaximumFrequencyStack {

	Map<Integer, MutableInt> freqMap = new HashMap<>();
    Map<Integer, Stack<Integer>> valueStacks = new HashMap<>();
    int maxFreq=0;

    static class MutableInt {
        int val;

        public MutableInt() {
            val=1;
        }

        public MutableInt inc() {
            val++;
            return this;
        }

        public void dec() {
            val--;
        }
    }

    public void push(int x) {
        int freq = freqMap.compute(x, (k,v) -> v==null ? new MutableInt() : v.inc()).val;
        if (freq > maxFreq)
            maxFreq = freq;

        valueStacks.computeIfAbsent(freq, k -> new Stack<>()).push(x);
    }

    public int pop() {
        int x = valueStacks.get(maxFreq).pop();
        freqMap.get(x).dec();
        if (valueStacks.get(maxFreq).size() == 0)
            maxFreq--;
        return x;
    }

    public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;

		 MaximumFrequencyStack st = new MaximumFrequencyStack();
		 st.push(4);
		 st.push(0);
		 st.push(9);
		 st.push(3);
		 st.push(4);
		 st.push(2);
		 System.out.println(st.pop());
		 st.push(6);
		 System.out.println(st.pop());
		 st.push(1);
		 System.out.println(st.pop());
		 st.push(1);
		 System.out.println(st.pop());
		 st.push(4);
		 System.out.println(st.pop());
		 System.out.println(st.pop());
		 System.out.println(st.pop());
		 System.out.println(st.pop());
		 System.out.println(st.pop());
		 System.out.println(st.pop());
    }

}
