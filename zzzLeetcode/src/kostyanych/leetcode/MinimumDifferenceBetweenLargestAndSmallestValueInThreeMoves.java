package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given an array nums, you are allowed to choose one element of nums and change it by any value in one move.
 * Return the minimum difference between the largest and smallest value of nums after perfoming at most 3 moves.
 *
 * Solution:
 * Anything less than 5 numbers can be changed to equal numbers in 3 moves.
 * So, the answer is 0.
 *
 * For anything else we should only consider min and max values.
 * There are these cases:
 * 1. min = max (change anything 3 times and return 0)
 * 2. change 3 first mins to max and consider (max - 4th min)
 * 3. change 3 last maxes to min and consider (4th last max - min)
 * 4. change 2 first mins and 1 last max to 3rd min and consider (2nd last max - 3rd min)
 * 5. change 1 first min and 2 last maxes to 2nd min and consider (3rd last max - 2nd min)
 * Obviously choose min between 2-5
 *
 */
public class MinimumDifferenceBetweenLargestAndSmallestValueInThreeMoves {

	 public static int minDifference(int[] nums) {
		 int len=nums.length;
		 if (len<=4)
			 return 0;
		 Arrays.sort(nums);
		 if (nums[0]==nums[len-1])
			 return 0;
		 int min=nums[len-1]-nums[3];
		 min = Math.min(min, nums[len-4]-nums[0]);
		 min = Math.min(min, nums[len-2]-nums[2]);
		 min = Math.min(min, nums[len-3]-nums[1]);
		 return min;
	 }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr= {20,66,68,57,45,18,42,34,37,58};
		//Arrays.sort(arr);
		System.out.println(Arrays.toString(arr));
		System.out.println(minDifference(arr));
//		System.out.println(hasAllCodes(s, k));
	}


}
