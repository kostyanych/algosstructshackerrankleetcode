package kostyanych.leetcode;

import java.util.Arrays;

/**
 * You have an initial power of P, an initial score of 0, and a bag of tokens where tokens[i] is the value of the ith token (0-indexed).
 * Your goal is to maximize your total score by potentially playing each token in one of two ways:
 * 		If your current power is at least tokens[i], you may play the ith token face up, losing tokens[i] power and gaining 1 score.
 * 		If your current score is at least 1, you may play the ith token face down, gaining tokens[i] power and losing 1 score.
 *
 * Each token may be played at most once and in any order. You do not have to play all the tokens.
 *
 * Return the largest possible score you can achieve after playing any number of tokens.
 *
 * Constraints:
 * 	0 <= tokens.length <= 1000
 * 	0 <= tokens[i], P < 10^4
 *
 */
public class BagOfTokens {

	static private int bagOfTokensScore(int[] tokens, int p) {
        int len=tokens.length;
        if (len<1)
            return 0;
        Arrays.sort(tokens);
        int score=0;
        int l=0;
        int r=len-1;
        while (l<=r) {
            if (tokens[l]<=p) {
                p-=tokens[l];
                score++;
                l++;
            }
            else {
                if (l>=r-1 || score<1)
                    break;
                p+=tokens[r];
                score--;
                r--;
            }
        }
        return score;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {71,55,82};
		System.out.println(Arrays.toString(arr));
		System.out.println(bagOfTokensScore(arr, 54));
	}
}
