package kostyanych.leetcode.runners;

import kostyanych.leetcode.ClosestRoom;
import kostyanych.leetcode.FurthestBuildingYouCanReach;
import kostyanych.leetcode.KnightProbabilityInChessboard;
import kostyanych.leetcode.OpenTheLock;
import kostyanych.leetcode.RankTransformOfAMatrix;
import kostyanych.leetcode.SumOfFlooredPairs;
import kostyanych.leetcode.Zadacha3;
import kostyanych.leetcode.Zadacha4;
import kostyanych.leetcode.arrays.CountOfSmallerNumbersAfterSelf;
import kostyanych.leetcode.arrays.DescribeThePainting;
import kostyanych.leetcode.arrays.MaximumErasureValue;
import kostyanych.leetcode.arrays.MaximumLengthOfRepeatedSubarray;
import kostyanych.leetcode.arrays.NumberOfSubmatricesThatSumToTarget;
import kostyanych.leetcode.arrays.NumberOfTheSmallestUnoccupiedChair;
import kostyanych.leetcode.arrays.NumberOfVisiblePeopleInAQueue;
import kostyanych.leetcode.arrays.RangeSumQuery2DImmutable;
import kostyanych.leetcode.arrays.ValidTriangleNumber;
import kostyanych.leetcode.dp.CombinationSumIV;
import kostyanych.leetcode.dp.DecodeWaysII;
import kostyanych.leetcode.dp.MinimumTotalSpaceWastedWithKResizingOperations;
import kostyanych.leetcode.graphs.CriticalConnectionsInANetwork;
import kostyanych.leetcode.linkedlists.ConvertSortedListToBinarySearchTree;
import kostyanych.leetcode.numbers.PowerfulIntegers;
import kostyanych.leetcode.strings.FlipStringToMonotoneIncreasing;
import kostyanych.leetcode.strings.ManachersPalindrome;
import kostyanych.leetcode.strings.MaximumProductOfTheLengthOfTwoPalindromicSubstrings;
import kostyanych.leetcode.strings.PalindromePartitioningII;
import kostyanych.leetcode.strings.RemoveAllAdjacentDuplicatesInString;
import kostyanych.leetcode.strings.SearchSuggestionsSystem;
import kostyanych.leetcode.trees.ConstructBinaryTreeFromPreorderAndInorderTraversal;
import kostyanyxh.zzz.javatests.ArrayDequeSpeed;
import kostyanyxh.zzz.javatests.DateTimes;
import kostyanyxh.zzz.javatests.ExecutorsTest;
import kostyanyxh.zzz.javatests.Flats;
import kostyanyxh.zzz.javatests.JDBCTests;
import kostyanyxh.zzz.javatests.Nio;
import kostyanyxh.zzz.javatests.PhaserTest;


public class Runner {

	public static void main(String[] args) {
		NumberOfSubmatricesThatSumToTarget.testMe(false);
		CombinationSumIV.testMe(false);
		CriticalConnectionsInANetwork.testMe(false);
		FurthestBuildingYouCanReach.testMe(false);
		PowerfulIntegers.testMe(false);
		ConvertSortedListToBinarySearchTree.testMe(false);
		RangeSumQuery2DImmutable.testMe(false);
		ClosestRoom.testMe(false);
		SumOfFlooredPairs.testMe(false);
		MaximumErasureValue.testMe(false);
		Zadacha3.testMe(false);
		SearchSuggestionsSystem.testMe(false);
		OpenTheLock.testMe(false);
		ConstructBinaryTreeFromPreorderAndInorderTraversal.testMe(false);

		KnightProbabilityInChessboard.testMe(false);
		CountOfSmallerNumbersAfterSelf.testMe(false);

		RemoveAllAdjacentDuplicatesInString.testMe(false);

		ArrayDequeSpeed.testMe(false);
		Flats.testMe(false);
		DateTimes.testMe(false);

		MaximumLengthOfRepeatedSubarray.testMe(false);
		DecodeWaysII.testMe(false);

		Nio.testMe(false);

		ValidTriangleNumber.testMe(false);

		PhaserTest.testMe(false);
		ExecutorsTest.testMe(false);

		NumberOfTheSmallestUnoccupiedChair.testMe(false);
		DescribeThePainting.testMe(false);

		JDBCTests.testMe(false);

		NumberOfVisiblePeopleInAQueue.testMe(false);

		Zadacha4.testMe(false);

		PalindromePartitioningII.testMe(false);

		RankTransformOfAMatrix.testMe(false);

		ManachersPalindrome.testMe(false);

		MaximumProductOfTheLengthOfTwoPalindromicSubstrings.testMe(true);
		MinimumTotalSpaceWastedWithKResizingOperations.testMe(false);

		FlipStringToMonotoneIncreasing.testMe(false);

	}

}
