package kostyanych.leetcode;

import java.util.Arrays;

public class JumpGameV {

	private static int[] origArr;
    private static int[] jumps;
    private static int distance=0;
    
    private static int maxJumps(int[] arr, int d) {
        jumps=new int[arr.length];
        origArr=arr;
        distance=d;
        Arrays.fill(jumps, -1);
        for (int i=0;i<arr.length;i++) {
            if (jumps[i]<0)
                calculateJumps(i);
        }
        int max=0;
        for (int i=0;i<arr.length;i++) {
            if (jumps[i]>max)
                max=jumps[i];
        }
        return max;
    }
    
    private static void calculateJumps(int ind) {
        int max=0;
        boolean canGo=false;
        for (int i=ind+1;i<=ind+distance;i++) {
            if (i>=jumps.length)
                break;
            if (origArr[i]>=origArr[ind])
                break;
            canGo=true;
            if (jumps[i]<0)
                calculateJumps(i);
            if (max<jumps[i])
                max=jumps[i];
        }
         for (int i=ind-1;i>=ind-distance;i--) {
            if (i<0)
                break;
            if (origArr[i]>=origArr[ind])
                break;
            canGo=true;
            if (jumps[i]<0)
                calculateJumps(i);
            if (max<jumps[i])
                max=jumps[i];
        }
        if (canGo)
            max++;
        jumps[ind]=max;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
    	int[] arr = {6,4,14,6,8,13,9,7,10,6,12};
    	int d=2;
    	System.out.println(maxJumps(arr,d));
    }
}
