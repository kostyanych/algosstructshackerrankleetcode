package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Given a string containing digits from 2-9 inclusive, 
 * return all possible letter combinations that the number could represent.
 */
public class LetterCombinationsPhoneNumber {

	private final static String[][] chars = {
	        {},{},
	        {"a","b","c"},
	        {"d","e","f"},
	        {"g","h","i"},
	        {"j","k","l"},
	        {"m","n","o"},
	        {"p","q","r","s"},
	        {"t","u","v"},
	        {"w","x","y","z"}
	};
	
	private static List<String> letterCombinations(String digits) {
		List<String> res = new ArrayList<>();
		int len=digits.length();
		for (int i=len-1;i>=0;i--) {
			int num=Character.getNumericValue(digits.charAt(i));
			res=addDigit(res,chars[num]);
		}
		return res;
    }
	
	private static List<String> addDigit(List<String> prev, String[] chrs) {
		List<String> res = new ArrayList<>();
		for (int i=0;i<chrs.length;i++) {
			if (prev.size()<1)
				res.add(chrs[i]);
			else {
				for (String p : prev) {
					res.add(chrs[i]+p);
				}
			}
		}
		prev.clear();
		return res;
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		List<String> res=letterCombinations("7");
		
		System.out.println(Arrays.toString(res.toArray()));
	}
	    
}
