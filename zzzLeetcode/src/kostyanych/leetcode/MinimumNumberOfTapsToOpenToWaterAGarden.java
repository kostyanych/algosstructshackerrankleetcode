package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * There is a one-dimensional garden on the x-axis. The garden starts at the point 0 and ends at the point n. (i.e The length of the garden is n).
 * There are n + 1 taps located at points [0, 1, ..., n] in the garden.
 *
 * Given an integer n and an integer array ranges of length n + 1 where ranges[i] (0-indexed) means
 * 		the i-th tap can water the area [i - ranges[i], i + ranges[i]] if it was open.
 *
 * Return the minimum number of taps that should be open to water the whole garden, If the garden cannot be watered return -1.
 *
 * Constraints:
 *
 * 1 <= n <= 10^4
 * ranges.length == n + 1
 * 0 <= ranges[i] <= 100
 *
 */
public class MinimumNumberOfTapsToOpenToWaterAGarden {


	/**
	 * 1. the tap can reach i+range[i] to the right
	 * 2. the tap can reach i-range[i] to the left
	 * 3. each point i can be covered from the right by some tap
	 * 4. we find the leftmost point X that the tap at point J can reach
	 * 5. for such a point we set the max of overall reach from it's own reach and reach of tap at point J
	 * 5a. do 4 and 5 for all points from left to right
	 * 6. obviously we thus get covered area from X to J's rightmost reach: J+reach[J]
	 * 6a. Note that we don't care here for the left reach of X, because we will get there from the left anyway
	 * 7. in the interval fro X to J+reach[J] we will look for a point with furthermost reach to the right
	 * 8. no we look at the interval [previous furthermost; new furthermost]
	 * 9. we need at least 1 tap open (can't have garden less than 1 size)
	 * 10. every time we transition the point at 7-8 we need to turn on the new tap
	 * 11. if our current futhermost reach is further than the garden end, we got enough taps
	 * @param ranges
	 * @return
	 */
	static private int minTaps(int n, int[] ranges) {
		int len=ranges.length;
		int[] maxJump = new int[len];
		for (int i=0;i<len;i++) {
			int right=i+ranges[i];
			maxJump[i]=right;
			int left=Math.max(0, i-ranges[i]);
			maxJump[left]=Math.max(maxJump[left], right);
		}
		if (maxJump[0]==0)
			return -1;
		int res=1;
		int prevMax=0;
		int curMax=maxJump[0];
		while(curMax<n){
            int maxIndex=curMax;
            for (int i=prevMax; i<=curMax; i++) {
                maxIndex = Math.max(maxIndex,maxJump[i]);
            }
            if (maxIndex==curMax)
            	return -1;
            prevMax = curMax;
            curMax = maxIndex;
            res++;
        }
        return res;

	}

	static private int minTapsSlow(int n, int[] ranges) {
        List<int[]> rngList = new ArrayList<>();
        for (int i=0;i<ranges.length;i++) {
            int range=ranges[i];
            if (range==0)
                continue;
            int l = i-range <0 ? 0 : i-range;
            int r = i+range > n ? n : i+range;
            rngList.add(new int[]{l,r});
        }
        if (rngList.size()<1)
            return -1;
        rngList.sort( (a1, a2) -> a1[0] == a2[0] ? Integer.compare(a2[1],a1[1]) :  Integer.compare(a1[0],a2[0]));
        int[] range=rngList.get(0);
        if (range[0]>0)
            return -1;
        int max=range[1];
        if (max==n)
            return 1;
        int ind=0;
        int left=0;
        int count=1;
        int maxCandidate=max;
        int indCandidate=0;
        for (int i=1;i<rngList.size();i++) {
            range=rngList.get(i);
            if (range[0]==max) {
            	if (range[1]>=n)
            		return count+1;
            	if (range[1]>maxCandidate) {
                	max=range[1];
                	maxCandidate=max;
                	ind=i;
                	indCandidate=ind;
                	left=range[0];
                	count++;
                }
            }
            if (range[0]>max) {
                if (range[0]>maxCandidate)
                    return -1;
                if (ind!=indCandidate) {
                	max=maxCandidate;
                	ind=indCandidate;
                	left=rngList.get(ind)[0];
                	count++;
                }
            }
            if (range[1]>maxCandidate){
            	maxCandidate=range[1];
                indCandidate=i;
                if (maxCandidate>=n)
                	return count+1;
            }
/*
            if (range[0]==left)
                continue;
            if (range[0]<max) {
                if (range[1]>maxCandidate){
                    maxCandidate=range[1];
                    indCandidate=i;
                    if (maxCandidate>=n)
                        return count+1;
                }
            }
            else { //range[0] >= max
                if (range[0]>maxCandidate)
                    return -1;
                max=maxCandidate;
                ind=indCandidate;
                left

            }
*/
        }
        return -1;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {2,4,0,0,2,0,0};
		System.out.println(minTaps(6, arr));
	}

}
