package kostyanych.leetcode;

/**
 * We are playing the Guess Game. The game is as follows:
 *
 * I pick a number from 1 to n. You have to guess which number I picked.
 * Every time you guess wrong, I'll tell you whether the number I picked is higher or lower.
 * However, when you guess a particular number x, and you guess wrong, you pay $x. You win the game when you guess the number I picked.
 *
 */
public class GuessNumberHigherOrLowerII {

	private static int[][] memo;

	private static int getMoneyAmount(int n) {
		memo = new int[n + 1][n + 1];

		for (int i = 1; i < n; i++) {
			memo[i][i] = i;
			memo[i][i + 1] = i;
			if (i > 1 && i < n - 1)
				memo[i - 1][i + 1] = i;
		}
		memo[n][n] = n;
		calcMemo(1, n);

		return memo[1][n];
	}

	private static int calcMemo(int l, int r) {
		if (l >= r)
			return 0;

		if (memo[l][r] != 0)
			return memo[l][r];

		for (int i = l; i <= r; i++) {
			int calcLeft = calcMemo(l, i - 1);
			int calcRight = calcMemo(i + 1, r);
			int max = Math.max(i + calcLeft, i + calcRight);
			if (memo[l][r] == 0)
				memo[l][r] = max;
			else
				memo[l][r] = Math.min(memo[l][r], max);
		}
		return memo[l][r];
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		System.out.println(4+" -> "+getMoneyAmount(4));
		System.out.println(10+" -> "+getMoneyAmount(10));
	}

}
