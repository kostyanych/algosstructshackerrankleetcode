package kostyanych.leetcode;

/**
 * Given a 2D binary matrix filled with 0's and 1's, find the area of the largest square containing only 1's and return its area.
 */
public class MaximalSquare {

	private static int maximalSquare(char[][] mx) {
        int rows = mx.length;
        if (rows==0)
            return 0;
        int cols = mx[0].length;
        if (cols==0)
            return 0;
        int[][] arr = new int[rows+1][cols+1];

        int res=0;
        for (int i=1; i<=rows;i++) {
            for (int j=1; j<=cols;j++) {
                if (mx[i-1][j-1]=='0')
                    continue;
                arr[i][j]=Math.min(arr[i][j-1],Math.min(arr[i-1][j-1],arr[i-1][j]))+1;
                res=Math.max(res,arr[i][j]);
            }
        }
        return res*res;
    }

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		 char[][] arr = {
				 {'1','0','1','0','0'},
				 {'1','0','1','1','1'},
				 {'1','1','1','1','1'},
				 {'1','0','0','1','0'}
		 	};
		 System.out.println(maximalSquare(arr));
	 }
}
