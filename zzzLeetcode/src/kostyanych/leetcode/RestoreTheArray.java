package kostyanych.leetcode;

import java.util.Arrays;

/**
 * A program was supposed to print an array of integers.
 * The program forgot to print whitespaces and the array is printed as a string of digits
 *    and all we know is that all integers in the array were in the range [1, k] and there are no leading zeros in the array.
 *
 * Given the string s and the integer k. There can be multiple ways to restore the array.
 *
 * Return the number of possible array that can be printed as a string s using the mentioned program.
 *
 * The number of ways could be very large so return it modulo 10^9 + 7
 *
 * Constraints:
 *   1 <= s.length <= 10^5.
 *   s consists of only digits and doesn't contain leading zeros.
 *   1 <= k <= 10^9.
 */
public class RestoreTheArray {

	private final static int MOD=1_000_000_007;

	/**
	 * 1. per problem statement there is at least 1 valid solution
	 * 2. for every number X of length l starting at pos i:
	 *    X is valid
	 *          if s[i] != '0' (no leading zeroes allowed for numbers and numbers>=1)
	 *             (for example: 101 - X=0 and X=01 at pos 1 are not valid
	 *      AND if X<=k
	 *      AND if at least 1 valid number exists from position i+l
	 *             (for example: 101 - X=1 at pos 0 is not valid, because at pos 1 there is a Y=0 or Y=01 which are both not valid)
	 * 3. any otherwise valid number is valid if comes directly at the end of the string
	 *             (for example, for string "3012" and k=40 both 2 and 12 are valid)
	 * 4. for every position I for every VALID number of len L the possible number W(I,L) of ways to restore is W(I+L)
	 * 4a.just sum W(I) for every valid number from this position
	 * 5. final result would be sum(W(0,L))
	 *
	 * NOTE: one must guard against X being more than int can hold, so work with longs before storing W(I)%MOD
	 */
	private static int numberOfArrays(String s, int k) {
		int len = s.length();
		int klen=getIntLen(k);
		long[] arr = new long[len+1];
		arr[len]=1;
		for (int i=len-1;i>=0;i--) {
			if (s.charAt(i)=='0')
				continue;
			int upperBound=Math.min(len-1, i+klen);
			long curNum=0;
			for (int j=i;j<=upperBound;j++) {
				int c=s.charAt(j)-'0';
				curNum=10*curNum+c;
				if (curNum<1 || curNum>k)
					continue;
				arr[i]=(arr[i]+arr[j+1])%MOD;
			}
		}
        return (int)arr[0];
	}

	private static int getIntLen(int n) {
		if (n<10)
			return 1;
		int res=0;
		while (n>0) {
			res++;
			n/=10;
		}
		return res;
	}


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s = "958119747063";
		int k = 1000000000;
//		String s="13112";
//		int k=30;
		System.out.println(s+", "+k+" -> "+numberOfArrays(s,k));
	}
}
