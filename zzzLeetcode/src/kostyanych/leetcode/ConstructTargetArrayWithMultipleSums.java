package kostyanych.leetcode;

import java.util.Collections;
import java.util.PriorityQueue;

/**
 * Given an array of integers target. From a starting array, A consisting of all 1's, you may perform the following procedure :
 * 
 * let x be the sum of all elements currently in your array.
 * choose index i, such that 0 <= i < target.size and set the value of A at index i to x.
 * You may repeat this procedure as many times as needed.
 * 
 * Return True if it is possible to construct the target array from A otherwise return False.
 */
public class ConstructTargetArrayWithMultipleSums {

	private static boolean isPossible(int[] target) {
		if (target.length==1) {
			if (target[0]==1)
				return true;
			else
				return false;
		}
	    long sum = 0;
	    PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
	    for (int num : target) {
	        sum += num;
	        pq.add(num);
	    }
	    //biggest number BN = previous bn+all others;
	    //BN+all others=sum
	    //allothers=sum-BN
	    //previos bn must be >=1 (we start with ones)
	    //previos bn=BN-allothers=BN-(sum-BN)=2BN-sum
	    //2BN-sum>=1 => 2BN-sum>0
	    //BN>sum/2 (if not, we're can't go on)
	    while (pq.peek() > sum / 2) {
	        int BN = pq.remove();
	        if (sum - BN == 1)
	        	return true;
//if BN much bigger than allothers, we'll have to do a lot of cycles if we just do (BN-allothers)
//but in that case BN=prev+allothers=(prev[-1]+allothers)+allothers=((prev[-2]+allothers)+allothers)+allothers
//ie BN=prev[-n]+n*allothers => prev[-n]=BN%allothers = BN%(sum-BN)	        
	        int prevBN = BN % (int)(sum - BN);
	        if (prevBN<1)
	        	return false;
	        pq.add(prevBN);
	        sum += prevBN - BN;
	    }
	    return sum == target.length;
	}	
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {3,5,1};
		long t1=System.nanoTime();
		boolean res=isPossible(arr);
		long t2=System.nanoTime();
		System.out.println(""+res+" in "+(t2-t1));
		
	}
}
