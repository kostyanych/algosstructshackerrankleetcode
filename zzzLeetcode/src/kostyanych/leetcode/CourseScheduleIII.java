package kostyanych.leetcode;

import java.util.Arrays;
import java.util.PriorityQueue;

import kostyanych.leetcode.helpers.Printer;

/**
 * There are n different online courses numbered from 1 to n.
 * Each course has some duration(course length) t and closed on dth day.
 * A course should be taken continuously for t days and must be finished before or on the dth day.
 * You will start at the 1st day.
 *
 * Given n online courses represented by pairs (t,d), your task is to find the maximal number of courses that can be taken.
 *
 * Note:
 *   The integer 1 <= d, t, n <= 10,000.
 *   You can't take two courses simultaneously.
 *
 * The solurion idea is simple:
 * 1. we process courses based on their end date. Sort ascending.
 * 2. if the time taken for already processed courses + duration of a new course is <= this new course's end date, mark it as taken, go to next course
 * 3. if not, find the longest course from already processed ones. If it's shorter than the new one, there's nothing we can do, the new one won't be taken.
 *    If it's longer than the new one, remove it from taken ones and put the new one in its place. Thus we change 1 old course for 1 new course (no loss here)
 *    but the overall duration of courses taken gets smaller, that could potentially allow us to squeeze more courses further down the timeline.
 *
 * Use PriorityQuery for storing courses taken, since it allows for the longest one to always be at the front.
 *
 */
public class CourseScheduleIII {

	private static int scheduleCourse(int[][] courses) {

		// sort the courses by their end_time ascending
		Arrays.sort(courses, (c1, c2) -> c1[1] - c2[1]);

		// longest course to the front
		PriorityQueue<int[]> pq = new PriorityQueue<>((c1, c2) -> c2[0] - c1[0]);

		int elapsed = 0;
		for (int[] course : courses) {

			// new course is ok with constraints
			if (elapsed + course[0] <= course[1]) {
				pq.add(course);
				elapsed += course[0];
			} else if (!pq.isEmpty()) {
				// new course couldn't be taken yet
				int[] longestCourse = pq.peek();
				if (longestCourse[0] > course[0]) { // we already took a longer course. let's untake it and change it
													// with this, shorter one
					elapsed -= longestCourse[0];
					elapsed += course[0];
					pq.poll(); // remove that longest previous course
					pq.add(course); // add new course
				}
				// if not, there's nothing we can do, just skip this new course
			}
		}
		return pq.size();
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
    	int[][] arr = {{100, 200}, {200, 1300}, {1000, 1250}, {2000, 3200}};
    	System.out.println(scheduleCourse(arr));
    	Printer.print2DArrayStream(arr, true);
    }


}
