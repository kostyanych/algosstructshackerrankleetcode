package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import kostyanych.leetcode.helpers.Generators;

/**
 * There are n people and 40 types of hats labeled from 1 to 40.
 *
 * Given a list of list of integers hats, where hats[i] is a list of all hats preferred by the i-th person.
 *
 * Return the number of ways that the n people wear different hats to each other.
 *
 * Since the answer may be too large, return it modulo 10^9 + 7.
 *
 */
public class NumberOfWaysToWearDifferentHatsToEachOther {

	private static final int MOD=1000000007;
	private int totalHats=5;
	private int people=0;
	private int maskAllDone=0;
	private List<Integer>[] hatsByPeople;

	public int numberWays(List<List<Integer>> hats) {
		this.people = hats.size();

		if (people==1)
			return hats.get(0).size();

		this.maskAllDone = (1<<people)-1;

		//from people->hats make hats->people data
		hatsByPeople = new List[totalHats+1];
		for (int i=0;i<people;i++) {
			List<Integer> personHats = hats.get(i);
			for (int hat : personHats) {
				if (hatsByPeople[hat]==null)
					hatsByPeople[hat] = new ArrayList<Integer>();
				hatsByPeople[hat].add(i);
			}
		}

		int maxdim=(1<<people)-1;
		int[][] memo = new int[maxdim][totalHats+1];
		for (int i=0;i<maxdim;i++) {
			Arrays.fill(memo[i], -1);
		}
		int res = count(memo, 0, 1); //start with NO people wearing any hats and 1st hat is one to start with
		return res;
	}

	/**
	 * Memo[mask][hatIndex] - shows how many ways there are for people in "mask" wearing hats up to hatIndex
	 * We going thru hats from 0 upwards, so at every new hat, all hats before it are alredy uesd in all possible ways.
	 * @param memo
	 * @param maskAlreadyWearing
	 * @param nextHat
	 * @return
	 */
	private int count(int[][] memo, int maskAlreadyWearing, int nextHat) {

		if (maskAlreadyWearing==this.maskAllDone) //already got a good result on previous hat, that's 1 variant, so return 1
			return 1;
		if (nextHat>this.totalHats) //no more hats
			return 0;

		if (memo[maskAlreadyWearing][nextHat]!=-1)
			return memo[maskAlreadyWearing][nextHat];

		// we can do 2 things here:
		// 1. skip this hat alltogether - nobody will wear it
		// 2. try to wear it (try it on everyone who can wear it)

		//1. don't wear it, try next one
		int ans = count(memo, maskAlreadyWearing, nextHat+1);

		//2. try this hat on everyone who can wear it
		List<Integer> people = hatsByPeople[nextHat];
		if (people!=null) {
			for (int person : people) {
				 if ( ((maskAlreadyWearing >> person) & 1) == 1 ) // this person is already wears some hat
		            	continue;
				int newMask=maskAlreadyWearing|(1<<person); //marking the bit for this person in our mask
				ans=(ans+count(memo, newMask, nextHat+1))%MOD;	//person p is now wearing hat nextHat, so we want to check the nextHat+1 for other people
			}
		}

		memo[maskAlreadyWearing][nextHat]=ans;
		return ans;
	}

	public int numberWaysSlow(List<List<Integer>> hats) {
		this.people = hats.size();

		if (people==1)
			return hats.get(0).size();

		Collections.sort(hats, (l1,l2)->l1.size()-l2.size());
		Set<Integer> set = new HashSet<>();
		int res=0;
		for (int i=0;i<hats.get(0).size();i++) {
			set.clear();
			set.add(hats.get(0).get(i));
			res=(dfsSlow(hats,1,people,set)%MOD+res)%MOD;
		}
		return res;
	}

	private static int dfsSlow(List<List<Integer>> hats, int ind, int n, Set<Integer> usedSet) {
		int res=0;
		List<Integer> ints = hats.get(ind);
		if (ind==n-1) { //last group
			for (Integer i : ints) {
				if (!usedSet.contains(i))
					res++;
			}
		}
		else {
			for (Integer i : ints) {
				if (!usedSet.contains(i)) {
					usedSet.add(i);
					res=(dfsSlow(hats, ind+1, n, usedSet)%MOD+res)%MOD;
					usedSet.remove(i);
				}
			}
		}
		return res%MOD;

	}

	private static int numberWaysDP(List<List<Integer>> hats, int hatsNum) {
        int n = hats.size();
        List<Integer>[] hatsByPeople = new List[hatsNum+1]; // h2p[i] indicates the list of people who can wear i_th hat
        for (int i = 1; i <= hatsNum; i++) {
        	hatsByPeople[i] = new ArrayList<>();
        }

        for (int i = 0; i < n; i++) {
            for (int hat : hats.get(i)) {
            	hatsByPeople[hat].add(i);
            }
        }
        Integer[][] dp = new Integer[hatsNum+1][1<<(n+1)];
        return dfsDP(hatsByPeople, (1 << n) - 1, 1, 0, dp, hatsNum);
    }

    // dfs(...hat, chosen...) number of ways to wear different hats given `chosen` mask (list of people were assigned hat)
    //     and used hats from 1 to `hat`-1.
    private static int dfsDP(List<Integer>[] hbp, int allMask, int hat, int chosen, Integer[][] dp, int hatsNum) {
        if (chosen == allMask)
        	return 1; // Check if assigned different hats to all people
        if (hat > hatsNum)
        	return 0; // no more hats to process
        if (dp[hat][chosen] != null)
        	return dp[hat][chosen];
        int ans = dfsDP(hbp, allMask, hat + 1, chosen, dp, hatsNum); // Don't wear this hat
        for (int p : hbp[hat]) {
            if (((chosen >> p) & 1) == 1) // Skip if person `p` was assigned hat
            	continue;
            ans += dfsDP(hbp, allMask, hat + 1, chosen | (1 << p), dp, hatsNum); // Wear this hat for p_th person
            ans %= MOD;
        }
        return dp[hat][chosen] = ans;
    }


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[][] arr = {{1,2},{2,3},{3}};
		int[][] arr = {{1},{2},{3,4,5}};
		int hats=3;
		List<List<Integer>> lst = Generators.arrToList(arr);
System.out.println(lst);
		NumberOfWaysToWearDifferentHatsToEachOther z=new NumberOfWaysToWearDifferentHatsToEachOther();
		System.out.println(z.numberWays(lst));
//		System.out.println(numberWaysDP(lst,hats));
	}
}