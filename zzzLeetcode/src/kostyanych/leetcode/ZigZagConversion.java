package kostyanych.leetcode;

/**
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
 *
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 *
 * (3 rows here)
 *
 * And then read line by line: "PAHNAPLSIIGYIR"
 *
 * Write the code that will take a string and make this conversion given a number of rows:
 * string convert(string s, int numRows);
 */
public class ZigZagConversion {

	private static String convert(String s, int numRows) {
        int len=s.length();
        if (len==0)
            return"";
        if (numRows==1)
        	return s;
        StringBuilder sb=new StringBuilder(len);
        int grpLen=2*numRows-2;
        int groupNum=len/grpLen;
        if (len%grpLen>0)
        	groupNum++;
        for (int row=0;row<numRows;row++) {
        	for (int i=0;i<groupNum;i++) {
            	if (row==0 || row==numRows-1) { //first and last rows - 1 char
            		int ind=row+grpLen*i;
            		if (ind<len)
            			sb.append(s.charAt(ind));
            	}
            	else { //all other rows get 2 chars
            		int ind=row+grpLen*i;
            		if (ind<len)
            			sb.append(s.charAt(ind));
            		ind=ind+2*(numRows-1-row);
            		if (ind<len)
            			sb.append(s.charAt(ind));
            	}
            }
        }
        return sb.toString();
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s="PAYPALISHIRING!";
//		System.out.println("s="+s+"  res[3]="+convert(s, 3));
//		System.out.println("s="+s+"  res[4]="+convert(s, 4));
		System.out.println("s="+s+"  res[25]="+convert(s, 25));
		System.out.println("s="+s+"  res[1]="+convert(s, 1));
		System.out.println("s="+s+"  res[2]="+convert(s, 2));
	}
}
