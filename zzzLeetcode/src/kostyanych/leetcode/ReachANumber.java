package kostyanych.leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * You are standing at position 0 on an infinite number line. There is a goal at position target.
 *
 * On each move, you can either go left or right. During the n-th move (starting from 1), you take n steps.
 *
 * Return the minimum number of steps required to reach the destination.
 *
 * Note:
 * target will be a non-zero integer in the range [-10^9, 10^9].
 *
 */
public class ReachANumber {

	static private int reachNumber(int target) {
        if (target==0)
            return 0;
        target=Math.abs(target);
        if (target==1)
            return 1;

        //using an ariphmetic progression sum: n*(1+n)/2 >= k
        //then quadratic non-equation: 1/2*n^2+1/2*n-k>=0;
        //n>=sqrt(1/4+2k)-1/2
        int n=(int)Math.ceil( Math.sqrt(0.25+2*target)-0.5 );

        int sum=n*(n+1)/2;

        if (sum==target)
        	return n;

        int dif = target-sum;
        if (dif%2==0)
        	return n;
        if ( (dif+n+1)%2==0 )
        	return n+1;
        return n+2;

    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(reachNumber(4));
	}
}
