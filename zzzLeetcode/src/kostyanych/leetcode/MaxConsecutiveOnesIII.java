package kostyanych.leetcode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Function;

/**
 * Given an array A of 0s and 1s, we may change up to K values from 0 to 1.
 * Return the length of the longest (contiguous) subarray that contains only 1s.
 *
 * Note:
 * 	1 <= A.length <= 20000
 * 	0 <= K <= A.length
 * 	A[i] is 0 or 1
 *
 */
public class MaxConsecutiveOnesIII {

	private static int longestOnes(int[] nums, int k) {
		int start=0; //window start
		int zeroes=0; //how many zeroes we've encountered
		int res=0;
		int len=nums.length;

		for(int i=0; i<len; i++) {
			if (nums[i]==0)
				zeroes++;
			while (zeroes>k && start<=i ) {
				if (nums[start]==0) { //it's the zero we cannot flip to 1 (zeroes>K)
					zeroes--;
				}
				start++;
			}
			res = Math.max(res, i-start+1);
		}

		return res;
	}

	private static int longestOnesSlowButClear(int[] A, int K) {
		Queue<Integer> zeroIndices = new LinkedList<>();
		int start=0; //window start
		int res=0;

		int len=A.length;
		for (int i=0;i<len;i++) {
			//if we have 1 here just go to the end and recalc the max len
			//if we have 0, however....
			if (A[i] == 0) {
				zeroIndices.add(i);
				if (zeroIndices.size()>K) { //too many zeroes, we can't change them ALL to 1 now
					int firstZeroInd = zeroIndices.remove();
					//now our window starts on the next index AFTER the first zero we can't flip to 1
					start=firstZeroInd+1;
				}
			}
			//i is the current window end index. start is the current window start ind. just calulate the current window length
			res=Math.max(res, i-start+1);
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {1,1,1,0,0,0,1,1,1,1,0};
		int k=2;
		System.out.println(Arrays.toString(arr));
		System.out.println(longestOnes(arr, k));
	}

}
