package kostyanych.leetcode.numbers;

/**
 * Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.
 *
 * Note:
 * * The length of both num1 and num2 is < 110.
 * * Both num1 and num2 contain only digits 0-9.
 * * Both num1 and num2 do not contain any leading zero, except the number 0 itself.
 * * You must not use any built-in BigInteger library or convert the inputs to integer directly.
 *
 */
public class MultiplyStrings {

	private static String multiply(String num1, String num2) {
		if (num1.equals("0") || num2.equals("0"))
			return "0";

		int m = num1.length();
		int n = num2.length();
		int maxDigits=m + n;
		int[] arr = new int[maxDigits];

		for (int i = m - 1; i >= 0; i --) {
			int m1 = num1.charAt(i) - '0';
			for (int j = n - 1; j >= 0; j --) {
				int product = m1 * (num2.charAt(j) - '0');
		        int sum = arr[i + j + 1] + product;
		        arr[i + j + 1] = sum % 10;	//resulting digit
		        arr[i + j] += sum / 10;	//carry
			}
		}

		StringBuilder sb = new StringBuilder(maxDigits);
		if (arr[0]!=0)
			sb.append(arr[0]);
		for (int i=1;i<maxDigits;i++) {
			sb.append(arr[i]);
		}
	    return sb.length() == 0 ? "0" : sb.toString();


//
//		int lup=num1.length();
//		int ldown=num2.length();
//		String up, down;
//		if (ldown>lup) {
//			up=num2;
//			down=num1;
//			ldown=lup;
//			lup=up.length();
//		}
//		else {
//			up=num1;
//			down=num2;
//		}
//
//		String curRes="";
//		StringBuilder sb=new StringBuilder(lup+ldown);
//		for (int i=ldown-1;i>=0;i--) {
//			int l=down.charAt(i)-'0';
//			if (l==0)
//				continue;
//			if (l==1) {
//				String s=up+zeroes(ldown-1-i);
//				if (curRes.isEmpty())
//					curRes=s;
//				else
//					curRes=add(curRes,s);
//				continue;
//			}
//			sb.setLength(0);
//			int carry=0;
//			for (int j=lup-1;j>=0;j--) {
//				int u=up.charAt(j)-'0';
//				u=u*l+carry;
//				carry=u/10;
//				u=u%10;
//				//sb.insert(0, u);
//				sb.append(u);
//			}
//			if (carry>0)
//				sb.append(carry);
//				//sb.insert(0, carry);
//			sb.reverse();
//			sb.append(zeroes(ldown-1-i));
//			curRes=add(curRes,sb.toString());
//		}
//
//		return curRes;
    }

//	private static String add(String su, String sl) {
//		if (su.isEmpty() || "0".equals(su))
//			return sl;
//		if (sl.isEmpty() || "0".equals(sl))
//			return su;
//
//		int u = su.length() - 1, l = sl.length() - 1;
//        int carry = 0;
//        StringBuffer sb = new StringBuffer();
//        while (u >= 0 || l >= 0) {
//            int sum = carry;
//            sum += u >= 0 ? (su.charAt(u) - '0') : 0;
//            sum += l >= 0 ? (sl.charAt(l) - '0') : 0;
//            sb.append(sum % 10);
//            carry = sum / 10;
//
//            u--;
//            l--;
//        }
//        if (carry != 0)
//        	sb.append(carry);
//        sb.reverse();
//        return sb.toString();
//	}

//	private static String zeroes(int count) {
//		if (count<1)
//			return "";
//		StringBuilder sb=new StringBuilder(count);
//		for (int i=0;i<count;i++)
//			sb.append("0");
//		return sb.toString();
//	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String num1 = "123";
		String num2 = "456";
		System.out.println(multiply(num1, num2));
	}

}
