package kostyanych.leetcode.numbers;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * The power of an integer x is defined as the number of steps needed to transform x into 1 using the following steps:
 *
 * if x is even then x = x / 2
 * if x is odd then x = 3 * x + 1
 * For example, the power of x = 3 is 7 because 3 needs 7 steps to become 1 (3 --> 10 --> 5 --> 16 --> 8 --> 4 --> 2 --> 1).
 *
 * Given three integers lo, hi and k, the task is
 *    to sort all integers in the interval [lo, hi] by the power value in ascending order,
 *    if two or more integers have the same power value sort them by ascending order.
 *
 * Return the k-th integer in the range [lo, hi] sorted by the power value.
 *
 * Notice that for any integer x (lo <= x <= hi) it is guaranteed that x will transform into 1
 *    using these steps and that the power of x is will fit in 32 bit signed integer.
 */
public class SortIntegersByPowerValue {

	private static Map<Integer,Integer> map = new HashMap<>();

	private static Comparator<Integer> cmp = new Comparator<Integer>() {
		@Override
		public int compare(Integer o1, Integer o2) {
			int p1=power(o1);
			int p2=power(o2);
			if (p1==p2)
				return o1-o2;
			return p1-p2;
		}

	};

	private static int getKth(int lo, int hi, int k) {
        Integer[] arr=new Integer[hi-lo+1];
        int ind=0;
        for (int i=lo;i<=hi;i++) {
        	arr[ind]=i;
        	ind++;
        }
        Arrays.sort(arr,cmp);
        return arr[k-1];
    }

	private static int power(int num) {
		if (num==1)
			return 0;
		if (num==2)
			return 1;
		Integer res=map.get(num);
		if (res!=null)
			return res;
		int key=num;
		int count=0;
		while (num!=1) {
			count++;
			if (num%2==0) {
				num=num/2;
				continue;
			}
			num = 3 * num + 1;
		}
		map.put(key,count);
		return count;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int lo = 12;
		int hi = 15;
		int k = 2;
		System.out.println(getKth(lo,hi,k));
		lo = 7;
		hi = 11;
		k = 4;
		System.out.println(getKth(lo,hi,k));
		lo = 10;
		hi = 20;
		k = 5;
		System.out.println(getKth(lo,hi,k));
		lo = 1;
		hi = 1000;
		k = 777;
		System.out.println(getKth(lo,hi,k));
	}

}
