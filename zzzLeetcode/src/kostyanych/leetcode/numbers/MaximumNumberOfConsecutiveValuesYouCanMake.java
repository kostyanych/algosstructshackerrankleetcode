package kostyanych.leetcode.numbers;

import java.util.Arrays;

/**
 * You are given an integer array coins of length n which represents the n coins that you own.
 * The value of the ith coin is coins[i]. You can make some value x if you can choose some of your n coins such that their values sum up to x.
 *
 * Return the maximum number of consecutive integer values that you can make with your coins starting from and including 0.
 *
 * Note that you may have multiple coins of the same value.
 *
 * SOLUTION:
 * 1.  NOTE: our sequence must start with 0 and continue with 1.
 * 2.  let's sort coins and iterate thru them
 * 3.  imagine we managed to generate the sequence of consequtive integers of 0 to m
 * 4.  now we have a coin C
 * 5.  we made 0 to m consecutive integers without this C, so with it we can make a sequence from C+0 to C+m
 * 6.  1=> we need the sequence 3 and sequence 5 overlap at least at point C=m, then our resulting sequence will go to C+m
 * 6a. if C>m then our sequence started at 0 is finished at m
 *
 */
public class MaximumNumberOfConsecutiveValuesYouCanMake {

	static private int getMaximumConsecutive(int[] coins) {
        Arrays.sort(coins);
        int curSeq=1; //started with 0 already
        for (int c : coins) {
        	if (c>curSeq)
        		break;
        	curSeq+=c;
        }
        return curSeq;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr = {1,4,10,3,1};
		System.out.println(getMaximumConsecutive(arr));
	}

}
