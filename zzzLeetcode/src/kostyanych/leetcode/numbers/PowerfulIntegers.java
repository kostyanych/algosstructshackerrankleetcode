package kostyanych.leetcode.numbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PowerfulIntegers {

	static private List<Integer> powerfulIntegers(int x, int y, int bound) {
		List<Integer> res = new ArrayList<>();
		if (bound < 2)
			return res;
		Set<Integer> set = new HashSet<>();
		int[] xarr = new int[21];
		int[] yarr = new int[21];
		int xb = 1;
		int yb = 1;
		xarr[0] = 1;
		xarr[1] = x;
		yarr[0] = 1;
		yarr[1] = y;
		for (int i = 2; i < 21; i++) {
			int buf = xarr[i - 1] * x;
			if (xarr[i-1]>0 && buf < bound) {
				xarr[i] = buf;
				xb = i;
			}
			buf = yarr[i - 1] * y;
			if (yarr[i-1]>0 && buf < bound) {
				yarr[i] = buf;
				yb = i;
			}
			System.out.println("xb=" + xb);
		}
		System.out.println("xb=" + xb);
		System.out.println(Arrays.toString(xarr));
		System.out.println("yb=" + yb);
		System.out.println(Arrays.toString(yarr));
		for (int i = 0; i <= xb; i++) {
			for (int j = 0; j <= yb; j++) {
				int buf = xarr[i] + yarr[j];
				if (buf > bound)
					break;
				if (!set.contains(buf)) {
					set.add(buf);
					res.add(buf);
				}
			}
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(powerfulIntegers(2,3,10));
	}

}
