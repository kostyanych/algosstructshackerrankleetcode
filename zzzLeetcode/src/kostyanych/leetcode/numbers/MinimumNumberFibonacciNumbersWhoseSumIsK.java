package kostyanych.leetcode.numbers;

/**
 * Given the number k, return the minimum number of Fibonacci numbers whose sum is equal to k,
 * whether a Fibonacci number could be used multiple times.
 *
 * 1 <= k <= 10^9
 *
 * For some reason the greesy algorithm works here
 */
public class MinimumNumberFibonacciNumbersWhoseSumIsK {

	private static int findMinFibonacciNumbers(int k) {

		int[] fibs=new int[44];
		fibs[0]=1;
		fibs[1]=1;
		for (int i=2;i<44;i++) {
			fibs[i]=fibs[i-2]+fibs[i-1];
		}

		int res=0;
		for (int i=43;i>=0;i--) {
			if (k<fibs[i])
				continue;
			k-=fibs[i];
			res++;
		}
		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(findMinFibonacciNumbers(645157245));
	}



}
