package kostyanych.leetcode.numbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Given an integer array arr. you have to sort the integers in the array in ascending order 
 * by the number of 1's in their binary representation and 
 * in case of two or more integers have the same number of 1's you have to sort them in ascending order.
 * 
 * Return the sorted array
 */
public class SortIntegersByNumberOf1Bits {
	
	private final static Comparator<Integer> c = new Comparator<Integer>() {
		public int compare(Integer arg0, Integer arg1) {
			int r0=countOnes(arg0);
			int r1=countOnes(arg1);
			if (r0!=r1)
				return r0-r1;
			return arg0-arg1;
		};
	};
	
	private static int[] sortByBits(int[] arr) {
		List<Integer> lst=new ArrayList<>();
		for (int i : arr)
			lst.add(i);
		Collections.sort(lst,c);
		for (int i=0;i<arr.length;i++)
			arr[i]=lst.get(i);
		return arr;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		int[] arr= {0,1,2,3,4,5,6,7,8};
		System.out.println(Arrays.toString(arr));
		System.out.println(Arrays.toString(sortByBits(arr)));
	}

	private static int countOnes(int n) {
		int res=0;
		if (n==0)
			return res;
		while (n>0) {
			if ((n & 1) == 1)
				res++;
			n=n>>1;
		}
		return res;
	}
	
}
