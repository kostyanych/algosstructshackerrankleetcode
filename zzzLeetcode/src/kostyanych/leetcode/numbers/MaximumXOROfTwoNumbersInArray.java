package kostyanych.leetcode.numbers;

import java.util.HashSet;
import java.util.Set;

/**
 * Given a non-empty array of numbers, a0, a1, a2, … , an-1, where 0 ≤ ai < 2^31.
 * Find the maximum result of ai XOR aj, where 0 ≤ i, j < n.
 * Could you do this in O(n) runtime?
 *
 */
public class MaximumXOROfTwoNumbersInArray {

	// binary trie, where each trie node has only 2 child
	// one child will represent bit "1" in the binary representation of num
	// one child will represent bit "0" in the binary representation of num
	private static class TrieNode {
	    // children[0]: represent bit "0" in the binary representation
	    // children[1]: represent bit "1" in the binary representation
	    private final TrieNode[] children;

	    private TrieNode() {
	        children = new TrieNode[2];
	    }
	}

	private static TrieNode root;

	private static int findMaximumXOR(int[] nums) {
		int len=nums.length;
		if (len<2)
			return 0;
		if (len==2)
			return nums[0]^nums[1];

		int maxNum=0;
		for (int i = 1; i < nums.length; i++) {
            maxNum = Math.max(maxNum, nums[i]);
        }

		int maxBits=findMaxBit(maxNum)-1;

	    root = new TrieNode();

	    // build the trie
	    for (int i=0; i< len; i++) {
	        // starting from root;
	    	TrieNode curr = root;

	        for (int j = maxBits; j >= 0; j--) {
	        	int neededBit = 1 << j;
	        	int bitSet = (nums[i] & neededBit) == 0 ? 0 : 1;

	            // if current bit is 0, it will go to children[0]
	            // if current bit is 1, it will go to children[1]
	            if (curr.children[bitSet] == null)
	                curr.children[bitSet] = new TrieNode();

	            curr = curr.children[bitSet];
	        }
	    }

	    int res = 0;

	    // iterate through each num, trie starts from root
	    // on each set bit of num we search for nums in trie where that bit is not set (and vice versa)
	    // 1. if we find such node in trie (meaning we found corresponding num), then we'll have this bit set in our answer (1^0==0^1=1)
	    // so we grab this node in trie for consecutive searches
	    // 2. if we don't find such node in trie, we'll have 0 bit in our answer
	    // and we have to grab the other node (we'll always have at least 1 node at each level)

	    //we thus compute the maximum XOR for every num[i]
	    //and then we only have to track max of this XORs
	    for (int i=0; i< len; i++) {
	        // starting from root
	        TrieNode curr = root;

	        int curNumRes = 0;
	        for (int j = maxBits; j >= 0; j--) {
	        	int neededBit = 1 << j;
	        	int bitSet = (nums[i] & neededBit) == 0 ? 0 : 1;

	        	//look in current level of curr trie if there is a node for bitSet^1 (0 for bitSet of 1, 1 for bitSet of 0)
	        	//if yes, oh joy!, grab this node and add 1 bit to our res
	        	if (curr.children[bitSet^1] != null) {
	                curr = curr.children[bitSet^1];
	                curNumRes += neededBit;
	            }
	            else // no needed bit in trie, so add 0 bit and continue down the only trie's path
	                curr = curr.children[bitSet];
	        }

	        // keep track of global maximum
	        res = Math.max(res, curNumRes);
	        // there is no need to continue when final result has reached max value
	        if (res == Integer.MAX_VALUE)
	        	break;
	    }

	    return res;
	}

	private static int findMaxBit(int num) {
		if (num==0)
			return -1;
		int res=1;
		while ((num=num/2)>0) {
			res++;
		}
		return res;

	}




	private static int findMaximumXORIncomprehensible(int[] nums) {
		 int res=0;
		 int mask = 0;
		 int len=nums.length;
		 if (len<2)
			 return 0;
		 if (len==2)
			 return nums[0]^nums[1];
		 Set<Integer> set = new HashSet<>();
		 for (int i=4; i>=0; i--) {
			 int bit=1<<i;
			 mask |= bit;
			 set.clear();
			 for (int j=0; j<len; j++) {
//				 if ( (nums[j]&mask) > 0)
//					 set.add(nums[j]);
				 set.add(nums[j]&mask);
			 }
			 int start = res | bit;
			 for (Integer num : set) {
				 if (set.contains(start^num)) {
					 res = start;
					 break;
				 }
			 }
		 }

		 return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = {3, 10, 5, 25, 2, 8};
		System.out.println(findMaximumXOR(arr));
	}




}
