package kostyanych.leetcode.numbers;

/**
 * On a broken calculator that has a number showing on its display, we can perform two operations:
 *
 * Double: Multiply the number on the display by 2, or;
 * Decrement: Subtract 1 from the number on the display.
 *
 * Initially, the calculator is displaying the number X.
 *
 * Return the minimum number of operations needed to display the number Y.
 *
 */
public class BrokenCalculator {

	static private int brokenCalc(int X, int Y) {
        if (X==Y)
            return 0;
        if (X>Y)
            return X-Y;
        if (Y%2==0 && X*2==Y)
            return 1;
        if (X>Y/2)
            return halved(X, Y);
        int res=0;

        while (Y > X) {
            res++;
            if (Y % 2 == 1)
                Y++;
            else
                Y /= 2;
        }

        return res + X - Y;
    }

    static private int halved(int X, int Y) {
        int cand=Integer.MAX_VALUE;
        //(x-n)*2
        if (Y%2==0)
            cand=1+X-Y/2;
        else
            cand=Math.min(cand, 2+X-(Y+1)/2);
        //2*x-n
        cand=Math.min(cand, 1+X*2-Y);
        return cand;
    }

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
    	System.out.println(brokenCalc(3, 25));
    }


}
