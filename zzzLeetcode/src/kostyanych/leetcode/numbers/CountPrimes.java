package kostyanych.leetcode.numbers;

import java.util.Arrays;

/**
 * Count the number of prime numbers less than a non-negative number, n.
 */
public class CountPrimes {

	private static int countPrimes(int n) {
		if (n<2)
			return 0;
		if (n==2)
			return 1;

		boolean[] sieve = sieve(n);
		int res=0;
		for (int i=2;i<=n;i++) {
			if (sieve[i])
				res++;
		}
		return res;

    }

	private static boolean[] sieve(int n) {
		boolean[] res = new boolean[n+1];
		Arrays.fill(res, true);
		res[0]=false;
		res[1]=false;

		for(int i=2; i*i<=n; i++){
			if(!res[i])
				continue;

			for(int j=i*i; j<=n; j +=i) {
		            res[j]=false;
		    }
		}

		return res;

	}

    public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
    	System.out.println(countPrimes(10));
    }

}
