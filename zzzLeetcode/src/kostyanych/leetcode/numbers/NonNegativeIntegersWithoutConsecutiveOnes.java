package kostyanych.leetcode.numbers;

/**
 * Given a positive integer n, find the number of non-negative integers less than or equal to n,
 * whose binary representations do NOT contain consecutive ones.
 *
 * Note: 1 <= n <= 10^9
 *
 * Useful observation:
 * for X=111...11 (n+1 [we're zerobased!] ones with no zero) the result is a fibonacci number of f(n-1)+f(n-2):
 *
 * X = 1 (f(0))
 * X = 11 (3) -> [00-01] + [10-10] = f(0)+1 = 2 (f2)
 * X = 111 (7) -> [000-011] + [100-101] (we cannot start with 110 already) = f(2) + f1(1) = 3 (f3)
 * X = 1111 (15) -> [0000-0111]+[1000-1011] = (f3)+f(2) = 5 (f4)
 * X = 11111 (31) -> [00000-01111]+[10000-10111] = f(4)+f(3) and so on
 *
 * now for example:
 * 39 (10 0111) ->
 *  bits:  10 0111
 * 00 0000-01 1111  (up to 1st 1 (at 5)
 * 10 0000-10 0011  (up to 2nd 1) (at2)
 * 10 0100-10 0101  (up to 3rd 1) (at 1)
 * and we can't go when we reached this 3rd bit, 'cause we get 100110 (2 ones)
 * so when we reaching 1 we count for it and then check - if had a 1 before it, stop counting at that
 * if there were no double ones thru the whole number we should count the number itself, adding 1
 *
 */
public class NonNegativeIntegersWithoutConsecutiveOnes {

	private static int findIntegers(int num) {
		int[] fiboBits = new int[32];
		fiboBits[0] = 1;
		fiboBits[1] = 2;
		for (int i=2;i<31;i++) {
			fiboBits[i]=fiboBits[i-1]+fiboBits[i-2];
		}
        int count = 0;
        for(int i=30;i>=0;i--) {
        	int msb=1<<i;
            if( (msb&num) >0 ) { //bit i is on
                count += fiboBits[i];
                msb=1<<(i+1); //check the previous bit, if it's also 1 then we already have 11... and it's the max we can do
                if( (msb&num)>0 )
                	return count;
            }
        }
        //the number itself is a valid one, count it also!
        return count+1;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(findIntegers(39));
	}
}
