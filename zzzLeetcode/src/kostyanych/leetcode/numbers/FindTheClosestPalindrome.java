package kostyanych.leetcode.numbers;

/**
 * Given an integer n, find the closest integer (not including itself), which is a palindrome.
 * The 'closest' is defined as absolute difference minimized between two integers.
 *
 * 1. we should not use the n itself if it's the palindrome itself!
 * 2. so, even if n is a palindrome, we should check the closest bigger and lesser palindromes
 * 3. we find the palindrome by reversing the first half over the tailing part: 123456 -> 123321
 * 4. how to find the closest palindrome?
 * 		consider the number 2992. if we change digits nearer the center (9), we'll have the closer palindrome than if we change the outer digits (2)
 * 		so, we'll use our increments in steps which are 10^m where m is len(n)/2 - 1. For 2992 this step would be 10
 * 		We'll need the bigger palindrome with incrementing by this step and finding the first one which is bigger than n itself
 * 		We'll need the lesser palindrome with derementing by this step and finding the first one which is lesser than n itself
 * 5. we also have to check the palindrome made out without incrementing and decrementing (2991 -> 2992), because for that the change coulb be less than our step
 *
 *
 *
 *
 */
public class FindTheClosestPalindrome {

	static private String nearestPalindromic(String n) {
		long num = Long.parseLong(n);
	    int len = n.length();
	    if ("1".equals(n))
	    	return "0";
		long step = 1;
	    for (int i = 0; i < len / 2 - 1; i++) {
	    	step = step * 10;
	    }

	    long larger = 0;
	    long smaller = 0;

	    long buf = num;
	    do {
	    	larger = makePalindrome(buf);
	    	buf += step;
	    } while (larger <= num);


	    buf = num;
	    do {
	        smaller = makePalindrome(buf);
	        if (buf < step)
	            break;
	        buf -= step;
	    } while (smaller >= num);

	    if(num - smaller <= larger -num)
	    	return String.valueOf(smaller);
	    else
	    	return String.valueOf(larger);
	}

	static private long makePalindrome(long number) {
		String result = String.valueOf(number);
	    char[] array = result.toCharArray();
	    int head = 0, tail = array.length - 1;
	    while (head < tail) {
	        array[tail--] = array[head++];
	    }
	    return Long.parseLong(new String(array));
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println("2993");
		System.out.println(nearestPalindromic("2993"));
	}

}
