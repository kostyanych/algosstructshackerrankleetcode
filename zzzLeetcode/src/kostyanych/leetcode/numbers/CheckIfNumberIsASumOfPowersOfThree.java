package kostyanych.leetcode.numbers;

/**
 * Given an integer n, return true if it is possible to represent n as the sum of distinct powers of three. Otherwise, return false.
 *
 * An integer y is a power of three if there exists an integer x such that y == 3x.
 *
 * Constraints:
 *
 * 1 <= n <= 10^7
 *
 */
public class CheckIfNumberIsASumOfPowersOfThree {

	static private boolean checkPowersOfThree(int n) {
		if (n==0 || n==1)
			return true;
		int rem=n%3;
		if (rem==2)
			return false;
		if (rem==1)
			n--;
		while (n%3==0) {
			n/=3;
		}
		return checkPowersOfThree(n);

    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(checkPowersOfThree(2000003));
	}
}
