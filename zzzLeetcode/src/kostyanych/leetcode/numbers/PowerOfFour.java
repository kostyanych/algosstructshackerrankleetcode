package kostyanych.leetcode.numbers;

/**
 * Given an integer (signed 32 bits), write a function to check whether it is a power of 4.
 * @author Krokokot
 *
 */
public class PowerOfFour {

	private static boolean isPowerOfFour(int num) {
        if (num==0)
            return false;
        if (num==1)
            return true;
        if (num<0)
        	return false;
        while (num>1) {
        	if (num%4>0)
        		return false;
        	num/=4;
        }
        return true;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(isPowerOfFour(-64));
	}
}
