package kostyanych.leetcode.numbers;

import java.util.Arrays;

/**
 * Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.
 *
 * If such arrangement is not possible, it must rearrange it as the lowest possible order (ie, sorted in ascending order).
 *
 * The replacement must be in-place and use only constant extra memory.
 *
 * Here are some examples. Inputs are in the left-hand column and its corresponding outputs are in the right-hand column.
 *
 * 1,2,3 → 1,3,2
 * 3,2,1 → 1,2,3
 * 1,1,5 → 1,5,1
 *
 */
public class NextPermutation {

	public static void nextPermutation(int[] nums) {
		int len=nums.length;
		if (len<2)
			return;
		//1. from right to left find the first digit that is less than any on his right
		int pivotIndex=len-2;
		while (pivotIndex>=0) {
			if (nums[pivotIndex]>=nums[pivotIndex+1])
				pivotIndex--;
			else
				break;
		}

		//2. no such dig
		if (pivotIndex<0) {
			Arrays.sort(nums);
			return;
		}

		//3. find min number greater that pivotDigit to ther right of pivot digit
		//and swap them
		int candidateIndex=pivotIndex+1;
		int min=nums[candidateIndex];
		for (int i=candidateIndex+1;i<len;i++) {
			if (nums[i]<nums[candidateIndex] && nums[i]>nums[pivotIndex])
				candidateIndex=i;
		}
		int tmp = nums[pivotIndex];
		nums[pivotIndex]=nums[candidateIndex];
		nums[candidateIndex]=tmp;
		//4. sort right hand digit in nondecreasing order
		Arrays.sort(nums,pivotIndex+1,len);
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;

		int[] arr= {7, 6, 2, 5, 4, 1};
		nextPermutation(arr);
		System.out.println(Arrays.toString(arr));
	}
}
