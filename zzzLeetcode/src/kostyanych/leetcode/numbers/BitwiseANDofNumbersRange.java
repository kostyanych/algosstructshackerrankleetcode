package kostyanych.leetcode.numbers;

import kostyanych.leetcode.helpers.TreeNode;

/**
 * Given a range [m, n] where 0 <= m <= n <= 2147483647, return the bitwise AND of all numbers in this range, inclusive.
 */
public class BitwiseANDofNumbersRange {



	/**
	 * let's consider the range from 5 to 13:
	 * 0101 - 5
	 * ....
	 * 1100 - 12
	 * 1101 - 13
	 * obviously we'll have 1s only for most significant bits which are common between ALL numbers in range
	 * for range 5 to 13 there are none (msb for 13 is 1000, while msb for 5 is 0100)
	 *
	 * on the other had for range 12-15 we'll have :
	 * 1100 - 12
	 * 1101 - 13
	 * 1110 - 14
	 * 1111 - 15
	 *
	 * msb for 12 are 1100 and for 15 - 1100
	 *
	 * So, basically we'll shift m and n to the right until they are equal (if no msb coincide, then they will eventually be both equal to 0)
	 * Then we'll have to shift the resulting number to the left the same number of shifts we've originally made
	 */
	private static int rangeBitwiseAnd(int m, int n) {
		if (m==0 || n==0)
			return 0;
		int shifts=0;
		while (m!=n) {
			m>>=1;
			n>>=1;
			shifts++;
		}
		m<<=shifts;
		return m;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int m=5;
		int n=12;
		System.out.println(""+m+", "+n+" -> "+rangeBitwiseAnd(m, n));
		m=0;
		n=1;
		System.out.println(""+m+", "+n+" -> "+rangeBitwiseAnd(m, n));
		m=5;
		n=7;
		System.out.println(""+m+", "+n+" -> "+rangeBitwiseAnd(m, n));

	}

}
