package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * A binary matrix means that all elements are 0 or 1.
 * For each individual row of the matrix, this row is sorted in non-decreasing order.
 *
 * Given a row-sorted binary matrix binaryMatrix, return leftmost column index(0-indexed) with 1 in it.
 * If such index doesn't exist, return -1.
 *
 * You can't access the Binary Matrix data directly.
 * You may only access the matrix using a BinaryMatrix interface:
 *    BinaryMatrix.get(x, y) returns the element of the matrix at index (x, y) (0-indexed).
 *    BinaryMatrix.dimensions() returns a list of 2 elements [n, m], which means the matrix is n * m.
 */
public class LeftmostColumnWithAtLeastOne {

	static class BinaryMatrix {
		private final int[][] arr;
		private final List<Integer> dims=new ArrayList<>();

		public BinaryMatrix(int[][] arr) {
			this.arr=arr;
			dims.add(this.arr.length);
			dims.add(this.arr[0].length);
		}

		public int get(int x, int y) {
			return arr[x][y];
		}

		public List<Integer> dimensions() {
			return dims;
		}
	}

	 private static int leftMostColumnWithOne(BinaryMatrix bm) {
	        List<Integer> dim=bm.dimensions();
	        int rows=dim.get(0);
	        int len=dim.get(1);
	        int left=0;
	        int right=len-1;

	        int res=-1;
	        while (left<=right) {
	        	int mid=(left+right)/2;
	        	boolean found=false;
	        	for (int i=0;i<rows;i++) {
	        		if (bm.get(i, mid)==1) {
	        			if (mid==0)
	        				return 0;
	        			res=mid;
	        			right=mid-1;
	        			mid=(left+right)/2;
	        			found=true;
	        		}
	        	}
        		if (!found)
        			left=mid+1;
	        }
	        return res;
	 }

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;
		//int[][] arr = {{1,1,1,1,1},{0,0,0,1,1},{0,0,1,1,1},{0,0,0,0,1},{0,0,0,0,0} };
		 int[][] arr = {
				 {0,0,0,0,0,0,1,1,1,1,1},
				 {0,0,0,0,0,1,1,1,1,1,1},
				 {0,0,0,0,1,1,1,1,1,1,1},
				 {0,0,0,0,0,0,1,1,1,1,1},
				 {0,0,0,0,0,1,1,1,1,1,1},
				 {0,0,0,0,0,1,1,1,1,1,1},
				 {0,0,0,0,0,0,0,0,0,1,1},
				 {0,0,0,0,1,1,1,1,1,1,1},
				 {0,0,1,1,1,1,1,1,1,1,1},
				 {0,0,0,1,1,1,1,1,1,1,1},
				 {0,0,0,0,0,0,0,0,0,1,1}
		 };
		 BinaryMatrix bm = new BinaryMatrix(arr);
		 System.out.println(leftMostColumnWithOne(bm));
	 }


}
