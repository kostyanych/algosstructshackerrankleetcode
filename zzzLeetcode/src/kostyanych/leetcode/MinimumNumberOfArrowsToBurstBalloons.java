package kostyanych.leetcode;

import java.util.Arrays;

import kostyanych.leetcode.helpers.Printer;

/**
 * There are a number of spherical balloons spread in two-dimensional space.
 * For each balloon, provided input is the start and end coordinates of the horizontal diameter.
 * Since it's horizontal, y-coordinates don't matter and hence the x-coordinates of start and end of the diameter suffice.
 * Start is always smaller than end. There will be at most 104 balloons.
 *
 * An arrow can be shot up exactly vertically from different points along the x-axis.
 * A balloon with xstart and xend bursts by an arrow shot at x if xstart ≤ x ≤ xend.
 * There is no limit to the number of arrows that can be shot. An arrow once shot keeps travelling up infinitely.
 * The problem is to find the minimum number of arrows that must be shot to burst all balloons.
 *
 */
public class MinimumNumberOfArrowsToBurstBalloons {

	private static int findMinArrowShots(int[][] points) {
        int len = points.length;
        if (len<1)
            return 0;
		Arrays.sort(points, (p1,p2) -> p1[1]-p2[1]);
		int res=1;
		int curend=points[0][1];
		for(int i=1; i<len;i++){
			if(curend<points[i][0]) { //next balloon doesn't intersect with previous
				curend = points[i][1];
				res++;
			}
		}
        return res;
    }

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] arr = {{3,9},{7,12},{3,8},{6,8},{9,10},{2,9},{0,9},{3,9},{0,6},{2,8}};
		Printer.print2DArray(arr, true);
		System.out.println(findMinArrowShots(arr));
	}
}
