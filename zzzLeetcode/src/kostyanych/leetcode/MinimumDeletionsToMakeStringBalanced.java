package kostyanych.leetcode;

/**
 * You are given a string s consisting only of characters 'a' and 'b'​​​​.
 *
 * You can delete any number of characters in s to make s balanced. s is balanced if there is no pair of indices (i,j)
 *      such that i < j and s[i] = 'b' and s[j]= 'a'.
 *
 * Return the minimum number of deletions needed to make s balanced.
 *
 * SOULTION:
 * 1. make an array adels[] to calculate how many As we encounter from the right including this position.
 * 2. make an array bdels[] to calculate how many Bs we encounter from the left including this position
 * 3. then going from the left at any position i we find the min of b[i] (since we cannot have bs to the left) + a[i+1] (since we can't have as to the right)
 *
 * note: since in 2 and 3 we're going from left to right and only need values for b calculated left and including current position,
 *    we can just use the single counter instead on an array
 *
 */
public class MinimumDeletionsToMakeStringBalanced {

	static private int minimumDeletions(String s) {
		int len = s.length();
		int[] aDeletions = new int[len  + 1];
		char[] chars=s.toCharArray();

		for (int i = len  - 1; i >= 0; i--) {
			aDeletions[i] = aDeletions[i + 1];
			if (chars[i] == 'a')
				aDeletions[i]++;
		}

		int bDeletions = 0;
		int res = aDeletions[0];	//max that we can do is to delete all as

		for (int i = 0; i < len; i++) {
			if (chars[i] == 'b')
				bDeletions++;
			res = Math.min(res, bDeletions + aDeletions[i + 1]);
		}

		return res;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(minimumDeletions("bbaaaaabb"));
	}
}
