package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LittleElephantAndTShirts {

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[][] shirts = {{2,3,1},{2,3}};
		int shirtAmount=3;


		int people=shirts.length;
		int[][] dp = new int[1<<people][shirtAmount+1];
		for (int[] ddp : dp) {
			Arrays.fill(ddp, -1);
		}


		List<Integer>[] shirtsByPeople = new List[shirtAmount+1]; // h2p[i] indicates the list of people who can wear i_th hat
        for (int i = 1; i <= shirtAmount; i++) {
        	shirtsByPeople[i] = new ArrayList<>();
        }

        for (int i = 0; i < people; i++) {
            for (int shirt : shirts[i]) {
            	shirtsByPeople[shirt].add(i);
            }
        }
		System.out.println(count(shirtsByPeople, people, shirtAmount, dp, 0, 1));
	}

	private static int count(List<Integer>[] shirtsByPeople, int people, int shirtAmount, int[][] dp, int curMask, int curShirt) {
		int allDone=(1 << people) - 1;
		if (curMask==allDone) // all bits are set, all persons have been alloted tshirts
			return 1;

		if(curShirt>shirtAmount)    // all tshirts finished
			return 0;

		if (dp[curMask][curShirt]!=-1)   // it has already been calculated, we won't calculate it again
			return dp[curMask][curShirt];

		int ans = count(shirtsByPeople, people, shirtAmount, dp, curMask, curShirt+1);
			// the case when we don't assign tshirt with id=tid to anyone
		    // the case when we assign tshirt with id=tid to someone
		    // we will assign the tshirt with id=tid to all possible persons we can, and add to answer the respective number of ways
		    // note that we are assigning distinct tshirts only, since tshirt with id=tid has never been assigned before to anyone.

		//for persons p(0<=p<=N-1) which have tshirt with id=tid:
		for (int p : shirtsByPeople[curShirt]) {
            if (((curMask >> p) & 1) == 1) // person p has already been alloted a tshirt
            	continue;
            ans += count(shirtsByPeople, people, shirtAmount, dp, curMask | (1 << p), curShirt+1);
		}
		return dp[curMask][curShirt]=ans;
	}

}
