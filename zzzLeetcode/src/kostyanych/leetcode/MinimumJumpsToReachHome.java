package kostyanych.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * A certain bug's home is on the x-axis at position x. Help them get there from position 0.
 *
 * The bug jumps according to the following rules:
 *    It can jump exactly a positions forward (to the right).
 *    It can jump exactly b positions backward (to the left).
 *    It cannot jump backward twice in a row.
 *    It cannot jump to any forbidden positions.
 *
 * The bug may jump forward beyond its home, but it cannot jump to positions numbered with negative integers.
 *
 * Given an array of integers forbidden, where forbidden[i] means that the bug cannot jump to the position forbidden[i],
 *    and integers a, b, and x, return the minimum number of jumps needed for the bug to reach its home.
 * If there is no possible sequence of jumps that lands the bug on position x, return -1.
 *
 * Constraints:
 *
 *    1 <= forbidden.length <= 1000
 *    1 <= a, b, forbidden[i] <= 2000
 *    0 <= x <= 2000
 *    All the elements in forbidden are distinct.
 *    Position x is not forbidden.
 *
 *
 */
public class MinimumJumpsToReachHome {

	static final private Set<Integer> fset = new HashSet<>();
	static private int[] axis;

	static private int minimumJumps(int[] forbidden, int a, int b, int x) {
		if (x==0)
			return 0;
		for (int f : forbidden)
			fset.add(f);
		if (fset.contains(a) || fset.contains(x))
			return -1;

		int maxLen=10000;

		//int[] axis=new int[x+2*b+1];
		axis=new int[maxLen];
		Arrays.fill(axis, -1);
		axis[0]=0;
		axis[a]=1;
		int cnt=1;
		Queue<Integer> backs = new LinkedList<>();
		for (int i=a;i<maxLen;i+=a) {
			if (fset.contains(i))
				break;
			if (i==x)
				return cnt;
			axis[i]=cnt;
			cnt++;
			if (i-b>0 && axis[i-b]<0 && !fset.contains(i-b))
				backs.add(i-b);
		}
		if (b==a)
			return -1;

		while (!backs.isEmpty()) {
			int size=backs.size();
			for (int i=0;i<size;i++) {
				int back=backs.remove();
				int base=axis[back+b]+1;
				axis[back]=base;
				if (back==x)
					return base;
				base++;
				for (int j=back+a;j<maxLen;j+=a) {
					if (fset.contains(j))
						break;
					if (axis[j]>0)
						break;
					axis[j]=base;
					if (j==x)
						return base;
					base++;
					if (j-b>0 && axis[j-b]<0 && !fset.contains(j-b))
						backs.add(j-b);
				}
			}
		}
		return -1;
	}


	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		String s=")))))))";
//		System.out.println(s);
//		System.out.println(minInsertions(s));

		int[] arr = {162,118,178,152,167,100,40,74,199,186,26,73,200,127,30,124,193,84,184,36,103,149,153,9,54,154,133,95,45,198,79,157,64,122,59,71,48,177,82,35,14,176,16,108,111,6,168,31,134,164,136,72,98};
		int a = 29;
		int b = 98;
		int x = 80;
		System.out.println(minimumJumps(arr,a,b,x));
	}
}
