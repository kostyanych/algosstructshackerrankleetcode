package kostyanych.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * You are given a 2D integer array, queries. For each queries[i], where queries[i] = [ni, ki],
 * find the number of different ways you can place positive integers into an array of size ni such that the product of the integers is ki.
 * As the number of ways may be too large, the answer to the ith query is the number of ways modulo 10^9 + 7.
 *
 * Return an integer array answer where answer.length == queries.length, and answer[i] is the answer to the ith query.
 *
 * Constraints:
 *
 * 1 <= queries.length <= 10^4
 * 1 <= ni, ki <= 10^4
 *
 */
public class CountWaysToMakeArrayWithProduct {

	public static final int MOD = 1_000_000_007;


//----------------------------------------------------------------------------------------------
	static private List<Integer> primes;
	static private long[][] tri;

	/**
	 * basic idea is to factorize every target, then group prime factors into n groups.
	 * It is very similar to put N balls into M boxes, allow empty boxes(because we can fill the empty box with 1)
	 * formula： C(N+M-1, M-1)
	 *
	 * Note that we don't have to compute C(10000+10000 10000),
	 * since the constraint limit the max number to 10000, say we choose minimum prime which is 2, 2^14>10000,    ?????
	 * so count of any single prime will not exceed 13, and also we know C(10013, 10000) = C(10013, 13).          ?????
	 * Then calculate pascal triangle to get C(m, n).
	 */
	static private int[] waysToFillArray1(int[][] queries) {
		int len = queries.length;
        int[] res = new int[len];
        primes = getPrimes(100);
        tri = getTri(10015, 15); //why?
        for(int i=0; i<len; i++)
            res[i] = calculate(queries[i][0], queries[i][1]);
        return res;
	}

	static private List<Integer> getPrimes(int limit){
        boolean[] notPrime = new boolean[limit+1];
        List<Integer> res = new ArrayList<>();
        for(int i=2; i<=limit; i++){
            if(!notPrime[i]){
                res.add(i);
                for(int j=i*i; j<=limit; j+=i)
                    notPrime[j]=true;
            }
        }
        return res;
    }

	static private long[][] getTri(int m, int n){
        long[][] res = new long[m+1][n+1];
        for(int i=0; i<=m; i++){
            res[i][0]=1;
            for(int j=1; j<=Math.min(n, i); j++)
                res[i][j] = (res[i-1][j-1]+res[i-1][j])%MOD;
        }
        return res;
    }

	static private int calculate(int n, int target){
        long res = 1;
        for (int prime : primes) {
            if(prime > target)
                break;
            int cnt = 0;
            while(target%prime==0){
                cnt++;
                target/=prime;
            }
            res = (res * tri[cnt+n-1][cnt])%MOD;
        }
        return target>1 ? (int)(res*n%MOD) : (int)res;
    }
//----------------------------------------------------------------------------------------------

	static private int[] waysToFillArray2(int[][] queries) {
		int[][] memo = new int[10][10];
		return new int[] {calc(queries[0][0], queries[0][1], memo)};
	}

	static private int calc(int count, int num, int [][] memo) {
        if (count==1 || num==1)
            return 1;
        if (memo[count][num]!=0)
        	return memo[count][num];
        int sum=0;
        for(int factor=1;factor<=num;factor++) {
            if(num%factor==0) // if factor is a divisor of j
                sum=(sum+calc(count-1, num/factor, memo))%MOD;
        }
        memo[count][num]=sum;
		return sum;
	}

//-----------------------------------------------------------------------------

    static private int choose(int n, int k, Integer[][] memo) {
        if (n==0 || k==0 || k==n)
        	return 1;
        if (k > n/2)
        	return choose(n, n-k, memo);
        if (memo[n][k]!=null)
        	return memo[n][k];
        return memo[n][k] = (choose(n-1, k, memo) + choose(n-1, k-1, memo))%MOD;
    }

    static private int[] waysToFillArray3(int[][] queries) {
        int len=queries.length;
        int[] res = new int[len];
        List<Integer> pList = getPrimes(10000);
        Integer[][] memo = new Integer[10024][24];

        //factorMap holds the power of primes for a number
        //it's reinitializes for every number
        // for example for 12 (=2*2*3) it will hold {2:2, 3:1}
        Map<Integer, Integer> factorMap = new HashMap<>();
        for (int i=0; i<len; i++) {
            int[] query = queries[i];
            if (query[1]==1 || query[0]==1) {
            	res[i]=1;
            	continue;
            }
            int idx = 0;
            int num = query[1];
            factorMap.clear();
            while (pList.get(idx)<= num) {
            	int prime = pList.get(idx);
            	if ( (num % prime) ==0) {
            		factorMap.merge(prime, 1, (a,b)-> a+b);
                    num /= prime;
            	}
                else
                	idx++;
            }
            long result = 1;
            for (int f: factorMap.values()) {
            	result = (result * choose(query[0]+f-1, f, memo)) % MOD;
            }
            res[i] = (int) result;
        }
        return res;
    }

//-----------------------------------------------------------------------------

    static private Map<Integer, Map<Integer, Integer>> CNK = new HashMap<>();

	static private int[] waysToFillArray4(int[][] queries) {
        int len=queries.length;
        int[] res = new int[len];
        for (int i=0;i< len;i++) {
        	int[] q = queries[i];
        	int size=q[0];
        	int num=q[1];
        	if (size==1 || num==1) {
        		res[i]=1;
        		continue;
        	}
        	Map<Integer, Integer>[] memo = new HashMap[size+1];
            res[i]=go(0, size, num, memo)%MOD;
        }
        return res;
    }

    static private int go(int placesTaken, int size, int num, Map<Integer, Integer>[] memo) {
        if(placesTaken==size){
			//query (2,6)--> 2,3 hence placesTaken==size so contribution 1 because no space to keep 1
//        	return num==1 ? 1 : 0;
        	return 1;
        }

//if we only have one place left and we got to put any N except 1 we can only do it just one way
//we cannot factorize it further as N,1
        if(placesTaken==size-1 && num!=1)
        	return 1;

    	Map<Integer, Integer> map = memo[placesTaken];
    	if (map==null) {
    		map=new HashMap<>();
    		memo[placesTaken]=map;
    	}
    	if (map.get(num)!=null)
    		return map.get(num);

        if(num==1) {
			//array with length placesTaken found now filling ones in remaining space
        	int ret=C(size,size-placesTaken)%MOD;
        	map.put(1, ret);
            return ret;
        }
        int ret=0;
		//checking all the factors
        for(int v=1;v*v<=num;v++){
            if(num%v==0){
            	if(v!=1)
            		ret=(ret+go(placesTaken+1,size, num/v, memo))%MOD;
                if(v!=num/v)
                    ret=(ret+go(placesTaken+1, size, v, memo))%MOD;
            }
        }
		//memoizing the state(placesTaken, num)
    	map.put(num, ret);
        return ret;
    }


  //nCr function with memoization
    static private int C(int n, int k) {
        if (k > n)
            return 0;
        if (k == 0 || k == n)
            return 1;
        Map<Integer,Integer> mapN= CNK.compute(n, (key,val)->val==null? new HashMap<>() : val);
        if (mapN.get(k)!=null)
        	return mapN.get(k);
        int ret = (C(n-1, k-1)%MOD+C(n - 1, k)%MOD)%MOD;
        mapN.put(k,ret);
        return ret;
    }

	 public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;

		 int[][] queries = {{4,16}};
		 System.out.println(Arrays.toString(waysToFillArray4(queries)));
	 }

}
