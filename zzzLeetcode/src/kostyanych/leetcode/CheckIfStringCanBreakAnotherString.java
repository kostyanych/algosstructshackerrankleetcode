package kostyanych.leetcode;

import java.util.Arrays;

/**
 * Given two strings: s1 and s2 with the same size, check if some permutation of string s1 can BREAK some permutation of string s2
 *     OR VICE-CERSA (in other words s2 can break s1).
 *
 * A string x can BREAK string y (both of size n) if x[i] >= y[i] (in alphabetical order) for all i between 0 and n-1.
 *
 */
public class CheckIfStringCanBreakAnotherString {

	private static boolean checkIfCanBreak(String s1, String s2) {
		int[] c1 = new int[26];
		int[] c2 = new int[26];
		for (int i=0;i<s1.length();i++) {
			int c=s1.charAt(i)-'a';
			c1[c]++;
			c=s2.charAt(i)-'a';
			c2[c]++;
		}

		int[] c12=Arrays.copyOf(c1, 26);
		int[] c22=Arrays.copyOf(c2, 26);
		if (canBreak(c1,c2))
			return true;
		return canBreak(c22,c12);
    }

	private static boolean canBreak(int[] c1, int[] c2) {
		for (int i=0;i<26;i++) {
			if (c1[i]<=c2[i])
				continue;
			int sum=c1[i];
			for (int j=i;j<26;j++) {
				if (c2[j]>sum) {
					c2[j]-=sum;
					sum=0;
				}
				else {
					sum-=c2[j];
					c2[j]=0;
				}
				if (sum==0)
					break;
			}
			if (sum>0)
				return false;
		}
		return true;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		String s1 = "abe";
		String s2 = "acd";
		System.out.println(s1+", "+s2+" -> "+checkIfCanBreak(s1, s2));
	}


}
