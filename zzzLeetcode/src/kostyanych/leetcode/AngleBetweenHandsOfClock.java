package kostyanych.leetcode;

public class AngleBetweenHandsOfClock {
	
	private static double angleClock(int hour, int minutes) {
		int aM=6*minutes;
		if (hour==12)
			hour=0;
		double aH=30*hour+0.5*minutes;
		double res=-1;
		if (aH>aM) {
			if (aH-aM > 180)
				res=(360-aH)+aM;
			else
				res=aH-aM;
		}
		else {
			if (aM-aH > 180)
				res=(360-aM)+aH;
			else
				res=aM-aH;
		}
		return res;
    }
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int hour = 3;
		int minutes = 15;
		System.out.println(""+hour+":"+minutes+" -> "+angleClock(hour, minutes));
		
		hour = 4;
		minutes = 50;
		System.out.println(""+hour+":"+minutes+" -> "+angleClock(hour, minutes));

		hour = 12;
		minutes = 0;
		System.out.println(""+hour+":"+minutes+" -> "+angleClock(hour, minutes));

		hour = 11;
		minutes = 5;
		System.out.println(""+hour+":"+minutes+" -> "+angleClock(hour, minutes));
		
		hour = 0;
		minutes = 55;
		System.out.println(""+hour+":"+minutes+" -> "+angleClock(hour, minutes));
		
		
	}
}
