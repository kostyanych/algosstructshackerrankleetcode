package kostyanych.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * There is a strange printer with the following two special requirements:
 *
 * On each turn, the printer will print a solid rectangular pattern of a single color on the grid.
 * This will cover up the existing colors in the rectangle.
 * Once the printer has used a color for the above operation, the same color cannot be used again.
 * You are given a m x n matrix targetGrid, where targetGrid[row][col] is the color in the position (row, col) of the grid.
 * Return true if it is possible to print the matrix targetGrid, otherwise, return false.
 *
 * Constraints:
 * 	m == targetGrid.length
 * 	n == targetGrid[i].length
 * 	1 <= m, n <= 60
 * 	1 <= targetGrid[row][col] <= 60
 *
 */
public class StrangePrinterII {

	private static boolean isPrintable(int[][] targetGrid) {
		int m = targetGrid.length;
		int n = targetGrid[0].length;
		//for every color we'll find the top-left corner ([0],[1]) and bottom-right corner ([2],[3])
		int rects[][] = new int[61][4];
		for (int i=1;i<61;i++) { //put unreachable max for top-left initial value, so we could safely use min on it.
			rects[i][0]=100;
			rects[i][1]=100;
		}
		//let's check what colors are there
		Set<Integer> colors = new HashSet<>();
		for (int i=0;i<m;i++) {
			for (int j=0;j<n;j++) {
				int color = targetGrid[i][j];
				colors.add(color);
				rects[color][0] = Math.min(rects[color][0],i);
				rects[color][1] = Math.min(rects[color][1],j);
				rects[color][2] = Math.max(rects[color][2],i);
				rects[color][3] = Math.max(rects[color][3],j);
			}
		}

		 while (!colors.isEmpty()) {
			 Set<Integer> next = new HashSet<>();
			 for (int color : colors) {
				 if (!erase(targetGrid, rects, color)) //there were colors on top of this color, so we'll have to process it again, hopefully we erased everything on top of it
					 next.add(color);
			 }
			 if (colors.size() == next.size()) //we could not erase anything, so it's the illegal position
				 return false;
            colors = next;
        }
        return true;
	}

	static private boolean erase(int[][] targetGrid, int[][] rects, int color) {
		int[] coords = rects[color];
        for (int i = coords[0]; i <= coords[2]; i++) {
            for (int j = coords[1]; j <= coords[3]; j++) {
                if (targetGrid[i][j] > 0 && targetGrid[i][j] != color) { //there are other colors above this color rectangle
                    return false;
                }
            }
        }
        for (int i = coords[0]; i <= coords[2]; i++) {
            for (int j = coords[1]; j <= coords[3]; j++) {
                targetGrid[i][j] = 0;
            }
        }
        return true;
    }

	public static void testMe(boolean isNeeded) {
		 if (!isNeeded)
			 return;

		 int[][] arr= {{1,1,1,1},{1,1,3,3},{1,1,3,4},{5,5,1,4}};
		 System.out.println(isPrintable(arr));
	 }
}
