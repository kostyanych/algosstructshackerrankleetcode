package kostyanych.hackerrank.tests;

import kostyanych.hackerrank.competes.ARaceAgainstTime;
import kostyanych.hackerrank.competes.CutAStrip;
import kostyanych.hackerrank.competes.WaysToGiveACheck;

public class HRCompeteTester {

	public static void testWaysToGiveACheck(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		char[][] board= {
				{'k','A','Q','#','#','#','#','#'},
				{'#','P','R','#','P','#','#','#'},
				{'#','#','#','#','K','#','#','#'},
				{'#','#','#','#','#','#','#','#'},
				{'#','#','#','#','#','#','#','#'},
				{'Q','#','#','#','#','B','#','#'},
				{'Q','#','#','#','K','#','#','#'},
				{'#','#','#','#','k','#','#','#'}
		};
		
		WaysToGiveACheck.run(board);
	}
	
	public static void testRaceAgainstTime(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		int n=4;
		int mason_height=5;
		int[] heights={2,3,1};
		int[] prices={2,3,2};
		
		ARaceAgainstTime.run(n, mason_height, heights, prices);
		
		n=4;
		mason_height=5;
		int[] heights2={2,6,2};
		int[] prices2={2,3,2};
		
		ARaceAgainstTime.run(n, mason_height, heights2, prices2);
		
		n=7;
		mason_height=3;
		int[] heights3={2,1,4,3,2,5};
		int[] prices3={-3,5,3,-3,1,2};
		
		ARaceAgainstTime.run(n, mason_height, heights3, prices3);
		
	}

	public static void testCutAStip(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		int n=3;
		int m=2;
		int k=1;
		int[][] arr= {
				{1, 2},
				{3, 4},
				{5, 6}
		};
		CutAStrip.run(n, m, k, arr);

		n=2;
		m=4;
		k=2;
		int[][] arr1= {
				{1,-3,4,-5},
				{2,1,-7,-2}
		};
		CutAStrip.run(n, m, k, arr1);

		n=3;
		m=4;
		k=3;
		int[][] arr2= {
				{-10,1,1,1},
				{-10,1,-1,1},
				{-10,1,1,1}
		};
		CutAStrip.run(n, m, k, arr2);
	}
	
}
