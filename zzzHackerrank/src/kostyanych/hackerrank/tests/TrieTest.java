package kostyanych.hackerrank.tests;

import kostyanych.hackerrank.ds.trees.Trie;

public class TrieTest {

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		Trie tr=new Trie();
		tr.insert("one");
		tr.insert("street");
		tr.insert("straight");
		tr.insert("there");
		tr.insert("the");
		tr.insert("their");
		
		tr.display();
		tr.delete("the");
		tr.display();
		tr.delete("their");
		tr.display();
		tr.delete("there");
		tr.display();
		
		
	}
}
