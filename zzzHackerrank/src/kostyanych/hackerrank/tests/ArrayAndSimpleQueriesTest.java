package kostyanych.hackerrank.tests;

import kostyanych.hackerrank.ds.trees.ArrayAndSimpleQueries;

public class ArrayAndSimpleQueriesTest {
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int m=8;
		int n=4;
		int[] vals={1,2,3,4,5,6,7,8};
		int[][] qs={
				{1,2,4},
				{2,3,5},
				{1,4,7},
				{2,1,4}
		};
		ArrayAndSimpleQueries.runme(m, n, vals, qs);
	}		
}
