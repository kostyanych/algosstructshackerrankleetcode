package kostyanych.hackerrank.tests;

import kostyanych.hackerrank.ds.stacks.LargestRectangle;
import kostyanych.hackerrank.ds.stacks.PoisonousPlants;

public class HRStacksTester {

	public static void testPoisonousPlants(boolean isNeeded) {
		if (!isNeeded)
			return;
		//int[] plants={20,5,2,2,2,5,5,9,12,5};
		//int[] plants={5,4,3,2,1};
		//int[] plants={20,5,6,15,2,2,17,2,11,5,14,5,10,9,19,12,5};
		//int[] plants={150, 5922, 18219, 14284, 2133, 19200, 3086, 8195, 18434, 19493, 17477, 7859, 8071, 11051, 16785, 9811, 16273, 13357, 3591, 18882, 9374, 9705, 18474, 7883};
		
		int[] plants={2,4,6,5,3,1};
		
//		20,5,2,2,2,5,5,9,12,5
//		20,5,2,2,2,5,5
//		20,5,2,2,2,5
//		20,5,2,2,2
		
		
		int days=PoisonousPlants.getMaxDaysMine(plants);
		System.out.println(days);
	}

	public static void testLargetsRectangle(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] heights={4,2,3,1,2,3,3,2,2};
		long res=LargestRectangle.largestRectangle(heights);
		System.out.println(res);
	}
	
}
