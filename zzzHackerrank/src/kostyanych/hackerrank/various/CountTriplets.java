package kostyanych.hackerrank.various;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
//import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class CountTriplets {
	
	static void increaseCountInMap(Long val, Map<Long,Count> map) {
        Count cnt=map.get(val);
        if (cnt==null) {
            cnt=new Count();
            map.put(val,cnt);
        }
        cnt.inc();
	}

	static void decreaseCountInMap(Long val, Map<Long,Count> map) {
        Count cnt=map.get(val);
        if (cnt==null)
            return;
        cnt.dec();
        if (cnt.count==0)
        	map.remove(val);
	}

// Complete the countTriplets function below.
	static long countTriplets(List<Long> lst, long r) {
		if (r==1)
			return calculateOne(lst.size());

		Map<Long,Count> lmap=new HashMap<Long,Count>();
		Count cnt=new Count();
		cnt.inc();
		lmap.put(lst.get(0),cnt);
    
		Map<Long,Count> rmap=new HashMap<Long,Count>();
		Long val;
		for (int i=1;i<lst.size();i++) {
			val=lst.get(i);
	        if (val%r!=0)
	            continue;
	        increaseCountInMap(val,rmap);
		}
    
	    long res=0;
	    Count cnt2;
	    for (int i=1;i<lst.size()-1;i++) {
	        val=lst.get(i);
	        if (val%r!=0) {
	            increaseCountInMap(val,lmap);
	            continue;
	        }
	        decreaseCountInMap(val,rmap);
	        cnt=lmap.get(val/r);
	        if (cnt!=null) {
	            cnt2=rmap.get(val*r);
	            if (cnt2!=null) {
	                res+=(cnt.count*cnt2.count);
	            }
	        }
	        increaseCountInMap(val,lmap);
	        
	/*            
	        if (cnt==null) {
	            increaseCountInMap(val,lmap);
	            decreaseCountInMap(val,rmap);
	            continue;
	        }
	        cnt2=rmap.get(val*r);
	        if (cnt2==null) {
	            increaseCountInMap(val,lmap);
	            decreaseCountInMap(val,rmap);
	            continue;
	        }
	
	        res+=(cnt.count*cnt2.count);
	        increaseCountInMap(val,lmap);
	        decreaseCountInMap(val,rmap);
	*/            
	    }
    
	    return res;
	}

	static long calculateOne(long cnt){
	    long res=0;
	    long n;
	    for (long i=0;i<cnt-2;i++){
	        n=cnt-i;
	        if (n==3)
	            res++;
	        else {
	            res+=((n-3)*n/2+1);
	        }
	    }
	    return res;
	}

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("e://counttriplets.txt"));
			String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
	
			int n = Integer.parseInt(nr[0]);
		    long r = Long.parseLong(nr[1]);
	
		    List<Long> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
		        .map(Long::parseLong)
		        .collect(toList());
		
		    bufferedReader.close();
		    
		    long ans = countTriplets(arr, r);
		    System.out.println(ans);

		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
static class Count {
	    public long count=0;
	    
	    public void inc() {
	        count++;
	    }
	    
	    public void dec() {
	        count--;
	    }
	    
	    public String toString() {
	    	return ""+count;
	    }
	}	
}
