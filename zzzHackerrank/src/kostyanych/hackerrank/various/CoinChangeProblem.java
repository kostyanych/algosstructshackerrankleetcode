package kostyanych.hackerrank.various;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CoinChangeProblem {
	
	static class Part{
        int sum;
        int coinIndex;
        
        public int hashCode() {
            return sum;
        }
        
        public Part(int s, int ci){
            sum=s;
            coinIndex=ci;
        }
        
        public boolean equals(Object obj) {
            if (! (obj instanceof Part) )
                return false;
            Part p2=(Part)obj;
            if (p2.sum==this.sum && p2.coinIndex==this.coinIndex)
                return true;
            return false;
        }
    }
    
    static Map<Part, Long> map=new HashMap<Part, Long>();
    
    static int m;
    static Integer[] coins;

    static long getWays(int sum, int coinIndex){
        Part p=new Part(sum, coinIndex);
        Long lways=map.get(p);
        if (lways!=null)
            return lways;
        long ways=0;
        if (coinIndex==m-1) {//smallest coin
            if (sum%coins[coinIndex]==0) 
                ways=1;
            map.put(p,ways);
            return ways;
        }
        int newSum=sum;
        long innerways=0;
        while (newSum>=0) {
            if (newSum>0) {
                innerways=getWays(newSum, coinIndex+1);
                if (innerways!=0)
                    ways+=innerways;
            }
            else
                ways+=1;
            newSum-=coins[coinIndex];
        }
        map.put(p,ways);
        return ways;
    }

    public static void runme(boolean isNeeded, int _sum, int _coinNum, int[] _coins) {
    	if (!isNeeded)
    		return;
        int n = _sum;
        m = _coinNum;
        coins = new Integer[m];
        for(int c_i=0; c_i < m; c_i++){
            coins[c_i] = _coins[c_i];
        }
        Arrays.sort(coins,Collections.reverseOrder());
        long ways = getWays(n, 0);
        System.out.println(ways);
    }	

}
