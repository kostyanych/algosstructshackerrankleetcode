package kostyanych.hackerrank.various;

public class SeparateTheNumbers {

	static void separateNumbers(String s) {
        int len=s.length();
        long min=-1;
        long first;
        String s1;
        for (int sslen=1;sslen<len/2+1;sslen++) {
            if (min>-1)
                break;
            if (s.charAt(0)=='0')
            	continue;
            s1=s.substring(0,sslen);
            first=Long.parseLong(s1);
            if (checkNextPart(first+1,s,0,sslen)) {
                if (min<0 || min>first)
                    min=first;
            }
        }
        if (min<0)
            System.out.println("NO");
        else
            System.out.println("YES "+min);
    }

    static boolean checkNextPart(Long num, String s, int curpos, int nextpos) {
    	if (s.charAt(nextpos)==0)
    		return false;
    	String snum=""+num;
    	if (s.indexOf(snum, curpos)!=nextpos)
    		return false;
    	if (nextpos+snum.length()==s.length())
    		return true;
    	return checkNextPart(num+1,s,nextpos, nextpos+snum.length());
    }
    
    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
/*
 * 10
1234567891011121314151617181920
5678910111213141516171819202122
9101112131415161718192021222324
979899100101102103104105106107
998999100010011002100310041005
1234567891011120314151617181920
5678910111213140516171819202122
9101112131415160718192021222324
979899100101102003104105106107
998999100010011902100310041005    	
 */
    	String[] arr={"1234567891011121314151617181920",
    			"5678910111213141516171819202122",
    			"9101112131415161718192021222324",
    			"979899100101102103104105106107",
    			"998999100010011002100310041005",
    			"1234567891011120314151617181920",
    			"5678910111213140516171819202122",
    			"9101112131415160718192021222324",
    			"979899100101102003104105106107",
    			"998999100010011902100310041005"};
    	for (int i=0;i<arr.length;i++) {
    		separateNumbers(arr[i]);
    	}
    	
/*        
    	int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String s = scanner.nextLine();

            separateNumbers(s);
        }

        scanner.close();*/
    }	
}
