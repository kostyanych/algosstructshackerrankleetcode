package kostyanych.hackerrank.various;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import kostyanych.hackerrank.various.CircleCity.Input;

public class SherlockAndCounting {
	
	static class Input {
		long d;
		long k;
		
		public Input(long d, long k) {
			this.d=d;
			this.k=k;
		}
		
		public String toString() {
			return ""+d+", "+k;
		}
	}
	
	static long solve(long n, long k) {
        long d = n*(n-4*k);
        if (d<0) {
        	if (1+n*k-n>0)
        		return n-1;
            return 0;
        }
        if (d==0)
            return n-1;
        double x1=(n-Math.sqrt(d))/2.;
        double x2=(n+Math.sqrt(d))/2.;
        int res=0;
        if (x1>0)
            res+=Math.floor(x1);
        if (x2>0 && Math.ceil(x2)<n)
            res+=(n-Math.ceil(x2));
        return res;
    }
	
	public static void testMe1(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(solve(3, 1));
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		List<String> properResults = new ArrayList<>();
		File file=new File("i:\\\\sherlockCountOut.txt");
		try(FileReader fr=new FileReader(file); BufferedReader br=new BufferedReader(fr);) {
			String line;  
			while((line=br.readLine())!=null) {
				properResults.add(line);
			}
		}
		catch(IOException e) {  
			e.printStackTrace();  
		}  
		
		try (Scanner scanner = new Scanner(new FileInputStream("i:\\sherlockCountIn.txt"));) {
	        int t = scanner.nextInt();
	        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
	
	        List<String> myResults = new ArrayList<>();
	        List<Input> inputs = new ArrayList<>();
	        for (int tItr = 0; tItr < t; tItr++) {
	            String[] dk = scanner.nextLine().split(" ");
	
	            long d = Long.parseLong(dk[0]);
	            long k = Long.parseLong(dk[1]);
	            inputs.add(new Input(d,k));
	
	            String result = ""+solve(d, k);
	            myResults.add(result);
	
	            System.out.println(result);
	        }
	        
	        for (int i=0;i<properResults.size();i++) {
	        	if (properResults.get(i).equals(myResults.get(i)))
	        		continue;
	        	System.out.println(inputs.get(i)+" must result in "+properResults.get(i));
	        	System.out.println("---------------------------------");
	        }
		}
		catch(IOException e) {  
			e.printStackTrace();  
		}
	}	
}
