package kostyanych.hackerrank.various;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class DNAHealthMine {

	static final int ALPHAB_LEN=26;
	static int[][] trie;
	static Map<Integer, List<Integer>> mapStatePattern = new HashMap<Integer, List<Integer>>();
	static Map<Integer, boolean[]> mapStatePattern2 = new HashMap<Integer, boolean[]>();
	static int[] fails;
	static int genesQuantity=0;
	
	private static final boolean USE_ARRAYS=false;

    static int getIndex(char c) {
        return c-'a';
    }

    static void addStatePattern2(int state, int patternidx) {
    	boolean[] patlist=mapStatePattern2.get(state);
    	if (patlist==null) {
    		patlist=new boolean[genesQuantity];
    		mapStatePattern2.put(state, patlist);
    	}
    	patlist[patternidx]=true;
    }
    
    static void addStatePatterns2(int indexTo, int indexFrom) {
    	boolean[] lfrom=mapStatePattern2.get(indexFrom);
    	if (lfrom==null)
    		return;
    	boolean[] lto=mapStatePattern2.get(indexTo);
    	if (lto==null) {
    		lto=new boolean[genesQuantity];
    		mapStatePattern2.put(indexTo,lto);
    	}
    	for (int i=0;i<genesQuantity;i++) {
    		lto[i]=lto[i]||lfrom[i];
    	}
    }

    static void addStatePattern(int state, int patternidx) {
    	if (USE_ARRAYS)
    		addStatePattern2(state,patternidx);
    	else
    		addStatePattern1(state,patternidx);
    }
    
    static void addStatePatterns(int indexTo, int indexFrom) {
    	if (USE_ARRAYS)
    		addStatePatterns1(indexTo,indexFrom);
    	else
    		addStatePatterns1(indexTo,indexFrom);
    }
    
    static void addStatePattern1(int state, int patternidx) {
    	List<Integer> patlist=mapStatePattern.get(state);
    	if (patlist==null) {
    		patlist=new ArrayList<Integer>();
    		mapStatePattern.put(state, patlist);
    	}
    	patlist.add(patternidx);
    }
    
    static void addStatePatterns1(int indexTo, int indexFrom) {
    	List<Integer> lfrom=mapStatePattern.get(indexFrom);
    	if (lfrom==null)
    		return;
    	List<Integer> lto=mapStatePattern.get(indexTo);
    	if (lto==null) {
    		lto=new ArrayList<Integer>(lfrom.size());
    		mapStatePattern.put(indexTo,lto);
    	}
    	lto.addAll(lfrom);
    }
    
	static int buildMatchingMachine(String[] patterns) {
		int patcount=patterns.length;
		int maxstates=0;
		for (int i=0; i < patcount; i++) {
			maxstates+=patterns[i].length();
		}
      
		//create and initialize trie to -1
		trie = new int[maxstates][ALPHAB_LEN];
		for (int i=0;i<maxstates;i++) {
			Arrays.fill(trie[i], -1);
		}
      
		int states = 0; 
      
		//building trie with states
		for (int i = 0; i < patcount; ++i) { 
			int currentState = 0; 
			for (int j = 0; j < patterns[i].length(); ++j) { 
				int ch = getIndex(patterns[i].charAt(j)); 
    
				if (trie[currentState][ch] == -1) 
					trie[currentState][ch] = ++states; 
    
				currentState = trie[currentState][ch]; 
			} 
			addStatePattern(currentState,i);
		}
    
		//filling trie with fail jumps
		for (int ch = 0; ch < ALPHAB_LEN; ch++) { 
			if (trie[0][ch] == -1) 
				trie[0][ch] = 0; 
		}
    
		fails = new int[states+1];
		Arrays.fill(fails, -1);
    
		Queue<Integer> q=new LinkedList<Integer>(); 
    
		for (int ch = 0; ch < ALPHAB_LEN; ch++) { 
			if (trie[0][ch] != 0) { 
				fails[trie[0][ch]] = 0; 
				q.add(trie[0][ch]); 
			} 
		} 

		while (q.size()>0) { 
			int state = q.poll(); 
     
			for (int ch = 0; ch < ALPHAB_LEN; ++ch) { 
				if (trie[state][ch] != -1) { 
					int failure = fails[state]; 
					while (trie[failure][ch] == -1) { 
						failure = fails[failure]; 
					}
    
					failure = trie[failure][ch]; 
					fails[trie[state][ch]] = failure;
					addStatePatterns(trie[state][ch], failure);
					q.add(trie[state][ch]); 
				} 
			} 
		}
		
		if (!USE_ARRAYS) {
			Set<Integer> keys=mapStatePattern.keySet();
			List<Integer> l;
			for (Integer k : keys) {
				l=mapStatePattern.get(k);
				if (l!=null)
					Collections.sort(l);
			}
		}
		return states; 
	}
	
    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
        
    	try {
	        String[] genes = {"sherz","hers","err","er"};
	        buildMatchingMachine(genes);
	        int[] health = {1,2,3,4};
	        System.out.println(getHealth("asherza",0,10, health));
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }	

    static int findNextState(int currentState, char nextInput) { 
        int answer = currentState; 
        int ch = getIndex(nextInput); 
        while (trie[answer][ch] == -1) 
            answer = fails[answer]; 
      
        return trie[answer][ch]; 
    } 
    
    static long getHealth(String text, int first, int last, int[] health) { 
        int currentState = 0; 
//        int patcount=health.length;
        int txtlen=text.length();
      
        long res=0;
        
        List<Integer> lst;
        
        for (int i = 0; i < txtlen; i++) { 
            currentState = findNextState(currentState, text.charAt(i)); 
      
            if (USE_ARRAYS) {
            	boolean[] arr=mapStatePattern2.get(currentState);
            	for (int jj=first;jj<=last;jj++) {
            		if (arr[jj])
            			res+=health[jj];
            	}
            }
            else {
	            lst=mapStatePattern.get(currentState);
	            if (lst==null) 
	                 continue;
	            
	            for (Integer idx : lst) {
	            	if (idx<first)
	            		continue;
	            	if (idx>last)
	            		break;
	            	res+=health[idx];
	            }
            }
        }
        return res;
    }     
    
    public static void doMeReal(boolean isNeeded) {
    	if (!isNeeded)
    		return;
        
    	try {
long b1=new Date().getTime();
    		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("e://dnahealth.txt")));        
long b2=new Date().getTime();
System.out.println("InputStream Opened: "+(b2-b1));
b1=new Date().getTime();
			int n = Integer.parseInt(br.readLine().trim());
			genesQuantity=n;
	        String[] genes = br.readLine().split(" ");
b2=new Date().getTime();
System.out.println("genes read: "+(b2-b1));
b1=new Date().getTime();
	        buildMatchingMachine(genes); 
b2=new Date().getTime();
	        int[] health = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
long b3=new Date().getTime();
System.out.println("machine built: "+(b2-b1));
System.out.println("health read: "+(b3-b2));

	        int s = Integer.parseInt(br.readLine().trim());
	
	        long min=Long.MAX_VALUE;
	        long max=0;
b1=new Date().getTime();
	        for (int sItr = 0; sItr < s; sItr++) {
	        	String[] firstLastd = br.readLine().split(" ");
	
	            int first = Integer.parseInt(firstLastd[0]);
	            int last = Integer.parseInt(firstLastd[1]);
	
	            String d = firstLastd[2];
	            long buf = getHealth(d,first,last,health);
	            if (buf<min)
	                min=buf;
	            if (buf>max)
	                max=buf;
	        }
b2=new Date().getTime();
System.out.println("another health got: "+(b2-b1));

	        System.out.println(""+min+" "+max);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }		

}
