package kostyanych.hackerrank.various;

import java.util.Scanner;
import java.util.Stack;

public class SimpleTextEditor {

	static public class Operation {
        public int command;
        public String str;
        
        public Operation(int c, String a){
            command=c;
            str=a;
        }
    }
	
	static Stack<Operation> stack=new Stack<Operation>();
	
	static String text="";
	
	public static void run(boolean isNeeded) {
		if (!isNeeded)
			return;
		Scanner sc=new Scanner(System.in);
		int q=sc.nextInt();
		int c;
		int k;
		String s;
		for (int i=0;i<q;i++){
			c=sc.nextInt();
			switch (c){
				case 1 :
					s=sc.next();
					append(s, true);
					break;
				case 2 :
					k=sc.nextInt();
					delete(k, true);
					break;
				case 3 :
					k=sc.nextInt();
					print(k);
					break;
				case 4 :
					undo();
					break;
			}
		}
		sc.close();
    }
	
	public static void append(String s, boolean needUndo){
		text+=s;
		if (needUndo)
			stack.push(new Operation(1,s));
	}
	
	public static void delete(int k, boolean needUndo) {
		if (needUndo) {
			String s=text.substring(text.length()-k,text.length());
			stack.push(new Operation(2,s));
		}
		text=text.substring(0,text.length()-k);
	}
	
	public static void print(int k) {
		System.out.println(text.charAt(k));
	}
	
	public static void undo() {
		Operation op=stack.pop();
		if (op.command==2)
			append(op.str,false);
		else
			delete(op.str.length(),false);
	}
	
	public static void printText() {
		System.out.println(text);
	}
	
}
