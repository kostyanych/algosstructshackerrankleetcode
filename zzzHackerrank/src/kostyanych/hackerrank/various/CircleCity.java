package kostyanych.hackerrank.various;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CircleCity {
	
	static class Input {
		long d;
		long k;
		
		public Input(long d, long k) {
			this.d=d;
			this.k=k;
		}
		
		public String toString() {
			return ""+d+", "+k;
		}
	}

	static private final String POSSIBLE = "possible";
	static private final String IMPOSSIBLE = "impossible";
	static private final Map<Long, Integer> map = new HashMap<>();

	// Complete the solve function below.
	private static String solve(long d, long k) {
//		if (k == 0)
//			return POSSIBLE;
		
		long buf = (long) Math.sqrt(d);
		long sqrt = 0;
		if (d == buf * buf)
			sqrt=buf;
		
		int res;
		Integer ig = map.get(d);
		if (ig != null)
			res = ig.intValue();
		else {
			res = sqrt==0 ? 0 : 4;
			for (int i = 1; i < buf+1; i++) {
				if (root(d - (i * i)) > 0)
					res += 4;
			}
			map.put(d, res);
		}
		// System.out.println(res);
		if (res <= k)
			return POSSIBLE;
		return IMPOSSIBLE;
	}

	private static long root(long d) {
		long buf = (long) Math.sqrt(d);
		if (d != buf * buf)
			return 0;
		return buf;
	}

	public static void testMe1(boolean isNeeded) {
		if (!isNeeded)
			return;
		System.out.println(solve(2,0));
	}	
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		List<String> properResults = new ArrayList<>();
		File file=new File("i:\\\\circlecityout.txt");
		try(FileReader fr=new FileReader(file); BufferedReader br=new BufferedReader(fr);) {
			String line;  
			while((line=br.readLine())!=null) {
				properResults.add(line);
			}
		}
		catch(IOException e) {  
			e.printStackTrace();  
		}  
		
		try (Scanner scanner = new Scanner(new FileInputStream("i:\\circlecityin.txt"));) {
	        int t = scanner.nextInt();
	        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
	
	        List<String> myResults = new ArrayList<>();
	        List<Input> inputs = new ArrayList<>();
	        for (int tItr = 0; tItr < t; tItr++) {
	            String[] dk = scanner.nextLine().split(" ");
	
	            long d = Long.parseLong(dk[0]);
	            long k = Long.parseLong(dk[1]);
	            inputs.add(new Input(d,k));
	
	            String result = solve(d, k);
	            myResults.add(result);
	
	            System.out.println(result);
	        }
	        
	        for (int i=0;i<properResults.size();i++) {
	        	if (properResults.get(i).equals(myResults.get(i)))
	        		continue;
	        	System.out.println(inputs.get(i)+" must result in "+properResults.get(i));
	        	System.out.println("---------------------------------");
	        }
		}
		catch(IOException e) {  
			e.printStackTrace();  
		}
	}
}
