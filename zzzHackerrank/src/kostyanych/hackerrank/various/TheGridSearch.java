package kostyanych.hackerrank.various;

import java.io.File;
import java.util.Scanner;

public class TheGridSearch {

	public static void runme(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		try {
			File f=new File("e:\\grsearch.txt");
			Scanner in = new Scanner(f);
	        int t = in.nextInt();
	        for(int a0 = 0; a0 < t; a0++){
	            int R = in.nextInt();
	            int C = in.nextInt();
	            String[] G = new String[R];
	            for(int G_i=0; G_i < R; G_i++){
	                G[G_i] = in.next();
	            }
	            int r = in.nextInt();
	            int c = in.nextInt();
	            String[] P = new String[r];
	            for(int P_i=0; P_i < r; P_i++){
	                P[P_i] = in.next();
	            }
	            search(G,P,C,c);
	        }
	        in.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
    }
    
    static void search(String[] g, String[] p, int gc, int pc){
        int curGR=0;
        while (curGR<=(g.length-p.length)) {
            for (int i=0;i<=gc-pc;i++){
                if (g[curGR].charAt(i)==p[0].charAt(0)) {
                    if (check(g,p,curGR,i, pc)){
                        System.out.println("YES");
                        return;
                    }
                }
            }
            curGR++;
        }
        System.out.println("NO");
    }
               
    static boolean check(String[] g, String[] p, int gStartR, int gStartC, int pc){
        for (int j=0;j<p.length;j++){
            for (int i=0;i<pc;i++) {
                if (g[gStartR+j].charAt(gStartC+i)!=p[j].charAt(i))
                    return false;
            }
        }
        return true;
    }

}
