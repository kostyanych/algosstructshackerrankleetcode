package kostyanych.hackerrank.various;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Query {
	public int first;
	public int last;
	public String str;
	
	public Query(String _str, int _first, int _last) {
		first=_first;
		last=_last;
		str=_str;
	}
}

public class DNAHealth3 {

    static final int ALPHAB_LEN = 26; 
//    static BitSet[] out; 
//    static int[] fails; 
    static int[][] trie;
    static int[] patternNodes;
    static int[] sufflink;
    static int sz=0;
    static List<Integer>[] queryNodes;
    static List<Integer>[] adj;
    static int ticks=0;
    static int[] lo;
    static int[] hi;
    static long[] BIT;
	
//	const int N = 100002, MAXSIZE = 2000002;
//	int n, sz, trie[MAXSIZE][26], ends_here[MAXSIZE], sufflink[MAXSIZE];
    static List<Integer>[] subtract;//=new List<Integer>[]
    static List<Integer>[] add;//=new List<Integer>[]
    static long[] ans;
	
	static int getIndex(char c) {
		return c-'a';
	}

	static void insertPatternString(String str, int idx){
		int len=str.length();
	    int currState = 0;
	    int ch;
	    for(int i=0;i<len;i++){
	        ch = getIndex(str.charAt(i));
	        if(trie[currState][ch] == -1)
	            trie[currState][ch] = ++sz;
	        currState = trie[currState][ch];
	    }
	    patternNodes[idx] = currState;
	}
	
	static void insertQueryString(String str, int idx){
		int len=str.length();
	    int currState = 0;
	    int ch;
	    for(int i=0;i<len;i++){
	    	ch = getIndex(str.charAt(i));
	        if(trie[currState][ch] == -1)
	            trie[currState][ch] = ++sz;
	        currState = trie[currState][ch];
	        if (queryNodes[idx]==null)
	        	queryNodes[idx]=new ArrayList<Integer>();
	        queryNodes[idx].add(currState);
	    }
	}
	
	static void bfs(){
	    sufflink[0] = 0;
	    Queue<Integer> qq = new LinkedList<Integer>();
	    for(int i=0;i<ALPHAB_LEN;i++) {
	        if(trie[0][i] != -1) {
	            sufflink[trie[0][i]] = 0;
	            qq.add(trie[0][i]);
	            if (adj[0]==null)
	            	adj[0]=new ArrayList<Integer>();
	            adj[0].add(trie[0][i]);
	        }
	    }

	    while(!qq.isEmpty()){
	    	int v = qq.remove();

    		for(int ch=0;ch<ALPHAB_LEN;ch++){
    			int vv = trie[v][ch];
    			if (vv == -1)
    				continue;

    			int j = sufflink[v];
    			while (j > 0 && trie[j][ch] == -1) {
    				j = sufflink[j];
    			}
    			if (trie[j][ch] != -1)
    				sufflink[vv] = trie[j][ch];

    			if(sufflink[vv] != vv){
	    			//create the new tree
    	            if (adj[sufflink[vv]]==null)
    	            	adj[sufflink[vv]]=new ArrayList<Integer>();
	    			adj[sufflink[vv]].add(vv);
    			}
	    		qq.add(vv);
	    	}
	    }
	}
	
	static int nextState(int v, int ch) {
	    int j = v;
	    while(j > 0 && trie[j][ch] == -1) {
	    	j = sufflink[j];
	    }
	    if (trie[j][ch] != -1)
	    	j = trie[j][ch];
	    return j;
	}

	static void dfs(int v){
	    lo[v] = ++ticks;
	    int len=adj[v].size();
	    for(int i=0;i<len;i++){
	        dfs(adj[v].get(i));
	    }
	    hi[v] = ticks;
	}

	static void update(int idx, long val){
	    while(idx <= ticks){
	        BIT[idx] += val;
	        idx += idx & (-idx);
	    }
	}

	static long query(int idx){
	    long ans = 0;
	    while(idx>0){
	        ans += BIT[idx];
	        idx -= idx & (-idx);
	    }
	    return ans;
	}

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;

    	try {

    		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("e://dnahealth.txt")));        
			int nGenes = Integer.parseInt(br.readLine().trim());
	        String[] genes = br.readLine().split(" ");
			int maxstates=0;
			for (int i=0; i<nGenes; i++) {
				maxstates+=genes[i].length();
			}
	        
	        int[] health = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();

	        int s = Integer.parseInt(br.readLine().trim());

	        queryNodes = new List[s+1];
	        subtract = new List[nGenes];
	        add = new List[nGenes];
	        
	        Query[] qs=new Query[s];
	        for(int i=0;i<s;i++){
	            String[] firstLastd = br.readLine().split(" ");
	            int first = Integer.parseInt(firstLastd[0])+1;
	            int last = Integer.parseInt(firstLastd[1])+1;
	            String d = firstLastd[2];
	            qs[i]=new Query(d, first, last);
	            maxstates+=d.length();
	        }

	        trie = new int[maxstates][ALPHAB_LEN];
	        for (int i=0;i<maxstates;i++) {
	            Arrays.fill(trie[i], -1);
	        }
	        sufflink = new int[maxstates];
	        adj = new List[maxstates];
	        
	        patternNodes = new int[nGenes+1];
	        
		    for(int i=1;i<=nGenes;i++){
		        insertPatternString(genes[i-1],i);
		    }
	        
	        for(int i=1;i<=s;i++){
	            insertQueryString(qs[i-1].str,i);
	            if (subtract[qs[i-1].first-1]==null)
	            	subtract[qs[i-1].first-1]=new ArrayList<Integer>();
	            subtract[qs[i-1].first-1].add(i);
	            if (add[qs[i-1].last]==null)
	            	add[qs[i-1].last]=new ArrayList<Integer>();
	            add[qs[i-1].last].add(i);
	        }

	        //create suffix links
	        bfs();
	        ticks = 0;
	        //create the new tree
	        dfs(0);
	        
	        for(int i=1;i<=nGenes;i++){
	            //"activate" this gene by executing range addition
	            int node_in_trie = patternNodes[i];
	            update(lo[node_in_trie], health[i]);
	            update(hi[node_in_trie]+1, -health[i]);

	            //subtract the value from all queries whose left endpoint is L+1
	            for(Integer idx : subtract[i]){
	                for(Integer node : queryNodes[idx])
	                    ans[idx] -= query(lo[node]);
	            }

	            //add the value to all queries whose right endpoint is R
	            for(Integer idx : add[i]){
	                for(Integer node : queryNodes[idx])
	                    ans[idx] += query(lo[node]);
	            }
	        }

	        long min=Long.MAX_VALUE;
	        long max=0;
	        for (int i=0;i<ans.length;i++) {
	        	if (ans[i]>max)
	        		max=ans[i];
	        	if (ans[i]<min)
	        		min=ans[i];
	        }
	        System.out.println(""+min+" "+max);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
	}
	
}
