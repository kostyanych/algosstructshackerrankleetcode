package kostyanych.hackerrank.various;

public class TwoCharacters {

	static int[] charCount=new int[26];
    static int max=0;
    
    public static void run(boolean isNeeded) {
    	if (!isNeeded)
    		return;

        String s = "cwomzxmuelmangtosqkgfdqvkzdnxerhravxndvomhbokqmvsfcaddgxgwtpgpqrmeoxvkkjunkbjeyteccpugbkvhljxsshpoymkryydtmfhaogepvbwmypeiqumcibjskmsrpllgbvc";
        int len=s.length();
        
        char c;
        int ind;
        for (int i=0;i<len;i++){
            c=s.charAt(i);
            ind=(int)c-(int)'a';
            charCount[ind]++;
        }
        
        for (int i=0;i<26;i++){
            if (charCount[i]==0)
                continue;
            calc(s,i,i+1);
        }
        
        System.out.println(max);
    }
    
    static void calc(String s, int thisI, int nextI){
        if (thisI>24 || nextI>25)
            return;
        char c;
        int ind;
        for (int cnt=nextI;cnt<26;cnt++) {
            if (charCount[cnt]==0)
                continue;
            if (charCount[cnt]!=charCount[thisI]
                    &&  charCount[cnt]!=charCount[thisI]+1
                    &&  charCount[cnt]!=charCount[thisI]-1)
                continue;
            int len=s.length();
            
            int lastInd=-1;
            int tlen=0;
            
            for (int i=0;i<len;i++){
                c=s.charAt(i);
                ind=(int)c-(int)'a';
                if (ind==lastInd) {
                    tlen=0;
                    break;
                }
                if (ind==thisI || ind==cnt) {
                    lastInd=ind;
                    tlen++;
                }
            }
            if (tlen<2)
                continue;
            if (max<tlen)
                max=tlen;
        }
    }
}
