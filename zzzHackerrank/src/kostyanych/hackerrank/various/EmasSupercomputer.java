package kostyanych.hackerrank.various;

public class EmasSupercomputer {
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		int n=10;
		int m=9;
		String[] rows={
				"BBBGBGBBB",
				"BBBGBGBBB",
				"BBBGBGBBB",
				"GGGGGGGGG",
				"BBBGBGBBB",
				"BBBGBGBBB",
				"GGGGGGGGG",
				"BBBGBGBBB",
				"BBBGBGBBB",
				"BBBGBGBBB"
		};
		runme(n,m,rows);
	}

	 public static void runme(int n, int m, String[] rows) {
	        int[][] field=new int[n][m];
	        String str;
	        for (int i=0;i<n;i++){
	            str=rows[i];
	            field[i]=convertStr(str,m);
	        }
	        
	        int buf;
	        int buf2;
	        int maxP=0;
	        for (int i=0;i<n;i++){
	            for (int j=0;j<m;j++){
	            	buf=calcRadius(i,j,n-1,m-1,field);
	            	if (buf==0)
	            		continue;
	            	buf2=findMaxProduct(field, i, j, buf, n, m);
	            	if (buf2==0)
	            		continue;
	            	if (maxP<buf2)
	            		maxP=buf2;
	            }
	        }
	        System.out.println(maxP);
	    }

	    static int calcRadius(int row,int col,int maxRow,int maxCol,int[][] field){
	        if (field[row][col]==0)
	            return 0;
	        int res=1;
	        int min=min(row,col);
	        min=min(min,maxRow-row);
	        min=min(min,maxCol-col);
	        for (int i=1;i<=min;i++){
	            if (field[row+i][col]*field[row-i][col]*field[row][col-i]*field[row][col+i]==0)
	                break;
	            res++;
	        }
	        return res;
	    }
	    
	    static void makeCleanedCopy(int[][] field,int[][] copyArr,int row,int col, int rad){
	    	int len;
	    	for (int i=0;i<field.length;i++){
	    		len=field[i].length;
	    		copyArr[i]=new int[len];
	    		System.arraycopy(field[i], 0, copyArr[i], 0, len);
	    	}
//	    	System.arraycopy(field, 0, copyArr, 0, field.length);
	    	copyArr[row][col]=0;
	    	for (int i=0;i<rad;i++){
	    		copyArr[row+i][col]=0;
	    		copyArr[row-i][col]=0;
	    		copyArr[row][col+i]=0;
	    		copyArr[row][col-i]=0;
	    	}
	    }
	    
	    static int findMaxProduct(int[][]field, int row,int col, int rad,int n, int m) {
	    	int max=0;
	    	int buf=0;
	    	int[][] copyArr=new int[n][m];
	    	for (int i=1;i<=rad;i++){
            	makeCleanedCopy(field,copyArr,row,col,i);
            	buf=findMaxRadius(copyArr,n-1,m-1);
            	if (buf==0)
            		break;
            	buf=(1+4*(buf-1))*(1+4*(i-1));
            	if (buf>max)
            		max=buf;
	    	}
	    	return max;
	    }
	    
	    static int findMaxRadius(int[][]copyArr, int lastRowInd, int lastColInd){
	    	int max=0;
	    	int buf;
	    	for (int i=0;i<=lastRowInd;i++){
	    		for (int j=0;j<=lastColInd;j++){
	    			buf=calcRadius(i,j,lastRowInd,lastColInd,copyArr);
	    			if (buf>max)
	    				max=buf;
	    		}
	    	}
	    	return max;
	    }
	 
	    static int calcSuare(int row,int col,int maxRow,int maxCol,int[][] field){
	        if (field[row][col]==0)
	            return 0;
	        int res=1;
	        int min=min(row,col);
	        min=min(min,maxRow-row);
	        min=min(min,maxCol-col);
	        for (int i=1;i<=min;i++){
	            if (field[row+i][col]*field[row-i][col]*field[row][col-i]*field[row][col+i]==0)
	                break;
	            res+=4;
	        }
System.out.println(row+","+col+"     res="+res);
	        return res;
	    }
	    
	    static int min(int a,int b){
	        if (a<=b)
	            return a;
	        return b;
	    }
	    
	    static int[] convertStr(String str, int len){
	        int[] res=new int[len];
	        for (int i=0;i<len;i++){
	            if (str.charAt(i)=='G')
	                res[i]=1;
	        }
	        return res;
	    }	

}
