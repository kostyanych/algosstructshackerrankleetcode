package kostyanych.hackerrank.various;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Comparator;

public class BigSorting {
	
	public static void testMe(boolean isNeeded){
		if (!isNeeded)
			return;
		String[] a={
				"31415926535897932384626433832795",
				"1",
				"3",
				"10",
				"3",
				"5"};
		run(a);
	}
	
	public static void run(String[] unsorted) {
		int n=unsorted.length;
        Arrays.sort(unsorted,new Comparator<String>(){
            public int compare(String o1, String o2) {
                return o1.length()-o2.length();
            } 
        });
        BigInteger[] lng=new BigInteger[n];
        for(int i=0; i < n; i++){
            lng[i]=new BigInteger(unsorted[i]);
        }
        Arrays.sort(lng);
        for(int i=0; i < n; i++){
            System.out.println(lng[i]);
        }
    }

}
