package kostyanych.hackerrank.various;

public class ArrayPacker {

	public static void packSortedArray(boolean isNeeded, int[] arr) {
		if (!isNeeded)
			return;

		int lastPlace=0;
        int i=1;
        int n=arr.length;
        while (i<n) {
            if (arr[i]==arr[lastPlace]) {
                i++;
            }
            else {
            	arr[lastPlace+1]=arr[i];
                lastPlace++;
                i++;
            }
        }
        
        for (int j=lastPlace+1;j<n;j++) {
        	arr[j]=0;
        }
	}
	
	public static void printArray(int[] arr) {
		System.out.print(arr[0]);
		for (int i=1;i<arr.length;i++){
			System.out.print(" "+arr[i]);
		}
		System.out.print("\r\n");
	}
}
