package kostyanych.hackerrank.various;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class SherlockAndGCD {
	static final Set<Integer> odds=new HashSet<Integer>();
    static final Set<Integer> evens=new HashSet<Integer>();

    // Complete the solve function below.
    static String solve(int[] a) {
    	if (a.length==1) {
    		if (a[0]==1)
    			return "YES";
    		else
    			return "NO";
    	}
    	
    	int curgcd=eucl(a[0],a[1]);
    	for (int i=2;i<a.length;i++) {
    		if (curgcd==1)
    			return "YES";
    		curgcd=eucl(curgcd,a[i]);
    	}
    	return "NO";
    }
    
    static int eucl(int p, int q) {
        int temp;
        while (q != 0) {
            temp = q;
            q = p % q;
            p = temp;
        }
        return p;
    }

    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	try {
	    	Scanner scanner = new Scanner(new FileInputStream("e://sherlockgcd.txt"));
	
	        int t = scanner.nextInt();
	        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
	
	        for (int tItr = 0; tItr < t; tItr++) {
	            int aCount = scanner.nextInt();
	            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
	
	            int[] a = new int[aCount];
	
	            String[] aItems = scanner.nextLine().split(" ");
	            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
	
	            for (int aItr = 0; aItr < aCount; aItr++) {
	                int aItem = Integer.parseInt(aItems[aItr]);
	                a[aItr] = aItem;
	            }
	
	            String result = solve(a);
	            System.out.println(result);
	        }
	
	        scanner.close();
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}
