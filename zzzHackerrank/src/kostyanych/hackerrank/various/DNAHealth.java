package kostyanych.hackerrank.various;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

public class DNAHealth {

    static final int ALPHAB_LEN = 26; 
    static BitSet[] out; 
    static int[] fails; 
    static int[][] trie;
     
    static int getIndex(char c) {
        return c-'a';
    }
    
    static int buildMatchingMachine(String[] patterns) {
    	return buildMatchingMachine(patterns, 0, patterns.length-1);
    }
    
    static int buildMatchingMachine(String[] patterns, int start, int end) {
//        int patcount=patterns.length;
        int maxstates=0;
        for (int i=start; i <= end; i++) {
            maxstates+=patterns[i].length();
        }
        out = new BitSet[maxstates];
        for (int i=0;i<maxstates;i++) {
        	out[i]=new BitSet(end-start+1);
        }
        
        trie = new int[maxstates][ALPHAB_LEN];
        for (int i=0;i<maxstates;i++) {
            Arrays.fill(trie[i], -1);
        }
        int states = 0; 
      
        for (int i = start; i <= end; ++i) { 
            int currentState = 0; 
            for (int j = 0; j < patterns[i].length(); ++j) { 
                int ch = getIndex(patterns[i].charAt(j)); 
      
                if (trie[currentState][ch] == -1) 
                    trie[currentState][ch] = ++states; 
      
                currentState = trie[currentState][ch]; 
            } 
            out[currentState].set(i); 
        } 
      
        for (int ch = 0; ch < ALPHAB_LEN; ch++) { 
            if (trie[0][ch] == -1) 
                trie[0][ch] = 0; 
        }
      
        fails = new int[states+1];
        Arrays.fill(fails, -1);
      
        Queue<Integer> q=new LinkedList<Integer>(); 
      
        for (int ch = 0; ch < ALPHAB_LEN; ch++) { 
            if (trie[0][ch] != 0) { 
                fails[trie[0][ch]] = 0; 
                q.add(trie[0][ch]); 
            } 
        } 
 
        while (q.size()>0) { 
            int state = q.poll(); 
       
            for (int ch = 0; ch < ALPHAB_LEN; ++ch) { 
                if (trie[state][ch] != -1) { 
                    int failure = fails[state]; 
                    while (trie[failure][ch] == -1) { 
                        failure = fails[failure]; 
                    }
      
                    failure = trie[failure][ch]; 
                    fails[trie[state][ch]] = failure; 
                    out[trie[state][ch]].or(out[failure]); 
                    q.add(trie[state][ch]); 
                } 
            } 
        } 
        return states; 
    } 
      
    static int findNextState(int currentState, char nextInput) { 
        int answer = currentState; 
        int ch = getIndex(nextInput); 
        while (trie[answer][ch] == -1) 
            answer = fails[answer]; 
      
        return trie[answer][ch]; 
    } 
      
    static int getHealth(String text, int first, int last, int[] health, String[] genes) { 
        int currentState = 0; 
        int patcount=health.length;
        int txtlen=text.length();
      
        int res=0;
        for (int i = 0; i < txtlen; i++) { 
            currentState = findNextState(currentState, text.charAt(i)); 
      
            if (out[currentState].isEmpty()) 
                 continue; 
      
            for (int j = first; j <= last; j++) { 
                if (out[currentState].get(j)) {
                        res+=health[j];
//                        System.out.println(genes[j]+"\t"+health[j]);
                } 
            } 
        }
        return res;
    }     

    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
        
    	try {
long b1=new Date().getTime();
    		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("e://dnahealth.txt")));        
long b2=new Date().getTime();
System.out.println("InputStream Opened: "+(b2-b1));
b1=new Date().getTime();
			int n = Integer.parseInt(br.readLine().trim());
	        String[] genes = br.readLine().split(" ");
b2=new Date().getTime();
System.out.println("genes read: "+(b2-b1));
b1=new Date().getTime();
//	        buildMatchingMachine(genes); 
b2=new Date().getTime();
	        int[] health = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
long b3=new Date().getTime();
System.out.println("machine built: "+(b2-b1));
System.out.println("health read: "+(b3-b2));

	        int s = Integer.parseInt(br.readLine().trim());
	
	        int min=Integer.MAX_VALUE;
	        int max=0;
b1=new Date().getTime();
	        for (int sItr = 0; sItr < s; sItr++) {
	            String[] firstLastd = br.readLine().split(" ");
	
	            int first = Integer.parseInt(firstLastd[0]);
	            int last = Integer.parseInt(firstLastd[1]);
	            
	            buildMatchingMachine(genes, first, last);
	
	            String d = firstLastd[2];
	            int buf = getHealth(d,first,last,health, genes);
	            if (buf<min)
	                min=buf;
	            if (buf>max)
	                max=buf;
	        }
b2=new Date().getTime();
System.out.println("another health got: "+(b2-b1));

	        System.out.println(""+min+" "+max);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }	
}
