package kostyanych.hackerrank.various;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IceCreamParlor {

	static int N;
    static int money;
    static Map<Integer,List<Integer>> map = new HashMap<Integer,List<Integer>>();

    static void whatFlavors(int[] cost) {
        map.clear();
        List<Integer> lst;
        for (int i=0;i<N;i++) {
            lst=map.get(cost[i]);
            if (lst==null) {
                lst=new ArrayList<Integer>();
                map.put(cost[i], lst);
            }
            lst.add(i);
        }
//        Arrays.sort(cost, (c1, c2)->c1.cost-c2.cost );
        int id1=-1;
        int id2=-1;
        int cost1;
        for (int i=0;i<N;i++) {
            id1=i;
            cost1=cost[i];
            lst=map.get(money-cost1);
            if (lst!=null && lst.size()>0) {
                if (money-cost1==cost1) {
                	if (lst.size()==1)
                		continue;
                    id2=lst.get(1);
                }
                else
                    id2=lst.get(0);
                break;
            }
        }
        id1++;
        id2++;
        if (id1<id2)
            System.out.println(""+id1+" "+id2);
        else
            System.out.println(""+id2+" "+id1);
    }
    
    public static void doIt(boolean doit){
    	if (!doit)
    		return;
    	
//    	12
//    	5
//    	7 2 5 4 11
    	
        N=5;
        money=8;
        int[] cost=new int[]{4,3,2,5,7};
        whatFlavors(cost);
    	
    }
    
    public static long calculateOne(long cnt){
        long res=0;
        long n;
        for (long i=0;i<cnt-2;i++){
            n=cnt-i;
            if (n==3)
                res++;
            else {
                res+=((n-3)*n/2+1);
            }
        }
        return res;
    }
}
