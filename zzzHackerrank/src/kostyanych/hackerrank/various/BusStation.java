package kostyanych.hackerrank.various;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BusStation {

	 // Complete the solve function below.
    static long[] solve(int[] a) {
        List<Long> lst=new ArrayList<Long>();
        int n=a.length;
        long max;
        long[] sum=new long[n];
        sum[0]=a[0];
        max=a[0];
        for (int i=1;i<n;i++) {
            sum[i]=a[i]+sum[i-1];
            if (a[i]>max)
                max=a[i];
        }
        int maxsteps=(int)(sum[n-1]/max);
        boolean needEven=(sum[n-1]%2==0);
        int left;
        int start=0;
        int cnt=0;
        int size;
        for (int i=maxsteps;i>1;i--) {
            if (i%2==0 && !needEven)
                continue;
        	if (sum[n-1]%i!=0)
                continue;
            long multipl;
            size=(int)(sum[n-1]/i);
            cnt=1;
            for (int j=start;j<n-1;j++) {
                if (sum[j]>size && j>0 && cnt<2)
                    start=j;
                multipl=size*cnt;
                if (sum[j]>multipl)
                	break;
                if (sum[j]==multipl)
                    cnt++;
                if (cnt==i) {
                    lst.add((long)size);
                    break;
                }
            }
        }
        lst.add(sum[n-1]);
        long[] res=new long[lst.size()];
        for (int i=0;i<res.length;i++) {
            res[i]=lst.get(i);
        }
        return res;

    }
    
    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	int[] arr={1,2,1,1,1,2,1,3};
    	
    	long[] result = solve(arr);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
        	System.out.print(result[resultItr]+" ");
        }    	
    }
    
    public static void doMeFile(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	try {
    		Scanner scanner = new Scanner(new FileInputStream("e://busstation.txt"));
    		int aCount = scanner.nextInt();
    		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    		int[] a = new int[aCount];

    		String[] aItems = scanner.nextLine().split(" ");
    		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
    		scanner.close();

    		for (int aItr = 0; aItr < aCount; aItr++) {
    			int aItem = Integer.parseInt(aItems[aItr]);
    			a[aItr] = aItem;
    		}

    		long[] result = solve(a);

    		for (int resultItr = 0; resultItr < result.length; resultItr++) {
    			System.out.print(result[resultItr]+" ");
            }
        }
        catch (Exception e) {
        	e.printStackTrace();
        }
    }    
}
