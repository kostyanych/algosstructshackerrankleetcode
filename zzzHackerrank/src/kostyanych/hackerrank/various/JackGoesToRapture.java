package kostyanych.hackerrank.various;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class JackGoesToRapture {
	
	public static void testMe(boolean isNeeded){
		if (!isNeeded)
			return;
		
//		int n=5;
//		int e=5;
//		long[][] edges= {
//				{1,2,60},
//				{3,5,70},
//				{1,4,120},
//				{4,5,150},
//				{2,3,80}
//			};
		int n=8;
		int e=9;
		long[][] edges= {
				{1,2,20},
				{1,5,9},
				{1,6,1},
				{2,5,9},
				{2,3,10},
				{2,4,5},
				{6,7,2},
				{4,7,1},
				{3,8,1}
			};
		
		runme(n, e, edges);
	}

	static class PricedEdge {
        int v2;
        long price;
        
        public PricedEdge(int _v, long _pr){
            v2=_v;
            price=_pr;
        }
    }
    
    static private List<PricedEdge>[] adj;
    static private Long[] rescost;
    static Set<Integer> visited=new HashSet<Integer>();
    static Queue<Integer> jobs = new LinkedList<Integer>();
    
    static int n;

    public static void runme(int _n, int e, long[][] edges) {
    	n=_n;
        adj = new List[n];
        for (int i=0; i<n; ++i) {
            adj[i] = new ArrayList<PricedEdge>();
        }
        rescost = new Long[n];
        rescost[0] = 0l;
        int p1;
        int p2;
        long cost;
        for (int i=0;i<e;i++) {
            p1=(int)edges[i][0];
            p2=(int)edges[i][1];
            cost=(int)edges[i][2];
            addEdge(p1,p2,cost);
        }
        //jobs.add(0);
        //processPaths2();
        //processPaths(0, -1);
        //processPaths3(0, null);
        processPaths4(0, null, -1);
//        if (rescost[n-1]==null)
//            System.out.println("NO PATH EXISTS");
//        else
//            System.out.println(rescost[n-1]);
        
      if (curMinCost<0)
    	  System.out.println("NO PATH EXISTS");
      else
    	  System.out.println(curMinCost);
    }
    
    static void addEdge(int p1, int p2, long price){
        adj[p1-1].add(new PricedEdge(p2-1, price));
        adj[p2-1].add(new PricedEdge(p1-1, price));
    }
    
    static void processPaths(int node, int parent){
        if (adj[node]==null || adj[node].size()<1)
            return;
        long curCost=0;
        for (PricedEdge pe : adj[node]) {
            if (pe.v2==parent)
                continue;
//            curCost=pe.price-rescost[node];
//            if (curCost<=0)
//                curCost=rescost[node];
            if (pe.price>rescost[node])
                curCost=pe.price;
            else
                curCost=rescost[node];
            if (rescost[pe.v2]!=null){
                if (rescost[pe.v2]<=curCost)
                    continue;
                rescost[pe.v2]=curCost;
            }
            else
            	rescost[pe.v2] = curCost;
            processPaths(pe.v2,node);
        }
    }
    
    static void processPaths2(){
        if (jobs.isEmpty())
            return;
        int node=jobs.remove();
        if (adj[node]==null || adj[node].size()<1)
            return;
        long curCost=0;
        for (PricedEdge pe : adj[node]) {
            if (visited.contains(pe.v2))
                continue;
            if (pe.price>rescost[node])
                curCost=pe.price;
            else
                curCost=rescost[node];
            if (rescost[pe.v2]!=null){
                if (rescost[pe.v2]<=curCost)
                    continue;
                rescost[pe.v2]=curCost;
            }
            else
            	rescost[pe.v2] = curCost;
            jobs.add(pe.v2);
        }
        visited.add(node);
        processPaths2();
    }
    
    static void processPaths3(int node, Set<Integer> visited){
        if (adj[node]==null || adj[node].size()<1)
            return;
        Set<Integer> thisVisited=new HashSet<Integer>();
        if (visited!=null)
        	thisVisited.addAll(visited);
        thisVisited.add(node);
        long curCost=0;
        for (PricedEdge pe : adj[node]) {
            if (thisVisited.contains(pe.v2))
                continue;
            if (pe.price>rescost[node])
                curCost=pe.price;
            else
                curCost=rescost[node];
            if (rescost[pe.v2]!=null){
                if (rescost[pe.v2]<=curCost)
                    continue;
                rescost[pe.v2]=curCost;
            }
            else
            	rescost[pe.v2] = curCost;
            processPaths3(pe.v2,thisVisited);
        }
    }

    static long curMinCost=-1;
    static void processPaths4(int node, Set<Integer> visited, long _thisPathCost){
    	long thisPathCost=_thisPathCost;
    	if (node==(n-1)) {
    		curMinCost=thisPathCost;
    		return;
    	}
        if (adj[node]==null || adj[node].size()<1)
            return;
        Set<Integer> thisVisited=new HashSet<Integer>();
        if (visited!=null)
        	thisVisited.addAll(visited);
        thisVisited.add(node);
        long curCost=0;
        for (PricedEdge pe : adj[node]) {
            if (thisVisited.contains(pe.v2))
                continue;
            if (pe.price>_thisPathCost)
            	thisPathCost=pe.price;
            if (curMinCost>0 && thisPathCost>curMinCost)
            	continue;
            processPaths4(pe.v2,thisVisited, thisPathCost);
        }
    }

}
