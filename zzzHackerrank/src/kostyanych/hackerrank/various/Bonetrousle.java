package kostyanych.hackerrank.various;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Bonetrousle {
	
	static final boolean doLog=false;
	
	static long[] bonetrousle(long n, long k, int b) {
        long[] err={-1};
        long[] res=new long[b];
        calc(res,1,k,n,0,b);
        return res;
    }	

	static void calc(long[] arr, long st, long end, long rem, int ind, int b) {
        if (b>=ind-1)
            return;
        for (long i=st;i<=end;i++) {
            if (i>rem) {
                arr[ind]=-1;
            }

            arr[ind]=i;
            calc(arr,i+1,end,rem-i,ind+1,b-1);
        }
    }
	
	
	final static long[] err={-1};

    static long[] bonetrousle2(long n, long k, int b) {
        if (b==1) {
            if (n>k)
                return err;
            return (new long[]{n});
        }
        long min=(1+(long)b)*(long)b/2;
        if (n<min)
            return err;
        long[] res=new long[b];
        for (int i=0;i<b;i++) {
            res[i]=i+1;
        }
        
        int ind=b-1;
        long cursum=min;
        long curmax=k;
        log("n="+n+"   k="+k);
        while (true) {
        	log("cursum="+cursum+"   curmax="+curmax+" ind="+ind);
            if (cursum==n)
                return res;
            if (ind <0)
                return err;

            if (n-cursum <= curmax-res[ind]) {
                res[ind]+=(n-cursum);
                cursum+=(n-cursum);
                log("1. "+Arrays.toString(res));
            }
            else {
                cursum+=(curmax-res[ind]);
                res[ind]+=(curmax-res[ind]);
                log("2. "+Arrays.toString(res));
            }
            ind--;
            curmax--;
        }
//        return err;

    }	
	
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;

//		20
//		14377409 6746 4829
//		12951446 15189828 3348
//		12419991 457384 4561
//		1128462 1395073 153
//		483233 4675 157
//		10964705 722268 28
//		6708325 5924241 16953
//		8822635 109122 1614
//		14994778 558074 863
//		1854961 15104 371
//		1498563 65983 1409
//		48171 1537 96
//		114186 932 189
//		7381224 14567008 47153
//		2722116 51058 296
//		8434500 2249102 2161
//		3950191 50036 1389
//		6358711 3642 3495
//		14292771 14304 5038
//		10855999 11895 1670		

//		long n=12951446;
//		long k=15189828;
//		int b=3348;

		long[][] src = {
//				{12419991,457384, 4561},
//				{1128462, 1395073, 153},
//				{483233, 4675, 157},
//				{10964705, 722268, 28},
//				{6708325, 5924241, 16953},
//				{8822635, 109122, 1614},
//				{14994778, 558074, 863},
//				{1854961, 15104, 371},
//				{1498563, 65983, 1409},
//				{48171, 1537, 96},
//				{114186, 932, 189},
				{7381224, 14567008, 47153} //,
//				{2722116, 51058, 296},
//				{8434500, 2249102, 2161},
//				{3950191, 50036, 1389},
//				{6358711, 3642, 3495},
//				{14292771, 14304, 5038},
//				{10855999, 11895, 1670}
		};

		for (int i=0;i<src.length;i++) {
			long n=src[i][0];
			long k=src[i][1];
			int b=(int)src[i][2];

			long[] res=bonetrousle2(n,k,b);
			System.out.println(Arrays.toString(res));
			System.out.println(""+n+"    "+checkRes(res,n,k,b));
		}
	}
	
	private static void log(String str) {
		if (doLog)
			System.out.println(str);
	}
	
	private static boolean checkRes(long[] res, long n, long k, int b) {
		Set<Long> set=new HashSet<Long>();
		long sum=0;
		for (long el : res) {
			if (el>k)
				return false;
			sum+=el;
			set.add(el);
		}
		
		if (sum!=n || set.size()!=b)
			return false;
		
		return true;
	}
}
