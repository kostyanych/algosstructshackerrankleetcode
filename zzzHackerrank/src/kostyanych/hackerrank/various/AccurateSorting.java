package kostyanych.hackerrank.various;

import java.util.Scanner;

public class AccurateSorting {

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        
        for(int a0 = 0; a0 < q; a0++){
        
        	int n = in.nextInt();
            int[] a = new int[n];
            for(int a_i=0; a_i < n; a_i++){
                a[a_i] = in.nextInt();
            }
            
            boolean yes=true;
            while (true) {
                boolean wasSuccessfulSwap=false;
                
                for (int i=0;i<n-1;i++) {
                    if ( a[i+1]>a[i] )
                        continue;
                    if ( swap(a,i+1,i) ) {
                        wasSuccessfulSwap=true;
                        continue;
                    }
                    if ( Math.abs(a[i+1]-a[i])>2 ) {
                        yes=false;
                        break;
                    }
                }
                
                if ( !wasSuccessfulSwap || !yes )
                    break;
            }
            
            if (yes)
                System.out.println("Yes");
            else
                System.out.println("No");
        }
    }

    static boolean swap(int[] a, int ind1, int ind2){
        if ( Math.abs(a[ind1]-a[ind2])>1 )
            return false;
        int buf=a[ind1];
        a[ind1]=a[ind2];
        a[ind2]=buf;
        return true;
    }
}
