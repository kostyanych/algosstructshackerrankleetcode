package kostyanych.hackerrank.various;

import static java.util.stream.Collectors.joining;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//import static java.util.stream.Collectors.toList;

public class FrequencyQueries {
	
	static Map<Integer, Count> freq = new HashMap<Integer, Count>();
	static Map<Integer, Count> counts = new HashMap<Integer, Count>();
	
	static int checkcount=0;

	// Complete the freqQuery function below.
	static List<Integer> freqQuery(List<int[]> queries) {
		List<Integer> res = new ArrayList<Integer>();
		for (int[] cmd : queries) {
			switch (cmd[0]) {
			case 1:
				insert(cmd[1]);
				break;
			case 2:
				delete(cmd[1]);
				break;
			case 3:
				res.add(check(cmd[1]));
				break;
			}
		}
		return res;
	}

	static void insert(int val) {
		int was=0;
		Count cnt = freq.get(val);
		if (cnt == null) {
			cnt = new Count();
			freq.put(val, cnt);
		}
		else
			was=cnt.count;
		cnt.inc();
		if (was!=0)
			decreaseCount(was);
		increaseCount(was+1);
	}
	
	static void decreaseCount(int count) {
		if (count==0)
			return;
		Count cnt = counts.get(count);
		if (cnt != null)
			cnt.dec();
	}

	static void increaseCount(int count) {
		if (count==0)
			return;
		Count cnt = counts.get(count);
		if (cnt == null) {
			cnt = new Count();
			counts.put(count, cnt);
		}
		cnt.inc();
	}
	
	static void delete(int val) {
		int was=0;
		Count cnt = freq.get(val);
		if (cnt == null)
			return;

		was = cnt.count;
		if (was==0)
			return;
		
		cnt.dec();
		
		decreaseCount(was);
		if (was-1>0)
			increaseCount(was-1);
	}

	static int check(int val) {
		checkcount++;
		if (checkcount==85) {
			System.out.println("checking VAL="+val);
			Set<Integer> set=freq.keySet();
			for (Integer i : set) {
				if (freq.get(i).count==5)
					System.out.println("!!! 5 of "+i);
			}
		}
		Count cnt = counts.get(val);
		if (cnt == null || cnt.count == 0)
			return 0;
		return 1;
	}

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("e://freqqueries.txt"));
			int q = Integer.parseInt(bufferedReader.readLine().trim());
			List<int[]> queries = new ArrayList<>(q);
			Pattern p = Pattern.compile("^(\\d+)\\s+(\\d+)\\s*$");
			for (int i = 0; i < q; i++) {
				int[] query = new int[2];
				Matcher m = p.matcher(bufferedReader.readLine());
				if (m.matches()) {
					query[0] = Integer.parseInt(m.group(1));
					query[1] = Integer.parseInt(m.group(2));
					queries.add(query);
				}
			}
			bufferedReader.close();
			List<Integer> ans = freqQuery(queries);
			System.out.println(ans.stream().map(Object::toString).collect(joining("\n")) + "\n");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

static class Count {
		public int count = 0;

		public void inc() {
			count++;
		}

		public void dec() {
			count--;
		}

		public String toString() {
			return "" + count;
		}
	}	
}

