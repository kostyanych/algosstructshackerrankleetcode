package kostyanych.hackerrank.various;

import java.util.ArrayList;
import java.util.List;

public class LongestCommonSubsequence {

	public static void testMe(boolean isNeeded){
		if (!isNeeded)
			return;
		
		int[] arr11={1,2,3,4,1};
		int[] arr12={3,4,1,2,1,3, 4};
		int[] arr3={16,27,89,79,60,76,24,88,55,94,57,42,56,74,24,95,55,33,69,29,14,7,94,41,8,71,12,15,43,3,23,49,84,78,73,63,5,46,98,26,40,76,41,89,24,20,68,14,88,26};
		int[] arr4={27,76,88,0,55,99,94,70,34,42,31,47,56,74,69,46,93,88,89,7,94,41,68,37,8,71,57,15,43,89,43,3,23,35,49,38,84,98,47,89,73,24,20,14,88,75};
		int[] arr5={16,27,89,76};
		int[] arr6={27,76,15};
		
		run(arr11,arr12);
	}

	static List<Integer> reverseList=new ArrayList<Integer>();
	
    static int[] arr1;
    static int[] arr2;
    static int[][] table;
    
    public static void run(int[] arr11, int[] arr22) {
    	arr1=arr11;
    	int n=arr1.length;
    	arr2=arr22;
    	int m=arr2.length;
        //calcNaive(arr1, arr2, arr1.length-1, arr2.length-1);
    	table=new int[n+1][m+1];
        initTable(n,m);
        calculateTable();
        reconstructSequence();
         
        int len=reverseList.size()-1;
        for (int i=0;i<=len;i++){
        	System.out.print(reverseList.get(len-i));
            if (i!=len)
            	System.out.print(" ");
        }
        System.out.println();    
    }
    
    static void initTable(int n, int m){
        for (int i=0;i<m;i++){
            table[0][i]=0;
        }
        for (int i=0;i<n;i++){
            table[i][0]=0;
        }
        for (int i=1;i<n;i++){
            for (int j=1;j<m;j++){
                table[i][j]=-1;
            }
        }
    }
    
    static int max(int a, int b){
        if (a>=b)
            return a;
        return b;
    }
    
    static void calculateTable() {
        int n=arr1.length;
        int m=arr2.length;
        for (int i=0;i<n;i++){
            for (int j=0;j<m;j++) {
                if (arr1[i]==arr2[j])
                    table[i+1][j+1]=1+table[i][j];
                else
                    table[i+1][j+1]=max(table[i][j+1],table[i+1][j]);
            }
        }
    }
    
    static void reconstructSequence() {
        int n=arr1.length;
        int m=arr2.length;
        
        while (true) {
            if (table[n][m-1]<table[n][m] && table[n-1][m]<table[n][m]) {
                reverseList.add(arr1[n-1]);
                n--;
                m--;
            }
            else if (table[n][m-1]==table[n][m]) {
                m--;
            }
            else
                n--;
            if (m==0 || n==0)
                break;
        }
    }    
    
    static void calcNaive(int[] arr1, int[] arr2, int ind1, int ind2){
        if (ind1<0 || ind2<0)
            return;
        int indInArr1=findIndInArr(arr1,ind1,arr2[ind2]);
        int indInArr2=findIndInArr(arr2,ind2,arr1[ind1]);
        if (indInArr1==-1 && indInArr2==-1)
        	calcNaive(arr1, arr2, ind1-1, ind2-1);
        else if (indInArr1>=indInArr2) {
            reverseList.add(arr2[ind2]);
            calcNaive(arr1, arr2, indInArr1-1, ind2-1);
        }
        else if (indInArr1<indInArr2) {
            reverseList.add(arr1[ind1]);
            calcNaive(arr1, arr2, ind1-1,indInArr2-1);
        }
    }
    
    static int findIndInArr(int[] arr, int lastInd, int val){
        for (int i=lastInd;i>=0;i--){
            if (arr[i]==val)
                return i;
        }
        return -1;
    }	
	
}
