package kostyanych.hackerrank.various;

import java.io.FileInputStream;
import java.util.Scanner;

public class JimAndTheJokes {
	
	static int daysPerMonth[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	
	static int MAX = decode(31, 12)+1;
	static int[] days=new int[MAX];

	 // Complete the solve function below.
    static long solve(int[][] dates) {
        int a;
        for (int i=0;i<dates.length;i++) {
        	if (dates[i][0]<1 || dates[i][0]>12)
        		continue;
        	if (dates[i][1]<1 || dates[i][1]>daysPerMonth[dates[i][0]])
        		continue;
        	
            a=decode(dates[i][1],dates[i][0]);
            if (a<0)
                continue;
            days[a]++;
        }
        
        //return points.size();
        long res=0;
        for (int i=0;i<MAX;i++) {
            //res+=c.cnt<3?c.cnt:combos(c.cnt);
        	if (days[i]>0)
        		res+=combos(days[i]);
        }
        return res;
    }
    
    static long combos(long n) {
        return n*(n-1)/2l;
    }
    
    static int decode(int num, int rdx) {
        int res=0;
        int i=1;
        int r;
        while (num>0) {
            r=num%10;
            if (r>=rdx)
                return -1;
            res+=i*r;
            i*=rdx;
            num=num/10;
        };
        
        return res;
    }

    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	
    	try {
	    	Scanner scanner = new Scanner(new FileInputStream("e://jimjokes.txt"));
	    	
	        int n = scanner.nextInt();
	        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
	
	        int[][] dates = new int[n][2];
	
	        for (int datesRowItr = 0; datesRowItr < n; datesRowItr++) {
	            String[] datesRowItems = scanner.nextLine().split(" ");
	            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
	
	            for (int datesColumnItr = 0; datesColumnItr < 2; datesColumnItr++) {
	                int datesItem = Integer.parseInt(datesRowItems[datesColumnItr]);
	                dates[datesRowItr][datesColumnItr] = datesItem;
	            }
	        }
	        
	        scanner.close();
	
	        long result = solve(dates);
	        
	        System.out.println(result);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
