package kostyanych.hackerrank.various;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class MergingCommunities {
	static Map<Integer,Set<Integer>> map=new HashMap<Integer,Set<Integer>>();

	public static void runMe(boolean isNeeded){
		if (!isNeeded)
			return;

		try {
			File f=new File("e:\\merg.inp");
	        Scanner sc=new Scanner(f);
	        int n=sc.nextInt();
	        int q=sc.nextInt();
	        int p1;
	        int p2;
	        String c;
	        int qcount=0;
	        for (int i=0;i<q;i++){
	            c=sc.next();
	            if (c.equals("Q")) {
	            	qcount++;
if (qcount==144)
	System.out.println("aaaa");
	                p1=sc.nextInt();
	                query(p1);
	            }
	            else {
	                p1=sc.nextInt();
	                p2=sc.nextInt();
if (p1==990 || p2==990
		|| p1==482 || p2==482
		|| p1==482 || p2==482)
	System.out.println("bbb "+p1+" "+p2);
	                addEdge(p1,p2);
	                addEdge(p2,p1);
	            }
	        }
	        sc.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
    }
    
    static void query(int p){
        int res=0;
        Set<Integer> set=map.get(p);
        if (set==null)
            res=1;
        else
            res=set.size()+1;
        System.out.println(res);
    }
    
    static void addEdge(int p1, int p2){
        Set<Integer> set=map.get(p1);
        if (set==null) {
            set=new HashSet<Integer>();
            map.put(p1,set);
        }
        set.add(p2);
        Set<Integer> set2=map.get(p2);
        if (set2!=null) {
            set.addAll(set2);
        }
        set.remove(p1);
    }	
}
