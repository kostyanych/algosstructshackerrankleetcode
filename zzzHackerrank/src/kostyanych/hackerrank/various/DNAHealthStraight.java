package kostyanych.hackerrank.various;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

class Vertex {
	
	public Vertex() {
		children = new HashMap<Character,Integer>();            
		isLeaf = false;
		parent = -1;
		linkToSuffix = -1;
		idWord = -1;
		linkToWordEnd= -1;
	}

    // Links to the child vertexes in the trie:
    // Key: A single character
    // Value: The ID of vertex
	public Map<Character,Integer> children;

    // Flag that some word from the dictionary ends in this vertex
	public boolean isLeaf;

    // Link to the parent vertex
	public int parent;

    // Char which moves us from the parent vertex to the current vertex
	public char parentChar;

    // Suffix link from current vertex (the equivalent of P[i] from the KMP algorithm)
	public int linkToSuffix;

    // Link to the leaf vertex of the maximum-length word we can make from the current prefix
	public int linkToWordEnd;

    // If the vertex is the leaf, we store the ID of the word
	public int idWord;
}

public class DNAHealthStraight {

	static List<Vertex> trie;
	static List<Integer> wordsLength;
	static int curState = 1;
	static int root = 0;

	static {
		trie = new ArrayList<Vertex>();
		wordsLength = new ArrayList<Integer>();
		trie.add(new Vertex());            
	}

	static private void addString(String s, int wordID) {
		int curVertex = root;
		int len=s.length();
		for (int i = 0; i < len; ++i) {// Iterating over the string's characters
			char c = s.charAt(i);

			Vertex newVert;
			// Checking if a vertex with this edge exists in the trie:
			if (!trie.get(curVertex).children.containsKey(c)) {
				newVert=new Vertex();
				newVert.parent=curVertex;
				newVert.parentChar=c;
				trie.add(newVert);
				trie.get(curVertex).children.put(c,curState);
				curState++;
			}
			curVertex = trie.get(curVertex).children.get(c); // Move to the new vertex in the trie
		}
		// Mark the end of the word and store its ID
		trie.get(curVertex).isLeaf = true;
		trie.get(curVertex).idWord = wordID;
		wordsLength.add(len);
	}
	
	static private void prepare() {
		Queue<Integer> vertexQueue = new LinkedList<Integer>();
		vertexQueue.add(root);
		int curVertex;
		while (!vertexQueue.isEmpty()) {
			curVertex = vertexQueue.remove();
			calcSuffLink(curVertex);

			for (Character key : trie.get(curVertex).children.keySet()) {
				vertexQueue.add(trie.get(curVertex).children.get(key));
			}
		}
	}
	
	static private void calcSuffLink(int vertex) {
		
		Vertex vert=trie.get(vertex);
		
		// Processing root (empty string)
		if (vertex == root) { 
			vert.linkToSuffix = root;
			vert.linkToWordEnd = root;
			return;
		}
		
		// Processing children of the root (one character substrings)
		if (vert.parent == root) { 
			vert.linkToSuffix = root;
			if (vert.isLeaf)
				vert.linkToWordEnd = vertex;
			else
				vert.linkToWordEnd = trie.get(vert.linkToSuffix).linkToWordEnd;
			return;
		}
		// Cases above are degenerate cases as for prefix function calculation; the
		// value is always 0 and links to the root vertex.

		// To calculate the suffix link for the current vertex, we need the suffix
		// link for the parent of the vertex and the character that moved us to the
		// current vertex.
		int curBetterVertex = trie.get(vert.parent).linkToSuffix;
		char chVertex = vert.parentChar; 
		Integer buf;
		// From this vertex and its substring we will start to look for the maximum
		// prefix for the current vertex and its substring.
		while (true) {
			// If there is an edge with the needed char, we update our suffix link
			// and leave the cycle
			buf=trie.get(curBetterVertex).children.get(chVertex);
			if (buf!=null) {
					vert.linkToSuffix = buf;
					break;
			}
			// Otherwise, we are jumping by suffix links until we reach the root
			// (equivalent of k == 0 in prefix function calculation) or we find a
			// better prefix for the current substring.
			if (curBetterVertex == root) { 
				vert.linkToSuffix = root;
					break;
			}
			curBetterVertex = trie.get(curBetterVertex).linkToSuffix; // Go back by sufflink
		}
		// When we complete the calculation of the suffix link for the current
		// vertex, we should update the link to the end of the maximum length word
		// that can be produced from the current substring.
		if (vert.isLeaf)
			vert.linkToWordEnd = vertex; 
		else 
			vert.linkToWordEnd = trie.get(vert.linkToSuffix).linkToWordEnd;
	}
	
    static int getHealth(String text, int first, int last, int[] health, String[] genes) {
    	int res=0;
    	
    	int textlen = text.length();
		
		// Current state value
		int currentState = root;

		// Targeted result value
		int result = 0;
		Vertex vert;

		Character ch;
		for (int j = 0; j < textlen; j++) {
			ch=text.charAt(j);
			// Calculating new state in the trie
			while (true) {
				vert=trie.get(currentState);
				// If we have the edge, then use it
				if (vert.children.containsKey(ch)) {
					currentState = vert.children.get(ch);
					break;
				}
				// Otherwise, jump by suffix links and try to find the edge with
				// this char

	            // If there aren't any possible edges we will eventually ascend to
	            // the root, and at this point we stop checking.
				if (currentState == root) 
					break;
				currentState = vert.linkToSuffix;
			}
			int checkState = currentState;

			// Trying to find all possible words from this prefix
			while (checkState != root) {
				// Checking all words that we can get from the current prefix
				checkState = trie.get(checkState).linkToWordEnd;

				// If we are in the root vertex, there are no more matches
				if (checkState == root)
					break;
				
				// If the algorithm arrived at this row, it means we have found a
				// pattern match. And we can make additional calculations like find
				// the index of the found match in the text. Or check that the found
				// pattern is a separate word and not just, e.g., a substring.
				result++;
				
				int idw=trie.get(checkState).idWord;
				if (idw>=first && idw<=last)
					res+=health[j];
//            	System.out.println("Word "+patterns[idw]+" appears from "+(indexOfMatch));
				// Trying to find all matched patterns of smaller length
				checkState = trie.get(checkState).linkToSuffix;
			}
		}

		return res;
	}
	
	static public void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		
    	if (!isNeeded)
    		return;
        
    	try {
long b1=new Date().getTime();
    		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("e://dnahealth.txt")));        
long b2=new Date().getTime();
System.out.println("InputStream Opened: "+(b2-b1));
b1=new Date().getTime();
			int n = Integer.parseInt(br.readLine().trim());
	        String[] genes = br.readLine().split(" ");
b2=new Date().getTime();
System.out.println("genes read: "+(b2-b1));
b1=new Date().getTime();
			for (int i=0;i<genes.length; i++) {
				addString(genes[i], i);
			}
			prepare();
b2=new Date().getTime();
	        int[] health = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
long b3=new Date().getTime();
System.out.println("machine built: "+(b2-b1));
System.out.println("health read: "+(b3-b2));

	        int s = Integer.parseInt(br.readLine().trim());
	
	        int min=Integer.MAX_VALUE;
	        int max=0;
b1=new Date().getTime();
	        for (int sItr = 0; sItr < s; sItr++) {
	            String[] firstLastd = br.readLine().split(" ");
	
	            int first = Integer.parseInt(firstLastd[0]);
	            int last = Integer.parseInt(firstLastd[1]);
	            
	            String d = firstLastd[2];
	            int buf = getHealth(d,first,last,health, genes);
	            if (buf<min)
	                min=buf;
	            if (buf>max)
	                max=buf;
	        }
b2=new Date().getTime();
System.out.println("another health got: "+(b2-b1));

	        System.out.println(""+min+" "+max);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
	}
	
}
