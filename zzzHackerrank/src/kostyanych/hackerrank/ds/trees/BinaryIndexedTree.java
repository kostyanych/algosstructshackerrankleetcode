package kostyanych.hackerrank.ds.trees;

/**
 * We have an array arr[0 . . . n-1]. We would like to
 *
 * 1. Compute the sum of the first i elements.
 * 2. Modify the value of a specified element of the array arr[i] = x where 0 <= i <= n-1.
 *
 * We use BIT for that.
 *
 * Lets say our number is 101101 in binary.
 *
 * Now, 101101 = 100000 + 1000 + 100 + 1
 *
 * Hence, we can divide the interval [1,101101]  into 4 parts of length 100000, 1000, 100 and 1 respectively.
 *
 * BIT is an array B[1...n] where the ith element stores SUM(j=0;j<2^l)A[i-j]
 * where l is the position of the right-most set bit of i  where the unit's place position is considered to be 0.
 *
 * For example:
 * B[101101] = A[101101]
 * B[101100] = A[101100] + A[101011] + A[101010] + A[101001]
 * B[101000] = A[101000] + A[100111] + A[100110] + A[100101] + A[100011] + A[100010] + A[100001]
 * ....
 *
 *  Thus Get(101101) = B[101101] + B[101100] + B[101000] + B[100000]
 *
 *  Also, i&(-i) will get the number with only the rightmost 1 bit of i:
 *  i=1001110, -i=0110001+1=0110010 => 1001110 & 0110010 = 0000010;
 *
 *  So we should use B[i] for any i from i via i=i - (i&-i) - sequentially subtracting the number with the rightmost bit



and so on.
 *
 */
public class BinaryIndexedTree {

	private final int size;
	private final int[] arr;

	public BinaryIndexedTree(int size) {
		super();
		this.size = size+1;
		this.arr=new int[this.size];
	}

	public BinaryIndexedTree(int sourceArr[]) {
		super();
		this.size = sourceArr.length+1;
		this.arr=new int[this.size];
		for(int i = 0; i < sourceArr.length; i++) {
            updateBIT(i, sourceArr[i]);
		}
	}

    /**
     * Returns sum of arr[0..index].
     * This function assumes that the array is preprocessed and partial sums of array elements are stored in BITree[].
     * @param index
     * @return
     */
    int getSum(int index) {
        int sum = 0; // Initialize result

        // index in BITree[] is 1 more than the index in arr[]
        index = index + 1;

        // Traverse ancestors of BITree[index]
        while(index>0) {
            // Add current element of BITree to sum
            sum += arr[index];

            // Move index to parent node in getSum View
            index -= index & (-index);
        }
        return sum;
    }

    /**
     * Updates a node in Binary Index Tree at given index in source array.
     * The given value 'val' is added to BITree[i] and all of its ancestors in tree.
     * @param index
     * @param val
     */
    public void updateBIT(int index, int val) {
        // index in BITree[] is 1 more than
        // the index in arr[]
        index = index + 1;

        // Traverse all ancestors and add 'val'
        while(index <= size) {
        // Add 'val' to current node of BIT Tree
        	arr[index] += val;

        // Update index to that of parent in update View
        	index += index & (-index);
        }
    }


}
