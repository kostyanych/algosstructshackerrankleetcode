package kostyanych.hackerrank.ds.trees;

public class ArrayAndSimpleQueries {
	static class Elem {
        int val;
        Elem next;
        
        public Elem(int v){
            val=v;
        }
    }
    
    public static void runme(int m, int n, int[] vals, int[][] qs) {
        Elem start=null;
        Elem last=null;
        Elem ths=null;
        for (int i=0;i<m;i++){
            ths=new Elem(vals[i]);
            if (last!=null)
                last.next=ths;
            last=ths;
            if (i==0)
                start=last;
        }
        
        for (int i=0;i<n;i++){
            if (qs[i][0]==1)
                start=toFront(start, qs[i][1], qs[i][2]);
            else
                start=toBack(start, qs[i][1], qs[i][2], m);
        }
        
        printDiff(start);
        printWhole(start);
    }
    
    static void printDiff(Elem start){
        int a1=start.val;
        while (start.next!=null){
            start=start.next;
        }
        System.out.println(Math.abs(a1-start.val));
    }

    static void printWhole(Elem start){
        while (start!=null){
            System.out.print(start.val+" ");
            start=start.next;
        }
        System.out.println();
    }
    
    static Elem toFront(Elem start, int ind1, int ind2) {
    	printWhole(start);
        if (ind1==1)
            return start;
        Elem preRangeStart=start;
        Elem rangeStart=start;
        Elem rangeEnd=null;
        
        for (int i=2;i<ind1;i++) {
            preRangeStart=preRangeStart.next;
        }
        rangeStart=preRangeStart.next;
        rangeEnd=rangeStart;
        for (int i=ind1+1;i<=ind2;i++) {
            rangeEnd=rangeEnd.next;
        }
        preRangeStart.next=rangeEnd.next;
        rangeEnd.next=start;
        return rangeStart;
    }
    
    static Elem toBack(Elem start, int ind1, int ind2, int num) {
    	printWhole(start);
        if (ind2==num)
            return start;
        Elem preRangeStart=start;
        Elem rangeStart=start;
        Elem rangeEnd=null;
        Elem end=null;
        
        for (int i=2;i<ind1;i++){
            preRangeStart=preRangeStart.next;
        }
        if (ind1>1)
        	rangeStart=preRangeStart.next;
        rangeEnd=rangeStart;
        for (int i=ind1+1;i<=ind2;i++) {
            rangeEnd=rangeEnd.next;
        }
        end=rangeEnd.next;
        for (int i=ind2+1;i<num;i++) {
           end=end.next;
        }
        if (ind1>1)
        	preRangeStart.next=rangeEnd.next;
        else
        	start=rangeEnd.next;
        end.next=rangeStart;
        rangeEnd.next=null;
        return start;
    }   
}
