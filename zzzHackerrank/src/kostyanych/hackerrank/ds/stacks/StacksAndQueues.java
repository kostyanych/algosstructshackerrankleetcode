package kostyanych.hackerrank.ds.stacks;

import java.util.Arrays;
import java.util.Stack;

public class StacksAndQueues {

	static long[] riddle2(long[] arr) {
        int len=arr.length;
        long[] res=new long[len];
        
        Stack<Integer> st=new Stack<Integer>();
        int[] l2r=new int[len];
        int[] r2l=new int[len];
        
        st.push(0);
        l2r[0]=1;
        for (int i = 1; i < len; i++) { 
            while (!st.empty() && arr[st.peek()] >= arr[i]) 
                st.pop(); 
      
            if (st.empty())
            	l2r[i]=i+1;
            else
            	l2r[i]=i-st.peek();
      
            st.push(i); 
        }        
        
        st.clear();
        st.push(len-1);
        r2l[len-1]=1;
        for (int i = len-2; i >=0; i--) { 
            while (!st.empty() && arr[st.peek()] >= arr[i]) 
                st.pop(); 
      
            if (st.empty())
            	r2l[i]=len-i;//i+1;
            else
            	r2l[i]=st.peek()-i;
      
            st.push(i); 
        }
        for (int i=0;i<len;i++) {
        	l2r[i]+=(r2l[i]-1);
        }
        int ind;
        for (int i=0;i<len;i++) {
        	ind=l2r[i];
        	res[ind-1]=Math.max(res[ind-1], arr[i]);
        }
System.out.println(Arrays.toString(res));
        for (int i=len-2;i>=0;i--) {
        	res[i]=Math.max(res[i], res[i+1]);
        }

        return res;
    }
	
	
	static long[] riddle(long[] arr) {
        int len=arr.length;
        long[] res=new long[len];
        long buf;
        long curres=-1l;
        for (int win=1;win<=len;win++) {
        	curres=-1l;
            for (int i=0;i<=len-win;i++) {
                buf=mininarr(arr,i,win);
                if(curres<buf)
                    curres=buf;
            }
            res[win-1]=curres;
        }
        return res;
    }
    
    static long mininarr(long[] arr, int st, int win) {
        long min=1000000001l;
        for (int i=st;i<st+win;i++) {
            if (arr[i]<min)
                min=arr[i];
        }
        return min;
    }
    
    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	long[] arr={2,6,1,12};
    	System.out.println(Arrays.toString(arr));
//    	System.out.println(Arrays.toString(riddle(arr)));
    	System.out.println(Arrays.toString(riddle2(arr)));
    }
}
