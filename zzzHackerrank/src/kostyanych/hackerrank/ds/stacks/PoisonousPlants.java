package kostyanych.hackerrank.ds.stacks;

import java.util.Arrays;
import java.util.Stack;

public class PoisonousPlants {

	public static int getMaxDays(int[] plants) {
		if (plants==null || plants.length<1)
			return 0;
		
		int n=plants.length;
		
		int[] days=new int[n];
		int min=plants[0];
		int max=0;
		days[0]=0;
		Stack<Integer> st=new Stack<Integer>();
		st.push(0);
		
		for (int i=1;i<n;i++) {
			if (plants[i]>plants[i-1])
				days[i]=1;
		
            if (min>plants[i])
                min = plants[i];
            
            int iiii=plants[i];
			
			while (!st.isEmpty() && plants[i]<=plants[st.peek()]) {
				if(plants[i] > min) {
//					if (days[i]<=days[st.peek()] + 1)
					if (days[i]!=1)	//those are already dead
                        days[i] = days[st.peek()] + 1;
				}
                st.pop();
			}
			
            if (max<days[i])
                max = days[i];
            st.push(i);

		}
		
		return max;
	}
	
	public static int getMaxDaysMine(int[] plants) {
		if (plants==null || plants.length<1)
			return 0;
		
		int n=plants.length;
		
		int[] days=new int[n];	//all days are zeroes at this point!
		int min=plants[0];
		int max=0;
		days[0]=0;
		Stack<Integer> st=new Stack<Integer>();
		st.push(0);
		
		for (int i=1;i<n;i++) {
			if (min>plants[i])
				min=plants[i]; //everything to the right of it up to a new min, will die eventually

			if (plants[i]>plants[i-1])
				days[i]=1;	//dead instantly
			
			//everything not bigger than stack top should be checked
			//otherwise it's already dead and will goto the top of the stack itself
			while (!st.isEmpty() && plants[i]<=plants[st.peek()]){
				
				if (plants[i] > min) { //it will die surely
					//but only after those that are on the left of it
					if (days[i]<days[st.peek()]+1)
						days[i]=days[st.peek()]+1;
				}
				
				//it's too big, we're not amused with it anymore
				st.pop();
			}
			
			if (max<days[i])
				max=days[i];
			st.push(i);	//whatever it is, push to the stack
		}
		
		return max;
	}
	
}
