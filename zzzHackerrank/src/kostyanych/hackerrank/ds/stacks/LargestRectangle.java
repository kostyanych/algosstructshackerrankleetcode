package kostyanych.hackerrank.ds.stacks;

import java.util.Stack;

public class LargestRectangle {
	
	public static long largestRectangle(int[] h) {
		int res=0;
		
		if (h==null || h.length<1)
			return res;
		
		Stack<Integer> st=new Stack<Integer>();
		st.push(0);
		
		int top=0;
		int last=0;
		int bufArea=0;
		int buf=0;
		
		for (int i=1;i<=h.length;i++) {
			if (i==h.length)
				buf=0;
			else
				buf=h[i];

			if (buf>h[st.peek()]) {
				st.push(i);
			}
			else {
				while (!st.isEmpty() && h[st.peek()]>=buf) {
					last=st.pop();
					top=h[last];
					if (st.isEmpty())
						bufArea=top*i;
					else
						bufArea=top*(i-st.peek()-1); //!!!!
					if (res<bufArea)
						res=bufArea;
				}
				st.push(i);
			}
		}
		
		return res;
	}

}
