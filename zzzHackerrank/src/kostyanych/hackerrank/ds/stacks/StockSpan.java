package kostyanych.hackerrank.ds.stacks;

import java.util.Arrays;
import java.util.Stack;

public class StockSpan {

	// a linear time solution for stock span problem 
    // A stack based efficient method to calculate stock span values 
    static int[] calculateSpan(int price[]) { 
    	
    	int n=price.length;
        // Create a stack and push index of first element to it 
        Stack<Integer> st= new Stack<>(); 
        st.push(0);  
          
         
        int[] res=new int[n];
        
        // Span value of first element is always 1
        res[0] = 1; 
          
        // Calculate span values for rest of the elements 
        for (int i = 1; i < n; i++) { 
            // Pop elements from stack while stack is not empty and top of 
            // stack is smaller than price[i] 
            while (!st.empty() && price[st.peek()] <= price[i]) 
                st.pop(); 
      
            // If stack becomes empty, then price[i] is greater than all elements 
            // on left of it, i.e., price[0], price[1],..price[i-1]. Else price[i] 
            // is greater than elements after top of stack 
            if (st.empty())
            	res[i]=i+1;
            else
            	res[i]=i-st.peek();
      
            // Push this element to stack 
            st.push(i); 
        }
        return res;
    }
    
    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	
    	int[] arr={10, 4, 5, 90, 120, 80};
    	System.out.println(Arrays.toString(arr));
    	System.out.println(Arrays.toString(calculateSpan(arr)));
    }
}
