package kostyanych.hackerrank.ds.arrays;

/**
 * Given two numbers N and M
 * (N indicates the number of elements in the array A[] (1-indexed) and M indicates number of queries)
 * you need to perform two types of queries on the array A[].
 *
 * You are given M queries. Queries can be of two types: Type1 and Type2.
 *
 * Type1 queries are represented as 1 i j : Modify the given array by removing elements from i to j
 *     and inserting them (in the original order) to the front of A[].
 *
 * Type 2 queries are represented as 2 i j : Modify the given array by removing elements from i to j
 *     and adding them (in the original order) to the back of A[].
 *
 * After all queries are performed, print 2 lines:
 * 1: abs(A[1]-A[N])
 * 2: resulting array A[]
 *
 * THIS DOES NOT WORK FAST ENOUGH!!!!
 */
public class ArrayAndSimpleQueries {
	static class Elem {
        int val;
        Elem next;

        public Elem(int v){
            val=v;
        }
    }

    public static void runme(int m, int n, int[] vals, int[][] qs) {
        Elem start=null;
        Elem last=null;
        Elem ths=null;
        for (int i=0;i<m;i++){
            ths=new Elem(vals[i]);
            if (last!=null)
                last.next=ths;
            last=ths;
            if (i==0)
                start=last;
        }

        for (int i=0;i<n;i++){
            if (qs[i][0]==1)
                start=toFront(start, qs[i][1], qs[i][2]);
            else
                start=toBack(start, qs[i][1], qs[i][2], m);
        }

        printDiff(start);
        printWhole(start);
    }

    static void printDiff(Elem start){
        int a1=start.val;
        while (start.next!=null){
            start=start.next;
        }
        System.out.println(Math.abs(a1-start.val));
    }

    static void printWhole(Elem start){
        while (start!=null){
            System.out.print(start.val+" ");
            start=start.next;
        }
        System.out.println();
    }

    static Elem toFront(Elem start, int ind1, int ind2) {
    	printWhole(start);
        if (ind1==1)
            return start;
        Elem preRangeStart=start;
        Elem rangeStart=start;
        Elem rangeEnd=null;

        for (int i=2;i<ind1;i++) {
            preRangeStart=preRangeStart.next;
        }
        rangeStart=preRangeStart.next;
        rangeEnd=rangeStart;
        for (int i=ind1+1;i<=ind2;i++) {
            rangeEnd=rangeEnd.next;
        }
        preRangeStart.next=rangeEnd.next;
        rangeEnd.next=start;
        return rangeStart;
    }

    static Elem toBack(Elem start, int ind1, int ind2, int num) {
    	printWhole(start);
        if (ind2==num)
            return start;
        Elem preRangeStart=start;
        Elem rangeStart=start;
        Elem rangeEnd=null;
        Elem end=null;

        for (int i=2;i<ind1;i++){
            preRangeStart=preRangeStart.next;
        }
        if (ind1>1)
        	rangeStart=preRangeStart.next;
        rangeEnd=rangeStart;
        for (int i=ind1+1;i<=ind2;i++) {
            rangeEnd=rangeEnd.next;
        }
        end=rangeEnd.next;
        for (int i=ind2+1;i<num;i++) {
           end=end.next;
        }
        if (ind1>1)
        	preRangeStart.next=rangeEnd.next;
        else
        	start=rangeEnd.next;
        end.next=rangeStart;
        rangeEnd.next=null;
        return start;
    }
}
