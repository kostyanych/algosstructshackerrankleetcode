package kostyanych.hackerrank.ds.arrays;

import java.util.Arrays;

/**
 * You are given a 1-indexed list of size N, initialized with zeroes. 
 * You have to perform M operations on the list and output 
 * the maximum of final values of all the N elements in the list. 
 * For every operation, you are given three integers A, B and K and you have to add value K 
 * to all the elements ranging from index A to B (both inclusive).
 */
public class ArrayManipulation {

	public static void runme(int n, int m, long[][] queries) {

		go(n, m, queries);
		// goEvenFaster(n, m, queries);
	}

	/**
	 * Initially all elements are zeroes!
	 * We're adding K to index A and -K to index B+1.
	 * Thus every i-th element has the value = SUM(all elements <=i).
	 * So after M queries we just calculate the running sum of all elements and taking the max value. 
	 * @param n
	 * @param m
	 * @param queries
	 */
	static void go(int n, int m, long[][] queries) {

		long max = 0;
		long[] arr = new long[n];

		int a;
		int b;
		long k;

		for (int i = 0; i < m; i++) {
			a = (int) queries[i][0] - 1; // src data is 1-indexed, and we use
											// 0-indexed arrays
			b = (int) queries[i][1];	//and here we don't use -1, 'cause in reality we need b+1 index, not b
			k = queries[i][2];

			arr[a] += k;
			if (b < n)
				arr[b] -= k;
		}

		long sum = 0;
		for (int i = 0; i < n; i++) {
			sum += arr[i];
			if (sum > max)
				max = sum;
		}
		System.out.println(max);
	}

	/**
	 * Idea is the same as in previous method. But in reality, since original list is populated with 0s,
	 * we only care about values from queries.
	 * So, we store these values and then sort them by index, so when we're gonna sum them we'll be
	 * sure to go from lower indices to higher.
	 * So we only need 2*m storage and 2*m summations. (well, 2*m-1, 'cause the last would be -K, 
	 * won't be maximizing our sum)
	 * @param n
	 * @param m
	 * @param queries
	 */
	static void goEvenFaster(int n, int m, long[][] queries) {

		long max = 0;
		LocalPair[] arr = new LocalPair[2*m];

		int a;
		int b;
		long k;

		for (int i = 0; i < m; i++) {
			a = (int) queries[i][0] - 1; // src data is 1-indexed, and we use
											// 0-indexed arrays
			b = (int) queries[i][1];	//and here we don't use -1, 'cause in reality we need b+1 index, not b
			k = queries[i][2];
			arr[2*i]=new LocalPair(a,k);
			arr[2*i+1]=new LocalPair(b,-1*k);
		}
		
		Arrays.sort(arr);

		long sum = 0;
		for (int i = 0; i < 2*m-1; i++) {
			sum += arr[i].delta;
			if (sum > max)
				max = sum;
		}
		System.out.println(max);
	}
	
}

class LocalPair implements Comparable<LocalPair> {
	
	public int index=0;
	public long delta=0;
	
	public LocalPair(int ind, long dlt) {
		index=ind;
		delta=dlt;
	}
	
	@Override
	public int compareTo(LocalPair o) {
		if (this.index<o.index)
			return -1;
		if (this.index>o.index)
			return 1;
		if (this.delta<o.delta)
			return -1;
		if (this.delta==o.delta)
			return 0;
		return 1;
	}
	
}
