package kostyanych.hackerrank.ds.arrays;

/**
 * A left rotation operation on an array of size N shifts each of the array's elements 1 unit to the left. 
 * For example, if 2 left rotations are performed on array {1,2,3,4,5}, 
 * then the array would become {3,4,5,1,2}.
 * 
 * Given an array of N integers and a number D, perform D left rotations on the array. 
 * Then print the updated array as a single line of space-separated integers.
 * 
 * Constraints: 1<= d <= n
 * (even if D could exceed N, just do D=D%N) 
 */
public class LeftRotation {

	public static void runme(int n, int[] arr, int d) {

		d=d%n;
		int[] output=null;
        if (d==0) {
        	output=arr;
        }
        else {
        	output = new int[n];
            for (int i=0;i<n;i++){
//1.easy way:
            	if (i+d<n)
            		output[i]=arr[i+d];
            	else
            		output[i]=arr[i+d-n];
//2.compact way:            	
                //output[(i-d+n)%n]=arr[i];
            }
        }
        for (int i=0;i<n;i++){
            System.out.print(output[i]+" ");
        }
        System.out.println();
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n=10;
		int d=6;
		int[] arr={1,2,3,4,5,6,7,8,9,10};
		LeftRotation.runme(n,arr, d);
	}
	
	
}
