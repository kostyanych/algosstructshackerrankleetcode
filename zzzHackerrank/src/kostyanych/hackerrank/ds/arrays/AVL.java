package kostyanych.hackerrank.ds.arrays;

public class AVL {
	
	public static void testInsert() {
		Node rt=AVL.insert(null, 3);
		AVL.insert(rt, 2);
		AVL.insert(rt, 4);
		AVL.insert(rt, 5);
		AVL.insert(rt, 6);
	}

	static int max(int a, int b){
        if (a>=b)
            return a;
        return b;
    }

    static public int height(Node N) {
        if (N == null)
            return -1;
 
        return N.ht;
    }

    static Node insert(Node root,int val) {
        if (root==null) {
            root=new Node();
            root.ht=0;
            root.val=val;
            return root;
        }
        
        if (val<root.val)
            root.left=insert(root.left,val);
        else
            root.right=insert(root.right,val);
        
        root.ht=1+max(height(root.right),height(root.left));
        
        int balance=height(root.left) - height(root.right);
        
        if (balance>1 && val<root.left.val) {
        	return leftRotate(root);
        }
        if (balance<-1 && val>root.right.val) {
        	return rightRotate(root);
        }
        if (balance>1 && val>root.left.val) {
            root.left=rightRotate(root.left);
            return leftRotate(root);
        }
        if (balance<-1 && val<root.right.val) {
            root.right=leftRotate(root.right);
            return rightRotate(root);
        }
        
        return root;
    }

    static private Node leftRotate(Node x) {
		Node newRt = x.left;
		Node T2 = newRt.right;
 
        newRt.right = x;
        x.left = T2;
 
        //  Update heights
        x.ht = Math.max(height(x.left), height(x.right)) + 1;
        newRt.ht = Math.max(height(newRt.left), height(newRt.right)) + 1;
        
        return newRt;
    }

    static private Node rightRotate(Node x) {
		Node newRt = x.right;
		Node T2 = newRt.left;
 
        newRt.left = x;
        x.right = T2;
 
        //  Update heights
        x.ht = Math.max(height(x.left), height(x.right)) + 1;
        newRt.ht = Math.max(height(newRt.left), height(newRt.right)) + 1;
        
        return newRt;
    }
	
}

class Node { 
	int val;   //Value
	int ht;      //Height
	Node left;   //Left child
	Node right;   //Right child
}