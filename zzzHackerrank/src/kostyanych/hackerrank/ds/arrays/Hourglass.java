package kostyanych.hackerrank.ds.arrays;

/**
 * Given a m x n 2D Array, A.

 We define an hourglass in  to be a subset of values with indices falling in this pattern 
 in 's graphical representation:

   a b c
     d
   e f g

There are X hourglasses in A, and an hourglass sum is the sum of an hourglass' values.

Task: Calculate the hourglass sum for every hourglass in A, then print the maximum hourglass sum.

Constraints: -9 <= A[i][j] <= 9

Note: The amount of hourglasses is obviously (m-2)*(n-2); 
 *
 */

public class Hourglass {
	
	public static void runme(int m, int n, int[][] arr) {
		int maxSum=(m-2)*(n-2)*(-9);
		int curSum;
		for (int i=0;i<m-2;i++) {
			for (int j=0;j<n-2;j++) {
				curSum=arr[i][j]+arr[i][j+1]+arr[i][j+2];
				curSum+=arr[i+1][j+1];
				curSum+=arr[i+2][j]+arr[i+2][j+1]+arr[i+2][j+2];
				if (curSum>maxSum)
					maxSum=curSum;
			}
		}
		System.out.println(maxSum);
	}
	
	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int m=6;
		int n=6;
		int[][] arr={ {1,1,1,0,0,0},
					  {0,1,0,0,0,0},
					  {1,1,1,0,0,0},
					  {0,0,2,4,4,0},
					  {0,0,0,2,0,0},
					  {0,0,1,2,4,0}
					};
		Hourglass.runme(m,n,arr);
	}

}
