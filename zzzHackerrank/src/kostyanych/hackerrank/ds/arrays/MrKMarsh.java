package kostyanych.hackerrank.ds.arrays;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Mr K has a rectangular plot of land which may have marshes where fenceposts cannot be set.
 * He wants you to find the perimeter of the largest rectangular fence that can be built on this land.
 * x marks a marsh and . marks good land.
 */
public class MrKMarsh {

	private static void kMarsh(String[] grid) {
		if (grid==null || grid.length<1)
            System.out.println("impossible");
        int rows = grid.length;
        int cols = grid[0].length();

        int[][][] mx = new int[rows][cols][2];

        for (int i=0;i<cols;i++) {
        	char c=grid[0].charAt(i);
        	if (c=='x') {
        		mx[0][i][0]=0;
        		mx[0][i][1]=0;
        	}
        	else {
        		mx[0][i][0]=1;
        		mx[0][i][1]=(i==0? 1 : mx[0][i-1][1]+1);
        	}
        }

        for (int i=1;i<rows;i++) {
        	char c=grid[i].charAt(0);
        	if (c=='x') {
        		mx[i][0][0]=0;
        		mx[i][0][1]=0;
        	}
        	else {
        		mx[i][0][0]=(mx[i-1][0][0]+1);
        		mx[i][0][1]=1;
        	}
        }

        for (int i=1;i<rows;i++) {
        	for (int j=1;j<cols;j++) {
        		char c=grid[i].charAt(j);
            	if (c=='x') {
            		mx[i][j][0]=0;
            		mx[i][j][1]=0;
            	}
            	else {
            		mx[i][j][0]=mx[i-1][j][0]+1;
            		mx[i][j][1]=mx[i][j-1][1]+1;
            	}

        	}
        }

        int res=0;

        for (int i=rows-1;i>0;i--) {
        	for (int j=cols-1;j>0;j--) {

        		if (mx[i][j][0]==0)
        			continue;
        		if (res>=perimeter(mx[i][j][0],mx[i][j][1]))
        			continue;

        		int buf=check(mx,i,j,res);
        		if (res<buf)
        			res=buf;
        	}
        }

        if (res==0)
        	System.out.println("impossible");
        else
        	System.out.println(res);
	}

	private static int check(int[][][] mx, int row, int col, int globalMax) {
		int xmax=mx[row][col][1];
		int ymax=mx[row][col][0];

		while (xmax>1) {
			if (mx[row][col-xmax+1][0]<2)
				xmax--;
			else
				break;
		}
		if (xmax<2)
			return 0;

		while (ymax>1) {
			if (mx[row-ymax+1][col][1]<2)
				ymax--;
			else
				break;
		}
		if (ymax<2)
			return 0;

		int curMax=0;
		for (int i=ymax;i>1;i--) {
			int maxLeft=Math.min(xmax, mx[row-i+1][col][1]);
			for (int j=maxLeft;j>1;j--) {
				int curP=perimeter(i,j);
				if (curP<=curMax)
					continue;
				if (mx[row][col-j+1][0]<i)
					continue;
				curMax=curP;
			}
		}
		return curMax;
	}


	private static int perimeter(int x, int y) {
		return 2*(x+y)-4;
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
//		String[] arr = {"....", "..x.", "..x.", "x..."};
//		kMarsh(arr);
//		String[] arr1 = {".....", ".x.x.", ".....", "....."};
//		kMarsh(arr1);
//		String[] arr2 = {".x","x."};
//		kMarsh(arr2);
		testWithFile();
	}

	private static void testWithFile() {
		try (FileInputStream fis = new FileInputStream("i://kmarsh.txt");
				Scanner scanner = new Scanner(fis)) {

	        String[] mn = scanner.nextLine().split(" ");

	        int m = Integer.parseInt(mn[0].trim());
	        int n = Integer.parseInt(mn[1].trim());
	        String[] grid = new String[m];

	        for (int gridItr = 0; gridItr < m; gridItr++) {
	            String gridItem = scanner.nextLine();
	            grid[gridItr] = gridItem;
	        }
System.out.println(grid[110].substring(0,14));
	        kMarsh(grid);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
    }

}
