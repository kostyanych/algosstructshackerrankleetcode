package kostyanych.hackerrank.ds.arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * THIS IS NOT A DYNAMIC ARRAY !!!
 * Create a list, seqList, of N empty sequences, where each sequence is indexed from  0 to N-1.
 * The elements within each of the N sequences also use 0-indexing.
 *  
 * Create an integer, lastAnswer, and initialize it to 0.
 *  
 * The 2 types of queries that can be performed on your list of sequences (seqList) are described below:
 * 
 * Query: 1 x y
 * Find the sequence, seq, at (index x^lastAnswer)%N in seqList.
 * Append integer y to sequence .
 * 
 * Query: 2 x y
 * Find the sequence, seq, at (index x^lastAnswer)%N in seqList.
 * Find the value of element y%size in seq (where size is the size of seq) and assign it to lastAnswer.
 * 
 * Print the new value of  on a new line
 * 
 * Task: Given N, and  Q queries, execute each query.
 *
 */
public class DynamicArray {
	
	public static void runme(int n, int q, int[][] queries) {
		@SuppressWarnings("unchecked")
		List<Integer>[] seqList=new List[n];
		for (int i=0;i<n;i++) {
			seqList[i]=new ArrayList<Integer>();
		}
		int lastAnswer=0;
		int seqInd=0;
		int qType;
		int x;
		int y;
		List<Integer> seq;
		for (int i=0;i<q;i++) {
			qType=queries[i][0];
			x=queries[i][1];
			y=queries[i][2];
			
			seqInd=(x^lastAnswer)%n;
			seq=seqList[seqInd];
			if (qType==1)
				seq.add(y);
			else {
				lastAnswer=seq.get(y%seq.size());
				System.out.println(lastAnswer);
			}
		}
	}

	public static void testMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int n=2;
		int q=5;
		int[][] arr={
					  {1,0,5},
					  {1,1,7},
					  {1,0,3},
					  {2,1,0},
					  {2,1,1}
					};
		DynamicArray.runme(n,q,arr);
	}
	
	

}
