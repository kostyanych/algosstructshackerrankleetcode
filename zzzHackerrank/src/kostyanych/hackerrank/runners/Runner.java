package kostyanych.hackerrank.runners;

import kostyanych.hackerrank.algos.dp.NikitaAndTheGame;
import kostyanych.hackerrank.algos.search.SimilarPair;
import kostyanych.hackerrank.ds.arrays.MrKMarsh;

public class Runner {

	public static void main(String[] args) {
		MrKMarsh.testMe(false);
		SimilarPair.test(false);
		NikitaAndTheGame.doMeFromFile(true);
	}

}
