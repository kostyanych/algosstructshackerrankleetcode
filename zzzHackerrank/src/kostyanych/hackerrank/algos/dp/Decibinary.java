package kostyanych.hackerrank.algos.dp;

public class Decibinary {
	
	static int decimalValue(int decibinary) {
		int res=0;
		int bin_factor=1;
		int rem;
		while (decibinary>0) {
			rem=decibinary%10;
			res+=rem*bin_factor;
			decibinary=decibinary/10;
			bin_factor*=2;
		}
		return res;
	}
	
	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		for (int i=0;i<10;i++) {
			System.out.println(""+i+": "+decimalValue(i)+" | "+(10+i)+": "+decimalValue(10+i)+" | "+(20+i)+": "+decimalValue(20+i));
		}
		
		for (int i=100;i<120;i++) {
			System.out.println(""+i+": "+decimalValue(i)+" | "+(10+i)+": "+decimalValue(10+i)+" | "+(20+i)+": "+decimalValue(20+i));
		}
		
	}

}
