package kostyanych.hackerrank.algos.dp;

public class Abbreviation {
	
	 static String abbreviation2(String a, String b) {
	        int lenb=b.length();
	        int lena=a.length();
	        boolean[][] arr=new boolean[lenb][lena];
	        boolean alltrues=false;
	        char curchara;
	        for (int i=0;i<lenb;i++) {
	            alltrues=false;
	            for (int j=0;j<lena;j++) {
	                if (alltrues) {
	                    arr[i][j]=true;
	                    continue;
	                }
	                curchara=a.charAt(j);
	                if (curchara==b.charAt(i) ||
	                		(Character.isLowerCase(curchara)
	                   && Character.toUpperCase(curchara)==b.charAt(i))) {
	                        if (i==0 
	                           || (j>0 && arr[i-1][j-1])) {
	                            arr[i][j]=true;
	                            alltrues=true;
	                        }
	                    }
	            }
	            if (!alltrues)
	                break;
	        }
	        if (arr[lenb-1][lena-1])
	            return "YES";
	        return "NO";
	    }	
	
	static String abbreviation(String a, String b) {
        int lenb=b.length();
        int lena=a.length();
        if (calc(a,lena-1,b,lenb-1))
            return "YES";
        return "NO";
    }
    
    static boolean calc(String a, int inda, String b, int indb) {
        if (inda<0 && indb>=0)
            return false;
        if (indb<0)
            return remainderA(a,inda);
        char curchara=a.charAt(inda);
        char curcharb=b.charAt(indb);
        if (curcharb==curchara)
            return calc(a,inda-1, b, indb-1);
        if (Character.isUpperCase(curchara))
            return false;
        if (Character.toUpperCase(curchara)==curcharb)
            return (calc(a,inda-1,b,indb)
                    ||
                    calc(a,inda-1,b,indb-1));
        return calc(a,inda-1,b,indb);
    }
    
    static boolean remainderA(String a, int inda) {
        while (inda>=0) {
            if (Character.isUpperCase(a.charAt(inda)))
                return false;
            inda--;
        }
        return true;
    }    
    
    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	System.out.println(abbreviation2("beFgH","EFG"));
    }
}
