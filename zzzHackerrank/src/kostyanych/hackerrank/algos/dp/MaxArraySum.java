package kostyanych.hackerrank.algos.dp;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class MaxArraySum {

	static long max(long a, long b, long c) {
        return Math.max(a,Math.max(b,c));
    }

    // Complete the maxSubsetSum function below.
    static long maxSubsetSum(int[] arr) {
        int len = arr.length;
        if (len == 1)
            return arr[0];
        long[] maxes = new long[len];
        maxes[0] = arr[0];
        maxes[1] = arr[0];
        long res=Math.max(maxes[0],maxes[1]);
        for (int i=2;i<len;i++) {
            maxes[i]=max(arr[i],maxes[i-1],maxes[i-2]+arr[i]);
//System.out.println("i="+i+"   arr[i]="+arr[i]+" maxes[i-2]="+maxes[i-2]);
            if (maxes[i]>res)
                res=maxes[i];
        }
System.out.println(Arrays.toString(maxes));        
        return res;
    }

    public static void doMe(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	try {
    		Scanner scanner = new Scanner(new FileInputStream("e://maxarrsum.txt"));

    		int n = scanner.nextInt();
    		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    		int[] arr = new int[n];

    		String[] arrItems = scanner.nextLine().split(" ");
    		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    		for (int i = 0; i < n; i++) {
    			int arrItem = Integer.parseInt(arrItems[i]);
    			arr[i] = arrItem;
    		}
    		
    		scanner.close();

    		long res = maxSubsetSum(arr);
    		
    		System.out.println(res);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
