package kostyanych.hackerrank.algos.dp;

import java.io.FileInputStream;
import java.util.Scanner;

public class NikitaAndTheGame {

	private static int arraySplitting(int[] arr) {
		int len = arr.length;
		if (len < 2)
			return 0;
		long[] sums = new long[len + 1];
		for (int i = 1; i < len; i++) {
			sums[i] = sums[i - 1] + arr[i - 1];
		}
		sums[len] = sums[len - 1] + arr[len - 1];
		return find(sums, 0, len - 1, 0);
	}

	static private int find(long[] sums, int first, int last, long delta) {
		if (first >= last)
			return 0;
		if ( (sums[last+1]-sums[first])%2!=0)
			return 0;
		if (sums[last+1]-sums[first]==0)
			return (last-first);
		long total = sums[last + 1];
		int mid = -1;
		for (int i = first; i <= last; i++) {
			if (sums[i] - delta == total - sums[i]) {
				mid = i;
				break;
			}
		}
		if (mid == -1)
			return 0;
		int left = find(sums, first, mid - 1, delta);
		/*
		 * for (int i=mid+1;i<=last;i++) { sums[i]-=sums[mid]; } sums[mid]=0;
		 */
		int right = find(sums, mid, last, sums[mid]);
		return Math.max(left, right) + 1;
	}

	public static void doMe(boolean isNeeded) {
		if (!isNeeded)
			return;
		int[] arr = { 2, 2, 2, 2 };
		System.out.println(arraySplitting(arr));
	}



	public static void doMeFromFile(boolean isNeeded) {
		if (!isNeeded)
			return;

		try( Scanner scanner = new Scanner(new FileInputStream("h:\\nikita1.txt")) ) {
			int t = Integer.parseInt(scanner.nextLine().trim());
			for (int tItr = 0; tItr < t; tItr++) {
				int arrCount = Integer.parseInt(scanner.nextLine().trim());
				int[] arr = new int[arrCount];

				String[] arrItems = scanner.nextLine().split(" ");
				for (int arrItr = 0; arrItr < arrCount; arrItr++) {
					int arrItem = Integer.parseInt(arrItems[arrItr].trim());
					arr[arrItr] = arrItem;
				}
				int result = arraySplitting(arr);
				System.out.println(result);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
