package kostyanych.hackerrank.algos.sorting;

import java.util.Arrays;

public class CountingInversions {

	public static long countInversions(int[] arr){
        int len = arr.length;
        
        if(len <= 1)
            return 0;
        
        // Recursive Case
        int mid = len/2;
        int[] left = Arrays.copyOfRange(arr, 0, mid);
        int[] right = Arrays.copyOfRange(arr, mid, len);
        
        long inversions = countInversions(left) + countInversions(right);
        
        //leftLen == mid !!! so we'll just skip creation of a new variable 
        int rightLen = right.length; //or len-mid, but semantically I like it worse
        int iLeft = 0;
        int iRight = 0;
        for(int i = 0; i < len; i++) {
        	if (iLeft<mid) {
        		if (iRight>=rightLen || left[iLeft] <= right[iRight])
        			arr[i] = left[iLeft++];
        		else {
        			arr[i] = right[iRight++];
        			inversions+=(mid-iLeft); //since we using an element from the right, we shifting remaining left elements
        		}
        	}
        	else if (iRight<rightLen)
        		arr[i] = right[iRight++]; //no shifting here, since left array is already used up 
        	
//        	if(
//            	iLeft < mid 
//                && (
//                    iRight >= range || left[iLeft] <= right[iRight]
//                )
//            ) {
//                a[i] = left[iLeft++];
//                inversions += iRight;
//            }
//            else if(iRight < range) {
//                a[i] = right[iRight++];
//            }
        }
        
        return inversions;
    }
  
    public static void doMe(int[] arr) {
    	System.out.println(countInversions(arr));
    }
    
    public static void test(boolean isNeeded) {
    	if (!isNeeded)
    		return;
    	int[] arr = {2,1,3,1,2};
    	doMe(arr);
    }
}
