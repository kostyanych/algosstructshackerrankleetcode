package kostyanych.hackerrank.algos.search;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

/**
 * A pair of nodes, (a,b), is a similar pair if the following conditions are true:
 *
 * 1. node a is the ancestor of node b
 * 2. abs(a-b)<=k
 *
 * Given a tree where each node is labeled from 1 to n, find the number of similar pairs in the tree.
 *
 */
public class SimilarPair {

	private static long res=0;

	static private long similarPair(int n, int k, int[][] edges) {
		int len=edges.length;

		long[] bit=new long[n+1];
		Map<Integer,List<Integer>> map = new HashMap<>();

		//nodes are from 1 to n with no gaps!!!
		boolean[] isRoot = new boolean[n+1];
		Arrays.fill(isRoot, true);
		for (int i=0;i<len;i++) {
			List<Integer> lst = map.computeIfAbsent(edges[i][0], key -> new ArrayList<>());
			lst.add(edges[i][1]);
			isRoot[edges[i][1]]=false;
		}
		int iRoot=-0;
		for (int i=1;i<=n;i++) {
			if (isRoot[i]) {
				iRoot=i;
				break;
			}
		}

		specialDfs(iRoot, n, bit, k, map);
		return res;
	}

	/**
	 * BIT contains all ancestral nodes for current node
	 * While traversing (dfs-ing) the tree we
	 * 1. query BIT for how many ANCESTRAL nodes fall into [node.val-k;node.val+k] (while checking boundaries of 1..n)
	 * 2. put curr node into BIT
	 * @param node
	 * @param bit
	 * @param k
	 */
	private static void specialDfs(int node1, int n, long[] bit, int k, Map<Integer,List<Integer>> map) {
		Stack<Integer> st = new Stack<>();
		st.push(node1);
		boolean[] visited=new boolean[n+1];
		while (!st.empty()) {
			int node = st.peek();
			if (visited[node]) {
				bitUpdate(bit, n, node, -1);
				st.pop();
				continue;
			}
			visited[node]=true;
			res += bitQuery(bit, n, Math.max(1, node-k), Math.min(n, node+k));
			bitUpdate(bit, n, node, 1);
			List<Integer> lst = map.get(node);
			if (lst!=null) {
				for(Integer i : lst)
					st.push(i);
			}
		}

	}

	private static void specialDfsRecursive(int node, int n, long[] bit, int k, Map<Integer,List<Integer>> map) {
		res += bitQuery(bit, n, Math.max(1, node-k), Math.min(n, node+k));
	    bitUpdate(bit, n, node, 1);
	    List<Integer> lst = map.get(node);
	    if (lst!=null) {
	    	for(Integer i : lst)
	    		specialDfs(i, n, bit, k, map);
	    }
	    bitUpdate(bit, n, node, -1);
	}

	/**
	 * Returns sum of bit range [i;j]
	 * That is bit sum(j) - bit sum(i-1)
	 * @param bit
	 * @param i
	 * @param j
	 * @return
	 */
	private static long bitQuery(long[] bit, int n, int i, int j) {
	    long res=0;
	    while(j>0) {
	        res+=bit[j];
	        j -= (j & (j*-1));
	    }

	    i--;
	    while(i>0) {
	        res-=bit[i];
	        i -= (i & (i*-1));
	    }
	    return res;
	};

	private static void bitUpdate(long[] bit, int n, int i, long diff) {
		while(i<=n) {
	        bit[i] += diff;
	        i += (i & (i*-1));
	    }
	};

	public static void test(boolean isNeeded) {
		if (!isNeeded)
			return;

		int n=5;
		int k=2;
		int[][] arr = {{3,2}, {3,1}, {1,4}, {1,5}};
		System.out.println(similarPair(n,k,arr));
		testWithFile();
	}

	private static void testWithFile()  {
		try (Scanner scanner = new Scanner(new FileInputStream("h://similar.txt"))) {

			String[] nk = scanner.nextLine().split(" ");
	        int n = Integer.parseInt(nk[0].trim());
	        int k = Integer.parseInt(nk[1].trim());
	        int[][] edges = new int[n-1][2];

	        for (int edgesRowItr = 0; edgesRowItr < n-1; edgesRowItr++) {
	            String[] edgesRowItems = scanner.nextLine().split(" ");

	            for (int edgesColumnItr = 0; edgesColumnItr < 2; edgesColumnItr++) {
	                int edgesItem = Integer.parseInt(edgesRowItems[edgesColumnItr].trim());
	                edges[edgesRowItr][edgesColumnItr] = edgesItem;
	            }
	        }

	        long result = similarPair(n, k, edges);
	        System.out.println(result);
	    }
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
