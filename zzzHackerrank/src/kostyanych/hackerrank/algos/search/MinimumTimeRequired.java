package kostyanych.hackerrank.algos.search;

public class MinimumTimeRequired {

	static long minTime(long[] machines, long goal) {
        long buf;
        long min=machines[0], max=machines[0];
        for (int i=1;i<machines.length;i++) {
        	buf=machines[i];
        	if (buf<min)
        		min=buf;
        	if (buf>max)
        		max=buf;
        }
        long maxdays=goal*min;
        long mindays=1l;//goal/max+1;
        long mid=(maxdays+mindays)/2;        
        long minDays=mindays;
        long prod;
        
        while (true) {
            prod=0;
            for (int i=0;i<machines.length;i++) {
                prod+=mid/machines[i];
            }
            if (prod>=goal) {
                minDays=mid;
                maxdays=mid;
                mid=(mindays+mid)/2;
            }
            else {
                if (mid==minDays-1)
                    return minDays;
                mindays=mid;
                mid=(mid+maxdays)/2;
            }
        }
    }
	
	public static void test(boolean isNeeded) {
		if (!isNeeded)
			return;
		
		long[] arr={2,3};
		System.out.println(minTime(arr,5));
	}
}
