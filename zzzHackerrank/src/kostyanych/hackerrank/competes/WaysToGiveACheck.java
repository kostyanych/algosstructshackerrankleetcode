package kostyanych.hackerrank.competes;

public class WaysToGiveACheck {

	static final char PAWN='P';
    static final char KING='k';
    static final char EMPTY='#';
    static final char ROOK='R';
    static final char QUEEN='Q';
    static final char BISHOP='B';

    static int waysToGiveACheck(char[][] board) {
        int res=0;
        char[] line7=board[1];
        for (int i=0;i<8;i++) {
        	if (discoveredChecks(i, board)>0)
        		res+=4;
        	else if (line7[i]==PAWN && board[0][i]==EMPTY) {
                res+=knightMoves(i, board);
                res+=2*bishopMoves(i, board);
                res+=2*towerMoves(i, board);
            }
        }
        return res;
    }
    
    static int discoveredChecks(int col, char[][] board) {
    	//1 tower left
    	char left='#';
    	for (int i=col-1;i>=0;i--){
    		if (board[1][i]==ROOK || board[1][i]==QUEEN) {
    			left=ROOK;
    			break;
    		}
    		if (board[1][i]==KING) {
    			left=KING;
    			break;
    		}
    		if (board[1][i]!=EMPTY)
    			break;
    	}
    	if (left!='#') {
        	for (int i=col+1;i<8;i++){
        		if (left==ROOK && board[1][i]==KING)
        			return 1;
        		if (left==KING && (board[1][i]==ROOK || board[1][i]==QUEEN))
        			return 1;
        		if (board[1][i]!=EMPTY)
        			break;
        	}
    	}
    	if (col>0 && checkLeftDiag(col, board))
    		return 1;
    	if (col<7 && checkRightDiag(col, board))
    		return 1;
    	return 0;
    }
    
    static boolean checkLeftDiag(int col, char[][] board) {
    	int c=col-1;
    	int r=2;
    	char left='#';
    	while (c>=0 && r<8) {
    		if (board[r][c]==BISHOP || board[r][c]==QUEEN) {
    			left=BISHOP;
    			break;
    		}
    		if (board[r][c]==KING) {
    			left=KING;
    			break;
    		}
    		if (board[r][c]!=EMPTY)
    			break;
    		c--;
    		r++;
    	}
    	c=col+1;
    	r=0;
    	while (c<8 && r>=0) {
    		if (left==KING && (board[r][c]==BISHOP || board[r][c]==QUEEN))
    			return true;
    		if (left==BISHOP && board[r][c]==KING)
    			return true;
    		if (board[r][c]!=EMPTY)
    			break;
    		c++;
    		r--;
    	}
    	return false;
    }
    
    static boolean checkRightDiag(int col, char[][] board) {
    	int c=col-1;
    	int r=0;
    	char left='#';
    	while (c>=0 && r>=0) {
    		if (board[r][c]==BISHOP || board[r][c]==QUEEN) {
    			left=BISHOP;
    			break;
    		}
    		if (board[r][c]==KING) {
    			left=KING;
    			break;
    		}
    		if (board[r][c]!=EMPTY)
    			break;
    		c--;
    		r--;
    	}
    	c=col+1;
    	r=2;
    	while (c<8 && r<8) {
    		if (left==KING && (board[r][c]==BISHOP || board[r][c]==QUEEN))
    			return true;
    		if (left==BISHOP && board[r][c]==KING)
    			return true;
    		if (board[r][c]!=EMPTY)
    			break;
    		c++;
    		r++;
    	}
    	return false;
    }
    
    static int knightMoves(int col, char[][] board) {
        if (col>1) {
            if (board[1][col-2]==KING)
                return 1;
        }
        if (col>0) {
            if (board[2][col-1]==KING)
                return 1;
        }
        if (col<6) {
            if (board[1][col+2]==KING)
                return 1;
        }
        if (col<7) {
            if (board[2][col+1]==KING)
                return 1;
        }
        return 0;
    }

    static int bishopMoves(int col, char[][] board) {
        //left
        int res=0;
        int c=col-1;
        int r=1;
        while (c>=0 && r<8) {
            if (board[r][c]==KING)
                res++;
            if (board[r][c]!=EMPTY)
                break;
            c--;
            r++;
        }
        //right
        c=col+1;
        r=1;
        while (c<8 && r<8) {
            if (board[r][c]==KING)
                res++;
            if (board[r][c]!=EMPTY)
                break;
            c++;
            r++;
        }
        return res;
    }

    static int towerMoves(int col, char[][] board) {
        if (checkVertTowerLine(col, board))
            return 1;
        for (int i=col-1;i>=0;i--){
            if (board[0][i]==KING)
                return 1;
            if (board[0][i]!=EMPTY)
                break;
        }
        for (int i=col+1;i<8;i++){
            if (board[0][i]==KING)
                return 1;
            if (board[0][i]!=EMPTY)
                break;
        }
        return 0;
    }
    
    static boolean checkVertTowerLine(int col, char[][] board) {
        for (int r=2;r<8;r++) {
            if (board[r][col]==KING)
                return true;
            if (board[r][col]!=EMPTY)
                return false;
        }
        return false;
    }    
    
    public static void run(char[][] board) {
    	int result = waysToGiveACheck(board);
        System.out.println(result);
    }
}
