package kostyanych.hackerrank.competes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kostya
 * https://www.hackerrank.com/contests/w36/challenges/a-race-against-time
 */
public class ARaceAgainstTime {
	
	static int[] lstToArr(List<Integer> lst) {
		int size=lst.size();
		int[] arr=new int[size];
		for (int i=0;i<size;i++) {
			arr[i]=lst.get(i);
		}
		return arr;
	}
	
	static long doIt(int n, int mason_height, int[] heights, int[] prices) {
		List<Integer> lessHeights=new ArrayList<Integer>();
		List<Integer> lessPrices=new ArrayList<Integer>();
		int maxHeight=mason_height;
		for (int i=0;i<n-1;i++) {
			if (heights[i]>maxHeight
					|| prices[i]<1) {
				maxHeight=heights[i];
				lessHeights.add(heights[i]);
				lessPrices.add(prices[i]);
			}
		}
		
		int[] newheights=lstToArr(lessHeights);
		int[] newprices=lstToArr(lessPrices);
		int newn=newheights.length+1;
		return n+raceAgainstTime(newn, mason_height, newheights, newprices);
	}

	static long raceAgainstTime(int n, int mason_height, int[] heights, int[] prices) {
		if (n==1)
			return 0;
		long[] calcprice=new long[n-1];
        calcprice[n-2]=prices[n-2];
        if (n-3>=0) {
        	for (int i=n-3;i>=0;i--) {
        		calcprice[i]=prices[i]+calcMinForGiven(i, heights[i], heights, prices, calcprice, n-1);
        	}
        }
        return calcMinForGiven(-1, mason_height, heights, prices, calcprice, n-1);
    }
	
	static long calcMinForGiven(int cur_ind, int curHeight, int[] heights, int[] prices, long[] calcprice, int n) {
//		long minCheck=calcprice[cur_ind+1];
		long minPrice=calcprice[cur_ind+1]+Math.abs(curHeight-heights[cur_ind+1]);
//		long minPrice=0;
		boolean hadToGoUp=false;
		long buf=0;
		for (int i=cur_ind+1;i<n;i++) {
			if (curHeight>=heights[i]) { //optimize by goind lower
				if (prices[i]>0)
					continue;
//				if (calcprice[i]>minCheck)
//					continue;
				buf=calcprice[i]+Math.abs(curHeight-heights[i]);
//				minCheck=calcprice[i];
				if (minPrice>buf)
					minPrice=buf;
			}
			else { //have to go higher
				hadToGoUp=true;
				buf=calcprice[i]+Math.abs(curHeight-heights[i]);
				if (minPrice>buf)
					minPrice=buf;
				break;
			}
		}
		if (!hadToGoUp && minPrice>0)
			minPrice=0;
		return minPrice;
	}

    public static void run(int n, int mason_height, int[] heights, int[] prices) {
        //long result = raceAgainstTime(n, mason_height, heights, prices);
    	long result = doIt(n, mason_height, heights, prices);
        System.out.println(result);
    }	
}
