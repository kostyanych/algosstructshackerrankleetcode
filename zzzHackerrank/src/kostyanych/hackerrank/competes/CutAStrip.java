package kostyanych.hackerrank.competes;

public class CutAStrip {
	
	static int[][] copyArray(int[][] arr) {
		int [][] res = new int[arr.length][];
		for(int i = 0; i < arr.length; i++) {
		  int[] aMatrix = arr[i];
		  res[i] = new int[aMatrix.length];
		  System.arraycopy(aMatrix, 0, res[i], 0, aMatrix.length);
		}
		return res;
	}

	static long cutAStrip(int[][] arr, int k) {
    	int cols = arr[0].length;
        int rows = arr.length;
        
        long bufRes=findMaxSubMatrix(arr);
        long res=bufRes;
        
        int[][] buf;
        int curStripLen=0;
        int curval=0;
        long stripVal=0;
		// 1. left to right
        for (int r=0;r<rows;r++) {
        	for (int c=0;c<cols;c++) {
            	curStripLen=0;
            	stripVal=0;
            	buf=null;
            	if (arr[r][c]>=0)
            		continue;
            	buf=copyArray(arr);

        		for (int cc=c;cc<cols && curStripLen<k;cc++) {
       				curval=arr[r][c];

       				if (curval>=0 && curStripLen<1)
       					continue;
       				stripVal+=curval;
       				curStripLen++;
       				buf[r][cc]=0;
       				if (stripVal>=0)
       					continue;
       				bufRes=findMaxSubMatrix(buf);
       				if (bufRes>res)
       					res=bufRes;
        		}
        	}
        }

		// 2. left to right
        for (int c=0;c<cols;c++) {
        	for (int r=0;r<rows;r++) {
            	curStripLen=0;
            	stripVal=0;
            	buf=null;
            	if (arr[r][c]>=0)
            		continue;
            	buf=copyArray(arr);
       			for (int rr=c;rr<rows && curStripLen<k;rr++) {
       				curval=arr[rr][c];

       				if (curval>=0 && curStripLen<1)
       					continue;
       				if (curval<0) {
       					stripVal+=curval;
       					curStripLen++;
       				}
       				if (stripVal>=0) {
       					buf[rr][c]=0;
       					continue;
       				}
       				bufRes=findMaxSubMatrix(buf);
       				if (bufRes>res)
       					res=bufRes;
        		}
        	}
        }
        
        return res;
    }
	
	public static long[] kadane(long[] a) {
        //result[0] == maxSum, result[1] == start, result[2] == end;
        long[] result = new long[]{Long.MIN_VALUE, 0, -1};
        int currentSum = 0;
        int localStart = 0;
        
        for (int i = 0; i < a.length; i++) {
        	currentSum += a[i];
            if (currentSum < 0) {
            	currentSum = 0;
                localStart = i + 1;
            }
            else if (currentSum > result[0]) {
                result[0] = currentSum;
                result[1] = localStart;
                result[2] = i;
            }
        }
         
        //all numbers in a are negative, just return max value and its indices
        if (result[2] == -1) {
            result[0] = 0;
            for (int i = 0; i < a.length; i++) {
                if (a[i] > result[0]) {
                    result[0] = a[i];
                    result[1] = i;
                    result[2] = i;
                }
            }
        }
         
        return result;
    }
    
    /**
     * To find maxSum, (left, top),(right, bottom)
     * return {maxSum, left, top, right, bottom}
     * 
     */
    public static long findMaxSubMatrix(int[][] a) {
    	//result[0] == maxSum, result[1] == left, result[2] == top, result[3] == right, result[4] == bottom;
    	int cols = a[0].length;
        int rows = a.length;
        long maxSum = Long.MIN_VALUE;
        long[] currentResult;
         
        for (int leftCol = 0; leftCol < cols; leftCol++) {
        	long[] tmp = new long[rows];
     
            for (int rightCol = leftCol; rightCol < cols; rightCol++) {
         
            	for (int i = 0; i < rows; i++) {
            		tmp[i] += a[i][rightCol];
                }
            	
                currentResult = kadane(tmp);
                if (currentResult[0] > maxSum) {
                	maxSum = currentResult[0];
                }
            }
        }
        
        return maxSum;
    }	

    public static void run(int n, int m, int k, int[][] arr) {
    	
    	boolean allPositive=true;
    	int minVal=arr[0][0];
    	long sum=0;
    	
        int[][] a = new int[n][m];
        for(int A_i = 0; A_i < n; A_i++){
            for(int A_j = 0; A_j < m; A_j++){
                a[A_i][A_j] = arr[A_i][A_j];
                sum+=a[A_i][A_j];
                if (a[A_i][A_j]<0)
                	allPositive=false;
                if (a[A_i][A_j]<minVal)
                	minVal=a[A_i][A_j];
            }
        }    	
        long result = 0;
        if (allPositive)
        	result=sum-minVal;
        else
        	result=cutAStrip(a,k);
        System.out.println(result);
    }	
}
