package kostyanych.hackerrank.competes;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Scanner;

public class SuperiorCharacters {

	public static void testMe() {
		//long fr[] = {1,2,2,3,4,0,3,4,4,1,3,1,4,4,1,0,0,0,0,0,4,2,3,2,2,1};
		//long fr[] = {3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		long fr[] = {1,1,7,2};
		//long fr[] = {15,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//		long fr[] = {922,39, 636, 498, 184, 686, 484, 139, 477, 84, 808, 53, 689, 509, 921, 902, 241, 805, 604, 326, 969, 650, 165, 412, 467, 903};
		long res = maximumSuperiorCharacters4(fr);
		System.out.println(res);
//		workWithFile();
	}
	
	static void workWithFile() {
		try {
			Scanner sc=new Scanner(new FileInputStream(new File("e:\\supchar")));
			int t = sc.nextInt();
	        // scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

	        for (int tItr = 0; tItr < t; tItr++) {
	            long[] freq = new long[26];
	            for (int i=0;i<26;i++) {
	                freq[i]=sc.nextLong();
	            }

	            long result = maximumSuperiorCharacters4(freq);
	            System.out.println(result);

	        }			
			sc.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	static long maximumSuperiorCharacters2(long[] freq) {
        long cnt=0;
        int start=0;
        int cur=freq.length-1;
        for (int i=cur;i>start;i--) {
        	while (start<cur) {
        		if (freq[cur]<1) {
        			cur--;
        			continue;
        		}
        		if ( (freq[cur]+1) <= freq[start] ) {
        			cnt+=freq[cur];
        			freq[start]=freq[start]-freq[cur]-1;
        			freq[cur]=0;
        			cur--;
        		}
        		else if (freq[start]==1) {
        			while (start<cur) {
        				start++;
        				if (freq[start]>0) {
        					freq[start]++;
        					break;
        				}
        			}
        		}
        		else {
        			cnt+=freq[start]-1;
        			freq[cur]=freq[cur]-(freq[start]-1);
        			freq[start]=1;
        			//start++;
        		}
        	}
        }
        return cnt;
	}
	
	static long  min(long a, long b) {
		if (a<=b)
			return a;
		return b;
	}

	static long maximumSuperiorCharacters3(long[] freq) {
		int len=freq.length;
		int maxind=len-1;
		long letterscount=0;
		
		for (int i=0;i<len;i++) {
			letterscount+=freq[i];
		}
		
		long biggestchars=0;
		long curans=0;
		long nextbiggestfreq=0;
		long remainder=0;
        for (int i=0;i<len;i++){
//        	if (letterscount>=2*biggestchars+1)
//        		curans=biggestchars;
        	nextbiggestfreq=freq[maxind-i]; //m
        	remainder = letterscount - (biggestchars+nextbiggestfreq); //s
        	if ( remainder>0 && (biggestchars+nextbiggestfreq+1)>remainder ) {
                long x = remainder-1-biggestchars;
                long y=min(biggestchars,(nextbiggestfreq-x)/2);
                curans=biggestchars+x+y;
                break;
            }
        	else
        		curans=biggestchars;
        	biggestchars+=freq[maxind-i];
        }

        return curans;
	}

	static long maximumSuperiorCharacters4(long[] freq) {
		int len=freq.length;
		int maxind=len-1;
		long letterscount=0;
		
		for (int i=0;i<len;i++) {
			letterscount+=freq[i];
		}
		
		long curans=0;
		long nextbiggestfreq=0;
		long remainder=0;
        for (int i=0;i<len;i++){
        	nextbiggestfreq=freq[maxind-i]; //m
        	remainder = letterscount - (curans+nextbiggestfreq); //s
        	if (remainder<1)	//all chars are exhausted
        		break;
        	//remainder must be greater or equal, than possible superior chars count + 1 (_ SUP _ SUP _ = 2 sups,3 remains)
        	if (remainder>=(curans+nextbiggestfreq+1))
        		curans+=nextbiggestfreq;
        	else {
//                long x = remainder-(curans+1);	//how much will be left (or needed!) for nextbigchars
//                long y=min(curans,(nextbiggestfreq-x)/2);
//                curans=curans+x+y;
        		long partOfNextBig=(remainder-curans+nextbiggestfreq-1)/2;
        		if (partOfNextBig<0)
        			partOfNextBig=0;
        		partOfNextBig=min(partOfNextBig,remainder-1);
        		curans+=partOfNextBig;
                break;
            }
        	
        }

        return curans;
	}

	
	
	static long maximumSuperiorCharacters(long[] freq) {
        long cnt=0;
//        long prev[]=new long[freq.length];
//        prev[0]=0;
//        for (int i=1;i<freq.length;i++) {
//        	prev[i]=prev[i-1]+freq[i-1];
//        }
        for (int i=1;i<freq.length;i++) {
        	cnt=findMax(freq,cnt,i);
        }
        return cnt;
	}
	
	static long findMax(long[] freq,long curMax,int start) {
        long prevamount=0;
        long cnt=0;
        long buf=0;
        long ths=0;
		
        for (int i=1;i<freq.length;i++) {
            prevamount+=freq[i-1];
            if (i<start)
            	continue;
            ths=freq[i];
            if (ths<1)
                continue;
            if (prevamount==1)
                continue;
            if (prevamount==2)
                buf=1;
            else {
                if (ths+1<=prevamount)
                    buf=ths;
                else {
//                    if (prevamount%2==0)
//                        buf=prevamount-1;
//                    else
//                        buf=prevamount;
                	buf=prevamount-1;
                }
            }
            cnt+=buf;
            prevamount=prevamount-(buf+1)-buf+1;
        }
        if (cnt>curMax)
        	return cnt;
        return curMax;
    }	
}
